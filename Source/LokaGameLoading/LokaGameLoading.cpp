// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "LokaGameLoading.h"
#include "GenericApplication.h"
#include "GenericApplicationMessageHandler.h"
#include "SlateExtras.h"
#if !WITH_EDITOR
#include "SLoadingScreen.h"
#endif
// This module must be loaded "PreLoadingScreen" in the .uproject file, otherwise it will not hook in time!
//
class FLokaGameLoadingModule : public ILokaGameLoadingModule
{
protected:
#if !UE_SERVER && !WITH_EDITOR
	TSharedPtr<SLoadingScreen> Slate_LoadingScreen;
#endif

public:
	virtual void StartupModule() override
	{		

#if !UE_SERVER && !WITH_EDITOR

		FLokaStyle::Initialize();

		SAssignNew(Slate_LoadingScreen, SLoadingScreen);

		FLoadingScreenAttributes LoadingScreen;
		LoadingScreen.bAutoCompleteWhenLoadingCompletes = false;
		LoadingScreen.bMoviesAreSkippable = false;
		LoadingScreen.MinimumLoadingScreenDisplayTime = 100.0f;
		LoadingScreen.PlaybackType = EMoviePlaybackType::MT_LoadingLoop;
		LoadingScreen.WidgetLoadingScreen = Slate_LoadingScreen.ToSharedRef();
		LoadingScreen.MoviePaths.Empty();

		SetLoadingConfiguration();

		if (GetMoviePlayer()) GetMoviePlayer()->SetupLoadingScreen(LoadingScreen);
#endif
	}
	
	virtual bool IsGameModule() const override
	{
		return true;
	}

#if !UE_SERVER && !WITH_EDITOR
	virtual void PlayMovie(bool bForce) override
	{
		if ( IsMoviePlayerEnabled() && (!GetMoviePlayer()->IsMovieCurrentlyPlaying() || bForce) )
		{
			// If we are focing a move to play, stop any existing movie.		
			if (bForce && GetMoviePlayer()->IsMovieCurrentlyPlaying())
			{
				StopMovie();
			}

			if (GetMoviePlayer())
			{
				GetMoviePlayer()->PlayMovie();
			}
		}
	}

	virtual void StopMovie() override
	{
		if (GetMoviePlayer()) GetMoviePlayer()->StopMovie();
	}

	virtual void WaitForMovieToFinished() override
	{
		if (GetMoviePlayer())
		{
			GetMoviePlayer()->WaitForMovieToFinish();
		}
	}


	virtual bool IsMoviePlaying() override
	{
		return GetMoviePlayer() ? GetMoviePlayer()->IsMovieCurrentlyPlaying() : false;
	}
	
	virtual void SetLoadingConfiguration(const FSlateBrush* InImage = nullptr, const FText& InTitle = FText::GetEmpty(), const FText& InTip = FText::GetEmpty()) override
	{
		if (Slate_LoadingScreen.IsValid())
		{
			if (InImage)
				Slate_LoadingScreen->SetLoadingImage(InImage);
			else
				Slate_LoadingScreen->SetRandomImage();

			Slate_LoadingScreen->SetLoadingTitle(InTitle);

			if (InTip.IsEmpty() == false)
				Slate_LoadingScreen->SetLoadingTip(InTip);
			else
				Slate_LoadingScreen->SetRandomTip();
		}
	}

#endif
};

IMPLEMENT_GAME_MODULE(FLokaGameLoadingModule, LokaGameLoading);
