// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

// This module must be loaded "PreLoadingScreen" in the .uproject file, otherwise it will not hook in time!

public class LokaGameLoading : ModuleRules
{
    public LokaGameLoading(ReadOnlyTargetRules Target) : base(Target)
	{
        PrivatePCHHeaderFile = "LokaGameLoading.h";

        PublicIncludePaths.AddRange( new string[] {
			"LokaGameLoading",
        } );
	
        PrivateDependencyModuleNames.AddRange(
			new string[] {
				"Core",
				"CoreUObject",
				"MoviePlayer",
                "LokaGame",
                "Slate",
				"SlateCore",
				"InputCore"
			}
		);
	}
}
