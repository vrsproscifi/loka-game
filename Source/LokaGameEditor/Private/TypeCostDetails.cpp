
#include "LokaGameEditor.h"
#include "DetailWidgetRow.h"
#include "TypeCostDetails.h"
#include "Slate/Components/SResourceTextBoxType.h"
#include "Types/TypeCost.h"
#include "IDetailChildrenBuilder.h"
#include "LokaStyle.h"
#include "SResourceTextBox.h"

TSharedRef<IPropertyTypeCustomization> FTypeCostDetails::MakeInstance()
{
	return MakeShareable(new FTypeCostDetails);
}

void FTypeCostDetails::CustomizeHeader(TSharedRef<class IPropertyHandle> InStructPropertyHandle, class FDetailWidgetRow& HeaderRow, IPropertyTypeCustomizationUtils& StructCustomizationUtils)
{
	TSharedPtr<IPropertyHandle> TypeHandle = InStructPropertyHandle->GetChildHandle(GET_MEMBER_NAME_CHECKED(FTypeCost, Currency));
	TSharedPtr<IPropertyHandle> ValueHandle = InStructPropertyHandle->GetChildHandle(GET_MEMBER_NAME_CHECKED(FTypeCost, Amount));	

	TArray<TSharedPtr<EGameCurrency::Type>> ItemsList;
	ItemsList.Add(MakeShareable(new EGameCurrency::Type(EGameCurrency::Money)));
	ItemsList.Add(MakeShareable(new EGameCurrency::Type(EGameCurrency::Donate)));
	ItemsList.Add(MakeShareable(new EGameCurrency::Type(EGameCurrency::Coin)));

	HeaderRow
	.NameContent()
	[
		InStructPropertyHandle->CreatePropertyNameWidget()
	]
	.ValueContent().MaxDesiredWidth(500)
	[
		SNew(SHorizontalBox)
		+ SHorizontalBox::Slot().AutoWidth().Padding(2, 0)
		[
			SNew(SBox).WidthOverride(48).HeightOverride(24)
			[
				SNew(SComboBox<TSharedPtr<EGameCurrency::Type>>)
				.OptionsSource(new TArray<TSharedPtr<EGameCurrency::Type>>(ItemsList))
				.OnGenerateWidget_Lambda([&](TSharedPtr<EGameCurrency::Type> InItem) -> TSharedRef<SWidget> {
					return SNew(SResourceTextBox).CustomSize(FVector(24, 24, 0)).Type(static_cast<EResourceTextBoxType::Type>(*InItem)).Visibility(EVisibility::HitTestInvisible);
				})
				.Content()
				[
					SNew(SResourceTextBox).CustomSize(FVector(24, 24, 0)).Type(this, &FTypeCostDetails::GetCurrencyType, TypeHandle.ToSharedRef()).Visibility(EVisibility::HitTestInvisible)
				]
				.OnSelectionChanged(this, &FTypeCostDetails::OnCostTypeChanged, TypeHandle.ToSharedRef())
			]
		]
		+ SHorizontalBox::Slot().AutoWidth().Padding(2, 0)
		[
			SNew(SBox).MinDesiredWidth(100)
			[
				ValueHandle->CreatePropertyValueWidget()
			]
		]
	];
}

void FTypeCostDetails::CustomizeChildren(TSharedRef<class IPropertyHandle> InStructPropertyHandle, class IDetailChildrenBuilder& StructBuilder, IPropertyTypeCustomizationUtils& StructCustomizationUtils)
{
	
}

void FTypeCostDetails::OnCostTypeChanged(TSharedPtr<EGameCurrency::Type> InItem, ESelectInfo::Type, TSharedRef<IPropertyHandle> PropertyHandle)
{
	PropertyHandle->SetValue(static_cast<uint8>(*InItem));
}

EResourceTextBoxType::Type FTypeCostDetails::GetCurrencyType(TSharedRef<IPropertyHandle> PropertyHandle) const
{
	uint8 Value;
	PropertyHandle->GetValue(Value);
	return static_cast<EResourceTextBoxType::Type>(Value);
}
