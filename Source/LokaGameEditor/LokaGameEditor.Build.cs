// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.IO;

public class LokaGameEditor : ModuleRules
{
	public LokaGameEditor(ReadOnlyTargetRules Target) : base(Target)
	{
        PrivatePCHHeaderFile = "Public/LokaGameEditor.h";

        //==================================================
        //  PublicDependencyModuleNames
        PublicDependencyModuleNames.AddRange(
			new string[] {
				"Core", 
				"CoreUObject", 
				"Engine", 
				"InputCore",
				"AssetRegistry",
				"OnlineSubsystem",
				"OnlineSubsystemUtils",
				"HTTP",
				"Json",
				"JsonUtilities",
				"UnrealEd", 
				"PropertyEditor"
			});
		
        //==================================================
        //  PrivateDependencyModuleNames
        PrivateDependencyModuleNames.AddRange(
			new string[] {
				"InputCore",
				"Slate",
				"SlateCore",
				"XmlParser",
				"LokaGame"
			});

		PrivateIncludePathModuleNames.AddRange(
			new string[] {
				"LokaGame",
				"PropertyEditor",
			});

		//==================================================
		PublicIncludePaths.AddRange(
			new string[] {
				"LokaGameEditor/Public",
			});

		PrivateIncludePaths.AddRange(
			new string[] {
				"LokaGameEditor/Private",
			});

		PrivateIncludePaths.AddRange(
			new string[] {
				"LokaGame",
				"LokaGame/Entity",
				"LokaGame/Slate/Components",
				"LokaGame/ServiceController",
				"LokaGame/ServiceController/IdentityController",
				"LokaGame/ServiceController/LobbyController",
				"LokaGame/ServiceController/StoreController",
			}
		);
	}
}
