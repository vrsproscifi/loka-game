// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DeveloperSettings.h"
#include "LokaEditorSettings.generated.h"


/**
 * 
 */
UCLASS(config = Editor, defaultconfig, meta = (DisplayName = "LOKA Settings", ToolTip = "LOKA Editor engine settings for extra action, entity etc..."))
class LOKAGAMEEDITOR_API ULokaEditorSettings : public UDeveloperSettings
{
	GENERATED_BODY()
	
public:

	UPROPERTY(globalconfig, EditAnywhere, Category = "Entity Capture Images", meta = (AllowedClasses = "MaterialInterface", DisplayName = "Capture Image Material"))
	FStringAssetReference Asset_SourceMaterial;

	UPROPERTY(globalconfig, EditAnywhere, Category = "Entity Capture Images", meta = (AllowedClasses = "TextureRenderTarget2D", DisplayName = "Capture Image Texture"))
	FStringAssetReference Asset_SourceTexture;
	
	UPROPERTY(globalconfig, EditAnywhere, Category = "Entity Capture Images", meta = (AllowedClasses = "TextureCube", DisplayName = "Capture Sky Cubemap"))
	FStringAssetReference Asset_SkyCubemap;

	UPROPERTY(globalconfig, EditAnywhere, Category = "Entity Capture Images", meta = (DisplayName = "Capture Sky Cubemap Angle"))
	float Conf_SkyCubemapAngle;

	UPROPERTY(globalconfig, EditAnywhere, Category = "Entity Capture Images", meta = (DisplayName = "Capture Sky Intensity"))
	float Conf_SkyIntensity;

	UPROPERTY(globalconfig, EditAnywhere, Category = "Entity Capture Images", meta = (DisplayName = "Captured Texture Path"))
	FDirectoryPath Path_CapturedStaticTexture;

	UPROPERTY(globalconfig, EditAnywhere, Category = "Entity Capture Images", meta = (DisplayName = "Capture Distance Multipler"))
	float Conf_DistanceMultipler;

	UPROPERTY(globalconfig, EditAnywhere, Category = "Entity Capture Images", meta = (DisplayName = "Capture Allow Auto-Save"))
	bool Conf_IfSaveCaptures;

	UPROPERTY(globalconfig, EditAnywhere, Category = "Entity Capture Images", meta = (DisplayName = "Capture Light Rotation"))
	FRotator Conf_LightRotation;
};
