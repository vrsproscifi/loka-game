
#pragma once

#include "CoreMinimal.h"
#include "IPropertyTypeCustomization.h"
#include "PropertyHandle.h"
#include "SResourceTextBoxType.h"
#include "TypeDescription.h"

class FTypeCostDetails : public IPropertyTypeCustomization
{
public:
	static TSharedRef<IPropertyTypeCustomization> MakeInstance();

	virtual void CustomizeHeader(TSharedRef<class IPropertyHandle> InStructPropertyHandle, class FDetailWidgetRow& HeaderRow, IPropertyTypeCustomizationUtils& StructCustomizationUtils) override;
	virtual void CustomizeChildren(TSharedRef<class IPropertyHandle> InStructPropertyHandle, class IDetailChildrenBuilder& StructBuilder, IPropertyTypeCustomizationUtils& StructCustomizationUtils) override;

protected:

	EResourceTextBoxType::Type GetCurrencyType(TSharedRef<IPropertyHandle> PropertyHandle) const;
	void OnCostTypeChanged(TSharedPtr<EGameCurrency::Type> InItem, ESelectInfo::Type, TSharedRef<IPropertyHandle>);
};
