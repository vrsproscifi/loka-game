// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/SCompoundWidget.h"
#include "Styles/TutorialSelectorWidgetStyle.h"


typedef TMap<int32, FText> FTutorialSelectorData;

class LOKAGAME_API STutorialSelector : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(STutorialSelector)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FTutorialSelectorStyle>("STutorialSelectorStyle"))
	{}
	SLATE_STYLE_ARGUMENT(FTutorialSelectorStyle, Style)
	SLATE_EVENT(FOnInt32ValueChanged, OnSelected)
	SLATE_ARGUMENT(FTutorialSelectorData, Data)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

protected:

	const FTutorialSelectorStyle* Style;

	FOnInt32ValueChanged OnSelected;

	TSharedRef<SWidget> GenerateButton(const TPair<int32, FText>& InButton) const;

	TSharedPtr<SHorizontalBox> Widget_Container;
};
