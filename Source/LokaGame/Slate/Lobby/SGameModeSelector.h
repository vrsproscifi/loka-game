// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/SCompoundWidget.h"
#include "GameModeTypeId.h"

class SAnimatedBackground;

DECLARE_DELEGATE_OneParam(FOnSelectedGameMode, const EGameMode&);

class LOKAGAME_API SGameModeSelector : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SGameModeSelector)
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_EVENT(FOnSelectedGameMode, OnSelectedGameMode)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	void ToggleWidget(const bool InToggle);

protected:

	FOnSelectedGameMode OnSelectedGameMode;

	TSharedPtr<SAnimatedBackground> Widget_Background[2];
	TSharedPtr<STileView<TSharedPtr<EGameMode>>> Widget_TileView;

	void OnSelected(TSharedPtr<EGameMode> InItem, ESelectInfo::Type InType);
};
