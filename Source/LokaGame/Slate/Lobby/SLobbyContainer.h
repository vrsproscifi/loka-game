// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SBaseContainer.h"

class SAnimatedBackground;
struct EGameMode;
class SGameModeSelector;
class AConstructionPlayerState;
class UPlayerFractionEntity;
struct FPlayerEntityInfo;

/**
 * 
 */
class LOKAGAME_API SLobbyContainer : public SBaseContainer
{
public:
	SLATE_BEGIN_ARGS(SLobbyContainer)
	{
		_Visibility = EVisibility::HitTestInvisible;
	}
	SLATE_ARGUMENT(TWeakObjectPtr<AConstructionPlayerState>, PlayerState)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);
	
	virtual void ToggleWidget(const bool InToggle) override;
	virtual bool IsInInteractiveMode() const override { return GetVisibility() == EVisibility::SelfHitTestInvisible; }

	FText GetIntoFightButtonText() const;
	ECheckBoxState GetIntoFightButtonState() const;

protected:
	
	TSharedPtr<SGameModeSelector> Widget_GameModeSelector;
	
	FReply OnClickedIntoFight(const EGameMode InGameMode);
	FReply OnClickedFightOptions();
};
