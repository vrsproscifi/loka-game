// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "STutorialSelector.h"
#include "SlateOptMacros.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void STutorialSelector::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	OnSelected = InArgs._OnSelected;

	ChildSlot
	[
		SAssignNew(Widget_Container, SHorizontalBox)
	];

	for (auto TargetData : InArgs._Data)
	{
		Widget_Container->AddSlot().AutoWidth()
		[
			GenerateButton(TargetData)
		];
	}
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

TSharedRef<SWidget> STutorialSelector::GenerateButton(const TPair<int32, FText>& InButton) const
{
	return SNew(SBox).MinDesiredWidth(300).MinDesiredHeight(100).Padding(4)
			[
				SNew(SButton)
				.ButtonStyle(&Style->Button)
				.TextStyle(&Style->Text)
				.HAlign(HAlign_Center)
				.VAlign(VAlign_Center)
				.ContentPadding(FMargin(5.0f))
				.Text(InButton.Value)
				.OnClicked_Lambda([&, i = InButton.Key]()
				{
					OnSelected.ExecuteIfBound(i);
					return FReply::Handled();
				})
			];
}