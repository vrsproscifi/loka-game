// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Utilities/SUsableCompoundWidget.h"
#include "Styles/LobbyContainerWidgetStyle.h"
#include "Windows/SWindowsContainer.h"
#include "TypeCost.h"

class SAnimatedBackground;
struct EGameMode;
class SGameModeSelector;
class AConstructionPlayerState;
class UPlayerFractionEntity;
struct FPlayerEntityInfo;
/**
 * 
 */
class LOKAGAME_API SBaseContainer : public SUsableCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SBaseContainer)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FLobbyContainerStyle>(TEXT("SLobbyContainerStyle")))
	{
		_Visibility = EVisibility::HitTestInvisible;
	}
	SLATE_STYLE_ARGUMENT(FLobbyContainerStyle, Style)
	SLATE_ARGUMENT(TWeakObjectPtr<AConstructionPlayerState>, PlayerState)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	TSharedRef<SWindowsContainer> GetWindowsContainer() const { return WindowsContainer.ToSharedRef(); }

	virtual void ToggleWidget(const bool InToggle) override;
	virtual bool IsInInteractiveMode() const override { return GetVisibility() == EVisibility::SelfHitTestInvisible; }

	void ToggleWidgetFromWindow(const bool InToggle);

	int64 GetPlayerCurrency(const EGameCurrency::Type) const;
	int64 GetPlayerLevel() const;
	int64 GetBoosterValue(const int32 InFlag) const;

	FText GetPlayerName() const;
	FText GetPlayerFractionRank() const;

	TOptional<float> GetPlayerLevelPercent() const;
	TOptional<float> GetPlayerFractionRankPercent() const;

	const FSlateBrush* GetPlayerFractionBrush() const;

	EVisibility GetBoosterVisibility(const int32 InFlag) const;

	FPlayerEntityInfo GetPlayerInfo() const;
	const UPlayerFractionEntity* GetPlayerFraction() const;

protected:

	const FLobbyContainerStyle* Style;

	TWeakObjectPtr<AConstructionPlayerState> PlayerState;

	TSharedPtr<SAnimatedBackground> Widget_Background;
	TSharedPtr<SWindowsContainer> WindowsContainer;
	TSharedPtr<SOverlay> Widget_Container;

	const FSlateBrush* EmptyBrush;

	FReply OnClickedCurrency(const EGameCurrency::Type InType);
};
