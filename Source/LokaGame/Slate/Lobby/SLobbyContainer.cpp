// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "SLobbyContainer.h"
#include "SlateOptMacros.h"

#include "Windows/SWindowsContainer.h"
#include "Windows/SWindowQuit.h"
#include "Components/SResourceTextBox.h"
#include "Friends/SFriendsForm.h"
#include "Layout/SScaleBox.h"

#include "ConstructionPlayerState.h"
#include "IdentityComponent.h"
#include "ProgressComponent.h"
#include "SquadComponent.h"
#include "PlayerFractionEntity.h"
#include "UTGameUserSettings.h"

#include "SSettingsManager.h"
#include "SGameModeSelector.h"
#include "SAnimatedBackground.h"
#include "Windows/SWindowHelp.h"
#include "NodeComponent/NodeSessionMatch.h"
#include "SLobbyChat.h"
#include "SNotifyList.h"
#include "Windows/SWindowDuels.h"
#include "SLokaToolTip.h"
#include "SRadialMenu.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SLobbyContainer::Construct(const FArguments& InArgs)
{
	SBaseContainer::Construct(SBaseContainer::FArguments().PlayerState(InArgs._PlayerState));
		
	MAKE_UTF8_SYMBOL(sOptions, 0xf078);

#pragma region WidgetConstruction
	
	Widget_Container->AddSlot().HAlign(HAlign_Center).VAlign(VAlign_Top).Padding(0, 10)
	[
		SNew(SBox).MinDesiredWidth(240)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot().AutoHeight()
			[
				SNew(SBox).MinDesiredHeight(40)
				[
					SNew(SCheckBox) // Into Fight
					.Type(ESlateCheckBoxType::ToggleButton)
					.Style(&Style->IntoFightButton)
					[
						SNew(SBox)
						.VAlign(VAlign_Center)
						.HAlign(HAlign_Center)
						[
							SNew(STextBlock)
							.TextStyle(&FLokaStyle::Get().GetWidgetStyle<FTextBlockStyle>(TEXT("Default_HeaderTextBlock")))
							.Text(this, &SLobbyContainer::GetIntoFightButtonText)
						]
					]
					.IsChecked(this, &SLobbyContainer::GetIntoFightButtonState)
					.OnCheckStateChanged_Lambda([&](ECheckBoxState) -> void {
						OnClickedIntoFight(EGameMode::AllFight);
					})
				]
			]
			+ SVerticalBox::Slot().AutoHeight().Padding(Style->FightOptionsPadding)
			[
				SNew(SButton) // Fight options
				.TextStyle(&Style->FightOptionsText)
				.ButtonStyle(&Style->FightOptionsButton)
				.VAlign(VAlign_Center)
				.HAlign(HAlign_Center)
				.Text(FText::FromString(sOptions))
				.OnClicked(this, &SLobbyContainer::OnClickedFightOptions)
				.IsEnabled_Lambda([&]() -> bool { return GetIntoFightButtonState() == ECheckBoxState::Unchecked; })
			]
		]
	];

	Widget_Container->AddSlot().HAlign(HAlign_Right).VAlign(VAlign_Center).Padding(10, 0)
	[
		SNew(STextBlock)
		.Text(NSLOCTEXT("SLobbyContainer", "SLobbyContainer.Coop", "Cooperative"))
		.TextStyle(&Style->CooperativeText)
		.Visibility_Lambda([&]() { return (PlayerState.IsValid() && PlayerState->GetNetMode() != NM_Standalone) ? EVisibility::Visible : EVisibility::Collapsed; })
	];

	Widget_Container->AddSlot()
	[
		SAssignNew(Widget_GameModeSelector, SGameModeSelector)
		.OnSelectedGameMode_Lambda([&](const EGameMode& InMode)
		{
			OnClickedIntoFight(InMode);
		})
	];

#pragma endregion 

}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SLobbyContainer::ToggleWidget(const bool InToggle)
{
	if (InToggle || (InToggle == false && WindowsContainer->RequestToggle(false, false)))
	{
		SetVisibility(InToggle ? EVisibility::SelfHitTestInvisible : EVisibility::HitTestInvisible);
		Widget_Background->ToggleWidget(InToggle);
		OnToggleInteractive.ExecuteIfBound(InToggle);
	}
}

FText SLobbyContainer::GetIntoFightButtonText() const
{
	if (PlayerState.IsValid() && PlayerState.Get()->GetSquadComponent())
	{
		auto SquadInformation = PlayerState.Get()->GetSquadComponent()->GetSqaudInformation();

		if (SquadInformation.IsInConstructionMode())
		{
			SquadInformation.Status = ESearchSessionStatus::None;
		}

		if (SquadInformation.Status == ESearchSessionStatus::None)
		{
			if (SquadInformation.IsInSquad() && SquadInformation.IsLeader == false)
			{
				if (SquadInformation.IsReady)
				{
					return NSLOCTEXT("SLobbyContainer", "SLobbyContainer.SquadNotReady", "Not ready");
				}
				return NSLOCTEXT("SLobbyContainer", "SLobbyContainer.SquadReady", "Ready");
			}
			return NSLOCTEXT("SLobbyContainer", "SLobbyContainer.SessionSearchBegin", "Into Battle");
		}

		if (SquadInformation.Status == ESearchSessionStatus::MatchSearch)
		{
			return NSLOCTEXT("SLobbyContainer", "SLobbyContainer.SessionSearchCancel", "Cancel");
		}

		//============================================================================================
		//	Client states
		if (SquadInformation.Status == ESearchSessionStatus::SearchStarting)
		{
			return NSLOCTEXT("SLobbyContainer", "SLobbyContainer.SessionSearchStarting", "Prepare to search");
		}

		if (SquadInformation.Status == ESearchSessionStatus::SearchStopping)
		{
			return NSLOCTEXT("SLobbyContainer", "SLobbyContainer.SessionSearchStopping", "Search canceling");
		}

		if (SquadInformation.Status == ESearchSessionStatus::SearchPrepare && SquadInformation.IsLeader)
		{
			return NSLOCTEXT("SLobbyContainer", "SLobbyContainer.SessionSearchPrepare", "Waiting for a members");
		}

		//============================================================================================
		//	Complete
		if (SquadInformation.Status == ESearchSessionStatus::FoundContinue)
		{
			return NSLOCTEXT("SLobbyContainer", "SLobbyContainer.SessionSearchComplete", "Complete");
		}

		if (SquadInformation.Status == ESearchSessionStatus::FoundPrepare)
		{
			return NSLOCTEXT("SLobbyContainer", "SLobbyContainer.SessionSearchFoundPrepare", "Prepare to join");
		}

		if (SquadInformation.Status == ESearchSessionStatus::FoundError)
		{
			return NSLOCTEXT("SLobbyContainer", "SLobbyContainer.SessionSearchFoundError", "Join error");
		}

		if (SquadInformation.Status == ESearchSessionStatus::FoundJoin)
		{
			return NSLOCTEXT("SLobbyContainer", "SLobbyContainer.SessionSearchFoundJoin", "Entering the game");
		}
	}

	return NSLOCTEXT("SLobbyContainer", "SLobbyContainer.SessionSearchWait", "Waiting");
}

ECheckBoxState SLobbyContainer::GetIntoFightButtonState() const
{
	if (PlayerState.IsValid() && PlayerState.Get()->GetSquadComponent())
	{
		auto SquadInformation = PlayerState.Get()->GetSquadComponent()->GetSqaudInformation();

		if (SquadInformation.IsInConstructionMode())
		{
			SquadInformation.Status = ESearchSessionStatus::None;
		}

		if (SquadInformation.Status == ESearchSessionStatus::None)
		{
			if (SquadInformation.IsInSquad() && SquadInformation.IsLeader == false)
			{
				if (SquadInformation.IsReady)
				{
					return ECheckBoxState::Checked;
				}
			}

			return ECheckBoxState::Unchecked;
		}

		if (SquadInformation.Status == ESearchSessionStatus::SearchPrepare && SquadInformation.IsLeader)
		{
			return ECheckBoxState::Checked;
		}

		if (SquadInformation.Status == ESearchSessionStatus::MatchSearch)
		{
			return ECheckBoxState::Checked;
		}
	}
	
	return ECheckBoxState::Undetermined;
}

FReply SLobbyContainer::OnClickedIntoFight(const EGameMode InGameMode)
{
	if (PlayerState.IsValid() && PlayerState.Get()->GetSquadComponent())
	{
		auto SquadComponent = PlayerState.Get()->GetSquadComponent();

		FSessionMatchOptions Options;
		Options.GameMode = InGameMode.Value;

		if (FFlagsHelper::HasAnyFlags(InGameMode.ToFlag(), EGameMode::DuelMatch.ToFlag()))
		{
			Options.Bet = FTypeCost(500, EGameCurrency::Money);
			Options.WeaponType = EWeaponType::All.ToFlag();
			Options.AdditionalFlag = 10;
		}

		SquadComponent->SendRequestToggleSearch(Options);
	}

	return FReply::Handled();
}

FReply SLobbyContainer::OnClickedFightOptions()
{
	Widget_GameModeSelector->ToggleWidget(true);
	return FReply::Handled();
}