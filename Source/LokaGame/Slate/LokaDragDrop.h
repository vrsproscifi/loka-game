// VRSPRO

#pragma once

#include "DragAndDrop.h"
#include "Widgets/SCompoundWidget.h"

class FLokaDragDropOp : public FGameDragDropOperation
{
public:
	DRAG_DROP_OPERATOR_TYPE(FLokaDragDropOp, FGameDragDropOperation)

	FLokaDragDropOp();
	FLokaDragDropOp(TSharedRef<SWidget> Decorator);
	FLokaDragDropOp(TSharedRef<SWidget> Source, TSharedRef<SWidget> Decorator);
	FLokaDragDropOp(TSharedRef<SWidget> Source, TSharedRef<SWidget> Decorator, UObject* Object);

	virtual TSharedPtr<SWidget> GetDefaultDecorator() const override;
	virtual void OnDragged(const class FDragDropEvent& DragDropEvent) override;
	virtual void Construct() override
	{
		FGameDragDropOperation::Construct();
	}

	template<typename ClassType>
	TSharedRef<ClassType> GetSourceAs()
	{
		return StaticCastSharedRef<ClassType>(SourceRef);
	}

	TWeakObjectPtr<UObject> GetObjectPtr() const;

protected:
	TSharedRef<SWidget> SourceRef;
	TSharedRef<SWidget> DecoratorRef;
	TWeakObjectPtr<UObject> ObjectPtr;
	float DPIScale;
};