﻿// VRSPRO

#include "LokaGame.h"
#include "SRadialMenu.h"
#include "SlateOptMacros.h"
#include "SConstraintCanvas.h"
#include "SAnimatedBackground.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SRadialMenu::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	bRotatedSegments = InArgs._RotatedSegments;
	SegmentScale = InArgs._SegmentsScale;
	WheelRadius = InArgs._Radius;
	OnGenerateCenter = InArgs._OnGenerateCenterWidget;
	OnActivateSlot = InArgs._OnActivateSlot;

	for (auto &slot : InArgs.Slots)
	{
		Children.Add(slot);
	}	

	ChildSlot.HAlign(HAlign_Center).VAlign(VAlign_Center)
	[
		SAssignNew(Widget_Animation, SAnimatedBackground)
		.InitAsHide(true)
		.IsEnabledBlur(false)
		.Duration(.35f)
		.IsRelative(true)
		.ShowAnimation(EAnimBackAnimation::ZoomIn)
		.HideAnimation(EAnimBackAnimation::ZoomIn)
		[
			SNew(SGridPanel)
			+ SGridPanel::Slot(0, 0, SGridPanel::Layer(0)).HAlign(HAlign_Fill).VAlign(VAlign_Fill)
			[
				SAssignNew(Widget_WheelContainer, SBox)
				[
					SNew(SBorder).HAlign(HAlign_Fill).VAlign(VAlign_Fill)
					.Padding(0)
					.BorderImage(&Style->Background)
					.RenderTransformPivot(FVector2D(.5f, .5f))
					.RenderTransform(FSlateRenderTransform(FQuat2D(FMath::DegreesToRadians(90))))
					[
						SNew(SBorder).HAlign(HAlign_Fill).VAlign(VAlign_Fill)
						.Padding(0)
						.BorderImage(&Style->OuterShadow)
						[
							SNew(SBorder).HAlign(HAlign_Fill).VAlign(VAlign_Fill)
							.Padding(0)
							.BorderImage(&Style->InnerShadow)
							[
								SNew(SBorder).HAlign(HAlign_Fill).VAlign(VAlign_Fill)
								.Padding(0)
								.BorderImage_Lambda([&]() { return &MIDBrush; })
								.BorderBackgroundColor(this, &SRadialMenu::GetSelectorColor)
							]
						]
					]	
				]
			]
			+ SGridPanel::Slot(0, 0, SGridPanel::Layer(1)).HAlign(HAlign_Center).VAlign(VAlign_Center)
			[
				SAssignNew(Widget_Container, SConstraintCanvas)
				.RenderTransformPivot(FVector2D(.5f, .5f))
			]
			+ SGridPanel::Slot(0, 0, SGridPanel::Layer(2)).HAlign(HAlign_Center).VAlign(VAlign_Center)
			[
				SAssignNew(Widget_CenterContainer, SBox).HAlign(HAlign_Center).VAlign(VAlign_Center)
			]
		]
	];	

	SelectedSegment = INDEX_NONE;
	RefreshWheel();
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SRadialMenu::ToggleWidget(const bool InToggle)
{
	Widget_Animation->ToggleWidget(InToggle);
	SetVisibility(InToggle ? EVisibility::Visible : EVisibility::HitTestInvisible);
	OnToggleInteractive.ExecuteIfBound(InToggle);

	if (InToggle == true)
	{
		const FVector2D localCenter = GetCachedGeometry().LocalToAbsolute(GetCachedGeometry().GetLocalSize() / 2);
		FSlateApplication::Get().SetCursorPos(localCenter);
	}

	if (InToggle == false && SelectedSegment != INDEX_NONE && Children.IsValidIndex(SelectedSegment))
	{
		if (FFlagsHelper::HasAnyFlags(Children[SelectedSegment].Method.Get(), ESegmentActiveMethod::Close))
		{
			Children[SelectedSegment].OnActivated.ExecuteIfBound();
			OnActivateSlot.ExecuteIfBound(SelectedSegment);
		}

		SelectedSegment = INDEX_NONE;
	}
}

bool SRadialMenu::IsInInteractiveMode() const
{
	return GetVisibility() == EVisibility::Visible;
}

void SRadialMenu::RefreshWheel()
{
	Widget_Container->ClearChildren();

	if (Children.Num() >= 2)
	{
		const float sPInum = PI * 2;
		FVector2D sSize(WheelRadius / 2 * SegmentScale, WheelRadius / 2 * SegmentScale);

		for (SIZE_T SlotIdx = 0; SlotIdx < Children.Num(); ++SlotIdx)
		{
			FVector2D sPosition(WheelRadius, WheelRadius);

			const float sCosSinSource = (SlotIdx * sPInum) / float(Children.Num()) + FMath::DegreesToRadians(-90.0f);

			const float sAngle = sCosSinSource + FMath::DegreesToRadians(90.0f);
			const float cCos = FMath::Cos(sCosSinSource);
			const float cSin = FMath::Sin(sCosSinSource);

			sPosition += FVector2D(WheelRadius * cCos, WheelRadius * cSin);

			Widget_Container->AddSlot().Offset(FMargin(sPosition.X, sPosition.Y, sSize.X, sSize.Y)).Alignment(FVector2D::ZeroVector).Anchors(FAnchors(0))
				[
					SNew(SBox).HAlign(HAlign_Center).VAlign(VAlign_Center)
					.RenderTransform(FSlateRenderTransform(FQuat2D(bRotatedSegments ? sAngle : .0f)))
					.RenderTransformPivot(FVector2D(.5f, .5f))
					[
						Children[SlotIdx].GetWidget()
					]
				];

			Children[SlotIdx].Angle = FRotator::NormalizeAxis(FMath::RadiansToDegrees(sCosSinSource));
		}

		const float localSegmentDelta = 360.0f / float(Children.Num());
		RotDeltaPerSegment = localSegmentDelta - localSegmentDelta / 2;

		const float OverSize = (WheelRadius * 2) + sSize.X * 2.5f;
		Widget_WheelContainer->SetWidthOverride(OverSize);
		Widget_WheelContainer->SetHeightOverride(OverSize);

		Widget_CenterContainer->SetWidthOverride(WheelRadius);
		Widget_CenterContainer->SetHeightOverride(WheelRadius);

		InitSelector(1.0f / float(Children.Num()));
	}
	else
	{
		Widget_Container->AddSlot()
			[
				SNew(STextBlock).Text(FText::FromString("Please fill 2 or more slots!"))
			];
	}
}

void SRadialMenu::SetRadius(const float InRadius)
{
	if (FMath::IsNearlyEqual(InRadius, WheelRadius) == false)
	{
		WheelRadius = FMath::Clamp(InRadius, 50.0f, 300.0f);
		RefreshWheel();
	}
}

void SRadialMenu::SetSegmentsScale(const float InScale)
{
	if (FMath::IsNearlyEqual(InScale, SegmentScale) == false)
	{
		SegmentScale = FMath::Clamp(InScale, .5f, 2.0f);
		RefreshWheel();
	}
}

void SRadialMenu::SetRotatedSegments(const bool InRotated)
{
	bRotatedSegments = InRotated;
	RefreshWheel();
}

void SRadialMenu::AddSlot(const FSlot::FArguments& InSlot)
{
	auto MySlot = new FSlot(InSlot);
	Children.Add(MySlot);
	RefreshWheel();
}

int32 SRadialMenu::RemoveSlot(const TSharedRef<SWidget>& SlotWidget)
{
	for (SIZE_T SlotIdx = 0; SlotIdx < Children.Num(); ++SlotIdx)
	{
		if (SlotWidget == Children[SlotIdx].GetWidget())
		{
			Children.RemoveAt(SlotIdx);
			RefreshWheel();
			return SlotIdx;
		}
	}

	return INDEX_NONE;
}

void SRadialMenu::ClearChildren()
{
	Children.Empty();
	RefreshWheel();
}

void SRadialMenu::Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime)
{
	SUsableCompoundWidget::Tick(AllottedGeometry, InCurrentTime, InDeltaTime);

	if (MID && MID->IsValidLowLevel() && FMath::IsNearlyEqual(CurrentRot, TargetRot) == false)
	{
		CurrentRot = FMath::FInterpTo(CurrentRot, TargetRot, InDeltaTime, 10.0f);
		MID->SetScalarParameterValue(TEXT("Rotation"), CurrentRot);
	}

	SelectorAlpha = (SelectedSegment != INDEX_NONE) ? SelectorAlpha + InDeltaTime * Style->SelectedShowHideTime : SelectorAlpha - InDeltaTime * Style->SelectedShowHideTime;
	SelectorAlpha = FMath::Clamp(SelectorAlpha, 0.0f, 1.0f);
}

FReply SRadialMenu::OnMouseMove(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent)
{
	if (IsInInteractiveMode())
	{
		const FVector2D localCenter = MyGeometry.GetLocalSize() / 2;
		const FVector2D localMousePosition = MyGeometry.AbsoluteToLocal(MouseEvent.GetScreenSpacePosition());
		const float sPInum = PI * 2;
		int32 nFound = INDEX_NONE;

		const float currentAngle = FMath::RadiansToDegrees(FMath::Atan2(localMousePosition.Y - localCenter.Y, localMousePosition.X - localCenter.X));
		FRotator RotationAngle(currentAngle, 0, 0);

		const float CurrentDistance = FVector2D::Distance(localCenter, localMousePosition);
		if (!(CurrentDistance < GetRadius() / 2 || CurrentDistance > GetRadius() * 1.5f))
		{
			for (SIZE_T SlotIdx = 0; SlotIdx < Children.Num(); ++SlotIdx)
			{
				FRotator RotationSlotAngle(Children[SlotIdx].Angle, 0, 0);
				if (RotationAngle.Equals(RotationSlotAngle, RotDeltaPerSegment))
				{
#if WITH_EDITOR
					GEngine->AddOnScreenDebugMessage(1, 1.0f, FColorList::OrangeRed, *FString::Printf(TEXT("currentAngle >> %.4f, Children[%d].Angle >> %.4f, RotDeltaPerSegment >> %.4f"), currentAngle, SlotIdx, Children[SlotIdx].Angle, RotDeltaPerSegment));
#endif
					nFound = SlotIdx;
					break;
				}
			}
		}

		if (SelectedSegment != nFound)
		{
			if (nFound != INDEX_NONE && FFlagsHelper::HasAnyFlags(Children[nFound].Method.Get(), ESegmentActiveMethod::Select) && FFlagsHelper::HasAnyFlags(Children[nFound].Method.Get(), ESegmentActiveMethod::Click | ESegmentActiveMethod::Timed | ESegmentActiveMethod::Close) == false)
			{
				Children[nFound].OnActivated.ExecuteIfBound();
				OnActivateSlot.ExecuteIfBound(nFound);
			}

			SetSelected(nFound);
		}

		return FReply::Handled();
	}

	return FReply::Unhandled();
}

FReply SRadialMenu::OnMouseButtonDown(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent)
{
	if (MouseEvent.GetEffectingButton() == EKeys::LeftMouseButton && IsInInteractiveMode() && SelectedSegment != INDEX_NONE)
	{
		if (FFlagsHelper::HasAnyFlags(Children[SelectedSegment].Method.Get(), ESegmentActiveMethod::Click))
		{
			Children[SelectedSegment].OnActivated.ExecuteIfBound();
			OnActivateSlot.ExecuteIfBound(SelectedSegment);
		}

		return FReply::Handled();
	}

	return SCompoundWidget::OnMouseButtonDown(MyGeometry, MouseEvent);
}

void SRadialMenu::AddReferencedObjects(FReferenceCollector& Collector)
{
	Collector.AddReferencedObject(MID);
}

void SRadialMenu::InitSelector(const float InScale)
{
	if (MID && MID->IsValidLowLevel())
	{
		MID->SetScalarParameterValue(TEXT("Percent"), InScale);
	}
	else if (auto Mat = Cast<UMaterialInterface>(Style->Selected.GetResourceObject()))
	{
		MID = UMaterialInstanceDynamic::Create(Mat, GetTransientPackage());
		MIDBrush = Style->Selected;
		MIDBrush.SetResourceObject(MID);
		MID->SetScalarParameterValue(TEXT("Percent"), InScale);
	}
}

void SRadialMenu::SetSelected(const int32 InNumber)
{
	SelectedSegment = FMath::Clamp<int32>(InNumber, INDEX_NONE, Children.Num() - 1);

	if (SelectedSegment != INDEX_NONE)
	{
		GenerateCenterWidget(InNumber);

		const float localSegmentDelta = 1.0f / float(Children.Num());
		TargetRot = (localSegmentDelta * (SelectedSegment + 1)) - (localSegmentDelta / 2);
	}

	InitSelector(1.0f / float(Children.Num()));
}

void SRadialMenu::GenerateCenterWidget(const int32 InSlot)
{
	const int32 TargetSlot = FMath::Clamp(InSlot, 0, Children.Num() - 1);
	if (Children[TargetSlot].OnGenerateCenterWidget.IsBound())
	{
		Widget_CenterContainer->SetContent(Children[TargetSlot].OnGenerateCenterWidget.Execute());
	}
	else if (OnGenerateCenter.IsBound())
	{
		Widget_CenterContainer->SetContent(OnGenerateCenter.Execute(TargetSlot));
	}
}

FSlateColor SRadialMenu::GetSelectorColor() const
{
	return FMath::Lerp(FLinearColor::Transparent, Style->Selected.TintColor.GetSpecifiedColor(), SelectorAlpha);
}
