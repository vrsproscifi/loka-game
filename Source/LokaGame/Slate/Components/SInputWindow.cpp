// VRSPRO

#include "LokaGame.h"
#include "SInputWindow.h"
#include "SlateOptMacros.h"

#include "SLokaPopUpError.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SInputWindow::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;

	ChildSlot
	[
		SNew(SVerticalBox)
		+ SVerticalBox::Slot().AutoHeight()
		[
			SAssignNew(Widget_TextBlock, STextBlock)
			.TextStyle(&Style->Content.Text)
			.Justification(ETextJustify::Center)
			.Margin(FMargin(10, 6))
			.AutoWrapText(true)
		]
		+ SVerticalBox::Slot().AutoHeight()
		[
			SAssignNew(Widget_EditableTextBox, SEditableTextBox)
			.Style(&Style->Content.Input)
			.ErrorReporting(SNew(SLokaPopUpError))
			.OnTextChanged_Lambda([&](const FText&) { Widget_EditableTextBox->SetError(""); })
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SInputWindow::SetContent(const FText& Content) 
{
	Widget_TextBlock->SetText(Content);
}

FText SInputWindow::GetInputText() const
{
	return Widget_EditableTextBox->GetText();
}

void SInputWindow::SetInputText(const FText& InText)
{
	Widget_EditableTextBox->SetText(InText);
}

void SInputWindow::SetInputError(const FText& InText)
{
	Widget_EditableTextBox->SetError(InText);
}