// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"

/**
 * 
 */
class LOKAGAME_API SSniperScope : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SSniperScope)
	{
		_Visibility = EVisibility::Hidden;
	}
	SLATE_END_ARGS()

	/** Constructs this widget with InArgs */
	void Construct(const FArguments& InArgs);

	void SetImage(const FSlateBrush*);
	void SetColor(const FSlateColor&);

protected:

	TSharedPtr<SImage> Image;
	TSharedPtr<SBorder> Borders[4];
};
