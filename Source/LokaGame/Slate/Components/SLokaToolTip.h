// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Styles/LokaToolTipWidgetStyle.h"
/**
 * 
 */
class LOKAGAME_API SLokaToolTip : public SCompoundWidget, public IToolTip
{
public:
	SLATE_BEGIN_ARGS(SLokaToolTip)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FLokaToolTipStyle>("SLokaToolTipStyle"))
		, _IsInteractive(false)
	{ }
	SLATE_ATTRIBUTE(FText, Text)
		SLATE_STYLE_ARGUMENT(FLokaToolTipStyle, Style)
		SLATE_DEFAULT_SLOT(FArguments, Content)
		SLATE_ATTRIBUTE(FSlateFontInfo, Font)
		SLATE_ATTRIBUTE(FSlateColor, FontColor)
		SLATE_ATTRIBUTE(FMargin, TextMargin)
		SLATE_ATTRIBUTE(const FSlateBrush*, BorderImage)
		SLATE_ATTRIBUTE(const FSlateBrush*, BackgroundImage)
		SLATE_ATTRIBUTE(bool, IsInteractive)
	SLATE_END_ARGS()

	/** Constructs this widget with InArgs */
	void Construct(const FArguments& InArgs);

	// IToolTip Interface begin

	virtual TSharedRef<class SWidget> AsWidget() override
	{
		return AsShared();
	}

	virtual TSharedRef<SWidget> GetContentWidget() override
	{
		return ChildSlot.GetWidget();
	}

	virtual void SetContentWidget(const TSharedRef<SWidget>& InContentWidget) override;
	virtual bool IsEmpty() const override;
	virtual bool IsInteractive() const override;
	virtual void OnOpening() override;
	virtual void OnClosed() override;

	// IToolTip Interface end

protected:

	const FLokaToolTipStyle* Style;

	TAttribute<FText> Attr_ContentText;
	TWeakPtr<SWidget> Weak_ContentWidget;

	TSharedPtr<SWidget> ToolTipContent;
	TSharedPtr<class SAnimatedBackground> FxWidget;

	TAttribute<FSlateFontInfo> Attr_Font;
	TAttribute<FSlateColor> Attr_FontColor;

	TAttribute<FMargin> Attr_TextMargin;

	TAttribute<const FSlateBrush*> Attr_BackgroundImage;
	TAttribute<const FSlateBrush*> Attr_BorderImage;

	TAttribute<bool> Attr_IsInteractive;
};
