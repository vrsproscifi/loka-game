// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "SSliderNumber.h"
#include "Styles/WindowBuyBuildingWidgetStyle.h"

#include "Types/TypeCash.h"

class UPlayerInventoryItem;
class UItemBaseEntity;


class LOKAGAME_API SWindowBuyBuilding : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SWindowBuyBuilding)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FWindowBuyBuildingStyle>("SWindowBuyBuildingStyle"))
	{}
	SLATE_STYLE_ARGUMENT(FWindowBuyBuildingStyle, Style)
	SLATE_ATTRIBUTE(TArray<FTypeCash>, Cash)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	void SetCurrentItem(const UPlayerInventoryItem* InItem);
	const UPlayerInventoryItem* GetCurrentItem() const;
	const UItemBaseEntity* GetCurrentItemEntity() const;

	void ToggleSellMode(const bool InToggle);

	int32 GetCurrentItemAmount() const;
	void  SetCurrentItemAmount(const int32&) const;

	FText GetCurrentDescription() const;

protected:

	const FWindowBuyBuildingStyle* Style;
	FText CurrentFormatedName;
	bool bIsSellMode;

	uint8 GetSliderMaximum() const;

	TAttribute<TArray<FTypeCash>> Attr_Cash;

	TSharedPtr<SBorder> Widget_ItemContainer;
	TSharedPtr<SSliderNumber<uint8>> Widget_SliderAmount;

	TWeakObjectPtr<UPlayerInventoryItem> SourcePtr;
};
