// VRSPRO

#include "LokaGame.h"
#include "SAnimatedBackground.h"
#include "SBackgroundBlur.h"
#include "SlateOptMacros.h"

#include "ToggableWidgetHelper.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SAnimatedBackground::Construct(const FArguments& InArgs)
{
	ShowAnimation = InArgs._ShowAnimation;
	HideAnimation = InArgs._HideAnimation;
	
	IsRelative = InArgs._IsRelative;
	WithColor = InArgs._WithColor;

	BlurStyle = InArgs._BlurStyle;
	WithBlur = InArgs._IsEnabledBlur;

	VisibilityOverride = Visibility;

	SBorder::Construct(SBorder::FArguments()
		.BorderImage(new FSlateNoResource())
		.Padding(0)
		.HAlign(HAlign_Fill)
		.VAlign(VAlign_Fill)
		.RenderTransformPivot(FVector2D(0.5f, 0.5f))
		.RenderTransform(this, &SAnimatedBackground::WidgetTrans)
		.ColorAndOpacity(this, &SAnimatedBackground::WidgetLineColor)
		[
			SAssignNew(Widget_BackgroundBlur, SBackgroundBlur)
			.Padding(InArgs._Padding)
			.HAlign(InArgs._HAlign)
			.VAlign(InArgs._VAlign)
			.bApplyAlphaToBlur(BlurStyle->bApplyAlphaToBlur)
			.BlurStrength(this, &SAnimatedBackground::GetBlurStrength)
			.BlurRadius(this, &SAnimatedBackground::GetBlurRadius)
			.LowQualityFallbackBrush(&BlurStyle->LowQualityFallbackBrush)
			[
				InArgs._Content.Widget
			]
		]
	);
	
	// Need this for set correctly
	this->SetRenderTransform(TAttribute<TOptional<FSlateRenderTransform>>(this, &SAnimatedBackground::WidgetTrans));
	this->SetRenderTransformPivot(FVector2D(0.5f, 0.5f));
	this->SetVisibility(TAttribute<EVisibility>(this, &SAnimatedBackground::GetVisibilityByAnim));

	SetDuration(InArgs._Duration, InArgs._Ease);

	if (!InArgs._InitAsHide)
	{
		AnimSequence.JumpToEnd();
	}
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SAnimatedBackground::SetDuration(const float InDuration, const ECurveEaseFunction Ease)
{
	const float TotalDuration = InDuration;
	const float ColorOffset = (TotalDuration / 100.0f) * 20.0f;
	const float ColorDuration = TotalDuration - ColorOffset;

	AnimSequence = FCurveSequence();

	ColorHandle = AnimSequence.AddCurve(ColorOffset, ColorDuration, Ease);
	TransHandle = AnimSequence.AddCurve(0.0f, TotalDuration, Ease);
}

EVisibility SAnimatedBackground::GetVisibilityByAnim() const
{
	return (TransHandle.GetLerp() > .0f) ? VisibilityOverride.Get(EVisibility::SelfHitTestInvisible) : EVisibility::Collapsed;
}

void SAnimatedBackground::ToggleWidget(const bool _Toggle)
{
	CurrentAnim = _Toggle ? ShowAnimation.Get() : HideAnimation.Get();
	FToggableWidgetHelper::ToggleWidget(AsShared(), AnimSequence, _Toggle);
}

void SAnimatedBackground::SetShowAnimation(const TAttribute<EAnimBackAnimation::Type> InAnimation)
{
	ShowAnimation = InAnimation;
}

void SAnimatedBackground::SetHideAnimation(const TAttribute<EAnimBackAnimation::Type> InAnimation)
{
	HideAnimation = InAnimation;
}

bool SAnimatedBackground::IsAnimAtStart() const
{
	return AnimSequence.IsAtStart();
}

bool SAnimatedBackground::IsAnimAtEnd() const
{
	return AnimSequence.IsAtEnd();
}

FLinearColor SAnimatedBackground::WidgetLineColor() const
{
	return FLinearColor(1.0f, 1.0f, 1.0f, WithColor ? ColorHandle.GetLerp() : 1.0f);
}

TOptional<FSlateRenderTransform> SAnimatedBackground::WidgetTrans() const
{
	switch (CurrentAnim)
	{
		case EAnimBackAnimation::LeftFade: return FSlateRenderTransform(FVector2D(FMath::Lerp(IsRelative ? -GetDesiredSize().X : -1920.0f, 0.0f, TransHandle.GetLerp()), 0.0f));
		case EAnimBackAnimation::RightFade: return FSlateRenderTransform(FVector2D(FMath::Lerp(IsRelative ? GetDesiredSize().X : 1920.0f, 0.0f, TransHandle.GetLerp()), 0.0f));
		case EAnimBackAnimation::UpFade: return FSlateRenderTransform(FVector2D(0.0f, FMath::Lerp(IsRelative ? -GetDesiredSize().Y : -1080.0f, 0.0f, TransHandle.GetLerp())));
		case EAnimBackAnimation::DownFade: return FSlateRenderTransform(FVector2D(0.0f, FMath::Lerp(IsRelative ? GetDesiredSize().Y : 1080.0f, 0.0f, TransHandle.GetLerp())));
		case EAnimBackAnimation::ZoomIn: return FSlateRenderTransform(FScale2D(TransHandle.GetLerp()));
		case EAnimBackAnimation::ZoomOut: return FSlateRenderTransform(FScale2D(FMath::Lerp(2.0f, 1.0f, TransHandle.GetLerp())));
		default: break;
	}

	return FSlateRenderTransform();
}

float SAnimatedBackground::GetBlurStrength() const
{
	return WithBlur ? FMath::Lerp(.0f, BlurStyle->BlurStrength, ColorHandle.GetLerp()) : .0f;
}

TOptional<int32> SAnimatedBackground::GetBlurRadius() const
{
	return WithBlur ? FMath::Lerp(0, BlurStyle->BlurRadius, ColorHandle.GetLerp()) : 0;
}

// SBackgroundBlur Begin
void SAnimatedBackground::SetApplyAlphaToBlur(bool bInApplyAlphaToBlur)
{
	Widget_BackgroundBlur->SetApplyAlphaToBlur(bInApplyAlphaToBlur);
}

void SAnimatedBackground::SetBlurRadius(const TAttribute<TOptional<int32>>& InBlurRadius)
{
	Widget_BackgroundBlur->SetBlurRadius(InBlurRadius);
}

void SAnimatedBackground::SetBlurStrength(const TAttribute<float>& InStrength)
{
	Widget_BackgroundBlur->SetBlurStrength(InStrength);
}

void SAnimatedBackground::SetLowQualityBackgroundBrush(const FSlateBrush* InBrush)
{
	Widget_BackgroundBlur->SetLowQualityBackgroundBrush(InBrush);
}
// SBackgroundBlur End