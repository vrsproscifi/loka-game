//// VRSPRO
//
#include "LokaGame.h"
#include "SWindowDuelRules.h"
#include "SlateOptMacros.h"

#include "SResourceTextBox.h"
#include "SLokaToolTip.h"

#include "GameModeTypeId.h"
#include "DuelStructures.h"
#include "SResourceTextBoxType.h"
#include "SSliderNumber.h"
#include "SSpinNumber.h"
#include "GameSingleton.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SWindowDuelRules::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	
	ResourceStyle = &FLokaStyle::Get().GetWidgetStyle<FResourceTextBoxStyle>("SResourceTextBoxStyle");

	TArray<TSharedPtr<EGameCurrency::Type>> Options_Money;
	Options_Money.Add(MakeShareable(new EGameCurrency::Type(EGameCurrency::Donate)));
	Options_Money.Add(MakeShareable(new EGameCurrency::Type(EGameCurrency::Money)));
	Options_Money.Add(MakeShareable(new EGameCurrency::Type(EGameCurrency::Coin)));

	TArray<TSharedPtr<EWeaponType>> Options_Weapons;
	Options_Weapons.Add(MakeShareable(new EWeaponType(EWeaponType::All)));
	Options_Weapons.Add(MakeShareable(new EWeaponType(EWeaponType::AssaultRifle)));
	Options_Weapons.Add(MakeShareable(new EWeaponType(EWeaponType::SniperRifle)));
	Options_Weapons.Add(MakeShareable(new EWeaponType(EWeaponType::Shootgun)));
	Options_Weapons.Add(MakeShareable(new EWeaponType(EWeaponType::LightMachineGun)));
	Options_Weapons.Add(MakeShareable(new EWeaponType(EWeaponType::Pistol)));
	Options_Weapons.Add(MakeShareable(new EWeaponType(EWeaponType::RocketLauncher)));

	TArray<TSharedPtr<EGameMap>> Options_Maps;
	Options_Maps.Add(MakeShareable(new EGameMap(EGameMap::AllFight)));
	Options_Maps.Add(MakeShareable(new EGameMap(EGameMap::DesertTown)));
	Options_Maps.Add(MakeShareable(new EGameMap(EGameMap::Hangar)));
	Options_Maps.Add(MakeShareable(new EGameMap(EGameMap::MilitaryBase)));
	Options_Maps.Add(MakeShareable(new EGameMap(EGameMap::PvP)));
	//Options_Maps.Add(MakeShareable(new EGameMap(EGameMap::Outpost)));

	ChildSlot
	[
		SNew(SVerticalBox)
		+ SVerticalBox::Slot()
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot().FillWidth(.6f)
			[
				SNew(STextBlock).Text(NSLOCTEXT("SWindowDuelRules", "SWindowDuelRules.SelectBet", "Bet")).TextStyle(&Style->Row)
			]
			+ SHorizontalBox::Slot().FillWidth(1)
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot().AutoWidth()
				[
					SNew(SBox)
					.WidthOverride(32)
					.HeightOverride(32)
					.HAlign(HAlign_Center)
					.VAlign(VAlign_Center)
					[
						SNew(SComboBox<TSharedPtr<EGameCurrency::Type>>)
						.HasDownArrow(false)
						.ComboBoxStyle(&Style->ComboBox)
						.OptionsSource(new TArray<TSharedPtr<EGameCurrency::Type>>(Options_Money))
						.InitiallySelectedItem(Options_Money[0])
						.OnGenerateWidget_Lambda([&](const TSharedPtr<EGameCurrency::Type> InType) {
							return SNew(SResourceTextBox).Type_Lambda([&, t = InType]()
							{
								return static_cast<EResourceTextBoxType::Type>(*t);
							}).Visibility(EVisibility::HitTestInvisible);
						})
						.OnSelectionChanged_Lambda([&](const TSharedPtr<EGameCurrency::Type> InType, ESelectInfo::Type) {
							CurrentCurrencyType = *InType;
						})
						.Content()
						[
							SNew(SResourceTextBox).Type_Lambda([&]()
							{
								return static_cast<EResourceTextBoxType::Type>(CurrentCurrencyType);
							}).Visibility(EVisibility::HitTestInvisible)
						]
					]
				]
				+ SHorizontalBox::Slot()
				[
					SAssignNew(Widget_SpinMoney, SSpinNumber<int32>)
					.MinValue_Lambda([&]() { return CurrentCurrencyType == EGameCurrency::Coin ? 10 : 1; })
					.MaxValue_Lambda([&]()
					{
						return UGameSingleton::Get()->GetSavedPlayerEntityInfo().TakeCash(CurrentCurrencyType);
					})
				]
			]
		]
		+ SVerticalBox::Slot()
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot().FillWidth(.6f)
			[
				SNew(STextBlock).Text(NSLOCTEXT("SWindowDuelRules", "SWindowDuelRules.SelectWeapon", "Weapon")).TextStyle(&Style->Row)
			]
			+ SHorizontalBox::Slot().FillWidth(1)
			[
				SNew(SComboBox<TSharedPtr<EWeaponType>>)
				.HasDownArrow(false)
				.ComboBoxStyle(&Style->ComboBox)
				.OptionsSource(new TArray<TSharedPtr<EWeaponType>>(Options_Weapons))
				.InitiallySelectedItem(Options_Weapons[0])
				.OnGenerateWidget_Lambda([&](const TSharedPtr<EWeaponType> InType) {
					return SNew(STextBlock).TextStyle(&Style->RowPopUp).Text(InType->GetDisplayName());
				})
				.OnSelectionChanged_Lambda([&](const TSharedPtr<EWeaponType> InType, ESelectInfo::Type) {
					CurrentWeaponType = *InType;
				})
				.Content()
				[
					SNew(STextBlock).TextStyle(&Style->RowPopUp).Text_Lambda([&]() { return CurrentWeaponType.GetDisplayName(); })
				]
			]
		]
		+ SVerticalBox::Slot()
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot().FillWidth(.6f)
			[
				SNew(STextBlock).Text(NSLOCTEXT("SWindowDuelRules", "SWindowDuelRules.SelectLifes", "Number of life")).TextStyle(&Style->Row)
			]
			+ SHorizontalBox::Slot().FillWidth(1)
			[
				SAssignNew(Widget_SpinLifes, SSpinNumber<int32>)
				.Delta(1)
				.MaxValue(15)
				.MinValue(1)
				.InitValue(5)
			]
		]
		+ SVerticalBox::Slot()
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot().FillWidth(.6f)
			[
				SNew(STextBlock).Text(NSLOCTEXT("SWindowDuelRules", "SWindowDuelRules.SelectMap", "Map")).TextStyle(&Style->Row)
			]
			+ SHorizontalBox::Slot().FillWidth(1)
			[
				SNew(SComboBox<TSharedPtr<EGameMap>>)
				.HasDownArrow(false)
				.ComboBoxStyle(&Style->ComboBox)
				.OptionsSource(new TArray<TSharedPtr<EGameMap>>(Options_Maps))
				.InitiallySelectedItem(nullptr)
				.OnGenerateWidget_Lambda([&](const TSharedPtr<EGameMap> InMap) {
					return SNew(STextBlock).Text(InMap->GetDisplayName()).TextStyle(&Style->RowPopUp);
				})
				.OnSelectionChanged_Lambda([&](const TSharedPtr<EGameMap> InMap, ESelectInfo::Type) {
					CurrentSelectedMap = *InMap;
				})
				.Content()
				[
					SNew(STextBlock).TextStyle(&Style->RowPopUp).Text_Lambda([&]() { return CurrentSelectedMap.GetDisplayName(); })
				]
			]
		]
	];

	CurrentCurrencyType = EGameCurrency::Donate;
	CurrentWeaponType = EDuelWeaponTypes::All;
	CurrentSelectedMap = EGameMapTypeId::End;

}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

FSessionMatchOptions SWindowDuelRules::GetOptions() const
{
	FSessionMatchOptions Options;
	Options.AdditionalFlag = Widget_SpinLifes->GetValue();
	Options.Bet = FTypeCost(Widget_SpinMoney->GetValue(), CurrentCurrencyType);
	Options.GameMap = CurrentSelectedMap.ToFlag();
	Options.GameMode = EGameMode::DuelMatch.ToFlag();
	Options.WeaponType = CurrentWeaponType.ToFlag();

	return Options;
}

FText SWindowDuelRules::GetCurrencyToolTip(const int32 v) const
{
	switch (v)
	{
		case EResourceTextBoxType::Money: return NSLOCTEXT("SResourceTextBox", "SResourceTextBox.Money", "Crypto-dollars, in-game currency.");
		case EResourceTextBoxType::Donate: return NSLOCTEXT("SResourceTextBox", "SResourceTextBox.Donate", "Energetic-crystal, premium currency.");
		case EResourceTextBoxType::Coin: return NSLOCTEXT("SResourceTextBox", "SResourceTextBox.Coin", "Crypto-coin, real currency.");
		default:
			break;
	}

	return FText::GetEmpty();
}