// VRSPRO

#pragma once

UENUM(Blueprintable)
namespace EResourceTextBoxType
{
	enum Type
	{
		Money,
		Donate,
		Coin,
		Time,
		RealMoney,
		Level,
		Premium,
		BoostMoney,
		BoostExperience,
		BoostReputation,
		Custom
	};
}
