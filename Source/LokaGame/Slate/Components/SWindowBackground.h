// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Styles/WindowBackgroundWidgetStyle.h"
#include "Styles/BackgroundBlurWidgetStyle.h"

class SBackgroundBlur;

class LOKAGAME_API SWindowBackground : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SWindowBackground)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FWindowBackgroundStyle>("SWindowBackgroundStyle"))
		, _BlurStyle(&FLokaStyle::Get().GetWidgetStyle<FBackgroundBlurStyle>("Default_BackgroundBlur"))
		, _Header(FText::GetEmpty())
		, _HeaderContent()
		, _HeaderPadding(FMargin(10, 6))
		, _IsEnabledHeaderBackground(true)
		, _Opacity(.0f)
		, _IsEnabledBlur(false)
		, _IsExpandable(false)
	{}
	SLATE_STYLE_ARGUMENT(FWindowBackgroundStyle, Style)
	SLATE_STYLE_ARGUMENT(FBackgroundBlurStyle, BlurStyle)
	SLATE_DEFAULT_SLOT(FArguments, Content)
	SLATE_NAMED_SLOT(FArguments, HeaderContent)
	SLATE_ARGUMENT(FText, Header)
	SLATE_ARGUMENT(bool, IsEnabledHeaderBackground)
	SLATE_ARGUMENT(bool, IsExpandable)
	SLATE_ARGUMENT(float, Opacity)
	SLATE_ATTRIBUTE(FMargin, HeaderPadding)
	SLATE_ATTRIBUTE(FMargin, Padding)
	// SBackgroundBlur Begin
	SLATE_ARGUMENT(bool, IsEnabledBlur)
	// SBackgroundBlur End
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	// SBackgroundBlur Begin
	void SetApplyAlphaToBlur(bool bInApplyAlphaToBlur);
	void SetBlurRadius(const TAttribute<TOptional<int32>>& InBlurRadius);
	void SetBlurStrength(const TAttribute<float>& InStrength);
	void SetLowQualityBackgroundBrush(const FSlateBrush* InBrush);
	// SBackgroundBlur End

	virtual void OnMouseEnter(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override;
	virtual void OnMouseLeave(const FPointerEvent& MouseEvent) override;

	void SetExpanded(const bool InExpanded, const bool WithAnimation = false);
	FORCEINLINE bool IsExpanded() const { return bIsExpanded; }

	FORCEINLINE void SetIsExpandable(const bool InIsExpandable) { bIsExpandable = InIsExpandable; }
	FORCEINLINE bool IsExpandable() const { return bIsExpandable; }

protected:

	FSlateBrush* BackgroundBrush;
	const FWindowBackgroundStyle* Style;
	bool bIsExpandable, bIsExpanded;

	TSharedPtr<SBackgroundBlur> Widget_BackgroundBlur;

	FCurveSequence ExpandableAnimation;

	FVector2D GetSectionScale() const;
	EVisibility OnGetContentVisibility() const;
};
