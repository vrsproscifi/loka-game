// VRSPRO

#include "LokaGame.h"
#include "STreePin.h"
#include "SlateOptMacros.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void STreePin::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	OtherPin = InArgs._OtherPin;

	ChildSlot
	[
		SAssignNew(Image, SImage).Image(OtherPin.IsValid() ? &Style->Normal : &Style->Disabled)
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void STreePin::SetOtherPin(TSharedRef<STreePin> _OtherPin)
{
	OtherPin = _OtherPin;
	Image->SetImage(&Style->Normal);
}

void STreePin::ResetOtherPin()
{
	OtherPin.Reset();
	Image->SetImage(&Style->Disabled);
}