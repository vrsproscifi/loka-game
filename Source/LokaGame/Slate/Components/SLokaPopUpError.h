// VRSPRO

#pragma once


#include "SComboButton.h"
#include "Widgets/SCompoundWidget.h"
#include "Notifications/SErrorText.h"

#include "Slate/Styles/LokaPopUpErrorWidgetStyle.h"
/**
 * 
 */
class LOKAGAME_API SLokaPopUpError : public SComboButton, public IErrorReportingWidget
{
public:
	SLATE_BEGIN_ARGS(SLokaPopUpError)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FLokaPopUpErrorStyle>("SLokaPopUpErrorStyle"))
	{}
	SLATE_STYLE_ARGUMENT(FLokaPopUpErrorStyle, Style)
	SLATE_ATTRIBUTE(FSlateFontInfo, Font)
	SLATE_ATTRIBUTE(FSlateColor, FontColorAndOpacity)
	SLATE_ATTRIBUTE(const FSlateBrush*, BackgroundImage)
	SLATE_ATTRIBUTE(FSlateColor, BackgroundColorAndOpacity)
	SLATE_END_ARGS()

	/** Constructs this widget with InArgs */
	void Construct(const FArguments& InArgs);

	// IErrorReportingWidget interface

	virtual void SetError(const FText& InErrorText) override;
	virtual void SetError(const FString& InErrorText) override;

	virtual bool HasError() const override;

	virtual TSharedRef<SWidget> AsWidget() override;

protected:

	TSharedPtr<SBorder> HasErrorSymbol;
	TSharedPtr<SBorder> ErrorTextContainer;
	TSharedPtr<STextBlock> ErrorText;

	const FLokaPopUpErrorStyle *Style;
};
