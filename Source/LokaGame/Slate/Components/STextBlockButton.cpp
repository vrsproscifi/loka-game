// VRSPRO

#include "LokaGame.h"
#include "STextBlockButton.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void STextBlockButton::Construct(const FArguments& InArgs)
{
	STextBlock::FArguments OriginArgs = InArgs._Origin;

	if (!InArgs._Text.IsEmpty()) 
	{
		OriginArgs.Text(InArgs._Text);
	}

	Style = InArgs._Style;
	OriginArgs.TextStyle(IsEnabled() ? &Style->Normal : &Style->Disabled);
	OriginArgs.Margin(Style->NormalPadding);
	OnClickedEvent = InArgs._OnClicked;
	ClickMethod = InArgs._ClickMethod;

	bIsPressed = false;

	STextBlock::Construct(OriginArgs);
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void STextBlockButton::OnMouseEnter(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent)
{
	STextBlock::OnMouseEnter(MyGeometry, MouseEvent);
	STextBlock::SetTextStyle(&Style->Hovered);
	STextBlock::SetMargin(Style->HoveredPadding);

	FSlateApplication::Get().PlaySound(Style->HoveredSound);
}

void STextBlockButton::OnMouseLeave(const FPointerEvent& MouseEvent)
{
	STextBlock::OnMouseLeave(MouseEvent);	

	if (ClickMethod == EButtonClickMethod::MouseDown || !bIsPressed)
	{
		Release();
	}
}

FReply STextBlockButton::OnMouseButtonDown(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent)
{	
	FReply Reply = FReply::Unhandled();
	if (IsEnabled() && (MouseEvent.GetEffectingButton() == EKeys::LeftMouseButton || MouseEvent.IsTouchEvent()))
	{
		Press();

		if (ClickMethod == EButtonClickMethod::MouseDown)
		{
			Reply = OnClickedEvent.IsBound() ? OnClickedEvent.Execute() : FReply::Handled();
			ensure(Reply.IsEventHandled() == true);
		}
		else
		{
			Reply = FReply::Handled().CaptureMouse(AsShared());
		}		
	}

	return Reply;
}

FReply STextBlockButton::OnMouseButtonUp(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent)
{
	FReply Reply = FReply::Unhandled();
	if (IsEnabled() && (MouseEvent.GetEffectingButton() == EKeys::LeftMouseButton || MouseEvent.IsTouchEvent()))
	{
		Release();

		if (ClickMethod == EButtonClickMethod::MouseDown)
		{

		}
		else
		{
			const bool bIsUnderMouse = MyGeometry.IsUnderLocation(MouseEvent.GetScreenSpacePosition());
			if (bIsUnderMouse)
			{
				// If we were asked to allow the button to be clicked on mouse up, regardless of whether the user
				// pressed the button down first, then we'll allow the click to proceed without an active capture
				const bool bTriggerForMouseEvent = (ClickMethod == EButtonClickMethod::MouseUp || HasMouseCapture());

				if (bTriggerForMouseEvent && OnClickedEvent.IsBound() == true)
				{
					Reply = OnClickedEvent.Execute();
				}
			}
		}

		if (Reply.IsEventHandled() == false)
		{
			Reply = FReply::Handled();
		}

		if (Reply.GetMouseCaptor().IsValid() == false)
		{
			Reply.ReleaseMouseCapture();
		}
	}

	return Reply;
}

void STextBlockButton::SetStyle(const FTextBlockButtonStyle *_Style)
{
	Style = _Style;

	if (this->IsEnabled())
	{
		if (this->bIsPressed)
		{
			STextBlock::SetTextStyle(&Style->Pressed);
			STextBlock::SetMargin(Style->PressedPadding);
		}
		else if (this->IsHovered())
		{
			STextBlock::SetTextStyle(&Style->Hovered);
			STextBlock::SetMargin(Style->HoveredPadding);
		}
		else
		{
			STextBlock::SetTextStyle(&Style->Normal);
			STextBlock::SetMargin(Style->NormalPadding);
		}
	}
	else
	{
		STextBlock::SetTextStyle(&Style->Disabled);
		STextBlock::SetMargin(Style->NormalPadding);
	}
}

void STextBlockButton::Press()
{
	if (this->IsEnabled())
	{
		bIsPressed = true;
		STextBlock::SetTextStyle(&Style->Pressed);
		STextBlock::SetMargin(Style->PressedPadding);

		FSlateApplication::Get().PlaySound(Style->PressedSound);
	}
}

void STextBlockButton::Release()
{
	if (this->IsEnabled())
	{
		bIsPressed = false;

		if (this->IsHovered())
		{
			STextBlock::SetTextStyle(&Style->Hovered);
			STextBlock::SetMargin(Style->HoveredPadding);
		}
		else
		{
			STextBlock::SetTextStyle(&Style->Normal);
			STextBlock::SetMargin(Style->NormalPadding);
		}
	}
}