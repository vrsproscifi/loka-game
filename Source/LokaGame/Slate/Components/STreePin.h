// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Slate/Styles/TreePinWidgetStyle.h"
/**
 * 
 */
class LOKAGAME_API STreePin : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(STreePin)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FTreePinStyle>("STreePinStyle"))
		, _OtherPin(nullptr)
	{
		_Visibility = EVisibility::HitTestInvisible;
	}
	SLATE_STYLE_ARGUMENT(FTreePinStyle, Style)
	SLATE_ARGUMENT(TSharedPtr<STreePin>, OtherPin)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	TSharedPtr<STreePin> GetOtherPin()
	{
		return OtherPin;
	}

	void SetOtherPin(TSharedRef<STreePin> _OtherPin);
	void ResetOtherPin();

protected:

	TSharedPtr<SImage> Image;
	TSharedPtr<STreePin> OtherPin;

	const FTreePinStyle *Style;
};
