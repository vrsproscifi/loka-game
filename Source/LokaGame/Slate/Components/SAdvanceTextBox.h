// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Slate/Styles/AdvanceTextBoxWidgetStyle.h"

class LOKAGAME_API SAdvanceTextBox : public SEditableTextBox
{
public:
	SLATE_BEGIN_ARGS(SAdvanceTextBox)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FAdvanceTextBoxStyle>("SAdvanceTextBoxStyle"))
		, _HintText()
		, _InitialText()
		, _OnTextChanged()
		, _OnTextCommitted()
		, _SelectAllTextWhenFocused(true)
		, _ErrorReporting()
	{ }

	SLATE_EVENT(FOnTextChanged, OnAcceptText)

	/** Style used to draw this search box */
	SLATE_STYLE_ARGUMENT(FAdvanceTextBoxStyle, Style)

	/** The text displayed in the SearchBox when no text has been entered */
	SLATE_ATTRIBUTE(FText, HintText)

	/** The text displayed in the SearchBox when it's created */
	SLATE_ATTRIBUTE(FText, InitialText)

	/** Invoked whenever the text changes */
	SLATE_EVENT(FOnTextChanged, OnTextChanged)

	/** Invoked whenever the text is committed (e.g. user presses enter) */
	SLATE_EVENT(FOnTextCommitted, OnTextCommitted)

	/** Whether to select all text when the user clicks to give focus on the widget */
	SLATE_ATTRIBUTE(bool, SelectAllTextWhenFocused)

	/** Minimum width that a text block should be */
	SLATE_ATTRIBUTE(float, MinDesiredWidth)

	/** Provide a alternative mechanism for error reporting. */
	SLATE_ARGUMENT(TSharedPtr<class IErrorReportingWidget>, ErrorReporting)

	SLATE_END_ARGS()

	/** Constructs this widget with InArgs */
	void Construct(const FArguments& InArgs);

	/** If allowed editing show controls */
	void SetAllowEdit(const bool IsAllowEdit);

protected:

	FOnTextChanged OnAcceptText;

	void ToggleEditMode(const bool isEdit = true);

	FText SourceText;

	TSharedPtr<class STextBlockButton> ButtonStartEdit;
	TSharedPtr<class STextBlockButton> ButtonAcceptEdit;
	TSharedPtr<class STextBlockButton> ButtonCancelEdit;

	FReply StartEdit();
	FReply AcceptEdit();
	FReply CancelEdit();

	const FAdvanceTextBoxStyle *Style;
};
