// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Styles/SliderNumberWidgetStyle.h"
#include "Styles/SliderPercentWidgetStyle.h" // Temp

/**
 * 
 */

template<typename NumberType>
class LOKAGAME_API SSliderNumber : public SCompoundWidget
{
public:
	SSliderNumber<NumberType>()
	{
		static_assert(
			TIsSame<NumberType, int8>().Value ||
			TIsSame<NumberType, uint8>().Value ||
			TIsSame<NumberType, int16>().Value ||
			TIsSame<NumberType, uint16>().Value ||
			TIsSame<NumberType, int32>().Value ||
			TIsSame<NumberType, uint32>().Value ||
			TIsSame<NumberType, int64>().Value ||
			TIsSame<NumberType, uint64>().Value ||
			TIsSame<NumberType, float>().Value,
			"This widget only support number types with float not double!"
			);
	}	

	typedef TBaseDelegate<void, NumberType> FOnSliderValueChange;

	SLATE_BEGIN_ARGS(SSliderNumber<NumberType>)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FSliderPercentStyle>("SSliderPercentStyle"))
		, _TextLeft(FText::FromString("<<"))
		, _TextRight(FText::FromString(">>"))
		, _ButtonsDelta(1)
		, _SliderMinimum(0)
		, _SliderMaximum(10)
		, _InitValue(1)
	{}
	SLATE_STYLE_ARGUMENT(FSliderPercentStyle, Style)
	SLATE_ARGUMENT(FText, TextLeft)
	SLATE_ARGUMENT(FText, TextRight)
	SLATE_ARGUMENT(NumberType, InitValue)
	SLATE_ATTRIBUTE(NumberType, ButtonsDelta)
	SLATE_ATTRIBUTE(NumberType, SliderMinimum)
	SLATE_ATTRIBUTE(NumberType, SliderMaximum)
	SLATE_ATTRIBUTE(FText, CustomCenterText)
	SLATE_EVENT(FOnSliderValueChange, OnValueChange)
	SLATE_END_ARGS()

	BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
	void Construct(const FArguments& InArgs)
	{
		Style = InArgs._Style;
		OnValueChange = InArgs._OnValueChange;
		ButtonsDelta = InArgs._ButtonsDelta;
		SliderMinimum = InArgs._SliderMinimum;
		SliderMaximum = InArgs._SliderMaximum;
		CustomCenterText = InArgs._CustomCenterText;

		ChildSlot
		[
			SNew(SBox).MaxDesiredHeight(30)
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot()
				.AutoWidth()
				[
					SNew(SButton)
					.ButtonStyle(&Style->ButtonLeft)
					.OnClicked(this, &SSliderNumber<NumberType>::OnClickedButton, false)
					.VAlign(EVerticalAlignment::VAlign_Center)
					[
						SNew(STextBlock)
						.TextStyle(&Style->TextButtons)
						.Margin(Style->ButtonsTextPadding)
						.Justification(ETextJustify::Center)
						.Text(InArgs._TextLeft)
					]
				]
				+ SHorizontalBox::Slot()
				.FillWidth(1.0f)
				[
					SNew(SBorder)
					.Padding(0)
					.BorderImage(&Style->Background)
					.VAlign(EVerticalAlignment::VAlign_Center)
					[
						SNew(SOverlay)
						+ SOverlay::Slot().Padding(Style->ProgressBarPadding)
						[
							SAssignNew(Widget_ProgressBar, SProgressBar)
							.Style(&Style->ProgressBar)
							.Percent(this, &SSliderNumber<NumberType>::GetCurrentValue)
							.BarFillType(EProgressBarFillType::LeftToRight)
						]
						+ SOverlay::Slot().Padding(Style->SliderPadding)
						[
							SAssignNew(Widget_Slider, SSlider)
							.Style(&Style->Slider)
							.OnValueChanged(this, &SSliderNumber<NumberType>::OnValueChangedEvent)
							.Orientation(Orient_Horizontal)
							.SliderHandleColor(Style->Slider.NormalThumbImage.TintColor)
							.IndentHandle(false)
						]
						+ SOverlay::Slot().VAlign(VAlign_Center)
						[
							SAssignNew(Widget_TextBlock, STextBlock)
							.Visibility(EVisibility::HitTestInvisible)
							.TextStyle(&Style->TextCenter)
							.Margin(Style->CenterTextPadding)
							.Justification(ETextJustify::Center)
							.Text(this, &SSliderNumber<NumberType>::GetCurrentValueText)
						]
					]
				]
				+ SHorizontalBox::Slot()
				.AutoWidth()
				[
					SNew(SButton)
					.ButtonStyle(&Style->ButtonRight)
					.OnClicked(this, &SSliderNumber<NumberType>::OnClickedButton, true)
					.VAlign(EVerticalAlignment::VAlign_Center)
					[
						SNew(STextBlock)
						.TextStyle(&Style->TextButtons)
						.Margin(FMargin(Style->ButtonsTextPadding.Right, Style->ButtonsTextPadding.Top, Style->ButtonsTextPadding.Left, Style->ButtonsTextPadding.Bottom))
						.Justification(ETextJustify::Center)
						.Text(InArgs._TextRight)
					]
				]
			]
		];

		CurrentValue = InArgs._InitValue;
	}
	END_SLATE_FUNCTION_BUILD_OPTIMIZATION

	void SetValue(const NumberType Value)
	{
		CurrentValue = Value;
		Widget_Slider->SetValue(GetCurrentValue().GetValue());
		OnValueChange.ExecuteIfBound(CurrentValue);
	}

	NumberType GetValue() const
	{
		return CurrentValue;
	}

protected:

	const FSliderPercentStyle* Style;

	FOnSliderValueChange OnValueChange;
	NumberType CurrentValue;

	FReply OnClickedButton(const bool IsUp)
	{
		const NumberType sMin = SliderMinimum.Get();
		const NumberType sMax = SliderMaximum.Get();

		SetValue(FMath::Clamp<NumberType>(IsUp ? CurrentValue + ButtonsDelta.Get() : CurrentValue - ButtonsDelta.Get(), sMin, sMax));
		return FReply::Handled();
	}

	void OnValueChangedEvent(float Value)
	{
		const NumberType sMin = SliderMinimum.Get();
		const NumberType sMax = SliderMaximum.Get();

		CurrentValue = FMath::Lerp<NumberType>(sMin, sMax, Value);
		OnValueChange.ExecuteIfBound(CurrentValue);
	}

	TOptional<float> GetCurrentValue() const
	{
		return float(CurrentValue) / float(SliderMaximum.Get());
	}

	FText GetCurrentValueText() const
	{
		if (CustomCenterText.IsBound())
		{
			return CustomCenterText.Get();
		}

		return FText::AsNumber(CurrentValue);
	}

	TAttribute<NumberType> ButtonsDelta, SliderMinimum, SliderMaximum;
	TAttribute<FText> CustomCenterText;

	TSharedPtr<STextBlock> Widget_TextBlock;
	TSharedPtr<SProgressBar> Widget_ProgressBar;
	TSharedPtr<SSlider> Widget_Slider;
};
