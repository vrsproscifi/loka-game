// VRSPRO

#pragma once

class LOKAGAME_API IOnlineSlateError
{
public:
	void ReportErrorCode(const int32 ErrorCode)
	{
		if (IsExistCode(ErrorCode))
		{
			OnErrorReportingCode(ErrorCode);
		}
	}
protected:
	virtual void OnErrorReportingCode(const int32 &ErrorCode) = 0;
	virtual bool IsExistCode(const int32 &ErrorCode) const = 0;
};