// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Styles/LokaSearchBoxWidgetStyle.h"
/**
 * 
 */
class LOKAGAME_API SLokaSearchBox : public SEditableTextBox
{
public:
	SLATE_BEGIN_ARGS(SLokaSearchBox)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FLokaSearchBoxStyle>("SLokaSearchBoxStyle"))
		, _HintText(NSLOCTEXT("SLokaSearchBox", "SearchHint", "Search"))
		, _InitialText()
		, _OnSearch()
		, _SelectAllTextWhenFocused(true)
		, _DelayChangeNotificationsWhileTyping(false)
	{ }
	SLATE_STYLE_ARGUMENT(FLokaSearchBoxStyle, Style)
		SLATE_ATTRIBUTE(FText, HintText)
		SLATE_ATTRIBUTE(FText, InitialText)
		SLATE_EVENT(FOnTextCommitted, OnSearch)
		SLATE_ATTRIBUTE(bool, SelectAllTextWhenFocused)
		SLATE_ATTRIBUTE(float, MinDesiredWidth)
		SLATE_ATTRIBUTE(bool, DelayChangeNotificationsWhileTyping)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

protected:

	const FLokaSearchBoxStyle* Style;

	FOnTextCommitted OnSearch;

	bool IsShowingCancel;
	FReply OnClickedTextButton(const bool);

	void HandleTextChanged(const FText&);
	void HandleTextCommitted(const FText&, ETextCommit::Type);
};
