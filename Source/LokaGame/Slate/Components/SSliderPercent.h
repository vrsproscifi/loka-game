// VRSPRO

#pragma once


#include "SSliderNumber.h"
#include "Styles/SliderPercentWidgetStyle.h"
/**
 * 
 */
class LOKAGAME_API SSliderPercent : public SSliderNumber<float>
{
public:
	SLATE_BEGIN_ARGS(SSliderPercent)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FSliderPercentStyle>("SSliderPercentStyle"))
		, _TextLeft(FText::FromString("<<"))
		, _TextRight(FText::FromString(">>"))
		, _ButtonsDelta(.1f)
	{}
	SLATE_STYLE_ARGUMENT(FSliderPercentStyle, Style)
	SLATE_ARGUMENT(FText, TextLeft)
	SLATE_ARGUMENT(FText, TextRight)
	SLATE_ATTRIBUTE(float, ButtonsDelta)
	SLATE_EVENT(FOnSliderValueChange, OnValueChange)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

protected:

	FText GetCurrentValueText_Custom() const;
};
