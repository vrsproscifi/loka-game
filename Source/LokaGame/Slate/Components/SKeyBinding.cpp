// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "SKeyBinding.h"
#include "SlateOptMacros.h"

#include "UTGameUserSettings.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SKeyBinding::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	Key = InArgs._Key;
	Binding = InArgs._Binding;
	IsAlternative = InArgs._IsAlternative;

	TSharedPtr<SWidget> Widget_Content;

	if (InArgs._WithBackground)
	{
		Widget_Content = SNew(SBorder)
			.HAlign(HAlign_Center)
			.VAlign(VAlign_Center)
			.Padding(FMargin(6, 3))
			.BorderImage(&Style->Background)
			[
				SNew(STextBlock)
				.TextStyle(&Style->Text)
				.Text(this, &SKeyBinding::GetKeyText)
			];
	}
	else
	{
		Widget_Content = SNew(STextBlock)
			.TextStyle(&Style->Text)
			.Text(this, &SKeyBinding::GetKeyText)
			;
	}

	ChildSlot
	[
		Widget_Content.ToSharedRef()
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

FText SKeyBinding::GetKeyText() const
{
	if (Binding.IsSet() && Binding.Get().IsEmpty() == false)
	{
		return UUTGameUserSettings::GetKeyBinding(Binding.Get(), IsAlternative.Get(false)).GetDisplayName();
	}

	return Key.Get().GetDisplayName();
}