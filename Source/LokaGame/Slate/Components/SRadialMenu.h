// VRSPRO

#pragma once

#include "Utilities/SUsableCompoundWidget.h"
#include "Styles/RadialMenuWidgetStyle.h"

class SAnimatedBackground;
class SConstraintCanvas;

namespace ESegmentActiveMethod
{
	enum Type
	{
		None = 1 << 0,
		Click = 1 << 1,
		Select = 1 << 2,
		Close = 1 << 3,
		Timed = 1 << 4
	};
}

DECLARE_DELEGATE_RetVal_OneParam(TSharedRef<SWidget>, FOnGenerateCenter, const int32);
DECLARE_DELEGATE_OneParam(FOnActivateSlot, const int32);

class LOKAGAME_API SRadialMenu 
	: public SUsableCompoundWidget
	, public FGCObject
{
public:

	class FSlot
	{
		friend class SRadialMenu;

	public:

		HACK_SLATE_SLOT_ARGS(FSlot)
			: _ActivateMethod(ESegmentActiveMethod::Click)
		{}
		SLATE_ATTRIBUTE(ESegmentActiveMethod::Type, ActivateMethod)
		SLATE_DEFAULT_SLOT(FArguments, Content)
		SLATE_EVENT(FOnGetContent, OnGenerateCenterWidget)
		SLATE_EVENT(FOnClickedOutside, OnActivated)
		SLATE_END_ARGS()

		FSlot(const FArguments& InArgs)
			: Method(InArgs._ActivateMethod)
			, Content(InArgs._Content)
			, Angle(0)
			, OnGenerateCenterWidget(InArgs._OnGenerateCenterWidget)
			, OnActivated(InArgs._OnActivated)
		{}
		
		FORCEINLINE_DEBUGGABLE void AttachWidget(const TSharedRef<SWidget>& InWidget)
		{
			Content.Widget = InWidget;
		}

		FORCEINLINE_DEBUGGABLE const TSharedRef<SWidget>& GetWidget() const
		{
			return Content.Widget;
		}

	protected:

		float Angle;
		TAttribute<ESegmentActiveMethod::Type> Method;
		TAlwaysValidWidget Content;
		FOnGetContent OnGenerateCenterWidget;
		FOnClickedOutside OnActivated;
	};

	SLATE_BEGIN_ARGS(SRadialMenu)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FRadialMenuStyle>(TEXT("SRadialMenuStyle")))
		, _Radius(200.0f)
		, _SegmentsScale(1.0f)
		, _RotatedSegments(false)
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_STYLE_ARGUMENT(FRadialMenuStyle, Style)
	SLATE_SUPPORTS_SLOT_WITH_ARGS(FSlot)
	SLATE_ARGUMENT(float, Radius)
	SLATE_ARGUMENT(float, SegmentsScale)
	SLATE_ARGUMENT(bool, RotatedSegments)
	SLATE_EVENT(FOnGenerateCenter, OnGenerateCenterWidget)
	SLATE_EVENT(FOnActivateSlot, OnActivateSlot)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	// SUsableCompoundWidget Begin
	virtual void ToggleWidget(const bool InToggle) override;
	virtual bool IsInInteractiveMode() const override;
	// SUsableCompoundWidget End

	void RefreshWheel();

	void SetRadius(const float InRadius);
	void SetSegmentsScale(const float InScale);
	void SetRotatedSegments(const bool InRotated);

	FORCEINLINE float GetRadius() const { return WheelRadius; }
	FORCEINLINE float GetSegmentsScale() const { return SegmentScale; }
	FORCEINLINE bool IsRotatedSegments() const { return bRotatedSegments; }

	// Slots

	static FSlot::FArguments Slot()
	{
		return FSlot::FArguments();
	}

	void AddSlot(const FSlot::FArguments& InSlot);
	int32 RemoveSlot(const TSharedRef<SWidget>& SlotWidget);
	void ClearChildren();

protected:

	const FRadialMenuStyle* Style;
	UMaterialInstanceDynamic* MID;
	FSlateBrush MIDBrush;

	int8 SelectedSegment;
	bool bRotatedSegments;
	float SegmentScale;
	float WheelRadius;
	float CurrentRot;
	float TargetRot;
	float RotDeltaPerSegment;
	float SelectorAlpha;

	virtual void Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime) override;
	virtual FReply OnMouseMove(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override;
	virtual FReply OnMouseButtonDown(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override;

	virtual void AddReferencedObjects(FReferenceCollector& Collector) override;

	void InitSelector(const float InScale);
	void SetSelected(const int32 InNumber);
	void GenerateCenterWidget(const int32 InSlot);

	TSharedPtr<SAnimatedBackground> Widget_Animation;
	TSharedPtr<SConstraintCanvas> Widget_Container;
	TSharedPtr<SBox> Widget_WheelContainer;
	TSharedPtr<SBox> Widget_CenterContainer;

	TIndirectArray<FSlot> Children;

	FOnGenerateCenter OnGenerateCenter;
	FOnActivateSlot OnActivateSlot;

	FSlateColor GetSelectorColor() const;
};
