// VRSPRO

#include "LokaGame.h"
#include "SSniperScope.h"
#include "SlateOptMacros.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SSniperScope::Construct(const FArguments& InArgs)
{
	auto _bgb = new FSlateColorBrush(FColor::White);

	ChildSlot
	[
		SNew(SVerticalBox)
		+ SVerticalBox::Slot().FillHeight(1)
		[
			SAssignNew(Borders[0], SBorder).BorderImage(_bgb)
		]
		+ SVerticalBox::Slot().AutoHeight()
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot().FillWidth(1)
			[
				SAssignNew(Borders[1], SBorder).BorderImage(_bgb)
			]
			+ SHorizontalBox::Slot().AutoWidth()
			[
				SAssignNew(Image, SImage).Image(new FSlateNoResource())
			]
			+ SHorizontalBox::Slot().FillWidth(1)
			[
				SAssignNew(Borders[2], SBorder).BorderImage(_bgb)
			]
		]
		+ SVerticalBox::Slot().FillHeight(1)
		[
			SAssignNew(Borders[3], SBorder).BorderImage(_bgb)
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SSniperScope::SetImage(const FSlateBrush* Img)
{
	Image->SetImage(Img);
}

void SSniperScope::SetColor(const FSlateColor& Col)
{
	for (auto t : Borders)
	{
		t->SetBorderBackgroundColor(Col);
	}
}