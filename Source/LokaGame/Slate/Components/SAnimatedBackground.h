// VRSPRO

#pragma once


#include "Widgets/Layout/SBorder.h"
#include "Styles/BackgroundBlurWidgetStyle.h"

class SBackgroundBlur;

namespace EAnimBackAnimation
{
	enum Type
	{
		None,
		LeftFade,
		RightFade,
		UpFade,
		DownFade,
		ZoomIn,
		ZoomOut,
		Color
	};
};

class LOKAGAME_API SAnimatedBackground : public SBorder
{
public:
	SLATE_BEGIN_ARGS(SAnimatedBackground)
		: _BlurStyle(&FLokaStyle::Get().GetWidgetStyle<FBackgroundBlurStyle>("Default_BackgroundBlur"))
		, _HAlign(HAlign_Fill)
		, _VAlign(VAlign_Fill)
		, _Padding(FMargin(.0f))
		, _Content()
		, _ShowAnimation(EAnimBackAnimation::LeftFade)
		, _HideAnimation(EAnimBackAnimation::RightFade)
		, _InitAsHide(true)
		, _Duration(1.5f)
		, _IsRelative(false)
		, _WithColor(true)
		, _Ease(ECurveEaseFunction::QuadInOut)
		, _IsEnabledBlur(false)
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_STYLE_ARGUMENT(FBackgroundBlurStyle, BlurStyle)
	SLATE_ARGUMENT(EHorizontalAlignment, HAlign)
	SLATE_ARGUMENT(EVerticalAlignment, VAlign)
	SLATE_ARGUMENT(float, Duration)
	SLATE_ATTRIBUTE(FMargin, Padding)
	SLATE_DEFAULT_SLOT(FArguments, Content)
	SLATE_ATTRIBUTE(EAnimBackAnimation::Type, ShowAnimation)
	SLATE_ATTRIBUTE(EAnimBackAnimation::Type, HideAnimation)
	SLATE_ARGUMENT(bool, InitAsHide)
	SLATE_ARGUMENT(bool, IsRelative)
	SLATE_ARGUMENT(bool, WithColor)
	SLATE_ARGUMENT(ECurveEaseFunction, Ease)
	// SBackgroundBlur Begin
	SLATE_ARGUMENT(bool, IsEnabledBlur)
	// SBackgroundBlur End
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	void ToggleWidget(const bool);
	void SetShowAnimation(const TAttribute<EAnimBackAnimation::Type>);
	void SetHideAnimation(const TAttribute<EAnimBackAnimation::Type>);

	void SetDuration(const float, const ECurveEaseFunction = ECurveEaseFunction::QuadInOut);
	bool IsAnimAtStart() const;
	bool IsAnimAtEnd() const;

	FORCEINLINE bool IsInAnimated() const { return AnimSequence.IsPlaying(); }

	// SBackgroundBlur Begin
	void SetApplyAlphaToBlur(bool bInApplyAlphaToBlur);
	void SetBlurRadius(const TAttribute<TOptional<int32>>& InBlurRadius);
	void SetBlurStrength(const TAttribute<float>& InStrength);
	void SetLowQualityBackgroundBrush(const FSlateBrush* InBrush);
	// SBackgroundBlur End

protected:

	const FBackgroundBlurStyle* BlurStyle;

	bool IsRelative;
	bool WithColor;
	bool WithBlur;

	EAnimBackAnimation::Type CurrentAnim;

	FCurveSequence AnimSequence;
	FCurveHandle TransHandle;
	FCurveHandle ColorHandle;

	TAttribute<EAnimBackAnimation::Type> ShowAnimation;
	TAttribute<EAnimBackAnimation::Type> HideAnimation;
	TAttribute<EVisibility> VisibilityOverride;

	TSharedPtr<SBackgroundBlur> Widget_BackgroundBlur;

	FLinearColor WidgetLineColor() const;
	TOptional<FSlateRenderTransform> WidgetTrans() const;
	float GetBlurStrength() const;
	TOptional<int32> GetBlurRadius() const;
	EVisibility GetVisibilityByAnim() const;
};
