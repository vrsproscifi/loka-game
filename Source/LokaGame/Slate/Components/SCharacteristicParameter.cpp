// VRSPRO

#include "LokaGame.h"
#include "SCharacteristicParameter.h"
#include "SlateOptMacros.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SCharacteristicParameter::Construct(const FArguments& InArgs)
{
	ValueTypeAttribute = InArgs._Value;

	auto box = SNew(SHorizontalBox);

	ChildSlot
	[
		box
	];

	if (InArgs._Name.IsSet())
	{
		box->AddSlot().FillWidth(1).VAlign(VAlign_Center)
			[
				SNew(STextBlock)
				.Text(InArgs._Name)
				.TextStyle(InArgs._TextStyleName)
			];
	}

	box->AddSlot().AutoWidth().VAlign(VAlign_Center).HAlign(HAlign_Center)
		[
			SNew(SImage).Image(InArgs._Icon)
		];

	box->AddSlot().FillWidth(.4f).VAlign(VAlign_Center)
		[
			SNew(STextBlock)
			.Text(this, &SCharacteristicParameter::GetValueText)
			.TextStyle(InArgs._TextStyleValue)
			.Margin(FMargin(4, 0))
		];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

FText SCharacteristicParameter::GetValueText() const
{
	return FText::AsNumber(ValueTypeAttribute.Get(), &FNumberFormattingOptions::DefaultWithGrouping());
}