// VRSPRO

#pragma once

#include "SResourceTextBoxType.h"

struct FResourceTextBoxValue
{
	int64 Value;
	EResourceTextBoxType::Type Type;
	
	FResourceTextBoxValue(){}
	FResourceTextBoxValue(const EResourceTextBoxType::Type& InType) : Value(0), Type(InType) {}
	FResourceTextBoxValue(const EResourceTextBoxType::Type& InType, const int64& InValue) : Value(InValue), Type(InType) {}

	static FResourceTextBoxValue Money(const int64& InValue);
	static FResourceTextBoxValue Donate(const int64& InValue);
};

Expose_TNameOf(FResourceTextBoxValue)