// VRSPRO

#pragma once

#include "Service/ItemServiceModelId.h"
#include "Widgets/SCompoundWidget.h"
#include "Styles/ResourceTextBoxWidgetStyle.h" 
//#include "Slate/LokaStyle.h"
#include "SResourceTextBoxValue.h"


class LOKAGAME_API SResourceTextBox : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SResourceTextBox)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FResourceTextBoxStyle>("SResourceTextBoxStyle"))
		, _Type(EResourceTextBoxType::Money)
		, _Value(0)
		, _CustomSize(FVector::ZeroVector)
		, _TextVAlign(VAlign_Center)
		, _TextHAlign(HAlign_Fill)
		, _ImageVAlign(VAlign_Center)
		, _ImageHAlign(HAlign_Center)
	{}

	SLATE_STYLE_ARGUMENT(FResourceTextBoxStyle, Style)

	// * Value type and amount pair
	SLATE_ATTRIBUTE(FResourceTextBoxValue, TypeAndValue)

	// * Value type
	SLATE_ATTRIBUTE(EResourceTextBoxType::Type, Type)

	// * Value amount
	SLATE_ATTRIBUTE(int64, Value)

	// * X,Y - Image size, Z - Font size
	SLATE_ARGUMENT(FVector, CustomSize)	
	
	SLATE_ARGUMENT(EVerticalAlignment, TextVAlign)
	SLATE_ARGUMENT(EHorizontalAlignment, TextHAlign)

	SLATE_ARGUMENT(EVerticalAlignment, ImageVAlign)
	SLATE_ARGUMENT(EHorizontalAlignment, ImageHAlign)

	SLATE_EVENT(FOnClicked, OnClicked)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);	

	void SetTypeAndValue(const TAttribute<FResourceTextBoxValue>& InTypeAndValueAttr);
	void SetType(const TAttribute<EResourceTextBoxType::Type>& InTypeAttr);
	void SetValue(const TAttribute<int64>& InValueAttr);
	void SetValue(const FText& InCustomText);	

	const int64 GetValue() const;
	const EResourceTextBoxType::Type GetValueType() const;

	void SetCustomSize(const FVector& = FVector::ZeroVector);

	static EResourceTextBoxType::Type ServiceModelToType(const ServiceTypeId::Type&);

protected:

	const FRTBStyles* GetResourceStyle() const;

	TAttribute<FResourceTextBoxValue> Attr_TypeAndValue;
	TAttribute<EResourceTextBoxType::Type> Attr_ValueType;
	TAttribute<int64> Attr_ValueAmount;

	TSharedPtr<SImage> Image; 
	TSharedPtr<STextBlock> TextBlock;

	FResourceTextBoxStyle *Style;
	FOnClicked OnClicked;

	FReply OnClickedOnImage(const FGeometry& InGeometry, const FPointerEvent& InEvent);

	FText GetCurrentValue() const;
	FText GetCurrentValueAsTime() const;
	FText GetCurrentToolTip() const;
	const FSlateBrush* GetCurrentImage() const;
	FSlateFontInfo GetCurrentFont() const;
	FSlateColor GetCurrentFontColor() const;

	EVisibility GetCurrentVisibilityText() const;
};
