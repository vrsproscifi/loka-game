
#include "LokaGame.h"
#include "Widgets/SCompoundWidget.h"
#include "ToggableWidgetHelper.h"

void FToggableWidgetHelper::ToggleWidget(TSharedRef<SWidget> SourceWidget, FCurveSequence& SourceCurveSequence, const bool Toggle)
{
	if (Toggle)
	{
		if (SourceCurveSequence.IsPlaying())
		{
			if (!SourceCurveSequence.IsForward())
			{
				SourceCurveSequence.Reverse();
			}
		}
		else
		{
			if (SourceCurveSequence.IsAtStart())
			{
				SourceCurveSequence.Play(SourceWidget);
			}
		}
	}
	else
	{
		if (SourceCurveSequence.IsPlaying())
		{
			if (SourceCurveSequence.IsForward())
			{
				SourceCurveSequence.Reverse();
			}
		}
		else
		{
			if (SourceCurveSequence.IsAtEnd())
			{
				SourceCurveSequence.PlayReverse(SourceWidget);
			}
		}
	}
}