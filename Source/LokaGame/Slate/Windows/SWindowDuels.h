// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SWindowBase.h"
#include "Shared/SessionMatchOptions.h"
#include "Shared/PlayerOnlineStatus.h"
#include "Styles/FriendsFormWidgetStyle.h"
#include "Styles/WindowDuelsWidgetStyle.h"

struct FTestDataDuel
{
	FGuid PlayerId;
	FString PlayerName;
	EPlayerFriendStatus PlayerStatus;
	TArray<FSessionMatchOptions> Options;
};

class LOKAGAME_API SWindowDuels : public SWindowBase
{
	friend class SWindowsContainer;

public:

	SLATE_BEGIN_ARGS(SWindowDuels)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FWindowDuelsStyle>("SWindowDuelsStyle"))
	{
		_Visibility = EVisibility::Collapsed;
	}
	SLATE_STYLE_ARGUMENT(FWindowDuelsStyle, Style)
	SLATE_END_ARGS()

	/** Constructs this widget with InArgs */
	void Construct(const FArguments& InArgs, TSharedRef<SWindowsContainer> InOwner);

protected:

	const FWindowDuelsStyle* Style;
	const FFriendsFormStyle* FriednsStyle;

	TArray<TSharedPtr<FTestDataDuel>> Duels;
};
