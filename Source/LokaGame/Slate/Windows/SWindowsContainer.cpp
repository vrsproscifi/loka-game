// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "SWindowsContainer.h"
#include "SlateOptMacros.h"
#include "SWindowBase.h"
#include "SWindowQuickPanel.h"


SWindowsContainer::SWindowsContainer()
	: Children(this)
{
	bCanTick = false;
	bCanSupportFocus = false;
}

SWindowsContainer::FSlot::FSlot()
	: TSlotBase<SWindowsContainer::FSlot>()
	, OwnerRef(StaticCastSharedRef<SWindowsContainer>(SNullWidget::NullWidget))
{

}

SWindowsContainer::FSlot& SWindowsContainer::FSlot::operator[](const TSharedRef<SWindowBase>& InChildWidget)
{
	if (InChildWidget != SNullWidget::NullWidget && InChildWidget->OwnerRef != OwnerRef)
	{
		InChildWidget->OwnerRef = OwnerRef;
	}

	this->AttachWidget(InChildWidget);

	if (OwnerRef != SNullWidget::NullWidget && OwnerRef->Widget_QuickPanel.IsValid())
	{
		OwnerRef->Widget_QuickPanel->Refresh();
	}

	return (FSlot&) (*this);
}

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SWindowsContainer::Construct(const FArguments& InArgs)
{
	bIsNeedQuickPanel = InArgs._IsNeedQuickPanel;
	OnRequestToggle = InArgs._OnRequestToggle;

	if (bIsNeedQuickPanel)
	{
		SAssignNew(Widget_QuickPanel, SWindowQuickPanel, TSharedRef<SWindowsContainer>(this));

		FSlot& NewSlot = *new FSlot();
		NewSlot.OwnerRef = TSharedRef<SWindowsContainer>(this);
		NewSlot[Widget_QuickPanel.ToSharedRef()];		
		this->Children.Add(&NewSlot);
	}

	const int32 NumSlots = InArgs.Slots.Num();
	for (int32 SlotIndex = 0; SlotIndex < NumSlots; ++SlotIndex)
	{
		InArgs.Slots[SlotIndex]->OwnerRef = TSharedRef<SWindowsContainer>(this);
		Children.Add(InArgs.Slots[SlotIndex]);
	}
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SWindowsContainer::ClearChildren()
{
	Children.Empty();
	if (Widget_QuickPanel.IsValid())
	{
		Widget_QuickPanel.Reset();
	}
}

SWindowsContainer::FSlot& SWindowsContainer::AddSlot()
{
	if (bIsNeedQuickPanel && Widget_QuickPanel.IsValid() == false)
	{
		SAssignNew(Widget_QuickPanel, SWindowQuickPanel, TSharedRef<SWindowsContainer>(this));

		FSlot& NewSlot = *new FSlot();
		NewSlot.OwnerRef = TSharedRef<SWindowsContainer>(this);
		NewSlot[Widget_QuickPanel.ToSharedRef()];		
		this->Children.Add(&NewSlot);		
	}

	FSlot& NewSlot = *new FSlot();
	NewSlot.OwnerRef = TSharedRef<SWindowsContainer>(this);
	this->Children.Add(&NewSlot);
	return NewSlot;
}

int32 SWindowsContainer::RemoveSlot(const TSharedRef<SWidget>& SlotWidget)
{
	for (int32 SlotIdx = 0; SlotIdx < Children.Num(); ++SlotIdx)
	{
		if (SlotWidget == Children[SlotIdx].GetWidget())
		{
			Children.RemoveAt(SlotIdx);
			if (Widget_QuickPanel.IsValid())
			{
				Widget_QuickPanel->Refresh();
			}
			return SlotIdx;
		}
	}

	return -1;
}

void SWindowsContainer::OnArrangeChildren(const FGeometry& AllottedGeometry, FArrangedChildren& ArrangedChildren) const
{
	if (Children.Num() > 0)
	{
		for (int32 ChildIndex = 0; ChildIndex < Children.Num(); ++ChildIndex)
		{
			TSharedRef<SWindowBase> Child = StaticCastSharedRef<SWindowBase>(Children[ChildIndex].GetWidget());

			ArrangedChildren.AddWidget(AllottedGeometry.MakeChild(
				Child,
				Child->GetPosition(),
				Child->GetSize()
			));
		}
	}
}

int32 SWindowsContainer::OnPaint(const FPaintArgs& Args,
	const FGeometry& AllottedGeometry,
	const FSlateRect& MyClippingRect,
	FSlateWindowElementList& OutDrawElements,
	int32 LayerId,
	const FWidgetStyle& InWidgetStyle,
	bool bParentEnabled) const
{
	FArrangedChildren ArrangedChildren(EVisibility::Visible);
	this->ArrangeChildren(AllottedGeometry, ArrangedChildren);

	// Because we paint multiple children, we must track the maximum layer id that they produced in case one of our parents
	// wants to an overlay for all of its contents.
	int32 MaxLayerId = LayerId;

	const FPaintArgs NewArgs = Args.WithNewParent(this);

	for (int32 ChildIndex = 0; ChildIndex < ArrangedChildren.Num(); ++ChildIndex)
	{
		FArrangedWidget& CurWidget = ArrangedChildren[ChildIndex];

		bool bWereOverlapping;
		const FSlateRect ChildClipRect = MyClippingRect.IntersectionWith(CurWidget.Geometry.GetLayoutBoundingRect(), bWereOverlapping);

		if (bWereOverlapping)
		{
			const int32 CurWidgetsMaxLayerId = CurWidget.Widget->Paint(NewArgs, CurWidget.Geometry, ChildClipRect, OutDrawElements, MaxLayerId + 1, InWidgetStyle, ShouldBeEnabled(bParentEnabled));

			MaxLayerId = FMath::Max(MaxLayerId, CurWidgetsMaxLayerId);
		}
	}

	return MaxLayerId;
}

FVector2D SWindowsContainer::ComputeDesiredSize(float) const
{
	return FVector2D::ZeroVector;
}

FChildren* SWindowsContainer::GetChildren()
{
	return &Children;
}

void SWindowsContainer::RequestZSort()
{
	Children.Sort([](const FSlot& A, const FSlot& B) {
		TSharedRef<SWindowBase> ChildA = StaticCastSharedRef<SWindowBase>(A.GetWidget());
		TSharedRef<SWindowBase> ChildB = StaticCastSharedRef<SWindowBase>(B.GetWidget());

		return ChildA->GetZOrder() < ChildB->GetZOrder();
	});

	for (int32 ChildIndex = 0; ChildIndex < Children.Num(); ++ChildIndex)
	{
		TSharedRef<SWindowBase> Child = StaticCastSharedRef<SWindowBase>(Children[ChildIndex].GetWidget());
		Child->ZOrder = Child->IsAlwaysOnTop() ? Children.Num() + ChildIndex * 2 : ChildIndex;
	}
}

bool SWindowsContainer::RequestToggle(const bool InToggle, const bool IsInternal)
{
	bool doRequestOut = true;

	if (InToggle == false && IsInternal == false)
	{
		if (Children.Num() > 0)
		{
			for (int32 ChildIndex = 0; ChildIndex < Children.Num(); ++ChildIndex)
			{
				TSharedRef<SWindowBase> Child = StaticCastSharedRef<SWindowBase>(Children[ChildIndex].GetWidget());

				if (Child->IsInInteractiveMode() && Child->CanClosable())
				{
					if (Child->CanInvisable() && Child->IsInInvisible() == false)
					{
						Child->InternalToggleMode(true);
						Child->SaveState();
						doRequestOut = false;
						break;
					}
					else if (Child->HasQuickButton() && !Child->CanInvisable())
					{
						Child->InternalToggleWidget(false);
						Child->SaveState();
						doRequestOut = false;
						break;
					}
				}
			}
		}
	}

	if (doRequestOut)
	{
		if (IsInternal)	OnRequestToggle.ExecuteIfBound(InToggle);
		return true;
	}

	return false;
}

bool SWindowsContainer::IsAnyClosableOpened() const
{
	bool bIsOpenedFound = false;

	if (Children.Num() > 0)
	{
		for (int32 ChildIndex = 0; ChildIndex < Children.Num(); ++ChildIndex)
		{
			TSharedRef<SWindowBase> Child = StaticCastSharedRef<SWindowBase>(Children[ChildIndex].GetWidget());

			if (Child->IsInInteractiveMode() && Child->CanClosable())
			{
				if (Child->CanInvisable() && Child->IsInInvisible() == false)
				{
					bIsOpenedFound = true;
					break;
				}
				else if (Child->HasQuickButton() && !Child->CanInvisable())
				{
					bIsOpenedFound = true;
					break;
				}
			}
		}
	}

	return bIsOpenedFound;
}
