// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "SWindowDuels.h"
#include "SlateOptMacros.h"
#include "SMessageBox.h"
#include "SWindowsContainer.h"
#include "SFriendsForm_Item.h"
#include "SResourceTextBox.h"
#include "SLokaToolTip.h"
#include "RichTextHelpers.h"
#include "Components/Shared/SessionMatchOptions.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SWindowDuels::Construct(const FArguments& InArgs, TSharedRef<SWindowsContainer> InOwner)
{
	Style = InArgs._Style;

	MAKE_UTF8_SYMBOL(sDuel, 0xf011);

	for (int32 i = 0; i < 10; ++i)
	{
		FTestDataDuel TestDuel;
		TestDuel.PlayerId = FGuid::NewGuid();
		TestDuel.PlayerName = FString("TestPlayer_" + FString::FromInt(i));
		TestDuel.PlayerStatus = EPlayerFriendStatus::Online;

		int32 rndNum = FMath::RandRange(1, 5);
		for (int32 b = 0; b < rndNum; ++b)
		{			
			FSessionMatchOptions TestOptions;
			TestOptions.Bet = FTypeCost(FMath::RandRange(1, 500), static_cast<EGameCurrency::Type>(FMath::RandRange(EGameCurrency::Money, EGameCurrency::Coin)));
			TestDuel.Options.Add(TestOptions);
		}

		Duels.Add(MakeShareable(new FTestDataDuel(TestDuel)));
	}



	FriednsStyle = &FLokaStyle::Get().GetWidgetStyle<FFriendsFormStyle>("SFriendsFormStyle");

	auto ListWidget = SNew(SListView<TSharedPtr<FTestDataDuel>>)
		.ListItemsSource(&Duels)
		.OnGenerateRow_Lambda([&](TSharedPtr<FTestDataDuel> InItem, const TSharedRef<STableViewBase>& InOwner)
		{
			TArray<TSharedPtr<FSessionMatchOptions>> OptionsArray;
			for (auto row : InItem->Options)
			{
				OptionsArray.Add(MakeShareable(new FSessionMatchOptions(row)));
			}

			//auto ContentRow = SNew(SListView<TSharedPtr<FSessionMatchOptions>>)
			//.ListItemsSource(new TArray<TSharedPtr<FSessionMatchOptions>>(OptionsArray))
			//.OnGenerateRow_Lambda([&](TSharedPtr<FSessionMatchOptions> InSubItem, const TSharedRef<STableViewBase>& InOwner)
			//{
			//	FFormatNamedArguments FormatNamedArguments;
			//	FormatNamedArguments.Add("Weapon", FRichHelpers::TextHelper_NotifyValue.SetValue(InSubItem->GetWeaponType().GetDisplayName()).ToText());
			//	FormatNamedArguments.Add("Map", FRichHelpers::TextHelper_NotifyValue.SetValue(InSubItem->GetGameMap().GetDisplayName()).ToText());
			//	FormatNamedArguments.Add("Bet", InSubItem->Bet.ToResourse());
			//	FormatNamedArguments.Add("Lifes", FRichHelpers::TextHelper_NotifyValue.SetValue(FText::AsNumber(InSubItem->GetNumberOfLives())).ToText());
			//
			//	auto MyToolTip = SNew(SLokaToolTip)
			//		.Text(FText::Format(NSLOCTEXT("SWindowDuels", "SWindowDuels.ToolTip", "Weapon: {Weapon}\nMap: {Map}\nBet: {Bet}\nNumber of lives: {Lifes}"), FormatNamedArguments));
			//
			//	return SNew(STableRow<TSharedPtr<FTestDataDuel>>, InOwner).ToolTip(MyToolTip).Padding(FMargin(4, 6)).Style(&Style->SubRow)
			//	[
			//		SNew(SHorizontalBox)
			//		+ SHorizontalBox::Slot().AutoWidth()
			//		[
			//			SNew(SResourceTextBox)
			//			.TypeAndValue(InSubItem->Bet.GetCostAsResource())
			//		]
			//		+ SHorizontalBox::Slot()
			//		+ SHorizontalBox::Slot().AutoWidth().HAlign(HAlign_Right)
			//		[
			//			SNew(SButton)
			//			.Text(NSLOCTEXT("SWindowDuels", "SWindowDuels.Accept", "Accept"))
			//		]
			//	];
			//});

			return SNew(STableRow<TSharedPtr<FTestDataDuel>>, InOwner).Style(&Style->MainRow)
			[
				SNew(SExpandableArea)
				.Style(&Style->ExpandableArea)
				.InitiallyCollapsed(true)
				.HeaderContent()
				[
					SNew(SHorizontalBox)
					+ SHorizontalBox::Slot().HAlign(HAlign_Center).VAlign(VAlign_Center).AutoWidth()
					[
						SNew(SImage)
						.ToolTipText_Static(&SFriendsForm_Item::GetStatusToolTip, InItem->PlayerStatus)
						.Image_Static(&SFriendsForm_Item::GetStatusImage, FriednsStyle, InItem->PlayerStatus)
					]
					+ SHorizontalBox::Slot().HAlign(HAlign_Left).VAlign(VAlign_Center).AutoWidth()
					[
						SNew(STextBlock).TextStyle(&FriednsStyle->Squad.Member).Text(FText::FromString(InItem->PlayerName))
					]
				]
				//.BodyContent()
				//[
				//	ContentRow
				//]
			];
		});

	auto SuperArgs = SWindowBase::FArguments()
		.SizeLimits(FBox2D(FVector2D(320, 400), FVector2D(320, 1000)))
		.StartPosition(FVector2D(0, 0))
		.TitleText(NSLOCTEXT("SWindowDuels", "SWindowDuels.Title", "Duel Fight List"))
		.QuickButton(FText::FromString(sDuel))
		.QuickButtonOrder(10)
		.CanClosable(true)
		.CanInvisable(true)
		.Content()
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot().AutoHeight()
			[
				SNew(SButton)
				.Text(NSLOCTEXT("SWindowDuels", "SWindowDuels.MakeNew", "Make New Duel"))
			]
			+ SVerticalBox::Slot()
			[
				ListWidget
			]
		];

	SWindowBase::Construct(SuperArgs, InOwner);
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION
