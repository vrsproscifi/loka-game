// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SWindowBase.h"

class LOKAGAME_API SWindowQuit : public SWindowBase
{
	friend class SWindowsContainer;

public:

	SLATE_BEGIN_ARGS(SWindowQuit)
	{
		_Visibility = EVisibility::Collapsed;
	}
	SLATE_END_ARGS()

	/** Constructs this widget with InArgs */
	void Construct(const FArguments& InArgs, TSharedRef<SWindowsContainer> InOwner);

	virtual void ToggleWidget(const bool InToggle, const bool IsAlternative) override;
};
