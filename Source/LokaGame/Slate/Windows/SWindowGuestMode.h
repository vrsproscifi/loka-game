// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SWindowBase.h"
#include "Styles/LobbyContainerWidgetStyle.h"

DECLARE_DELEGATE_OneParam(FOnNextLocation, const FGuid&);

struct FConstructionWorldData;
/**
 * 
 */
class LOKAGAME_API SWindowGuestMode : public SWindowBase
{
public:
	SLATE_BEGIN_ARGS(SWindowGuestMode)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FLobbyContainerStyle>(TEXT("SLobbyContainerStyle")))
	{}
	SLATE_STYLE_ARGUMENT(FLobbyContainerStyle, Style)
	SLATE_ATTRIBUTE(const FConstructionWorldData*, Data)
	SLATE_EVENT(FOnNextLocation, OnNextLocation)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs, TSharedRef<SWindowsContainer> InOwner);

protected:

	const FLobbyContainerStyle* Style;

	TAttribute<const FConstructionWorldData*> Data;
	FOnNextLocation OnNextLocation;

	FText GetDescription() const;
	FText GetPrevName() const;
	FText GetNextName() const;

	FReply OnClickedHandler(const bool IsNext);
};
