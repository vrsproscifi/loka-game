// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SWindowBase.h"
#include "Widgets/SCompoundWidget.h"
#include "Utilities/SlateUtilityTypes.h"

class SWindowQuickPanel;

class LOKAGAME_API SWindowsContainer 
	: public SPanel
	, public TStaticSlateWidget<SWindowsContainer>
{
public:
	SWindowsContainer();

	class FSlot : public TSlotBase<FSlot>
	{
		friend class SWindowsContainer;

	public:

		FSlot();
		FSlot& operator[](const TSharedRef<SWindowBase>& InChildWidget);

	protected:

		TSharedRef<SWindowsContainer> OwnerRef;
	};

	SLATE_BEGIN_ARGS(SWindowsContainer)
		: _IsNeedQuickPanel(true)
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_SUPPORTS_SLOT(SWindowsContainer::FSlot)
	SLATE_EVENT(FOnBooleanValueChanged, OnRequestToggle)
	SLATE_ARGUMENT(bool, IsNeedQuickPanel)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	FSlot& AddSlot();

	int32 RemoveSlot(const TSharedRef<SWidget>& SlotWidget);
	void ClearChildren();

	virtual void RequestZSort();
	// true - ���� ��� ��������� �������� ���� ������� ����� false
	virtual bool RequestToggle(const bool InToggle, const bool IsInternal = true);
	virtual bool IsAnyClosableOpened() const;

public:

	// SWidget overrides

	virtual void OnArrangeChildren(const FGeometry& AllottedGeometry, FArrangedChildren& ArrangedChildren) const override;
	virtual int32 OnPaint(const FPaintArgs& Args, const FGeometry& AllottedGeometry, const FSlateRect& MyClippingRect, FSlateWindowElementList& OutDrawElements, int32 LayerId, const FWidgetStyle& InWidgetStyle, bool bParentEnabled) const override;
	virtual FVector2D ComputeDesiredSize(float) const override;
	virtual FChildren* GetChildren() override;

protected:

	bool bIsNeedQuickPanel;
	TPanelChildren< FSlot > Children;
	TSharedPtr<SWindowQuickPanel> Widget_QuickPanel;

	FOnBooleanValueChanged OnRequestToggle;
};
