// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Utilities/SUsableCompoundWidget.h"
#include "Styles/WindowsWidgetStyle.h"

class SWindowsContainer;

struct FWindowBaseRect : public FSlateRect
{
	FWindowBaseRect();
	FWindowBaseRect(const FVector2D& InPosition, const FVector2D& InSize);

	FWindowBaseRect& ClampSizeToLimits(const FVector2D& InMin, const FVector2D& InMax);
	FWindowBaseRect& ClampSizeToLimits(const FBox2D& InLimits);

	FWindowBaseRect& ExtendBy(const FMargin& InValue);
	FWindowBaseRect& ExtendByWithLimits(const FMargin& InValue, const FBox2D& InLimits);

	FWindowBaseRect& operator=(const FSlateRect& InValue);	
};

class LOKAGAME_API SWindowBase : public SUsableCompoundWidget
{
	friend class SWindowsContainer;
	friend class SWindowQuickPanel;

public:
	SWindowBase();

	SLATE_BEGIN_ARGS(SWindowBase)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FWindowsStyle>("SWindowsStyle"))
		, _SizeLimits(FBox2D(FVector2D(200, 200), FVector2D(500, 500)))
		, _StartSize(FVector2D(250, 250))
		, _StartPosition(FVector2D(250, 250))
		, _InitAsInvisible(false)
		, _InitAsClosed(false)
		, _QuickButtonOrder(0)
		, _CanClosable(true)
		, _CanInvisable(true)
		, _IsAlwaysOnTop(false)
	{
		_Visibility = EVisibility::Collapsed;
	}
	SLATE_STYLE_ARGUMENT(FWindowsStyle, Style)
	SLATE_ATTRIBUTE(FBox2D, SizeLimits)
	SLATE_ARGUMENT(FVector2D, StartPosition)
	SLATE_ARGUMENT(FVector2D, StartSize)
	SLATE_ARGUMENT(bool, InitAsInvisible)
	SLATE_ARGUMENT(bool, InitAsClosed)
	SLATE_ARGUMENT(uint8, QuickButtonOrder)
	SLATE_ARGUMENT(FName, SaveTag)
	SLATE_ATTRIBUTE(FText, TitleText)
	SLATE_ATTRIBUTE(FText, QuickButton)
	SLATE_ATTRIBUTE(bool, CanClosable)
	SLATE_ATTRIBUTE(bool, CanInvisable)
	SLATE_ATTRIBUTE(bool, IsAlwaysOnTop)
	SLATE_NAMED_SLOT(FArguments, Title)
	SLATE_NAMED_SLOT(FArguments, Content)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs, TSharedRef<SWindowsContainer> InOwner);

	FVector2D GetPosition() const;
	FVector2D GetSize() const;

	void SetTitle(const TAttribute<FText>& InTitle);
	void SetTitle(const TSharedRef<SWidget> InTitleWidget);
	void SetContent(const TSharedRef<SWidget> InContentWidget);
	void SetPosition(const FVector2D& InPosition, const bool InIsForce = false);
	void SetSize(const FVector2D& InSize, const bool InIsForce = false);
	void SetSizeLimits(const TAttribute<FBox2D>& InSizeLimits, const bool InIsForce = false);
	void SetQuickButton(const TAttribute<FText>& InQuickButton);
	void SetFlashing(const bool InToggle, const float InTime = .0f);

	// SUsableCompoundWidget Begin
	virtual void ToggleWidget(const bool InToggle) override;
	virtual bool IsInInteractiveMode() const;
	// SUsableCompoundWidget End

	virtual void ToggleWidget(const bool InToggle, const bool IsAlternative);

	// false - visible, true - invisible window background and header
	virtual void ToggleMode(const bool InToggle);
	virtual bool IsInInvisible() const { return bIsInvisible; }
	virtual bool IsFlashing() const { return bIsFlashing; }

	virtual bool CanClosable() const;
	virtual bool CanInvisable() const;
	virtual bool HasQuickButton() const;
	virtual bool IsAlwaysOnTop() const;

	virtual uint64 GetZOrder() const;

	virtual TSharedRef<SWidget> GetToggableButton();

	enum EMoveType
	{
		MT_None,
		MT_Move,
		MT_Size,
		MT_End
	};

	enum ESizeType
	{
		ST_None,
		ST_LeftUp,
		ST_Up,
		ST_RightUp,
		ST_Right,
		ST_RightDown,
		ST_Down,
		ST_LeftDown,
		ST_Left,
		ST_Title,
		ST_End
	};

protected:

	const FWindowsStyle* Style;
	const FSlateBrush* EmptyBrush;

	FName SaveTag;
	EMoveType CurrentMoveType;
	ESizeType CurrentSizeType;
	FVector2D CurrentPosition;
	FVector2D CurrentSize;
	FVector2D ActionOffset;
	FVector2D TargetPosition;
	FVector2D TargetSize;
	uint64 ZOrder;
	int64 LastVersion;
	uint8 QOrder;
	bool bIsInvisible;
	bool bIsFlashing;

	TAttribute<FBox2D>	Attr_SizeLimits;
	TAttribute<FText>	Attr_QuickButton;
	TAttribute<bool>	Attr_CanClosable;
	TAttribute<bool>	Attr_CanInvisable;
	TAttribute<bool>	Attr_IsAlwaysOnTop;

	TSharedPtr<SBox>			Widget_TitleContainer;
	TSharedPtr<SBorder>			Widget_ContentContainer;
	TSharedPtr<SHorizontalBox>	Widget_ControllsContainer;

	TSharedRef<SWindowsContainer> OwnerRef;

	float GetBlurStrength() const;
	TOptional<int32> GetBlurRadius() const;

	virtual void OnWindowControllsGeneration();

	virtual void Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime) override;
	virtual FCursorReply OnCursorQuery(const FGeometry& MyGeometry, const FPointerEvent& CursorEvent) const override;
	virtual FReply OnMouseButtonDown(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override;
	virtual FReply OnMouseButtonUp(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override;
	virtual FReply OnMouseMove(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override;

	ESizeType DetectZone(const FVector2D& InPosition) const;	

	void LoadState();
	void SaveState();

	virtual void InternalToggleWidget(const bool InToggle);
	virtual void InternalToggleMode(const bool InToggle);

	void InternalSetPosition(const FVector2D& InPosition, const bool InIsForce = false);
	void InternalSetSize(const FVector2D& InSize, const bool InIsForce = false);
};
