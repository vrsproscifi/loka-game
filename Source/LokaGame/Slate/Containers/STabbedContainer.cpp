// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "STabbedContainer.h"
#include "SlateOptMacros.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void STabbedContainer::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	Orientation = InArgs._Orientation;
	ActiveSlot = 0;

	TSharedRef<SWidget> localWidgetMain = SNullWidget::NullWidget;

	SAssignNew(Widget_ControllsVContainer, SVerticalBox);
	SAssignNew(Widget_ControllsHContainer, SHorizontalBox);

	if (Orientation == Orient_Vertical)
	{
		localWidgetMain =
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot().AutoWidth()
			[
				Widget_ControllsVContainer.ToSharedRef()
			]
			+ SHorizontalBox::Slot()
			[
				SAssignNew(Widget_ContentContainer, SWidgetSwitcher)
				.WidgetIndex(this, &STabbedContainer::GetActiveSlot)
			];
	}
	else
	{
		localWidgetMain =
			SNew(SVerticalBox)
			+ SVerticalBox::Slot().AutoHeight()
			[
				Widget_ControllsHContainer.ToSharedRef()
			]
			+ SVerticalBox::Slot()
			[
				SAssignNew(Widget_ContentContainer, SWidgetSwitcher)
				.WidgetIndex(this, &STabbedContainer::GetActiveSlot)
			];
	}

	ChildSlot
	[
		localWidgetMain
	];

	for (auto TargetSlot : InArgs.Slots)
	{
		Slots.Add(TargetSlot);
	}

	RegenerateWidgets();
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void STabbedContainer::AddSlot(const FSlot::FArguments& InSlot)
{
	Slots.Add(new FSlot(InSlot));
	RegenerateWidgets();
}

bool STabbedContainer::AddSlotUnique(const FSlot::FArguments& InSlot)
{
	bool localIsFoundByName = false;
	for (auto MySlot : Slots)
	{
		if (MySlot.Name.Get().EqualToCaseIgnored(InSlot._Name.Get()))
		{
			localIsFoundByName = true;
			break;
		}
	}

	if (localIsFoundByName == false)
	{
		AddSlot(InSlot);
		return true;
	}

	return false;
}

void STabbedContainer::ClearChildren()
{
	ClearChildrenInternal();
	Slots.Empty();
}

bool STabbedContainer::SetActiveSlot(const int32 InSlotIndex)
{
	if (Slots.IsValidIndex(InSlotIndex))
	{
		ActiveSlot = InSlotIndex;
		return true;
	}

	return false;
}

bool STabbedContainer::SetActiveSlot(const TSharedRef<SWidget> InWidgetRef)
{
	int32 _count = 0;
	for (auto MySlot : Slots)
	{
		if (MySlot.Content.Widget == InWidgetRef)
		{
			return SetActiveSlot(_count);			
		}

		++_count;
	}

	return false;
}

ECheckBoxState STabbedContainer::GetControllState(const int32 InIndex) const
{
	return InIndex == ActiveSlot ? ECheckBoxState::Checked : ECheckBoxState::Unchecked;
}

int32 STabbedContainer::GetActiveSlot() const
{
	return ActiveSlot;
}

void STabbedContainer::ClearChildrenInternal()
{
	Widget_ControllsHContainer->ClearChildren();
	Widget_ControllsVContainer->ClearChildren();

	for (auto MySlot : Slots)
	{
		Widget_ContentContainer->RemoveSlot(MySlot.Content.Widget);
	}
}

void STabbedContainer::RegenerateWidgets()
{
	ClearChildrenInternal();
	
	int32 _count = 0;
	for (auto MySlot : Slots)
	{
		Widget_ContentContainer->AddSlot(_count).Padding(MySlot.Padding)[MySlot.Content.Widget];

		const FCheckBoxStyle* TargetStyle_CheckBox = nullptr;
		const FTextBlockStyle* TargetStyle_TextBlock = nullptr;
		FMargin TargetMargin_TextBlock;

		if (Orientation == Orient_Vertical)
		{
			TargetStyle_TextBlock = &Style->VerticalControlls.Text;
			TargetMargin_TextBlock = Style->VerticalControlls.TextMargin;

			if (_count == 0)
			{
				TargetStyle_CheckBox = &Style->VerticalControlls.First;
			}
			else if (_count == Slots.Num() - 1)
			{
				TargetStyle_CheckBox = &Style->VerticalControlls.Last;
			}
			else
			{
				TargetStyle_CheckBox = &Style->VerticalControlls.Middle;
			}
		}
		else
		{
			TargetStyle_TextBlock = &Style->HorizontalControlls.Text;
			TargetMargin_TextBlock = Style->HorizontalControlls.TextMargin;

			if (_count == 0)
			{
				TargetStyle_CheckBox = &Style->HorizontalControlls.First;
			}
			else if (_count == Slots.Num() - 1)
			{
				TargetStyle_CheckBox = &Style->HorizontalControlls.Last;
			}
			else
			{
				TargetStyle_CheckBox = &Style->HorizontalControlls.Middle;
			}
		}

		auto CtrlWidget = SNew(SCheckBox)
			.Type(ESlateCheckBoxType::ToggleButton)
			.Style(TargetStyle_CheckBox)
			.OnCheckStateChanged(this, &STabbedContainer::OnControllStateChanged, _count)
			.IsChecked(this, &STabbedContainer::GetControllState, _count)
			[
				SNew(SBox).HAlign(HAlign_Center).VAlign(VAlign_Center)
				[
					SNew(STextBlock)
					.Justification(ETextJustify::Center)
					.Text(MySlot.Name)
					.TextStyle(TargetStyle_TextBlock)
					.Margin(TargetMargin_TextBlock)
				]
			];

		if (Orientation == Orient_Vertical)
		{
			auto& MyCtrlSlot = Widget_ControllsVContainer->AddSlot()[CtrlWidget];
			if (MySlot.IsAutoSize.Get())
			{
				MyCtrlSlot.AutoHeight();
			}
		}
		else
		{
			auto& MyCtrlSlot = Widget_ControllsHContainer->AddSlot()[CtrlWidget];
			if (MySlot.IsAutoSize.Get())
			{
				MyCtrlSlot.AutoWidth();
			}
		}

		++_count;
	}	
}

void STabbedContainer::OnControllStateChanged(ECheckBoxState InState, const int32 InIndex)
{
	ActiveSlot = InIndex;
}
