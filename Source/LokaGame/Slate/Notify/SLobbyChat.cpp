// VRSPRO

#include "LokaGame.h"
#include "SLobbyChat.h"
#include "SlateOptMacros.h"

#include "SFriends_Commands.h"

#include "STextBlockButton.h"
#include "Text/SlateHyperlinkRun.h"

#include "RichTextLayoutMarshaller.h"
#include "Decorators/LokaTextDecorator.h"
#include "Containers/STabbedContainer.h"
#include "ChatComponent/ChatMessageContainer.h"
#include "ChatComponent.h"
#include "RichTextHelpers.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SLobbyChat::Construct(const FArguments& InArgs, TSharedRef<SWindowsContainer> InOwner)
{
	Style = InArgs._Style;
	ChatComponent = InArgs._ChatComponent;

	MAKE_UTF8_SYMBOL(sChat, 0xf086);

	auto SuperArgs = SWindowBase::FArguments()
	.QuickButton(FText::FromString(sChat))
	.QuickButtonOrder(2)
	.SizeLimits(FBox2D(FVector2D(420, 320), FVector2D(1600, 900)))
	.StartPosition(FVector2D(20.0f, 960))
	.TitleText(NSLOCTEXT("SLobbyChat", "SLobbyChat.Title", "Chat"))
	.Content()
	[
		SNew(SVerticalBox)
		+ SVerticalBox::Slot()
		[
			SAssignNew(MessagesSwitcher, STabbedContainer)
			+ STabbedContainer::Slot().Name(NSLOCTEXT("SLobbyChat", "Channel.Global", "Global"))
			[
				SAssignNew(MessagesContainer[EChatChanelGroup::Global], SScrollBox)
				.ConsumeMouseWheel(EConsumeMouseWheel::WhenScrollingPossible)
				.AllowOverscroll(EAllowOverscroll::Yes)
				.Orientation(EOrientation::Orient_Vertical)
				.ScrollBarStyle(&FLokaStyle::Get().GetWidgetStyle<FScrollBarStyle>("Default_ScrollBar"))
				.ScrollBarThickness(FVector2D(4, 4))
			]
			+ STabbedContainer::Slot().Name(NSLOCTEXT("SLobbyChat", "Channel.Squad", "Squad"))
			[
				SAssignNew(MessagesContainer[EChatChanelGroup::Squad], SScrollBox)
				.ConsumeMouseWheel(EConsumeMouseWheel::WhenScrollingPossible)
				.AllowOverscroll(EAllowOverscroll::Yes)
				.Orientation(EOrientation::Orient_Vertical)
				.ScrollBarStyle(&FLokaStyle::Get().GetWidgetStyle<FScrollBarStyle>("Default_ScrollBar"))
				.ScrollBarThickness(FVector2D(4, 4))
			]
		]
		+ SVerticalBox::Slot().AutoHeight()
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot().FillWidth(1)
			[
				SAssignNew(MessageInput, SEditableTextBox)
				.ClearKeyboardFocusOnCommit(false)
				.Style(&FLokaStyle::Get().GetWidgetStyle<FEditableTextBoxStyle>("Default_EditableTextBox"))
				.OnTextCommitted(this, &SLobbyChat::OnMessageCommitted)
			]
			+ SHorizontalBox::Slot().AutoWidth()
			[
				SNew(SButton)
				.Text(NSLOCTEXT("SLobbyChat", "Button.Send", "Send"))
				.ButtonStyle(&Style->SendButton)
				.TextStyle(&Style->SendButtonText)
				.ContentPadding(FMargin(20, 0))
				.HAlign(HAlign_Center)
				.VAlign(VAlign_Center)
				.OnClicked_Lambda([&] { OnMessageCommitted(MessageInput->GetText(), ETextCommit::OnEnter); return FReply::Handled(); })
			]
		]
	];

	SWindowBase::Construct(SuperArgs, InOwner);

	if (ChatComponent.IsValid())
	{
		ChatComponent->OnChannelsUpdate.AddSP(this, &SLobbyChat::OnUpdateChannels);
		ChatComponent->OnMessagesUpdate.AddSP(this, &SLobbyChat::OnUpdateMessages);
	}
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SLobbyChat::OnMessageCommitted(const FText& Message, ETextCommit::Type Type)
{
	if (Type == ETextCommit::OnEnter)
	{
		if (ChatComponent.IsValid())
		{
			auto MyActiveSlot = MessagesSwitcher->GetActiveSlot(); // 0 = Global, 1 = Squad
			for (auto MyChannel : ChanneslMap)
			{
				if ((MyChannel.Value.ChanelGroup == EChatChanelGroup::Global && MyActiveSlot == 0) ||
					(MyChannel.Value.ChanelGroup == EChatChanelGroup::Squad  && MyActiveSlot == 1))
				{
					FChatMessageSendModel sMessage;
					sMessage.ChanelId = MyChannel.Value.ChanelId;
					sMessage.Message = Message.ToString();

					ChatComponent->SendRequestPostMessageTo(sMessage);
					break;
				}
			}			
		}

		MessageInput->SetText(FText::GetEmpty());
		FSlateApplication::Get().SetKeyboardFocus(MessageInput, EFocusCause::Mouse);
	}
}

void SLobbyChat::OnUpdateChannels(const TArray<FChatChanelModel>& InChannels)
{
	ChanneslMap.Empty();

	for (auto MyChannel : InChannels)
	{
		ChanneslMap.Add(MyChannel.ChanelId, MyChannel);
	}
}

void SLobbyChat::OnUpdateMessages(const TArray<FChatMessageReciveModel>& InMessages)
{
	for (auto MyMessage : InMessages)
	{
		AppendMessage(MyMessage);
	}

	SetFlashing(true, 10.0f);
}

//=====================================================================================================================================================] Public Functions

void SLobbyChat::AppendMessage(const FChatMessageReciveModel& Message)
{
	if (ChanneslMap.Contains(Message.ChanelId))
	{
		auto MyChannel = ChanneslMap[Message.ChanelId];

		if (MyChannel.ChanelGroup == EChatChanelGroup::Global || 
			MyChannel.ChanelGroup == EChatChanelGroup::Clan || 
			MyChannel.ChanelGroup == EChatChanelGroup::Squad)
		{
			if (MessagesContainer[MyChannel.ChanelGroup].IsValid())
			{
				FFormatNamedArguments Arguments;
				Arguments.Add("Name", FText::FromString(Message.PlayerName));
				Arguments.Add("Time", FText::AsTime(FDateTime::FromUnixTimestamp(Message.CreatedDate)));
				Arguments.Add("Text", FText::FromString(Message.Message));
				Arguments.Add("PlayerId", FText::FromString(Message.PlayerId.ToString(EGuidFormats::Digits)));
				
				TMap<FName, FSlateFontInfo> Fonts;
				Fonts.Add("AttrFontTime", Style->Row.TextTime.Font);
				Fonts.Add("AttrFontText", Style->Row.TextMessage.Font);

				TArray<TSharedRef<ITextDecorator>> TextDecorators;
				TextDecorators.Add(FLokaTextDecorator::Create(Style->Row.TextMessage, Fonts));
				TextDecorators.Add(FHyperlinkDecorator::Create(Message.PlayerId.ToString(EGuidFormats::Digits), FSlateHyperlinkRun::FOnClick::CreateLambda([&](const TMap<FString, FString>& InData) {
					if (InData.Contains("id")) CurrentPlayerId = FLokaGuid::FromString(InData.FindChecked("id"));
					if (InData.Contains("name")) OpenContextMenu(InData.FindChecked("name"));
				})));

				FRichHelpers::FTextHelper TimeTextHelper;
				TimeTextHelper.SetValue("{Time}").SetColor(Style->Row.TextTime.ColorAndOpacity.GetSpecifiedColor()).SetFont("AttrFontTime").SetSize(8);

				FRichHelpers::FTextHelper MessageTextHelper;
				MessageTextHelper.SetValue("{Text}").SetColor(Style->Row.TextMessage.ColorAndOpacity.GetSpecifiedColor()).SetFont("AttrFontText");

				FText ChatRow = FText::Format(FText::FromString(TimeTextHelper.ToString() + " [<a id=\"{PlayerId}\" name=\"{Name}\" textstyle=\"SLobbyChatStyleHyperlink\">{Name}</>]: " + MessageTextHelper.ToString()), Arguments);
				MessagesContainer[MyChannel.ChanelGroup]->AddSlot()
					[
						SNew(SMultiLineEditableTextBox)
						.AutoWrapText(true)
						.IsReadOnly(true)
						.RevertTextOnEscape(false)
						.Style(&Style->ChatRow)
						.SelectAllTextWhenFocused(false)
						.Text(ChatRow)
						.Marshaller(FRichTextLayoutMarshaller::Create(TextDecorators, &FLokaStyle::Get()))
					];

				MessagesContainer[MyChannel.ChanelGroup]->ScrollToEnd();
			}
		}
	}
}

void SLobbyChat::OpenContextMenu(const FString& Name)
{
	SFriendsCommands::GetAlt().SetCurrentTargetId(CurrentPlayerId);
	FMenuBuilder MenuBuilder(true, SFriendsCommands::Get().GetCommandList());
	{
		MenuBuilder.BeginSection("FriendMenu.Header", FText::FromString(Name));
		{
			//MenuBuilder.AddMenuEntry(SFriendsCommands::Get().PrivateMessage);
			MenuBuilder.AddMenuEntry(SFriendsCommands::Get().GetCommand(SFriendsCommands::ConstructionInvite));
			MenuBuilder.AddMenuEntry(SFriendsCommands::Get().GetCommand(SFriendsCommands::SquadInvite));
			MenuBuilder.AddMenuEntry(SFriendsCommands::Get().GetCommand(SFriendsCommands::DuelInvite));			
		}
		MenuBuilder.EndSection();

		MenuBuilder.BeginSection("FriendMenu.Middle");
		{
			MenuBuilder.AddMenuEntry(SFriendsCommands::Get().GetCommand(SFriendsCommands::AddFriend));
		}
		MenuBuilder.EndSection();
	}

	FLokaStyle::ReGenerateContextMenuStyle();
	MenuBuilder.SetStyle(&FLokaStyle::Get(), "Menu");

	FSlateApplication::Get().PushMenu(FSlateApplication::Get().GetGameViewport().ToSharedRef(), FWidgetPath(), MenuBuilder.MakeWidget(), FSlateApplication::Get().GetCursorPos(), FPopupTransitionEffect(FPopupTransitionEffect::ContextMenu));
}