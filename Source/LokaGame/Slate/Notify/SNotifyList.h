// VRSPRO

#pragma once


#include "Utilities/SUsableCompoundWidget.h"
#include "Styles/NotifyListWidgetStyle.h"
#include "Utilities/SlateUtilityTypes.h"
#include "Windows/SWindowBase.h"

struct FNotifyInfo
{
	TSharedPtr<SWidget> ContentWidget;

	FText Title;
	FText Content;
	FText Hyperlink;
	float ShowTime;

	const FSlateBrush* Image;

	FNotifyInfo();

	FNotifyInfo(const FText &_Content);
	FNotifyInfo(const FText &_Content, const FText &_Title);
	FNotifyInfo(const FText &_Content, const FText &_Title, const FText &_Hyperlink);
	FNotifyInfo(const FText &_Content, const FText &_Title, const FText &_Hyperlink, const FSlateBrush* _Image);

	FNotifyInfo(TSharedRef<SWidget> _Content);
	FNotifyInfo(TSharedRef<SWidget> _Content, const FText &_Title);
	FNotifyInfo(TSharedRef<SWidget> _Content, const FText &_Title, const FText &_Hyperlink);
	FNotifyInfo(TSharedRef<SWidget> _Content, const FText &_Title, const FText &_Hyperlink, const FSlateBrush* _Image);
};

class LOKAGAME_API SNotifyList 
	: public SWindowBase
	, public TStaticSlateWidget<SNotifyList>
{
public:
	SLATE_BEGIN_ARGS(SNotifyList)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FNotifyListStyle>("SNotifyListStyle"))
	{
		_Visibility = EVisibility::HitTestInvisible;
	}
	SLATE_STYLE_ARGUMENT(FNotifyListStyle, Style)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	virtual void ToggleWidget(const bool InToggle) override;
	virtual void ToggleWidget(const bool InToggle, const bool IsAlternative) override;
	virtual bool IsInInteractiveMode() const override;
	virtual void ToggleMode(const bool InToggle) override;

	void AddNotify(const FNotifyInfo);

protected:

	TSharedPtr<SScrollBox> StaticNotifyContainer;
	TSharedPtr<SScrollBox> DynamicNotifyContainer;

	int32 CurrentMode() const { return IsInInvisible(); }

	virtual void InternalToggleWidget(const bool InToggle) override;
	virtual void InternalToggleMode(const bool InToggle) override;
	void OnRemoveDynamicNotify(const TSharedRef<SWidget> &);

	const FNotifyListStyle *Style;
};
