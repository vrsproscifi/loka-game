// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "SMessageTip.h"
#include "SlateOptMacros.h"
#include "SAnimatedBackground.h"
#include "Decorators/LokaTextDecorator.h"
#include "Decorators/LokaResourceDecorator.h"
#include "Decorators/LokaKeyDecorator.h"
#include "Decorators/LokaSpaceDecorator.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SMessageTip::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;

	ChildSlot.HAlign(HAlign_Center).VAlign(VAlign_Top).Padding(100, 80)
	[
		SNew(SBox)
		.MinDesiredWidth(this, &SMessageTip::GetBoxSizeParameter, ESizeParameter::MinDesiredWidth) //Style->Box.MinSize.X)
		.MaxDesiredWidth(this, &SMessageTip::GetBoxSizeParameter, ESizeParameter::MaxDesiredWidth) //(Style->Box.MaxSize.X)
		.MinDesiredHeight(this, &SMessageTip::GetBoxSizeParameter, ESizeParameter::MinDesiredHeight) //(Style->Box.MinSize.Y)
		.MaxDesiredHeight(this, &SMessageTip::GetBoxSizeParameter, ESizeParameter::MaxDesiredHeight) //(Style->Box.MaxSize.Y)
		[
			SAssignNew(Widget_Animation, SAnimatedBackground)
			.ShowAnimation(EAnimBackAnimation::UpFade)
			.HideAnimation(EAnimBackAnimation::DownFade)
			.Duration(.8f)
			.IsRelative(true)
			.InitAsHide(true)
			.IsEnabledBlur(true)
			[
				SNew(SBorder)
				.BorderImage(&Style->Background)
				[
					SNew(SVerticalBox)
					+ SVerticalBox::Slot().AutoHeight() // Title
					[
						SAssignNew(Widget_Title, SRichTextBlock)
						.TextStyle(&Style->TitleFont)
						.Justification(ETextJustify::Center)
						.DecoratorStyleSet(&FLokaStyle::Get())
						+ SRichTextBlock::Decorator(FLokaTextDecorator::Create(Style->TitleFont))
					]
					+ SVerticalBox::Slot().AutoHeight() // Seperator
					[
						SNew(SSeparator)
						.SeparatorImage(&Style->Separator.SeparatorImage)
						.Orientation(Style->Separator.Orientation)
						.ColorAndOpacity(Style->Separator.ColorAndOpacity)
						.Thickness(Style->Separator.Thickness)
					]
					+ SVerticalBox::Slot().Expose(ContentSlot).FillHeight(1) // Message & Buttons
				]
			]
		]
	];

	SetAllowedSize(FBox2D(Style->MinSize, Style->MaxSize));

	ShowSound = Style->ShowSound;
	HideSound = Style->HideSound;
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SMessageTip::ToggleWidget(const bool InToggle)
{
	Widget_Animation->ToggleWidget(InToggle);
	FSlateApplication::Get().PlaySound(InToggle ? ShowSound : HideSound);

	if (InToggle == false)
	{
		RegisterActiveTimer(.8f, FWidgetActiveTimerDelegate::CreateLambda([&](double InCurrentTime, float InDeltaTime) {

			if (QueueId != NAME_None)
			{
				auto CurrentName = QueueId;
				QueueId = NAME_None;
				RemoveQueue(CurrentName);
			}

			ShowSound = Style->ShowSound;
			HideSound = Style->HideSound;

			return EActiveTimerReturnType::Stop;
		}));
	}
}

void SMessageTip::SetTitle(const FText& InTitle)
{
	Widget_Title->SetText(InTitle);
}

void SMessageTip::SetContent(const FText& Text, const ETextJustify::Type TextJustify)
{
	BoxSize = FBox2D(Style->MinSize, Style->MaxSize);

	TMap<FName, FSlateFontInfo> Fonts;

	auto Widget = SNew(SRichTextBlock)
		.Text(Text)
		.AutoWrapText(true)
		.TextStyle(&Style->ContentFont)
		.Justification(TextJustify)
		.DecoratorStyleSet(&FLokaStyle::Get())
		+ SRichTextBlock::Decorator(FLokaTextDecorator::Create( Style->ContentFont))
		+ SRichTextBlock::Decorator(FLokaResourceDecorator::Create())
		+ SRichTextBlock::Decorator(FLokaKeyDecorator::Create())
		+ SRichTextBlock::Decorator(FLokaSpaceDecorator::Create())
		+ SRichTextBlock::Decorator(FHyperlinkDecorator::Create(TEXT(""), FSlateHyperlinkRun::FOnClick::CreateLambda([&](const FSlateHyperlinkRun::FMetadata& InData) {
			if (InData.Contains("url")) FPlatformProcess::LaunchURL(*InData.FindChecked("url"), nullptr, nullptr);
			OnHyperlink.ExecuteIfBound(InData);
		})));

	if (Text.ToString().Len() >= 800)
	{
		ContentSlot->AttachWidget(
			SNew(SScrollBox)
			.Orientation(EOrientation::Orient_Vertical)
			.ScrollBarStyle(&FLokaStyle::Get().GetWidgetStyle<FScrollBarStyle>("Default_ScrollBar")).ScrollBarThickness(FVector2D(4, 4))
			+ SScrollBox::Slot()
			[
				Widget
			]
		);
	}
	else
	{
		ContentSlot->AttachWidget(Widget);
	}

	ContentSlot->Padding(FMargin(20, 10));
}

void SMessageTip::SetContent(TSharedPtr<SWidget> Widget)
{
	BoxSize = FBox2D(Style->MinSize, Style->MaxSize);
	ContentSlot->AttachWidget(Widget.ToSharedRef());
}

void SMessageTip::SetAllowedSize(const FBox2D& InSize)
{
	BoxSize = InSize;
}

FOptionalSize SMessageTip::GetBoxSizeParameter(const ESizeParameter InParameter) const
{
	switch (InParameter)
	{
		case ESizeParameter::MinDesiredWidth: return BoxSize.Min.X;
		case ESizeParameter::MaxDesiredWidth: return BoxSize.Max.X;
		case ESizeParameter::MinDesiredHeight: return BoxSize.Min.Y;
		case ESizeParameter::MaxDesiredHeight: return BoxSize.Max.Y;
	}

	return .0f;
}