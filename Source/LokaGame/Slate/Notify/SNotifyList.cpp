// VRSPRO

#include "LokaGame.h"
#include "SNotifyList.h"
#include "SlateOptMacros.h"
#include "Components/SWindowBackground.h"

#include "Decorators/LokaTextDecorator.h"
#include "Decorators/LokaResourceDecorator.h"
#include "SScaleBox.h"
#include "Windows/SWindowsContainer.h"
#include "Decorators/LokaSpaceDecorator.h"
#include "Decorators/LokaKeyDecorator.h"

DECLARE_DELEGATE_OneParam(FOnRemoveNotifyItem, const TSharedRef<SWidget> &)


class LOKAGAME_API SNotifyItem : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SNotifyItem)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FNotifyListStyle>("SNotifyListStyle"))
		, _NotifyInfo()
		, _IsDynamic(false)
		, _ShowTime(10.0f)
	{
		_Visibility = EVisibility::Visible;
	}
	SLATE_STYLE_ARGUMENT(FNotifyListStyle, Style)
	SLATE_ARGUMENT(FNotifyInfo, NotifyInfo)
	SLATE_ARGUMENT(bool, IsDynamic)
	SLATE_ARGUMENT(float, ShowTime)
	SLATE_EVENT(FOnRemoveNotifyItem, OnRemove)
	SLATE_END_ARGS()	

	BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
	void Construct(const FArguments& InArgs)
	{
		auto Style = InArgs._Style;

		ColorHandle = Anim.AddCurve(0, 1.5f, ECurveEaseFunction::Linear);
		TransformHandle = Anim.AddCurve(0, 1, ECurveEaseFunction::QuadIn);
		Anim.AddCurveRelative(0, InArgs._ShowTime * .5f);

		NotifyInfo = InArgs._NotifyInfo;
		OnRemove = InArgs._OnRemove;
		IsDynamic = InArgs._IsDynamic;

		if (!NotifyInfo.ContentWidget.IsValid())
		{
			NotifyInfo.ContentWidget =
				SNew(SRichTextBlock)
				.AutoWrapText(true)
				.Text(NotifyInfo.Content)
				.TextStyle(&Style->NotifyContent)
				.DecoratorStyleSet(&FLokaStyle::Get())
				+ SRichTextBlock::Decorator(FLokaTextDecorator::Create(Style->NotifyContent))
				+ SRichTextBlock::Decorator(FLokaResourceDecorator::Create())
				+ SRichTextBlock::Decorator(FLokaSpaceDecorator::Create())
				+ SRichTextBlock::Decorator(FLokaKeyDecorator::Create());
		}

		ChildSlot
		[
			SNew(SBorder)
			.Padding(0)
			.BorderImage(new FSlateNoResource())
			.ColorAndOpacity(this, &SNotifyItem::WidgetColor)
			.RenderTransformPivot(FVector2D(.5f, .5f))
			.RenderTransform(this, &SNotifyItem::WidgetPosition)
			[
				SNew(SWindowBackground)
				.Padding(4)
				.IsEnabledBlur(true)
				.IsEnabledHeaderBackground(false)
				.HeaderContent()
				[
					SNew(SRichTextBlock)
					.Text(NotifyInfo.Title)
					.TextStyle(&Style->NotifyTitle)
					.DecoratorStyleSet(&FLokaStyle::Get())
					.Visibility(!NotifyInfo.Title.IsEmpty() ? EVisibility::Visible : EVisibility::Collapsed)
					+ SRichTextBlock::Decorator(FLokaTextDecorator::Create(Style->NotifyTitle))
					+ SRichTextBlock::Decorator(FLokaResourceDecorator::Create())
				]
				.Content()
				[
					SNew(SHorizontalBox)
					+ SHorizontalBox::Slot() // Content
					.FillWidth(1)
					[
						SNew(SHorizontalBox)
						+ SHorizontalBox::Slot() // Image
						.VAlign(VAlign_Center)
						.HAlign(HAlign_Left)
						.AutoWidth()
						[
							SNew(SBox)
							.Visibility((NotifyInfo.Image != nullptr) ? EVisibility::Visible : EVisibility::Collapsed)
							.MaxDesiredWidth(128)
							.MaxDesiredHeight(128)
							[
								SNew(SScaleBox)
								.HAlign(HAlign_Center)
								.VAlign(VAlign_Center)
								.Stretch(EStretch::ScaleToFit)
								[
									SNew(SImage)
									.Image(NotifyInfo.Image)
								]
							]
						]
						+ SHorizontalBox::Slot() // Content
						.FillWidth(1)
						[
							NotifyInfo.ContentWidget.ToSharedRef()
						]
					]
					+ SHorizontalBox::Slot() // Button
					.VAlign(VAlign_Bottom)
					.HAlign(HAlign_Center)
					.Padding(FMargin(6, 4))
					.AutoWidth()
					[
						SNew(SHyperlink)
						.Visibility(!NotifyInfo.Hyperlink.IsEmpty() ? EVisibility::Visible : EVisibility::Collapsed)
						.Text(NotifyInfo.Hyperlink)
					]
				]
			]
		];

		if (IsDynamic)
		{
			TargetOffset = 80.0f;
			Anim.Play(this->AsShared());
		}
		else
		{
			Anim.JumpToEnd();
		}
	}
	END_SLATE_FUNCTION_BUILD_OPTIMIZATION

protected:

	bool IsDynamic;

	FOnRemoveNotifyItem OnRemove;

	FNotifyInfo NotifyInfo;

	FCurveSequence Anim;
	FCurveHandle ColorHandle;
	FCurveHandle TransformHandle;

	float TargetOffset;

	FLinearColor WidgetColor() const
	{
		return FLinearColor(1, 1, 1, ColorHandle.GetLerp());
	}

	TOptional<FSlateRenderTransform> WidgetPosition() const
	{
		return FSlateRenderTransform(FVector2D(.0f, FMath::Lerp(TargetOffset, .0f, TransformHandle.GetLerp())));
	}

	virtual void Tick(const FGeometry & AllottedGeometry, const double InCurrentTime, const float InDeltaTime) override
	{
		if (!Anim.IsPlaying() && IsDynamic)
		{
			if (Anim.IsAtEnd()) { TargetOffset = -280.0f;  Anim.PlayReverse(this->AsShared()); }
			else { OnRemove.ExecuteIfBound(this->AsShared()); }
		}
	}
};

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SNotifyList::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;

	MAKE_UTF8_SYMBOL(sNotify, 0xf06a);

	auto SuperArgs = SWindowBase::FArguments()
	.QuickButton(FText::FromString(sNotify))
	.QuickButtonOrder(120)
	.SaveTag("SNotifyList")
	.CanClosable(false)
	.CanInvisable(true)
	.InitAsInvisible(true)
	.IsAlwaysOnTop(true)
	.SizeLimits(FBox2D(FVector2D(400, 320), FVector2D(680, 1000)))
	.StartSize(FVector2D(420, 560))
	.TitleText(NSLOCTEXT("SNotifyList", "SNotifyList.Title", "Notifications"))
	.Content()
	[
		SNew(SWidgetSwitcher)
		.WidgetIndex(this, &SNotifyList::CurrentMode)
		+ SWidgetSwitcher::Slot()
		[
			SAssignNew(StaticNotifyContainer, SScrollBox)
			.ConsumeMouseWheel(EConsumeMouseWheel::Always)
			.Orientation(Orient_Vertical)
			.ScrollBarStyle(&FLokaStyle::Get().GetWidgetStyle<FScrollBarStyle>("Default_ScrollBar"))
		]
		+ SWidgetSwitcher::Slot()
		[
			SAssignNew(DynamicNotifyContainer, SScrollBox)
			.ConsumeMouseWheel(EConsumeMouseWheel::WhenScrollingPossible)
			.AllowOverscroll(EAllowOverscroll::No)
			.Orientation(Orient_Vertical)
			.ScrollBarAlwaysVisible(false)
			.ScrollBarVisibility(EVisibility::Collapsed)
			.Visibility(EVisibility::SelfHitTestInvisible)
		]
	];

	TSharedRef<SWindowsContainer> sNull = StaticCastSharedRef<SWindowsContainer>(SNullWidget::NullWidget);
	SWindowBase::Construct(SuperArgs, sNull);
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SNotifyList::ToggleWidget(const bool InToggle)
{
	ToggleMode(!InToggle);
}

void SNotifyList::ToggleWidget(const bool InToggle, const bool IsAlternative)
{
	ToggleMode(!InToggle);
}

bool SNotifyList::IsInInteractiveMode() const
{
	return IsInInvisible() == false;
}

void SNotifyList::ToggleMode(const bool InToggle)
{
	SWindowBase::ToggleMode(InToggle);
	SetVisibility(!InToggle ? EVisibility::Visible : EVisibility::SelfHitTestInvisible);
	Widget_TitleContainer->SetVisibility(!InToggle ? EVisibility::Visible : EVisibility::Hidden);
	Widget_ContentContainer->SetVisibility(!InToggle ? EVisibility::Visible : EVisibility::HitTestInvisible);
}

void SNotifyList::InternalToggleWidget(const bool InToggle)
{
	InternalToggleMode(!InToggle);
}

void SNotifyList::InternalToggleMode(const bool InToggle)
{
	SWindowBase::InternalToggleMode(InToggle);
	SetVisibility(!InToggle ? EVisibility::Visible : EVisibility::SelfHitTestInvisible);
	Widget_TitleContainer->SetVisibility(!InToggle ? EVisibility::Visible : EVisibility::Hidden);
	Widget_ContentContainer->SetVisibility(!InToggle ? EVisibility::Visible : EVisibility::HitTestInvisible);
}

void SNotifyList::OnRemoveDynamicNotify(const TSharedRef<SWidget> &Widget)
{
	DynamicNotifyContainer->RemoveSlot(Widget);
}

void SNotifyList::AddNotify(const FNotifyInfo Info)
{
	StaticNotifyContainer->AddSlot().AttachWidget(SNew(SNotifyItem).NotifyInfo(Info));
	StaticNotifyContainer->ScrollToEnd();

	DynamicNotifyContainer->AddSlot().AttachWidget(SNew(SNotifyItem).ShowTime(Info.ShowTime).NotifyInfo(Info).IsDynamic(true).OnRemove(this, &SNotifyList::OnRemoveDynamicNotify));
	DynamicNotifyContainer->ScrollToEnd();

	FSlateApplication::Get().PlaySound(Style->NotifySound);

	SetFlashing(true, 20.0f);
}


FNotifyInfo::FNotifyInfo()
	: ContentWidget()
	, Title()
	, Content()
	, Hyperlink()
	, Image()
	, ShowTime(10.0f)
{}
FNotifyInfo::FNotifyInfo(const FText &_Content)
	: ContentWidget()
	, Title()
	, Content(_Content)
	, Hyperlink()
	, Image()
	, ShowTime(10.0f)
{}
FNotifyInfo::FNotifyInfo(const FText &_Content, const FText &_Title)
	: ContentWidget()
	, Title(_Title)
	, Content(_Content)
	, Hyperlink()
	, Image()
	, ShowTime(10.0f)
{}
FNotifyInfo::FNotifyInfo(const FText &_Content, const FText &_Title, const FText &_Hyperlink)
	: ContentWidget()
	, Title(_Title)
	, Content(_Content)
	, Hyperlink(_Hyperlink)
	, Image()
	, ShowTime(10.0f)
{}
FNotifyInfo::FNotifyInfo(const FText &_Content, const FText &_Title, const FText &_Hyperlink, const FSlateBrush* _Image)
	: ContentWidget()
	, Title(_Title)
	, Content(_Content)
	, Hyperlink(_Hyperlink)
	, Image(_Image)
	, ShowTime(10.0f)
{}
FNotifyInfo::FNotifyInfo(TSharedRef<SWidget> _Content)
	: ContentWidget(_Content)
	, Title()
	, Content()
	, Hyperlink()
	, Image()
	, ShowTime(10.0f)
{}
FNotifyInfo::FNotifyInfo(TSharedRef<SWidget> _Content, const FText &_Title)
	: ContentWidget(_Content)
	, Title(_Title)
	, Content()
	, Hyperlink()
	, Image()
	, ShowTime(10.0f)
{}
FNotifyInfo::FNotifyInfo(TSharedRef<SWidget> _Content, const FText &_Title, const FText &_Hyperlink)
	: ContentWidget(_Content)
	, Title(_Title)
	, Content()
	, Hyperlink(_Hyperlink)
	, Image()
	, ShowTime(10.0f)
{}
FNotifyInfo::FNotifyInfo(TSharedRef<SWidget> _Content, const FText &_Title, const FText &_Hyperlink, const FSlateBrush* _Image)
	: ContentWidget(_Content)
	, Title(_Title)
	, Content()
	, Hyperlink(_Hyperlink)
	, Image(_Image)
	, ShowTime(10.0f)
{}