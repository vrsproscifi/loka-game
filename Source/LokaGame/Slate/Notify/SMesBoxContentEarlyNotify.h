// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/SCompoundWidget.h"
#include "Styles/ContentEarlyNotifyWidgetStyle.h"

/**
 * 
 */
class LOKAGAME_API SMesBoxContentEarlyNotify : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SMesBoxContentEarlyNotify)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FContentEarlyNotifyStyle>("SContentEarlyNotifyStyle"))
	{}
	SLATE_STYLE_ARGUMENT(FContentEarlyNotifyStyle, Style)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

protected:

	const FContentEarlyNotifyStyle* Style;
};
