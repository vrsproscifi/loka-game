// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/SCompoundWidget.h"

class UPlayerInventoryItem;
class UItemBaseEntity;
/**
 * 
 */
class LOKAGAME_API SInventoryItem : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SInventoryItem)
	{}
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs, UPlayerInventoryItem* InInstance);

protected:

	TWeakObjectPtr<UPlayerInventoryItem> InstancePtr;

	UItemBaseEntity* GetEntity() const;

	const FSlateBrush* GetBrush() const;
	FText GetName() const;
	FText GetDescription() const;

	virtual FReply OnMouseButtonDown(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override;
	virtual FReply OnDragDetected(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override;
};
