// VRSPRO

#include "LokaGame.h"
#include "SAuthForm.h"
#include "SlateOptMacros.h"

#include "Notify/SMessageBox.h"
#include "Components/SAnimatedBackground.h"
#include "SBackgroundBlur.h"

#define LOCTEXT_NAMESPACE "SAuthForm"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SAuthForm::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	OnQuit = InArgs._OnQuit;
	OnAuthentication = InArgs._OnAuthentication;

	ChildSlot
	[
		SAssignNew(AnimatedBackground[0], SAnimatedBackground)
		.ShowAnimation(EAnimBackAnimation::Color)
		.HideAnimation(EAnimBackAnimation::Color)
		.IsRelative(true)
		.Duration(1.6f)
		[
			SNew(SBackgroundBlur)
			.HAlign(HAlign_Left)
			.VAlign(VAlign_Fill)
			.Padding(FMargin(100, 0))
			.bApplyAlphaToBlur(Style->bApplyAlphaToBlur)
			.BlurStrength(Style->BlurStrength)
			.BlurRadius(Style->BlurRadius)
			.LowQualityFallbackBrush(new FSlateNoResource())
			[
				SNew(SBox).WidthOverride(400)
				[
					SAssignNew(AnimatedBackground[1], SAnimatedBackground)
					.ShowAnimation(EAnimBackAnimation::RightFade)
					.HideAnimation(EAnimBackAnimation::LeftFade)
					.IsRelative(true)
					.Duration(1.5f)
					[
						SNew(SBorder)
						.BorderImage(&Style->Background)
						.Padding(0)
						[
							SNew(SBorder)
							.BorderImage(&Style->Border)
							.Padding(FMargin(10, 100))
							[
								SNew(SVerticalBox)
								+ SVerticalBox::Slot().FillHeight(1).VAlign(VAlign_Top)
								[
									SNew(STextBlock).Text(LOCTEXT("Welcome", "\tWelcome \r\n\t\t\t to LOKA"))
									.TextStyle(&Style->Header)
								]
								+ SVerticalBox::Slot().AutoHeight().VAlign(VAlign_Center).Padding(2)
								[
									SAssignNew(Widget_InputEmail, SEditableTextBox)
									.Style(&Style->Inputs)
									.HintText(LOCTEXT("E-Mail", "E-Mail"))
								]
								+ SVerticalBox::Slot().AutoHeight().VAlign(VAlign_Center).Padding(2)
								[
									SAssignNew(Widget_InputPass, SEditableTextBox)
									.Style(&Style->Inputs)
									.HintText(LOCTEXT("Password", "Password"))
									.IsPassword(true)
								]
								+ SVerticalBox::Slot().AutoHeight().VAlign(VAlign_Center).Padding(2)
								[
									SAssignNew(Widget_IsRememberMe, SCheckBox)
									.Style(&Style->CheckBox)
									[
										SNew(STextBlock).Text(LOCTEXT("RememberMe", "Remember me"))
										.TextStyle(&Style->CheckBoxText)
									]
								]
								+ SVerticalBox::Slot().AutoHeight().VAlign(VAlign_Center)
								[
									SNew(SHorizontalBox)
									+ SHorizontalBox::Slot()
									[
										SNew(SButton).Text(LOCTEXT("SignIn", "SIGN IN"))
										.OnClicked(this, &SAuthForm::OnClickedSign, true)
										.HAlign(HAlign_Center)
										.VAlign(VAlign_Center)
										.TextStyle(&Style->ButtonsText)
										.ButtonStyle(&Style->ButtonLeft)
									]
									+ SHorizontalBox::Slot()
									[
										SNew(SButton).Text(LOCTEXT("SignUp", "SIGN UP"))
										.OnClicked(this, &SAuthForm::OnClickedSign, false)
										.HAlign(HAlign_Center)
										.VAlign(VAlign_Center)
										.TextStyle(&Style->ButtonsText)
										.ButtonStyle(&Style->ButtonRight)
									]
								]
								+ SVerticalBox::Slot().FillHeight(.5f).VAlign(VAlign_Center)
								[
									SNew(SBox)
									[
										SAssignNew(AnimatedError, SAnimatedBackground)
										.ShowAnimation(EAnimBackAnimation::ZoomIn)
										.HideAnimation(EAnimBackAnimation::ZoomOut)
										.Duration(.5f)
										.InitAsHide(true)
										[
											SNew(SBorder)
											.BorderImage(&Style->ErrorBackground)
											.Padding(FMargin(8, 4))
											[
												SAssignNew(Widget_ErrorMessage, STextBlock).TextStyle(&Style->ErrorText).AutoWrapText(true)
											]
										]
									]
								]
								+ SVerticalBox::Slot().FillHeight(.5f).VAlign(VAlign_Bottom)
								[
									SNew(SHorizontalBox)
									+ SHorizontalBox::Slot()
									[
										SNew(SButton).Text(LOCTEXT("Settings", "Settings"))
										.OnClicked(InArgs._OnClickedSettings)
										.ButtonStyle(&Style->ButtonLeft)
										.HAlign(HAlign_Center)
										.VAlign(VAlign_Center)
										.TextStyle(&Style->ButtonsText)
									]
									+ SHorizontalBox::Slot()
									[
										SNew(SButton).Text(LOCTEXT("Quit", "Quit"))
										.HAlign(HAlign_Center)
										.VAlign(VAlign_Center)
										.TextStyle(&Style->ButtonsText)
										.OnClicked_Lambda([&]() {
											SMessageBox::Get()->SetHeaderText(NSLOCTEXT("SLobbyContainer", "Lobby.Exit", "Exit"));
											SMessageBox::Get()->SetContent(NSLOCTEXT("SLobbyContainer", "Lobby.Exit.Content", "You seriously want to exit game?"));
											SMessageBox::Get()->SetButtonText(EMessageBoxButton::Left, NSLOCTEXT("SLobbyContainer", "Lobby.Exit.Yes", "Yes"));
											SMessageBox::Get()->SetButtonText(EMessageBoxButton::Middle, FText::GetEmpty());
											SMessageBox::Get()->SetButtonText(EMessageBoxButton::Right, NSLOCTEXT("SLobbyContainer", "Lobby.Exit.No", "No"));
											SMessageBox::Get()->SetEnableClose(false);
											SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](const EMessageBoxButton& Button)
											{
												if (Button == EMessageBoxButton::Left)
												{
													OnQuit.ExecuteIfBound();
												}

												SMessageBox::Get()->ToggleWidget(false);
											});

											SMessageBox::Get()->ToggleWidget(true);

											return FReply::Handled();
										})
										.ButtonStyle(&Style->ButtonRight)
									]
								]
							]
						]
					]
				]
			]
		]
	];

	LoadAccount();

	AnimatedBackground[0]->ToggleWidget(true);
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

FReply SAuthForm::OnClickedSign(const bool IsSignIn)
{
	if (IsSignIn == false)
	{
		FPlatformProcess::LaunchURL(TEXT("http://lokagame.com/Account/Register"), nullptr, nullptr);
		if (FSlateApplication::Get().GetActiveTopLevelWindow().IsValid())
		{
			FSlateApplication::Get().GetActiveTopLevelWindow()->Minimize();
		}
	}
	else
	{
		OnAuthentication.ExecuteIfBound(FPlayerAuthorizationRequest(Widget_InputEmail->GetText().ToString(), Widget_InputPass->GetText().ToString(), IsSignIn));
		SaveAccount();
	}

	return FReply::Handled();
}

void SAuthForm::ToggleWidget(const bool InToggle)
{
	AnimatedBackground[0]->ToggleWidget(InToggle);
	AnimatedBackground[1]->ToggleWidget(InToggle);

	OnToggleInteractive.ExecuteIfBound(InToggle);
}

bool SAuthForm::IsInInteractiveMode() const
{
	return AnimatedBackground[0]->IsAnimAtEnd();
}

void SAuthForm::OnErrorMessage(const FText& Message)
{
	if (!Message.IsEmpty())
	{
		Widget_ErrorMessage->SetText(Message);
		AnimatedError->ToggleWidget(true);
		
		RegisterActiveTimer(15.0f, FWidgetActiveTimerDelegate::CreateSP(this, &SAuthForm::OnErrorMessageHide));
	}
	else
	{
		AnimatedError->ToggleWidget(false);
	}
}

EActiveTimerReturnType SAuthForm::OnErrorMessageHide(double InCurrentTime, float InDeltaTime)
{
	AnimatedError->ToggleWidget(false);
	return EActiveTimerReturnType::Stop;
}

void SAuthForm::SaveAccount()
{
	const FString 
		_login = Widget_IsRememberMe->IsChecked() ? Widget_InputEmail->GetText().ToString() : FString(),
		_passw = Widget_IsRememberMe->IsChecked() ? Widget_InputPass->GetText().ToString() : FString();

	const auto SaveFileName = FPaths::GeneratedConfigDir() + "Account.ini";
	auto SaveFile = FConfigFile();
	SaveFile.SetString(TEXT("Account"), TEXT("RememberMe"), Widget_IsRememberMe->IsChecked() ? TEXT("True") : TEXT("False"));
	SaveFile.SetString(TEXT("Account"), TEXT("Login"), *_login);
	SaveFile.SetString(TEXT("Account"), TEXT("Password"), *_passw);

	if (!SaveFile.Write(SaveFileName))
	{
		SaveFile.UpdateSections(*SaveFileName, TEXT("Account"));
	}
}

void SAuthForm::LoadAccount()
{
	FString _remember("False"), _login, _passw;

	auto SaveFile = FConfigFile();
	SaveFile.Read(FPaths::GeneratedConfigDir() + "Account.ini");
	SaveFile.GetString(TEXT("Account"), TEXT("RememberMe"), _remember);
	SaveFile.GetString(TEXT("Account"), TEXT("Login"), _login);
	SaveFile.GetString(TEXT("Account"), TEXT("Password"), _passw);

	Widget_IsRememberMe->SetIsChecked(_remember.Equals("True", ESearchCase::IgnoreCase) ? ECheckBoxState::Checked : ECheckBoxState::Unchecked);
	Widget_InputEmail->SetText(FText::FromString(_login));
	Widget_InputPass->SetText(FText::FromString(_passw));
}

#undef LOCTEXT_NAMESPACE
