// VRSPRO

#include "LokaGame.h"
#include "SWeaponModularModule.h"
#include "SlateOptMacros.h"
#include "Module/ItemModuleEntity.h"
#include "Fraction/FractionEntity.h"

// Widgets included
#include "Layout/SScaleBox.h"
#include "SLokaToolTip.h"
#include "SResourceTextBox.h"

// Buy border
#include "Styles/FractionManagerWidgetStyle.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SWeaponModularModule::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	Instance = InArgs._Instance;
	
	auto BuyStyle = &FLokaStyle::Get().GetWidgetStyle<FFractionManagerStyle>(TEXT("SFractionManagerStyle"));

	//TODO: SeNTIke
	//ChildSlot
	//[
	//	SNew(SBox)
	//	.ToolTip(SNew(SLokaToolTip).Visibility_Lambda([&]() { return (Instance.Get() != nullptr && Instance.Get()->GetFractionEntity() && Instance.Get()->GetLevel() >= Instance.Get()->GetFractionEntity()->Progress.CurrentRank) ? EVisibility::Visible : EVisibility::Hidden; })
	//	[		
	//		SNew(SBox)
	//		.WidthOverride(200)
	//		[
	//			SNew(SVerticalBox)
	//			+ SVerticalBox::Slot().AutoHeight().HAlign(HAlign_Center).Padding(FMargin(10, 6))
	//			[
	//				SNew(STextBlock).Text(NSLOCTEXT("SWeaponModularModule", "SWeaponModularModule.UnlockedBy", "Unlocked By"))
	//			]
	//			+ SVerticalBox::Slot().AutoHeight().Padding(FMargin(10, 4))
	//			[
	//				SNew(SHorizontalBox)
	//				+ SHorizontalBox::Slot()
	//				[
	//					SNew(STextBlock).Text(NSLOCTEXT("SWeaponModularModule", "SWeaponModularModule.FractionName", "Fraction:"))
	//				]
	//				+ SHorizontalBox::Slot().AutoWidth()
	//				[
	//					SNew(STextBlock).Text(this, &SWeaponModularModule::GetFractionName)
	//				]
	//			]
	//			+ SVerticalBox::Slot().AutoHeight().Padding(FMargin(10, 4))
	//			[
	//				SNew(SHorizontalBox)
	//				+ SHorizontalBox::Slot()
	//				[
	//					SNew(STextBlock).Text(NSLOCTEXT("SWeaponModularModule", "SWeaponModularModule.FractionRank", "Rank:"))
	//				]
	//				+ SHorizontalBox::Slot().AutoWidth()
	//				[
	//					SNew(STextBlock).Text(this, &SWeaponModularModule::GetFractionRank)
	//				]
	//			]
	//		]
	//	])
	//	.WidthOverride(160)
	//	.HeightOverride(80)
	//	[
	//		SNew(SWidgetSwitcher)
	//		.WidgetIndex_Lambda([&]() { return (Instance.Get() != nullptr); })
	//		+ SWidgetSwitcher::Slot()
	//		.HAlign(HAlign_Center)
	//		.VAlign(VAlign_Center)
	//		[
	//			SNew(STextBlock)
	//			.TextStyle(&Style->Module.EmptyName)
	//			.Text(NSLOCTEXT("SWeaponModularModule", "SWeaponModularModule.EmptyName", "Empty Slot"))
	//		]
	//		+ SWidgetSwitcher::Slot()
	//		[
	//			SNew(SOverlay)
	//			+ SOverlay::Slot()
	//			[
	//				SNew(SScaleBox)
	//				.VAlign(VAlign_Center)
	//				.HAlign(HAlign_Center)
	//				.Stretch(EStretch::ScaleToFit)
	//				[
	//					SNew(SImage)
	//					.Image_Lambda([&, e = new FSlateNoResource()]() { return Instance.Get() ? Instance.Get()->GetImage() : e; })
	//				]
	//			]
	//			+ SOverlay::Slot().HAlign(HAlign_Fill).VAlign(VAlign_Top).Padding(2)
	//			[
	//				SNew(STextBlock)
	//				.Text_Lambda([&, e = FText::GetEmpty()] { return Instance.Get() ? Instance.Get()->GetDescription().Name : e; })
	//				.TextStyle(&Style->Module.Name)
	//				.Justification(ETextJustify::Left)
	//			]
	//			+ SOverlay::Slot()
	//			.HAlign(HAlign_Fill)
	//			.VAlign(VAlign_Bottom)
	//			[
	//				SNew(SBorder)
	//				.BorderImage(&BuyStyle->Item.BuyBorder)
	//				.Visibility_Lambda([&]() { return (Instance.Get() != nullptr && Instance.Get()->IsLocked() && IsHovered()) ? EVisibility::Visible : EVisibility::Hidden; })
	//				.Padding(FMargin(8, 2))
	//				.HAlign(HAlign_Fill)
	//				[
	//					SNew(SVerticalBox)
	//					+ SVerticalBox::Slot()
	//					[
	//						SNew(STextBlock)
	//						.Text(NSLOCTEXT("SFractionManager", "Item.Buy", "BUY"))
	//						.Justification(ETextJustify::Center)
	//						.Font(BuyStyle->Item.BuyText)
	//						.ColorAndOpacity(BuyStyle->Item.BuyTextColor)
	//					]
	//					+ SVerticalBox::Slot().HAlign(HAlign_Center)
	//					[
	//						SNew(SResourceTextBox).CustomSize(FVector(16, 16, 12))
	//						.Type_Lambda([&]() { return (Instance.Get() != nullptr && Instance.Get()->GetCost().IsDonate) ? EResourceTextBoxType::Donate : EResourceTextBoxType::Money; })
	//						.Value_Lambda([&]() { return (Instance.Get() != nullptr) ? Instance.Get()->GetCost().Amount : 0; })
	//					]
	//				]
	//			]
	//		]
	//	]
	//];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

FText SWeaponModularModule::GetFractionName() const
{
	//TODO: SeNTIke
	//if (Instance.Get() && Instance.Get()->GetFractionEntity())
	//{
	//	return Instance.Get()->GetFractionEntity()->GetDescription().Name;
	//}

	return FText::GetEmpty();
}

FText SWeaponModularModule::GetFractionRank() const
{
	//TODO: SeNTIke
	//if (Instance.Get() && Instance.Get()->GetFractionEntity())
	//{
	//	return Instance.Get()->GetFractionEntity()->RankList[FMath::Clamp<int32>(Instance.Get()->GetLevel(), 0, Instance.Get()->GetFractionEntity()->RankList.Num() - 1)].Name;
	//}

	return FText::GetEmpty();
}