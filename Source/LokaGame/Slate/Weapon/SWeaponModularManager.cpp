// VRSPRO

#include "LokaGame.h"
#include "SWeaponModularManager.h"
#include "SlateOptMacros.h"

// Experimental
#include "WidgetLayoutLibrary.h"
//TODO: SeNTIke
//#include "Weapon/WeaponModular.h"
#include "Weapon/ItemWeaponEntity.h"
#include "Fraction/FractionEntity.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SWeaponModularManager::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;

	OnGetSocketScreen = InArgs._OnGetSocketScreen;
	OnSelectedModule = InArgs._OnSelectedModule;

	ChildSlot
	[
		SNew(SBorder)
		.BorderImage(new FSlateNoResource())
		.ColorAndOpacity(this, &SWeaponModularManager::GetContentOpacity)
		.Visibility(EVisibility::SelfHitTestInvisible)
		[
			SAssignNew(Layers, SCanvas)	
		]
	];

	AnimInstance = FCurveSequence();
	AnimHandle = AnimInstance.AddCurve(1.1f, 2.f, ECurveEaseFunction::QuadIn);

}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

FLinearColor SWeaponModularManager::GetContentOpacity() const
{
	return FLinearColor(1, 1, 1, FMath::Lerp(0.0f, 1.0f, AnimHandle.GetLerp()));
}

TSharedRef<ITableRow> SWeaponModularManager::OnGenerateModuleRow(UItemModuleEntity* Source, const TSharedRef<STableViewBase>& OwnerTable)
{
	return SNew(STableRow<UItemModuleEntity*>, OwnerTable);
	//TODO: SeNTike
	//return SNew(STableRow<UItemModuleEntity*>, OwnerTable)
	//		.IsEnabled((!Source->IsLocked() || (Source->GetFractionEntity() && Source->GetLevel() < Source->GetFractionEntity()->Progress.CurrentRank)))
	//		.Style(&Style->Module.Row)
	//		[
	//			SNew(SWeaponModularModule)
	//			.Instance(Source)
	//		];
}

void SWeaponModularManager::OnSelectionModuleRow(UItemModuleEntity* Source, ESelectInfo::Type Type)
{
	if (Type != ESelectInfo::Direct)
	{
		if (Lists.Contains(Source->TargetSocket))
		{
			auto &List = Lists.FindChecked(Source->TargetSocket);
			List->ClearSelection();
		}

		OnSelectedModule.ExecuteIfBound(Source);		
	}
}

void SWeaponModularManager::SetInstance(const UItemWeaponEntity* InInstance)
{
	Instance = InInstance;

	Layers->ClearChildren();
	LayersPosition.Empty();
	TargetPosition.Empty();
	CachedSlots.Empty();
	Lists.Empty();

	if (Instance)
	{
		for (auto name : Instance->SupportedSockets)
		{
			GenerateSocketModules(name);
		}

		AnimInstance.Play(this->AsShared());
	}
}

void SWeaponModularManager::GenerateSocketModules(const FName& Socket)
{
	if (!Instance->Modules.Find(Socket) || Socket == NAME_None) return;

	auto List = Lists.Add(Socket, SNew(SListView<UItemModuleEntity*>)
		.ListItemsSource(&Instance->Modules[Socket].Array)
		.OnGenerateRow(this, &SWeaponModularManager::OnGenerateModuleRow)
		.OnSelectionChanged(this, &SWeaponModularManager::OnSelectionModuleRow));

	SCanvas::FSlot* Slot = nullptr;

	const FSlateBrush* kEmpty = new FSlateNoResource();

	Layers->AddSlot().Expose(Slot)
	.HAlign(HAlign_Center)
	.Size(FVector2D(160, 80))
	.Position(TAttribute<FVector2D>::Create(TAttribute<FVector2D>::FGetter::CreateSP(this, &SWeaponModularManager::GetCurrentPositionFor, Socket)))
	[
		SNew(SBorder)
		.BorderImage(&Style->Module.EmptyBorder)
		.Padding(0)
		[
			SNew(SComboButton)
			.HasDownArrow(false)
			.ComboButtonStyle(&Style->Module.Button)
			.ButtonContent()
			[
				SNew(SWeaponModularModule)
				.Instance_Lambda([l = List.ToSharedRef()]() {
					if (l->GetSelectedItems().Num() && l->GetSelectedItems()[0])
					{
						return l->GetSelectedItems()[0];
					}
					return (UItemModuleEntity *)nullptr;
				})
			]
			.MenuContent()
			[
				List.ToSharedRef()
			]
		]
	];

	CachedSlots.Add(Socket, Slot);

	LayersPosition.Add(Socket);
	TargetPosition.Add(Socket);
}

FVector2D SWeaponModularManager::GetCurrentPositionFor(const FName i) const
{
	if (LayersPosition.Contains(i))
	{
		return LayersPosition[i];
	}

	return FVector2D::ZeroVector;
}

void SWeaponModularManager::Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime)
{
	static float Delta;
	Delta += InDeltaTime;
	FVector2D Center = AllottedGeometry.GetLocalSize() / 2;

	for (auto &s : CachedSlots)
	{
		TargetPosition[s.Key] = OnGetSocketScreen.Execute(s.Key) - FVector2D(0, 100);

		if (TargetPosition[s.Key].Y < Center.Y || TargetPosition[s.Key].X < (Center.X - Center.X / 2)) // Up
		{
			LayersPosition[s.Key] = TargetPosition[s.Key] - FVector2D(60, 120);
			CachedSlots[s.Key]->HAlign(HAlign_Right);
			CachedSlots[s.Key]->VAlign(VAlign_Bottom);
		}
		else // Down
		{
			LayersPosition[s.Key] = TargetPosition[s.Key] + FVector2D(60, 120);
			CachedSlots[s.Key]->HAlign(HAlign_Left);
			CachedSlots[s.Key]->VAlign(VAlign_Top);
		}
	}

	if (Delta > .4f)
	{
		Delta = .0f;

		if (ModularActor && Lists.Num())
		{
			//TODO: SeNTIke
			//for (auto &m : ModularActor->InstalledModules)
			//{
			//	if (Lists.Contains(m.Key))
			//	{
			//		auto TargetList = Lists.FindChecked(m.Key).ToSharedRef();					
			//
			//		if (TargetList->GetSelectedItems().Num() && TargetList->GetSelectedItems()[0])
			//		{
			//			if (!TargetList->IsItemSelected(m.Value) && !TargetList->IsHovered())
			//			{
			//				TargetList->SetSelection(m.Value, ESelectInfo::Direct);
			//			}
			//		}
			//		else
			//		{
			//			TargetList->SetSelection(m.Value, ESelectInfo::Direct);
			//		}
			//	}
			//}
		}
	}
}

int32 SWeaponModularManager::OnPaint(const FPaintArgs& Args, const FGeometry& AllottedGeometry, const FSlateRect& MyClippingRect, FSlateWindowElementList& OutDrawElements, int32 LayerId, const FWidgetStyle& InWidgetStyle, bool bParentEnabled) const
{
	for (auto s : CachedSlots)
	{		
		TArray<FVector2D> Lines;

		Lines.Add(TargetPosition[s.Key]);
		Lines.Add(LayersPosition[s.Key]);
		
		FSlateDrawElement::MakeLines(OutDrawElements, ++LayerId, AllottedGeometry.ToPaintGeometry(), Lines, ESlateDrawEffect::None, FColor::White.WithAlpha(FMath::Lerp(0, 128, AnimHandle.GetLerp())), true);
	}

	return SCompoundWidget::OnPaint(Args, AllottedGeometry, MyClippingRect, OutDrawElements, LayerId, InWidgetStyle, bParentEnabled);
}