// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "WindowsWidgetStyle.h"


FWindowsStyle::FWindowsStyle()
{
}

FWindowsStyle::~FWindowsStyle()
{
}

const FName FWindowsStyle::TypeName(TEXT("FWindowsStyle"));

const FWindowsStyle& FWindowsStyle::GetDefault()
{
	static FWindowsStyle Default;
	return Default;
}

void FWindowsStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

