// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "ContentEarlyNotifyWidgetStyle.generated.h"

USTRUCT()
struct FLokaTestingGraph
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere) FText		Target;
	UPROPERTY(EditAnywhere) FDateTime	OpenTime;
	UPROPERTY(EditAnywhere) FDateTime	CloseTime;

	FLokaTestingGraph() : Target(), OpenTime(), CloseTime() {}
	FLokaTestingGraph(const FDateTime& InOpenTime, const FDateTime& InCloseTime, const FText& InTarget = FText::GetEmpty()) : Target(InTarget), OpenTime(InOpenTime), CloseTime(InCloseTime) {}
};

/**
 * 
 */
USTRUCT()
struct LOKAGAME_API FContentEarlyNotifyStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)	FHeaderRowStyle		TableHeaderRow;
	UPROPERTY(Category = Appearance, EditAnywhere)	FTableRowStyle		TableRow;

	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle		VersionDescText;
	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle		VersionNumText;
	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle		TableHeaderRowText;
	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle		TableRowText;

	UPROPERTY(Category = Content, EditAnywhere)									FText	TinyDesc;
	UPROPERTY(Category = Content, EditAnywhere, meta = (MultiLine = true))		FText	ShortDesc;
	UPROPERTY(Category = Content, EditAnywhere, meta = (MultiLine = true))		FText	LongDesc;

	UPROPERTY(Category = Content, EditAnywhere)		TArray<FLokaTestingGraph>			TestTimes;

	FContentEarlyNotifyStyle();
	virtual ~FContentEarlyNotifyStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FContentEarlyNotifyStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UContentEarlyNotifyWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FContentEarlyNotifyStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
