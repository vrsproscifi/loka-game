// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "BackgroundBlurWidgetStyle.h"


FBackgroundBlurStyle::FBackgroundBlurStyle()
{
}

FBackgroundBlurStyle::~FBackgroundBlurStyle()
{
}

const FName FBackgroundBlurStyle::TypeName(TEXT("FBackgroundBlurStyle"));

const FBackgroundBlurStyle& FBackgroundBlurStyle::GetDefault()
{
	static FBackgroundBlurStyle Default;
	return Default;
}

void FBackgroundBlurStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

