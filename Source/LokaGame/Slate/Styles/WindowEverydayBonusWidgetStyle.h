// VRSPRO

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "WindowEverydayBonusWidgetStyle.generated.h"

UENUM()
namespace EWindowEverydayBonusBorder
{
	enum Type
	{
		Complete,
		Active,
		Next,
		End UMETA(Hidden)
	};
}

USTRUCT()
struct LOKAGAME_API FWindowEverydayBonusStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle DayText;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle DescText;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateBrush Background[EWindowEverydayBonusBorder::End];

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateBrush Border[EWindowEverydayBonusBorder::End];

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateBrush Shadow[EWindowEverydayBonusBorder::End];

	FWindowEverydayBonusStyle();
	virtual ~FWindowEverydayBonusStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FWindowEverydayBonusStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UWindowEverydayBonusWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FWindowEverydayBonusStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
