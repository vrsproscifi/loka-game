// VRSPRO

#include "LokaGame.h"
#include "WindowTournamentRulesWidgetStyle.h"


FWindowTournamentRulesStyle::FWindowTournamentRulesStyle()
{
}

FWindowTournamentRulesStyle::~FWindowTournamentRulesStyle()
{
}

const FName FWindowTournamentRulesStyle::TypeName(TEXT("FWindowTournamentRulesStyle"));

const FWindowTournamentRulesStyle& FWindowTournamentRulesStyle::GetDefault()
{
	static FWindowTournamentRulesStyle Default;
	return Default;
}

void FWindowTournamentRulesStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

