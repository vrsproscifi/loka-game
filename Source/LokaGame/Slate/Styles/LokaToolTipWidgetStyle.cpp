// VRSPRO

#include "LokaGame.h"
#include "LokaToolTipWidgetStyle.h"


FLokaToolTipStyle::FLokaToolTipStyle()
{
}

FLokaToolTipStyle::~FLokaToolTipStyle()
{
}

const FName FLokaToolTipStyle::TypeName(TEXT("FLokaToolTipStyle"));

const FLokaToolTipStyle& FLokaToolTipStyle::GetDefault()
{
	static FLokaToolTipStyle Default;
	return Default;
}

void FLokaToolTipStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

