// VRSPRO

#include "LokaGame.h"
#include "UpgradeWindowWidgetStyle.h"


FUpgradeWindowStyle::FUpgradeWindowStyle()
{
}

FUpgradeWindowStyle::~FUpgradeWindowStyle()
{
}

const FName FUpgradeWindowStyle::TypeName(TEXT("FUpgradeWindowStyle"));

const FUpgradeWindowStyle& FUpgradeWindowStyle::GetDefault()
{
	static FUpgradeWindowStyle Default;
	return Default;
}

void FUpgradeWindowStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

