// VRSPRO

#include "LokaGame.h"
#include "ContextMenuWidgetStyle.h"


FContextMenuStyle::FContextMenuStyle()
{
}

FContextMenuStyle::~FContextMenuStyle()
{
}

const FName FContextMenuStyle::TypeName(TEXT("FContextMenuStyle"));

const FContextMenuStyle& FContextMenuStyle::GetDefault()
{
	static FContextMenuStyle Default;
	return Default;
}

void FContextMenuStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
	OutBrushes.Add(&Background);
	OutBrushes.Add(&Icon);
	OutBrushes.Add(&Expand);
	OutBrushes.Add(&SubMenuIndicator);
	OutBrushes.Add(&Separator);
	Label.GetResources(OutBrushes);
	EditableText.GetResources(OutBrushes);
	Keybinding.GetResources(OutBrushes);
	Heading.GetResources(OutBrushes);
	CheckBox.GetResources(OutBrushes);
	Check.GetResources(OutBrushes);
	RadioButton.GetResources(OutBrushes);
	ToggleButton.GetResources(OutBrushes);
	Button.GetResources(OutBrushes);
	OutBrushes.Add(&ButtonChecked);
	OutBrushes.Add(&ButtonChecked_Hovered);
	OutBrushes.Add(&ButtonChecked_Pressed);
	OutBrushes.Add(&ButtonSubMenuOpen);
}

