// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SlateWidgetStyleContainerBase.h"
#include "WidgetTransform.h"
#include "GameHUDWidgetStyle.generated.h"

USTRUCT()
struct LOKAGAME_API FGameHUDStyle_Ability
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Ability, EditAnywhere)
	FSlateBrush MaterialObject;

	UPROPERTY(Category = Ability, EditAnywhere)
	FName MaterialParameter;

	UPROPERTY(Category = Ability, EditAnywhere)
	FTextBlockStyle Key;

	UPROPERTY(Category = Ability, EditAnywhere)
	FTextBlockStyle Name;

	UPROPERTY(Category = Ability, EditAnywhere)
	FLinearColor ReadyEmpty;

	UPROPERTY(Category = Ability, EditAnywhere)
	FLinearColor ReadyFull;

	UPROPERTY(Category = Ability, EditAnywhere)
	FLinearColor UseEmpty;

	UPROPERTY(Category = Ability, EditAnywhere)
	FLinearColor UseFull;

	UPROPERTY(Category = Ability, EditAnywhere)
	FSlateBrush TipBackground;

	UPROPERTY(Category = Ability, EditAnywhere)
	FTextBlockStyle TipText;
};

USTRUCT()
struct LOKAGAME_API FGameHUDStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	FGameHUDStyle();
	virtual ~FGameHUDStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FGameHUDStyle& GetDefault();

	UPROPERTY(Category = Ability, EditAnywhere)
		FGameHUDStyle_Ability Ability;

	UPROPERTY(Category = LiveBar, EditAnywhere)
		FSlateBrush ArmorBackground;

	UPROPERTY(Category = LiveBar, EditAnywhere)
		FSlateBrush HealthBackground;

	UPROPERTY(Category = LiveBar, EditAnywhere)
		FSlateBrush IconHealth;

	UPROPERTY(Category = LiveBar, EditAnywhere)
		FSlateBrush IconArmour;

	UPROPERTY(Category = LiveBar, EditAnywhere)
		FSlateBrush ProgressBackground;

	UPROPERTY(Category = LiveBar, EditAnywhere)
		FSlateBrush ProgressForeground;

	UPROPERTY(Category = WeaponBar, EditAnywhere)
		FSlateBrush WeaponBackgroundLarge;

	UPROPERTY(Category = WeaponBar, EditAnywhere)
		FSlateBrush WeaponBackgroundMedium;

	UPROPERTY(Category = WeaponBar, EditAnywhere)
		FSlateBrush WeaponBackgroundSmall;

	UPROPERTY(Category = WeaponBar, EditAnywhere)
		FSlateBrush WeaponGlowLarge;

	UPROPERTY(Category = WeaponBar, EditAnywhere)
		FSlateBrush WeaponGlowMedium;

	UPROPERTY(Category = WeaponBar, EditAnywhere)
		FSlateBrush WeaponGlowSmall;

	UPROPERTY(Category = LiveBar, EditAnywhere)
		FSlateColor ColorHealth;

	UPROPERTY(Category = LiveBar, EditAnywhere)
		FSlateColor ColorArmor;

	UPROPERTY(Category = LiveBar, EditAnywhere)
		FTextBlockStyle PercentText;

	UPROPERTY(Category = WeaponBar, EditAnywhere)
		FTextBlockStyle AmmoText;

	UPROPERTY(Category = Chat, EditAnywhere)
		FSlateBrush ChatBackground;

	UPROPERTY(Category = Chat, EditAnywhere)
		FSlateBrush ChatIcon;

	UPROPERTY(Category = Chat, EditAnywhere)
		FEditableTextBoxStyle ChatInput;

	UPROPERTY(Category = Chat, EditAnywhere)
		FButtonStyle ChatButton;

	UPROPERTY(Category = Chat, EditAnywhere)
		FScrollBoxStyle ChatContainer;

	UPROPERTY(Category = Chat, EditAnywhere)
		FScrollBarStyle ChatContainerScroll;

	UPROPERTY(Category = Chat, EditAnywhere)
		FTextBlockStyle ChatToggleStyle;

	UPROPERTY(Category = Chat, EditAnywhere)
		FCheckBoxStyle ChatToggleType;

	UPROPERTY(Category = Chat, EditAnywhere)
		FSlateFontInfo ChatPlayer;

	UPROPERTY(Category = Chat, EditAnywhere)
		FSlateFontInfo ChatText;

	UPROPERTY(Category = Chat, EditAnywhere)
		FSlateColor ColorMessageToAll;

	UPROPERTY(Category = RespawnTime, EditAnywhere)
		FSlateFontInfo RespawnTextFont;

	UPROPERTY(Category = RespawnTime, EditAnywhere)
		FSlateBrush RespawnTextBackground;

	UPROPERTY(Category = RespawnTime, EditAnywhere)
		FSlateColor RespawnInColor;

	UPROPERTY(Category = RespawnTime, EditAnywhere)
		FSlateColor RespawnCountColor;

	UPROPERTY(Category = Configuration, EditAnywhere)
		FVector2D HealthSpacer;

	UPROPERTY(Category = Configuration, EditAnywhere)
		FVector2D ArmorSpacer;

	UPROPERTY(Category = Configuration, EditAnywhere)
		FVector2D SecondBlockTranslation;

	UPROPERTY(Category = Configuration, EditAnywhere)
		FVector2D WeaponBlocksSize;

	UPROPERTY(Category = Configuration, EditAnywhere)
		float WeaponBlocksUpDownOffset;

	UPROPERTY(Category = Configuration, EditAnywhere)
		float WeaponBlocksLargeOffset;

	UPROPERTY(Category = Configuration, EditAnywhere)
		float WeaponBlocksMediumOffset;

	UPROPERTY(Category = Configuration, EditAnywhere)
		FVector2D TranslationLargeText;

	UPROPERTY(Category = Configuration, EditAnywhere)
		FVector2D TranslationMediumText;

	UPROPERTY(Category = Configuration, EditAnywhere)
		FVector2D TranslationSmallText;

	UPROPERTY(Category = Default, EditAnywhere)
		FSlateBrush WeaponLarge;

	UPROPERTY(Category = Default, EditAnywhere)
		FSlateBrush WeaponMedium;

	UPROPERTY(Category = Default, EditAnywhere)
		FSlateBrush WeaponSmall;

	UPROPERTY(Category = Default, EditAnywhere)
		FWidgetTransform LiveBarTransform;

	UPROPERTY(Category = Default, EditAnywhere)
		FVector2D LiveBarTransformPivot;

	UPROPERTY(Category = Default, EditAnywhere)
		FWidgetTransform WeaponBarTransform;

	UPROPERTY(Category = Default, EditAnywhere)
		FVector2D WeaponBarTransformPivot;

	UPROPERTY(Category = Default, EditAnywhere)
		FTextBlockStyle MedalText;
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UGameHUDWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FGameHUDStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
