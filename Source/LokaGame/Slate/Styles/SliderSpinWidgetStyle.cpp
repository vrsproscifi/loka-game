// VRSPRO

#include "LokaGame.h"
#include "SliderSpinWidgetStyle.h"


FSliderSpinStyle::FSliderSpinStyle()
{
}

FSliderSpinStyle::~FSliderSpinStyle()
{
}

const FName FSliderSpinStyle::TypeName(TEXT("FSliderSpinStyle"));

const FSliderSpinStyle& FSliderSpinStyle::GetDefault()
{
	static FSliderSpinStyle Default;
	return Default;
}

void FSliderSpinStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

