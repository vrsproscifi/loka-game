// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"
#include "SliderSpinWidgetStyle.h"
#include "VotingWidgetStyle.generated.h"

USTRUCT()
struct LOKAGAME_API FVotingStyle_Changes
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTableRowStyle Row;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FButtonStyle VoteButton;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle VoteTitle;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle VoteTimer;

	UPROPERTY(Category = Appearance, EditAnywhere)
	float RowHeightPadding;
};

USTRUCT()
struct LOKAGAME_API FVotingStyle_Proposals
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSliderSpinStyle SpinStyle;
};

USTRUCT()
struct LOKAGAME_API FVotingStyle_Details
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)
	float d;
};

USTRUCT()
struct LOKAGAME_API FVotingStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)
	FVotingStyle_Changes Changes;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FVotingStyle_Proposals Proposals;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FVotingStyle_Details Details;

	FVotingStyle();
	virtual ~FVotingStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FVotingStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UVotingWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FVotingStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
