// VRSPRO

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "DetailsBoxRowWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT()
struct LOKAGAME_API FDetailsBoxRowStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	FDetailsBoxRowStyle();
	virtual ~FDetailsBoxRowStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FDetailsBoxRowStyle& GetDefault();

	UPROPERTY(Category = Visual, EditAnywhere)
		FProgressBarStyle ProgressBar;

	UPROPERTY(Category = Visual, EditAnywhere)
		FSlateColor ColorUpgrade;

	UPROPERTY(Category = Visual, EditAnywhere)
		FSlateColor ColorDowngrade;

	UPROPERTY(Category = Visual, EditAnywhere)
		FSlateFontInfo FontName;

	UPROPERTY(Category = Visual, EditAnywhere)
		FSlateColor ColorFontName;

	UPROPERTY(Category = Visual, EditAnywhere)
		FSlateFontInfo FontValue;

	UPROPERTY(Category = Visual, EditAnywhere)
		FSlateColor ColorFontValue;

	UPROPERTY(Category = Configuration, EditAnywhere)
		FMargin MarginFontValue;
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UDetailsBoxRowWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FDetailsBoxRowStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
