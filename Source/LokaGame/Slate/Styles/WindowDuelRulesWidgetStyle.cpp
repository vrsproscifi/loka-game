// VRSPRO

#include "LokaGame.h"
#include "WindowDuelRulesWidgetStyle.h"


FWindowDuelRulesStyle::FWindowDuelRulesStyle()
{
}

FWindowDuelRulesStyle::~FWindowDuelRulesStyle()
{
}

const FName FWindowDuelRulesStyle::TypeName(TEXT("FWindowDuelRulesStyle"));

const FWindowDuelRulesStyle& FWindowDuelRulesStyle::GetDefault()
{
	static FWindowDuelRulesStyle Default;
	return Default;
}

void FWindowDuelRulesStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

