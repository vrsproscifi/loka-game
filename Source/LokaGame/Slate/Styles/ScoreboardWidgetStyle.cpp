// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "ScoreboardWidgetStyle.h"


FScoreboardStyle::FScoreboardStyle()
: DefaultKillRowShowTime(5.0f)
{
}

FScoreboardStyle::~FScoreboardStyle()
{
}

const FName FScoreboardStyle::TypeName(TEXT("LokaScoreboardStyle"));

const FScoreboardStyle& FScoreboardStyle::GetDefault()
{
	static FScoreboardStyle Default;
	return Default;
}

void FScoreboardStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	OutBrushes.Add(&LeftBackground);
	OutBrushes.Add(&RightBackground); 
	OutBrushes.Add(&LeftBackgroundEdge); 
	OutBrushes.Add(&RightBackgroundEdge);
	OutBrushes.Add(&LeftIcon);
	OutBrushes.Add(&RightIcon);
	OutBrushes.Add(&UpLine);
	OutBrushes.Add(&DownLine);

	OutBrushes.Add(&IconLeftDeaths);
	OutBrushes.Add(&IconLeftKills);
	OutBrushes.Add(&IconRightDeaths);
	OutBrushes.Add(&IconRightKills);
}
