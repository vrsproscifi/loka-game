// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "BuildSystemWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT()
struct FBuildSystemStyle_Inventory
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)	FSlateBrush		Background;
	UPROPERTY(Category = Appearance, EditAnywhere)	FSlateBrush		ItemBackground;
	UPROPERTY(Category = Appearance, EditAnywhere)	FButtonStyle	ItemBuyButton;
	UPROPERTY(Category = Appearance, EditAnywhere)	FButtonStyle	ItemSubButton;
	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle ItemName;
	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle ItemDesc;
	UPROPERTY(Category = Appearance, EditAnywhere)	FCheckBoxStyle	SortButton;
	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle SortButtonText;
	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle SortButtonTextSelected;
	UPROPERTY(Category = Appearance, EditAnywhere)	FCheckBoxStyle	SubSortButton;
	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle SubSortButtonText;
	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle SubSortButtonTextSelected;
};

USTRUCT()
struct LOKAGAME_API FBuildSystemStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle CountFont;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle BindingsTipFont;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle FontAwesome;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FBuildSystemStyle_Inventory Inventory;

	UPROPERTY(Category = Configuration, EditAnywhere)
	FVector CurrencySize;

	UPROPERTY(Category = Configuration, EditAnywhere)
	float InventoryHPadding;

	FBuildSystemStyle();
	virtual ~FBuildSystemStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FBuildSystemStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UBuildSystemWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FBuildSystemStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
