// VRSPRO

#include "LokaGame.h"
#include "WeaponModularManagerWidgetStyle.h"


FWeaponModularManagerStyle::FWeaponModularManagerStyle()
{
}

FWeaponModularManagerStyle::~FWeaponModularManagerStyle()
{
}

const FName FWeaponModularManagerStyle::TypeName(TEXT("FWeaponModularManagerStyle"));

const FWeaponModularManagerStyle& FWeaponModularManagerStyle::GetDefault()
{
	static FWeaponModularManagerStyle Default;
	return Default;
}

void FWeaponModularManagerStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

