// VRSPRO

#include "LokaGame.h"
#include "FractionManagerWidgetStyle.h"


FFractionManagerStyle::FFractionManagerStyle()
{
}

FFractionManagerStyle::~FFractionManagerStyle()
{
}

const FName FFractionManagerStyle::TypeName(TEXT("FFractionManagerStyle"));

const FFractionManagerStyle& FFractionManagerStyle::GetDefault()
{
	static FFractionManagerStyle Default;
	return Default;
}

void FFractionManagerStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

