// VRSPRO

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"
#include "TextBlockButtonWidgetStyle.h"
#include "LokaSearchBoxWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT()
struct LOKAGAME_API FLokaSearchBoxStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockButtonStyle Glass;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockButtonStyle Cancel;

	FLokaSearchBoxStyle();
	virtual ~FLokaSearchBoxStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FLokaSearchBoxStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class ULokaSearchBoxWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FLokaSearchBoxStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
