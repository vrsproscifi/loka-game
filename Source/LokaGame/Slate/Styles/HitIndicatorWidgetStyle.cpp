// VRSPRO

#include "LokaGame.h"
#include "HitIndicatorWidgetStyle.h"


FHitIndicatorStyle::FHitIndicatorStyle()
{
}

FHitIndicatorStyle::~FHitIndicatorStyle()
{
}

const FName FHitIndicatorStyle::TypeName(TEXT("FHitIndicatorStyle"));

const FHitIndicatorStyle& FHitIndicatorStyle::GetDefault()
{
	static FHitIndicatorStyle Default;
	return Default;
}

void FHitIndicatorStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

