// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "PlayerThumbnailWidgetStyle.h"


FPlayerThumbnailStyle::FPlayerThumbnailStyle()
{
}

FPlayerThumbnailStyle::~FPlayerThumbnailStyle()
{
}

const FName FPlayerThumbnailStyle::TypeName(TEXT("FPlayerThumbnailStyle"));

const FPlayerThumbnailStyle& FPlayerThumbnailStyle::GetDefault()
{
	static FPlayerThumbnailStyle Default;
	return Default;
}

void FPlayerThumbnailStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

