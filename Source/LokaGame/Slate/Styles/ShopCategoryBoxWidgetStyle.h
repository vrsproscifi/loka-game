// VRSPRO

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"
#include "StyleHelper.h"
#include "TextBlockButtonWidgetStyle.h"
#include "ShopCategoryBoxWidgetStyle.generated.h"

USTRUCT()
struct LOKAGAME_API FShopCategoryBoxStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	FShopCategoryBoxStyle();
	virtual ~FShopCategoryBoxStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FShopCategoryBoxStyle& GetDefault();

	UPROPERTY(Category = Visual, EditAnywhere)
		FTextBlockButtonStyle TypeButton[EButtonState::End];

	UPROPERTY(Category = Visual, EditAnywhere)
		FButtonStyle CategoryButton[EButtonState::End];

	//UPROPERTY(Category = Visual, EditAnywhere)
	//	FSlateFontInfo TypeFont;
	//
	//UPROPERTY(Category = Visual, EditAnywhere)
	//	FSlateColor TypeFontColor;

	UPROPERTY(Category = Visual, EditAnywhere)
		FSlateFontInfo CategoryFont;

	UPROPERTY(Category = Visual, EditAnywhere)
		FSlateColor CategoryFontColor;

	UPROPERTY(Category = Visual, EditAnywhere)
		FSlateBrush CategoryBackground;

	UPROPERTY(Category = Configuration, EditAnywhere)
		FMargin TypeTextMargin;

	UPROPERTY(Category = Configuration, EditAnywhere)
		FMargin CategoryTextMargin;

	UPROPERTY(Category = Configuration, EditAnywhere)
		FVector2D Test;
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UShopCategoryBoxWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FShopCategoryBoxStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
