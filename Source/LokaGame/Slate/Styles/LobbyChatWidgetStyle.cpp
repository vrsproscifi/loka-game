// VRSPRO

#include "LokaGame.h"
#include "LobbyChatWidgetStyle.h"


FLobbyChatStyle::FLobbyChatStyle()
{
}

FLobbyChatStyle::~FLobbyChatStyle()
{
}

const FName FLobbyChatStyle::TypeName(TEXT("FLobbyChatStyle"));

const FLobbyChatStyle& FLobbyChatStyle::GetDefault()
{
	static FLobbyChatStyle Default;
	return Default;
}

void FLobbyChatStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

