// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "WindowDuelsWidgetStyle.h"


FWindowDuelsStyle::FWindowDuelsStyle()
{
}

FWindowDuelsStyle::~FWindowDuelsStyle()
{
}

const FName FWindowDuelsStyle::TypeName(TEXT("FWindowDuelsStyle"));

const FWindowDuelsStyle& FWindowDuelsStyle::GetDefault()
{
	static FWindowDuelsStyle Default;
	return Default;
}

void FWindowDuelsStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

