// VRSPRO

#include "LokaGame.h"
#include "LokaSearchBoxWidgetStyle.h"


FLokaSearchBoxStyle::FLokaSearchBoxStyle()
{
}

FLokaSearchBoxStyle::~FLokaSearchBoxStyle()
{
}

const FName FLokaSearchBoxStyle::TypeName(TEXT("FLokaSearchBoxStyle"));

const FLokaSearchBoxStyle& FLokaSearchBoxStyle::GetDefault()
{
	static FLokaSearchBoxStyle Default;
	return Default;
}

void FLokaSearchBoxStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

