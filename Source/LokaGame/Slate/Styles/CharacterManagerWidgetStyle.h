// VRSPRO

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"
#include "TextBlockButtonWidgetStyle.h"
#include "AdvanceTextBoxWidgetStyle.h"
#include "CharacterManagerWidgetStyle.generated.h"

USTRUCT(Blueprintable)
struct FCharacterManagerStyle_Modify
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FSlateBrush Background;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FSlateBrush Border;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FButtonStyle ButtonBack;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTextBlockStyle ButtonBackFont;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FComboButtonStyle ButtonTextures;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTextBlockStyle Header;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FButtonStyle ModiferBack;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTextBlockStyle ModiferName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTextBlockStyle ModiferValue;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FButtonStyle ModiferButtonChange;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FButtonStyle ModiferButtonCancel;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FButtonStyle ModiferButtonAccept;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FButtonStyle ModiferButtonMoney;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FButtonStyle ModiferButtonDonate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTextBlockStyle LuckText;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTextBlockStyle LockedText;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTextBlockStyle LockedSymbol;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTableRowStyle TileStyle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FButtonStyle SkinButtonBuy;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTextBlockStyle SkinTextBuy;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FButtonStyle SkinButtonCancel;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTextBlockStyle SkinTextCancel;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTextBlockStyle SkinTextName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FSlateSound SoundOpen;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FSlateSound SoundClose;
};

USTRUCT(Blueprintable)
struct FCharacterManagerStyle_Select
{
	GENERATED_USTRUCT_BODY()
		
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FButtonStyle Button;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTextBlockStyle ButtonText;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FButtonStyle BigButtons;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTextBlockStyle BigButtonsText;
};

USTRUCT(Blueprintable)
struct FCharacterManagerStyle_Lock
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTextBlockStyle HeadText;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTextBlockStyle DescText;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FButtonStyle Button;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTextBlockStyle ButtonText;
};

USTRUCT(Blueprintable)
struct FCharacterManagerStyle_Slot
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FComboButtonStyle Button;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTextBlockStyle Text;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FSlateBrush AllowDrop;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FSlateBrush DisallowDrop;
};

USTRUCT(Blueprintable)
struct FCharacterManagerStyle_Asset
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FSpinBoxStyle SpinBox;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FCheckBoxStyle CheckBox;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FButtonStyle Button;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTextBlockStyle CheckBox_Text;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTextBlockStyle Name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTextBlockStyle ParameterName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTextBlockStyle ParameterValue;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FSlateBrush Background;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FSlateBrush IconHealth;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FSlateBrush IconArmour;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FSlateBrush IconPower;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FSlateBrush IconSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FSlateBrush IconEnergy;
};

USTRUCT(Blueprintable)
struct FCharacterManagerStyle_Assets
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FAdvanceTextBoxStyle Input;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FSlateBrush Header;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FCheckBoxStyle Button;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTextBlockStyle Number;
};

USTRUCT(Blueprintable)
struct FCharacterManagerStyle_Item
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FButtonStyle Button;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTextBlockStyle Name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTextBlockStyle Level;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTextBlockButtonStyle Upgrade;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FSlateBrush TextBackground;
};

USTRUCT(Blueprintable)
struct FCharacterManagerStyle_List
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FScrollBoxStyle ScrollBox;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FScrollBarStyle ScrollBar;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FSlateBrush Background;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FSlateBrush Border;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FComboButtonStyle SortButton;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTableRowStyle SortRow;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTextBlockStyle SortText;
};


/**
 * 
 */
USTRUCT(BlueprintType)
struct LOKAGAME_API FCharacterManagerStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)
	FCharacterManagerStyle_Item Item;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FCharacterManagerStyle_List List;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FCharacterManagerStyle_Assets Assets;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FCharacterManagerStyle_Asset Asset;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FCharacterManagerStyle_Slot Slot;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FCharacterManagerStyle_Lock Lock;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FCharacterManagerStyle_Select Select;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FCharacterManagerStyle_Modify Modify;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTextBlockStyle Headers;

	FCharacterManagerStyle();
	virtual ~FCharacterManagerStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FCharacterManagerStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UCharacterManagerWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FCharacterManagerStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
