// VRSPRO

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "WeaponModularManagerWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT()
struct LOKAGAME_API FWeaponModularManagerStyle_Module
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateBrush EmptyBorder;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle EmptyName;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTableRowStyle Row;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle Name;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FComboButtonStyle Button;
};

USTRUCT()
struct LOKAGAME_API FWeaponModularManagerStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)
	FWeaponModularManagerStyle_Module Module;

	FWeaponModularManagerStyle();
	virtual ~FWeaponModularManagerStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FWeaponModularManagerStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UWeaponModularManagerWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FWeaponModularManagerStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
