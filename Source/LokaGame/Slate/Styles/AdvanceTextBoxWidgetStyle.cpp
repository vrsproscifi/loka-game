// VRSPRO

#include "LokaGame.h"
#include "AdvanceTextBoxWidgetStyle.h"


FAdvanceTextBoxStyle::FAdvanceTextBoxStyle()
{
}

FAdvanceTextBoxStyle::~FAdvanceTextBoxStyle()
{
}

const FName FAdvanceTextBoxStyle::TypeName(TEXT("FAdvanceTextBoxStyle"));

const FAdvanceTextBoxStyle& FAdvanceTextBoxStyle::GetDefault()
{
	static FAdvanceTextBoxStyle Default;
	return Default;
}

void FAdvanceTextBoxStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

