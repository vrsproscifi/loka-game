// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SlateWidgetStyleContainerBase.h"
#include "GameTimerWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT()
struct LOKAGAME_API FGameTimerStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	FGameTimerStyle();
	virtual ~FGameTimerStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FGameTimerStyle& GetDefault();

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateBrush ScoreBorder;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateBrush ScoreEdge;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FProgressBarStyle ScoreProgress;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateBrush TimeBorder;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FProgressBarStyle TimeProgress;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FTextBlockStyle ScoreFont;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateColor LeftScoreColor;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateColor RightScoreColor;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FTextBlockStyle TimeFont;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FTextBlockStyle SymbolFont;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FTextBlockStyle LastHeroText;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FTextBlockStyle LastHeroNumber;
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UGameTimerWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FGameTimerStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
