// VRSPRO

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"
#include "TextBlockButtonWidgetStyle.h"
#include "FriendsFormWidgetStyle.generated.h"

USTRUCT()
struct LOKAGAME_API FFriendsFormStyle_Squad
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle Member;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle Local;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTableRowStyle TableRow;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FHeaderRowStyle HeaderRow;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockButtonStyle Remove;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockButtonStyle ModeAllow;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockButtonStyle ModeDisallow;
};

USTRUCT()
struct LOKAGAME_API FFriendsFormStyle_List
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)	FButtonStyle			Button;
	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle			Name;
	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockStyle			Status;

	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockButtonStyle	BigHeader;
	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockButtonStyle	MiddleHeader;
	UPROPERTY(Category = Appearance, EditAnywhere)	FTextBlockButtonStyle	SmallHrader;
};

USTRUCT()
struct LOKAGAME_API FFriendsFormStyle_Status
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)	FSlateBrush Offline;
	UPROPERTY(Category = Appearance, EditAnywhere)	FSlateBrush Online;
	UPROPERTY(Category = Appearance, EditAnywhere)	FSlateBrush Waiting;
	UPROPERTY(Category = Appearance, EditAnywhere)	FSlateBrush Playing;
	UPROPERTY(Category = Appearance, EditAnywhere)	FSlateBrush Building;
};

USTRUCT()
struct LOKAGAME_API FFriendsFormStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	FFriendsFormStyle();
	virtual ~FFriendsFormStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FFriendsFormStyle& GetDefault();

	UPROPERTY(Category = Appearance, EditAnywhere)
	FFriendsFormStyle_Status Status;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FFriendsFormStyle_List List;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSearchBoxStyle SearchBox;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle Header;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FFriendsFormStyle_Squad Squad;
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UFriendsFormWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FFriendsFormStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
