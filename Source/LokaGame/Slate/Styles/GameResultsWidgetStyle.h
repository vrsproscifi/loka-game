// VRSPRO

#pragma once

#include "SlateWidgetStyleContainerBase.h"
#include "WindowBackgroundWidgetStyle.h"
#include "GameResultsWidgetStyle.generated.h"

UENUM()
namespace EEStyleTeam
{
	enum Type
	{
		Friendly,
		Enemy,
		Neutral,
		End
	};
}

typedef EEStyleTeam::Type EStyleTeam;

USTRUCT(BlueprintType)
struct LOKAGAME_API FGRPTips
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere, BlueprintReadWrite)
		FSlateBrush Background;

	UPROPERTY(Category = Appearance, EditAnywhere, BlueprintReadWrite)
		FSlateBrush Border;

	UPROPERTY(Category = Appearance, EditAnywhere, BlueprintReadWrite)
		FTextBlockStyle TextStyle;

	UPROPERTY(Category = Appearance, EditAnywhere, BlueprintReadWrite)
		FButtonStyle LeftButton;

	UPROPERTY(Category = Appearance, EditAnywhere, BlueprintReadWrite)
		FButtonStyle RightButton;

	UPROPERTY(Category = Appearance, EditAnywhere, BlueprintReadWrite)
		FTextBlockStyle Awesome;

	UPROPERTY(Category = Appearance, EditAnywhere, BlueprintReadWrite)
		FWindowBackgroundStyle WindowBackground;
};

USTRUCT(BlueprintType)
struct LOKAGAME_API FGRPlayersLists
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere, BlueprintReadWrite)
		FSlateBrush UpLine;

	UPROPERTY(Category = Appearance, EditAnywhere, BlueprintReadWrite)
		FSlateBrush Background;

	UPROPERTY(Category = Appearance, EditAnywhere, BlueprintReadWrite)
		FTextBlockStyle TextStyle;

	UPROPERTY(Category = Appearance, EditAnywhere, BlueprintReadWrite)
		FMargin Padding;
};

USTRUCT(BlueprintType)
struct LOKAGAME_API FGRColumnsSize
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Configuration, EditAnywhere, BlueprintReadWrite)
		float Name;

	UPROPERTY(Category = Configuration, EditAnywhere, BlueprintReadWrite)
		float Kills;

	UPROPERTY(Category = Configuration, EditAnywhere, BlueprintReadWrite)
		float Deaths;

	UPROPERTY(Category = Configuration, EditAnywhere, BlueprintReadWrite)
		float Score;

	UPROPERTY(Category = Configuration, EditAnywhere, BlueprintReadWrite)
		float PlusMoney;

	UPROPERTY(Category = Configuration, EditAnywhere, BlueprintReadWrite)
		float PlusExp;
};

USTRUCT(BlueprintType)
struct LOKAGAME_API FGRTeamStatus
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateBrush Background;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateBrush Background2;

	UPROPERTY(Category = Appearance, EditAnywhere)
		float Background2Width;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateBrush Image[EEStyleTeam::End];

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateColor Color[EEStyleTeam::End];

	UPROPERTY(Category = Appearance, EditAnywhere)
		FTextBlockStyle TextStyle;

	UPROPERTY(Category = Configuration, EditAnywhere)
		FVector2D HeightAndRightOffset;

	UPROPERTY(Category = Sound, EditAnywhere)
		FSlateSound Sound[EEStyleTeam::End];
};

USTRUCT(BlueprintType)
struct LOKAGAME_API FGRAchievements
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateBrush Line;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FMargin LinePadding;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateBrush Background;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FTextBlockStyle TextStyle;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FMargin Padding;

	UPROPERTY(Category = Appearance, EditAnywhere)
		float BoxHeight;
};

USTRUCT(BlueprintType)
struct LOKAGAME_API FGRAchievement
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateBrush Icon;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FTextBlockStyle TextStyle;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FString Tag;
};

USTRUCT()
struct LOKAGAME_API FGameResultsStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	FGameResultsStyle();
	virtual ~FGameResultsStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FGameResultsStyle& GetDefault();
	
	//EStyleTeam

	UPROPERTY(Category = Appearance, EditAnywhere)
		FTableRowStyle TableRow;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FHeaderRowStyle TableHeader;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FGRTeamStatus TeamStatus;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FGRAchievements Achievements;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FGRPlayersLists TeamStyle;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateColor TeamColor[EEStyleTeam::End];

	UPROPERTY(Category = Appearance, EditAnywhere)
		FTextBlockStyle TeamTextStyle[EEStyleTeam::End];

	UPROPERTY(Category = Appearance, EditAnywhere)
		FTextBlockStyle TeamTextStyleLocal;

	UPROPERTY(Category = Configuration, EditAnywhere)
		FGRColumnsSize ColumnsSize;

	UPROPERTY(Category = Configuration, EditAnywhere)
		FMargin ContentPadding;

	UPROPERTY(Category = Configuration, EditAnywhere)
		FMargin TeamStyleTextPadding;

	UPROPERTY(Category = Configuration, EditAnywhere)
		TArray<FGRAchievement> AchievementsArray;

	UPROPERTY(Category = Configuration, EditAnywhere)
		FGRPTips Tips;
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UGameResultsWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FGameResultsStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
