// VRSPRO

#include "LokaGame.h"
#include "ShopCategoryBoxWidgetStyle.h"


FShopCategoryBoxStyle::FShopCategoryBoxStyle()
{
}

FShopCategoryBoxStyle::~FShopCategoryBoxStyle()
{
}

const FName FShopCategoryBoxStyle::TypeName(TEXT("ShopCategoryBoxStyle"));

const FShopCategoryBoxStyle& FShopCategoryBoxStyle::GetDefault()
{
	static FShopCategoryBoxStyle Default;
	return Default;
}

void FShopCategoryBoxStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

