// VRSPRO

#include "LokaGame.h"
#include "AuthFormWidgetStyle.h"


FAuthFormStyle::FAuthFormStyle()
{
}

FAuthFormStyle::~FAuthFormStyle()
{
}

const FName FAuthFormStyle::TypeName(TEXT("FAuthFormStyle"));

const FAuthFormStyle& FAuthFormStyle::GetDefault()
{
	static FAuthFormStyle Default;
	return Default;
}

void FAuthFormStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

