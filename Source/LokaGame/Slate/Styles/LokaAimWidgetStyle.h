// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SlateWidgetStyleContainerBase.h"
#include "LokaAimWidgetStyle.generated.h"

USTRUCT()
struct LOKAGAME_API FLokaAimStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	FLokaAimStyle();
	virtual ~FLokaAimStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FLokaAimStyle& GetDefault();

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateBrush Brush;

	UPROPERTY(Category = Configuration, EditAnywhere)
		FVector2D Size;

	//* X - Defoult, Y - Maximum
	UPROPERTY(Category = Configuration, EditAnywhere)
		FVector2D Offsets;

	UPROPERTY(Category = Configuration, EditAnywhere)
		float BaseAngle;

	UPROPERTY(Category = Configuration, EditAnywhere)
		bool IsValid;
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class ULokaAimWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FLokaAimStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
