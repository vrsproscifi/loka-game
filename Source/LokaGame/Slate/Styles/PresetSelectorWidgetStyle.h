// VRSPRO

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "PresetSelectorWidgetStyle.generated.h"

USTRUCT()
struct FPresetSelectorStyle_Item
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateBrush Background;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateBrush BackgroundSelected;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateBrush Border;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateBrush BorderSelected;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateFontInfo TextName;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateColor TextNameUnselected;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateColor TextNameSelected;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateBrush LockedImage;
		
};

USTRUCT()
struct LOKAGAME_API FPresetSelectorStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere)
	FPresetSelectorStyle_Item Item;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateBrush Background;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle TipText;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FTextBlockStyle TipModeText;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateBrush TipModeBackground;

	UPROPERTY(Category = Appearance, EditAnywhere)
	FSlateSound SwitchSound;

	FPresetSelectorStyle();
	virtual ~FPresetSelectorStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FPresetSelectorStyle& GetDefault();
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UPresetSelectorWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FPresetSelectorStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
