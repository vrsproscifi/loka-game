// VRSPRO

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "ShopFormItemWidgetStyle.generated.h"

USTRUCT(BlueprintType)
struct LOKAGAME_API FSFIBackground
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Visual, EditAnywhere)
		FSlateBrush Background;

	UPROPERTY(Category = Visual, EditAnywhere)
		FSlateBrush Shadow;
};

USTRUCT(BlueprintType)
struct LOKAGAME_API FSFIButtons
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Visual, EditAnywhere)
		FButtonStyle ItemBackground;

	UPROPERTY(Category = Visual, EditAnywhere)
		FButtonStyle ItemBuy;
};

USTRUCT(BlueprintType)
struct LOKAGAME_API FSFIFonts
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Visual, EditAnywhere)
		FSlateFontInfo TitleFont;

	UPROPERTY(Category = Visual, EditAnywhere)
		FSlateColor TitleColor;

	UPROPERTY(Category = Visual, EditAnywhere)
		FSlateFontInfo PriceFont;

	UPROPERTY(Category = Visual, EditAnywhere)
		FSlateFontInfo BuyFont;

	UPROPERTY(Category = Visual, EditAnywhere)
		FSlateColor BuyFontColorAllow;

	UPROPERTY(Category = Visual, EditAnywhere)
		FSlateColor BuyFontColorTime;

	UPROPERTY(Category = Visual, EditAnywhere)
		FSlateColor BuyFontColorDisallow;
	
};

USTRUCT()
struct LOKAGAME_API FShopFormItemStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	FShopFormItemStyle();
	virtual ~FShopFormItemStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FShopFormItemStyle& GetDefault();
		
	UPROPERTY(Category = Visual, EditAnywhere)
		FSFIBackground Back;

	UPROPERTY(Category = Visual, EditAnywhere)
		FSFIButtons Buttons;

	UPROPERTY(Category = Visual, EditAnywhere)
		FSFIFonts Texts;

	UPROPERTY(Category = Configuration, EditAnywhere)
		FVector2D Size;

	UPROPERTY(Category = Default, EditAnywhere)
		FSlateBrush DefaultIcon;

	UPROPERTY(Category = Default, EditAnywhere)
		FSlateBrush DefaultIcon1;

	UPROPERTY(Category = Default, EditAnywhere)
		FTextBlockStyle AwesomeFont;

};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UShopFormItemWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FShopFormItemStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
