// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "GameHUDWidgetStyle.h"


FGameHUDStyle::FGameHUDStyle()
{
}

FGameHUDStyle::~FGameHUDStyle()
{
}

const FName FGameHUDStyle::TypeName(TEXT("GameHUDStyle"));

const FGameHUDStyle& FGameHUDStyle::GetDefault()
{
	static FGameHUDStyle Default;
	return Default;
}

void FGameHUDStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	OutBrushes.Add(&ArmorBackground);
	OutBrushes.Add(&HealthBackground);
	OutBrushes.Add(&IconHealth);
	OutBrushes.Add(&IconArmour);
	OutBrushes.Add(&ProgressBackground);
	OutBrushes.Add(&ProgressForeground);

	OutBrushes.Add(&WeaponBackgroundLarge);
	OutBrushes.Add(&WeaponBackgroundMedium);
	OutBrushes.Add(&WeaponBackgroundSmall);

	OutBrushes.Add(&WeaponGlowLarge);
	OutBrushes.Add(&WeaponGlowMedium);
	OutBrushes.Add(&WeaponGlowSmall);

	OutBrushes.Add(&ChatBackground);
	OutBrushes.Add(&ChatIcon);

	ChatInput.GetResources(OutBrushes);
	ChatButton.GetResources(OutBrushes);
	ChatContainer.GetResources(OutBrushes);
	ChatContainerScroll.GetResources(OutBrushes);
	ChatToggleType.GetResources(OutBrushes);
}
