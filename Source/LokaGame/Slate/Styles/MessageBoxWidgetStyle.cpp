// VRSPRO

#include "LokaGame.h"
#include "MessageBoxWidgetStyle.h"


FMessageBoxStyle::FMessageBoxStyle()
{
}

FMessageBoxStyle::~FMessageBoxStyle()
{
}

const FName FMessageBoxStyle::TypeName(TEXT("FMessageBoxStyle"));

const FMessageBoxStyle& FMessageBoxStyle::GetDefault()
{
	static FMessageBoxStyle Default;
	return Default;
}

void FMessageBoxStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

