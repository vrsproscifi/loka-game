// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "ContentEarlyNotifyWidgetStyle.h"


FContentEarlyNotifyStyle::FContentEarlyNotifyStyle()
{
}

FContentEarlyNotifyStyle::~FContentEarlyNotifyStyle()
{
}

const FName FContentEarlyNotifyStyle::TypeName(TEXT("FContentEarlyNotifyStyle"));

const FContentEarlyNotifyStyle& FContentEarlyNotifyStyle::GetDefault()
{
	static FContentEarlyNotifyStyle Default;
	return Default;
}

void FContentEarlyNotifyStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

