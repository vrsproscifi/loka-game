// VRSPRO

#include "LokaGame.h"
#include "MovableWidgetStyle.h"


FMovableStyle::FMovableStyle()
{
}

FMovableStyle::~FMovableStyle()
{
}

const FName FMovableStyle::TypeName(TEXT("FMovableStyle"));

const FMovableStyle& FMovableStyle::GetDefault()
{
	static FMovableStyle Default;
	return Default;
}

void FMovableStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

