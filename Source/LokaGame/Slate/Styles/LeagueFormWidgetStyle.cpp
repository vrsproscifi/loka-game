// VRSPRO

#include "LokaGame.h"
#include "LeagueFormWidgetStyle.h"


FLeagueFormStyle::FLeagueFormStyle()
{
}

FLeagueFormStyle::~FLeagueFormStyle()
{
}

const FName FLeagueFormStyle::TypeName(TEXT("FLeagueFormStyle"));

const FLeagueFormStyle& FLeagueFormStyle::GetDefault()
{
	static FLeagueFormStyle Default;
	return Default;
}

void FLeagueFormStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

