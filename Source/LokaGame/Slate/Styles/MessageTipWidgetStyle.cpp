// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "MessageTipWidgetStyle.h"


FMessageTipStyle::FMessageTipStyle()
{
}

FMessageTipStyle::~FMessageTipStyle()
{
}

const FName FMessageTipStyle::TypeName(TEXT("FMessageTipStyle"));

const FMessageTipStyle& FMessageTipStyle::GetDefault()
{
	static FMessageTipStyle Default;
	return Default;
}

void FMessageTipStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

