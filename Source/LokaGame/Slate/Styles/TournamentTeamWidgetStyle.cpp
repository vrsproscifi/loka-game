// VRSPRO

#include "LokaGame.h"
#include "TournamentTeamWidgetStyle.h"


FTournamentTeamStyle::FTournamentTeamStyle()
{
}

FTournamentTeamStyle::~FTournamentTeamStyle()
{
}

const FName FTournamentTeamStyle::TypeName(TEXT("FTournamentTeamStyle"));

const FTournamentTeamStyle& FTournamentTeamStyle::GetDefault()
{
	static FTournamentTeamStyle Default;
	return Default;
}

void FTournamentTeamStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

