// VRSPRO

#include "LokaGame.h"
#include "WindowBackgroundWidgetStyle.h"


FWindowBackgroundStyle::FWindowBackgroundStyle()
{
}

FWindowBackgroundStyle::~FWindowBackgroundStyle()
{
}

const FName FWindowBackgroundStyle::TypeName(TEXT("FWindowBackgroundStyle"));

const FWindowBackgroundStyle& FWindowBackgroundStyle::GetDefault()
{
	static FWindowBackgroundStyle Default;
	return Default;
}

void FWindowBackgroundStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

