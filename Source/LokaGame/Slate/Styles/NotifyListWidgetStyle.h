// VRSPRO

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "NotifyListWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT()
struct LOKAGAME_API FNotifyListStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	FNotifyListStyle();
	virtual ~FNotifyListStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FNotifyListStyle& GetDefault();

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateBrush Background;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FSlateBrush Border;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FScrollBarStyle ScrollBar;

	UPROPERTY(Category = Notify, EditAnywhere)
		FSlateBrush NotifyBackground;

	UPROPERTY(Category = Notify, EditAnywhere)
		FTextBlockStyle NotifyTitle;

	UPROPERTY(Category = Notify, EditAnywhere)
		FTextBlockStyle NotifyContent;

	UPROPERTY(Category = Notify, EditAnywhere)
		FSlateSound NotifySound;
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UNotifyListWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FNotifyListStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
