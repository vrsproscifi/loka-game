// VRSPRO

#include "LokaGame.h"
#include "SettingsManagerWidgetStyle.h"


FSettingsManagerStyle::FSettingsManagerStyle()
{
}

FSettingsManagerStyle::~FSettingsManagerStyle()
{
}

const FName FSettingsManagerStyle::TypeName(TEXT("FSettingsManagerStyle"));

const FSettingsManagerStyle& FSettingsManagerStyle::GetDefault()
{
	static FSettingsManagerStyle Default;
	return Default;
}

void FSettingsManagerStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

