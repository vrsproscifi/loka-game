// VRSPRO

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "LoadingScreenWidgetStyle.generated.h"

USTRUCT(BlueprintType)
struct LOKAGAME_API FLSFonts
{
	GENERATED_USTRUCT_BODY()
	
	UPROPERTY(Category = Appearance, EditAnywhere, BlueprintReadWrite)
		FTextBlockStyle Title;

	UPROPERTY(Category = Appearance, EditAnywhere, BlueprintReadWrite)
		FTextBlockStyle Message;
};


USTRUCT(BlueprintType)
struct LOKAGAME_API FLSAnimated
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Appearance, EditAnywhere, BlueprintReadWrite)
		TArray<FSlateBrush> Sprites;

	UPROPERTY(Category = Appearance, EditAnywhere, BlueprintReadWrite)
		FSlateBrush Background;

	UPROPERTY(Category = Appearance, EditAnywhere, BlueprintReadWrite)
		FSlateBrush Rotator;
};

USTRUCT()
struct LOKAGAME_API FLoadingScreenStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	FLoadingScreenStyle();
	virtual ~FLoadingScreenStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FLoadingScreenStyle& GetDefault();

	UPROPERTY(Category = Appearance, EditAnywhere)
		FLSAnimated Animated;

	UPROPERTY(Category = Configuration, EditAnywhere)
		FLSFonts Fonts;

	UPROPERTY(Category = Default, EditAnywhere)
		TArray<FSlateBrush> Images;

	UPROPERTY(Category = Default, EditAnywhere)
		TArray<FText> Tips;
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class ULoadingScreenWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FLoadingScreenStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
