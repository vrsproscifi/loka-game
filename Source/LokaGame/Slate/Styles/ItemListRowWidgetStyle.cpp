// VRSPRO

#include "LokaGame.h"
#include "ItemListRowWidgetStyle.h"


FItemListRowStyle::FItemListRowStyle()
{
}

FItemListRowStyle::~FItemListRowStyle()
{
}

const FName FItemListRowStyle::TypeName(TEXT("FItemListRowStyle"));

const FItemListRowStyle& FItemListRowStyle::GetDefault()
{
	static FItemListRowStyle Default;
	return Default;
}

void FItemListRowStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

