// VRSPRO

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"
#include "StyleHelper.h"
#include "ItemListRowWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT()
struct LOKAGAME_API FItemListRowStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	FItemListRowStyle();
	virtual ~FItemListRowStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FItemListRowStyle& GetDefault();

	UPROPERTY(Category = Visual, EditAnywhere)
		FSlateBrush Background;

	UPROPERTY(Category = Visual, EditAnywhere)
		FSlateBrush Border;

	UPROPERTY(Category = Visual, EditAnywhere)
		FTextBlockStyle TextName;

	UPROPERTY(Category = Visual, EditAnywhere)
		FTextBlockStyle TextSubName;

	UPROPERTY(Category = Visual, EditAnywhere)
		FTextBlockStyle TextAmount;

	UPROPERTY(Category = Visual, EditAnywhere)
		FTextBlockStyle TextButton;

	UPROPERTY(Category = Visual, EditAnywhere)
		FButtonStyle Button;

	UPROPERTY(Category = Configuration, EditAnywhere)
		FSlotHelper ImageSlot;

	UPROPERTY(Category = Configuration, EditAnywhere)
		FSlotHelper RootSlot;

	UPROPERTY(Category = Default, EditAnywhere)
		FSlateBrush DefaultImage;

	UPROPERTY(Category = Default, EditAnywhere)
		FSlateBrush DefaultIcon;
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UItemListRowWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FItemListRowStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
