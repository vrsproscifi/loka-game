// VRSPRO

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "LeagueFormWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT()
struct LOKAGAME_API FLeagueFormStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	FLeagueFormStyle();
	virtual ~FLeagueFormStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FLeagueFormStyle& GetDefault();

	UPROPERTY(Category = Appearance, EditAnywhere)
		FScrollBarStyle ScrollBar;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FCheckBoxStyle LeftToggleButton;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FCheckBoxStyle RightToggleButton;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FEditableTextBoxStyle MakeInputs;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FTextBlockStyle CheckBoxText;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FTextBlockStyle MakeText;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FButtonStyle MakeButton;

	UPROPERTY(Category = Appearance, EditAnywhere)
		FTextBlockStyle NeedBoxText;

	UPROPERTY(Category = Configuration, EditAnywhere)
		float WidthList;

	UPROPERTY(Category = Configuration, EditAnywhere)
		float WidthMake;

	UPROPERTY(Category = Configuration, EditAnywhere)
		float WidthMakeInputName;

	UPROPERTY(Category = Configuration, EditAnywhere)
		float WidthMakeInputAbbr;

	UPROPERTY(Category = Configuration, EditAnywhere)
		float WidthMakeCheckBox;
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class ULeagueFormWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FLeagueFormStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
