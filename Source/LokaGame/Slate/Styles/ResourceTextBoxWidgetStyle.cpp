// VRSPRO

#include "LokaGame.h"
#include "ResourceTextBoxWidgetStyle.h"


FResourceTextBoxStyle::FResourceTextBoxStyle()
{
}

FResourceTextBoxStyle::~FResourceTextBoxStyle()
{
}

const FName FResourceTextBoxStyle::TypeName(TEXT("FResourceTextBoxStyle"));

const FResourceTextBoxStyle& FResourceTextBoxStyle::GetDefault()
{
	static FResourceTextBoxStyle Default;
	return Default;
}

void FResourceTextBoxStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

