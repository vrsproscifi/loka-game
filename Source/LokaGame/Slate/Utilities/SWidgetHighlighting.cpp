// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "SWidgetHighlighting.h"
#include "SlateOptMacros.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SWidgetHighlighting::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SWidgetHighlighting::AddWidget(const TSharedRef<SWidget>& InWidget)
{
	HighlightWidgets.AddUnique(InWidget);
}

void SWidgetHighlighting::RemoveWidget(const TSharedRef<SWidget>& InWidget)
{
	HighlightWidgets.Remove(InWidget);
}

int32 SWidgetHighlighting::OnPaint(const FPaintArgs& Args,
	const FGeometry& AllottedGeometry,
	const FSlateRect& MyCullingRect,
	FSlateWindowElementList& OutDrawElements,
	int32 LayerId,
	const FWidgetStyle& InWidgetStyle,
	bool bParentEnabled) const
{
	for (int32 WidgetIndex = 0; WidgetIndex < HighlightWidgets.Num(); ++WidgetIndex)
	{
		FWidgetPath WidgetPath;
		FSlateApplication::Get().FindPathToWidget(HighlightWidgets[WidgetIndex], WidgetPath, EVisibility::Visible);

		if (WidgetPath.TopLevelWindow.IsValid())
		{
			FPaintGeometry WindowSpaceGeometry = HighlightWidgets[WidgetIndex]->GetCachedGeometry().ToInflatedPaintGeometry(FVector2D(Style->BrushOffset, Style->BrushOffset));
			WindowSpaceGeometry.AppendTransform(TransformCast<FSlateLayoutTransform>(Inverse(WidgetPath.TopLevelWindow->GetPositionInScreen())));

			FSlateDrawElement::MakeBox(
				OutDrawElements,
				++LayerId,
				WindowSpaceGeometry,
				&Style->Brush,
				ESlateDrawEffect::None,
				Style->Brush.TintColor.GetSpecifiedColor()
			);
		}
	}

	return LayerId;
}
