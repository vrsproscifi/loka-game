// VRSPRO

#pragma once

#include "Widgets/SCompoundWidget.h"

DECLARE_DELEGATE_OneParam(FOnToggleInteractive, const bool);

class LOKAGAME_API SUsableCompoundWidget : public SCompoundWidget
{
	friend class UUTGameViewportClient;
public:
	SLATE_BEGIN_ARGS(SUsableCompoundWidget)
	{}
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs){}

	virtual void ToggleWidget(const bool) {}
	virtual bool IsInInteractiveMode() const { return false; }

protected:

	FOnToggleInteractive OnToggleInteractive;
};
