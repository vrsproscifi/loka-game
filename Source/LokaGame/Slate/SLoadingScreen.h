// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SlateBasics.h"
#include "Slate/LokaStyle.h"
#include "Slate/LokaSlateStyle.h"

#include "Styles/LoadingScreenWidgetStyle.h"

class LOKAGAME_API SLoadingScreen : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SLoadingScreen)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FLoadingScreenStyle>("SLoadingScreenStyle"))
	{}

	SLATE_STYLE_ARGUMENT(FLoadingScreenStyle, Style)

	SLATE_END_ARGS()

	/** Constructs this widget with InArgs */
	void Construct(const FArguments& InArgs);

	void SetLoadingImage(const FSlateBrush *InImage);
	void SetLoadingTitle(const FText &InText);
	void SetLoadingTip(const FText &InText);

	void SetRandomImage();
	void SetRandomTip();

	void SetRandomImageAndTip();

protected:
	virtual void Tick(const FGeometry & AllottedGeometry, const double InCurrentTime, const float InDeltaTime) override;

	const FLoadingScreenStyle *Style;

	TSharedPtr<SImage> WidgetImage;
	TSharedPtr<STextBlock> WidgetLoadingTitle;
	TSharedPtr<SRichTextBlock> WidgetLoadingTip;

	TSharedPtr<SBorder> SUKA;
	TSharedPtr<SCanvas> SpritesContainer;

	FCurveSequence ZliponAnim;
	FCurveHandle ZaliponHandle[12];

	FCurveSequence ZaliponRotateAnim[2];
	FCurveHandle ZaliponRotateHandle[2];
	//bool bReverse[12];

	FSlateColor ZaliponColor(int32 _index) const;
};
