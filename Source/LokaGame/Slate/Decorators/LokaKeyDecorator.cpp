
#include "LokaGame.h"
#include "LokaKeyDecorator.h"
#include "SKeyBinding.h"

FLokaKeyDecorator::FLokaKeyDecorator()
{
}

TSharedRef<FLokaKeyDecorator> FLokaKeyDecorator::Create()
{
	return MakeShareable(new FLokaKeyDecorator());
}

bool FLokaKeyDecorator::Supports(const FTextRunParseResults& RunParseResult, const FString& Text) const
{
	return (RunParseResult.Name == TEXT("key"));
}

TSharedRef<ISlateRun> FLokaKeyDecorator::Create(const TSharedRef<FTextLayout>& TextLayout, const FTextRunParseResults& RunParseResult, const FString& OriginalText, const TSharedRef<FString>& InOutModelText, const ISlateStyle* Style)
{
	FTextRange ModelRange;
	ModelRange.BeginIndex = InOutModelText->Len();
	*InOutModelText += TEXT('\x200B'); // Zero-Width Breaking Space
	ModelRange.EndIndex = InOutModelText->Len();

	FTextRunInfo RunInfo(RunParseResult.Name, FText::FromString(OriginalText.Mid(RunParseResult.ContentRange.BeginIndex, RunParseResult.ContentRange.EndIndex - RunParseResult.ContentRange.BeginIndex)));
	for (const TPair<FString, FTextRange>& Pair : RunParseResult.MetaData)
	{
		RunInfo.MetaData.Add(Pair.Key, OriginalText.Mid(Pair.Value.BeginIndex, Pair.Value.EndIndex - Pair.Value.BeginIndex));
	}

	//==============================================================================================================================]

	const bool bAlt = RunInfo.MetaData.Contains("alt");
	const bool bUseBackground = RunInfo.MetaData.Contains("background");	// binding
	FString Binding;

	if (RunInfo.MetaData.Contains("binding"))	
	{
		Binding = *RunInfo.MetaData.Find("binding");		
	}
	else if (RunInfo.Content.IsEmpty() == false)
	{
		Binding = RunInfo.Content.ToString();
	}	

	TSharedRef<SWidget> ResultWidget = SNew(SKeyBinding).WithBackground(bUseBackground).Binding(Binding).IsAlternative(bAlt);

	return FSlateWidgetRun::Create(TextLayout, RunInfo, InOutModelText, FSlateWidgetRun::FWidgetRunInfo(ResultWidget, 0), ModelRange);
}