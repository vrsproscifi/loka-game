
#include "LokaGame.h"
#include "LokaSliderNumberDecorator.h"
#include "SSliderNumber.h"

FLokaSliderNumberDecorator::FLokaSliderNumberDecorator()
{
}

TSharedRef<FLokaSliderNumberDecorator> FLokaSliderNumberDecorator::Create(FOnInt32ValueChanged& InDelegate)
{
	TSharedRef<FLokaSliderNumberDecorator> Decorator = MakeShareable(new FLokaSliderNumberDecorator());
	Decorator->OnValueChange = InDelegate;
	return Decorator;
}

bool FLokaSliderNumberDecorator::Supports(const FTextRunParseResults& RunParseResult, const FString& Text) const
{
	return (RunParseResult.Name == TEXT("numslider"));
}

TSharedRef<ISlateRun> FLokaSliderNumberDecorator::Create(const TSharedRef<FTextLayout>& TextLayout, const FTextRunParseResults& RunParseResult, const FString& OriginalText, const TSharedRef<FString>& InOutModelText, const ISlateStyle* Style)
{
	FTextRange ModelRange;
	ModelRange.BeginIndex = InOutModelText->Len();
	*InOutModelText += TEXT('\x200B'); // Zero-Width Breaking Space
	ModelRange.EndIndex = InOutModelText->Len();

	FTextRunInfo RunInfo(RunParseResult.Name, FText::FromString(OriginalText.Mid(RunParseResult.ContentRange.BeginIndex, RunParseResult.ContentRange.EndIndex - RunParseResult.ContentRange.BeginIndex)));
	for (const TPair<FString, FTextRange>& Pair : RunParseResult.MetaData)
	{
		RunInfo.MetaData.Add(Pair.Key, OriginalText.Mid(Pair.Value.BeginIndex, Pair.Value.EndIndex - Pair.Value.BeginIndex));
	}

	//==============================================================================================================================]

	int32 Min = 0;
	auto MinNumberStr = RunInfo.MetaData.Find("min");
	if (MinNumberStr && MinNumberStr->Len())
	{
		Min = FCString::Atoi(**MinNumberStr);
	}

	int32 Max = 0;
	auto MaxNumberStr = RunInfo.MetaData.Find("max");
	if (MaxNumberStr && MaxNumberStr->Len())
	{
		Max = FCString::Atoi(**MaxNumberStr);
	}

	int32 Delta = 1;
	auto DeltaNumberStr = RunInfo.MetaData.Find("delta");
	if (DeltaNumberStr && DeltaNumberStr->Len())
	{
		Delta = FCString::Atoi(**DeltaNumberStr);
	}

	int32 Width = 200;
	auto WidthNumberStr = RunInfo.MetaData.Find("width");
	if (WidthNumberStr && WidthNumberStr->Len())
	{
		Width = FCString::Atoi(**WidthNumberStr);
	}

	int32 Height = 30;
	auto HeightNumberStr = RunInfo.MetaData.Find("height");
	if (HeightNumberStr && HeightNumberStr->Len())
	{
		Height = FCString::Atoi(**HeightNumberStr);
	}

	int32 Value = 0;
	if (RunInfo.Content.IsNumeric())
	{
		Value = FCString::Atoi(*RunInfo.Content.ToString());
	}

	auto ResultWidget = SNew(SSliderNumber<int32>).SliderMinimum(Min).SliderMaximum(Max).ButtonsDelta(Delta).OnValueChange(OnValueChange);
	ResultWidget->SetValue(Value);

	return FSlateWidgetRun::Create(TextLayout, RunInfo, InOutModelText, FSlateWidgetRun::FWidgetRunInfo(SNew(SBox).MinDesiredWidth(Width).MaxDesiredHeight(Height)[ResultWidget], 0), ModelRange);
}