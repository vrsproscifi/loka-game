// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Components/SSliderSpin.h"
#include "Styles/SettingsManagerWidgetStyle.h"
/**
 * 
 */
class LOKAGAME_API SSettingsManager_Spin : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SSettingsManager_Spin)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FSettingsManagerStyle>("SSettingsManagerStyle"))
		, _IsGraphics(true)
	{}
	SLATE_STYLE_ARGUMENT(FSettingsManagerStyle, Style)
	SLATE_ARGUMENT(FText, Name)
	SLATE_ARGUMENT(bool, IsGraphics)
	SLATE_ARGUMENT(TArray<FText>, Values)
	SLATE_EVENT(FOnListSliderValue, OnValueChange)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	void SetName(const FText&);
	FORCEINLINE FText GetName() const { return Widget_TextBlock->GetText(); }
	void SetValues(TArray<FText>);
	void AddValue(const FText&);

	void SetActiveValue(const FText&);
	void SetActiveValueIndex(const int32&);

	FText GetActiveValue() const;
	int32 GetActiveValueIndex() const;

protected:

	const FSettingsManagerStyle* Style;

	TSharedPtr<SSliderSpin> Widget_SliderSpin;
	TSharedPtr<STextBlock> Widget_TextBlock;
};
