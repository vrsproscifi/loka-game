// VRSPRO

#pragma once

#include "Styles/SettingsManagerWidgetStyle.h"
#include "Windows/SWindowBase.h"

class STabbedContainer;
class UUTGameUserSettings;

class SSettingsManager_Spin;
class SSettingsManager_Percent;
class SSettingsManager_Check;
class SSettingsManager_Controll;
class SSettingsManager_Dialog;

struct FInputActionKeyMapping;
struct FInputAxisKeyMapping;

struct FControllWidget
{
	FText Desc;
	FString Key;
	TSharedPtr<SSettingsManager_Controll> Widget;
	FKey FirstKey;
	FKey SecondKey;

	FControllWidget(const FText& InDesc, const FString& InKey) : Desc(InDesc), Key(InKey), Widget(), FirstKey(), SecondKey() {}
};

class LOKAGAME_API SSettingsManager : public SWindowBase
{
public:
	SLATE_BEGIN_ARGS(SSettingsManager)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FSettingsManagerStyle>("SSettingsManagerStyle"))
		, _IsCentered(false)
	{}
	SLATE_STYLE_ARGUMENT(FSettingsManagerStyle, Style)
	SLATE_ARGUMENT(UUTGameUserSettings*, GameUserSettings)
	SLATE_ARGUMENT(bool, IsCentered)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs, TSharedRef<SWindowsContainer> InOwner);
	void LoadSettings();
	void SaveSettings();

	bool CheckInputBinds();

	FOnClickedOutside OnClickedX;
	UUTGameUserSettings* GameUserSettings;

protected:

	const FSettingsManagerStyle* Style;

	TArray<FInputActionKeyMapping> Input_ActionMappings;
	TArray<FInputAxisKeyMapping> Input_AxisMappings;

	bool IsTargetAxis;
	FInputActionKeyMapping* Input_ActionSelected;
	FInputAxisKeyMapping* Input_AxisSelected;

	FReply OnClickedGeneralButton(const bool);
	void OnShowChangeKeyDialog(const FString&, const bool);
	void OnRemoveKey(const FString&, const bool);
	void OnKeyChanged(const FKey&);

	void FillKeys();

	FInputActionKeyMapping* FindKeyBind(const FString&, const bool);
	FInputAxisKeyMapping* FindAxisBind(const FString&, const bool);

	TArray<FName> Widgets_Changed;
	bool IsAllowChanged;

	TSharedPtr<STabbedContainer> Widget_TabbedContainer;

	TSharedPtr<SSettingsManager_Spin> 
		  Widget_Language
		, Widget_GeneralQuality
		, Widget_ScreenResolution
		, Widget_ViewDistance
		, Widget_AntiAliasingMethod
		, Widget_AntiAliasing
		, Widget_ShadowQuality
		, Widget_PostProcess
		, Widget_TextureQuality
		, Widget_EffectsQuality
		, Widget_FoliageQuality
		, Widget_WindowMode;

	TSharedPtr<SSettingsManager_Percent>
		  Widget_SoundMaster
		, Widget_SoundSFX
		, Widget_SoundUI
		, Widget_SoundMusic
		, Widget_SoundVoice
		, Widget_ResolutionQuality
		, Widget_MouseSens;

	TSharedPtr<SSettingsManager_Dialog>
		  Widget_KeyDialog;

	TSharedPtr<SSettingsManager_Check>
		  Widget_CheckVSync
		, Widget_CheckSmoothMouse
		, Widget_CheckInvertMouseX
		, Widget_CheckInvertMouseY
		, Widget_CheckShowPing
		, Widget_CheckShowFPS
		, Widget_CheckAmbientOcclusion
		, Widget_CheckMotionBlur
		, Widget_CheckBloom
		, Widget_CheckDamageNumbers
		, Widget_CheckCVA;

	TSharedPtr<SVerticalBox> ControllsContainer;
	TArray<FControllWidget> ControllWidgets;
};
