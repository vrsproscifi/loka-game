// VRSPRO

#include "LokaGame.h"
#include "SSettingsManager_Controll.h"
#include "SlateOptMacros.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SSettingsManager_Controll::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	Key = InArgs._Key;
	OnCallChangeKey = InArgs._OnCallChangeKey;
	OnRemoveKey = InArgs._OnRemoveKey;

	MAKE_UTF8_SYMBOL(sRemove, 0xf00d);

	ChildSlot
	[
		SNew(SVerticalBox)
		+ SVerticalBox::Slot().AutoHeight()
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot()
			[
				SAssignNew(Widget_TextBlock, STextBlock).Text(InArgs._Name)
				.TextStyle(&Style->Names)
			]
			+ SHorizontalBox::Slot()
			[
				SNew(SBox).HeightOverride(Style->HeightOverride)
				[
					SNew(SHorizontalBox)
					+ SHorizontalBox::Slot()
					[
						SAssignNew(Widget_ButtonKey[false], SButton)
						.OnClicked(this, &SSettingsManager_Controll::OnClickedButtonEvent, false)
						.HAlign(HAlign_Fill)
						.VAlign(VAlign_Center)
						.ButtonStyle(&Style->Ctrl.ButtonKey)
						[
							SNew(SOverlay)
							+ SOverlay::Slot().HAlign(HAlign_Center)
							[
								SAssignNew(Widget_ButtonOne, STextBlock)
								.TextStyle(&Style->Ctrl.ButtonTextKey)
							]
							+ SOverlay::Slot().HAlign(HAlign_Right).VAlign(VAlign_Center)
							[
								SNew(SButton)
								.Visibility(this, &SSettingsManager_Controll::GetVisibilityRemove, false)
								.OnClicked(this, &SSettingsManager_Controll::OnClickedButtonEventDelete, false)
								.HAlign(HAlign_Center)
								.VAlign(VAlign_Center)
								.ButtonStyle(&Style->Ctrl.ButtonRemove)
								[
									SNew(STextBlock)
									.Text(FText::FromString(sRemove))
									.TextStyle(&Style->Ctrl.ButtonTextRemove)
								]
							]
						]
					]
					+ SHorizontalBox::Slot()
					[
						SAssignNew(Widget_ButtonKey[true], SButton)
						.OnClicked(this, &SSettingsManager_Controll::OnClickedButtonEvent, true)
						.HAlign(HAlign_Fill)
						.VAlign(VAlign_Center)
						.ButtonStyle(&Style->Ctrl.ButtonKey)
						[
							SNew(SOverlay)
							+ SOverlay::Slot().HAlign(HAlign_Center)
							[
								SAssignNew(Widget_ButtonTwo, STextBlock)
								.TextStyle(&Style->Ctrl.ButtonTextKey)
							]
							+ SOverlay::Slot().HAlign(HAlign_Right).VAlign(VAlign_Center)
							[
								SNew(SButton)
								.Visibility(this, &SSettingsManager_Controll::GetVisibilityRemove, true)
								.OnClicked(this, &SSettingsManager_Controll::OnClickedButtonEventDelete, true)
								.HAlign(HAlign_Center)
								.VAlign(VAlign_Center)
								.ButtonStyle(&Style->Ctrl.ButtonRemove)
								[
									SNew(STextBlock)
									.Text(FText::FromString(sRemove))
									.TextStyle(&Style->Ctrl.ButtonTextRemove)
								]
							]
						]
					]
				]
			]
		]
		+ SVerticalBox::Slot().AutoHeight()
		[
			SNew(SSpacer).Size(FVector2D(0, Style->BottomRowPadding))
		]
	];

}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

EVisibility SSettingsManager_Controll::GetVisibilityRemove(const bool IsAlternative) const
{
	return Widget_ButtonKey[IsAlternative]->IsHovered() ? EVisibility::Visible : EVisibility::Collapsed;
}

FReply SSettingsManager_Controll::OnClickedButtonEvent(const bool IsAlternative)
{
	OnCallChangeKey.ExecuteIfBound(Key, IsAlternative);
	return FReply::Handled();
}

FReply SSettingsManager_Controll::OnClickedButtonEventDelete(const bool IsAlternative)
{
	OnRemoveKey.ExecuteIfBound(Key, IsAlternative);
	return FReply::Handled();
}

void SSettingsManager_Controll::SetName(const FText& Name)
{
	Widget_TextBlock->SetText(Name);
}

void SSettingsManager_Controll::SetKeyName(const FText& InKey, const bool IsAlternative)
{
	if (!IsAlternative)
	{
		Widget_ButtonOne->SetText(InKey);
	}
	else
	{
		Widget_ButtonTwo->SetText(InKey);
	}
}