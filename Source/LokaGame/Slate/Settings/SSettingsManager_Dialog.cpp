// VRSPRO

#include "LokaGame.h"
#include "SSettingsManager_Dialog.h"
#include "SlateOptMacros.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SSettingsManager_Dialog::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	OnKeyChanged = InArgs._OnKeyChanged;
	CurrentKey = EKeys::Invalid;

	ChildSlot
	[
		SNew(SBorder)
		.BorderImage(&Style->Dialog.Background)
		.Padding(0)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot().AutoHeight()
			[
				SNew(SBorder).HAlign(HAlign_Center).VAlign(VAlign_Center)
				.BorderImage(&Style->Dialog.Head)
				[
					SNew(STextBlock).Text(NSLOCTEXT("Settings", "Settings.Dialog.Header", "Change action key"))
					.Justification(ETextJustify::Center)
					.TextStyle(&Style->Dialog.HeadText)
				]
			]
			+ SVerticalBox::Slot().AutoHeight()
			[
				SNew(STextBlock).Text(NSLOCTEXT("Settings", "Settings.Dialog.Message", "Please down any key for replace..."))
				.Justification(ETextJustify::Center)
				.TextStyle(&Style->Dialog.HeadText)
			]
			+ SVerticalBox::Slot().AutoHeight().Padding(FMargin(0, 6))
			[
				SAssignNew(Widget_TestKeyName, STextBlock)
				.Justification(ETextJustify::Center)
				.TextStyle(&Style->Dialog.KeyText)
			]
			+ SVerticalBox::Slot().AutoHeight()
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot()
				[
					SNew(SButton).Text(NSLOCTEXT("Settings", "Settings.Dialog.Accept", "Accept"))
					.OnClicked(this, &SSettingsManager_Dialog::OnClickedButtonEvent, true)
					.HAlign(HAlign_Center).VAlign(VAlign_Center)
					.TextStyle(&Style->Dialog.ButtonText)
					.ButtonStyle(&Style->Dialog.Button)
				]
				+ SHorizontalBox::Slot()
				[
					SNew(SButton).Text(NSLOCTEXT("Settings", "Settings.Dialog.Cancel", "Cancel"))
					.OnClicked(this, &SSettingsManager_Dialog::OnClickedButtonEvent, false)
					.HAlign(HAlign_Center).VAlign(VAlign_Center)
					.TextStyle(&Style->Dialog.ButtonText)
					.ButtonStyle(&Style->Dialog.Button)
				]
			]
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

FReply SSettingsManager_Dialog::OnMouseButtonDown(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent)
{	
	CurrentKey = MouseEvent.GetEffectingButton();
	Widget_TestKeyName->SetText(CurrentKey.GetDisplayName());
	return FReply::Handled();
}

FReply SSettingsManager_Dialog::OnPreviewKeyDown(const FGeometry& MyGeometry, const FKeyEvent& InKeyEvent)
{
	CurrentKey = InKeyEvent.GetKey();
	Widget_TestKeyName->SetText(CurrentKey.GetDisplayName());
	return FReply::Handled();
}

FReply SSettingsManager_Dialog::OnMouseWheel(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent)
{
	if (FMath::IsNearlyEqual(MouseEvent.GetWheelDelta(), 1.0f))
		CurrentKey = EKeys::MouseScrollUp;
	else
		CurrentKey = EKeys::MouseScrollDown;

	Widget_TestKeyName->SetText(CurrentKey.GetDisplayName());
	return FReply::Handled();
}

bool SSettingsManager_Dialog::SupportsKeyboardFocus() const
{
	return true;
}

FReply SSettingsManager_Dialog::OnClickedButtonEvent(const bool IsAccept)
{
	OnKeyChanged.ExecuteIfBound(IsAccept ? CurrentKey : EKeys::Invalid);
	CurrentKey = EKeys::Invalid;
	return FReply::Handled();
}