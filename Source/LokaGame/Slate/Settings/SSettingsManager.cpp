// VRSPRO

#include "LokaGame.h"
#include "SSettingsManager.h"
#include "SlateOptMacros.h"

#include "GameInstance/LokaGameUserSettings.h"

#include "SSettingsManager_Spin.h"
#include "SSettingsManager_Percent.h"
#include "SSettingsManager_Check.h"
#include "SSettingsManager_Controll.h"
#include "SSettingsManager_Dialog.h"

#include "SWindowBackground.h"

#include "SMessageBox.h"
#include "UTGameUserSettings.h"
#include "Containers/STabbedContainer.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SSettingsManager::Construct(const FArguments& InArgs, TSharedRef<SWindowsContainer> InOwner)
{
	Style = InArgs._Style;
	GameUserSettings = /*InArgs._GameUserSettings ? InArgs._GameUserSettings :*/ Cast<UUTGameUserSettings>(GEngine->GameUserSettings);

#pragma region WidgetConstruction	

	MAKE_UTF8_SYMBOL(sSettings, 0xf013);

	auto ContentWidget = SNew(SOverlay)
			+ SOverlay::Slot().Padding(0, 0, 0, 54)
			[
				SAssignNew(Widget_TabbedContainer, STabbedContainer)
				+ STabbedContainer::Slot().Name(NSLOCTEXT("Settings", "Settings.General", "General"))
				[
					SNew(SScrollBox)
					.ScrollBarStyle(&FLokaStyle::Get().GetWidgetStyle<FScrollBarStyle>("Default_ScrollBar")).ScrollBarThickness(FVector2D(4, 4))
					+ SScrollBox::Slot()[SNew(SBox).HeightOverride(10)] // Top Padding
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_Language, SSettingsManager_Spin).Name(NSLOCTEXT("Settings", "Settings.Language", "Language")).IsGraphics(false)
						.OnValueChange_Lambda([&](int32) { if (IsAllowChanged) { Widgets_Changed.Add("Language"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_CheckShowPing, SSettingsManager_Check).Name(NSLOCTEXT("Settings", "Settings.ShowPing", "Show Ping"))
						.OnValueChange_Lambda([&](bool) { if (IsAllowChanged) { Widgets_Changed.Add("ShowPing"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_CheckShowFPS, SSettingsManager_Check).Name(NSLOCTEXT("Settings", "Settings.ShowFPS", "Show FPS"))
						.OnValueChange_Lambda([&](bool) { if (IsAllowChanged) { Widgets_Changed.Add("ShowFPS"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_CheckDamageNumbers, SSettingsManager_Check).Name(NSLOCTEXT("Settings", "Settings.ShowDamageNumbers", "Show Damage Numbers"))
						.OnValueChange_Lambda([&](bool) { if (IsAllowChanged) { Widgets_Changed.Add("ShowDamageNumbers"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_CheckCVA, SSettingsManager_Check).Name(NSLOCTEXT("Settings", "Settings.CVA", "Allow Change View By Ability"))
						.OnValueChange_Lambda([&](bool) { if (IsAllowChanged) { Widgets_Changed.Add("CVA"); } })
					]
				]
				+ STabbedContainer::Slot().Name(NSLOCTEXT("Settings", "Settings.Graphics", "Graphics"))
				[
					SNew(SScrollBox)
					.ScrollBarStyle(&FLokaStyle::Get().GetWidgetStyle<FScrollBarStyle>("Default_ScrollBar")).ScrollBarThickness(FVector2D(4, 4))
					+ SScrollBox::Slot()[SNew(SBox).HeightOverride(10)] // Top Padding
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_GeneralQuality, SSettingsManager_Spin).Name(NSLOCTEXT("Settings", "Settings.GeneralQuality", "General Quality"))
						.OnValueChange_Lambda([&](int32) { if (IsAllowChanged) { Widgets_Changed.Add("GeneralQuality"); } })
					]
					+ SScrollBox::Slot()
					[
						SNew(SSpacer).Size(FVector2D(.0f, 40.0f))
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_WindowMode, SSettingsManager_Spin).Name(NSLOCTEXT("Settings", "Settings.WindowMode", "Window Mode")).IsGraphics(false)
						.OnValueChange_Lambda([&](int32) { if (IsAllowChanged) { Widgets_Changed.Add("WindowMode"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_ScreenResolution, SSettingsManager_Spin).Name(NSLOCTEXT("Settings", "Settings.ScreenResolution", "Screen Resolution")).IsGraphics(false)
						.OnValueChange_Lambda([&](int32) { if (IsAllowChanged) { Widgets_Changed.Add("ScreenResolution"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_AntiAliasingMethod, SSettingsManager_Spin).Name(NSLOCTEXT("Settings", "Settings.AntiAliasingMethod", "Anti-Aliasing Method")).IsGraphics(false)
						.OnValueChange_Lambda([&](int32) { if (IsAllowChanged) { Widgets_Changed.Add("AntiAliasingMethod"); } })
					]
					+ SScrollBox::Slot()
					[
						SNew(SSpacer).Size(FVector2D(.0f, 20.0f))
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_ResolutionQuality, SSettingsManager_Percent).Name(NSLOCTEXT("Settings", "Settings.ResolutionQuality", "Resolution Quality")).IsEnabled_Lambda([&]() { return Widget_GeneralQuality->GetActiveValueIndex() == static_cast<int32>(EQualityLevels::Personal); })
						.OnValueChange_Lambda([&](float) { if (IsAllowChanged) { Widgets_Changed.Add("ResolutionQuality"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_ViewDistance, SSettingsManager_Spin).Name(NSLOCTEXT("Settings", "Settings.ViewDistanceQuality", "View Distance")).IsEnabled_Lambda([&]() { return Widget_GeneralQuality->GetActiveValueIndex() == static_cast<int32>(EQualityLevels::Personal); })
						.OnValueChange_Lambda([&](int32) { if (IsAllowChanged) { Widgets_Changed.Add("ViewDistanceQuality"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_AntiAliasing, SSettingsManager_Spin).Name(NSLOCTEXT("Settings", "Settings.AntiAliasingQuality", "Anti-Aliasing")).IsEnabled_Lambda([&]() { return Widget_GeneralQuality->GetActiveValueIndex() == static_cast<int32>(EQualityLevels::Personal); })
						.OnValueChange_Lambda([&](int32) { if (IsAllowChanged) { Widgets_Changed.Add("AntiAliasingQuality"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_ShadowQuality, SSettingsManager_Spin).Name(NSLOCTEXT("Settings", "Settings.ShadowQuality", "Shadow Quality")).IsEnabled_Lambda([&]() { return Widget_GeneralQuality->GetActiveValueIndex() == static_cast<int32>(EQualityLevels::Personal); })
						.OnValueChange_Lambda([&](int32) { if (IsAllowChanged) { Widgets_Changed.Add("ShadowQuality"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_PostProcess, SSettingsManager_Spin).Name(NSLOCTEXT("Settings", "Settings.PostProcessQuality", "Post-Processing")).IsEnabled_Lambda([&]() { return Widget_GeneralQuality->GetActiveValueIndex() == static_cast<int32>(EQualityLevels::Personal); })
						.OnValueChange_Lambda([&](int32) { if (IsAllowChanged) { Widgets_Changed.Add("PostProcessQuality"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_TextureQuality, SSettingsManager_Spin).Name(NSLOCTEXT("Settings", "Settings.TextureQuality", "Texture Quality")).IsEnabled_Lambda([&]() { return Widget_GeneralQuality->GetActiveValueIndex() == static_cast<int32>(EQualityLevels::Personal); })
						.OnValueChange_Lambda([&](int32) { if (IsAllowChanged) { Widgets_Changed.Add("TextureQuality"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_EffectsQuality, SSettingsManager_Spin).Name(NSLOCTEXT("Settings", "Settings.EffectsQuality", "Effects Quality")).IsEnabled_Lambda([&]() { return Widget_GeneralQuality->GetActiveValueIndex() == static_cast<int32>(EQualityLevels::Personal); })
						.OnValueChange_Lambda([&](int32) { if (IsAllowChanged) { Widgets_Changed.Add("EffectsQuality"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_FoliageQuality, SSettingsManager_Spin).Name(NSLOCTEXT("Settings", "Settings.FoliageQuality", "Foliage Quality")).IsEnabled_Lambda([&]() { return Widget_GeneralQuality->GetActiveValueIndex() == static_cast<int32>(EQualityLevels::Personal); })
						.OnValueChange_Lambda([&](int32) { if (IsAllowChanged) { Widgets_Changed.Add("FoliageQuality"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_CheckVSync, SSettingsManager_Check).Name(NSLOCTEXT("Settings", "Settings.VSync", "VSync"))
						.OnValueChange_Lambda([&](bool) { if (IsAllowChanged) { Widgets_Changed.Add("VSync"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_CheckAmbientOcclusion, SSettingsManager_Check).Name(NSLOCTEXT("Settings", "Settings.AmbientOcclusion", "Ambient Occlusion"))
						.OnValueChange_Lambda([&](bool) { if (IsAllowChanged) { Widgets_Changed.Add("AmbientOcclusion"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_CheckMotionBlur, SSettingsManager_Check).Name(NSLOCTEXT("Settings", "Settings.MotionBlur", "Motion Blur"))
						.OnValueChange_Lambda([&](bool) { if (IsAllowChanged) { Widgets_Changed.Add("MotionBlur"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_CheckBloom, SSettingsManager_Check).Name(NSLOCTEXT("Settings", "Settings.Bloom", "Bloom"))
						.OnValueChange_Lambda([&](bool) { if (IsAllowChanged) { Widgets_Changed.Add("Bloom"); } })
					]
				]
				+ STabbedContainer::Slot().Name(NSLOCTEXT("Settings", "Settings.Sound", "Sound"))
				[
					SNew(SScrollBox)
					.ScrollBarStyle(&FLokaStyle::Get().GetWidgetStyle<FScrollBarStyle>("Default_ScrollBar")).ScrollBarThickness(FVector2D(4, 4))
					+ SScrollBox::Slot()[SNew(SBox).HeightOverride(10)] // Top Padding
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_SoundMaster, SSettingsManager_Percent).Name(NSLOCTEXT("Settings", "Settings.SoundMaster", "Master"))
						.OnValueChange_Lambda([&](float) { if (IsAllowChanged) { Widgets_Changed.Add("SoundMaster"); } })
					]
					+ SScrollBox::Slot()
					[
						SNew(SSpacer).Size(FVector2D(.0f, 40.0f))
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_SoundSFX, SSettingsManager_Percent).Name(NSLOCTEXT("Settings", "Settings.SoundSFX", "Effects"))
						.OnValueChange_Lambda([&](float) { if (IsAllowChanged) { Widgets_Changed.Add("SoundSFX"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_SoundUI, SSettingsManager_Percent).Name(NSLOCTEXT("Settings", "Settings.SoundUI", "UI Effects"))
						.OnValueChange_Lambda([&](float) { if (IsAllowChanged) { Widgets_Changed.Add("SoundUI"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_SoundMusic, SSettingsManager_Percent).Name(NSLOCTEXT("Settings", "Settings.SoundMusic", "Music"))
						.OnValueChange_Lambda([&](float) { if (IsAllowChanged) { Widgets_Changed.Add("SoundMusic"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_SoundVoice, SSettingsManager_Percent).Name(NSLOCTEXT("Settings", "Settings.SoundVoice", "Voices (screams)"))
						.OnValueChange_Lambda([&](float) { if (IsAllowChanged) { Widgets_Changed.Add("SoundVoice"); } })
					]
				]
				+ STabbedContainer::Slot().Name(NSLOCTEXT("Settings", "Settings.Controlls", "Controlls"))
				[
					SNew(SScrollBox)
					.ScrollBarStyle(&FLokaStyle::Get().GetWidgetStyle<FScrollBarStyle>("Default_ScrollBar")).ScrollBarThickness(FVector2D(4, 4))
					+ SScrollBox::Slot()[SNew(SBox).HeightOverride(10)] // Top Padding
					+ SScrollBox::Slot()
					[
						SNew(STextBlock).TextStyle(&Style->Names).Text(NSLOCTEXT("Settings", "Settings.Mouse.Title", "Mouse Options")).Justification(ETextJustify::Center)
					]
					+ SScrollBox::Slot()[SNew(SBox).HeightOverride(10)] // Top Padding
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_CheckSmoothMouse, SSettingsManager_Check).Name(NSLOCTEXT("Settings", "Settings.SmoothMouse", "Smooth Mouse"))
						.OnValueChange_Lambda([&](bool) { if (IsAllowChanged) { Widgets_Changed.Add("SmoothMouse"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_CheckInvertMouseX, SSettingsManager_Check).Name(NSLOCTEXT("Settings", "Settings.InvertMouseX", "Invert Mouse X"))
						.OnValueChange_Lambda([&](bool) { if (IsAllowChanged) { Widgets_Changed.Add("InvertMouseX"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_CheckInvertMouseY, SSettingsManager_Check).Name(NSLOCTEXT("Settings", "Settings.InvertMouseY", "Invert Mouse Y"))
						.OnValueChange_Lambda([&](bool) { if (IsAllowChanged) { Widgets_Changed.Add("InvertMouseY"); } })
					]
					+ SScrollBox::Slot()
					[
						SAssignNew(Widget_MouseSens, SSettingsManager_Percent).Name(NSLOCTEXT("Settings", "Settings.MouseSens", "Mouse Sensitivity"))
						.OnValueChange_Lambda([&](float) { if (IsAllowChanged) { Widgets_Changed.Add("MouseSens"); } })
						.ButtonsDelta(.01f)
					]
					+ SScrollBox::Slot()[SNew(SBox).HeightOverride(10)] // Top Padding
					+ SScrollBox::Slot()
					[
						SNew(STextBlock).TextStyle(&Style->Names).Text(NSLOCTEXT("Settings", "Settings.Keyboard.Title", "Keyboard or Gamepad")).Justification(ETextJustify::Center)
					]
					+ SScrollBox::Slot()[SNew(SBox).HeightOverride(10)] // Top Padding
					+ SScrollBox::Slot()
					[
						SAssignNew(ControllsContainer, SVerticalBox)
					]
				]
			]
			+ SOverlay::Slot().HAlign(HAlign_Right).VAlign(VAlign_Bottom)
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot().AutoWidth().Padding(4)
				[
					SNew(SButton)
					.HAlign(HAlign_Center)
					.VAlign(VAlign_Center)
					.ContentPadding(FMargin(10, 4))
					.ButtonStyle(&Style->Container.ActionButton)
					.TextStyle(&Style->Container.ActionButtonText)
					.Text(NSLOCTEXT("Settings", "Settings.Apply", "Apply"))
					.OnClicked(this, &SSettingsManager::OnClickedGeneralButton, true)
				]
				+ SHorizontalBox::Slot().AutoWidth().Padding(4)
				[
					SNew(SButton)
					.HAlign(HAlign_Center)
					.VAlign(VAlign_Center)
					.ContentPadding(FMargin(10, 4))
					.ButtonStyle(&Style->Container.ActionButton)
					.TextStyle(&Style->Container.ActionButtonText)
					.Text(NSLOCTEXT("Settings", "Settings.Cancel", "Cancel"))
					.OnClicked(this, &SSettingsManager::OnClickedGeneralButton, false)
				]
			]
			+ SOverlay::Slot().HAlign(HAlign_Left).VAlign(VAlign_Bottom).Padding(4)
			[
				SNew(SButton)
				.HAlign(HAlign_Center)
				.VAlign(VAlign_Center)
				.ContentPadding(FMargin(10, 4))
				.ButtonStyle(&Style->Container.ActionButton)
				.TextStyle(&Style->Container.ActionButtonText)
				.Text(NSLOCTEXT("Settings", "Settings.RestoreDefault", "Restore Default"))
				.Visibility_Lambda([&]() { return (Widget_TabbedContainer.IsValid() && Widget_TabbedContainer->GetActiveSlot() == 3) ? EVisibility::Visible : EVisibility::Hidden; })
				.OnClicked_Lambda([&]() {

					SMessageBox::AddQueueMessage("SSettingsManager.RestoreDefault", FOnClickedOutside::CreateLambda([&]() {
						SMessageBox::Get()->SetHeaderText(NSLOCTEXT("SSettingsManager", "SSettingsManager.RestoreDefault.Title", "Restore Default"));
						SMessageBox::Get()->SetContent(NSLOCTEXT("SSettingsManager", "SSettingsManager.RestoreDefault.Desc", "You sure, want to be restore default key binding?"));
						SMessageBox::Get()->SetButtonsText(NSLOCTEXT("All", "All.Ok", "OK"));
						SMessageBox::Get()->SetEnableClose(true);
						SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](EMessageBoxButton Button) {
							SMessageBox::RemoveQueueMessage("SSettingsManager.RestoreDefault");
							SMessageBox::Get()->ToggleWidget(false);

							if (GameUserSettings)
							{
								GameUserSettings->ResetInputSettings();
								LoadSettings();
							}
						});
						SMessageBox::Get()->OnMessageBoxHidden.AddLambda([&]() {
							SMessageBox::RemoveQueueMessage("SSettingsManager.RestoreDefault");
						});
						SMessageBox::Get()->ToggleWidget(true);
					}));						

					return FReply::Handled();
				})
			]
			+ SOverlay::Slot().VAlign(VAlign_Center)
			[
				SAssignNew(Widget_KeyDialog, SSettingsManager_Dialog)
				.OnKeyChanged(this, &SSettingsManager::OnKeyChanged)
			];

#pragma endregion 

	auto SuperArgs =
		SWindowBase::FArguments()
		.SaveTag("SSettingsManager")
		.SizeLimits(FBox2D(FVector2D(680, 400), FVector2D(680, 1000)))
		.TitleText(NSLOCTEXT("All", "All.Settings", "Settings"))
		.QuickButton(FText::FromString(sSettings))
		.QuickButtonOrder(120)
		.CanInvisable(false)
		.Content()
		[
			ContentWidget
		];

	SWindowBase::Construct(SuperArgs, InOwner);

	Widget_KeyDialog->SetVisibility(EVisibility::Hidden);
	Widget_GeneralQuality->AddValue(NSLOCTEXT("Settings", "Settings.Personal", "Personal"));

#pragma region WidgetInputSettings

	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.MoveForward", "Move Forward"), "MoveForward+"));
	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.MoveBackward", "Move Backward"), "MoveForward-"));
	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.MoveLeft", "Move Left"), "MoveRight-"));
	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.MoveRight", "Move Right"), "MoveRight+"));

	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.Jump", "Jump"), "Jump"));
	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.Run", "Run"), "Run"));
	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.ToggleRun", "Run (Switch)"), "ToggleRun"));

	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.TapForward", "Strafe Forward"), "TapForward"));
	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.TapBack", "Strafe Back"), "TapBack"));
	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.TapLeft", "Strafe Left"), "TapLeft"));
	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.TapRight", "Strafe Right"), "TapRight"));

	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.WeaponSwitchUp", "Next Weapon"), "WeaponSwitchUp"));
	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.WeaponSwitchDown", "Previus Weapon"), "WeaponSwitchDown"));
	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.Fire", "Weapon Fire"), "Fire"));
	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.InGameAiming", "Weapon Aiming"), "InGameAiming"));
	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.InGameAimingToggle", "Weapon Aiming (Switch)"), "InGameAimingToggle"));
	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.InGameChat", "Open Chat"), "InGameChat"));
	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.InGameESC", "Open Menu"), "InGameESC"));
	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.InGameTable", "Open Table"), "InGameTable"));

	//ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.ShowNames", "Show Names"), "ShowNames"));
	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.WeaponReload", "Reload Weapon"), "WeaponReload"));
	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.UseAny", "Use"), "UseAny"));
	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.UseGrenade", "Grenade"), "UseGrenade"));
	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.UseAbility", "Ability"), "UseAbility"));
	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.Crouch", "Crouch"), "Crouch"));
	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.CrouchToggle", "Crouch Toggle"), "CrouchToggle"));
	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.ToggleHUD", "ON/OFF HUD (In Fight)"), "ToggleHUD"));
	//ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.TiltLeft", "Tilt Left"), "TiltLeft"));
	//ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.TiltRight", "Tilt Right"), "TiltRight"));
	//ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.TiltLeftToggle", "Tilt Left (Switch)"), "TiltLeftToggle"));
	//ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.TiltRightToggle", "Tilt Right (Switch)"), "TiltRightToggle"));
	//ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.HudHand", "HUD Hand"), "HudHand"));
	//ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.ToggleHudHand", "HUD Hand (Switch)"), "ToggleHudHand"));
	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.SwitchView", "3P/1P View (Switch)"), "SwitchView"));
	// UT Input
	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.Slide", "Slide"), "Slide"));
	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.SingleTapDodge", "SingleTapDodge"), "SingleTapDodge"));
	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.DropCarriedObject", "DropCarriedObject"), "DropCarriedObject"));
	// Build Input
	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.BuildActionPlace", "Building Action 1"), "BuildActionPlace"));
	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.BuildActionCancel", "Building Action 2"), "BuildActionCancel"));
	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.BuildRotateLeft", "Building Rotate Left"), "BuildRotateLeft"));
	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.BuildRotateRight", "Building Rotate Right"), "BuildRotateRight"));
	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.BuildSelection_1", "Building Slot 1"), "BuildSelection_1"));
	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.BuildSelection_2", "Building Slot 2"), "BuildSelection_2"));
	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.BuildSelection_3", "Building Slot 3"), "BuildSelection_3"));
	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.BuildSelection_4", "Building Slot 4"), "BuildSelection_4"));
	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.BuildSelection_5", "Building Slot 5"), "BuildSelection_5"));
	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.BuildSelection_6", "Building Slot 6"), "BuildSelection_6"));
	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.BuildSelection_7", "Building Slot 7"), "BuildSelection_7"));
	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.BuildSelection_8", "Building Slot 8"), "BuildSelection_8"));
	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.BuildSelection_9", "Building Slot 9"), "BuildSelection_9"));
	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.BuildSelectionLeft", "Building Previus Slot"), "BuildSelectionLeft"));
	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.BuildSelectionRight", "Building Next Slot"), "BuildSelectionRight"));
	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.BuildInventory", "Building Inventory"), "BuildInventory"));
	ControllWidgets.Add(FControllWidget(NSLOCTEXT("Settings", "Settings.BuildMode", "Building Mode"), "BuildMode"));

	for (auto& i : ControllWidgets)
	{
		ControllsContainer->AddSlot().AutoHeight()
			[
				SAssignNew(i.Widget, SSettingsManager_Controll)
				.Name(i.Desc).Key(i.Key)
				.OnCallChangeKey(this, &SSettingsManager::OnShowChangeKeyDialog)
				.OnRemoveKey(this, &SSettingsManager::OnRemoveKey)
			];
	}

#pragma endregion 

	IsAllowChanged = true;

	if (GameUserSettings && GameUserSettings->IsValidLowLevel())
	{
		GameUserSettings->InitSettings();
		LoadSettings();
	}
	
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

FReply SSettingsManager::OnClickedGeneralButton(const bool IsApply)
{
	if (IsApply)
	{
		SaveSettings();
	}
	else
	{
		LoadSettings();
		OnClickedX.ExecuteIfBound();
	}

	return FReply::Handled();
}

void SSettingsManager::OnRemoveKey(const FString& Key, const bool IsAlternative)
{
	if (Key.Contains("+") || Key.Contains("-"))
	{
		FindAxisBind(Key, IsAlternative)->Key = EKeys::Invalid;
	}
	else
	{
		FindKeyBind(Key, IsAlternative)->Key = EKeys::Invalid;
	}

	FillKeys();
}

void SSettingsManager::OnShowChangeKeyDialog(const FString& Key, const bool IsAlternative)
{
	Widget_KeyDialog->SetVisibility(EVisibility::Visible);
	FSlateApplication::Get().SetKeyboardFocus(Widget_KeyDialog);

	if (Key.Contains("+") || Key.Contains("-"))
	{
		IsTargetAxis = true;
		Input_AxisSelected = FindAxisBind(Key, IsAlternative);
	}
	else
	{
		IsTargetAxis = false;
		Input_ActionSelected = FindKeyBind(Key, IsAlternative);
	}
}

void SSettingsManager::OnKeyChanged(const FKey& Key)
{
	Widget_KeyDialog->SetVisibility(EVisibility::Hidden);

	if (Key != EKeys::Invalid)
	{
		if (IsTargetAxis)
		{
			if (Input_AxisSelected)
				Input_AxisSelected->Key = Key;
		}
		else
		{
			if (Input_ActionSelected)
				Input_ActionSelected->Key = Key;
		}

		FillKeys();
	}
}

FInputActionKeyMapping* SSettingsManager::FindKeyBind(const FString& Key, const bool IsAlternative)
{
	bool IsFound = false;

	for (SSIZE_T i = 0; i < Input_ActionMappings.Num(); ++i)
	{
		if (Input_ActionMappings[i].ActionName.ToString().Equals(Key))
		{
			if (!IsAlternative || IsFound)
			{
				return &Input_ActionMappings[i];
			}
			else
			{
				IsFound = true;
			}
		}
	}

	Input_ActionMappings.Add(FInputActionKeyMapping(*Key, EKeys::Invalid));
	return &Input_ActionMappings.Last();
}

FInputAxisKeyMapping* SSettingsManager::FindAxisBind(const FString& Key, const bool IsAlternative)
{
	float Scale = 1.0f;
	FString KeyString = Key;

	if (KeyString.Contains("-"))
	{
		KeyString.RemoveAt(KeyString.Find("-"));
		Scale = -1.0f;
	}
	else
	{
		KeyString.RemoveAt(KeyString.Find("+"));
	}

	bool IsFound = false;

	for (SSIZE_T i = 0; i < Input_AxisMappings.Num(); ++i)
	{
		if (Input_AxisMappings[i].AxisName.ToString().Equals(KeyString) && FMath::IsNearlyEqual(Input_AxisMappings[i].Scale, Scale))
		{
			if (!IsAlternative || IsFound)
			{
				return &Input_AxisMappings[i];
			}
			else
			{
				IsFound = true;
			}
		}
	}

	Input_AxisMappings.Add(FInputAxisKeyMapping(*KeyString, EKeys::Invalid, Scale));
	return &Input_AxisMappings.Last();
}

void SSettingsManager::FillKeys()
{
	for (auto& i : ControllWidgets)
	{
		if (i.Key.Contains("+") || i.Key.Contains("-"))
		{
			i.FirstKey = FindAxisBind(i.Key, false)->Key;
			i.Widget->SetKeyName(i.FirstKey.GetDisplayName());
			i.SecondKey = FindAxisBind(i.Key, true)->Key;
			i.Widget->SetKeyName(i.SecondKey.GetDisplayName(), true);
		}
		else
		{
			i.FirstKey = FindKeyBind(i.Key, false)->Key;
			i.Widget->SetKeyName(i.FirstKey.GetDisplayName());
			i.SecondKey = FindKeyBind(i.Key, true)->Key;
			i.Widget->SetKeyName(i.SecondKey.GetDisplayName(), true);
		}
	}
}

void SSettingsManager::LoadSettings()
{
	IsAllowChanged = false;

	Widgets_Changed.Empty();

	Widget_Language->SetValues(TArray<FText>());
	Widget_WindowMode->SetValues(TArray<FText>());
	Widget_ScreenResolution->SetValues(TArray<FText>());
	Widget_AntiAliasingMethod->SetValues(TArray<FText>());

	Widget_WindowMode->AddValue(NSLOCTEXT("Settings", "Settings.WindowMode.Fullscreen", "Fullscreen"));
	Widget_WindowMode->AddValue(NSLOCTEXT("Settings", "Settings.WindowMode.WindowedFullscreen", "Windowed fullscreen"));
	Widget_WindowMode->AddValue(NSLOCTEXT("Settings", "Settings.WindowMode.Windowed", "Windowed"));
	Widget_WindowMode->SetActiveValueIndex(GameUserSettings->GetFullscreenMode());

	Widget_AntiAliasingMethod->AddValue(NSLOCTEXT("Settings", "Settings.AntiAliasingMethod.None", "None"));
	Widget_AntiAliasingMethod->AddValue(NSLOCTEXT("Settings", "Settings.AntiAliasingMethod.FXAA", "FXAA"));
	Widget_AntiAliasingMethod->AddValue(NSLOCTEXT("Settings", "Settings.AntiAliasingMethod.TemporalAA", "TemporalAA"));

	if (IsForwardShadingEnabled(GMaxRHIShaderPlatform))
	{
		Widget_AntiAliasingMethod->AddValue(NSLOCTEXT("Settings", "Settings.AntiAliasingMethod.MSAA2", "2xMSAA"));
		Widget_AntiAliasingMethod->AddValue(NSLOCTEXT("Settings", "Settings.AntiAliasingMethod.MSAA4", "4xMSAA"));
	}

	Widget_AntiAliasingMethod->SetActiveValueIndex(GameUserSettings->GetAAMode());

	SIZE_T _count = 0;
	for (auto loc : GameUserSettings->GetLanguages())
	{
		Widget_Language->AddValue(FText::FromString(loc->GetEnglishName()));

		if (GameUserSettings->GetCurrentLanguage() == loc->GetLCID())
		{
			Widget_Language->SetActiveValueIndex(_count);
		}

		++_count;
	}		

	auto Scalability = GameUserSettings->ScalabilityQuality;
	auto ScreenResolutions = GameUserSettings->GetScreenResolutions();
	auto CurrentScreenResolution = GameUserSettings->GetScreenResolution();

	_count = 0;
	for (auto res : ScreenResolutions)
	{
		Widget_ScreenResolution->AddValue(FText::Format(FText::FromString("{0}x{1}"), FText::AsNumber(res.Width, &FNumberFormattingOptions::DefaultNoGrouping()), FText::AsNumber(res.Height, &FNumberFormattingOptions::DefaultNoGrouping())));

		if (CurrentScreenResolution == FIntPoint(res.Width, res.Height))
		{
			Widget_ScreenResolution->SetActiveValueIndex(_count);
		}

		++_count;
	}

	Widget_GeneralQuality			->SetActiveValueIndex(static_cast<int32>(GameUserSettings->GeneralQualityLevel));
	Widget_ViewDistance				->SetActiveValueIndex(Scalability.ViewDistanceQuality);
	Widget_AntiAliasing				->SetActiveValueIndex(Scalability.AntiAliasingQuality);
	Widget_ShadowQuality			->SetActiveValueIndex(Scalability.ShadowQuality);
	Widget_PostProcess				->SetActiveValueIndex(Scalability.PostProcessQuality);
	Widget_TextureQuality			->SetActiveValueIndex(Scalability.TextureQuality);
	Widget_EffectsQuality			->SetActiveValueIndex(Scalability.EffectsQuality);
	Widget_ResolutionQuality		->SetValue(float(Scalability.ResolutionQuality) / 100.0f);
	Widget_FoliageQuality			->SetActiveValueIndex(Scalability.FoliageQuality);

	Widget_SoundMaster				->SetValue(GameUserSettings->GetSoundVolume(ESoundClassType::Master));
	Widget_SoundSFX					->SetValue(GameUserSettings->GetSoundVolume(ESoundClassType::SFX));
	Widget_SoundUI					->SetValue(GameUserSettings->GetSoundVolume(ESoundClassType::UI));
	Widget_SoundMusic				->SetValue(GameUserSettings->GetSoundVolume(ESoundClassType::Music));
	Widget_SoundVoice				->SetValue(GameUserSettings->GetSoundVolume(ESoundClassType::Voice));

	Widget_CheckVSync				->SetIsChecked(GameUserSettings->IsVSyncEnabled());
	Widget_CheckShowPing			->SetIsChecked(GameUserSettings->IsShowPing());
	Widget_CheckShowFPS				->SetIsChecked(GameUserSettings->IsShowFPS());

	Widget_CheckAmbientOcclusion	->SetIsChecked(GameUserSettings->IsEnabledAmbientOcclusion());
	Widget_CheckMotionBlur			->SetIsChecked(GameUserSettings->IsEnabledMotionBlur());
	Widget_CheckBloom				->SetIsChecked(GameUserSettings->IsEnabledBloom());
	Widget_CheckDamageNumbers		->SetIsChecked(GameUserSettings->IsEnabledDamageNumbers());
	Widget_CheckCVA					->SetIsChecked(GameUserSettings->IsAllowChangeViewByAbility());

	// Input bEnableMouseSmoothing
	auto InputSettings = GetDefault<UInputSettings>();

	Widget_CheckSmoothMouse			->SetIsChecked(InputSettings->bEnableMouseSmoothing);

	Input_ActionMappings = InputSettings->ActionMappings;
	Input_AxisMappings = InputSettings->AxisMappings;

	for (TObjectIterator<UPlayerInput> It; It; ++It)
	{
		Widget_CheckInvertMouseX->SetIsChecked(It->GetInvertAxisKey(EKeys::MouseX));
		Widget_CheckInvertMouseY->SetIsChecked(It->GetInvertAxisKey(EKeys::MouseY));

		Widget_MouseSens->SetValue(It->GetMouseSensitivityX());
		//Widget_MouseSens->SetValue(It->GetMouseSensitivity());
		break;
	}

	FillKeys();

	IsAllowChanged = true;
}

void SSettingsManager::SaveSettings()
{
	IsAllowChanged = false;

	//if (!CheckInputBinds())
	//{
	//	//show message
	//	SMessageBox::AddQueueMessage("SSettingsManager.KeyWarning", FOnClickedOutside::CreateLambda([&]() {
	//		SMessageBox::Get()->SetHeaderText(NSLOCTEXT("SSettingsManager", "SSettingsManager.KeyWarning.Title", "Duplicate keyword"));
	//		SMessageBox::Get()->SetContent(NSLOCTEXT("SSettingsManager", "SSettingsManager.KeyWarning.Desc", "Duplicate keyboard or gamepad key was found, maybe not working some keys..."));
	//		SMessageBox::Get()->SetButtonsText(NSLOCTEXT("All", "All.Ok", "OK"));
	//		SMessageBox::Get()->SetEnableClose(false);
	//		SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](EMessageBoxButton Button) {
	//			SMessageBox::RemoveQueueMessage("SSettingsManager.KeyWarning");
	//			SMessageBox::Get()->ToggleWidget(false);
	//		});
	//		SMessageBox::Get()->ToggleWidget(true);
	//	}));
	//}

	auto Scalability = &GameUserSettings->ScalabilityQuality;

	if (Widgets_Changed.Contains("GeneralQuality"))
	{
		GameUserSettings->GeneralQualityLevel = static_cast<EQualityLevels>(Widget_GeneralQuality->GetActiveValueIndex());
	}
	
	if (GameUserSettings->GeneralQualityLevel == EQualityLevels::Personal)
	{
		if (Widgets_Changed.Contains("ViewDistanceQuality"))	Scalability->ViewDistanceQuality	= Widget_ViewDistance->GetActiveValueIndex();
		if (Widgets_Changed.Contains("AntiAliasingQuality"))	Scalability->AntiAliasingQuality	= Widget_AntiAliasing->GetActiveValueIndex();
		if (Widgets_Changed.Contains("ShadowQuality"))			Scalability->ShadowQuality			= Widget_ShadowQuality->GetActiveValueIndex();
		if (Widgets_Changed.Contains("PostProcessQuality"))		Scalability->PostProcessQuality		= Widget_PostProcess->GetActiveValueIndex();
		if (Widgets_Changed.Contains("TextureQuality"))			Scalability->TextureQuality			= Widget_TextureQuality->GetActiveValueIndex();
		if (Widgets_Changed.Contains("EffectsQuality"))			Scalability->EffectsQuality			= Widget_EffectsQuality->GetActiveValueIndex();
		if (Widgets_Changed.Contains("ResolutionQuality"))		Scalability->ResolutionQuality		= Widget_ResolutionQuality->GetValue() * 100.0f;
		if (Widgets_Changed.Contains("FoliageQuality"))			Scalability->FoliageQuality			= Widget_FoliageQuality->GetActiveValueIndex();		
	}
	else
	{		
		Scalability->SetFromSingleQualityLevel(static_cast<int32>(GameUserSettings->GeneralQualityLevel));
	}

	if (Widgets_Changed.Contains("ScreenResolution"))
	{
		FString Width, Height;
		Widget_ScreenResolution->GetActiveValue().ToString().Split("x", &Width, &Height);

		GameUserSettings->SetScreenResolution(FIntPoint(FCString::Atoi(*Width), FCString::Atoi(*Height)));
	}

	if (Widgets_Changed.Contains("Language"))
	{
		for (auto loc : GameUserSettings->GetLanguages())
		{
			if (loc->GetEnglishName() == Widget_Language->GetActiveValue().ToString())
			{
				GameUserSettings->SetCurrentLanguage(loc->GetLCID());
			}
		}
	}

	if (Widgets_Changed.Contains("WindowMode"))
	{
		GameUserSettings->SetFullscreenMode(static_cast<EWindowMode::Type>(Widget_WindowMode->GetActiveValueIndex()));
	}

	if (Widgets_Changed.Contains("AntiAliasingMethod"))
	{
		GameUserSettings->SetAAMode(Widget_AntiAliasingMethod->GetActiveValueIndex());
	}

	if (Widgets_Changed.Contains("SoundMaster"))			GameUserSettings->SetSoundVolume(ESoundClassType::Master, Widget_SoundMaster->GetValue());
	if (Widgets_Changed.Contains("SoundSFX"))				GameUserSettings->SetSoundVolume(ESoundClassType::SFX, Widget_SoundSFX->GetValue());
	if (Widgets_Changed.Contains("SoundUI"))				GameUserSettings->SetSoundVolume(ESoundClassType::UI, Widget_SoundUI->GetValue());
	if (Widgets_Changed.Contains("SoundMusic"))				GameUserSettings->SetSoundVolume(ESoundClassType::Music, Widget_SoundMusic->GetValue());
	if (Widgets_Changed.Contains("SoundVoice"))				GameUserSettings->SetSoundVolume(ESoundClassType::Voice, Widget_SoundVoice->GetValue());

	if (Widgets_Changed.Contains("VSync"))					GameUserSettings->SetVSyncEnabled(Widget_CheckVSync->IsChecked());
	if (Widgets_Changed.Contains("ShowPing"))				GameUserSettings->SetShowPing(Widget_CheckShowPing->IsChecked());
	if (Widgets_Changed.Contains("ShowFPS"))				GameUserSettings->SetShowFPS(Widget_CheckShowFPS->IsChecked());

	if (Widgets_Changed.Contains("AmbientOcclusion"))		GameUserSettings->SetEnabledAmbientOcclusion(Widget_CheckAmbientOcclusion->IsChecked());
	if (Widgets_Changed.Contains("MotionBlur"))				GameUserSettings->SetEnabledMotionBlur(Widget_CheckMotionBlur->IsChecked());
	if (Widgets_Changed.Contains("Bloom"))					GameUserSettings->SetEnabledBloom(Widget_CheckBloom->IsChecked());
	if (Widgets_Changed.Contains("ShowDamageNumbers"))		GameUserSettings->SetEnabledDamageNumbers(Widget_CheckDamageNumbers->IsChecked());
	if (Widgets_Changed.Contains("CVA"))					GameUserSettings->SetAllowChangeViewByAbility(Widget_CheckCVA->IsChecked());


	GameUserSettings->ApplySettings(false);

	// Input
	auto InputSettings = GetMutableDefault<UInputSettings>();

	InputSettings->ActionMappings = Input_ActionMappings;
	InputSettings->AxisMappings = Input_AxisMappings;

	if (Widgets_Changed.Contains("SmoothMouse"))	InputSettings->bEnableMouseSmoothing = Widget_CheckSmoothMouse->IsChecked();

	InputSettings->SaveConfig();

	if (Widgets_Changed.Contains("InvertMouseX"))
	{
		FInputAxisProperties AxisKeyProperties;
		for (TObjectIterator<UPlayerInput> It; It; ++It)
		{			
			if (It->GetAxisProperties(EKeys::MouseX, AxisKeyProperties))
			{
				AxisKeyProperties.bInvert = Widget_CheckInvertMouseX->IsChecked();
				It->SetAxisProperties(EKeys::MouseX, AxisKeyProperties);				
			}
		}

		for (auto& Entry : InputSettings->AxisConfig)
		{
			if (Entry.AxisKeyName == EKeys::MouseX)
			{
				Entry.AxisProperties.bInvert = AxisKeyProperties.bInvert;
			}
		}
	}

	if (Widgets_Changed.Contains("InvertMouseY"))
	{
		FInputAxisProperties AxisKeyProperties;
		for (TObjectIterator<UPlayerInput> It; It; ++It)
		{			
			if (It->GetAxisProperties(EKeys::MouseY, AxisKeyProperties))
			{
				AxisKeyProperties.bInvert = Widget_CheckInvertMouseY->IsChecked();
				It->SetAxisProperties(EKeys::MouseY, AxisKeyProperties);				
			}
		}

		for (auto& Entry : InputSettings->AxisConfig)
		{
			if (Entry.AxisKeyName == EKeys::MouseY)
			{
				Entry.AxisProperties.bInvert = AxisKeyProperties.bInvert;
			}
		}
	}

	if (Widgets_Changed.Contains("MouseSens"))
	{
		for (TObjectIterator<UPlayerInput> It; It; ++It)
		{
			It->SetMouseSensitivity(Widget_MouseSens->GetValue());
		}

		for (auto& Entry : InputSettings->AxisConfig)
		{
			if (Entry.AxisKeyName == EKeys::MouseX || Entry.AxisKeyName == EKeys::MouseY)
			{
				Entry.AxisProperties.Sensitivity = Widget_MouseSens->GetValue();
			}
		}
	}

	for (TObjectIterator<UPlayerInput> It; It; ++It)
	{
		It->ForceRebuildingKeyMaps(true);
	}

	LoadSettings();
}

bool SSettingsManager::CheckInputBinds()
{
	for (auto &i : ControllWidgets)
	{
		for (auto &k : ControllWidgets)
		{
			const bool bIsFirstKeys = (k.Key != i.Key && k.FirstKey == i.FirstKey && k.FirstKey.IsValid() && i.FirstKey.IsValid());
			const bool bIsSecondKeys = (k.Key != i.Key && k.SecondKey == i.SecondKey && k.SecondKey.IsValid() && i.SecondKey.IsValid());
			const bool bIsFirstSecondKeys = (k.Key != i.Key && k.SecondKey == i.FirstKey && k.SecondKey.IsValid() && i.FirstKey.IsValid());

			if (bIsFirstKeys || bIsSecondKeys || bIsFirstSecondKeys)
			{
				return false;
			}
		}
	}

	return true;
}