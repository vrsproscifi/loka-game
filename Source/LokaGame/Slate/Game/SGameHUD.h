// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SGameHUD_Live.h"
#include "SGameHUD_Weapons.h"
#include "SGameHUD_Medals.h"
#include "SGameHUD_Ability.h"

#include "Utilities/SUsableCompoundWidget.h"
#include "Styles/GameHUDWidgetStyle.h"
#include "Weapon/WeaponContainer.h"

class AUTCharacter;

UENUM()
enum class ETimedTextMessage : uint8
{
	None,
	Respawn,
	WaitingPlayers,
	WaitingStart,
	WaitingEnemy,
	End
};

UENUM(BlueprintType, Category = "Loka Game|Slate|Chat")
enum class ESendMessageTo : uint8
{
	None,
	Team,
	All
};

UENUM(BlueprintType)
enum class EActionMessage : uint8
{
	Kill,
	Death,
	Assist,
	InHead,
	Capture,
	TurretKill,
	AbilityKill,
	AbilityAction,
	BuildingKill,
	End
};

DECLARE_DELEGATE_TwoParams(FOnMessageSend, const FText&, const ESendMessageTo)

class LOKAGAME_API SGameHUD : public SUsableCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SGameHUD)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FGameHUDStyle>(TEXT("SGameHUDStyle")))
		, _IsUnion(false)
	{}
	SLATE_EVENT(FOnMessageSend, OnMessageSend)
	SLATE_STYLE_ARGUMENT(FGameHUDStyle, Style)
	SLATE_ARGUMENT(bool, IsUnion)
	SLATE_ARGUMENT(APlayerController*, Controller)
	SLATE_ATTRIBUTE(AUTCharacter*, Character)
	SLATE_END_ARGS()

	/** Constructs this widget with InArgs */
	void Construct(const FArguments& InArgs);

	void SetLiveBarFleshing(const bool _IsHealth = true);

	void ToggleChat(const bool _Toggle);
	void SetIsAutoHideChat(const bool _IsHide);
	void SetIsAutoClearInput(const bool _IsClear);
	void SetMessageSendTo(const ESendMessageTo _SendTo);
	void AppendMessage(const FText &_Player, const FSlateColor &_PlayerColor, const FText &_Message, const ESendMessageTo _SendTo = ESendMessageTo::All);

	FORCEINLINE bool IsChatOpened() const
	{
		return ChatContainer->GetVisibility() == EVisibility::Visible;
	}

	void ScrollChat(const float _Offset);

	virtual void ToggleWidget(const bool) override;
	virtual bool IsInInteractiveMode() const override;

	void SetBottomTextTime(const int32 &_Time);

	void ToggleBottomText(const bool Visible);
	void SetBottomText(const FText &_Text, const bool IsTime = false);

	void SetOnMessageSend(const FOnMessageSend &_Event);

	void AppendAction(const EActionMessage, const int32&);

	void SetCharacter(const TAttribute<AUTCharacter*>&);

	TSharedRef<SGameHUD_Live> GetLiveBar()
	{
		return LiveBar.ToSharedRef();
	}

	TSharedRef<SGameHUD_Weapons> GetWeaponsBar()
	{
		return WeaponBar.ToSharedRef();
	}

	TSharedRef<SGameHUD_Medals> GetMedalsBar()
	{
		return Widget_Medals.ToSharedRef();
	}

private:

	ESendMessageTo eSendMessageTo;
	uint32 bAutoHideChat : 1;
	uint32 bAutoClearInput : 1;

	const FGameHUDStyle *Style;
	FOnMessageSend SendMessageEvent;

	FCurveSequence ShowAnim;
	FCurveHandle ShowColorHandle;

	FCurveSequence RespawnCountAnim;
	FCurveHandle RespawnCountHandle;

	FCurveSequence RespawnVisibleAnim;
	FCurveHandle RespawnVisibleHandle;

	FLinearColor WidgetColor() const;
	FSlateColor RespawnCountColor() const;
	FLinearColor RespawnColor() const;

protected:	

	TAttribute<AUTCharacter*> Character;

	FReply OnSendMessageClicked();
	void OnSendMessage(const FText &_Message, ETextCommit::Type _Type);	
	void OnSendMessageType(ECheckBoxState _State);

	TSharedPtr<SScrollBox> MessagesContainer;
	TSharedPtr<SScrollBox> SimpleContainer;

	TSharedPtr<SVerticalBox> ChatContainer;
	TSharedPtr<SEditableTextBox> ChatInput;
	TSharedPtr<STextBlock> ChatTypeText;
	TSharedPtr<SCheckBox> ChatTypeToggle;

	TSharedPtr<SGameHUD_Live> LiveBar;
	TSharedPtr<SGameHUD_Weapons> WeaponBar;
	TSharedPtr<SGameHUD_Medals> Widget_Medals;
	TSharedPtr<SGameHUD_Ability> Widget_Ability;

	TSharedPtr<SBorder>		RespawnContainer;
	TSharedPtr<STextBlock>	RespawnCounter, RespawnText;

	TSharedPtr<SWidgetSwitcher> BottomSwitcher;
	TSharedPtr<STextBlock> BottomText;

	TSharedPtr<SScrollBox>  ActionsContainer;

	class APlayerController* Controller;

	EActiveTimerReturnType OnBottomTextTime(double InCurrentTime, float InDeltaTime);
	int32 BottomTextTime;
	TSharedPtr<FActiveTimerHandle> BottomTextTime_Handle;

	virtual int32 OnPaint(const FPaintArgs& Args, const FGeometry& AllottedGeometry, const FSlateRect& MyClippingRect, FSlateWindowElementList& OutDrawElements, int32 LayerId, const FWidgetStyle& InWidgetStyle, bool bParentEnabled) const override;
};
