// VRSPRO

#include "LokaGame.h"
#include "SGameHUD_Ability.h"
#include "SlateOptMacros.h"

#include "Layout/SScaleBox.h"
#include "SAnimatedBackground.h"

#include "CharacterAbility.h"
#include "SKeyBinding.h"
#include "UTGameUserSettings.h"

#include "Decorators/LokaTextDecorator.h"
#include "Decorators/LokaResourceDecorator.h"
#include "Decorators/LokaKeyDecorator.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SGameHUD_Ability::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	MIDBrush = FSlateNoResource();

	ChildSlot.VAlign(VAlign_Bottom).Padding(FMargin(520, 40))
	[
		SAssignNew(AnimatedBackground[0], SAnimatedBackground)
		.InitAsHide(true)
		.ShowAnimation(EAnimBackAnimation::DownFade)
		.HideAnimation(EAnimBackAnimation::DownFade)
		.Duration(1.5f)
		.IsRelative(true)
		[
			SNew(SHorizontalBox)
			.Visibility(this, &SGameHUD_Ability::GetAbilityVisibility)
			+ SHorizontalBox::Slot()
			[
				SNew(SOverlay)
				+ SOverlay::Slot()
				.Padding(FMargin(10, 0))
				.VAlign(VAlign_Center)
				.HAlign(HAlign_Right)
				[
					SAssignNew(AnimatedBackground[2], SAnimatedBackground)
					.InitAsHide(true)
					.ShowAnimation(EAnimBackAnimation::RightFade)
					.HideAnimation(EAnimBackAnimation::LeftFade)
					.Duration(1.0f)
					.IsRelative(true)
					[
						SNew(STextBlock) //Name 
						.TextStyle(&Style->Ability.Name)
						.Text(this, &SGameHUD_Ability::GetAbilityName)
					]
				]
				+ SOverlay::Slot()
				.VAlign(VAlign_Center)
				.HAlign(HAlign_Right)
				.Padding(FMargin(20, 0))
				[
					SAssignNew(AnimatedBackground[1], SAnimatedBackground)
					.InitAsHide(true)
					.ShowAnimation(EAnimBackAnimation::RightFade)
					.HideAnimation(EAnimBackAnimation::LeftFade)
					.Duration(1.0f)
					.IsRelative(true)
					[
						SNew(SBorder)
						.BorderImage(&Style->Ability.TipBackground)
						.Padding(10)
						[
							SNew(SRichTextBlock)
							.Text(this, &SGameHUD_Ability::GetAbilityDescription)
							.AutoWrapText(true)
							.TextStyle(&Style->Ability.TipText)
							.DecoratorStyleSet(&FLokaStyle::Get())
							+ SRichTextBlock::Decorator(FLokaTextDecorator::Create(Style->Ability.TipText))
							+ SRichTextBlock::Decorator(FLokaResourceDecorator::Create())
							+ SRichTextBlock::Decorator(FLokaKeyDecorator::Create())
						]
					]
				]
			]
			+ SHorizontalBox::Slot().AutoWidth()
			[
				SNew(SBox)
				.WidthOverride(Style->Ability.MaterialObject.ImageSize.X)
				.HeightOverride(Style->Ability.MaterialObject.ImageSize.Y)
				[
					SNew(SBorder)
					.Padding(0)
					.HAlign(HAlign_Center)
					.VAlign(VAlign_Center)
					.BorderImage(this, &SGameHUD_Ability::GetAbilityBrush)
					[
						SNew(SKeyBinding)
						.WithBackground(false)
						.Key(this, &SGameHUD_Ability::GetAbilityKey)
					]
				]
			]
			+ SHorizontalBox::Slot().FillWidth(.8f)
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SGameHUD_Ability::Init(const TWeakObjectPtr<ACharacterAbility>& InAbility)
{
	Ability = InAbility;

	if (Ability.IsValid())
	{
		if (MID && MID->IsValidLowLevel())
		{
			MIDBrush = Style->Ability.MaterialObject;
			MIDBrush.SetResourceObject(MID);
		}
		else if (auto Mat = Cast<UMaterialInterface>(Style->Ability.MaterialObject.GetResourceObject()))
		{
			MID = UMaterialInstanceDynamic::Create(Mat, GetTransientPackage());
			MIDBrush = Style->Ability.MaterialObject;
			MIDBrush.SetResourceObject(MID);
		}
	}
}

EVisibility SGameHUD_Ability::GetAbilityVisibility() const
{
	return Ability.IsValid() ? EVisibility::HitTestInvisible : EVisibility::Hidden;
}

FText SGameHUD_Ability::GetAbilityName() const
{
	return Ability.IsValid() ? Ability->GetAbilityName() : FText::GetEmpty();
}

FText SGameHUD_Ability::GetAbilityDescription() const
{
	return Ability.IsValid() ? Ability->GetAbilityDescription() : FText::GetEmpty();
}

FKey SGameHUD_Ability::GetAbilityKey() const
{
	return Ability.IsValid() ? Ability->KeyToUse : EKeys::Invalid;
}

const FSlateBrush* SGameHUD_Ability::GetAbilityBrush() const
{
	return &MIDBrush;
}

void SGameHUD_Ability::Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime)
{
	SCompoundWidget::Tick(AllottedGeometry, InCurrentTime, InDeltaTime);

	if (MID && MID->IsValidLowLevel() && Style && Ability.IsValid())
	{
		if (Ability->IsActivated())
		{
			const float UsePercent = Ability->GetAbilityUsePercent();
			IsReverse = FMath::FInterpTo(IsReverse, 1.0f, InDeltaTime, 1.5f);
			MID->SetScalarParameterValue(Style->Ability.MaterialParameter, UsePercent);
			MID->SetScalarParameterValue("IsReverse", IsReverse);
			MIDBrush.TintColor = FMath::Lerp(Style->Ability.UseEmpty, Style->Ability.UseFull, UsePercent);
		}
		else
		{
			const float ReadyPercent = Ability->GetAbilityReadyPercent();
			IsReverse = FMath::FInterpTo(IsReverse, 0.0f, InDeltaTime, 1.5f);
			MID->SetScalarParameterValue(Style->Ability.MaterialParameter, ReadyPercent);
			MID->SetScalarParameterValue("IsReverse", IsReverse);
			MIDBrush.TintColor = FMath::Lerp(Style->Ability.ReadyEmpty, Style->Ability.ReadyFull, ReadyPercent);
		}
	}

	TimeAfterShowing += InDeltaTime;
	if (bHiddenTip == false && TimeAfterShowing >= 10.0f)
	{
		bHiddenTip = true;
		AnimatedBackground[1]->ToggleWidget(false);
		AnimatedBackground[2]->ToggleWidget(true);
	}
}

void SGameHUD_Ability::AddReferencedObjects(FReferenceCollector& Collector)
{
	Collector.AddReferencedObject(MID);
}

void SGameHUD_Ability::ToggleWidget(const bool Toggle)
{
	AnimatedBackground[0]->ToggleWidget(Toggle);
	AnimatedBackground[1]->ToggleWidget(Toggle);

	if (Toggle)
	{
		TimeAfterShowing = .0f;
		bHiddenTip = false;
		AnimatedBackground[2]->ToggleWidget(false);
	}
}
