// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "SGameContainer.h"
// Blur
#include "SBackgroundBlur.h"
#include "SAnimatedBackground.h"
#include "SVerticalMenuBuilder.h"
#include "Windows/SWindowsContainer.h"
#include "SSettingsManager.h"
#include "SNotifyList.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SGameContainer::Construct(const FArguments& InArgs)
{
	OnRequestLeave = InArgs._OnRequestLeave;

#pragma region WidgetConstruction

	TArray<FText> _TDMMenu;
	_TDMMenu.Add(NSLOCTEXT("TDMMenu", "ButtonContinue", "Continue"));			// EPauseMenuButtons::Continue
	_TDMMenu.Add(NSLOCTEXT("TDMMenu", "ButtonSettings", "Settings"));			// EPauseMenuButtons::SwitchTeam
	_TDMMenu.Add(NSLOCTEXT("TDMMenu", "ButtonLeaveMatch", "Leave this match"));	// EPauseMenuButtons::ReturnToLobby

	ChildSlot
	[
		SNew(SOverlay)
		+ SOverlay::Slot()
		[
			SAssignNew(Widget_Background, SAnimatedBackground)
			.Duration(.5f)
			.Ease(ECurveEaseFunction::QuadIn)
			.IsEnabledBlur(true)
			.ShowAnimation(EAnimBackAnimation::Color)
			.HideAnimation(EAnimBackAnimation::Color)
		]
		+ SOverlay::Slot()
		[
			SAssignNew(Widget_WindowsManager, SWindowsContainer)
			.IsNeedQuickPanel(false)
			.OnRequestToggle(this, &SGameContainer::ToggleWidget)
		]
		+ SOverlay::Slot().HAlign(HAlign_Center).VAlign(VAlign_Center)
		[
			SAssignNew(Widget_EscapeMenu, SVerticalMenuBuilder)
			.IsDoubleClickProtection(true)
			.ButtonsText(_TDMMenu)
			.OnClickAnyButton(this, &SGameContainer::OnEscapeMenuButton)
		]			
	];

#pragma endregion 

	Widget_WindowsManager->AddSlot()
	[
		SAssignNew(Widget_SettingsManager, SSettingsManager, Widget_WindowsManager.ToSharedRef())
	];

	Widget_WindowsManager->AddSlot()
	[
		SNotifyList::Get()
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SGameContainer::ToggleWidget(const bool InToggle)
{
	if (InToggle || (InToggle == false && Widget_WindowsManager->RequestToggle(false, false)))
	{
		SetVisibility(InToggle ? EVisibility::SelfHitTestInvisible : EVisibility::HitTestInvisible);
		Widget_Background->ToggleWidget(InToggle);
		Widget_EscapeMenu->ToggleWidget(InToggle);
		OnToggleInteractive.ExecuteIfBound(InToggle);
	}
}

bool SGameContainer::IsInInteractiveMode() const
{
	return GetVisibility() == EVisibility::SelfHitTestInvisible;
}

void SGameContainer::OnEscapeMenuButton(uint8 InButtonIndex)
{
	if (InButtonIndex == 1)
	{
		Widget_SettingsManager->ToggleWidget(true);
	}
	else if (InButtonIndex == 2)
	{
		OnRequestLeave.ExecuteIfBound();
	}

	if (Widget_WindowsManager->IsAnyClosableOpened())
	{
		Widget_EscapeMenu->ToggleWidget(false);
	}
	else
	{
		ToggleWidget(false);
	}
}
