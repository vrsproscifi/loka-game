// VRSPRO

#pragma once

#include "SGeneralBackground.h"

#include "Styles/GameResultsWidgetStyle.h"


class AUTGameState;
class AUTPlayerState;

class LOKAGAME_API SGameResults : public SGeneralBackground
{
public:
	SLATE_BEGIN_ARGS(SGameResults)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FGameResultsStyle>("SGameResultsStyle"))
		, _MatchWinner(INDEX_NONE)
	{}
	SLATE_STYLE_ARGUMENT(FGameResultsStyle, Style)
	SLATE_ARGUMENT(SGeneralBackground::FArguments, BackgroundArguments)
	SLATE_ARGUMENT(int32, MatchWinner)
	SLATE_ARGUMENT(AUTGameState*, GameState)
	SLATE_ARGUMENT(AUTPlayerState*, PlayerState)
	SLATE_END_ARGS()

	/** Constructs this widget with InArgs */
	void Construct(const FArguments& InArgs);

	void AddAchievement(const int32 &_index, const FText &_text);
	void AddAchievement(const FString &_tag, const FText &_text);

	void AddAchievement(const int32 &_index, const int32 &_amount);
	void AddAchievement(const FString &_tag, const int32 &_amount);

	void AddAchievementCustom(const FSlateBrush *, const FText &);

private:

	const FGameResultsStyle *Style;

protected:

	AUTGameState* GameState;
	AUTPlayerState* PlayerState;

	TSharedPtr<SScrollBox> GreenTeamPlayersList;
	TSharedPtr<SScrollBox> RedTeamPlayersList;

	TSharedPtr<SScrollBox> AchievementsBox;

	TSharedPtr<SRichTextBlock> Widget_TipsText;

	TArray<FText> TipsText;
	int32 CurrentTip;
};
