// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SlateBasics.h"
#include "Utilities/SUsableCompoundWidget.h"
#include "Styles/BackgroundBlurWidgetStyle.h"

class SSettingsManager;
class SWindowsContainer;
class SVerticalMenuBuilder;
class SAnimatedBackground;

class LOKAGAME_API SGameContainer : public SUsableCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SGameContainer)
	{
		_Visibility = EVisibility::HitTestInvisible;
	}
	SLATE_EVENT(FOnClickedOutside, OnRequestLeave)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	// SUsableCompoundWidget begin
	virtual void ToggleWidget(const bool InToggle) override;
	virtual bool IsInInteractiveMode() const override;
	// SUsableCompoundWidget end

protected:

	FOnClickedOutside OnRequestLeave;

	TSharedPtr<SAnimatedBackground> Widget_Background;
	TSharedPtr<SVerticalMenuBuilder> Widget_EscapeMenu;
	TSharedPtr<SWindowsContainer> Widget_WindowsManager;
	TSharedPtr<SSettingsManager> Widget_SettingsManager;

	void OnEscapeMenuButton(uint8 InButtonIndex);
};
