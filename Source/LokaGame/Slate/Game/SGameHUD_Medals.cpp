// VRSPRO

#include "LokaGame.h"
#include "SGameHUD_Medals.h"
#include "SlateOptMacros.h"

#include "Layout/SScaleBox.h"
#include "SAnimatedBackground.h"

#include "Private/Application/ActiveTimerHandle.h"

class LOKAGAME_API SGameHUD_Medal : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SGameHUD_Medal)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FGameHUDStyle>(TEXT("SGameHUDStyle")))
		, _IsFast(false)
	{
		_Visibility = EVisibility::HitTestInvisible;
	}
	SLATE_STYLE_ARGUMENT(FGameHUDStyle, Style)
	SLATE_ARGUMENT(const FSlateBrush*, Icon)
	SLATE_ARGUMENT(FText, Text)
	SLATE_ARGUMENT(const FSlateSound*, Sound)
	SLATE_ARGUMENT(bool, IsFast)
	SLATE_EVENT(FOnClickedOutside, OnNotifyToRemove)
	SLATE_END_ARGS()

	BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
	void Construct(const FArguments& InArgs)
	{
		Style = InArgs._Style;
		Sound = InArgs._Sound;
		IsFast = InArgs._IsFast;
		OnNotifyToRemove = InArgs._OnNotifyToRemove;

		ChildSlot
		[
			SAssignNew(Widget_AnimMain, SAnimatedBackground)
			.ShowAnimation(EAnimBackAnimation::UpFade)
			.HideAnimation(EAnimBackAnimation::Color)
			.Duration(IsFast ? .25f : .5f)
			.InitAsHide(true)
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot()
				[
					SAssignNew(Widget_AnimImage, SAnimatedBackground)
					.ShowAnimation(EAnimBackAnimation::Color)
					.HideAnimation(EAnimBackAnimation::Color)
					.Duration(IsFast ? .4f : .8f)
					.InitAsHide(true)
					[
						SNew(SBox).WidthOverride(100).HeightOverride(100)
						[
							SNew(SScaleBox)
							.Stretch(EStretch::ScaleToFit)
							[
								SNew(SImage).Image(InArgs._Icon)
							]
						]
					]
				]
				+ SVerticalBox::Slot()
				[
					SNew(STextBlock)
					.Margin(FMargin(0, 4))
					.Justification(ETextJustify::Center)
					.TextStyle(&Style->MedalText)
					.Text(InArgs._Text)
				]
			]
		];
	}
	END_SLATE_FUNCTION_BUILD_OPTIMIZATION

	void StartDraw()
	{
		timer_Hide = RegisterActiveTimer(IsFast ? 1.4f : 3.8f, FWidgetActiveTimerDelegate::CreateSP(this, &SGameHUD_Medal::OnHide));

		Widget_AnimMain->ToggleWidget(true);
		Widget_AnimImage->ToggleWidget(true);

		if (Sound)
		{
			FSlateApplication::Get().PlaySound(*Sound);
		}
	}

	void SetIsFast(const bool InIsFast = true)
	{
		IsFast = InIsFast;

		Widget_AnimMain->SetDuration(IsFast ? .25f : .5f);
		Widget_AnimImage->SetDuration(IsFast ? .4f : .8f);

		if (timer_Hide.IsValid())
		{
			UnRegisterActiveTimer(timer_Hide.ToSharedRef());
			timer_Hide = RegisterActiveTimer(IsFast ? 1.4f : 3.8f, FWidgetActiveTimerDelegate::CreateSP(this, &SGameHUD_Medal::OnHide));
		}

		if (timer_Remove.IsValid())
		{
			UnRegisterActiveTimer(timer_Remove.ToSharedRef());
			timer_Remove = RegisterActiveTimer(IsFast ? .5f : 1.0f, FWidgetActiveTimerDelegate::CreateSP(this, &SGameHUD_Medal::OnEnd));
		}
	}

protected:

	const FGameHUDStyle* Style;
	const FSlateSound* Sound;
	bool IsFast;

	TSharedPtr<SAnimatedBackground> Widget_AnimMain, Widget_AnimImage;
	FOnClickedOutside OnNotifyToRemove;

	TSharedPtr<FActiveTimerHandle> timer_Hide, timer_Remove;

	EActiveTimerReturnType OnHide(double InCurrentTime, float InDeltaTime)
	{
		Widget_AnimMain->ToggleWidget(false);
		Widget_AnimImage->ToggleWidget(false);

		timer_Remove = RegisterActiveTimer(IsFast ? .5f : 1.0f, FWidgetActiveTimerDelegate::CreateSP(this, &SGameHUD_Medal::OnEnd));

		return EActiveTimerReturnType::Stop;
	}

	EActiveTimerReturnType OnEnd(double InCurrentTime, float InDeltaTime)
	{
		OnNotifyToRemove.ExecuteIfBound();
		return EActiveTimerReturnType::Stop;
	}
};


BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SGameHUD_Medals::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SGameHUD_Medals::AddMedal(const FSlateBrush* Icon, const FText& Text, const FSlateSound* Sound)
{
	if (QueueMedals.Num())
	{
		QueueMedals[0]->SetIsFast();
	}

	QueueMedals.Insert(SNew(SGameHUD_Medal).Icon(Icon).Text(Text).Sound(Sound).OnNotifyToRemove(this, &SGameHUD_Medals::OnRemoveMedal), 0);

	if (QueueMedals.Num() == 1)
	{
		ChildSlot.AttachWidget(QueueMedals[0].ToSharedRef());
		QueueMedals[0]->StartDraw();
	}
}

void SGameHUD_Medals::OnRemoveMedal()
{
	QueueMedals.RemoveAt(QueueMedals.Num() - 1);

	if (QueueMedals.Num())
	{
		ChildSlot.AttachWidget(QueueMedals.Last().ToSharedRef());
		QueueMedals.Last()->StartDraw();
	}
}