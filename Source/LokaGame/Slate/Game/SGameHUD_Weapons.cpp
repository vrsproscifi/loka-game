// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "SGameHUD_Weapons.h"
#include "ToggableWidgetHelper.h"
// Experimental
#include "UTCharacter.h"
#include "Grenade/ItemGrenadeEntity.h"
#include "Weapon/ShooterWeapon.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SGameHUD_Weapons::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	Character = InArgs._Character;

	NoResource = new FSlateNoResource();

	//.Offset(FMargin(14, -36, 720, 200))
	ChildSlot.HAlign(HAlign_Right).VAlign(VAlign_Bottom).Padding(FMargin(0, 0, 14, 36))
	[
		SNew(SBox)
		.WidthOverride(720)
		.HeightOverride(200)
		.RenderTransform(Style->WeaponBarTransform.ToSlateRenderTransform())
		.RenderTransformPivot(Style->WeaponBarTransformPivot)
		[
			SNew(SBorder)
			.BorderBackgroundColor(FLinearColor::Transparent)
			.ColorAndOpacity(this, &SGameHUD_Weapons::WidgetColor)
			.Padding(0)
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot()
				.HAlign(EHorizontalAlignment::HAlign_Center)
				.VAlign(EVerticalAlignment::VAlign_Center)
				.AutoWidth()
				[
					SNew(SBox)
					.RenderTransform(FVector2D(Style->WeaponBlocksLargeOffset, Style->WeaponBlocksUpDownOffset))
					.WidthOverride(Style->WeaponBlocksSize.X)
					.HeightOverride(Style->WeaponBlocksSize.Y)
					[
						SNew(SBorder)
						.Padding(0)
						.BorderImage(&Style->WeaponBackgroundLarge)
						[
							SNew(SOverlay)
							+ SOverlay::Slot()
							.HAlign(EHorizontalAlignment::HAlign_Center)
							.VAlign(EVerticalAlignment::VAlign_Center)
							[
								SNew(SBox)
								.WidthOverride(Style->WeaponBlocksSize.Y)
								.HeightOverride(Style->WeaponBlocksSize.Y)
								[
									SAssignNew(WeaponIcon[EShooterWeaponSlot::Primary], SImage)
									.RenderTransform(FSlateRenderTransform(FScale2D(-1, 1), FVector2D(Style->WeaponBlocksSize.Y, 0)))
									.Image(this, &SGameHUD_Weapons::GetWeaponIcon, EShooterWeaponSlot::Primary)
								]
							]
							+ SOverlay::Slot()
							.HAlign(EHorizontalAlignment::HAlign_Center)
							.VAlign(EVerticalAlignment::VAlign_Bottom)
							[
								SNew(SHorizontalBox).RenderTransform(Style->TranslationLargeText)
								+ SHorizontalBox::Slot().AutoWidth()
								[
									SAssignNew(AmmoClip[EShooterWeaponSlot::Primary], STextBlock)
									.TextStyle(&Style->AmmoText)
									.Justification(ETextJustify::Right)
									.Text(this, &SGameHUD_Weapons::GetWeaponClipAmmo, EShooterWeaponSlot::Primary)
								]
								+ SHorizontalBox::Slot().AutoWidth()
								[
									SAssignNew(AmmoTotal[EShooterWeaponSlot::Primary], STextBlock)
									.TextStyle(&Style->AmmoText)
									.Justification(ETextJustify::Left)
									.Text(this, &SGameHUD_Weapons::GetWeaponTotalAmmo, EShooterWeaponSlot::Primary)
								]
							]
							+ SOverlay::Slot()
							.HAlign(EHorizontalAlignment::HAlign_Fill)
							.VAlign(EVerticalAlignment::VAlign_Fill)
							[
								SAssignNew(Glow[EShooterWeaponSlot::Primary], SBorder)
								.BorderImage(&Style->WeaponGlowLarge)
								.Visibility(this, &SGameHUD_Weapons::IsWeaponSelected, EShooterWeaponSlot::Primary)
							]
						]
					]
				]
				+ SHorizontalBox::Slot()
				.HAlign(EHorizontalAlignment::HAlign_Center)
				.VAlign(EVerticalAlignment::VAlign_Center)
				.AutoWidth()
				[
					SNew(SBox)
					.RenderTransform(FVector2D(Style->WeaponBlocksMediumOffset, 0))
					.WidthOverride(Style->WeaponBlocksSize.X)
					.HeightOverride(Style->WeaponBlocksSize.Y)
					[
						SNew(SBorder)
						.Padding(0)
						.BorderImage(&Style->WeaponBackgroundMedium)
						[
							SNew(SOverlay)
							+ SOverlay::Slot()
							.HAlign(EHorizontalAlignment::HAlign_Center)
							.VAlign(EVerticalAlignment::VAlign_Center)
							[
								SNew(SBox)
								.WidthOverride(Style->WeaponBlocksSize.Y*0.6)
								.HeightOverride(Style->WeaponBlocksSize.Y*0.6)
								[
									SAssignNew(WeaponIcon[EShooterWeaponSlot::Secondary], SImage)
									.RenderTransform(FSlateRenderTransform(FScale2D(-1, 1), FVector2D(Style->WeaponBlocksSize.Y*0.6, 0)))
									.Image(this, &SGameHUD_Weapons::GetWeaponIcon, EShooterWeaponSlot::Secondary)
								]
							]
							+ SOverlay::Slot()
							.HAlign(EHorizontalAlignment::HAlign_Center)
							.VAlign(EVerticalAlignment::VAlign_Bottom)
							[
								SNew(SHorizontalBox).RenderTransform(Style->TranslationMediumText)
								+ SHorizontalBox::Slot().AutoWidth()
								[
									SAssignNew(AmmoClip[EShooterWeaponSlot::Secondary], STextBlock)
									.TextStyle(&Style->AmmoText)
									.Justification(ETextJustify::Right)
									.Text(this, &SGameHUD_Weapons::GetWeaponClipAmmo, EShooterWeaponSlot::Secondary)
								]
								+ SHorizontalBox::Slot().AutoWidth()
								[
									SAssignNew(AmmoTotal[EShooterWeaponSlot::Secondary], STextBlock)
									.TextStyle(&Style->AmmoText)
									.Justification(ETextJustify::Left)
									.Text(this, &SGameHUD_Weapons::GetWeaponTotalAmmo, EShooterWeaponSlot::Secondary)
								]
							]
							+ SOverlay::Slot()
							.HAlign(EHorizontalAlignment::HAlign_Fill)
							.VAlign(EVerticalAlignment::VAlign_Fill)
							[
								SAssignNew(Glow[EShooterWeaponSlot::Secondary], SBorder)
								.BorderImage(&Style->WeaponGlowMedium)
								.Visibility(this, &SGameHUD_Weapons::IsWeaponSelected, EShooterWeaponSlot::Secondary)
							]
						]
					]
				]
				+ SHorizontalBox::Slot()
				.HAlign(EHorizontalAlignment::HAlign_Center)
				.VAlign(EVerticalAlignment::VAlign_Center)
				.AutoWidth()
				[
					SNew(SBox)
					.RenderTransform(FVector2D(0, -Style->WeaponBlocksUpDownOffset))
					.WidthOverride(Style->WeaponBlocksSize.X)
					.HeightOverride(Style->WeaponBlocksSize.Y)
					[
						SNew(SBorder)
						.Padding(0)
						.BorderImage(&Style->WeaponBackgroundSmall)
						[
							SNew(SOverlay)
							+ SOverlay::Slot()
							.HAlign(EHorizontalAlignment::HAlign_Center)
							.VAlign(EVerticalAlignment::VAlign_Center)
							[
								SNew(SBox)
								.WidthOverride(Style->WeaponBlocksSize.Y*0.5)
								.HeightOverride(Style->WeaponBlocksSize.Y*0.5)
								[
									SAssignNew(WeaponIcon[EShooterWeaponSlot::Grenade], SImage)
									.RenderTransform(FSlateRenderTransform(FScale2D(-1, 1), FVector2D(Style->WeaponBlocksSize.Y*0.5, 0)))
									.Image(this, &SGameHUD_Weapons::GetWeaponIcon, EShooterWeaponSlot::Grenade)
								]
							]
							+ SOverlay::Slot()
							.HAlign(EHorizontalAlignment::HAlign_Center)
							.VAlign(EVerticalAlignment::VAlign_Center)
							[
								SAssignNew(AmmoTotal[EShooterWeaponSlot::Grenade], STextBlock)
								.RenderTransform(Style->TranslationSmallText)
								.TextStyle(&Style->AmmoText)
								.Justification(ETextJustify::Left)
								.Text(this, &SGameHUD_Weapons::GetWeaponTotalAmmo, EShooterWeaponSlot::Grenade)
							]
							+ SOverlay::Slot()
							.HAlign(EHorizontalAlignment::HAlign_Fill)
							.VAlign(EVerticalAlignment::VAlign_Fill)
							[
								SAssignNew(Glow[EShooterWeaponSlot::Grenade], SBorder)
								.BorderImage(&Style->WeaponGlowSmall)
								.Visibility(this, &SGameHUD_Weapons::IsWeaponSelected, EShooterWeaponSlot::Grenade)
							]
						]
					]
				]
			]
		]
	];

	ShowColorHandle = ShowAnim.AddCurve(0.0f, 1.5f, ECurveEaseFunction::QuadOut);
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

#define WEAPON_CHECK_BY_SLOT if (Character.Get() && Character.Get()->Inventory[Slot])
#define WEAPON_CHECK_BY_SLOT_EQUAL if (Character.Get() && Character.Get()->Inventory[Slot] && Character.Get()->Inventory[Slot] == Character.Get()->GetWeapon())

void SGameHUD_Weapons::SetCharacter(const TWeakObjectPtr<AUTCharacter>& InCharacter)
{
	Character = InCharacter;
}

EVisibility SGameHUD_Weapons::IsWeaponSelected(const EShooterWeaponSlot Slot) const
{
	if (Character.IsValid() && Character->Inventory[Slot])
	{
		return (Character->Inventory[Slot] == Character->GetWeapon()) ? EVisibility::Visible : EVisibility::Hidden;
	}
	return EVisibility::Hidden;
}

const FSlateBrush* SGameHUD_Weapons::GetWeaponIcon(const EShooterWeaponSlot Slot) const
{
	if (Slot == EShooterWeaponSlot::Grenade)
	{
		if (Character.IsValid() && Character->Grenades.Num() && Character->Grenades[0])
		{
			return Character->Grenades[0]->GetIcon();
		}
	}
	else if (Character.IsValid() && Character->Inventory[Slot] && Character->Inventory[Slot])
	{
		if (const auto instance = Character->Inventory[Slot]->GetInstanceEntity())
		{
			return instance->GetIcon();
		}
	}
	return NoResource;
}

FText SGameHUD_Weapons::GetWeaponClipAmmo(const EShooterWeaponSlot Slot) const
{
	if (Character.IsValid())
	{
		if (const auto item = Character->Inventory[Slot])
		{
			return FText::AsNumber(item->GetCurrentAmmoInClip(), &FNumberFormattingOptions::DefaultNoGrouping());
		}
	}

	return FText::AsNumber(0);
}

FText SGameHUD_Weapons::GetWeaponTotalAmmo(const EShooterWeaponSlot Slot) const
{
	if (Slot == EShooterWeaponSlot::Grenade)
	{
		if (Character.IsValid())
		{
			return FText::AsNumber(Character->Grenades.Num());
		}
	}
	else
	{
		if (Character.IsValid())
		{
			if (const auto item = Character->Inventory[Slot])
			{
				const int32 _ammo = FMath::Max(item->GetCurrentAmmo() - item->GetCurrentAmmoInClip(), 0);
				return FText::FromString(FString("/" + FString::FromInt(_ammo)));
			}
		}
	}

	return FText::FromString(FString("/" + FString::FromInt(0)));
}

void SGameHUD_Weapons::ToggleWidget(const bool Toggle)
{
	FToggableWidgetHelper::ToggleWidget(AsShared(), ShowAnim, Toggle);
}
