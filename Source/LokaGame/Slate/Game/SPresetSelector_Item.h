// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Styles/PresetSelectorWidgetStyle.h"

class UInventoryComponent;
struct FClientPreSetData;
struct FPresetDisplayData
{
	bool IsAllow;
	FString Name;

	TAttribute<const FSlateBrush*> ImageOne;
	TAttribute<const FSlateBrush*> ImageTwo;
	TAttribute<const FSlateBrush*> ImageClass;

	FText Character;

	FPresetDisplayData() : IsAllow(false), Name(), ImageOne(new FSlateNoResource()), ImageTwo(new FSlateNoResource()), ImageClass(new FSlateNoResource()), Character() {}
};

class LOKAGAME_API SPresetSelector_Item : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SPresetSelector_Item)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FPresetSelectorStyle>("SPresetSelectorStyle"))
	{}
	SLATE_STYLE_ARGUMENT(FPresetSelectorStyle, Style)
	SLATE_ATTRIBUTE(const UInventoryComponent*, InventoryComponent)
	SLATE_ATTRIBUTE(const FClientPreSetData*, ProfileData)
	SLATE_ATTRIBUTE(bool, IsSelected)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

protected:

	const FPresetSelectorStyle* Style;
	FSlateBrush EmptyBrush;

	TAttribute<bool> IsSelected;
	TAttribute<const FClientPreSetData*> ProfileData;
	TAttribute<const UInventoryComponent*> InventoryComponent;

	const FSlateBrush* GetBackgroundImage() const;
	const FSlateBrush* GetBorderImage() const;

	const FSlateBrush* GetBrushPrimaryWeapon() const;
	const FSlateBrush* GetBrushCharacter() const;
	const FSlateBrush* GetBrushCharacterAbility() const;

	int32 GetWidgetIndexIsEnabled() const;

	FText GetTextName() const;
	FText GetTextCharacter() const;
};
