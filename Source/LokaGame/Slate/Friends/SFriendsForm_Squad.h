//// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Styles/FriendsFormWidgetStyle.h"
#include "SquadComponent/SqaudMemberInformation.h"

struct FPlayerEntityInfo;

struct FSquadColumn
{
	static FName Status;
	static FName Name;
	static FName Action;
};

class LOKAGAME_API SFriendsForm_Squad : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SFriendsForm_Squad)
		: _Style(&FLokaStyle::Get().GetWidgetStyle<FFriendsFormStyle>("SFriendsFormStyle"))
	{}
	SLATE_STYLE_ARGUMENT(FFriendsFormStyle, Style)
	SLATE_EVENT(FOnClickedOutside, OnRemoveMember)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs, TSharedRef<FQueueMemberContainer> InInstance);

protected:

	const FFriendsFormStyle* Style;
	FOnClickedOutside OnRemoveMember;
	TSharedPtr<FQueueMemberContainer> Instance;
};
