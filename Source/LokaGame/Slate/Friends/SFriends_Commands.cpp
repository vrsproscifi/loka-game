#include "LokaGame.h"
#include "SFriends_Commands.h"
//#include "LobbyGameMode.h"
#include "SMessageBox.h"
#include "SWindowDuelRules.h"
#include "BasePlayerState.h"
#include "FriendComponent.h"
#include "SquadComponent.h"
#include "ConstructionGameState.h"
#include "ConstructionComponent.h"
#include "StringTableRegistry.h"

void SFriendsCommands::RegisterCommands()
{
	FUICommandInfo::MakeCommandInfo(
		this->AsShared(),
		Commands[PrivateMessage],
		TEXT("InviteMessage"),
		NSLOCTEXT("SFriendsCommands", "Command.PrivateMessage", "Private Message"),
		FText::GetEmpty(),
		FSlateIcon(),
		EUserInterfaceActionType::Button,
		FInputChord()
	);

	FUICommandInfo::MakeCommandInfo(
		this->AsShared(),
		Commands[AddBlockList],
		TEXT("AddBlockList"),
		NSLOCTEXT("SFriendsCommands", "Command.AddBlockList", "Add to black list"),
		FText::GetEmpty(),
		FSlateIcon(),
		EUserInterfaceActionType::Button,
		FInputChord()
	);

	FUICommandInfo::MakeCommandInfo(
		this->AsShared(),
		Commands[AddFriend],
		TEXT("AddFriend"),
		NSLOCTEXT("SFriendsCommands", "Command.AddFriend", "Add Friend"),
		FText::GetEmpty(),
		FSlateIcon(),
		EUserInterfaceActionType::Button,
		FInputChord()
		);

	FUICommandInfo::MakeCommandInfo(
		this->AsShared(),
		Commands[AcceptInvite],
		TEXT("AcceptInvite"),
		NSLOCTEXT("SFriendsCommands", "Command.AcceptInvite", "Accept Invite"),
		FText::GetEmpty(),
		FSlateIcon(),
		EUserInterfaceActionType::Button,
		FInputChord()
	);

	FUICommandInfo::MakeCommandInfo(
		this->AsShared(),
		Commands[DeclineInvite],
		TEXT("DeclineInvite"),
		NSLOCTEXT("SFriendsCommands", "Command.DeclineInvite", "Decline Invite"),
		FText::GetEmpty(),
		FSlateIcon(),
		EUserInterfaceActionType::Button,
		FInputChord()
	);

	FUICommandInfo::MakeCommandInfo(
		this->AsShared(),
		Commands[DeleteFriend],
		TEXT("DeleteFriend"),
		NSLOCTEXT("SFriendsCommands", "Command.Delete", "Delete"),
		FText::GetEmpty(),
		FSlateIcon(),
		EUserInterfaceActionType::Button,
		FInputChord()
	);

	FUICommandInfo::MakeCommandInfo(
		this->AsShared(),
		Commands[SquadInvite],
		TEXT("SquadInvite"),
		NSLOCTEXT("SFriendsCommands", "Command.SquadInvite", "Squad Invite"),
		FText::GetEmpty(),
		FSlateIcon(),
		EUserInterfaceActionType::Button,
		FInputChord()
	);

	FUICommandInfo::MakeCommandInfo(
		this->AsShared(),
		Commands[SquadRequest],
		TEXT("SquadRequest"),
		NSLOCTEXT("SFriendsCommands", "Command.SquadRequest", "Squad Request"),
		FText::GetEmpty(),
		FSlateIcon(),
		EUserInterfaceActionType::Button,
		FInputChord()
	);

	FUICommandInfo::MakeCommandInfo(
		this->AsShared(),
		Commands[DuelInvite],
		TEXT("DuelInvite"),
		NSLOCTEXT("SFriendsCommands", "Command.DuelInvite", "Duel Invite"),
		FText::GetEmpty(),
		FSlateIcon(),
		EUserInterfaceActionType::Button,
		FInputChord()
	);

	FUICommandInfo::MakeCommandInfo(
		this->AsShared(),
		Commands[ConstructionInvite],
		TEXT("ConstructionInvite"),
		NSLOCTEXT("SFriendsCommands", "Command.ConstructionInvite", "Construction Invite"),
		FText::GetEmpty(),
		FSlateIcon(),
		EUserInterfaceActionType::Button,
		FInputChord()
	);

	FUICommandInfo::MakeCommandInfo(
		this->AsShared(),
		Commands[ConstructionRequest],
		TEXT("ConstructionRequest"),
		NSLOCTEXT("SFriendsCommands", "Command.ConstructionRequest", "Construction Request"),
		FText::GetEmpty(),
		FSlateIcon(),
		EUserInterfaceActionType::Button,
		FInputChord()
	);

	FUICommandInfo::MakeCommandInfo(
		this->AsShared(),
		Commands[Attack],
		TEXT("Attack"),
		NSLOCTEXT("SFriendsCommands", "Command.Attack", "Attack on player"),
		FText::GetEmpty(),
		FSlateIcon(),
		EUserInterfaceActionType::Button,
		FInputChord()
	);

	FUICommandInfo::MakeCommandInfo(
		this->AsShared(),
		Commands[GuestView],
		TEXT("GuestView"),
		NSLOCTEXT("SFriendsCommands", "Command.GuestView", "Entry as guest"),
		FText::GetEmpty(),
		FSlateIcon(),
		EUserInterfaceActionType::Button,
		FInputChord()
	);

	UICommandList = MakeShareable(new FUICommandList());
	
	UICommandList->MapAction(Commands[AddFriend], FExecuteAction::CreateLambda([&]() 
	{
		UE_LOG(LogInit, Error, TEXT("> SFriendsCommands::AddFriend | PlayerState: %d | GetFriendComponent: %d"), PlayerState.IsValid(), GetFriendComponent() != nullptr);

		if (auto MyComponent = GetFriendComponent())
		{
			MyComponent->SendRequestAdd(CurrentTargetId);
		}

		if (OnAnyAction.IsBound()) OnAnyAction.Broadcast();
	}));

	UICommandList->MapAction(Commands[AddBlockList], FExecuteAction::CreateLambda([&]() {
		//OnAddFriend.ExecuteIfBound(FAddFriendRequestContainer(CurrentTargetId, true));
		if (OnAnyAction.IsBound()) OnAnyAction.Broadcast();
	}));

	UICommandList->MapAction(Commands[AcceptInvite], FExecuteAction::CreateLambda([&]() {
		if (auto MyComponent = GetFriendComponent())
		{
			MyComponent->SendRequestRespond(FAddFriendRequestRespond(CurrentTargetId, true));
		}

		if (OnAnyAction.IsBound()) OnAnyAction.Broadcast();
	}));

	UICommandList->MapAction(Commands[DeclineInvite], FExecuteAction::CreateLambda([&]() {
		if (auto MyComponent = GetFriendComponent())
		{
			MyComponent->SendRequestRespond(FAddFriendRequestRespond(CurrentTargetId, false));
		}

		if (OnAnyAction.IsBound()) OnAnyAction.Broadcast();
	}));

	UICommandList->MapAction(Commands[SquadInvite], FExecuteAction::CreateLambda([&]() 
	{
		UE_LOG(LogInit, Error, TEXT("> SFriendsCommands::SquadInvite | PlayerState: %d | GetFriendComponent: %d"), PlayerState.IsValid(), GetFriendComponent() != nullptr);

		if (auto MyComponent = GetSquadComponent())
		{
			MyComponent->SendRequestAdd(CurrentTargetId, FSessionMatchOptions(), ESquadInviteDirection::Invite);
		}

		if (OnAnyAction.IsBound()) OnAnyAction.Broadcast();
	}));

	UICommandList->MapAction(Commands[SquadRequest], FExecuteAction::CreateLambda([&]()
	{
		UE_LOG(LogInit, Error, TEXT("> SFriendsCommands::SquadRequest | PlayerState: %d | GetFriendComponent: %d"), PlayerState.IsValid(), GetFriendComponent() != nullptr);

		if (auto MyComponent = GetSquadComponent())
		{
			MyComponent->SendRequestAdd(CurrentTargetId, FSessionMatchOptions(), ESquadInviteDirection::Request);
		}

		if (OnAnyAction.IsBound()) OnAnyAction.Broadcast();
	}));

	UICommandList->MapAction(Commands[DuelInvite], FExecuteAction::CreateLambda([&]() {
		QueueMessageBegin("MakeDuelWindow")
			auto WindowContent = SNew(SWindowDuelRules);
			SMessageBox::Get()->SetHeaderText(NSLOCTEXT("MakeDuelWindow", "MakeDuelWindow.Title", "Make Duel"));
			SMessageBox::Get()->SetContent(WindowContent);
			SMessageBox::Get()->SetButtonsText(LOCTABLE("BaseStrings", "All.Apply"));
			SMessageBox::Get()->SetEnableClose(true);
			SMessageBox::Get()->SetIsOnceClickButtons(true);
			SMessageBox::Get()->OnMessageBoxButton.AddLambda([&, Widget = WindowContent](const EMessageBoxButton InButton)
			{
				if (auto MyComponent = GetSquadComponent())
				{
					MyComponent->SendRequestAdd(CurrentTargetId, Widget->GetOptions(), ESquadInviteDirection::Invite);
				}

				SMessageBox::Get()->ToggleWidget(false);
			});
			SMessageBox::Get()->ToggleWidget(true);
		QueueMessageEnd

		if (OnAnyAction.IsBound()) OnAnyAction.Broadcast();
	}));

	UICommandList->MapAction(Commands[ConstructionInvite], FExecuteAction::CreateLambda([&]() {
		if (auto MyComponent = GetSquadComponent())
		{
			FSessionMatchOptions SessionMatchOptions;
			SessionMatchOptions.GameMode = EGameMode::Construction.Value;
			MyComponent->SendRequestAdd(CurrentTargetId, SessionMatchOptions, ESquadInviteDirection::Invite);
		}

		if (OnAnyAction.IsBound()) OnAnyAction.Broadcast();
	}));

	UICommandList->MapAction(Commands[ConstructionRequest], FExecuteAction::CreateLambda([&]() {
		if (auto MyComponent = GetSquadComponent())
		{
			FSessionMatchOptions SessionMatchOptions;
			SessionMatchOptions.GameMode = EGameMode::Construction.Value;
			MyComponent->SendRequestAdd(CurrentTargetId, SessionMatchOptions, ESquadInviteDirection::Request);
		}

		if (OnAnyAction.IsBound()) OnAnyAction.Broadcast();
	}));

	UICommandList->MapAction(Commands[DeleteFriend], FExecuteAction::CreateLambda([&]() {
		if (auto MyComponent = GetFriendComponent())
		{
			MyComponent->SendRequestRemove(CurrentTargetId);
		}

		if (OnAnyAction.IsBound()) OnAnyAction.Broadcast();
	}));

	UICommandList->MapAction(Commands[GuestView], FExecuteAction::CreateLambda([&]() {
		if (PlayerState.IsValid())
		{
			auto MyGameState = GetWorldGameState<AConstructionGameState>(PlayerState->GetWorld());
			if (MyGameState && MyGameState->GetConstructionComponent())
			{
				MyGameState->GetConstructionComponent()->SendGetBuildingList(CurrentTargetId);
			}
		}

		if (OnAnyAction.IsBound()) OnAnyAction.Broadcast();
	}));

	UICommandList->MapAction(Commands[Attack], FExecuteAction::CreateLambda([&]() {
		//TODO: SeNTIke

		//if(FPlayerEntityInfo::Instance.DefenceExpiryDate >= FDateTime::UtcNow().ToUnixTimestamp())
		//{
		//	SMessageBox::AddQueueMessage("AttackDefenceExpiry", FOnClickedOutside::CreateLambda([&, id = CurrentTargetId]()
		//	{
		//
		//		SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Notify", "MessageBox.AttackDefenceExpiry.Title", "Attack on the player"));
		//		SMessageBox::Get()->SetContent(
		//			FText::Format
		//			(
		//				NSLOCTEXT("Notify", "MessageBox.AttackDefenceExpiry.Desc", "You are protected from attacks before {0}. If you attack other players, you will lose protection from attacks. And the player you will attacked will be protected for a while."), FText::FromString(FDateTime::FromUnixTimestamp(FPlayerEntityInfo::Instance.DefenceExpiryDate).ToString())
		//			)
		//		);
		//
		//		SMessageBox::Get()->SetButtonsText(FButtonNameLocalization::Yes, FText::GetEmpty(), FButtonNameLocalization::No);
		//		SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](EMessageBoxButton button)
		//		{
		//			if (button == EMessageBoxButton::Left)
		//			{
		//				OnAttackTo.ExecuteIfBound(CurrentTargetId);
		//			}
		//			SMessageBox::Get()->ToggleWidget(false);
		//			SMessageBox::RemoveQueueMessage("AttackDefenceExpiry");
		//		});
		//		SMessageBox::Get()->SetEnableClose(false);
		//		SMessageBox::Get()->ToggleWidget(true);
		//	}));
		//}
		//else
		//{
		//	OnAttackTo.ExecuteIfBound(CurrentTargetId);
		//}
		//if (OnAnyAction.IsBound()) OnAnyAction.Broadcast();
	}));
}

void SFriendsCommands::SetPlayerState(TWeakObjectPtr<ABasePlayerState> InPlayerState)
{
	PlayerState = InPlayerState;
	if (PlayerState.IsValid() == false)
	{
		UE_LOG(LogInit, Error, TEXT("> SFriendsCommands::SetPlayerState | InPlayerState was invalid"));
	}
	else if (GetValidObject(PlayerState.Get()) == nullptr)
	{
		UE_LOG(LogInit, Error, TEXT("> SFriendsCommands::SetPlayerState | InPlayerState was nullptr"));
	}
}

USquadComponent* SFriendsCommands::GetSquadComponent() const
{
	if (PlayerState.IsValid())
	{
		return PlayerState->GetSquadComponent();
	}
	else
	{
		UE_LOG(LogInit, Error, TEXT("> SFriendsCommands::GetSquadComponent | PlayerState was nullptr"));
	}

	return nullptr;
}

UFriendComponent* SFriendsCommands::GetFriendComponent() const
{
	if (PlayerState.IsValid())
	{
		return PlayerState->GetFriendComponent();
	}
	else
	{
		UE_LOG(LogInit, Error, TEXT("> SFriendsCommands::GetFriendComponent | PlayerState was nullptr"));
	}

	return nullptr;
}
