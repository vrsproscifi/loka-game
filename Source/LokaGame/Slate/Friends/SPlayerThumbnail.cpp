// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "SPlayerThumbnail.h"
#include "SlateOptMacros.h"
#include "Utilities/SlateUtilityTypes.h"
#include "SScaleBox.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SPlayerThumbnail::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;

	Status = InArgs._Status;
	Name = InArgs._Name;
	IsLocal = InArgs._IsLocal;
	IsSquad = InArgs._IsSquad;
	PlayerId = InArgs._PlayerId;

	ChildSlot
	[
		SNew(SHorizontalBox)
		+ SHorizontalBox::Slot().AutoWidth()
		[
			SNew(SBorder).BorderImage(this, &SPlayerThumbnail::GetStatusImage).Padding(2)
			[
				SNew(SBox).WidthOverride(50).HeightOverride(50)
				[
					SNew(SScaleBox)
					.Stretch(EStretch::ScaleToFit)
					[
						SNew(SImage)
						.Image(FPlayerAvatarHelper::GetPlayerImage(PlayerId.Get().ToString(EGuidFormats::DigitsWithHyphens))->Attr())
					]
				]
			]
		]
		+ SHorizontalBox::Slot().Padding(4, 2)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot().AutoHeight()
			[
				SNew(STextBlock)
				.Text(this, &SPlayerThumbnail::GetNameText)
				.Font(this, &SPlayerThumbnail::GetNameFont)
				.ColorAndOpacity(this, &SPlayerThumbnail::GetNameColorAndOpacity)
				.ShadowOffset(this, &SPlayerThumbnail::GetNameShadowOffset)
				.ShadowColorAndOpacity(this, &SPlayerThumbnail::GetNameShadowColor)
			]
			+ SVerticalBox::Slot().AutoHeight().Padding(0, 2)
			[
				SNew(STextBlock)
				.Text(this, &SPlayerThumbnail::GetStatusText)
				.Font(this, &SPlayerThumbnail::GetStatusFont)
				.ColorAndOpacity(this, &SPlayerThumbnail::GetStatusColorAndOpacity)
				.ShadowOffset(this, &SPlayerThumbnail::GetStatusShadowOffset)
				.ShadowColorAndOpacity(this, &SPlayerThumbnail::GetStatusShadowColor)
			]
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

FSlateFontInfo SPlayerThumbnail::GetNameFont() const
{
	if (IsLocal.IsSet() && IsLocal.Get())
	{
		return Style->Name[EStylePlayerState::Local].Font;
	}
	else if (FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::PlayGame))
	{
		return Style->Name[EStylePlayerState::Playing].Font;
	}
	else if (FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::Online | EPlayerFriendStatus::Global))
	{
		if (IsSquad.Get() && FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::Leader | PlayerFriendStatus::Ready) == false)
		{
			return Style->Name[EStylePlayerState::Waiting].Font;
		}

		return Style->Name[EStylePlayerState::Online].Font;
	}
	else if (FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::SearchGame))
	{
		return Style->Name[EStylePlayerState::Waiting].Font;
	}
	else if (FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::Building))
	{
		return Style->Name[EStylePlayerState::Building].Font;
	}
	else
	{
		return Style->Name[EStylePlayerState::Offline].Font;
	}
}

FSlateFontInfo SPlayerThumbnail::GetStatusFont() const
{
	if (IsLocal.IsSet() && IsLocal.Get())
	{
		return Style->Status[EStylePlayerState::Local].Font;
	}
	else if (FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::PlayGame))
	{
		return Style->Status[EStylePlayerState::Playing].Font;
	}
	else if (FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::Online | EPlayerFriendStatus::Global))
	{
		if (IsSquad.Get() && FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::Leader | PlayerFriendStatus::Ready) == false)
		{
			return Style->Status[EStylePlayerState::Waiting].Font;
		}

		return Style->Status[EStylePlayerState::Online].Font;
	}
	else if (FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::SearchGame))
	{
		return Style->Status[EStylePlayerState::Waiting].Font;
	}
	else if (FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::Building))
	{
		return Style->Status[EStylePlayerState::Building].Font;
	}
	else
	{
		return Style->Status[EStylePlayerState::Offline].Font;
	}
}

FSlateColor SPlayerThumbnail::GetNameColorAndOpacity() const
{
	if (IsLocal.IsSet() && IsLocal.Get())
	{
		return Style->Name[EStylePlayerState::Local].ColorAndOpacity;
	}
	else if (FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::PlayGame))
	{
		return Style->Name[EStylePlayerState::Playing].ColorAndOpacity;
	}
	else if (FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::Online | EPlayerFriendStatus::Global))
	{
		if (IsSquad.Get() && FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::Leader | PlayerFriendStatus::Ready) == false)
		{
			return Style->Name[EStylePlayerState::Waiting].ColorAndOpacity;
		}

		return Style->Name[EStylePlayerState::Online].ColorAndOpacity;
	}
	else if (FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::SearchGame))
	{
		return Style->Name[EStylePlayerState::Waiting].ColorAndOpacity;
	}
	else if (FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::Building))
	{
		return Style->Name[EStylePlayerState::Building].ColorAndOpacity;
	}
	else
	{
		return Style->Name[EStylePlayerState::Offline].ColorAndOpacity;
	}
}

FSlateColor SPlayerThumbnail::GetStatusColorAndOpacity() const
{
	if (IsLocal.IsSet() && IsLocal.Get())
	{
		return Style->Status[EStylePlayerState::Local].ColorAndOpacity;
	}
	else if (FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::PlayGame))
	{
		return Style->Status[EStylePlayerState::Playing].ColorAndOpacity;
	}
	else if (FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::Online | EPlayerFriendStatus::Global))
	{
		if (IsSquad.Get() && FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::Leader | PlayerFriendStatus::Ready) == false)
		{
			return Style->Status[EStylePlayerState::Waiting].ColorAndOpacity;
		}

		return Style->Status[EStylePlayerState::Online].ColorAndOpacity;
	}
	else if (FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::SearchGame))
	{
		return Style->Status[EStylePlayerState::Waiting].ColorAndOpacity;
	}
	else if (FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::Building))
	{
		return Style->Status[EStylePlayerState::Building].ColorAndOpacity;
	}
	else
	{
		return Style->Status[EStylePlayerState::Offline].ColorAndOpacity;
	}
}

FVector2D SPlayerThumbnail::GetNameShadowOffset() const
{
	if (IsLocal.IsSet() && IsLocal.Get())
	{
		return Style->Name[EStylePlayerState::Local].ShadowOffset;
	}
	else if (FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::PlayGame))
	{
		return Style->Name[EStylePlayerState::Playing].ShadowOffset;
	}
	else if (FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::Online | EPlayerFriendStatus::Global))
	{
		if (IsSquad.Get() && FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::Leader | PlayerFriendStatus::Ready) == false)
		{
			return Style->Name[EStylePlayerState::Waiting].ShadowOffset;
		}

		return Style->Name[EStylePlayerState::Online].ShadowOffset;
	}
	else if (FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::SearchGame))
	{
		return Style->Name[EStylePlayerState::Waiting].ShadowOffset;
	}
	else if (FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::Building))
	{
		return Style->Name[EStylePlayerState::Building].ShadowOffset;
	}
	else
	{
		return Style->Name[EStylePlayerState::Offline].ShadowOffset;
	}
}

FVector2D SPlayerThumbnail::GetStatusShadowOffset() const
{
	if (IsLocal.IsSet() && IsLocal.Get())
	{
		return Style->Status[EStylePlayerState::Local].ShadowOffset;
	}
	else if (FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::PlayGame))
	{
		return Style->Status[EStylePlayerState::Playing].ShadowOffset;
	}
	else if (FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::Online | EPlayerFriendStatus::Global))
	{
		if (IsSquad.Get() && FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::Leader | PlayerFriendStatus::Ready) == false)
		{
			return Style->Status[EStylePlayerState::Waiting].ShadowOffset;
		}

		return Style->Status[EStylePlayerState::Online].ShadowOffset;
	}
	else if (FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::SearchGame))
	{
		return Style->Status[EStylePlayerState::Waiting].ShadowOffset;
	}
	else if (FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::Building))
	{
		return Style->Status[EStylePlayerState::Building].ShadowOffset;
	}
	else
	{
		return Style->Status[EStylePlayerState::Offline].ShadowOffset;
	}
}

FLinearColor SPlayerThumbnail::GetNameShadowColor() const
{
	if (IsLocal.IsSet() && IsLocal.Get())
	{
		return Style->Name[EStylePlayerState::Local].ShadowColorAndOpacity;
	}
	else if (FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::PlayGame))
	{
		return Style->Name[EStylePlayerState::Playing].ShadowColorAndOpacity;
	}
	else if (FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::Online | EPlayerFriendStatus::Global))
	{
		if (IsSquad.Get() && FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::Leader | PlayerFriendStatus::Ready) == false)
		{
			return Style->Name[EStylePlayerState::Waiting].ShadowColorAndOpacity;
		}

		return Style->Name[EStylePlayerState::Online].ShadowColorAndOpacity;
	}
	else if (FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::SearchGame))
	{
		return Style->Name[EStylePlayerState::Waiting].ShadowColorAndOpacity;
	}
	else if (FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::Building))
	{
		return Style->Name[EStylePlayerState::Building].ShadowColorAndOpacity;
	}
	else
	{
		return Style->Name[EStylePlayerState::Offline].ShadowColorAndOpacity;
	}
}

FLinearColor SPlayerThumbnail::GetStatusShadowColor() const
{
	if (IsLocal.IsSet() && IsLocal.Get())
	{
		return Style->Status[EStylePlayerState::Local].ShadowColorAndOpacity;
	}
	else if (FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::PlayGame))
	{
		return Style->Status[EStylePlayerState::Playing].ShadowColorAndOpacity;
	}
	else if (FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::Online | EPlayerFriendStatus::Global))
	{
		if (IsSquad.Get() && FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::Leader | PlayerFriendStatus::Ready) == false)
		{
			return Style->Status[EStylePlayerState::Waiting].ShadowColorAndOpacity;
		}

		return Style->Status[EStylePlayerState::Online].ShadowColorAndOpacity;
	}
	else if (FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::SearchGame))
	{
		return Style->Status[EStylePlayerState::Waiting].ShadowColorAndOpacity;
	}
	else if (FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::Building))
	{
		return Style->Status[EStylePlayerState::Building].ShadowColorAndOpacity;
	}
	else
	{
		return Style->Status[EStylePlayerState::Offline].ShadowColorAndOpacity;
	}
}

const FSlateBrush* SPlayerThumbnail::GetStatusImage() const
{
	if (IsLocal.IsSet() && IsLocal.Get())
	{
		return &Style->Background[EStylePlayerState::Local];
	}
	else if (FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::PlayGame))
	{
		return &Style->Background[EStylePlayerState::Playing];
	}
	else if (FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::Online | EPlayerFriendStatus::Global))
	{
		if (IsSquad.Get() && FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::Leader | PlayerFriendStatus::Ready) == false)
		{
			return &Style->Background[EStylePlayerState::Waiting];
		}

		return &Style->Background[EStylePlayerState::Online];
	}
	else if (FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::SearchGame))
	{
		return &Style->Background[EStylePlayerState::Waiting];
	}
	else if (FFlagsHelper::HasAnyFlags(Status.Get(), EPlayerFriendStatus::Building))
	{
		return &Style->Background[EStylePlayerState::Building];
	}
	else
	{
		return &Style->Background[EStylePlayerState::Offline];
	}
}

FText SPlayerThumbnail::GetNameText() const
{
	if (Name.IsSet())
	{
		return Name.Get();
	}

	return FText::GetEmpty();
}

FText SPlayerThumbnail::GetStatusText() const
{
	if (FFlagsHelper::HasAnyFlags(Status.Get(), PlayerFriendStatus::PlayGame))
	{
		return NSLOCTEXT("SFriendsFormItem", "Playing", "Playing");
	}
	else if (FFlagsHelper::HasAnyFlags(Status.Get(), PlayerFriendStatus::Online | PlayerFriendStatus::Global))
	{
		if (IsSquad.Get())
		{
			if (Status.Get() & PlayerFriendStatus::Leader)
			{
				return NSLOCTEXT("SFriendsFormItem", "LeaderOnline", "Leader Online");
			}
			else
			{
				if (Status.Get() & PlayerFriendStatus::Ready)
				{
					return NSLOCTEXT("SFriendsFormItem", "Ready", "Ready");
				}
				else
				{
					return NSLOCTEXT("SFriendsFormItem", "NotReady", "Not ready");
				}
			}
		}

		return NSLOCTEXT("SFriendsFormItem", "Online", "Online");
	}
	else if (FFlagsHelper::HasAnyFlags(Status.Get(), PlayerFriendStatus::SearchGame))
	{
		return NSLOCTEXT("SFriendsFormItem", "Waiting", "Waiting game");
	}
	else if (FFlagsHelper::HasAnyFlags(Status.Get(), PlayerFriendStatus::Building))
	{
		return NSLOCTEXT("SFriendsFormItem", "Building", "Construction");
	}
	else
	{
		if (IsSquad.Get())
		{
			if (Status.Get() & PlayerFriendStatus::Leader)
			{
				return NSLOCTEXT("SFriendsFormItem", "LeaderOffline", "Leader Offline");
			}
		}

		return NSLOCTEXT("SFriendsFormItem", "Offline", "Offline");
	}
}
