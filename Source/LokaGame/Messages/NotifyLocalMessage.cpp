// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "NotifyLocalMessage.h"
#include "SNotifyList.h"

void UNotifyLocalMessage::OnReceive_Implementation(const FBaseLocalMessageData& MessageData) const
{
	FNotifyInfo NotifyInfo;

	if (MessageData.OptionalData.Num())
	{
		FFormatNamedArguments NamedArguments;

		for (auto v : MessageData.OptionalData)
		{
			NamedArguments.Add(v.Key, v.Value.GetDataAsText());
		}

		NotifyInfo.Title = FText::Format(Title, NamedArguments);
		NotifyInfo.Content = FText::Format(Message, NamedArguments);
	}
	else
	{
		NotifyInfo.Title = Title;
		NotifyInfo.Content = Message;
	}

	if (Image.HasUObject())
	{
		NotifyInfo.Image = &Image;
	}

	SNotifyList::Get()->AddNotify(NotifyInfo);
}
