// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Messages/BaseLocalMessage.h"
#include "NotifyLocalMessage.generated.h"

/**
 * 
 */
UCLASS()
class LOKAGAME_API UNotifyLocalMessage : public UBaseLocalMessage
{
	GENERATED_BODY()
	
public:

	virtual void OnReceive_Implementation(const FBaseLocalMessageData& MessageData) const override;
	
protected:

	UPROPERTY(EditDefaultsOnly, Category = Message)
	FText Title;
	
	UPROPERTY(EditDefaultsOnly, Category = Message)
	FSlateBrush Image;
};
