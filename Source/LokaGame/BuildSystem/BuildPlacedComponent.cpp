// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "BuildPlacedComponent.h"
#include "BuildBoxComponent.h"
#include "BuildSphereComponent.h"
#include "Build/ItemBuildEntity.h"
#include "UTPlayerState.h"
#include "Engine/ActorChannel.h"
// For search instance if null
#include "ShooterGameInstance.h"
#include "GameSingleton.h"
#include "PlayerInventoryItem.h"
#include "ConstructionGameState.h"
#include "ConstructionComponent.h"
#include "DestructibleComponent.h"

// Sets default values
ABuildPlacedComponent::ABuildPlacedComponent()
{
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));

	DestroyPSC = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("DestroyPSC"));
	DestroyPSC->SetupAttachment(RootComponent);
	DestroyPSC->bAutoActivate = false;
	DestroyPSC->SetComponentTickEnabled(false);

	DestroyAC = CreateDefaultSubobject<UAudioComponent>(TEXT("DestroyAC"));
	DestroyAC->SetupAttachment(RootComponent);
	DestroyAC->SetComponentTickEnabled(false);
	DestroyAC->bAutoActivate = false;

	Instance = CreateDefaultSubobject<UPlayerInventoryItem>(TEXT("Instance"));
	Instance->SetIsReplicated(true);

	PrimaryActorTick.bCanEverTick = false;
	PrimaryActorTick.bStartWithTickEnabled = false;
	PrimaryActorTick.TickInterval = FMath::FRandRange(100.0f, 200.0f);
	bReplicates = true;

	Health = FVector2D(1000, 1000);
	LastDamageTime = -20.0f;
	MaterialParameterName = TEXT("alpha");
	bIsAllowPlaceOnBuilding = false;
	bIsDeath = false;

	RewardTotalScorePlayer = 5;
	RewardTotalScoreTeam = 10;

	bCanSelectable = true;
	bCanPlaceable = true;
	bCanRemovable = true;
	bIsPrePlaced = false;
	IsAppliedDynamicMats = false;
	bIsWorldGridComponent = true; // true = for fast view result
	WorldGridStep = FVector(100.0f); // 100.0f = default cube step
	BuildingTime = .0f;

	AllowPlaceAxies = FFlagsHelper::ToFlag<int32>(EAxis::X) | FFlagsHelper::ToFlag<int32>(EAxis::Y) | FFlagsHelper::ToFlag<int32>(EAxis::Z);
	AllowRotateAxies = AllowPlaceAxies;

	PrePlacedInstanceId = INDEX_NONE;

	ConstructorHelpers::FObjectFinderOptional<USoundBase> PlaceSoundOb(TEXT("/Game/RestrictedAssets/Audio/Movers/A_Door_Metal03_CloseStop_Cue"));
	if (PlaceSoundOb.Succeeded())
	{
		PlaceSound = GetValidObject(PlaceSoundOb.Get());
	}

	ConstructorHelpers::FObjectFinderOptional<USoundBase> SelectSoundOb(TEXT("/Game/RestrictedAssets/Audio/UI/A_UI_Pause01_Cue"));
	if (SelectSoundOb.Succeeded())
	{
		SelectSound = GetValidObject(SelectSoundOb.Get());
	}

	ConstructorHelpers::FObjectFinderOptional<USoundBase> RemoveSoundOb(TEXT("/Game/RestrictedAssets/Audio/Movers/A_Door_Metal03_CloseStart_Cue"));
	if (RemoveSoundOb.Succeeded())
	{
		RemoveSound = GetValidObject(RemoveSoundOb.Get());
	}
}

void ABuildPlacedComponent::OnRep_Instance()
{
	if (auto instance = GetInstance())
	{
		if(auto entity = instance->GetEntity<UItemBuildEntity>())
		{
			const auto mats = entity->GetReplaceMaterials();
			for (const auto ReplacePair : mats)
			{
				auto MyMeshes = GetMeshComponentsInternal();
				for (auto TargetMesh : MyMeshes)
				{
					if (TargetMesh && TargetMesh->IsValidLowLevel())
					{
						auto TargetIndex = TargetMesh->GetMaterialIndex(ReplacePair.Key);
						if (TargetIndex != INDEX_NONE)
						{
							TargetMesh->SetMaterial(TargetIndex, ReplacePair.Value);

							if (OriginalMaterialsMap.Contains(TargetMesh) && OriginalMaterialsMap.FindChecked(TargetMesh).Materials.IsValidIndex(TargetIndex))
							{
								OriginalMaterialsMap.FindChecked(TargetMesh).Materials[TargetIndex] = ReplacePair.Value;
							}
							else
							{
								FOrigenalMaterialsContainer Container;
								Container.Materials.Insert(ReplacePair.Value, TargetIndex);
								OriginalMaterialsMap.Add(TargetMesh, Container);
							}
						}
					}
				}
			}
		}
	}
}

void ABuildPlacedComponent::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	GetComponents<UMeshComponent>(MeshComponents);
	GetComponents<UBuildBoxComponent>(BoxComponents);
	GetComponents<UDestructibleComponent>(DestructibleComponents);

	auto MyBoxes = GetBoxComponentsInternal();
	for (auto box : MyBoxes)
	{
		if (box && box->IsValidLowLevel() && box->IsFoundationChecker())
		{
			BoxFoundationChecker = box;
		}
		else if (box && box->IsValidLowLevel() && box->IsOverlapChecker())
		{
			BoxOverlapChecker = box;
		}
	}

	auto MyDestructibles = GetDestructibleComponentsInternal();
	for (auto Destructible : MyDestructibles)
	{
		if (Destructible && Destructible->IsValidLowLevel())
		{
			Destructible->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		}
	}

	auto MyMeshes = GetMeshComponentsInternal();
	for (auto mesh : MyMeshes)
	{
		if (mesh && mesh->IsValidLowLevel())
		{
			FOrigenalMaterialsContainer Container;
			Container.Materials = mesh->GetMaterials();
			OriginalMaterialsMap.Add(mesh, Container);
		}
	}
}

FWorldBuildingProperty ABuildPlacedComponent::GetDynamicPropertyData(const FString& InKey) const
{
	if (DynamicPropertyData.Contains(InKey))
	{
		return DynamicPropertyData.FindChecked(InKey);
	}

	return FWorldBuildingProperty();
}

void ABuildPlacedComponent::UpdateDynamicPropertyData(const FString& InKey, const FString& InData, const bool IsFromServer)
{
	DynamicPropertyData.FindOrAdd(InKey).Value = InData;

	if (IsFromServer)
	{
		OnDynamicPropertyDataUpdate();
	}
	else
	{
		auto MyGameState = GetWorldGameState<AConstructionGameState>(GetWorld());
		auto MyConstructionComp = MyGameState ? MyGameState->GetConstructionComponent() : nullptr;
		if (MyConstructionComp)
		{
			FUpdateBuildingProperty UpdateBuildingProperty;
			UpdateBuildingProperty.BuildingId = GUID;
			UpdateBuildingProperty.Key = InKey;
			UpdateBuildingProperty.Value = InData;

			MyConstructionComp->SendRequestUpdateProperty(UpdateBuildingProperty);
		}
	}
}

void ABuildPlacedComponent::InitializeDynamicPropertyData(const FString& InKey, const FWorldBuildingProperty& InData)
{
	DynamicPropertyData.FindOrAdd(InKey) = InData;
	OnDynamicPropertyDataUpdate();
}

void ABuildPlacedComponent::OnDynamicPropertyDataUpdate_Implementation()
{
	
}

void ABuildPlacedComponent::InitializeInstance(UPlayerInventoryItem* InInstance)
{
	if(const auto instance = GetValidObject(InInstance))
	{
		Instance = instance;
		OnRep_Instance();
	}
}

void ABuildPlacedComponent::InitializeInstance(UItemBuildEntity* InInstance)
{
	if(const auto instance = GetValidObject(InInstance))
	{
		if (auto i = GetInstance())
		{
			i->SetItemEntity(instance);
			InitializeInstance(i);
		}
	}
}

TArray<UMeshComponent*> ABuildPlacedComponent::GetMeshComponentsInternal() const
{
	TArray<UMeshComponent*> Answer = MeshComponents;
	for (auto MyMesh : GetMeshComponents())
	{
		Answer.AddUnique(MyMesh);
	}

	return Answer;
}

TArray<UBuildBoxComponent*> ABuildPlacedComponent::GetBoxComponentsInternal() const
{
	TArray<UBuildBoxComponent*> Answer = BoxComponents;
	for (auto MyBox : GetBoxComponents())
	{
		Answer.AddUnique(MyBox);
	}

	return Answer;
}

TArray<UDestructibleComponent*> ABuildPlacedComponent::GetDestructibleComponentsInternal() const
{
	TArray<UDestructibleComponent*> Answer = DestructibleComponents;
	for (auto MyDestructible : GetDestructibleComponents())
	{
		Answer.AddUnique(MyDestructible);
	}

	return Answer;
}

// Called when the game starts or when spawned
void ABuildPlacedComponent::BeginPlay()
{
	Super::BeginPlay();	
}

// Called every frame
void ABuildPlacedComponent::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

bool ABuildPlacedComponent::IsSingleMeshComponent() const
{
	return GetMeshComponentsInternal().Num() == 1;
}

TArray<UMeshComponent*> ABuildPlacedComponent::GetMeshComponents_Implementation() const
{
	return MeshComponents;
}

TArray<UBuildBoxComponent*> ABuildPlacedComponent::GetBoxComponents_Implementation() const
{
	return BoxComponents;
}

TArray<UDestructibleComponent*> ABuildPlacedComponent::GetDestructibleComponents_Implementation() const
{
	return DestructibleComponents;
}

FVector ABuildPlacedComponent::GetBoundsExtent() const
{
	if (IsSingleMeshComponent())
	{
		if (const auto c = GetMeshComponentsInternal()[0])
		{
			return c->Bounds.GetBox().GetExtent();
		}
	}

	return FVector::ZeroVector;
}

void ABuildPlacedComponent::SetIndicatorMaterial_Implementation(UMaterialInterface* InMaterial, const bool WithSnapped, const bool WithAllSnapped)
{
	IndicatorMaterial = GetValidObject(InMaterial);

	auto MyMeshes = GetMeshComponentsInternal();
	for (auto Mesh : MyMeshes)
	{
		if (auto mesh = GetValidObject(Mesh))
		{
			for (int32 i = 0; i < mesh->GetNumMaterials(); ++i)
			{
				if (InMaterial == nullptr && OriginalMaterialsMap.Contains(mesh) && OriginalMaterialsMap.FindChecked(mesh).Materials.IsValidIndex(i))
				{
					mesh->SetMaterial(i, OriginalMaterialsMap.FindChecked(mesh).Materials[i]);
				}
				else
				{
					mesh->SetMaterial(i, IndicatorMaterial);
				}
			}
		}
	}

	if (WithSnapped && WithAllSnapped)
	{
		for (auto Component : GetAllSnappedComponents())
		{
			if (auto c = GetValidObject(Component))
			{
				c->SetIndicatorMaterial(IndicatorMaterial);
			}
		}
	}
	else if (WithSnapped)
	{
		for (auto Component : GetChildSnappedComponents())
		{
			if (auto c = GetValidObject(Component))
			{
				c->SetIndicatorMaterial(IndicatorMaterial);
			}
		}
	}
}

bool ABuildPlacedComponent::IsSettedIndicatorMaterial_Implementation() const
{
	return (IndicatorMaterial != nullptr);
}

TArray<ABuildPlacedComponent*> ABuildPlacedComponent::GetAllSnappedComponents(const bool WithFoundation) const
{
	TArray<ABuildPlacedComponent*> ReturnArray;

	for (auto child : ChildBuildComponents)
	{
		if (child && ((WithFoundation == false && child->GetType() != EBuildComponentType::Foundation) || WithFoundation))
		{
			ReturnArray.Append(child->GetAllSnappedComponents(WithFoundation));
			ReturnArray.Add(child);
		}
	}

	return ReturnArray;
}

TArray<ABuildPlacedComponent*> ABuildPlacedComponent::GetChildSnappedComponents(const bool WithFoundation) const
{
	TArray<ABuildPlacedComponent*> ReturnArray;

	for (auto child : ChildBuildComponents)
	{
		if (child && ((WithFoundation == false && child->GetType() != EBuildComponentType::Foundation) || WithFoundation))
		{
			ReturnArray.Add(child);
		}
	}

	return ReturnArray;
}

UBuildBoxComponent* ABuildPlacedComponent::GetOverlapChecker() const
{
	return GetValidObject(BoxOverlapChecker);
}

UBuildBoxComponent* ABuildPlacedComponent::GetFoundationChecker() const
{
	return GetValidObject(BoxFoundationChecker);
}

UPlayerInventoryItem* ABuildPlacedComponent::GetInstance() const
{
	return GetValidObject(Instance);
}

void ABuildPlacedComponent::GetLifetimeReplicatedProps(TArray<class FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ABuildPlacedComponent, Health);
	DOREPLIFETIME(ABuildPlacedComponent, Instance);
	DOREPLIFETIME(ABuildPlacedComponent, bIsDeath);
}

float ABuildPlacedComponent::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	const float OriginAmount = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	if (bIsDeath == false)
	{
		if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
		{
			Health.X -= OriginAmount * 1.5f;
		}
		else
		{
			Health.X -= OriginAmount * 0.2f;
		}

		Health.X = FMath::Max(Health.X, .0f);

		if (EventInstigator)
		{
			float& Value = DamageByPlayers.FindOrAdd(EventInstigator->GetPlayerState<AUTPlayerState>());
			Value += OriginAmount;
		}

		if (Health.X <= .0f)
		{
			IDestroyableActorInterface::Execute_OnDeath(this);

			//for (auto child : GetAllSnappedComponents())
			//{
			//	if (child)
			//	{
			//		child->SetActorEnableCollision(false);
			//		child->SetActorHiddenInGame(true);
			//		child->OnComponentDeath.Broadcast(child);
			//	}
			//}

			bIsDeath = true;
			OnRep_Death();
		}

		OnComponentTakeDamage.Broadcast(this);
	}

	return OriginAmount;
}

void ABuildPlacedComponent::OnRep_Death()
{
	if (bIsDeath)
	{
		for (auto Comp : GetComponents())
		{
			if (auto BoxComp = GetValidObject<UBoxComponent, UActorComponent>(Comp))
			{
				BoxComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
				BoxComp->SetVisibility(false);
			}
			else if (auto MeshComp = GetValidObject<UMeshComponent, UActorComponent>(Comp))
			{
				MeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
				MeshComp->SetVisibility(false);
			}
		}

		OnComponentDeath.Broadcast(this);

		if(auto psc = GetValidObject(DestroyPSC))
		{
			psc->ActivateSystem();
		}

		if (auto ac = GetValidObject(DestroyAC))
		{
			ac->Play();
		}

		auto MyDestructibles = GetDestructibleComponentsInternal();
		for (auto Destructible : MyDestructibles)
		{
			if (auto des = GetValidObject(Destructible))
			{
				des->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
				des->ApplyDamage(1000.0f, des->GetComponentLocation(), des->GetUpVector(), 10.0f);
				des->SetVisibility(true);
			}
		}
	}
}

void ABuildPlacedComponent::OnRep_Health()
{
	if (GetNetMode() == NM_Client || GetNetMode() == NM_ListenServer)
	{
		LastDamageTime = GetWorld()->GetTimeSeconds() + 20.0f;
	}

	if (IsAppliedDynamicMats == false && Health.X < Health.Y * .8f)
	{
		auto MyMeshes = GetMeshComponentsInternal();
		for (auto TargetMesh : MyMeshes)
		{
			if (auto mesh = GetValidObject(TargetMesh))
			{
				for (int32 i = 0; i < mesh->GetNumMaterials(); ++i)
				{
					if (OriginalMaterialsMap.Contains(mesh) && OriginalMaterialsMap.FindChecked(mesh).Materials.IsValidIndex(i))
					{
						OriginalMaterialsMap.FindChecked(mesh).Materials[i] = mesh->CreateDynamicMaterialInstance(i, mesh->GetMaterial(i));
					}
					else
					{
						FOrigenalMaterialsContainer Container;
						Container.Materials.Insert(mesh->CreateDynamicMaterialInstance(i, mesh->GetMaterial(i)), i);
						OriginalMaterialsMap.Add(mesh, Container);
					}
				}
			}
		}

		IsAppliedDynamicMats = true;
	}

	if (IsAppliedDynamicMats == true)
	{
		// Damage don't support currently
		//for (auto MID : OriginalMaterialsMap)
		//{
		//	if (MID.Key && MID.Key->IsValidLowLevel())
		//	{
		//		if (OriginalMaterialsMap.Contains(MID.Key) && OriginalMaterialsMap.FindChecked(MID.Key).Materials.Num() > 0)
		//		{
		//			for (auto MID2 : OriginalMaterialsMap.FindChecked(MID.Key).Materials)
		//			{
		//				if (auto Casted = Cast<UMaterialInstanceDynamic>(MID2))
		//				{
		//					Casted->SetScalarParameterValue(MaterialParameterName, IDestroyableActorInterface::Execute_GetHealthPercent(this));
		//				}
		//			}
		//		}
		//	}
		//}
	}
}

FText ABuildPlacedComponent::GetLocalizedName_Implementation() const
{
	if(const auto inst = GetValidObject(Instance))
	{
		if (auto entity = inst->GetEntity<UItemBuildEntity>())
		{
			return entity->GetDescription().Name;
		}
	}
	
	return FText::GetEmpty();
}

bool ABuildPlacedComponent::GetIsVisiblyUserInterface_Implementation() const
{
	if (GetNetMode() != NM_DedicatedServer)
	{
		float LastOnScreen = -1.0f;
		auto MyMeshes = GetMeshComponentsInternal();
		for (auto MeshComp : MyMeshes)
		{
			if (const auto mesh = GetValidObject(MeshComp))
			{
				LastOnScreen = FMath::Max(LastOnScreen, mesh->LastRenderTimeOnScreen);
			}
		}
		
		return (FMath::IsNearlyEqual(LastOnScreen, GetWorld()->GetTimeSeconds(), 2.0f) && LastDamageTime > GetWorld()->GetTimeSeconds());
	}

	return false;
}

void ABuildPlacedComponent::OnComponentPlaced_Implementation(const bool IsInAttack)
{
	if (IsInAttack)
	{
		DamageByPlayers.Empty();
	}

	if(auto inst = GetValidObject(Instance))
	{
		if (inst->GetEntity<UItemBuildEntity>() == nullptr && PrePlacedInstanceId >= 0)
		{
			if (const auto MyGameSingleton = UGameSingleton::Get())
			{
				inst->SetItemEntity(MyGameSingleton->FindItem(PrePlacedInstanceId, ECategoryTypeId::Building));
			}
		}
	}
}

uint8 ABuildPlacedComponent::GetTeamNum() const
{
	const auto MyOwnerPS = GetValidObject<AUTPlayerState, AActor>(GetOwner());
	return MyOwnerPS ? MyOwnerPS->GetTeamNum() : IUTTeamInterface::NONE_TEAM_ID;
}