// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "BuildBoxComponent.h"
#include "BuildSphereComponent.h"


UBuildSphereComponent::UBuildSphereComponent()
	: Super()
{	
	SetCollisionProfileName(TEXT("NoCollision"));

	FFlagsHelper::SetFlags(AllowedComponents, FFlagsHelper::ToFlag<int32>(EBuildComponentType::None));
	FFlagsHelper::SetFlags(AllowedSnaps, FFlagsHelper::ToFlag<int32>(EBuildSphereFlag::None));
	SnapType = EBuildSphereFlag::None;	
}

void UBuildSphereComponent::CopyExtraDataFrom(const UBuildSphereComponent* InOther)
{
	if (const auto other = GetValidObject(InOther))
	{
		AllowedComponents = other->AllowedComponents;
		AllowedSnaps = other->AllowedSnaps;
		SnapType = other->SnapType;
	}
}