// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/BoxComponent.h"
#include "BuildSystemData.h"
#include "BuildBoxComponent.generated.h"

/**
 * BuildBox is grid component used in raycasting
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class LOKAGAME_API UBuildBoxComponent : public UBoxComponent
{
	GENERATED_BODY()
	
public:
	
	UBuildBoxComponent();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Type)
	FORCEINLINE bool IsFoundationChecker() const { return bIsFoundationChecker; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Type)
	FORCEINLINE bool IsOverlapChecker() const { return bIsOverlapChecker; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Type)
	FORCEINLINE bool IsAlwaysFindTerrain() const { return bIsAlwaysFindTerrain; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Type)
	bool IsAllowedComponnent(const int32& InFlags) const;

	UFUNCTION(BlueprintCallable, Category = Construction)
	void CopyExtraDataFrom(const UBuildBoxComponent* InOther);

protected:

	UPROPERTY(EditDefaultsOnly, Category = Type, Meta = (Bitmask, BitmaskEnum = "EBuildComponentType"))
	int32 AllowedComponnents;
	
	UPROPERTY(EditDefaultsOnly, Category = Type)
	bool bIsFoundationChecker;

	UPROPERTY(EditDefaultsOnly, Category = Type)
	bool bIsOverlapChecker;

	UPROPERTY(EditDefaultsOnly, Category = Type)
	bool bIsAlwaysFindTerrain;
};
