// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ArrowComponent.h"
#include "BuildArrowComponent.generated.h"

/**
 * Helper for snap point, used for rotation around snap;
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class LOKAGAME_API UBuildArrowComponent : public UArrowComponent
{
	GENERATED_BODY()

public:

	UBuildArrowComponent();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Type)
	FORCEINLINE bool IsFaceArrow() const { return bIsFaceArrow; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Type)
	bool IsAllowedComponnent(const int32& InFlags) const;

	UFUNCTION(BlueprintCallable, Category = Construction)
	void CopyExtraDataFrom(const UBuildArrowComponent* InOther);

protected:

	UPROPERTY(EditDefaultsOnly, Category = Type, Meta = (Bitmask, BitmaskEnum = "EBuildComponentType"))
	int32 AllowedComponnents;	

	UPROPERTY(EditDefaultsOnly, Category = Type)
	bool bIsFaceArrow;
};
