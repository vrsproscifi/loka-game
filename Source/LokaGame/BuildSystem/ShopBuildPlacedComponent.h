// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BuildSystem/UsableBuildPlacedComponent.h"
#include "WorldPreviewItem.h"
#include "Item/ItemModelInfo.h"
#include "ShopBuildPlacedComponent.generated.h"


class UMarketComponent;
class UItemBaseEntity;

UCLASS()
class LOKAGAME_API AShopBuildPlacedComponent : public AUsableBuildPlacedComponent
{
	GENERATED_BODY()
	
public:

	AShopBuildPlacedComponent();
	
	virtual void InitializeMarket(UMarketComponent* InMarketComponent);

	virtual void OnFocusStart_Implementation(AActor* InInstigator) override;
	virtual void OnFocusLost_Implementation(AActor* InInstigator) override;
	virtual void OnInteractStart_Implementation(AActor* InInstigator) override;
	virtual void OnInteractLost_Implementation(AActor* InInstigator) override;

	virtual void Tick(float DeltaSeconds) override;

protected:

	UPROPERTY(EditDefaultsOnly, Category = Configuration)
	FTransform SpawnItemTrans;
	
	UPROPERTY(EditDefaultsOnly, Category = Configuration)
	FTransform ShowItemTrans;

	UPROPERTY()
	AWorldPreviewItem* WorldPreviewItem;

	UPROPERTY(Replicated)
	UMarketComponent* MarketComponent;

	UPROPERTY()
	FItemModelInfo LastModelInfo;

	FORCEINLINE UMarketComponent* GetMarketComponent() const;


	FORCEINLINE AWorldPreviewItem* GetWorldPreviewItem() const;
};
