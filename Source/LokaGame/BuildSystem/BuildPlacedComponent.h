// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "BuildSystemData.h"
#include "Interfaces/DestroyableActorInterface.h"
#include "Interfaces/UTTeamInterface.h"
#include "Build/ItemBuildModelId.h"
#include "BuildingController/Models/WorldBuildingItemView.h"
#include "BuildPlacedComponent.generated.h"


class UBuildBoxComponent;
class UBuildSphereComponent;
class UItemBuildEntity;
class UPlayerInventoryItem;
class UWidgetComponent;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnBuildingSignature, ABuildPlacedComponent*, Component);

USTRUCT(Blueprintable)
struct FOrigenalMaterialsContainer
{
	GENERATED_USTRUCT_BODY();

	UPROPERTY(BlueprintReadWrite)
	TArray<UMaterialInterface*> Materials;
};

UCLASS(abstract)
class LOKAGAME_API ABuildPlacedComponent 
	: public AActor
	, public IDestroyableActorInterface
	, public IUTTeamInterface
{
	friend class ABuildHelperComponent;
	friend class AGameMode_Build;
	friend class ALobbyGameMode;
	friend class AShooterGame_BuildingAttack;
	friend class UBuildingComponent;
	friend class UConstructionComponent;

	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABuildPlacedComponent();

	virtual void PostInitializeComponents() override;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	bool IsSingleMeshComponent() const;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, BlueprintPure, Category = Component)
	TArray<UMeshComponent*> GetMeshComponents() const;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, BlueprintPure, Category = Component)
	TArray<UBuildBoxComponent*> GetBoxComponents() const;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, BlueprintPure, Category = Component)
	TArray<UDestructibleComponent*> GetDestructibleComponents() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	FORCEINLINE TEnumAsByte<EBuildComponentType::Type> GetType() const { return Type; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component, Meta = (Bitmask, BitmaskEnum = "EBuildComponentType"))
	FORCEINLINE int32 GetSnapTo() const { return SnapTo; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	FVector GetBoundsExtent() const;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = Component)
	void SetIndicatorMaterial(UMaterialInterface* InMaterial, const bool WithSnapped = false, const bool WithAllSnapped = false);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, BlueprintPure, Category = Component)
	bool IsSettedIndicatorMaterial() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	TArray<ABuildPlacedComponent*> GetAllSnappedComponents(const bool WithFoundation = false) const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	TArray<ABuildPlacedComponent*> GetChildSnappedComponents(const bool WithFoundation = false) const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	FORCEINLINE bool IsPlaceAsFoundation() const { return (GetFoundationChecker() != nullptr); }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	FORCEINLINE bool IsAllowPlaceOnBuilding() const { return bIsAllowPlaceOnBuilding; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	UBuildBoxComponent* GetOverlapChecker() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	UBuildBoxComponent* GetFoundationChecker() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	UPlayerInventoryItem* GetInstance() const;

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

	// IDestroyableActorInterface Begin
	virtual FVector2D GetHealth_Implementation() const override { return Health; }
	virtual float GetCurrentHealth_Implementation() const override { return Health.X; }
	virtual float GetHealthPercent_Implementation() const override { return Health.X / Health.Y; }
	virtual bool IsDying_Implementation() const override { return bIsDeath; }
	virtual FText GetLocalizedName_Implementation() const override;
	virtual bool GetIsVisiblyUserInterface_Implementation() const override;
	// IDestroyableActorInterface End

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = Component)
	void OnComponentPlaced(const bool IsInAttack = false);

	UPROPERTY(BlueprintAssignable, Category = Component)
	FOnBuildingSignature OnComponentDeath;

	UPROPERTY(BlueprintAssignable, Category = Component)
	FOnBuildingSignature OnComponentTakeDamage;

	// IUTTeamInterface Begin
	virtual uint8 GetTeamNum() const override;
	virtual void SetTeamForSideSwap_Implementation(uint8 NewTeamNum) override { }
	// IUTTeamInterface End

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	FORCEINLINE bool CanSelectable() const { return bCanSelectable; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	FORCEINLINE bool CanPlaceable() const { return bCanPlaceable; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	FORCEINLINE bool CanRemovable() const { return bCanRemovable; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	FORCEINLINE bool IsPrePlaced() const { return bIsPrePlaced; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	FORCEINLINE int32 GetPrePlacedInstanceId() const { return PrePlacedInstanceId; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	FORCEINLINE bool IsWorldGridComponent() const { return bIsWorldGridComponent; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	FORCEINLINE FVector GetWorldGridStep() const { return WorldGridStep; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	FORCEINLINE int32 GetAllowPlaceAxies() const { return AllowPlaceAxies; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	FORCEINLINE int32 GetAllowRotateAxies() const { return AllowRotateAxies; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	FWorldBuildingProperty GetDynamicPropertyData(const FString& InKey) const;

	UFUNCTION(BlueprintCallable, Category = Component)
	void UpdateDynamicPropertyData(const FString& InKey, const FString& InData, const bool IsFromServer = false);

	void InitializeDynamicPropertyData(const FString& InKey, const FWorldBuildingProperty& InData);
	void InitializeInstance(UPlayerInventoryItem* InInstance);
	void InitializeInstance(UItemBuildEntity* InInstance);

protected:

	TArray<UMeshComponent*> GetMeshComponentsInternal() const;
	TArray<UBuildBoxComponent*> GetBoxComponentsInternal() const;
	TArray<UDestructibleComponent*> GetDestructibleComponentsInternal() const;

	UFUNCTION(BlueprintNativeEvent, Category = Component)
	void OnDynamicPropertyDataUpdate();

	UPROPERTY(VisibleAnywhere, Category = Effect)
	UParticleSystemComponent* DestroyPSC;

	UPROPERTY(VisibleAnywhere, Category = Effect)
	UAudioComponent* DestroyAC;

	UPROPERTY()
	FGuid GUID;

	UFUNCTION()
	void OnRep_Instance();

	UPROPERTY(ReplicatedUsing=OnRep_Instance)
	UPlayerInventoryItem* Instance;

	UPROPERTY()
	UMaterialInterface* IndicatorMaterial;

	UPROPERTY()
	TArray<UMeshComponent*> MeshComponents;

	UPROPERTY()
	TArray<UBuildBoxComponent*> BoxComponents;

	UPROPERTY()
	TArray<UDestructibleComponent*> DestructibleComponents;

	UPROPERTY()
	UBuildBoxComponent* BoxOverlapChecker;

	UPROPERTY()
	UBuildBoxComponent* BoxFoundationChecker;
	
	UPROPERTY(EditDefaultsOnly, Category = Type)
	bool bCanSelectable;

	UPROPERTY(EditDefaultsOnly, Category = Type)
	bool bCanPlaceable;

	UPROPERTY(EditDefaultsOnly, Category = Type)
	bool bCanRemovable;

	UPROPERTY(EditDefaultsOnly, Category = Type)
	bool bIsPrePlaced;

	UPROPERTY(EditDefaultsOnly, Category = Type)
	bool bIsWorldGridComponent;

	UPROPERTY(EditDefaultsOnly, Category = Type)
	FVector WorldGridStep;

	UPROPERTY(EditDefaultsOnly, Category = Type, meta = (Bitmask, Bitmaskenum = "EAxis"))
	int32 AllowPlaceAxies;

	UPROPERTY(EditDefaultsOnly, Category = Type, meta = (Bitmask, Bitmaskenum = "EAxis"))
	int32 AllowRotateAxies;

	UPROPERTY(EditDefaultsOnly, Category = Type, Meta = (Bitmask, BitmaskEnum = "EBuildComponentType"))
	TEnumAsByte<EBuildComponentType::Type> Type;

	UPROPERTY(EditDefaultsOnly, Category = Type, Meta = (Bitmask, BitmaskEnum = "EBuildComponentType"))
	int32 SnapTo;

	UPROPERTY(EditDefaultsOnly, Category = Type)
	float BuildingTime;

	UFUNCTION()
	void OnRep_Health();

	UPROPERTY(EditDefaultsOnly, Category = Gameplay, ReplicatedUsing = OnRep_Health)
	FVector2D Health;

	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	FName MaterialParameterName;

	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	float RewardTotalScorePlayer;

	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	float RewardTotalScoreTeam;

	UPROPERTY()
	TArray<ABuildPlacedComponent*> ChildBuildComponents;

	UPROPERTY()
	TArray<ABuildPlacedComponent*> ParentBuildComponents;

	UPROPERTY()
	float LastDamageTime;

	UPROPERTY(EditDefaultsOnly, Category = Component)
	bool bIsAllowPlaceOnBuilding;

	UFUNCTION()
	virtual void OnRep_Death();

	UPROPERTY(ReplicatedUsing = OnRep_Death)
	bool bIsDeath;

	UPROPERTY(BlueprintReadOnly, Category = Component)
	TMap<APlayerState*, float> DamageByPlayers;

	// * Used only with pre-placed buildings for search instance on event
	UPROPERTY(EditDefaultsOnly, Category = Component)
	int32 PrePlacedInstanceId;

	UPROPERTY(EditDefaultsOnly, Category = Sound)
	USoundBase* PlaceSound;

	UPROPERTY(EditDefaultsOnly, Category = Sound)
	USoundBase* SelectSound;

	UPROPERTY(EditDefaultsOnly, Category = Sound)
	USoundBase* RemoveSound;
	
	UPROPERTY(BlueprintReadOnly, Category = Component)
	TMap<UMeshComponent*, FOrigenalMaterialsContainer> OriginalMaterialsMap;

	UPROPERTY(BlueprintReadOnly, Category = Component)
	bool IsAppliedDynamicMats;

	UPROPERTY(BlueprintReadOnly, Category = Component)
	TMap<FString, FWorldBuildingProperty> DynamicPropertyData;
};
