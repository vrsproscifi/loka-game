// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "Item/ItemBaseEntity.h"
#include "ShopBuildPlacedComponent.h"
#include "ConstructionPlayerState.h"
#include "MarketComponent.h"
#include "GameSingleton.h"
#include "ConstructionPlayerController.h"

AShopBuildPlacedComponent::AShopBuildPlacedComponent()
	: Super()
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
	PrimaryActorTick.TickInterval = .0f;
}

void AShopBuildPlacedComponent::InitializeMarket(UMarketComponent* InMarketComponent)
{
	MarketComponent = InMarketComponent;	

	if (WorldPreviewItem == nullptr)
	{
		FActorSpawnParameters ActorSpawnParameters;
		ActorSpawnParameters.Owner = this;
		ActorSpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		WorldPreviewItem = GetWorld()->SpawnActor<AWorldPreviewItem>(AWorldPreviewItem::StaticClass(), GetActorTransform() + SpawnItemTrans, ActorSpawnParameters);
	}
}

void AShopBuildPlacedComponent::OnFocusStart_Implementation(AActor* InInstigator)
{
	
}

void AShopBuildPlacedComponent::OnFocusLost_Implementation(AActor* InInstigator)
{
	
}

void AShopBuildPlacedComponent::OnInteractStart_Implementation(AActor* InInstigator)
{
	if (auto MyInstigator = GetValidObject<ACharacter, AActor>(InInstigator))
	{
		if (auto MyPlayerState = GetValidObject<AConstructionPlayerState, APlayerState>(MyInstigator->GetPlayerState()))
		{
			InitializeMarket(MyPlayerState->GetMarketComponent());
		}

		auto item = GetWorldPreviewItem();
		if (auto MyController = GetValidObject<AConstructionPlayerController, AController>(MyInstigator->GetController()))
		{
			MyController->SetWorldPreviewItem(item);
		}		

		if (const auto c = GetMarketComponent())
		{
			if (c && item)
			{
				c->ToggleMarketWindow(true);

				auto TargetQuat = FRotationMatrix::MakeFromX(MyInstigator->GetActorLocation() - item->GetActorLocation()).ToQuat();
				item->SetActorRotation(TargetQuat);
			}
		}
	}
}

void AShopBuildPlacedComponent::OnInteractLost_Implementation(AActor* InInstigator)
{

}

void AShopBuildPlacedComponent::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if(const auto c = GetMarketComponent())
	{
		if(auto item = GetWorldPreviewItem())
		{
			if (c->GetSelectedItem().CategoryTypeId != LastModelInfo.CategoryTypeId || c->GetSelectedItem().ModelId != LastModelInfo.ModelId)
			{
				LastModelInfo = c->GetSelectedItem();
//				item->InitializeInstance(UGameSingleton::Get()->FindItem(LastModelInfo));
			}
		}
	}
}

UMarketComponent* AShopBuildPlacedComponent::GetMarketComponent() const
{
	return GetValidObject(MarketComponent);
}

AWorldPreviewItem* AShopBuildPlacedComponent::GetWorldPreviewItem() const
{
	return GetValidObject(WorldPreviewItem);
}

void AShopBuildPlacedComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AShopBuildPlacedComponent, MarketComponent);
}
