// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "WarehousePlacedComponent.h"

FString AWarehousePlacedComponent::ParameterName_Storage = TEXT("Storage");

AWarehousePlacedComponent::AWarehousePlacedComponent()
	: Super()
{
	Storage = FIntPoint::ZeroValue;
}

void AWarehousePlacedComponent::OnInteractStart_Implementation(AActor* InInstigator)
{
	
}

void AWarehousePlacedComponent::OnInteractLost_Implementation(AActor* InInstigator)
{
	
}

void AWarehousePlacedComponent::OnDynamicPropertyDataUpdate_Implementation()
{
	Storage = GetDynamicPropertyData(ParameterName_Storage).ToIntPoint();
	OnRep_Storage();
}

void AWarehousePlacedComponent::OnRep_Storage_Implementation()
{
	
}

void AWarehousePlacedComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AWarehousePlacedComponent, Storage);
}
