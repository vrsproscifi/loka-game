// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "BuildSystemData.h"
#include "BuildPlacedComponent.h"
#include "BuildHelperComponent.generated.h"

#define COLLISION_OVERLAPBLOCKER ECC_GameTraceChannel11

class UItemBuildEntity;
class UPlayerInventoryItem;
//
UENUM(BlueprintType)
namespace EBuildHelperMode
{
	enum Type
	{
		None,
		Building,
		Selecting,
		End
	};
}

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnBuildItemSelected, const int32, TargetNum, UPlayerInventoryItem*, Item);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnBuildItemAction, UPlayerInventoryItem*, Item);

UCLASS(abstract, Config = Game)
class LOKAGAME_API ABuildHelperComponent : public AActor
{
	GENERATED_BODY()

public:	

	ABuildHelperComponent();

	UFUNCTION(BlueprintCallable, Category = Actions)
	void ProcessActionPlace(const bool IsPlace);

	UFUNCTION(Reliable, Server, WithValidation)
	void ServerActionPlace(const bool IsPlace);

	UFUNCTION(BlueprintCallable, Category = Actions)
	void ProcessActionRotate(const bool IsRight);

	UFUNCTION(BlueprintCallable, Category = Actions)
	void ProcessActionSelecting(const int32 InNumber);

	UFUNCTION(Reliable, Server, WithValidation)
	void ServerActionSelecting(const int32 InNumber);

	UFUNCTION(BlueprintCallable, Reliable, Server, WithValidation, Category = Actions)
	void ServerRequestPlace(const int32& InRotateNumTarget, const TArray<uint8>& InGridRotations);

	UFUNCTION(BlueprintCallable, Category = Actions)
	bool SetSelectionFromClass(const TSubclassOf<ABuildPlacedComponent>& InClass);

	UFUNCTION(BlueprintCallable, Category = Actions)
	void SetCurrentMode(const TEnumAsByte<EBuildHelperMode::Type>& InMode);

	UFUNCTION(Reliable, Server, WithValidation)
	void ServerSetCurrentMode(const uint8 InMode);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Actions)
	FORCEINLINE TEnumAsByte<EBuildHelperMode::Type> GetCurrentMode() const { return CurrentMode; }

	UFUNCTION(BlueprintCallable, Category = System)
	void SetSlotFromItem(const int32& InSlotNumTarget, UPlayerInventoryItem* InItem);

	UFUNCTION(Reliable, Server, WithValidation)
	void ServerSetSlotFromItem(const int32& InSlotNumTarget, UPlayerInventoryItem* InItem);

	UFUNCTION(BlueprintCallable, Category = System)
	bool DestroySelected();

	UFUNCTION(Reliable, Server, WithValidation)
	void ServerDestroySelected();

	UFUNCTION(Reliable, Server, WithValidation)
	void TakeSelected();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Actions)
	FORCEINLINE int32 GetSelectedNum() const { return SelectedNum; }

	UPROPERTY(BlueprintAssignable, Category = Actions)
	FOnBuildItemSelected OnBuildItemSelected;

	UPROPERTY(BlueprintAssignable, Category = Actions)
	FOnBuildItemAction OnBuildItemBuyRequest;

	UPROPERTY(BlueprintAssignable, Category = Actions)
	FOnBuildingSignature OnBuildingSelected;

	UPROPERTY(BlueprintAssignable, Category = Actions)
	FOnBuildingSignature OnBuildingPlaced;

	UPROPERTY(BlueprintAssignable, Category = Actions)
	FOnBuildingSignature OnBuildingPreRemove;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Actions)
	UPlayerInventoryItem* GetBuildItemFromSlot(const int32& InSlot);

	UFUNCTION(BlueprintCallable, Category = Actions)
	void SetTargetAxis(const EAxis::Type& InAxis);

	UFUNCTION(BlueprintCallable, Category = Actions)
	FORCEINLINE EAxis::Type GetTargetAxis() const { return TargetRotationAxis; }

	UFUNCTION(BlueprintCallable, Category = Actions)
	void ProccesRotateAxis();

	UFUNCTION(BlueprintCallable, Category = Actions)
	void SetAllowAnyMode(const bool InAllow);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Actions)
	FORCEINLINE bool IsAllowAnyMode() const { return bIsAllowAnyMode; }

	UFUNCTION(BlueprintCallable, Category = Actions)
	void StartLoadSavedInventory();

protected:

	static float LastCallTime;

	virtual void PostInitializeComponents() override;
	virtual void BeginPlay() override;

	UFUNCTION()
	void OnRep_Inventory();

	UPROPERTY(ReplicatedUsing=OnRep_Inventory, EditDefaultsOnly, Category = System)
	TArray<int32> Inventory;

	UPROPERTY(Config)
	TArray<int32> SavedInventory;

	UFUNCTION(Reliable, Server, WithValidation)
	void LoadSavedInventory(const TArray<int32>& InData);

	UFUNCTION(Reliable, Client)
	void ClientNotAllowAction(const int32 InAction);

	UPROPERTY(EditDefaultsOnly, Category = Appearance)
	UMaterialInterface* HoloMaterialSource;

	UPROPERTY(EditDefaultsOnly, Category = Appearance)
	UMaterialInterface* HoloMaterialSelecting;
		
	UPROPERTY(EditDefaultsOnly, Category = Appearance)
	FName HoloMaterialParameter;

	//* X - Allow, Y - Disallow
	UPROPERTY(EditDefaultsOnly, Category = Appearance)
	FVector2D HoloMaterialValues;

	UPROPERTY(BlueprintReadOnly, Category = System)
	UMaterialInstanceDynamic* HoloMaterialInstance;

	UPROPERTY(VisibleAnywhere)
	UChildActorComponent* ChildActorComponent;

	virtual void Tick(float DeltaSeconds) override;
	bool TryPlacement();
	bool TrySelecting(const bool InIsForAction = true);
	bool ProcessOverlaps(class UBuildBoxComponent*);
	bool ProcessOverlaps(const FVector& InLocation, const FQuat& InQuat, const ECollisionChannel& TraceChannel, const FCollisionShape& InCollisionShape);
	bool ProcessGrid(const FHitResult&);
	bool ProcessParents(ABuildPlacedComponent* InPlacedComponent = nullptr, const bool InAttach = false);

	UFUNCTION()
	void OnRep_CurrentComponent();

	UPROPERTY(ReplicatedUsing=OnRep_CurrentComponent, BlueprintReadOnly, Category = System)
	ABuildPlacedComponent* CurrentComponent;

	UPROPERTY(BlueprintReadOnly, Category = System)
	FTransform ConfirmedTransform;

	UPROPERTY(BlueprintReadOnly, Category = System)
	int32 RotateNumTarget;

	UPROPERTY(Replicated, BlueprintReadOnly, Category = System)
	int32 SelectedNum;

	UPROPERTY(BlueprintReadOnly, Category = System)
	bool bIsStopAroundRotation;

	UPROPERTY(BlueprintReadOnly, Category = System)
	ABuildPlacedComponent* LastSelectedComponent;

	UPROPERTY(BlueprintReadOnly, Category = System)
	ABuildPlacedComponent* CurrentSelectedComponent;

	ABuildPlacedComponent* GetLastSelectedComponent() const;
	ABuildPlacedComponent* GetCurrentSelectedComponent() const;


	UPROPERTY(Replicated, BlueprintReadOnly, Category = System)
	TEnumAsByte<EBuildHelperMode::Type> CurrentMode;

	UPROPERTY(Replicated, BlueprintReadOnly, Category = System)
	bool bIsAllowAnyMode;

	UFUNCTION(BlueprintCallable, Category = System)
	bool GetTraceLocations(FVector& OutStart, FVector& OutEnd, const bool IsForSelection = false);

	UFUNCTION(BlueprintCallable, Category = System)
	bool IsAllowAction(const int32& InFlag) const;

	UPROPERTY()
	TEnumAsByte<EAxis::Type> TargetRotationAxis;

	UPROPERTY()
	int32 TargetRotatedAxis[EAxis::Z + 1];

	virtual bool HasNetOwner() const override;
};
