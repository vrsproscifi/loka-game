﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "BuildBoxComponent.h"
#include "BuildSphereComponent.h"
#include "BuildArrowComponent.h"
#include "BuildHelperComponent.h"
#include "Build/ItemBuildEntity.h"
#include "ShooterGameInstance.h"
#include "PlayerInventoryItem.h"
#include "ConstructionGameState.h"
#include "ConstructionPlayerState.h"
#include "InventoryComponent.h"
#include "SMessageBox.h"
#include "RichTextHelpers.h"

float ABuildHelperComponent::LastCallTime = .0f;

// Sets default values for this component's properties
ABuildHelperComponent::ABuildHelperComponent()
	: Super()
{
	ChildActorComponent = CreateDefaultSubobject<UChildActorComponent>(TEXT("ChildActorComponent"));

	Inventory.Init(INDEX_NONE, 10);
	bIsStopAroundRotation = false;
	TargetRotationAxis = EAxis::Z;
	bIsAllowAnyMode = true;

	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
}

void ABuildHelperComponent::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	HoloMaterialInstance = UMaterialInstanceDynamic::Create(HoloMaterialSource, this);
	LoadConfig();
}

void ABuildHelperComponent::BeginPlay()
{
	Super::BeginPlay();
}

void ABuildHelperComponent::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ABuildHelperComponent, Inventory);
	DOREPLIFETIME(ABuildHelperComponent, SelectedNum);
	DOREPLIFETIME(ABuildHelperComponent, CurrentMode);
	DOREPLIFETIME(ABuildHelperComponent, CurrentComponent);
}

void ABuildHelperComponent::OnRep_CurrentComponent()
{
	if (const auto c = GetValidObject(CurrentComponent))
	{
		if (HasNetOwner())
		{
			c->SetActorEnableCollision(false);
			if (auto material = GetValidObject(HoloMaterialInstance))
			{
				c->SetIndicatorMaterial(material);
			}
		}
		else
		{
			SetActorEnableCollision(false);
			SetActorHiddenInGame(true);
		}
	}
}

void ABuildHelperComponent::ProcessActionPlace(const bool IsPlace)
{
	if (IsPlace)
	{
		if (CurrentMode == EBuildHelperMode::Building)
		{
			if (const auto c = GetValidObject(CurrentComponent))
			{
				if (c->CanPlaceable() && TryPlacement())
				{
					TArray<uint8> GridRotations;
					for (SIZE_T i = 0; i <= EAxis::Z; ++i)
					{
						GridRotations.Add(TargetRotatedAxis[i]);
					}

					ServerRequestPlace(RotateNumTarget, GridRotations);
				}
			}
		}
		else if (CurrentMode == EBuildHelperMode::Selecting && TrySelecting())
		{
			DestroySelected();
		}
	}
	else
	{
		if (CurrentMode == EBuildHelperMode::Selecting && TrySelecting())
		{
			TakeSelected();
		}
		else
		{
			if (const auto c = GetValidObject(CurrentComponent))
			{
				if (c->IsWorldGridComponent())
				{
					auto TargetAxis = GetTargetAxis();
					if (TargetAxis == EAxis::Z)
					{
						TargetAxis = EAxis::X;
					}
					else
					{
						TargetAxis = static_cast<EAxis::Type>(TargetAxis + 1);
					}

					SetTargetAxis(TargetAxis);
				}
				else
				{
					bIsStopAroundRotation = !bIsStopAroundRotation;
				}
			}
			else
			{
				bIsStopAroundRotation = !bIsStopAroundRotation;
			}
		}
	}
}

bool ABuildHelperComponent::ServerActionPlace_Validate(const bool IsPlace) { return true; }
void ABuildHelperComponent::ServerActionPlace_Implementation(const bool IsPlace)
{
	ProcessActionPlace(IsPlace);
}

void ABuildHelperComponent::ProcessActionRotate(const bool IsRight)
{
	if (CurrentComponent && CurrentComponent->IsWorldGridComponent())
	{
		ProccesRotateAxis();
	}
	else
	{
		if (IsRight)
			++RotateNumTarget;
		else
			--RotateNumTarget;
	}
}

void ABuildHelperComponent::ProcessActionSelecting(const int32 InNumber)
{
	if (HasAuthority())
	{
		auto TargetItem = GetBuildItemFromSlot(InNumber);
		if (TargetItem && IsAllowAnyMode())
		{
			if (CurrentMode != EBuildHelperMode::Building)
			{
				SetCurrentMode(EBuildHelperMode::Building);
			}

			SelectedNum = InNumber;
			SetSelectionFromClass(TargetItem->GetEntity<UItemBuildEntity>()->ComponentTemplate);
		}
	}
	else
	{
		ServerActionSelecting(InNumber);
	}
}

bool ABuildHelperComponent::ServerActionSelecting_Validate(const int32 InNumber) { return true; }
void ABuildHelperComponent::ServerActionSelecting_Implementation(const int32 InNumber)
{
	ProcessActionSelecting(InNumber);
}

bool ABuildHelperComponent::TakeSelected_Validate() { return true; }
void ABuildHelperComponent::TakeSelected_Implementation()
{
	if (IsAllowAction(EAllowConstructionMode::Selecting) == false)
	{
		ClientNotAllowAction(EAllowConstructionMode::Selecting);
		return;
	}

	if (TrySelecting())
	{
		UPlayerInventoryItem* ItemInst = CurrentSelectedComponent ? CurrentSelectedComponent->Instance : nullptr;
		if (DestroySelected())
		{
			bool IsTakedIntoFreeSlot = false;
			for (SIZE_T i = 1; i < Inventory.Num(); ++i)
			{
				auto TargetItem = GetBuildItemFromSlot(i);
				if (TargetItem == ItemInst || TargetItem == nullptr)
				{
					SetSlotFromItem(i, ItemInst);
					SelectedNum = i;
					IsTakedIntoFreeSlot = true;
					break;
				}
			}

			if (IsTakedIntoFreeSlot == false)
			{
				SetSlotFromItem(1, ItemInst);
				SelectedNum = 1;
			}

			SetCurrentMode(EBuildHelperMode::Building);
		}
	}
}

bool ABuildHelperComponent::DestroySelected()
{
	if (IsAllowAction(EAllowConstructionMode::Selecting) == false)
	{
		ClientNotAllowAction(EAllowConstructionMode::Selecting);
		return false;
	}

	if (LastCallTime < GetWorld()->GetTimeSeconds() || GetNetMode() == NM_Standalone)
	{
		LastCallTime = GetWorld()->GetTimeSeconds() + 0.1f;
		if (HasAuthority() && TrySelecting())
		{
			if (CurrentSelectedComponent && CurrentSelectedComponent->CanRemovable())
			{
				if (auto Inst = CurrentSelectedComponent->Instance)
				{
					Inst->SetCount(Inst->GetCount() + 1);

					//for (auto Component : CurrentSelectedComponent->GetAllSnappedComponents())
					//{
					//	if (auto SubInst = Component->Instance)
					//	{
					//		SubInst->SetCount(SubInst->GetCount() + 1);
					//	}

					//	if (MyGameMode)
					//	{
					//		MyGameMode->buildingController.RemoveObject.Request(Component->GUID.ToString());
					//	}

					//	Component->Destroy();
					//	Component = nullptr;
					//}

					OnBuildingPreRemove.Broadcast(CurrentSelectedComponent);

					if (CurrentSelectedComponent->RemoveSound) UGameplayStatics::PlaySoundAtLocation(GetWorld(), CurrentSelectedComponent->RemoveSound, CurrentSelectedComponent->GetActorLocation());

					CurrentSelectedComponent->Destroy();
					CurrentSelectedComponent = nullptr;

					return true;
				}
			}
		}
		else
		{
			ServerDestroySelected();
		}
	}

	return false;
}

bool ABuildHelperComponent::ServerDestroySelected_Validate() { return true; }
void ABuildHelperComponent::ServerDestroySelected_Implementation() 
{ 
	DestroySelected();
}

void ABuildHelperComponent::SetCurrentMode(const TEnumAsByte<EBuildHelperMode::Type>& InMode)
{
	if (CurrentMode != InMode)
	{
		if (FFlagsHelper::HasAnyFlags(CurrentMode, EBuildHelperMode::Building) && CurrentComponent)
		{
			ChildActorComponent->DestroyChildActor();
			CurrentComponent = nullptr;
		}
		else if (FFlagsHelper::HasAnyFlags(CurrentMode, EBuildHelperMode::Selecting) && LastSelectedComponent)
		{
			LastSelectedComponent->SetIndicatorMaterial(nullptr, false, false);
			LastSelectedComponent = nullptr;
		}
	}

	if (HasAuthority())
	{
		CurrentMode = InMode;

		if (CurrentMode == EBuildHelperMode::Building && Inventory.IsValidIndex(SelectedNum) && Inventory[SelectedNum])
		{
			ProcessActionSelecting(SelectedNum);
		}
	}
	else
	{
		ServerSetCurrentMode(InMode);
	}
}

bool ABuildHelperComponent::ServerSetCurrentMode_Validate(const uint8 InMode) { return true; }
void ABuildHelperComponent::ServerSetCurrentMode_Implementation(const uint8 InMode)
{
	SetCurrentMode(TEnumAsByte<EBuildHelperMode::Type>(InMode));
}

bool ABuildHelperComponent::ServerRequestPlace_Validate(const int32& InRotateNumTarget, const TArray<uint8>& InGridRotations) { return true; }
void ABuildHelperComponent::ServerRequestPlace_Implementation(const int32& InRotateNumTarget, const TArray<uint8>& InGridRotations)
{
	auto TargetItem = GetBuildItemFromSlot(SelectedNum);

	if (IsAllowAction(EAllowConstructionMode::Building) == false)
	{
		ClientNotAllowAction(EAllowConstructionMode::Building);
		return;
	}

	if (TargetItem && SetSelectionFromClass(TargetItem->GetEntity<UItemBuildEntity>()->ComponentTemplate))
	{
		RotateNumTarget = InRotateNumTarget;

		SIZE_T _count = 0;
		for (auto TargetAxis : InGridRotations)
		{
			TargetRotatedAxis[_count] = TargetAxis;
			++_count;
		}		

		if (TryPlacement())
		{
			if (TargetItem->GetCount() <= 0)
			{
				OnBuildItemBuyRequest.Broadcast(TargetItem);
			}
			else
			{
				TargetItem->SetCount(TargetItem->GetCount() - 1);

				FActorSpawnParameters ActorSpawnParameters;
				ActorSpawnParameters.Instigator = Cast<ACharacter>(GetOwner());
				ActorSpawnParameters.Owner = GetOwner();
				ActorSpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				if (auto SpawnedActor = GetWorld()->SpawnActorDeferred<ABuildPlacedComponent>(CurrentComponent->GetClass(), ConfirmedTransform, ActorSpawnParameters.Owner, ActorSpawnParameters.Instigator, ActorSpawnParameters.SpawnCollisionHandlingOverride))
				{
					SpawnedActor->GUID = FGuid::NewGuid();
					SpawnedActor->OnComponentPlaced();

					if (CurrentSelectedComponent)
					{
						CurrentSelectedComponent->ChildBuildComponents.Add(SpawnedActor);
						SpawnedActor->ParentBuildComponents.Add(CurrentSelectedComponent);
					}

					SpawnedActor->FinishSpawning(ConfirmedTransform);
					SpawnedActor->InitializeInstance(TargetItem);

					OnBuildingPlaced.Broadcast(SpawnedActor);

					if (const auto PlaceSound = GetValidObject(SpawnedActor->PlaceSound))
					{
						UGameplayStatics::PlaySoundAtLocation(GetWorld(), PlaceSound, SpawnedActor->GetActorLocation());
					}
				}
			}
		}
	}
}

bool ABuildHelperComponent::SetSelectionFromClass(const TSubclassOf<ABuildPlacedComponent>& InClass)
{
	CurrentComponent = nullptr;

	if (auto child = GetValidObject(ChildActorComponent))
	{
		child->DestroyChildActor();

		if (InClass)
		{
			child->SetChildActorClass(InClass);
			child->CreateChildActor();

			CurrentComponent = GetValidObject<ABuildPlacedComponent, AActor>(child->GetChildActor());

			if (CurrentComponent)
			{
				CurrentComponent->SetActorEnableCollision(false);
				CurrentComponent->SetIndicatorMaterial(HoloMaterialInstance);

				return true;
			}
		}
	}
	return false;
}

void ABuildHelperComponent::SetSlotFromItem(const int32& InSlotNumTarget, UPlayerInventoryItem* InItem)
{
	if (HasAuthority())
	{
		if (Inventory.IsValidIndex(InSlotNumTarget))
		{
			Inventory[InSlotNumTarget] = InItem ? InItem->GetEntityModel().ModelId : INDEX_NONE;
			auto TargetItem = GetBuildItemFromSlot(InSlotNumTarget);

			OnBuildItemSelected.Broadcast(InSlotNumTarget, TargetItem);

			if (InSlotNumTarget == SelectedNum && CurrentComponent && TargetItem)
			{
				if (auto entity = TargetItem->GetEntity<UItemBuildEntity>())
				{
					SetSelectionFromClass(entity->ComponentTemplate);
				}
			}

			OnRep_Inventory();
		}
	}
	else
	{
		ServerSetSlotFromItem(InSlotNumTarget, InItem);
	}
}

bool ABuildHelperComponent::ServerSetSlotFromItem_Validate(const int32& InSlotNumTarget, UPlayerInventoryItem* InItem) { return true; }
void ABuildHelperComponent::ServerSetSlotFromItem_Implementation(const int32& InSlotNumTarget, UPlayerInventoryItem* InItem)
{
	SetSlotFromItem(InSlotNumTarget, InItem);
}

void ABuildHelperComponent::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);	

	if (GetNetMode() != NM_DedicatedServer && HasNetOwner())
	{
		if (CurrentMode == EBuildHelperMode::Building)
		{
			if (HoloMaterialInstance && CurrentComponent)
			{
				const bool IsAllow = TryPlacement();
				HoloMaterialInstance->SetScalarParameterValue(HoloMaterialParameter, IsAllow ? HoloMaterialValues.X : HoloMaterialValues.Y);
			}
		}
		else if (CurrentMode == EBuildHelperMode::Selecting)
		{
			TrySelecting(false);
		}
	}
}

bool ABuildHelperComponent::TryPlacement()
{
#define DEFAULT_DEBUGBOXDRAW_PARAMS false, -1.0f, (uint8)'\000'

	if (GetWorld() && CurrentComponent && CurrentComponent->GetType() != EBuildComponentType::None && CurrentComponent->CanPlaceable())
	{
		// Trace to detect snap target
		FVector StartTrace = FVector::ZeroVector;
		FVector EndTrace = FVector::ZeroVector;

		if (GetTraceLocations(StartTrace, EndTrace))
		{
			bool IsSnapped = false;

			FHitResult HitResult;
			TArray<FHitResult> HitResults;
			FCollisionQueryParams QueryFindSnapBox(TEXT("FindBuildingSnap"), true, this);
			QueryFindSnapBox.AddIgnoredActor(GetOwner());
			QueryFindSnapBox.AddIgnoredActor(CurrentComponent);

			FCollisionResponseParams ResponseFindSnapBox = FCollisionResponseParams::DefaultResponseParam;
			ResponseFindSnapBox.CollisionResponse.SetAllChannels(ECR_Overlap);
			ResponseFindSnapBox.CollisionResponse.SetResponse(COLLISION_PLAYERBUILDING_OBJECT, ECR_Block);
			ResponseFindSnapBox.CollisionResponse.SetResponse(ECC_Visibility, ECR_Block);
			ResponseFindSnapBox.CollisionResponse.SetResponse(ECC_WorldStatic, ECR_Block);

			GetWorld()->LineTraceMultiByChannel(HitResults, StartTrace, EndTrace, COLLISION_PLAYERBUILDING_BOX, QueryFindSnapBox, ResponseFindSnapBox);

			ResponseFindSnapBox.CollisionResponse.SetAllChannels(ECR_Ignore);
			ResponseFindSnapBox.CollisionResponse.SetResponse(COLLISION_PLAYERBUILDING_OBJECT, ECR_Block);

			GetWorld()->LineTraceSingleByChannel(HitResult, StartTrace, EndTrace, COLLISION_PLAYERBUILDING_OBJECT, QueryFindSnapBox, ResponseFindSnapBox);

			if (CurrentComponent->IsWorldGridComponent())
			{
				IsSnapped = ProcessGrid(HitResult);
			}
			else
			{
				if (FHitResult::GetNumBlockingHits(HitResults) <= 0 && FHitResult::GetNumOverlapHits(HitResults) <= 0)
				{
#if (ENABLE_DRAW_DEBUG && WITH_EDITOR)
					::DrawDebugLine(GetWorld(), StartTrace, EndTrace, FColor::Red);
#endif
					SetActorLocation(EndTrace);
					if (bIsStopAroundRotation == false)
					{
						SetActorRotation(GetOwner()->GetActorRotation());
					}
				}
				else
				{
					if (auto Target = Cast<ABuildPlacedComponent>(HitResult.GetActor()))
					{
						HitResults.Sort([t = Target](const FHitResult& A, const FHitResult& B) {return A.GetActor() == t; });
					}

					for (auto result : HitResults)
					{
						//if (result.bBlockingHit)
						{
							auto HitActor = Cast<ABuildPlacedComponent>(result.GetActor());
							auto HitComponent = Cast<UBuildBoxComponent>(result.GetComponent());

							if (HitComponent && result.GetActor() && !HitActor)
							{
								HitActor = Cast<ABuildPlacedComponent>(result.GetActor()->GetOwner());
							}

							if (HitActor && HitComponent)
							{
								CurrentSelectedComponent = HitActor;
								auto SnapTo = CurrentComponent->GetSnapTo();

								if (HitComponent->IsAllowedComponnent(SnapTo))
								{
									if (HitComponent->GetAttachChildren().Num())
									{
#if (ENABLE_DRAW_DEBUG && WITH_EDITOR)
										::DrawDebugBox(GetWorld(), HitComponent->GetComponentLocation(), HitComponent->GetScaledBoxExtent(), HitComponent->GetComponentQuat(), FColor::Cyan, DEFAULT_DEBUGBOXDRAW_PARAMS, 4);
#endif

										if (auto SnapPointComp = Cast<UBuildSphereComponent>(HitComponent->GetAttachChildren()[0]))
										{
											FVector CurrentActorLocation = CurrentComponent->GetActorLocation();

											float BestDist = 100.0f * 50.0f;
											UBuildSphereComponent* BestSnap = nullptr;
											auto SnapRemoteLocation = SnapPointComp->GetComponentLocation();

											TArray<UBuildSphereComponent*> SnapPoints;
											CurrentComponent->GetComponents<UBuildSphereComponent>(SnapPoints);

											for (auto Snap : SnapPoints)
											{
												auto SnapLocalLocation = Snap->GetComponentLocation();
												auto SnapDist = FVector::Dist(SnapRemoteLocation, SnapLocalLocation);
												const bool isSnappedToAllowedComp = FFlagsHelper::HasAnyFlags(Snap->AllowedComponents, FFlagsHelper::ToFlag<int32>(EBuildComponentType::None) | FFlagsHelper::ToFlag<int32>(HitActor->GetType().GetValue()));
												const bool isSnappedToAllowedSnap = FFlagsHelper::HasAnyFlags(SnapPointComp->AllowedSnaps, FFlagsHelper::ToFlag<int32>(Snap->SnapType.GetValue()));
												if (SnapDist < BestDist && isSnappedToAllowedComp && isSnappedToAllowedSnap)
												{
													BestDist = SnapDist;
													BestSnap = Snap;
												}
											}

											if (BestSnap)
											{
												ConfirmedTransform = FTransform::Identity;
												UBuildArrowComponent* FaceArrow = nullptr;

												if (BestSnap->GetAttachChildren().Num())
												{
													for (auto AComp : BestSnap->GetAttachChildren())
													{
														auto arrow = Cast<UBuildArrowComponent>(AComp);
														if (arrow && arrow->IsFaceArrow())
														{
															FaceArrow = arrow;
															break;
														}
													}

													TArray<UBuildArrowComponent*> Arrows;
													for (auto AComp : SnapPointComp->GetAttachChildren())
													{
														if (auto arrow = Cast<UBuildArrowComponent>(AComp))
														{
															if (arrow->IsAllowedComponnent(SnapTo))
															{
																Arrows.Add(arrow);
															}
														}
													}

													if (Arrows.Num() && FaceArrow)
													{
														if (RotateNumTarget > 0 && Arrows.IsValidIndex(RotateNumTarget) == false)
														{
															RotateNumTarget = 0;
														}
														else if (Arrows.IsValidIndex(RotateNumTarget) == false)
														{
															RotateNumTarget = Arrows.Num() - 1;
														}

														FTransform ComponentTrans = GetTransform();
														ComponentTrans.SetScale3D(FVector::OneVector);
														ComponentTrans.SetLocation(FVector::ZeroVector);
														ComponentTrans.NormalizeRotation();
														FTransform ArrowTrans = Arrows[RotateNumTarget]->GetComponentTransform();
														ArrowTrans.SetScale3D(FVector::OneVector);
														ArrowTrans.SetLocation(FVector::ZeroVector);
														ArrowTrans.NormalizeRotation();
														FTransform FaceArrowTrans = FaceArrow->GetComponentTransform();
														FaceArrowTrans.SetScale3D(FVector::OneVector);
														FaceArrowTrans.SetLocation(FVector::ZeroVector);
														FaceArrowTrans.NormalizeRotation();
														FTransform RelativeTrans = FaceArrowTrans.GetRelativeTransform(ComponentTrans);
														RelativeTrans.NormalizeRotation();
														FQuat FinalRotation = ArrowTrans.GetRotation() + RelativeTrans.GetRotation();
														FTransform FinalTrans = ArrowTrans * RelativeTrans;
														//FinalTrans.NormalizeRotation();
														FinalRotation.Normalize();
														ConfirmedTransform.SetRotation(FinalTrans.GetRotation());

														//FRotator ArrowRotation = Arrows[RotateNumTarget]->GetComponentToWorld().Rotator().GetNormalized();
														//FRotator FaceArrowRotation = FaceArrow->GetComponentToWorld().Rotator().GetNormalized();
														//FRotator RotationOffset = ArrowRotation - FaceArrowRotation;
														//FRotator OriginRotation = GetActorRotation().GetNormalized();
														//FRotator FinalRotation = OriginRotation + RotationOffset;

																											//auto SnapRemoteRotation = Arrows[RotateNumTarget]->GetComponentRotation();
																											//auto RotationOffset = GetActorRotation() - FaceArrow->GetComponentRotation();
																											//ConfirmedTransform.SetRotation((SnapRemoteRotation + RotationOffset).Quaternion());

#if (ENABLE_DRAW_DEBUG && WITH_EDITOR)
														const auto DebugAngle = FMath::DegreesToRadians(25);
														::DrawDebugCone(GetWorld(), Arrows[RotateNumTarget]->GetComponentLocation(), Arrows[RotateNumTarget]->GetForwardVector(), 100, DebugAngle, DebugAngle, 6, FColorList::Orange, DEFAULT_DEBUGBOXDRAW_PARAMS, 3);
														::DrawDebugCone(GetWorld(), FaceArrow->GetComponentLocation(), FaceArrow->GetForwardVector(), 100, DebugAngle, DebugAngle, 4, FColorList::OrangeRed, DEFAULT_DEBUGBOXDRAW_PARAMS, 2);
#endif

														//ConfirmedTransform.SetRotation(FinalRotation);
													}
													else
													{
														ConfirmedTransform.SetRotation(HitActor->GetActorQuat());
														SetActorRotation(HitActor->GetActorRotation());
													}
												}
												else
												{
													ConfirmedTransform.SetRotation(HitActor->GetActorQuat());
													SetActorRotation(HitActor->GetActorRotation());
												}

												//auto PlaceOffset = FRotationMatrix(ConfirmedTransform.Rotator()).TransformPosition(BestSnap->RelativeLocation);
												auto PlaceOffset = CurrentActorLocation - BestSnap->GetComponentLocation();
												ConfirmedTransform.SetLocation(SnapRemoteLocation + PlaceOffset);
												SetActorTransform(ConfirmedTransform);

#if (ENABLE_DRAW_DEBUG && WITH_EDITOR)
												::DrawDebugSphere(GetWorld(), BestSnap->GetComponentLocation(), BestSnap->GetScaledSphereRadius(), 10, FColor::Blue, DEFAULT_DEBUGBOXDRAW_PARAMS, 4);
												::DrawDebugSphere(GetWorld(), SnapPointComp->GetComponentLocation(), SnapPointComp->GetScaledSphereRadius(), 8, FColorList::ForestGreen, DEFAULT_DEBUGBOXDRAW_PARAMS, 3);
#endif

												IsSnapped = true;
												break;
											}
										}
									}
								}
								else
								{
#if (ENABLE_DRAW_DEBUG && WITH_EDITOR)
									::DrawDebugPoint(GetWorld(), result.Location, 10, FColorList::Magenta);
#endif
								}
							}
						}
					}

					if (IsSnapped == false && FHitResult::GetFirstBlockingHit(HitResults))
					{
						CurrentSelectedComponent = nullptr;
						HitResult = *FHitResult::GetFirstBlockingHit(HitResults);

#if (ENABLE_DRAW_DEBUG && WITH_EDITOR)
						::DrawDebugLine(GetWorld(), StartTrace, HitResult.Location, FColor::Green);
						::DrawDebugPoint(GetWorld(), HitResult.Location, 6, FColor::Red);
#endif
						SetActorLocation(HitResult.Location);
						if (bIsStopAroundRotation == false)
						{
							SetActorRotation(GetOwner()->GetActorRotation());
						}
					}
				}
			}

			// Trace to detect terrain
			auto FoundationChecker = CurrentComponent->GetFoundationChecker();
			bool IsNeedFindTerrain = false;

			if (FoundationChecker)
			{
				IsNeedFindTerrain = (FoundationChecker->IsAlwaysFindTerrain() || IsSnapped == false);
			}

			if (IsNeedFindTerrain)
			{
				FQuat QueryBoxQuat = FQuat::Identity;
				FVector QueryBoxExtent = FVector::ZeroVector;
				FCollisionQueryParams QueryFindTerrainPoint(TEXT("FindTerrainPoint"), true, this);
				QueryFindTerrainPoint.bTraceAsyncScene = true;
				QueryFindTerrainPoint.AddIgnoredActor(GetOwner());
				QueryFindTerrainPoint.AddIgnoredActor(CurrentComponent);

				QueryBoxExtent = FoundationChecker->GetScaledBoxExtent();
				QueryBoxQuat = FoundationChecker->GetComponentQuat();
				FVector CompLocation = FoundationChecker->GetComponentLocation();
				CompLocation.Z -= QueryBoxExtent.Z * 2;
#if (ENABLE_DRAW_DEBUG && WITH_EDITOR)
				::DrawDebugBox(GetWorld(), CompLocation, QueryBoxExtent, QueryBoxQuat, FColor::Green);
#endif
				StartTrace = CompLocation;
				EndTrace = CompLocation;

				StartTrace.Z += QueryBoxExtent.Z;
				EndTrace.Z -= QueryBoxExtent.Z;

				if (IsSnapped == false)
				{
					CurrentSelectedComponent = nullptr;
					GetWorld()->SweepSingleByChannel(HitResult, StartTrace, EndTrace, QueryBoxQuat, COLLISION_PLAYERBUILDING_OBJECT, FCollisionShape::MakeBox(FVector(QueryBoxExtent.X, QueryBoxExtent.Y, 1.0f)), QueryFindTerrainPoint);

					if (HitResult.bBlockingHit)
					{
#if (ENABLE_DRAW_DEBUG && WITH_EDITOR)
						::DrawDebugBox(GetWorld(), HitResult.Location, FVector(QueryBoxExtent.X, QueryBoxExtent.Y, 1.0f), QueryBoxQuat, FColor::Red);
						::DrawDebugPoint(GetWorld(), HitResult.Location, 6, FColor::Red);
#endif			
						ConfirmedTransform = FTransform::Identity;
						ConfirmedTransform.SetLocation(FVector(HitResult.Location.X, HitResult.Location.Y, HitResult.Location.Z - (QueryBoxExtent.Z * 2)));
						ConfirmedTransform.SetRotation(QueryBoxQuat);
						SetActorTransform(ConfirmedTransform);
					}
				}

				if (IsSnapped == true)
				{
					StartTrace.Z += QueryBoxExtent.Z * 2;
					EndTrace.Z += QueryBoxExtent.Z * 2;
				}

				FRotationMatrix RotationMatrix(QueryBoxQuat.Rotator());

				FVector SubTrace[4];
				SubTrace[0].X = QueryBoxExtent.X;
				SubTrace[0].Y = QueryBoxExtent.Y;
				SubTrace[0].Z = 0;

				SubTrace[1].X = -QueryBoxExtent.X;
				SubTrace[1].Y = -QueryBoxExtent.Y;
				SubTrace[1].Z = 0;

				SubTrace[2].X = -QueryBoxExtent.X;
				SubTrace[2].Y = QueryBoxExtent.Y;
				SubTrace[2].Z = 0;

				SubTrace[3].X = QueryBoxExtent.X;
				SubTrace[3].Y = -QueryBoxExtent.Y;
				SubTrace[3].Z = 0;

				bool IsSuccessFoundHit[4];

				for (SIZE_T i = 0; i < 4; ++i)
				{
					SubTrace[i] = RotationMatrix.TransformPosition(SubTrace[i]);
					GetWorld()->LineTraceSingleByChannel(HitResult, StartTrace + SubTrace[i], EndTrace + SubTrace[i], COLLISION_PLAYERBUILDING_OBJECT, QueryFindTerrainPoint);
					IsSuccessFoundHit[i] = (HitResult.bBlockingHit && (CurrentComponent->IsAllowPlaceOnBuilding() || Cast<ABuildPlacedComponent>(HitResult.GetActor()) == nullptr));

#if (ENABLE_DRAW_DEBUG && WITH_EDITOR)
					::DrawDebugLine(GetWorld(), StartTrace + SubTrace[i], HitResult.bBlockingHit ? HitResult.Location : EndTrace + SubTrace[i], FColor::Orange, DEFAULT_DEBUGBOXDRAW_PARAMS, 4);
					if (HitResult.bBlockingHit)
					{
						::DrawDebugPoint(GetWorld(), HitResult.Location, 20, FColor::Green);
					}
#endif
				}

				IsSnapped = (IsSuccessFoundHit[0] && IsSuccessFoundHit[1] && IsSuccessFoundHit[2] && IsSuccessFoundHit[3]);
			}

			// Find any overlap object
			auto OverlapChecker = CurrentComponent->GetOverlapChecker();
			if (IsSnapped && OverlapChecker)
			{
				IsSnapped = !ProcessOverlaps(OverlapChecker);
			}

			return IsSnapped;
		}
	}

	return false;
}

ABuildPlacedComponent* ABuildHelperComponent::GetLastSelectedComponent() const
{
	return GetValidObject(LastSelectedComponent);
}

ABuildPlacedComponent* ABuildHelperComponent::GetCurrentSelectedComponent() const
{
	return GetValidObject(CurrentSelectedComponent);
}

bool ABuildHelperComponent::TrySelecting(const bool InIsForAction)
{
	if (GetWorld() && CurrentComponent == nullptr)
	{
		//if ((InIsForAction && LastCallTime < GetWorld()->GetTimeSeconds()) || GetNetMode() != NM_DedicatedServer)
		{
			//LastCallTime = GetWorld()->GetTimeSeconds() + 0.1f;
			FVector StartTrace = FVector::ZeroVector;
			FVector EndTrace = FVector::ZeroVector;

			if (GetTraceLocations(StartTrace, EndTrace, true) && StartTrace.ContainsNaN() == false && EndTrace.ContainsNaN() == false)
			{
				FHitResult HitResult;
				FCollisionQueryParams QueryFindPlacedObject(*FString::Printf(TEXT("FindPlacedObject_%d"), FDateTime::UtcNow().GetMillisecond()), false, this);
				QueryFindPlacedObject.bTraceAsyncScene = true;
				QueryFindPlacedObject.AddIgnoredActor(GetOwner());

				FCollisionResponseParams ResponseFindPlacedObject = FCollisionResponseParams::DefaultResponseParam;
				ResponseFindPlacedObject.CollisionResponse.SetAllChannels(ECR_Overlap);
				ResponseFindPlacedObject.CollisionResponse.SetResponse(COLLISION_PLAYERBUILDING_OBJECT, ECR_Block);

				GetWorld()->LineTraceSingleByChannel(HitResult, StartTrace, EndTrace, COLLISION_PLAYERBUILDING_OBJECT, QueryFindPlacedObject, ResponseFindPlacedObject);

				if (HitResult.Actor.IsValid())
				{
					//	Раньше почему то вылетало, попробую так решить проблему
					CurrentSelectedComponent = GetValidObject<ABuildPlacedComponent, AActor>(HitResult.GetActor());
				}
				else
				{
					CurrentSelectedComponent = nullptr;
				}

				if (CurrentSelectedComponent && CurrentSelectedComponent->CanSelectable())
				{
					if (CurrentSelectedComponent != GetLastSelectedComponent())
					{
						if (auto last = GetLastSelectedComponent())
						{
							last->SetIndicatorMaterial(nullptr, false, false);
						}

						LastSelectedComponent = CurrentSelectedComponent;

						if (LastSelectedComponent)
						{
							LastSelectedComponent->SetIndicatorMaterial(HoloMaterialSelecting, false, false);
							OnBuildingSelected.Broadcast(LastSelectedComponent);
							if (auto sound = GetValidObject(LastSelectedComponent->SelectSound))
							{
								UGameplayStatics::PlaySoundAtLocation(GetWorld(), sound, LastSelectedComponent->GetActorLocation());
							}
						}
					}

					return (CurrentSelectedComponent != nullptr);
				}
				else if (CurrentSelectedComponent != LastSelectedComponent && LastSelectedComponent)
				{
					LastSelectedComponent->SetIndicatorMaterial(nullptr, false, false);
					LastSelectedComponent = nullptr;
				}
			}
		}
	}

	return false;
}

bool ABuildHelperComponent::GetTraceLocations(FVector& OutStart, FVector& OutEnd, const bool IsForSelection)
{
	ACharacter* PlayerCharacter = GetValidObject<ACharacter, AActor>(GetOwner());
	APlayerController* PlayerController = PlayerCharacter ? GetValidObject<APlayerController, AController>(PlayerCharacter->GetController()) : nullptr;
	OutStart = FVector::ZeroVector;
	OutEnd = FVector::ZeroVector;

	if (PlayerController && PlayerCharacter)
	{
		FVector CamLoc;
		FRotator CamRot;
		PlayerController->GetPlayerViewPoint(CamLoc, CamRot);
		//CamLoc = PlayerCharacter->GetActorLocation() + FVector(0, 0, PlayerCharacter->GetSimpleCollisionHalfHeight());
		OutEnd = CamLoc + CamRot.Vector() * 100.0f * 30.0f;
		OutStart = IsForSelection ? CamLoc + CamRot.Vector() * 20.0f : CamLoc + CamRot.Vector() * 100.0f * 2.0f;

		return true;
	}

	return false;
}

bool ABuildHelperComponent::IsAllowAction(const int32& InFlag) const
{
	ACharacter* PlayerCharacter = Cast<ACharacter>(GetOwner());
	if (PlayerCharacter && PlayerCharacter->GetPlayerState())
	{
		if (auto cState = Cast<AConstructionPlayerState>(PlayerCharacter->GetPlayerState()))
		{
			return FFlagsHelper::HasAnyFlags(cState->AllowFlags, InFlag);
		}
	}

	return false;
}

bool ABuildHelperComponent::HasNetOwner() const
{
	if (auto MyFirstOwner = GetValidObject<ACharacter>(GetOwner()))
	{
		return MyFirstOwner->IsLocallyControlled();
	}

	return false;
}

bool ABuildHelperComponent::ProcessOverlaps(UBuildBoxComponent* InTarget)
{
	if (const auto target = GetValidObject(InTarget))
	{
		const FVector QueryBoxExtent = target->GetScaledBoxExtent();
		const FQuat QueryBoxQuat = target->GetComponentQuat();
		const FVector CompLocation = target->GetComponentLocation();
		const FCollisionShape CollisionShape = FCollisionShape::MakeBox(QueryBoxExtent);

		return ProcessOverlaps(CompLocation, QueryBoxQuat, COLLISION_OVERLAPBLOCKER, CollisionShape);
	}

	return false;
}

bool ABuildHelperComponent::ProcessOverlaps(const FVector& InLocation, const FQuat& InQuat, const ECollisionChannel& TraceChannel, const FCollisionShape& InCollisionShape)
{
	FCollisionQueryParams QueryFindOverlap(TEXT("BuildFindOverlap"), true, this);
	QueryFindOverlap.bTraceAsyncScene = true;
	QueryFindOverlap.AddIgnoredActor(CurrentComponent);

	FCollisionResponseParams ResponseFindOverlap = FCollisionResponseParams::DefaultResponseParam;
	ResponseFindOverlap.CollisionResponse.SetAllChannels(ECR_Block);
	ResponseFindOverlap.CollisionResponse.SetResponse(ECC_Camera, ECR_Ignore);
	ResponseFindOverlap.CollisionResponse.SetResponse(ECC_Visibility, ECR_Ignore);

	TArray<FOverlapResult> Overlaps;
	bool IsSnapped = GetWorld()->OverlapMultiByChannel(Overlaps, InLocation, InQuat, TraceChannel, InCollisionShape, QueryFindOverlap, ResponseFindOverlap);

#if (ENABLE_DRAW_DEBUG && WITH_EDITOR)
	::DrawDebugBox(GetWorld(), InLocation, InCollisionShape.GetExtent(), InQuat, FColor::Orange);

	if (IsSnapped)
	{
		for (auto Overlap : Overlaps)
		{
			if (Overlap.GetComponent())
			{
				Overlap.GetComponent()->UpdateBounds();
				const FVector Extent = Overlap.GetComponent()->Bounds.GetBox().GetExtent();
				::DrawDebugBox(GetWorld(), Overlap.GetComponent()->GetComponentLocation(), Extent, FQuat::Identity, FColor::Red, DEFAULT_DEBUGBOXDRAW_PARAMS, 4.0f);
			}
		}
	}
#endif

	return IsSnapped;
}

bool ABuildHelperComponent::ProcessGrid(const FHitResult& InHitResult)
{
	SIZE_T _correctionCount = 0;
	FVector CorrectionDir = FVector::ZeroVector;

	const int32 AllowPlaceAxies = CurrentComponent->GetAllowPlaceAxies();
	const int32 AllowRotateAxies = CurrentComponent->GetAllowRotateAxies();
	const FVector CurrentGridStep = CurrentComponent->GetWorldGridStep();
	const FVector GridVector = CurrentComponent->GetWorldGridStep();
	const FVector OverlapExtent = CurrentComponent->GetOverlapChecker() ? CurrentComponent->GetOverlapChecker()->GetScaledBoxExtent() : FVector::ZeroVector;	

	// Pitch = Y, Yaw = Z, Roll = X
	for (SIZE_T i = 0; i <= EAxis::Z; ++i)
	{
		if (TargetRotatedAxis[i] > 3)
		{
			TargetRotatedAxis[i] = 0;
		}
	}

	FRotator GridRotation = FRotator::ZeroRotator;

	if (FFlagsHelper::HasAnyFlags(AllowRotateAxies, FFlagsHelper::ToFlag<int32>(EAxis::X))) { GridRotation.Roll = 90.0f * TargetRotatedAxis[EAxis::X]; }
	if (FFlagsHelper::HasAnyFlags(AllowRotateAxies, FFlagsHelper::ToFlag<int32>(EAxis::Y))) { GridRotation.Pitch = 90.0f * TargetRotatedAxis[EAxis::Y]; }
	if (FFlagsHelper::HasAnyFlags(AllowRotateAxies, FFlagsHelper::ToFlag<int32>(EAxis::Z))) { GridRotation.Yaw = 90.0f * TargetRotatedAxis[EAxis::Z]; }

	//SetActorRotation(GridRotation);

	//const FQuat OverlapQuat = CurrentComponent->GetOverlapChecker() ? CurrentComponent->GetOverlapChecker()->GetComponentQuat() : FQuat::Identity;
	//FCollisionShape CollisionShape = FCollisionShape::MakeBox(OverlapExtent);

_correctionLabel:
	
	const FVector CorrectionAxis = CorrectionDir * _correctionCount;	
	const FVector CorrectionVector = CorrectionAxis * (CurrentGridStep / 2.0f);
	const FVector HitLocation = InHitResult.Location + CorrectionVector;	
	const FVector GridCellLocation = FVector(FMath::GridSnap(HitLocation.X, GridVector.X), FMath::GridSnap(HitLocation.Y, GridVector.Y), FMath::GridSnap(HitLocation.Z, GridVector.Z));

	ConfirmedTransform.SetLocation(GridCellLocation);
	ConfirmedTransform.SetRotation(GridRotation.Quaternion());
	SetActorTransform(ConfirmedTransform);

	if (CurrentComponent->GetOverlapChecker() && ProcessOverlaps(CurrentComponent->GetOverlapChecker()))
	{
		++_correctionCount;

		if (CorrectionDir == FVector::ZeroVector)
		{
			float BestDist = MAX_FLT;

			const FVector TargetUpVector = FVector::UpVector * CurrentGridStep.Z / 2;
			const float UpDist = FVector::Distance(GridCellLocation + TargetUpVector, InHitResult.Location);
			if (UpDist < BestDist)
			{
				BestDist = UpDist;
				CorrectionDir = FVector::UpVector;
			}

			const float DownDist = FVector::Distance(GridCellLocation + -TargetUpVector, InHitResult.Location);
			if (DownDist < BestDist)
			{
				BestDist = DownDist;
				CorrectionDir = -FVector::UpVector;
			}

			const FVector TargetForwardVector = FVector::ForwardVector * CurrentGridStep.X / 2;
			const float ForwardDist = FVector::Distance(GridCellLocation + TargetForwardVector, InHitResult.Location);
			if (ForwardDist < BestDist)
			{
				BestDist = ForwardDist;
				CorrectionDir = FVector::ForwardVector;
			}

			const float BackwardDist = FVector::Distance(GridCellLocation + -TargetForwardVector, InHitResult.Location);
			if (BackwardDist < BestDist)
			{
				BestDist = BackwardDist;
				CorrectionDir = -FVector::ForwardVector;
			}

			const FVector TargetRightVector = FVector::RightVector * CurrentGridStep.Y / 2;
			const float RightDist = FVector::Distance(GridCellLocation + TargetRightVector, InHitResult.Location);
			if (RightDist < BestDist)
			{
				BestDist = RightDist;
				CorrectionDir = FVector::RightVector;
			}

			const float LeftDist = FVector::Distance(GridCellLocation + -TargetRightVector, InHitResult.Location);
			if (LeftDist < BestDist)
			{
				BestDist = LeftDist;
				CorrectionDir = -FVector::RightVector;
			}
		}

		if (_correctionCount < 5)
		{
			goto _correctionLabel;
		}
		else
		{
			return false;
		}
	}

	return true;
}

bool ABuildHelperComponent::ProcessParents(ABuildPlacedComponent* InPlacedComponent, const bool InAttach)
{
	ABuildPlacedComponent* TargetComponent = InPlacedComponent;
	if (InAttach == false && TargetComponent == nullptr)
	{
		TargetComponent = CurrentComponent;
	}

	if (TargetComponent && TargetComponent->GetOverlapChecker() && GetWorld())
	{
		auto TargetChecker = TargetComponent->GetOverlapChecker();
		const bool IsGridComponent = TargetComponent->IsWorldGridComponent();

		FVector QueryBoxExtent = TargetChecker->GetScaledBoxExtent();
		if (IsGridComponent == false)
		{
			QueryBoxExtent *= 2;
		}
		FQuat QueryBoxQuat = TargetChecker->GetComponentQuat();
		FVector CompLocation = TargetChecker->GetComponentLocation();

		FCollisionShape CollisionShape;
		FCollisionQueryParams QueryFindOverlap(TEXT("BuildFindParents"), true, this);
		QueryFindOverlap.bTraceAsyncScene = true;
		QueryFindOverlap.AddIgnoredActor(GetOwner());
		QueryFindOverlap.AddIgnoredActor(CurrentComponent);

		FCollisionResponseParams ResponseFindOverlap = FCollisionResponseParams::DefaultResponseParam;
		ResponseFindOverlap.CollisionResponse.SetResponse(ECC_WorldStatic, ECR_Block);
		ResponseFindOverlap.CollisionResponse.SetResponse(ECC_WorldDynamic, ECR_Block);
		ResponseFindOverlap.CollisionResponse.SetResponse(COLLISION_PLAYERBUILDING_OBJECT, ECR_Block);

		if (IsGridComponent == false)
		{
#if (ENABLE_DRAW_DEBUG && WITH_EDITOR)
			::DrawDebugBox(GetWorld(), CompLocation, QueryBoxExtent, QueryBoxQuat, FColorList::LimeGreen);
#endif

			CollisionShape = FCollisionShape::MakeBox(QueryBoxExtent);

			TArray<FOverlapResult> OverlapsTest;
			const bool IsFoundTest = GetWorld()->OverlapMultiByChannel(OverlapsTest, CompLocation, QueryBoxQuat, COLLISION_PLAYERBUILDING_OBJECT, CollisionShape, QueryFindOverlap, ResponseFindOverlap);

			//TArray<FOverlapResult> OverlapsUp, OverlapsDown, OverlapsForward, OverlapsBackward, OverlapsRight, OverlapsLeft;
			//const bool IsFoundUp = GetWorld()->OverlapMultiByChannel(OverlapsUp, CompLocation, QueryBoxQuat, COLLISION_PLAYERBUILDING_OBJECT, CollisionShape, QueryFindOverlap, ResponseFindOverlap);
			//const bool IsFoundDown = GetWorld()->OverlapMultiByChannel(OverlapsDown, CompLocation, QueryBoxQuat, COLLISION_PLAYERBUILDING_OBJECT, CollisionShape, QueryFindOverlap, ResponseFindOverlap);
			//const bool IsFoundForward = GetWorld()->OverlapMultiByChannel(OverlapsForward, CompLocation, QueryBoxQuat, COLLISION_PLAYERBUILDING_OBJECT, CollisionShape, QueryFindOverlap, ResponseFindOverlap);
			//const bool IsFoundBackward = GetWorld()->OverlapMultiByChannel(OverlapsBackward, CompLocation, QueryBoxQuat, COLLISION_PLAYERBUILDING_OBJECT, CollisionShape, QueryFindOverlap, ResponseFindOverlap);
			//const bool IsFoundRight = GetWorld()->OverlapMultiByChannel(OverlapsRight, CompLocation, QueryBoxQuat, COLLISION_PLAYERBUILDING_OBJECT, CollisionShape, QueryFindOverlap, ResponseFindOverlap);
			//const bool IsFoundLeft = GetWorld()->OverlapMultiByChannel(OverlapsLeft, CompLocation, QueryBoxQuat, COLLISION_PLAYERBUILDING_OBJECT, CollisionShape, QueryFindOverlap, ResponseFindOverlap);

			const bool IsDebugDrawing = (ENABLE_DRAW_DEBUG && WITH_EDITOR);

#define OVERLAPS_PROCESS(Direction)\
		if (IsFound##Direction)\
		{\
			for (auto Overlap : Overlaps##Direction)\
			{\
				if (IsDebugDrawing && Overlap.GetComponent())\
				{\
					Overlap.GetComponent()->UpdateBounds();\
					const FVector cExtent = Overlap.GetComponent()->Bounds.GetBox().GetExtent();\
					::DrawDebugBox(GetWorld(), Overlap.GetComponent()->GetComponentLocation(), cExtent, Overlap.GetComponent()->GetComponentQuat(), FColor::Green, DEFAULT_DEBUGBOXDRAW_PARAMS, 4.0f);\
				}\
				if (auto OtherBuilding = Cast<ABuildPlacedComponent>(Overlap.GetActor()))\
				{\
					OtherBuilding->ChildBuildComponents.Add(TargetComponent);\
					TargetComponent->ParentBuildComponents.Add(OtherBuilding);\
				}\
			}\
		}

			if (InAttach || GIsEditor)
			{
				OVERLAPS_PROCESS(Test)
					//OVERLAPS_PROCESS(Up)
					//OVERLAPS_PROCESS(Down)
					//OVERLAPS_PROCESS(Forward)
					//OVERLAPS_PROCESS(Backward)
					//OVERLAPS_PROCESS(Right)
					//OVERLAPS_PROCESS(Left)
			}

			return IsFoundTest;// (IsFoundUp || IsFoundDown || IsFoundForward || IsFoundBackward || IsFoundRight || IsFoundLeft);
		}
		else
		{
			const bool IsDebugDrawing = (ENABLE_DRAW_DEBUG && WITH_EDITOR);

			FHitResult HitResultUp, HitResultDown, HitResultForward, HitResultBackward, HitResultRight, HitResultLeft;
			const bool IsFoundUp = GetWorld()->LineTraceSingleByChannel(HitResultUp, CompLocation, CompLocation + FVector::UpVector * QueryBoxExtent.Size(), COLLISION_PLAYERBUILDING_OBJECT, QueryFindOverlap, ResponseFindOverlap);
			const bool IsFoundDown = GetWorld()->LineTraceSingleByChannel(HitResultDown, CompLocation, CompLocation - FVector::UpVector * QueryBoxExtent.Size(), COLLISION_PLAYERBUILDING_OBJECT, QueryFindOverlap, ResponseFindOverlap);
			const bool IsFoundForward = GetWorld()->LineTraceSingleByChannel(HitResultForward, CompLocation, CompLocation + FVector::ForwardVector * QueryBoxExtent.Size(), COLLISION_PLAYERBUILDING_OBJECT, QueryFindOverlap, ResponseFindOverlap);
			const bool IsFoundBackward = GetWorld()->LineTraceSingleByChannel(HitResultBackward, CompLocation, CompLocation - FVector::ForwardVector * QueryBoxExtent.Size(), COLLISION_PLAYERBUILDING_OBJECT, QueryFindOverlap, ResponseFindOverlap);
			const bool IsFoundRight = GetWorld()->LineTraceSingleByChannel(HitResultRight, CompLocation, CompLocation + FVector::RightVector * QueryBoxExtent.Size(), COLLISION_PLAYERBUILDING_OBJECT, QueryFindOverlap, ResponseFindOverlap);
			const bool IsFoundLeft = GetWorld()->LineTraceSingleByChannel(HitResultLeft, CompLocation, CompLocation - FVector::RightVector * QueryBoxExtent.Size(), COLLISION_PLAYERBUILDING_OBJECT, QueryFindOverlap, ResponseFindOverlap);

#define OVERLAPS_PROCESS_LINE(Direction)\
			if (IsDebugDrawing && HitResult##Direction.bBlockingHit)\
			{\
				::DrawDebugLine(GetWorld(), CompLocation, HitResult##Direction.Location, FColorList::LimeGreen, DEFAULT_DEBUGBOXDRAW_PARAMS, 2.0f);\
				::DrawDebugPoint(GetWorld(), CompLocation, 10.0f, FColorList::Green);\
			}

			OVERLAPS_PROCESS_LINE(Up)
			OVERLAPS_PROCESS_LINE(Down)
			OVERLAPS_PROCESS_LINE(Forward)
			OVERLAPS_PROCESS_LINE(Backward)
			OVERLAPS_PROCESS_LINE(Right)
			OVERLAPS_PROCESS_LINE(Left)

			return (IsFoundUp || IsFoundDown || IsFoundForward || IsFoundBackward || IsFoundRight || IsFoundLeft);
		}
	}

	return false;
}

UPlayerInventoryItem* ABuildHelperComponent::GetBuildItemFromSlot(const int32& InSlot)
{
	UPlayerInventoryItem* ReturnValue = nullptr;

	auto MyGameState = GetWorldGameState<AConstructionGameState>(GetWorld());
	if (MyGameState && Inventory.IsValidIndex(InSlot) && Inventory[InSlot] != INDEX_NONE)
	{
		ReturnValue = MyGameState->GetInventoryComponent()->FindItem(Inventory[InSlot], ECategoryTypeId::Building);
	}

	return ReturnValue;
}

void ABuildHelperComponent::SetTargetAxis(const EAxis::Type& InAxis)
{
	TargetRotationAxis = InAxis;
}

void ABuildHelperComponent::ProccesRotateAxis()
{
	++TargetRotatedAxis[TargetRotationAxis];
}

void ABuildHelperComponent::SetAllowAnyMode(const bool InAllow)
{
	bIsAllowAnyMode = InAllow;

	if (GetCurrentMode() != EBuildHelperMode::None)
	{
		SetCurrentMode(EBuildHelperMode::None);
	}
}

void ABuildHelperComponent::StartLoadSavedInventory()
{
	if (GetNetMode() != NM_DedicatedServer)
	{
		LoadSavedInventory(SavedInventory);
	}
}

bool ABuildHelperComponent::LoadSavedInventory_Validate(const TArray<int32>& InData) { return true; }
void ABuildHelperComponent::LoadSavedInventory_Implementation(const TArray<int32>& InData)
{
	for (SIZE_T i = 1; i < 10; ++i)
	{
		if (InData.IsValidIndex(i))
		{
			Inventory[i] = InData[i];

			if (const auto TargetItem = GetBuildItemFromSlot(i))
			{
				SetSlotFromItem(i, TargetItem);
			}
		}
	}

}

void ABuildHelperComponent::OnRep_Inventory()
{
	if (HasNetOwner())
	{
		auto last = SavedInventory;
		SavedInventory = Inventory;
		if (last != SavedInventory)
		{
			SaveConfig();
		}
	}
}

void ABuildHelperComponent::ClientNotAllowAction_Implementation(const int32 InAction)
{
	QueueMessageBegin("NotAllowBuildAction")
		SMessageBox::Get()->SetHeaderText(NSLOCTEXT("ABuildHelperComponent", "ABuildHelperComponent.NotAllow.Title", "Action Denied"));
		SMessageBox::Get()->SetContent(FText::Format(NSLOCTEXT("ABuildHelperComponent", "ABuildHelperComponent.NotAllow.Desc", "Action denied \"{0}\" for you, talk with owner to resolve this."),
		FRichHelpers::TextHelper_NotifyValue.SetValue((InAction == EAllowConstructionMode::Building) ? NSLOCTEXT("ABuildHelperComponent", "ABuildHelperComponent.Mode.Building", "Building") : NSLOCTEXT("ABuildHelperComponent", "ABuildHelperComponent.Mode.Selecting", "Selecting")).ToText()));
		SMessageBox::Get()->SetButtonsText();
		SMessageBox::Get()->SetEnableClose(true);
		SMessageBox::Get()->ToggleWidget(true);
	QueueMessageEnd
}
