// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "BuildInstancedComponent.h"
#include "BuildSystemContainer.h"


// Sets default values
ABuildSystemContainer::ABuildSystemContainer()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABuildSystemContainer::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABuildSystemContainer::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

