// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Serialization/JsonSerializerMacros.h"

template<typename T>
struct FDynamicPropertyValue : public FJsonSerializable
{
	FDynamicPropertyValue() : Value(), Default() {}
	FDynamicPropertyValue(const T InValue) : Value(InValue), Default() {}
	FDynamicPropertyValue(const T InValue, const T InDefault) : Value(InValue), Default(InDefault) {}

	virtual ~FDynamicPropertyValue() {}

	T Value;
	T Default;

	BEGIN_JSON_SERIALIZER
		JSON_SERIALIZE("Value", Value);
		JSON_SERIALIZE("Default", Default);
	END_JSON_SERIALIZER
};
