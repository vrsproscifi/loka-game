// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "ProfilePlacedComponent.h"
#include "ConstructionGameState.h"
#include "ConstructionPlayerState.h"
#include "InventoryComponent.h"
#include "ProfileComponent.h"
#include "PlayerInventoryItem.h"
#include "Character/CharacterBaseEntity.h"
#include "UTGameViewportClient.h"
#include "SRadialMenu.h"
#include "BaseCharacter.h"
#include "WorldPreviewItem.h"
#include "Engine/InputDelegateBinding.h"
#include "Inventory/SWindowInventory.h"
#include "Windows/SWindowsContainer.h"
#include "Inventory/SInventoryContainer.h"
#include "BuildDynamicProperty.h"

FString AProfilePlacedComponent::ParameterName_Profile = TEXT("Profile");

AProfilePlacedComponent::AProfilePlacedComponent()
	: Super()
{
	CharacterMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("ProfileCharacterMesh"));
	CharacterMesh->SetupAttachment(RootComponent);
	CharacterMesh->SetReceivesDecals(false);

	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("ProfileSpringArm"));
	SpringArmComponent->SetupAttachment(RootComponent);

	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("ProfileCamera"));
	CameraComponent->SetupAttachment(SpringArmComponent);

	LastProfile = EPlayerPreSetId::End;
	CurrentProfile = EPlayerPreSetId::One;

	bReplicates = true;
}

void AProfilePlacedComponent::OnFocusLost_Implementation(AActor* InInstigator)
{
	OnInteractLost_Implementation(InInstigator);
}

void AProfilePlacedComponent::OnInteractStart_Implementation(AActor* InInstigator)
{
	if (GetNetMode() == NM_Client || GetNetMode() == NM_Standalone)
	{
		auto MyCharacter = Cast<ABaseCharacter>(InInstigator);
		if ((Slate_RadialMenu.IsValid() || TryCreateMenu()) && IsInSettingsMode() == false)
		{
			LastUser = MyCharacter ? Cast<ABasePlayerState>(MyCharacter->GetPlayerState()) : nullptr;
			Slate_RadialMenu->ToggleWidget(true);
			GetWorldTimerManager().ClearTimer(timer_DestroyMenu);
		}
	}
}

void AProfilePlacedComponent::OnInteractLost_Implementation(AActor* InInstigator)
{
	if (GetNetMode() == NM_Client || GetNetMode() == NM_Standalone)
	{
		if (Slate_RadialMenu.IsValid())
		{
			Slate_RadialMenu->ToggleWidget(false);
			GetWorldTimerManager().SetTimer(timer_DestroyMenu, FTimerDelegate::CreateUObject(this, &AProfilePlacedComponent::OnDestroyMenu), 10.0f, false);
		}
	}
}

void AProfilePlacedComponent::InitializeProfile(const TEnumAsByte<EPlayerPreSetId::Type> InProfile)
{
	CurrentProfile = InProfile;
	LastProfile = CurrentProfile;

	const auto MyInventoryComponent = GetOwnerInventoryComponent();
	const auto MyProfileComponent = GetOwnerProfileComponent();

	if (MyInventoryComponent && MyProfileComponent)
	{
		FActorSpawnParameters ActorSpawnParameters;
		ActorSpawnParameters.Owner = this;
		ActorSpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		if (PrimaryWeapon == nullptr)
		{
			PrimaryWeapon = GetWorld()->SpawnActor<AWorldPreviewItem>(AWorldPreviewItem::StaticClass(), ActorSpawnParameters);
		}

		if (SecondaryWeapon == nullptr)
		{
			SecondaryWeapon = GetWorld()->SpawnActor<AWorldPreviewItem>(AWorldPreviewItem::StaticClass(), ActorSpawnParameters);
		}
		
		if (MyProfileComponent->GetProfileDataPtr(CurrentProfile) && (LastData == MyProfileComponent->GetProfileData(CurrentProfile)) == false)
		{
			LastData = MyProfileComponent->GetProfileData(CurrentProfile);

			auto FoundItem = MyInventoryComponent->FindItem(MyProfileComponent->GetProfileDataPtr(CurrentProfile)->CharacterId);
			UCharacterBaseEntity* FoundChar = FoundItem ? FoundItem->GetEntity<UCharacterBaseEntity>() : nullptr;

			if (FoundChar && FoundChar->IsValidLowLevel())
			{
				for (SIZE_T i = 0; i < CharacterMesh->GetNumMaterials(); ++i)
				{
					CharacterMesh->SetMaterial(i, nullptr);
				}

				CharacterMesh->SetSkeletalMesh(FoundChar->GetProfileMeshData().Mesh3P);
				CharacterMesh->SetAnimInstanceClass(FoundChar->GetProfileMeshData().MeshAnim3P);

				for (auto FoundInventoryItem : MyProfileComponent->GetProfileDataPtr(CurrentProfile)->Items)
				{
					if (FoundInventoryItem.SlotId == EItemSetSlotId::SecondaryWeapon)
					{
						PrimaryWeapon->InitializeInstance(MyInventoryComponent->FindItem(FoundInventoryItem.ItemId));
						PrimaryWeapon->AttachToComponent(CharacterMesh, FAttachmentTransformRules::SnapToTargetIncludingScale, TEXT("LeftHandSocket"));
					}
					else if (FoundInventoryItem.SlotId == EItemSetSlotId::PrimaryWeapon)
					{
						SecondaryWeapon->InitializeInstance(MyInventoryComponent->FindItem(FoundInventoryItem.ItemId));
						SecondaryWeapon->AttachToComponent(CharacterMesh, FAttachmentTransformRules::SnapToTargetIncludingScale, TEXT("RightHandSocket"));
					}
				}				
			}
		}

		if (Slate_InventoryContainer.IsValid())
		{
			Slate_InventoryContainer->Refresh();
		}
	}
}

void AProfilePlacedComponent::SaveProfile()
{
	if (const auto ProfileComp = GetOwnerProfileComponent())
	{
		UpdateDynamicPropertyData(ParameterName_Profile, ProfileComp->GetProfileData(CurrentProfile).AssetId.ToString());
	}
}

void AProfilePlacedComponent::OnAddPitch(float InValue)
{
	if (FMath::IsNearlyEqual(InValue, .0f, .1f) == false)
	{
		SpringArmComponent->AddRelativeRotation(FRotator(-InValue, 0, 0));
		SpringArmComponent->RelativeRotation.Pitch = FMath::Clamp(SpringArmComponent->RelativeRotation.Pitch, -45.0f, 45.0f);
	}
}

void AProfilePlacedComponent::OnAddYaw(float InValue)
{
	if (FMath::IsNearlyEqual(InValue, .0f, .1f) == false)
	{
		SpringArmComponent->AddRelativeRotation(FRotator(0, InValue, 0));
	}
}

void AProfilePlacedComponent::OnScrollArmLen(const bool InIsDown)
{
	if (InIsDown)
	{
		SpringArmComponent->TargetArmLength += 5.0f;
	}
	else
	{
		SpringArmComponent->TargetArmLength -= 5.0f;
	}
}

void AProfilePlacedComponent::Destroyed()
{
	Super::Destroyed();

	OnDestroyMenu();
	OnDestroyInventory();

	if (PrimaryWeapon && PrimaryWeapon->IsValidLowLevel()) PrimaryWeapon->Destroy();
	if (SecondaryWeapon && SecondaryWeapon->IsValidLowLevel()) SecondaryWeapon->Destroy();
}

UProfileComponent* AProfilePlacedComponent::GetOwnerProfileComponent() const
{
	const auto MyGameState = Cast<AConstructionGameState>(GetOwner());
	if (MyGameState && MyGameState->GetOwnerState())
	{
		return MyGameState->GetOwnerState()->GetProfileComponent();
	}
	else if (auto MyOwner = Cast<AConstructionPlayerState>(GetOwner()))
	{
		return MyOwner->GetProfileComponent();
	}

	return nullptr;
}

UInventoryComponent* AProfilePlacedComponent::GetOwnerInventoryComponent() const
{
	const auto MyGameState = Cast<AConstructionGameState>(GetOwner());
	if (MyGameState && MyGameState->GetOwnerState())
	{
		return MyGameState->GetOwnerState()->GetInventoryComponent();
	}
	else if (auto MyOwner = Cast<AConstructionPlayerState>(GetOwner()))
	{
		return MyOwner->GetInventoryComponent();
	}

	return nullptr;
}

void AProfilePlacedComponent::OnDynamicPropertyDataUpdate_Implementation()
{
	FGuid TargetId = GetDynamicPropertyData(ParameterName_Profile).GetValue<FGuid>();
	bool bSuccess = false;

	if (const auto ProfileComp = GetOwnerProfileComponent())
	{
		SIZE_T _count = 0;
		auto MyProfiles = ProfileComp->GetProfilesData();
		for (auto TargetProfile : MyProfiles)
		{
			if (TargetProfile.AssetId == TargetId)
			{
				InitializeProfile(static_cast<EPlayerPreSetId::Type>(_count));
				bSuccess = true;
				break;
			}

			++_count;
		}
	}

	if (bSuccess == false)
	{
		GetWorldTimerManager().SetTimer(timer_ReUpdate, this, &AProfilePlacedComponent::OnDynamicPropertyDataUpdate_Implementation, FMath::FRandRange(1.0f, 2.0f), false);
	}
}

bool AProfilePlacedComponent::TryCreateMenu()
{
	auto MyViewport = GetGameViewportClient();
	if (Slate_RadialMenu.IsValid() == false && MyViewport)
	{
		SAssignNew(Slate_RadialMenu, SRadialMenu)
		.Radius(250)
		+ SRadialMenu::Slot().OnActivated_Lambda([&]()
		{
			if (LastUser && LastUser->IsValidLowLevel())
			{
				LastUser->ChangeProfile(CurrentProfile);
				LastUser->InitializeSelectedProfile(true);
			}
		}).ActivateMethod(static_cast<ESegmentActiveMethod::Type>(ESegmentActiveMethod::Click | ESegmentActiveMethod::Close))
		[
			SNew(STextBlock).TextStyle(&FLokaStyle::Get().GetWidgetStyle<FTextBlockStyle>("Default_TextBlock")).AutoWrapText(true)
			.Text(NSLOCTEXT("AProfilePlacedComponent", "AProfilePlacedComponent.Menu.Activate", "Activate"))
		]		
		+ SRadialMenu::Slot().OnActivated_Lambda([&]()
		{
			auto TargetCurrentProfile = static_cast<EPlayerPreSetId::Type>(CurrentProfile + 1);
			if (TargetCurrentProfile == EPlayerPreSetId::End)
			{
				TargetCurrentProfile = EPlayerPreSetId::One;
			}
			ServerChangeProfile(TargetCurrentProfile);
		})
		[
			SNew(STextBlock).TextStyle(&FLokaStyle::Get().GetWidgetStyle<FTextBlockStyle>("Default_TextBlock")).AutoWrapText(true)
			.Text(NSLOCTEXT("AProfilePlacedComponent", "AProfilePlacedComponent.Menu.Next", "Switch to\nNext Profile"))
		]		
		+ SRadialMenu::Slot().OnActivated_Lambda([&]()
		{
			OnSettingsEntry();
		}).ActivateMethod(static_cast<ESegmentActiveMethod::Type>(ESegmentActiveMethod::Click | ESegmentActiveMethod::Close))
		[
			SNew(STextBlock).TextStyle(&FLokaStyle::Get().GetWidgetStyle<FTextBlockStyle>("Default_TextBlock")).AutoWrapText(true)
			.Text(NSLOCTEXT("AProfilePlacedComponent", "AProfilePlacedComponent.Menu.Settings", "Settings"))
		]
		+ SRadialMenu::Slot().OnActivated_Lambda([&]()
		{
			auto TargetCurrentProfile = CurrentProfile - 1;
			if (TargetCurrentProfile < EPlayerPreSetId::One)
			{
				TargetCurrentProfile = EPlayerPreSetId::Five;
			}
			ServerChangeProfile(TargetCurrentProfile);
		})
		[
			SNew(STextBlock).TextStyle(&FLokaStyle::Get().GetWidgetStyle<FTextBlockStyle>("Default_TextBlock")).AutoWrapText(true)
			.Text(NSLOCTEXT("AProfilePlacedComponent", "AProfilePlacedComponent.Menu.Prev", "Switch to\nPrevious Profile"))
		]
		;

		MyViewport->AddUsableViewportWidgetContent_AlwaysVisible(Slate_RadialMenu.ToSharedRef());

		return true;
	}

	return false;
}

bool AProfilePlacedComponent::TryCreateInventory()
{
	auto MyViewport = GetGameViewportClient();
	if (MyViewport && Slate_InventoryContainer.IsValid() == false)
	{
		SAssignNew(Slate_InventoryContainer, SInventoryContainer)
		.InventoryComponent(GetOwnerInventoryComponent())
		.ProfileComponent(GetOwnerProfileComponent())
		.ProfileIndex_Lambda([&]() { return CurrentProfile; })
		.OnDropItemIntoSlot_Lambda([&](UPlayerInventoryItem* InItem, const int32& InSlot)
		{
			auto ProfileComp = GetOwnerProfileComponent();
			if (ProfileComp && InItem)
			{
				ProfileComp->SendRequestItemEquip(FPreSetEquipRequest(InItem->GetEntityId(), InItem->GetEntityModel().CategoryTypeId, InSlot, ProfileComp->GetProfileData(CurrentProfile).AssetId, true));
			}
		});

		if (auto MyInventory = GetOwnerInventoryComponent())
		{
			Slate_InventoryContainer->OnFillList(MyInventory->FindList(ECategoryTypeId::Weapon));
		}

		MyViewport->AddUsableViewportWidgetContent_AlwaysVisible(Slate_InventoryContainer.ToSharedRef());

		return true;
	}

	return false;
}

void AProfilePlacedComponent::OnDestroyMenu()
{
	auto MyViewport = GetGameViewportClient();
	if (Slate_RadialMenu.IsValid() && MyViewport)
	{
		Slate_RadialMenu->ToggleWidget(false);
		timer_DestroyMenu.Invalidate();
		MyViewport->RemoveViewportWidgetContent(Slate_RadialMenu.ToSharedRef());
		Slate_RadialMenu.Reset();
	}
}

void AProfilePlacedComponent::OnDestroyInventory()
{
	auto MyViewport = GetGameViewportClient();
	if (Slate_InventoryContainer.IsValid() && MyViewport)
	{
		Slate_InventoryContainer->ToggleWidget(false);
		MyViewport->RemoveViewportWidgetContent(Slate_InventoryContainer.ToSharedRef());
		Slate_InventoryContainer.Reset();
	}
}

bool operator!=(const FClientPreSetData& InLhs, const FClientPreSetData& InRhs)
{
	const bool IsEqualData = InLhs.Items == InRhs.Items;

	return InLhs.AssetId != InRhs.AssetId ||
	InLhs.CharacterId != InRhs.CharacterId ||
	InLhs.IsEnabled != InRhs.IsEnabled ||
	InLhs.Name != InRhs.Name ||
	IsEqualData == false;
}

void AProfilePlacedComponent::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (CurrentProfile != LastProfile)
	{
		InitializeProfile(CurrentProfile);
	}
	else if (GetOwnerProfileComponent() && LastData != GetOwnerProfileComponent()->GetProfileData(CurrentProfile))
	{
		++NeedRefreshCounter;
		OnRep_CurrentProfile();
	}
}

bool AProfilePlacedComponent::ServerChangeProfile_Validate(const int32 InProfile) { return true; }
void AProfilePlacedComponent::ServerChangeProfile_Implementation(const int32 InProfile)
{
	InitializeProfile(static_cast<EPlayerPreSetId::Type>(InProfile));
	SaveProfile();
}

void AProfilePlacedComponent::OnSettingsEntry_Implementation()
{
	bInSettingsMode = true;

	if (InputComponent == nullptr)
	{
		static const FName InputComponentName(TEXT("ProfileInputComponent0"));
		InputComponent = NewObject<UInputComponent>(this, InputComponentName);
	}

	if (InputComponent)
	{
		InputComponent->BindAxis("Turn", this, &AProfilePlacedComponent::OnAddYaw);
		InputComponent->BindAxis("LookUp", this, &AProfilePlacedComponent::OnAddPitch);

		InputComponent->BindAction("WeaponSwitchUp", IE_Pressed, this, &AProfilePlacedComponent::OnScrollArmLen_Helper<false>);
		InputComponent->BindAction("WeaponSwitchDown", IE_Pressed, this, &AProfilePlacedComponent::OnScrollArmLen_Helper<true>);

		InputComponent->BindAction("InGameESC", IE_Pressed, this, &AProfilePlacedComponent::OnSettingsLeave);

		InputComponent->RegisterComponent();
		if (UInputDelegateBinding::SupportsInputDelegate(GetClass()))
		{
			InputComponent->bBlockInput = bBlockInput;
			UInputDelegateBinding::BindInputDelegates(GetClass(), InputComponent);
		}
	}

	APlayerController* MyLocalController = GetWorld() ? GetWorld()->GetFirstPlayerController() : nullptr;
	if (MyLocalController)
	{
		EnableInput(MyLocalController);
		MyLocalController->SetViewTargetWithBlend(this, 1.0f, VTBlend_EaseIn, 1, true);
	}

	if (TryCreateInventory() || Slate_InventoryContainer.IsValid())
	{
		Slate_InventoryContainer->ToggleWidget(true);
	}
}

void AProfilePlacedComponent::OnSettingsLeave_Implementation()
{
	bInSettingsMode = false;

	if (InputComponent)
	{
		InputComponent->DestroyComponent();
		InputComponent = nullptr;
	}

	APlayerController* MyLocalController = GetWorld() ? GetWorld()->GetFirstPlayerController() : nullptr;
	if (MyLocalController)
	{
		DisableInput(MyLocalController);
		MyLocalController->SetViewTargetWithBlend(MyLocalController->GetPawn(), 1.0f, VTBlend_EaseIn, 1, true);
	}

	if (Slate_InventoryContainer.IsValid())
	{
		Slate_InventoryContainer->ToggleWidget(false);
	}
}

void AProfilePlacedComponent::OnRep_CurrentProfile()
{
	InitializeProfile(CurrentProfile);
}

void AProfilePlacedComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AProfilePlacedComponent, CurrentProfile);
	DOREPLIFETIME(AProfilePlacedComponent, NeedRefreshCounter);
}