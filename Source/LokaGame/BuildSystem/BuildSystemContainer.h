// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "BuildSystemContainer.generated.h"

class UBuildInstancedComponent;

UCLASS()
class LOKAGAME_API ABuildSystemContainer : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABuildSystemContainer();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

protected:
	
	// Used only if loaded as non editable
	UPROPERTY(BlueprintReadOnly, Category = System)
	TArray<UBuildInstancedComponent*> BuildInstancedComponents;
};
