#include "LokaGame.h"
#include "SlateStyleManager.h"
#include "SlateMaterialBrush.h"
#include "SlateGameResources.h"
#include <TokenizedMessage.h>

TSharedPtr< FSlateStyleGameSet > FSlateStyleManager::StyleInstance = nullptr;

TSharedRef<FSlateStyleGameSet> FSlateStyleGameSet::New(const FName& InStyleSetName, const FString& ScopeToDirectory, const FString& InBasePath)
{
	const TSharedRef<FSlateStyleGameSet> NewStyle = MakeShareable(new FSlateStyleGameSet(InStyleSetName));
	NewStyle->Initialize(ScopeToDirectory, InBasePath);
	return NewStyle;
}

void FSlateStyleManager::Initialize()
{
	if (StyleInstance.IsValid() == false || StyleInstance.Get() == nullptr)
	{
		StyleInstance = Create();
		FSlateStyleRegistry::RegisterSlateStyle(*StyleInstance.Get());
	}
}

void FSlateStyleManager::Shutdown()
{
	FSlateStyleRegistry::UnRegisterSlateStyle(*StyleInstance.Get());
	ensure(StyleInstance.IsUnique());
	StyleInstance.Reset();
}

FName FSlateStyleManager::GetStyleSetName()
{
	static FName StyleSetName(TEXT("NewLokaStyle"));
	return StyleSetName;
}

TSharedRef< FSlateStyleGameSet > FSlateStyleManager::Create()
{
	return FSlateStyleGameSet::New(GetStyleSetName(), "/Game/1LOKAgame/UserInterface/NewStyles", "/Game/1LOKAgame/UserInterface/NewStyles");
}

void FSlateStyleManager::ReloadTextures()
{
	FSlateApplication::Get().GetRenderer()->ReloadTextureResources();
}

const FSlateStyleGameSet& FSlateStyleManager::Get()
{
	Initialize();
	return *StyleInstance;
}
