#pragma once

#include "Margin.h"
#include "SlateBorderStyle.generated.h"

USTRUCT()
struct FSlateBorderStyle
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly)	FSlateBrush Brush;

	UPROPERTY(EditDefaultsOnly)	FMargin Padding;

	FSlateBorderStyle(){}
	FSlateBorderStyle(const FMargin& padding)
		: Padding(padding)
	{
		
	}
};