#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"
#include "SlateStyleComponent.generated.h"

USTRUCT()
struct FSlateStyleComponent
	: public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

public:
	static FName TypeName;
	FSlateStyleComponent()
	{
		
	}

	FSlateStyleComponent(const FName& name)
	{
		TypeName = name;
	}

	template< typename TBase>
	static const TBase& GetDefault()
	{
		static TBase base;
		return base;
	}

	// ReSharper disable CppConstValueFunctionReturnType
	virtual const FName GetTypeName() const override
	{
		return TypeName;
	}
	// ReSharper restore CppConstValueFunctionReturnType
};


//	����������� ��� �������������� ������� ���� �����
//	��� �������� ���������� ����� ����� ������� ��������� ;
#define SlateStyleContructor(TStyleName) TStyleName() : FSlateStyleComponent(#TStyleName) { };
