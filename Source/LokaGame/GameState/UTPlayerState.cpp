// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "LokaGame.h"
#include "UTGameMode.h"
#include "UTPlayerState.h"
#include "Net/UnrealNetwork.h"
#include "UTGameMessage.h"

#include "UTCharacterContent.h"
#include "StatNames.h"

#include "UTEngineMessage.h"
#include "UTGameState.h"
#include "UTDemoRecSpectator.h"

#include "UTHUDWidget_NetInfo.h"

#include "UTGameUserSettings.h"
#include "UTHUD.h"
#include "UTTeamInfo.h"

#include "UTLocalPlayer.h"
#include "Engine/NetworkObjectList.h"
#include "ProfileComponent.h"
#include "InventoryComponent.h"
#include "IdentityComponent.h"
#include "InventoryHacks.h"
#include "GameSingleton.h"
#include "Item/ItemBaseEntity.h"

/** disables load warnings for dedicated server where invalid client input can cause unpreventable logspam, but enables on clients so developers can make sure their stuff is working */
static inline ELoadFlags GetCosmeticLoadFlags()
{
	return IsRunningDedicatedServer() ? LOAD_NoWarn : LOAD_None;
}

AUTPlayerState::AUTPlayerState()
	: Super()
	, IsABot(false)
{
	bReadyToPlay = false;
	bIsWarmingUp = false;
	bCaster = false;
	LastKillTime = 0.0f;
	Kills = 0;
	DamageDone = 0;
	RoundDamageDone = 0;
	RoundKills = 0;
	Deaths = 0;
	bShouldAutoTaunt = false;
	bSentLogoutAnalytics = false;
	SelectionOrder = 255;

	// We want to be ticked.
	PrimaryActorTick.bCanEverTick = true;

	StatManager = nullptr;
	bIsDemoRecording = false;
	EngineMessageClass = UUTEngineMessage::StaticClass();
	LastTauntTime = -1000.f;
	TotalChallengeStars = 0;
	bAnnounceWeaponSpree = false;
	bAnnounceWeaponReward = false;
	ReadyMode = 0;
	RespawnWaitTime = 0.f;

	CoolFactorCombinationWindow = 5.0f;
	CoolFactorBleedSpeed = 20.0f;
	MinimumConsiderationForCoolFactorHistory = 200.0f;
	SpawnAttempts = 0;

	Experience = 0;
	PremiumExperience = 0;
	Reputation = 0;
	PremiumReputation = 0;
}

void AUTPlayerState::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	//======================================================================
	//								LOKA 

	DOREPLIFETIME(AUTPlayerState, Cash);
	DOREPLIFETIME(AUTPlayerState, PremiumCash);
	DOREPLIFETIME(AUTPlayerState, Experience);
	DOREPLIFETIME(AUTPlayerState, Reputation);
	DOREPLIFETIME(AUTPlayerState, FractionId);
	DOREPLIFETIME(AUTPlayerState, Achievements);
	DOREPLIFETIME(AUTPlayerState, PlayerCoefficient);
	DOREPLIFETIME(AUTPlayerState, UsingPremiumService);

	DOREPLIFETIME(AUTPlayerState, PremiumExperience);
	DOREPLIFETIME(AUTPlayerState, PremiumReputation);
	
	//======================================================================


	DOREPLIFETIME(AUTPlayerState, CarriedObject);
	DOREPLIFETIME(AUTPlayerState, bReadyToPlay);
	DOREPLIFETIME(AUTPlayerState, bIsWarmingUp);
	DOREPLIFETIME(AUTPlayerState, RespawnWaitTime);
	DOREPLIFETIME(AUTPlayerState, RemainingLives);
	DOREPLIFETIME(AUTPlayerState, Kills);
	DOREPLIFETIME(AUTPlayerState, RoundKills);
	DOREPLIFETIME(AUTPlayerState, Deaths);
	DOREPLIFETIME(AUTPlayerState, Team);
	DOREPLIFETIME(AUTPlayerState, FlagCaptures);
	DOREPLIFETIME(AUTPlayerState, FlagReturns);
	DOREPLIFETIME(AUTPlayerState, Assists);
	DOREPLIFETIME(AUTPlayerState, LastKillerPlayerState);
	DOREPLIFETIME(AUTPlayerState, ChatDestination);
	DOREPLIFETIME(AUTPlayerState, RemainingBoosts);
	DOREPLIFETIME(AUTPlayerState, BoostClass);
	DOREPLIFETIME(AUTPlayerState, BoostRechargeTimeRemaining);
	DOREPLIFETIME(AUTPlayerState, TotalChallengeStars);
	DOREPLIFETIME(AUTPlayerState, SelectedCharacter);
	DOREPLIFETIME(AUTPlayerState, bSpecialPlayer);
	DOREPLIFETIME(AUTPlayerState, bSpecialTeamPlayer);
	DOREPLIFETIME(AUTPlayerState, KickCount);
	DOREPLIFETIME(AUTPlayerState, bIsRconAdmin);
	DOREPLIFETIME(AUTPlayerState, SelectionOrder);

	DOREPLIFETIME_CONDITION(AUTPlayerState, WeaponSpreeDamage, COND_OwnerOnly);

	DOREPLIFETIME(AUTPlayerState, SpectatingID);
	DOREPLIFETIME(AUTPlayerState, SpectatingIDTeam);
	DOREPLIFETIME(AUTPlayerState, bCaster);
	DOREPLIFETIME(AUTPlayerState, bHasLifeLimit);
	DOREPLIFETIME_CONDITION(AUTPlayerState, bIsDemoRecording, COND_InitialOnly);
	DOREPLIFETIME(AUTPlayerState, MatchHighlights);
	DOREPLIFETIME(AUTPlayerState, MatchHighlightData);
	DOREPLIFETIME(AUTPlayerState, ReadyMode);

	DOREPLIFETIME_CONDITION(AUTPlayerState, CriticalObject, COND_OwnerOnly);

	// Allow "displayall UTPlayerState CurrentCoolFactor" in development builds
#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
	DOREPLIFETIME_CONDITION(AUTPlayerState, CurrentCoolFactor, COND_OwnerOnly);
#endif
}

void AUTPlayerState::Destroyed()
{
	Super::Destroyed();
	GetWorldTimerManager().ClearAllTimersForObject(this);
}

void AUTPlayerState::IncrementDamageDone(int32 AddedDamage)
{
	DamageDone += AddedDamage;
	RoundDamageDone += AddedDamage;
}

bool AUTPlayerState::IsFemale()
{
	return GetSelectedCharacter() && GetSelectedCharacter().GetDefaultObject()->bIsFemale;
}

void AUTPlayerState::SetPlayerName(const FString& S)
{
	Super::SetPlayerName(S);
	bHasValidClampedName = false;
}

void AUTPlayerState::UpdatePing(float InPing)
{
}

void AUTPlayerState::CalculatePing(float NewPing)
{
	if (NewPing < 0.f)
	{
		// caused by timestamp wrap around
		return;
	}

	float OldPing = ExactPing;
	Super::UpdatePing(NewPing);

	AUTPlayerController* PC = Cast<AUTPlayerController>(GetOwner());
	if (PC)
	{
		PC->LastPingCalcTime = GetWorld()->GetTimeSeconds();
		if (ExactPing != OldPing)
		{
			PC->ServerUpdatePing(ExactPing);
		}
		if (PC->bShowNetInfo && PC->NetInfoWidget)
		{
			PC->NetInfoWidget->AddPing(NewPing);
		}
	}
}

bool AUTPlayerState::ShouldAutoTaunt() const
{
	if (!bIsABot)
	{
		return bShouldAutoTaunt;
	}
	UUTGameUserSettings* GS = Cast<UUTGameUserSettings>(GEngine->GetGameUserSettings());
	return GS != nullptr && GS->GetBotSpeech() == BSO_All;
}

void AUTPlayerState::AnnounceKill()
{
	if (CharacterVoice && ShouldAutoTaunt() && GetWorld()->GetAuthGameMode())
	{
		int32 NumTaunts = CharacterVoice.GetDefaultObject()->TauntMessages.Num();
		if (NumTaunts > 0)
		{
			AUTGameState* GS = GetWorld()->GetGameState<AUTGameState>();
			int32 TauntSelectionIndex = GS ? GS->TauntSelectionIndex % NumTaunts : 0;
			int32 SelectedTaunt = TauntSelectionIndex + FMath::Min(FMath::RandRange(0, 2), NumTaunts - TauntSelectionIndex - 1);
			if (GS)
			{
				GS->TauntSelectionIndex += 3;
			}
			GetWorld()->GetAuthGameMode<AGameMode>()->BroadcastLocalized(GetOwner(), CharacterVoice, SelectedTaunt, this);
		}
	}
}

void AUTPlayerState::AnnounceSameTeam(AUTPlayerController* ShooterPC)
{
	if (CharacterVoice && ShouldAutoTaunt() && GetWorld()->GetAuthGameMode() && (GetWorld()->GetTimeSeconds() - ShooterPC->LastSameTeamTime > 5.f) 
		&& (CharacterVoice.GetDefaultObject()->SameTeamMessages.Num() > 0))
	{
		ShooterPC->LastSameTeamTime = GetWorld()->GetTimeSeconds();
		ShooterPC->ClientReceiveLocalizedMessage(CharacterVoice, CharacterVoice.GetDefaultObject()->SameTeamBaseIndex + FMath::RandRange(0, CharacterVoice.GetDefaultObject()->SameTeamMessages.Num() - 1), this, ShooterPC->GetPlayerState(), NULL);
	}
}

void AUTPlayerState::AnnounceReactionTo(const AUTPlayerState* ReactionPS) const
{
	if (CharacterVoice && ShouldAutoTaunt() && GetWorld()->GetAuthGameMode())
	{
		AUTPlayerController* PC = Cast<AUTPlayerController>(ReactionPS->GetOwner());
		if (!PC)
		{
			return;
		}
		int32 NumFriendlyReactions = CharacterVoice.GetDefaultObject()->FriendlyReactions.Num();
		int32 NumEnemyReactions = CharacterVoice.GetDefaultObject()->EnemyReactions.Num();
		AUTGameState* GS = GetWorld()->GetGameState<AUTGameState>();
		bool bSameTeam = GS ? GS->OnSameTeam(this, ReactionPS) : false;
		if (GS == nullptr || bSameTeam || (NumEnemyReactions == 0) || (FMath::FRand() < 0.6f))
		{
			// only friendly reaction if on same team
			if (NumFriendlyReactions > 0)
			{
				PC->ClientReceiveLocalizedMessage(CharacterVoice, CharacterVoice.GetDefaultObject()->FriendlyReactionBaseIndex + FMath::RandRange(0, CharacterVoice.GetDefaultObject()->FriendlyReactions.Num() - 1), const_cast<AUTPlayerState*>(this), PC->GetPlayerState(), NULL);
				return;
			}
			else if (bSameTeam)
			{
				return;
			}
		}
		if (NumEnemyReactions > 0)
		{
			PC->ClientReceiveLocalizedMessage(CharacterVoice, CharacterVoice.GetDefaultObject()->EnemyReactionBaseIndex + FMath::RandRange(0, CharacterVoice.GetDefaultObject()->EnemyReactions.Num() - 1), const_cast<AUTPlayerState*>(this), PC->GetPlayerState(), NULL);
		}
	}
}

void AUTPlayerState::AnnounceStatus(FName NewStatus, int32 SwitchOffset, bool bSkipSelf)
{
	GetCharacterVoiceClass();
	if (CharacterVoice != nullptr)
	{
		// send to same team only
		AUTGameState* GS = GetWorld()->GetGameState<AUTGameState>();
		if (!GS)
		{
			return;
		}
		int32 Switch = CharacterVoice.GetDefaultObject()->GetStatusIndex(NewStatus);
		if (Switch < 0)
		{
			UE_LOG(UT, Warning, TEXT("No valid index found"));
			// no valid index found (NewStatus was not a valid selection)
			return;
		}
		Switch += SwitchOffset;
//		UE_LOG(UT, Warning, TEXT("Switch is %d"), Switch);
		for (FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
		{
			AUTPlayerController* PC = Cast<AUTPlayerController>(*Iterator);
			if (PC && GS->OnSameTeam(this, PC) && (!bSkipSelf || (PC->GetPlayerState() != this)))
			{
				PC->ClientReceiveLocalizedMessage(CharacterVoice, Switch, this, PC->GetPlayerState(), NULL);
			}
		}
	}
}

bool AUTPlayerState::ShouldBroadCastWelcomeMessage(bool bExiting)
{
	return !bIsInactive && ((GetNetMode() == NM_Standalone) || (GetNetMode() == NM_Client));
}

void AUTPlayerState::NotifyTeamChanged_Implementation()
{
	for (FConstPawnIterator It = GetWorld()->GetPawnIterator(); It; ++It)
	{
		AUTCharacter* P = Cast<AUTCharacter>(*It);
		if (P != nullptr && P->GetPlayerState() == this && !P->GetTearOff())
		{
			P->NotifyTeamChanged();
		}
	}
	// HACK: remember last team player got on the URL for travelling purposes
	if (Team != nullptr)
	{
		AUTPlayerController* PC = Cast<AUTPlayerController>(GetOwner());
		if (PC != nullptr)
		{
			UUTLocalPlayer* LP = Cast<UUTLocalPlayer>(PC->Player);
			if (LP != nullptr)
			{
				LP->SetDefaultURLOption(TEXT("Team"), FString::FromInt(Team->TeamIndex));

				// make sure proper outlines are shown for new team
				AGameState* GS = GetWorld()->GetGameState<AGameState>();
				if (GS != nullptr)
				{
					for (int32 i = 0; i < GS->PlayerArray.Num(); i++)
					{
						AUTPlayerState* PS = Cast<AUTPlayerState>(GS->PlayerArray[i]);
						if (PS != nullptr && PS->bSpecialTeamPlayer && PS->GetUTCharacter() != nullptr)
						{
							PS->UpdateSpecialTacComFor(PS->GetUTCharacter(), PC);
						}
					}
				}
			}
		}
	}
}

void AUTPlayerState::IncrementKills(TSubclassOf<UDamageType> DamageType, bool bEnemyKill, AUTPlayerState* VictimPS)
{
	if (bEnemyKill)
	{
		AddCoolFactorEvent(100.0f);

		AUTGameState* GS = GetWorld()->GetGameState<AUTGameState>();
		AUTGameMode* GM = GetWorld()->GetAuthGameMode<AUTGameMode>();
		AController* Controller = Cast<AController>(GetOwner());
		APawn* Pawn = nullptr;
		AUTCharacter* UTChar = nullptr;
		bool bShouldTauntKill = false;
		if (Controller)
		{
			Pawn = Controller->GetPawn();
			UTChar = Cast<AUTCharacter>(Pawn);
		}
		AUTPlayerController* MyPC = Cast<AUTPlayerController>(GetOwner());
		TSubclassOf<UUTDamageType> UTDamage(*DamageType);

		if (GS != nullptr && GetWorld()->TimeSeconds - LastKillTime < GS->MultiKillDelay)
		{
			FName MKStat[4] = { NAME_MultiKillLevel0, NAME_MultiKillLevel1, NAME_MultiKillLevel2, NAME_MultiKillLevel3 };
			ModifyStatsValue(MKStat[FMath::Min(MultiKillLevel, 3)], 1);
			MultiKillLevel++;
			bShouldTauntKill = true;
			if (MyPC != nullptr)
			{
				MyPC->SendPersonalMessage(GS->MultiKillMessageClass, MultiKillLevel - 1, this, NULL);
			}

			AddCoolFactorEvent(MultiKillLevel * 100.0f);

			if (GM)
			{
				GM->AddMultiKillEventToReplay(Controller, FMath::Min(MultiKillLevel - 1, 3));
			}
		}
		else
		{
			MultiKillLevel = 0;
		}

		bAnnounceWeaponSpree = false;
		if (UTDamage)
		{
			if (!UTDamage.GetDefaultObject()->StatsName.IsEmpty())
			{
				// FIXMESTEVE - preset, not constructed FName
				ModifyStatsValue(FName(*(UTDamage.GetDefaultObject()->StatsName + TEXT("Kills"))), 1);
			}
			if (UTDamage.GetDefaultObject()->SpreeSoundName != NAME_None)
			{
				int32 SpreeIndex = -1;
				for (int32 i = 0; i < WeaponSprees.Num(); i++)
				{
					if (WeaponSprees[i].SpreeSoundName == UTDamage.GetDefaultObject()->SpreeSoundName)
					{
						SpreeIndex = i;
						break;
					}
				}
				if (SpreeIndex == -1)
				{
					new(WeaponSprees)FWeaponSpree(UTDamage.GetDefaultObject()->SpreeSoundName);
					SpreeIndex = WeaponSprees.Num() - 1;
				}

				WeaponSprees[SpreeIndex].Kills++;

				// delay actual announcement to keep multiple announcements in preferred order
				bAnnounceWeaponSpree = (WeaponSprees[SpreeIndex].Kills == UTDamage.GetDefaultObject()->WeaponSpreeCount);
				bShouldTauntKill = bShouldTauntKill || bAnnounceWeaponSpree;
				// more likely to kill again with same weapon, so shorten search through array by swapping
				WeaponSprees.Swap(0, SpreeIndex);
			}
			//if (UTDamage.GetDefaultObject()->RewardAnnouncementClass)
			//{
			//	bShouldTauntKill = true;
			//	if (MyPC != nullptr)
			//	{
			//		MyPC->GetPlayerState<AUTPlayerState>()->AddCoolFactorMinorEvent();
			//		MyPC->SendPersonalMessage(UTDamage.GetDefaultObject()->RewardAnnouncementClass, 0, this, VictimPS);
			//	}
			//}
		}



		LastKillTime = GetWorld()->TimeSeconds;
		Kills++;
		RoundKills++;

		if (bAnnounceWeaponSpree)
		{
			AnnounceWeaponSpree(UTDamage);
		}
		bool bVictimIsLocalPlayer = (Cast<AUTPlayerController>(VictimPS->GetOwner()) != nullptr);
		bShouldTauntKill = bShouldTauntKill || (GM && (GetWorld()->GetTimeSeconds() - GM->LastGlobalTauntTime > 30.f));
		bVictimIsLocalPlayer = bVictimIsLocalPlayer && (GetWorld()->GetNetMode() == NM_Standalone);
		if (bShouldTauntKill && Controller)
		{
			if (bVictimIsLocalPlayer || ((GetWorld()->GetTimeSeconds() - LastTauntTime > 6.f) && GM && (GetWorld()->GetTimeSeconds() - GM->LastGlobalTauntTime > 3.f)))
			{
				LastTauntTime = GetWorld()->GetTimeSeconds();
				GM->LastGlobalTauntTime = LastTauntTime;
				if (!GetWorldTimerManager().IsTimerActive(PlayKillAnnouncement))
				{
					GetWorldTimerManager().SetTimer(PlayKillAnnouncement, this, &AUTPlayerState::AnnounceKill, 0.7f, false);
				}
			}
		}
		ModifyStatsValue(NAME_Kills, 1);
	}
	else
	{
		ModifyStatsValue(NAME_Suicides, 1);
	}
}

void AUTPlayerState::TrySendTauntMessage()
{
	AUTGameState* GS = GetWorld()->GetGameState<AUTGameState>();
	AUTGameMode* GM = GetWorld()->GetAuthGameMode<AUTGameMode>();
	AAIController* Controller = Cast<AAIController>(GetOwner());

	bool bShouldTauntKill = (GM && (GetWorld()->GetTimeSeconds() - GM->LastGlobalTauntTime > 30.f));
	if (bShouldTauntKill && Controller)
	{
		if ((GetWorld()->GetTimeSeconds() - LastTauntTime > 6.f) && GM && (GetWorld()->GetTimeSeconds() - GM->LastGlobalTauntTime > 3.f))
		{
			LastTauntTime = GetWorld()->GetTimeSeconds();
			GM->LastGlobalTauntTime = LastTauntTime;
			if (!GetWorldTimerManager().IsTimerActive(PlayKillAnnouncement))
			{
				GetWorldTimerManager().SetTimer(PlayKillAnnouncement, this, &AUTPlayerState::AnnounceKill, 0.7f, false);
			}
		}
	}
}

void AUTPlayerState::AnnounceWeaponSpree(TSubclassOf<UUTDamageType> UTDamage)
{
	// will be replicated to owning player, causing OnWeaponSpreeDamage()
	WeaponSpreeDamage = UTDamage;
	
	// for standalone
	AUTPlayerController* MyPC = Cast<AUTPlayerController>(GetOwner());
	if (MyPC && MyPC->IsLocalPlayerController())
	{
		OnWeaponSpreeDamage();
	}
}

void AUTPlayerState::OnWeaponSpreeDamage()
{
	// received replicated 
	AUTPlayerController* MyPC = Cast<AUTPlayerController>(GetOwner());
	AUTGameState* GS = GetWorld()->GetGameState<AUTGameState>();
	if (MyPC && GS && WeaponSpreeDamage)
	{
		MyPC->SendPersonalMessage(GS->SpreeMessageClass, 99, this, NULL, this);
	}
}

void AUTPlayerState::IncrementDeaths(TSubclassOf<UDamageType> DamageType, AUTPlayerState* KillerPlayerState)
{
	Deaths += 1;

	ModifyStatsValue(NAME_Deaths, 1);
	TSubclassOf<UUTDamageType> UTDamage(*DamageType);
	if (UTDamage && !UTDamage.GetDefaultObject()->StatsName.IsEmpty())
	{
		// FIXMESTEVE - preset, not constructed FName
		ModifyStatsValue(FName(*(UTDamage.GetDefaultObject()->StatsName + TEXT("Deaths"))), 1);
	}

	// spree has ended
	if (Spree >= 5 && GetWorld()->GetAuthGameMode<AGameMode>() != nullptr)
	{
		AUTGameState* GS = GetWorld()->GetGameState<AUTGameState>();
		if (GS != nullptr)
		{
			GetWorld()->GetAuthGameMode<AGameMode>()->BroadcastLocalized(GetOwner(), GS->SpreeMessageClass, Spree / -5, this, KillerPlayerState);
		}
	}
	Spree = 0;

	SetNetUpdateTime(FMath::Min<double>(FindNetworkObjectInfo()->NextUpdateTime, GetWorld()->TimeSeconds + 0.3f * FMath::FRand()));

	if (Role == ROLE_Authority)
	{
		// Trigger it locally
		OnDeathsReceived();
	}
}

void AUTPlayerState::AdjustScore(int32 ScoreAdjustment)
{
	Score += ScoreAdjustment;
	ForceNetUpdate();
}

void AUTPlayerState::OnRespawnWaitReceived()
{
	AUTGameState* UTGameState = GetWorld()->GetGameState<AUTGameState>();
	if (UTGameState != nullptr)
	{
		RespawnTime = UTGameState->GetRespawnWaitTimeFor(this);
		ForceRespawnTime = RespawnTime + UTGameState->ForceRespawnTime;
	}
}

void AUTPlayerState::OnDeathsReceived()
{
	OnRespawnWaitReceived();
}

void AUTPlayerState::SetRemainingBoosts(uint8 NewRemainingBoosts)
{
	if (Role == ROLE_Authority)
	{
		RemainingBoosts = NewRemainingBoosts;
		AUTGameState* GS = GetWorld()->GetGameState<AUTGameState>();
		if (GS != nullptr && RemainingBoosts < GS->BoostRechargeMaxCharges)
		{
			if (BoostRechargeTimeRemaining <= 0.0f)
			{
				BoostRechargeTimeRemaining = GS->BoostRechargeTime;
			}
		}
		else
		{
			BoostRechargeTimeRemaining = 0.0f;
		}
	}
}

void AUTPlayerState::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (Role == ROLE_Authority)
	{
		AUTCharacter* UTChar = GetUTCharacter();

		//if (!StatsID.IsEmpty() && !RequestedName.IsEmpty() && Cast<AController>(GetOwner()))
		//{
		//	AUTGameState* UTGameState = GetWorld()->GetGameState<AUTGameState>();
		//	if (UTGameState)
		//	{
		//		TSharedRef<const FUniqueNetId> UserId = MakeShareable(new FUniqueNetIdString(*StatsID));
		//		FText EpicAccountName = UTGameState->GetEpicAccountNameForAccount(UserId);
		//		if (!EpicAccountName.IsEmpty())
		//		{
		//			GetWorld()->GetAuthGameMode()->ChangeName(Cast<AController>(GetOwner()), RequestedName, false);
		//			RequestedName = TEXT("");
		//		}
		//	}
		//}
	}
	// If we are waiting to respawn then count down
	RespawnTime -= DeltaTime;
	ForceRespawnTime -= DeltaTime;

	if (BoostRechargeTimeRemaining > 0.0f)
	{
		AUTGameState* GS = GetWorld()->GetGameState<AUTGameState>();
		if (GS != nullptr && GS->IsMatchInProgress())
		{
			bool bIsDead = false;
			if (Cast<AController>(GetOwner()) != nullptr)
			{
				bIsDead = ((AController*)GetOwner())->GetPawn() == nullptr;
			}
			else
			{
				bIsDead = GetUTCharacter() == nullptr;
			}

			BoostRechargeTimeRemaining -= DeltaTime * (bIsDead ? GS->BoostRechargeRateDead : GS->BoostRechargeRateAlive);
			if (BoostRechargeTimeRemaining <= 0.0f)
			{
				BoostRechargeTimeRemaining += GS->BoostRechargeTime;
				SetRemainingBoosts(RemainingBoosts + 1); // note: will set BoostRechargeTimeRemaining to zero if we're no longer allowed to recharge
			}
		}
	}

	if (CurrentCoolFactor > 0.0f)
	{
		CurrentCoolFactor = FMath::Max(0.f, CurrentCoolFactor - CoolFactorBleedSpeed * DeltaTime);
	}
}

AUTCharacter* AUTPlayerState::GetUTCharacter()
{
	AController* Controller = Cast<AController>(GetOwner());
	if (Controller != nullptr)
	{
		AUTCharacter* UTChar = Cast<AUTCharacter>(Controller->GetPawn());
		if (UTChar != nullptr)
		{
			return UTChar;
		}
	}
	
	// iterate through all pawns and find matching playerstate ref
	// note: this is set up to use the old character as long as possible after death, until the player respawns
	if (CachedCharacter == nullptr || CachedCharacter->IsDeadOld() || CachedCharacter->GetPlayerState() != this)
	{
		for (FConstPawnIterator Iterator = GetWorld()->GetPawnIterator(); Iterator; ++Iterator)
		{
			AUTCharacter* UTChar = Cast<AUTCharacter>(*Iterator);
			if (UTChar != nullptr && UTChar->GetPlayerState() == this && !UTChar->IsDeadOld())
			{
				CachedCharacter = UTChar;
				return UTChar;
			}
		}
	}

	return CachedCharacter;
}


void AUTPlayerState::HandleTeamChanged(AController* Controller)
{
	AUTCharacter* Pawn = Cast<AUTCharacter>(Controller->GetPawn());
	if (Pawn != nullptr)
	{
		Pawn->PlayerChangedTeam();
	}
	if (Team)
	{
		int32 Switch = (Team->TeamIndex == 0) ? 9 : 10;
		AUTPlayerController* PC = Cast<AUTPlayerController>(Controller);
		if (PC)
		{
			PC->ClientReceiveLocalizedMessage(UUTGameMessage::StaticClass(), Switch, this, NULL, NULL);
		}
	}
}

void AUTPlayerState::ServerRequestChangeTeam_Implementation(uint8 NewTeamIndex)
{
	AUTGameMode* Game = GetWorld()->GetAuthGameMode<AUTGameMode>();
	if (Game != nullptr && Game->bTeamGame)
	{
		AController* Controller =  Cast<AController>( GetOwner() );
		if (Controller != nullptr)
		{
			if (NewTeamIndex == 255 && Team != nullptr)
			{
				NewTeamIndex = (Team->TeamIndex + 1) % FMath::Max<uint8>(1, GetWorld()->GetGameState<AUTGameState>()->Teams.Num());
			}
			if (Game->ChangeTeam(Controller, NewTeamIndex, true))
			{
				HandleTeamChanged(Controller);
			}
		}
	}
}

bool AUTPlayerState::ServerRequestChangeTeam_Validate(uint8 FireModeNum)
{
	return true;
}

void AUTPlayerState::CopyProperties(APlayerState* PlayerState)
{
	Super::CopyProperties(PlayerState);

	AUTPlayerState* PS = Cast<AUTPlayerState>(PlayerState);
	if (PS != nullptr)
	{
		if (PS->Team == nullptr)
		{
			// MovePlayerToTeam(AController* Player, AUTPlayerState* PS, uint8 NewTeam
		}
		PS->Team = Team;
		PS->TeamId = TeamId;
		PS->FractionId = FractionId;


		PS->StatsID = StatsID;
		PS->Kills = Kills;
		PS->RoundKills = RoundKills;
		PS->DamageDone = DamageDone;
		PS->Deaths = Deaths;
		PS->Assists = Assists;
		PS->SelectedCharacter = SelectedCharacter;
		PS->StatManager = StatManager;
		PS->StatsData = StatsData;
		PS->BoostClass = BoostClass;
		PS->RemainingBoosts = RemainingBoosts;
		PS->RemainingLives = RemainingLives;
		PS->BoostRechargeTimeRemaining = BoostRechargeTimeRemaining;
		if (PS->StatManager)
		{
			PS->StatManager->InitializeManager(PS);
		}
	}
}
void AUTPlayerState::OverrideWith(APlayerState* PlayerState)
{
	Super::OverrideWith(PlayerState);
	AUTPlayerState* PS = Cast<AUTPlayerState>(PlayerState);
	if (PS != nullptr)
	{
		// note that we don't need to call Add/RemoveFromTeam() here as that happened when Team was assigned to the passed in PlayerState
		Team = PS->Team;

		SpectatingID = PS->SpectatingID;
		SpectatingIDTeam = PS->SpectatingIDTeam;

#if WITH_PROFILE
		if (PS->McpProfile != nullptr)
		{
			FMcpProfileSetter::Set(this, PS->GetMcpProfile());
		}
#endif
	}
}

void AUTPlayerState::OnCarriedObjectChanged()
{
	SetCarriedObject(CarriedObject);
}

void AUTPlayerState::SetCarriedObject(AUTCarriedObject* NewCarriedObject)
{
	if (Role == ROLE_Authority)
	{
		CarriedObject = NewCarriedObject;
		ForceNetUpdate();
	}
	AUTPlayerController *PlayerOwner = Cast<AUTPlayerController>(GetOwner());
	if (PlayerOwner && PlayerOwner->MyUTHUD && CarriedObject)
	{
		PlayerOwner->MyUTHUD->LastFlagGrabTime = GetWorld()->GetTimeSeconds();
	}
}

void AUTPlayerState::ClearCarriedObject(AUTCarriedObject* OldCarriedObject)
{
	if (Role == ROLE_Authority)
	{
		if (CarriedObject == OldCarriedObject)
		{
			CarriedObject = NULL;
			ForceNetUpdate();
		}
	}
}

uint8 AUTPlayerState::GetTeamNum() const
{
	return (Team != nullptr) ? Team->GetTeamNum() : 255;
}

void AUTPlayerState::EndPlay(const EEndPlayReason::Type Reason)
{
	if (!bIsInactive && Team != nullptr && GetOwner() != nullptr)
	{
		Team->RemoveFromTeam(Cast<AController>(GetOwner()));
	}
	GetWorldTimerManager().ClearAllTimersForObject(this);
	Super::EndPlay(Reason);
}

void AUTPlayerState::BeginPlay()
{
	// default so value is never NULL
	if (SelectedCharacter == nullptr)
	{
		SelectedCharacter = GetDefault<AUTCharacter>()->CharacterData;
	}

	Super::BeginPlay();

	if (Role == ROLE_Authority && StatManager == nullptr)
	{
		//Make me a statmanager
		StatManager = NewObject<UStatManager>(this, UStatManager::StaticClass());
		StatManager->InitializeManager(this);
	}
}

void AUTPlayerState::SetCharacterVoice(const FString& CharacterVoicePath)
{
	if (Role == ROLE_Authority)
	{
		CharacterVoice = (CharacterVoicePath.Len() > 0) ? LoadClass<UUTCharacterVoice>(NULL, *CharacterVoicePath, NULL, GetCosmeticLoadFlags(), NULL) : NULL;
		// redirect from blueprint, for easier testing in the editor via C/P
#if WITH_EDITORONLY_DATA
		if (CharacterVoice == nullptr)
		{
			UBlueprint* BP = LoadObject<UBlueprint>(NULL, *CharacterVoicePath, NULL, GetCosmeticLoadFlags(), NULL);
			if (BP != nullptr)
			{
				CharacterVoice = *BP->GeneratedClass;
			}
		}
#endif
	}
}

bool AUTPlayerState::IsOwnedByReplayController() const
{
	return Cast<AUTDemoRecSpectator>(GetOwner()) != nullptr;
}

void AUTPlayerState::SetCharacter(const FString& CharacterPath)
{
	if (Role == ROLE_Authority || IsOwnedByReplayController())
	{
		TSubclassOf<AUTCharacterContent> NewCharacter = (CharacterPath.Len() > 0) ? TSubclassOf<AUTCharacterContent>(FindObject<UClass>(NULL, *CharacterPath, false)) : GetDefault<AUTCharacter>()->CharacterData;
// redirect from blueprint, for easier testing in the editor via C/P
#if WITH_EDITORONLY_DATA
		if (NewCharacter == nullptr && CharacterPath.Len() > 0 && GetNetMode() == NM_Standalone)
		{
			UBlueprint* BP = LoadObject<UBlueprint>(NULL, *CharacterPath, NULL, LOAD_NoWarn, NULL);
			if (BP != nullptr)
			{
				NewCharacter = *BP->GeneratedClass;
			}
		}
#endif
		if (NewCharacter == nullptr)
		{
			// async load the character class
			UObject* CharPkg = NULL;
			FString Path = CharacterPath;
			if (ResolveName(CharPkg, Path, false, false) && Cast<UPackage>(CharPkg) != nullptr)
			{
				TWeakObjectPtr<AUTPlayerState> PS(this);
				if (FPackageName::DoesPackageExist(CharPkg->GetName()))
				{
					LoadPackageAsync(CharPkg->GetName(), FLoadPackageAsyncDelegate::CreateLambda([PS, CharacterPath](const FName& PackageName, UPackage* LoadedPackage, EAsyncLoadingResult::Type Result)
					{
						if (Result == EAsyncLoadingResult::Succeeded && PS.IsValid() && TSubclassOf<AUTCharacterContent>(FindObject<UClass>(NULL, *CharacterPath, false)) != nullptr)
						{
							PS->SetCharacter(CharacterPath);
						}
						else
						{
							// redirectors don't work when using FindObject, handle that case
							UObjectRedirector* Redirector = FindObject<UObjectRedirector>(NULL, *CharacterPath, true);
							if (Redirector != nullptr && Redirector->DestinationObject != nullptr)
							{
								PS->SetCharacter(Redirector->DestinationObject->GetPathName());
							}
						}
					}));
				}
			}
		}
		// make sure it's not an invalid base class
		else if (NewCharacter != nullptr && !(NewCharacter->ClassFlags & CLASS_Abstract) && NewCharacter.GetDefaultObject()->GetMesh()->SkeletalMesh != nullptr)
		{
			SelectedCharacter = NewCharacter;
			NotifyTeamChanged();
		}
	}
}

bool AUTPlayerState::ServerSetCharacter_Validate(const FString& CharacterPath)
{
	return true;
}
void AUTPlayerState::ServerSetCharacter_Implementation(const FString& CharacterPath)
{
#if WITH_EDITOR
	// Don't allow character loading during join in progress right now, it causes poor performance
//	if (!bOnlySpectator && (GetNetMode() == NM_Standalone || !GetWorld()->GetGameState()->HasMatchStarted() || (nullptr != GetWorld()->GetAuthGameMode<AUTLobbyGameMode>())))
#else
	if (!bOnlySpectator && (GetNetMode() == NM_Standalone || SelectedCharacter == nullptr || !GetWorld()->GetGameState()->HasMatchStarted()))
#endif
	{
		SetCharacter(CharacterPath);
	}
}

bool AUTPlayerState::ModifyStat(FName StatName, int32 Amount, EStatMod::Type ModType)
{
	//if (StatManager != nullptr)
	//{
	//	FStat* Stat = StatManager->GetStatByName(StatName);
	//	if (Stat != nullptr)
	//	{
	//		Stat->ModifyStat(Amount, ModType);
	//		if (ModType == EStatMod::Delta && Amount > 0)
	//		{
	//			// award XP
	//			GiveXP(Stat->XPPerPoint * Amount);
	//		}
	//		return true;
	//	}
	//	else
	//	{
	//		return false;
	//	}
	//}
	//else
	{
		return false;
	}
}

bool AUTPlayerState::ServerNextChatDestination_Validate() { return true; }
void AUTPlayerState::ServerNextChatDestination_Implementation()
{
	AUTGameMode* GameMode = GetWorld()->GetAuthGameMode<AUTGameMode>();
	if (GameMode)
	{
		//ChatDestination = GameMode->GetNextChatDestination(this, ChatDestination);
	}
}

void AUTPlayerState::AddMatchHighlight(FName NewHighlight, float HighlightData)
{
	AUTGameState* GameState = GetWorld()->GetGameState<AUTGameState>();
	float NewPriority = GameState ? GameState->HighlightPriority.FindRef(NewHighlight) : 0.f;

	for (int32 i = 0; i < 5; i++)
	{
		if (MatchHighlights[i] == NAME_None)
		{
			MatchHighlights[i] = NewHighlight;
			MatchHighlightData[i] = HighlightData;
			return;
		}
		else if (GameState)
		{
			float TestPriority = GameState->HighlightPriority.FindRef(MatchHighlights[i]);
			if (NewPriority > TestPriority)
			{
				// insert the highlight, look for a spot for the displaced highlight
				FName MovedHighlight = MatchHighlights[i];
				float MovedData = MatchHighlightData[i];
				MatchHighlights[i] = NewHighlight;
				MatchHighlightData[i] = HighlightData;
				NewHighlight = MovedHighlight;
				HighlightData = MovedData;
			}
		}
	}

	// if no open slots, try to replace lowest priority highlight
	if (GameState)
	{
		NewPriority = GameState->HighlightPriority.FindRef(NewHighlight);
		float WorstPriority = 0.f;
		int32 WorstIndex = -1.f;
		for (int32 i = 0; i < 5; i++)
		{
			float TestPriority = GameState->HighlightPriority.FindRef(MatchHighlights[i]);
			if (WorstPriority < TestPriority)
			{
				WorstPriority = TestPriority;
				WorstIndex = i;
			}
		}
		if ((WorstIndex >= 0) && (NewPriority < WorstPriority))
		{
			MatchHighlights[WorstIndex] = NewHighlight;
			MatchHighlightData[WorstIndex] = HighlightData;
		}
	}
}



void AUTPlayerState::RegisterPlayerWithSession(bool bWasFromInvite)
{
	UDemoNetDriver* DemoDriver = GetWorld()->DemoNetDriver;
	if (DemoDriver)
	{
		return;
	}

	Super::RegisterPlayerWithSession(bWasFromInvite);
}

void AUTPlayerState::UnregisterPlayerWithSession()
{
	UDemoNetDriver* DemoDriver = GetWorld()->DemoNetDriver;
	if (DemoDriver)
	{
		return;
	}

	Super::UnregisterPlayerWithSession();
}


void AUTPlayerState::UpdateOldName()
{
	SetOldPlayerName(GetPlayerName());
}

void AUTPlayerState::OnRep_PlayerName()
{
	bHasValidClampedName = false;

	if (GetPlayerName().IsEmpty())
	{
		// Demo rec spectator is allowed empty name
		return;
	}

	if (GetWorld()->TimeSeconds < 2.f)
	{
		UpdateOldName();
		bHasBeenWelcomed = true;
		return;
	}

	if (!GetWorldTimerManager().IsTimerActive(UpdateOldNameHandle))
	{
		GetWorldTimerManager().SetTimer(UpdateOldNameHandle, this, &AUTPlayerState::UpdateOldName, 5.f, false);
	}

	// new player or name change
	if (bHasBeenWelcomed)
	{
		if (ShouldBroadCastWelcomeMessage())
		{
			for (FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
			{
				APlayerController* PlayerController = Iterator->Get();
				if (PlayerController != nullptr && PlayerController->IsLocalPlayerController())
				{
					PlayerController->ClientReceiveLocalizedMessage(EngineMessageClass, 2, this);
				}
			}
		}
	}
	else
	{
		int32 WelcomeMessageNum = bOnlySpectator ? 16 : 1;
		bHasBeenWelcomed = true;

		if (ShouldBroadCastWelcomeMessage())
		{
			for (FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
			{
				APlayerController* PlayerController = Iterator->Get();
				if (PlayerController != nullptr && PlayerController->IsLocalPlayerController())
				{
					PlayerController->ClientReceiveLocalizedMessage(EngineMessageClass, WelcomeMessageNum, this);
				}
			}
		}
	}
}

void AUTPlayerState::SetReadyToPlay(bool bNewReadyState)
{
	bReadyToPlay = bNewReadyState;

	uint8 NewReadyState = bReadyToPlay;
	if ((ReadySwitchCount > 2) && bReadyToPlay)
	{
		if (GetWorld()->GetTimeSeconds() - LastReadySwitchTime > 0.2f)
		{
			ReadySwitchCount++;
			LastReadySwitchTime = GetWorld()->GetTimeSeconds();
			ReadyMode = 1;
		}
		if (ReadySwitchCount > 10)
		{
			ReadyMode = 2;
		}
	}
	else if (!bReadyToPlay && (GetWorld()->GetTimeSeconds() - LastReadySwitchTime > 0.5f))
	{
		ReadyMode = 0;
	}
	else if (NewReadyState != LastReadyState)
	{
		ReadySwitchCount = (GetWorld()->GetTimeSeconds() - LastReadySwitchTime < 0.5f) ? ReadySwitchCount + 1 : 0;
		LastReadySwitchTime = GetWorld()->GetTimeSeconds();
	}
	LastReadyState = NewReadyState;
}

void AUTPlayerState::OnRepSpecialPlayer()
{
	AUTPlayerController* UTPC = Cast<AUTPlayerController>(GetWorld()->GetFirstPlayerController());
	for (FConstPawnIterator It = GetWorld()->GetPawnIterator(); It; ++It)
	{
		AUTCharacter* Character = Cast<AUTCharacter>(*It);
		if (Character != nullptr && Character->GetPlayerState() == this && !Character->GetTearOff())
		{
			UpdateSpecialTacComFor(Character, UTPC);
		}
	}
}

void AUTPlayerState::OnRepSpecialTeamPlayer()
{
	AUTPlayerController* UTPC = Cast<AUTPlayerController>(GetWorld()->GetFirstPlayerController());
	for (FConstPawnIterator It = GetWorld()->GetPawnIterator(); It; ++It)
	{
		AUTCharacter* Character = Cast<AUTCharacter>(*It);
		if (Character != nullptr && Character->GetPlayerState() == this && !Character->GetTearOff())
		{
			UpdateSpecialTacComFor(Character, UTPC);
		}
	}
}

void AUTPlayerState::UpdateSpecialTacComFor(AUTCharacter* Character, AUTPlayerController* UTPC)
{
	Character->SetOutlineLocal(bSpecialPlayer || (bSpecialTeamPlayer && UTPC && UTPC->GetTeamNum() == GetTeamNum()), true);
}

float AUTPlayerState::GetStatsValue(FName StatsName) const
{
	return StatsData.FindRef(StatsName);
}

void AUTPlayerState::SetStatsValue(FName StatsName, float NewValue)
{
	AUTGameState* UTGameState = GetWorld()->GetGameState<AUTGameState>();
	if (UTGameState && UTGameState->HasMatchStarted())
	{
		LastScoreStatsUpdateTime = GetWorld()->GetTimeSeconds();
		StatsData.Add(StatsName, NewValue);
	}
}

void AUTPlayerState::ModifyStatsValue(FName StatsName, float Change)
{
	AUTGameState* UTGameState = GetWorld()->GetGameState<AUTGameState>();
	if (UTGameState && UTGameState->HasMatchStarted())
	{
		LastScoreStatsUpdateTime = GetWorld()->GetTimeSeconds();
		float CurrentValue = StatsData.FindRef(StatsName);
		StatsData.Add(StatsName, CurrentValue + Change);
	}
}

void AUTPlayerState::OnRep_bIsInactive()
{
	//Now that we replicate InactivePRI's the super function is unsafe without these checks
	if (GetWorld() && GetWorld()->GetGameState())
	{
		Super::OnRep_bIsInactive();
	}
}


void AUTPlayerState::ClientReceiveRconMessage_Implementation(const FString& Message)
{	

}

bool AUTPlayerState::IsABeginner(AUTGameMode* DefaultGameMode) const
{
	int32 Badge = 0;
	int32 Level = 0;
	return (Badge == 0);
}

bool AUTPlayerState::ServerSetBoostItem_Validate(int PowerupIndex) { return true; }
void AUTPlayerState::ServerSetBoostItem_Implementation(int PowerupIndex)
{
	AUTGameState* UTGameState = GetWorld()->GetGameState<AUTGameState>();
	if (UTGameState)
	{
		TSubclassOf<class AUTInventory> SelectedBoost = UTGameState->GetSelectableBoostByIndex(this, PowerupIndex);
		if (SelectedBoost != nullptr)
		{
			BoostClass = SelectedBoost;
		}
	}
}

TSubclassOf<UUTCharacterVoice> AUTPlayerState::GetCharacterVoiceClass()
{
	if (CharacterVoice == nullptr)
	{
		if (SelectedCharacter == nullptr)
		{
			SelectedCharacter = GetDefault<AUTCharacter>()->CharacterData;
		}
		if (SelectedCharacter != nullptr)
		{
			AUTCharacterContent* CharacterDefaultObject = Cast<AUTCharacterContent>(SelectedCharacter.GetDefaultObject());
			if (CharacterDefaultObject != nullptr && CharacterDefaultObject->CharacterVoice != nullptr)
			{
				CharacterVoice = CharacterDefaultObject->CharacterVoice;
			}
		}
	}

	return CharacterVoice;
}

void AUTPlayerState::AddCoolFactorMinorEvent()
{
	AddCoolFactorEvent(50.0f);
}

void AUTPlayerState::AddCoolFactorEvent(float CoolFactorAddition)
{
	// No negative cool please
	if (CoolFactorAddition <= 0)
	{
		return;
	}

	float CurrentWorldTime = GetWorld()->GetTimeSeconds();
	CurrentCoolFactor += CoolFactorAddition;

	if (CurrentCoolFactor >= MinimumConsiderationForCoolFactorHistory)
	{
		UE_LOG(UT, Verbose, TEXT("CoolFactorHistory updated for %s at %f"), *GetPlayerName(), CurrentWorldTime);

		if (CoolFactorHistory.Num() > 0 && CoolFactorHistory[CoolFactorHistory.Num() - 1].TimeOccurred - CurrentWorldTime < CoolFactorCombinationWindow)
		{
			// Overwrite the old cool history if we're cooler, highly likely
			if (CurrentCoolFactor > CoolFactorHistory[CoolFactorHistory.Num() - 1].CoolFactorAmount)
			{
				CoolFactorHistory[CoolFactorHistory.Num() - 1].CoolFactorAmount = CurrentCoolFactor;
				CoolFactorHistory[CoolFactorHistory.Num() - 1].TimeOccurred = CurrentWorldTime;
			}
		}
		else
		{
			FCoolFactorHistoricalEvent NewEvent;
			NewEvent.CoolFactorAmount = CurrentCoolFactor;
			NewEvent.TimeOccurred = CurrentWorldTime;
			CoolFactorHistory.Add(NewEvent);
		}		
	}
}

void AUTPlayerState::SetUniqueId(const TSharedPtr<const FUniqueNetId>& InUniqueId)
{
	Super::SetUniqueId(InUniqueId); 
}




void AUTPlayerState::AddAchievement(const ItemAchievementTypeId::Type AchievementId)
{
	Achievements.Add(AchievementId);

	if (auto MyCtrl = Cast<AUTPlayerController>(GetOwner()))
	{
		MyCtrl->ClientShowAchievement(AchievementId);
	}
}

bool AUTPlayerState::AddDamageData(const int32& Index, const FPlayerDamageData& Data)
{
	auto &Value = DamageData.FindOrAdd(Index);
	Value += Data;
	DamageDataLast = Data;

	return true;
}

TMap<int32, FPlayerDamageData>& AUTPlayerState::GetDamageData()
{
	return DamageData;
}

FPlayerDamageData& AUTPlayerState::GetDamageDataLast()
{
	return DamageDataLast;
}

float AUTPlayerState::GetAccuracyBySurface(const uint8& Surface)
{
	SIZE_T _count = 0, _countBySurface = 0;

	for (auto& i : ShootData)
	{
		if (i.Surface == Surface)
		{
			++_countBySurface;
		}
		++_count;
	}

	return float(_countBySurface) / float(_count);
}

float AUTPlayerState::GetAccuracyByPawn(const bool IsFriendly)
{
	SIZE_T _count = 0, _countByPawn = 0;

	for (auto& i : ShootData)
	{
		if ((i.IsPawn_Friendly && IsFriendly) || (i.IsPawn_Enemy && !IsFriendly))
		{
			++_countByPawn;
		}
		++_count;
	}

	return float(_countByPawn) / float(_count);
}

float AUTPlayerState::GetAccuracyAnyPawn()
{
	SIZE_T _count = 0, _countByPawn = 0;

	for (auto& i : ShootData)
	{
		if (i.IsPawn_Friendly || i.IsPawn_Enemy)
		{
			++_countByPawn;
		}
		++_count;
	}

	return float(_countByPawn) / float(_count);
}

int32 AUTPlayerState::GetHitsByPawn(const bool IsFriendly) const
{
	SIZE_T _count = 0, _countByPawn = 0;

	for (auto& i : ShootData)
	{
		if ((i.IsPawn_Friendly && IsFriendly) || (i.IsPawn_Enemy && !IsFriendly))
		{
			++_countByPawn;
		}
		++_count;
	}

	return _countByPawn;
}

void AUTPlayerState::AddShootData(const FPlayerShootData& Data)
{
	ShootData.Add(Data);
}

void AUTPlayerState::ScoreKill(AUTPlayerState* Victim, int32 Points)
{
	auto MyGameState = GetWorld()->GetGameState<AUTGameState>();
	if (MyGameState && Victim)
	{
		auto& Value = KillsDataLast.FindOrAdd(Victim->GetDamageDataLast().Weapon);
		const bool IsFreindly = FFlagsHelper::HasAnyFlags(MyGameState->GetGameModeType().Value, EGameMode::LostDeadMatch.ToFlag() | EGameMode::DuelMatch.ToFlag()) ? false : (GetTeamNum() == Victim->GetTeamNum());
		IsFreindly ? Value : ++Value;
		IsFreindly ? Kills : ++Kills;
	}
	else
	{
		Kills++;
	}

	ScorePoints(Points);
}

void AUTPlayerState::ScoreDeath(AUTPlayerState* KilledBy, int32 Points)
{
	DamageData.Empty();
	DamageDataLast = FPlayerDamageData();

	for (auto& i : KillsDataLast)
	{
		KillsData.Add(i.Key, i.Value);
	}

	KillsDataLast.Empty();

	Deaths++;
	ScorePoints(Points);
}

void AUTPlayerState::ScorePoints(int32 Points)
{
	Score += Points;
}

void AUTPlayerState::InitializeProfiles(const TArray<FProfileHack>& InProfiles)
{
	if (HasAuthority())
	{
		GetIdentityComponent()->OnAuthorizeAsSimulation();

		if (InProfiles.Num())
		{
			auto InitCharacter = [](FClientPreSetData& InData, TArray<FInvertoryItemWrapper>& InWrapperArray, const FProfileHack& InHack) {
				InWrapperArray.AddZeroed();

				InWrapperArray.Last().EntityId = FGuid::NewGuid();
				InWrapperArray.Last().CategoryTypeId = ECategoryTypeId::Character;
				InWrapperArray.Last().Count = 1;
				InWrapperArray.Last().ModelId = InHack.CharacterModelId;

				InData.CharacterId = InWrapperArray.Last().EntityId;
			};

			auto InitWeapon = [](FClientPreSetData& InData, TArray<FInvertoryItemWrapper>& InWrapperArray, const FProfileHack& InHack, const bool IsPrimary) {
				//auto FoundWeaponInstance = GetGameSingleton()->FindItem<UItemWeaponEntity>(IsPrimary ? InHack.PrimaryWeaponSkin : InHack.SecondaryWeaponSkin, ECategoryTypeId::Weapon);

				FInvertoryInstalledAddonContainer AddonInfo;
				TArray<FInvertoryInstalledAddonContainer> localInstalledAddons;

				InWrapperArray.AddZeroed();

				InWrapperArray.Last().EntityId = FGuid::NewGuid();
				InWrapperArray.Last().CategoryTypeId = ECategoryTypeId::Material;
				InWrapperArray.Last().Count = 1;
				InWrapperArray.Last().ModelId = IsPrimary ? InHack.PrimaryWeaponSkin : InHack.SecondaryWeaponSkin;

				AddonInfo.AddonId = InWrapperArray.Last().EntityId;
				AddonInfo.AddonSlot = 100;

				localInstalledAddons.Add(AddonInfo);

				for (auto Addon : IsPrimary ? InHack.PrimaryWeaponAddons : InHack.SecondaryWeaponAddons)
				{
					InWrapperArray.AddZeroed();

					InWrapperArray.Last().EntityId = FGuid::NewGuid();
					InWrapperArray.Last().CategoryTypeId = ECategoryTypeId::Addon;
					InWrapperArray.Last().Count = 1;
					InWrapperArray.Last().ModelId = Addon;

					//auto FoundModuleInstance = GetGameSingleton()->FindItem<UItemModuleEntity>(Addon, ECategoryTypeId::Addon);

					AddonInfo.AddonId = InWrapperArray.Last().EntityId;
					AddonInfo.AddonSlot = 0;

					/*						int32 _count = 0;
					for (auto Socket : FoundWeaponInstance->SupportedSockets)
					{
					if (Socket == FoundModuleInstance->TargetSocket)
					{
					AddonInfo.AddonSlot = _count;
					break;
					}

					++_count;
					}*/

					localInstalledAddons.Add(AddonInfo);
				}

				InWrapperArray.AddZeroed();

				InWrapperArray.Last().EntityId = FGuid::NewGuid();
				InWrapperArray.Last().CategoryTypeId = ECategoryTypeId::Weapon;
				InWrapperArray.Last().Count = 1;
				InWrapperArray.Last().ModelId = IsPrimary ? InHack.PrimaryWeaponModelId : InHack.SecondaryWeaponModelId;

				FPlayerInventoryItemContainer InventoryItemContainer;
				InventoryItemContainer.ItemId = InWrapperArray.Last().EntityId;
				InventoryItemContainer.SlotId = UGameSingleton::Get()->FindItem(InWrapperArray.Last().ModelId, ECategoryTypeId::Weapon)->GetDescription().SlotType;
				InData.Items.Add(InventoryItemContainer);

				InWrapperArray.Last().InstalledAddons = localInstalledAddons;
			};

			TArray<FInvertoryItemWrapper> InventoryItems;
			TArray<FClientPreSetData> ProfilesData;

			for (auto Data : InProfiles)
			{
				if (Data.CharacterModelId != INDEX_NONE)
				{
					FClientPreSetData PreSetData;

					PreSetData.IsEnabled = true;
					PreSetData.Name = FString("Editor Profile ") + FString::FromInt(Data.CharacterModelId);

					InitCharacter(PreSetData, InventoryItems, Data);

					if (Data.PrimaryWeaponModelId != INDEX_NONE)
					{
						InitWeapon(PreSetData, InventoryItems, Data, true);
					}

					if (Data.SecondaryWeaponModelId != INDEX_NONE)
					{
						InitWeapon(PreSetData, InventoryItems, Data, false);
					}

					ProfilesData.Add(PreSetData);
				}
			}

			GetInventoryComponent()->OnLoadPlayerInventorySuccessfully(InventoryItems);
			GetProfileComponent()->OnProfilesUpdateHandle(ProfilesData);
		}
	}
}