#include "LokaGame.h"
#include "OnlineGameState.h"
#include "NodeComponent.h"
#include <Online.h>
#include "OnlineGameMode.h"

AOnlineGameState::AOnlineGameState()
	: Super()
{
	//==========================================================================
	NodeComponent = CreateDefaultSubobject<UNodeComponent>(TEXT("NodeComponent"));
	NodeComponent->SetIsReplicated(true);


	//==========================================================================
	//	Добавляем виртуальное событие при успешной загрузке информации о матче
	//	Что бы перегрузить его в режиме строительства
	NodeComponent->OnGetMatchInformationSuccessfullyDelegate.AddUObject(this, &AOnlineGameState::OnGetMatchInformation_Successfully);
}

void AOnlineGameState::HandleBeginPlay()
{
	Super::HandleBeginPlay();
	GetNodeComponent()->BeginPoolingMatchInformation();
}

void AOnlineGameState::OnGetMatchInformation_Successfully(const FNodeSessionMatch& information)
{
	UE_LOG(LogOnline, Display, TEXT("AOnlineGameState::OnGetMatchInformation_Successfully"));
	GetNodeComponent()->BeginPoolingStartMatchRequest();	
	InitGameState(information);
	GetOnlineGameMode()->OnInitializeMatchInformation();
}

void AOnlineGameState::InitGameState(const FNodeSessionMatch& information)
{
	UE_LOG(LogInit, Display, TEXT(">> AOnlineGameState::InitGameState"));
}

TAttribute<FGuid> AOnlineGameState::GetSessionToken() const
{
	return GetNodeComponent()->GetIdentityToken();
}

FNodeSessionMatch AOnlineGameState::GetMatchInformation() const
{
	if (auto node = GetNodeComponent())
	{
		return node->GetMatchInformation();
	}

	return FNodeSessionMatch();
}

UNodeComponent* AOnlineGameState::GetNodeComponent() const
{
	return GetValidObject(NodeComponent);
}

EGameMode AOnlineGameState::GetGameModeType() const
{
	return GetMatchInformation().Options.GetGameMode();
}

EGameMap AOnlineGameState::GetGameMapType() const
{
	return GetMatchInformation().Options.GetGameMap();
}

//===================================================================
//  Game  Mode Helper

AGameMode* AOnlineGameState::GetBasicGameMode() const
{
	if (auto w = GetValidWorld())
	{
		return w->GetAuthGameMode<AGameMode>();
	}

	return nullptr;
}

AOnlineGameMode* AOnlineGameState::GetOnlineGameMode() const
{
	return Cast<AOnlineGameMode>(GetBasicGameMode());
}

UWorld* AOnlineGameState::GetValidWorld() const
{
	return GetValidObject(GetWorld());
}
