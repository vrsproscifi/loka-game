#pragma once

#include "OnlineGameState.h"
#include "Interfaces/LokaNetUserInterface.h"
#include "ConstructionGameState.generated.h"

class UPlayerInventoryItem;
class UInventoryComponent;
class UConstructionComponent;
class AConstructionPlayerState;

UCLASS()
class AConstructionGameState 
	: public AOnlineGameState
	, public ILokaNetUserInterface
{
	friend class AConstructionGameMode;

	GENERATED_BODY()

public:

	AConstructionGameState();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	UConstructionComponent* GetConstructionComponent() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Component)
	UInventoryComponent* GetInventoryComponent() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Identity)
	FORCEINLINE FGuid GetOwnerToken() const
	{
		return OwnerToken;
	}

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Identity)
	bool SetOwnerToken(AConstructionPlayerState* InFrom, const FGuid& InToken);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Identity)
	AConstructionPlayerState* GetOwnerState() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Identity)
	FGuid GetOwnerPlayerId() const;

	virtual bool IsAllowUserInventory() const override { return true; }
	virtual FGuid GetIdentityToken() const override { return OwnerToken; }
	virtual bool SetIdentityTokenString(const FString& InTokenString) override;
	virtual void SetIdentityToken(const FGuid& InToken) override;

protected:

	UPROPERTY()					UConstructionComponent*		ConstructionComponent;
	UPROPERTY()					UInventoryComponent*		InventoryComponent;
	UPROPERTY()					FGuid						OwnerToken;
	UPROPERTY(Replicated)		AConstructionPlayerState*	OwnerState;
	UPROPERTY(Replicated)		FGuid						OwnerPlayerId;

	UFUNCTION()					void						OnInventoryUpdate(const TArray<UPlayerInventoryItem*> InData);
};