// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "StoreController/Models/TutorialShortView.h"
#include "TutorialBase.generated.h"

class UTutorialComponent;
struct FConfirmTutorialStepResponse;

UENUM(BlueprintType)
namespace ETutorialAction
{
	enum Type
	{
		Skip,
		Time,
		BuyItem,
		EquipItem,
		BuildingSelect,
		BuildingPlace,
		BuildingRemove,
		End UMETA(Hidden)
	};
}

UENUM(BlueprintType)
namespace ETutorialInfoShowAs
{
	enum Type
	{
		None,
		Notify,
		Message,
		Tip,
		End UMETA(Hidden)
	};
}

UENUM(BlueprintType)
namespace ETutorialInfoSound
{
	enum Type
	{
		Show,
		Hide,
		RewardShow,
		RewardHide,
		Voice,
		End
	};
}

USTRUCT(BlueprintType)
struct FTutorialInfoBase
{
	GENERATED_USTRUCT_BODY();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TEnumAsByte<ETutorialAction::Type> TargetAction;

	/*
	 *	For paste player name or other elemets now allow use format keys:
	 *	{PlayerName}
	 *	{PlayerMoney}
	 *	{PlayerDonate}
	 *	{PlayerCoin}
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (MultiLine = true))
	FText Title;

	/*
	 *	For paste player name or other elemets now allow use format keys:
	 *	{PlayerName}
	 *	{PlayerMoney}
	 *	{PlayerDonate}
	 *	{PlayerCoin}
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (MultiLine = true))
	FText Message;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TEnumAsByte<ETextJustify::Type> MessageJustification;

	/*
	 * Used for define display message as Message Box or Notify
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TEnumAsByte<ETutorialInfoShowAs::Type> ShowAs;

	/*
	 * Used for TargetAction
	 * if TargetAction = Time, this is time in seconds
	 * if TargetAction = BuyItem, this is item category
	 * if TargetAction = EquipItem, this is item category
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 CompleteSpeciferPrimary;

	/*
	 * Used for TargetAction
	 * if TargetAction = BuyItem, this is item model index
	 * if TargetAction = EquipItem, this is item model index
	 * if TargetAction = BuildingSelect, this is item model index
	 * if TargetAction = BuildingPlace, this is item model index
	 * if TargetAction = BuildingRemove, this is item model index
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 CompleteSpeciferSecondary;

	/*
	 * Used for play unique sound on action
	 */
	UPROPERTY(EditDefaultsOnly)
	FSlateSound Sounds[ETutorialInfoSound::End];

	/*
	* Used for Highlight any slate widgets by tag
	* Key as FName, this is widget tag.
	* Value as float, this is highlight timer, use 0 to infinity
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TMap<FName, float> WidgetsHighlightList;

	/*
	* Used for Un Highlight any slate widgets by tag
	* List tags for Highlight off
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FName> WidgetsUnhighlightList;
};

UCLASS(Blueprintable, Abstract)
class LOKAGAME_API UTutorialBase : public UObject
{
	friend class UGameSingleton;
	friend class UTutorialComponent;

	GENERATED_BODY()
	
public:	
	UTutorialBase();
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = Stage)
	bool ForceStage(const int32& InStage);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Stage)
	FORCEINLINE int32 GetCurrentStage() const { return CurrentStage; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Stage)
	FORCEINLINE int32 GetTutorialIndex() const { return TutorialIndex; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Stage)
	FORCEINLINE FText GetTutorialName() const { return TutorialName; }

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = Stage)
	bool StartTutorial(const int32& InStage = 0);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = Stage)
	void EndCurrentStage();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = Stage)
	bool EndTutorial();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = Visual)
	void ForceHide();

#if WITH_ENGINE
	virtual class UWorld* GetWorld() const;
#endif

protected:

	void OnConfirmCurrentStage(const FConfirmTutorialStepResponse& InResponse);

	UPROPERTY(BlueprintReadWrite, Category = Stage)
	int32 CurrentStage;

	UPROPERTY(BlueprintReadWrite, Category = Stage)
	bool bIsStarted;

	UPROPERTY(Transient)
	UTutorialComponent* TutorialComponent;

	UPROPERTY(EditDefaultsOnly, Category = Stage)
	int32 TutorialIndex;	

	UPROPERTY(EditDefaultsOnly, Category = Stage, meta = (MultiLine = true))
	FText TutorialName;

	UPROPERTY(EditDefaultsOnly, Category = Stage)
	TMap<int32, FText> NextTutorials;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stage)
	TArray<FTutorialInfoBase> Stages;

	FTimerHandle timer_TutorialTime;

	UPROPERTY()
	FTutorialShortView TutorialServerData;
};
