// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "SMessageBox.h"
#include "SNotifyList.h"
#include "RichTextHelpers.h"
#include "TutorialBase.h"
#include "SResourceTextBoxType.h"
#include "GameSingleton.h"
#include "Currency/ItemCurrencyEntity.h"
#include "Models/ConfirmTutorialStepResponse.h"
#include "TutorialComponent.h"
#include "SResourceTextBox.h"
#include "UTGameViewportClient.h"
#include "QueueTutorialsData.h"
#include "SMessageTip.h"
#include "STutorialSelector.h"
#include "StringTableRegistry.h"

UTutorialBase::UTutorialBase()
	: Super()
{
	CurrentStage = 0;
	bIsStarted = false;
}

bool UTutorialBase::ForceStage_Implementation(const int32& InStage)
{
	if (bIsStarted && Stages.IsValidIndex(InStage))
	{
		CurrentStage = InStage;	
		auto TargetStage = Stages[CurrentStage];

		auto ThisWorld = GetWorld();		

		if (TargetStage.ShowAs == ETutorialInfoShowAs::Notify || TargetStage.ShowAs == ETutorialInfoShowAs::Message || TargetStage.ShowAs == ETutorialInfoShowAs::Tip)
		{
			FFormatNamedArguments FormatNamedArguments;
			FormatNamedArguments.Add(TEXT("PlayerName"), FRichHelpers::TextHelper_NotifyValue.SetValue(UGameSingleton::Get()->GetSavedPlayerEntityInfo().Login).ToText());
			FormatNamedArguments.Add(TEXT("PlayerMoney"), FRichHelpers::FResourceHelper().SetType(EResourceTextBoxType::Money).SetValue(UGameSingleton::Get()->GetSavedPlayerEntityInfo().TakeCash(EGameCurrency::Money)).ToText());
			FormatNamedArguments.Add(TEXT("PlayerDonate"), FRichHelpers::FResourceHelper().SetType(EResourceTextBoxType::Donate).SetValue(UGameSingleton::Get()->GetSavedPlayerEntityInfo().TakeCash(EGameCurrency::Donate)).ToText());
			FormatNamedArguments.Add(TEXT("PlayerCoin"), FRichHelpers::FResourceHelper().SetType(EResourceTextBoxType::Coin).SetValue(UGameSingleton::Get()->GetSavedPlayerEntityInfo().TakeCash(EGameCurrency::Coin)).ToText());

			if (TargetStage.ShowAs == ETutorialInfoShowAs::Notify)
			{
				FNotifyInfo NotifyInfo(FText::Format(TargetStage.Message, FormatNamedArguments), FText::Format(TargetStage.Title.IsEmpty() ? GetTutorialName() : TargetStage.Title, FormatNamedArguments));
				NotifyInfo.ShowTime = 40.0f;

				SNotifyList::Get()->AddNotify(NotifyInfo);
			}
			else if (TargetStage.ShowAs == ETutorialInfoShowAs::Message)
			{
				FName QueueName = *FString::Printf(TEXT("UTutorialBase_%d"), CurrentStage);
				SMessageBox::AddQueueMessage(QueueName, FOnClickedOutside::CreateLambda([stage = TargetStage, args = FormatNamedArguments, uName = GetTutorialName()]() {
					SMessageBox::Get()->SetHeaderText(FText::Format(stage.Title.IsEmpty() ? uName : stage.Title, args));
					SMessageBox::Get()->SetContent(FText::Format(stage.Message, args), stage.MessageJustification);
					SMessageBox::Get()->SetButtonsText(LOCTABLE("BaseStrings", "All.Continue"));
					SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](EMessageBoxButton) {
						SMessageBox::Get()->ToggleWidget(false);
					});
					SMessageBox::Get()->SetEnableClose(true);
					if (stage.Sounds[ETutorialInfoSound::Show].GetResourceObject())	SMessageBox::Get()->SetSound(stage.Sounds[ETutorialInfoSound::Show], true);
					if (stage.Sounds[ETutorialInfoSound::Hide].GetResourceObject())	SMessageBox::Get()->SetSound(stage.Sounds[ETutorialInfoSound::Hide], false);
					SMessageBox::Get()->ToggleWidget(true);
				}));
			}
			else if (TargetStage.ShowAs == ETutorialInfoShowAs::Tip)
			{
				FName QueueName = *FString::Printf(TEXT("UTutorialBase_%d"), CurrentStage);
				QueueBegin(SMessageTip, QueueName, stage = TargetStage, args = FormatNamedArguments)
					SMessageTip::Get()->SetTitle(FText::Format(stage.Title.IsEmpty() ? GetTutorialName() : stage.Title, args));
					SMessageTip::Get()->SetContent(FText::Format(stage.Message, args), stage.MessageJustification);
					SMessageTip::Get()->ToggleWidget(true);
					SMessageTip::Get()->OnHyperlink.BindLambda([s = stage](FSlateHyperlinkRun::FMetadata InData)
					{
						if (InData.Contains(TEXT("action")) && InData.FindChecked(TEXT("action")).Equals(TEXT("playsound"), ESearchCase::IgnoreCase))
						{
							if (s.Sounds[ETutorialInfoSound::Voice].GetResourceObject())
							{
								FSlateApplication::Get().PlaySound(s.Sounds[ETutorialInfoSound::Voice]);
							}
						}
					});
				QueueEnd
			}
		}

		if (TargetStage.Sounds[ETutorialInfoSound::Voice].GetResourceObject())
		{
			FSlateApplication::Get().PlaySound(TargetStage.Sounds[ETutorialInfoSound::Voice]);
		}

		auto MyViewport = GetGameViewportClient();
		if (TargetStage.WidgetsHighlightList.Num() && MyViewport)
		{
			for (auto MyTarget : TargetStage.WidgetsHighlightList)
			{
				MyViewport->HighlightWidgetByTag(MyTarget.Key, MyTarget.Value);
			}
		}

		if (TargetStage.WidgetsUnhighlightList.Num() && MyViewport)
		{
			for (auto MyTarget : TargetStage.WidgetsUnhighlightList)
			{
				MyViewport->UnHighlightWidgetByTag(MyTarget);
			}
		}

		if ((TargetStage.TargetAction == ETutorialAction::Skip || TargetStage.TargetAction == ETutorialAction::Time) && ThisWorld)
		{
			ThisWorld->GetTimerManager().SetTimer(timer_TutorialTime, this, &UTutorialBase::EndCurrentStage, TargetStage.TargetAction == ETutorialAction::Time ? TargetStage.CompleteSpeciferPrimary : 5.0f, false);
		}

		return true;
	}
	else if (bIsStarted && Stages.Num() <= InStage)
	{
		return EndTutorial();
	}

	return false;
}

bool UTutorialBase::StartTutorial_Implementation(const int32& InStage)
{
	if (bIsStarted == false)
	{
		bIsStarted = true;
		CurrentStage = InStage;
		ForceStage(CurrentStage);
		return true;
	}
	return false;
}

void UTutorialBase::EndCurrentStage_Implementation()
{
	FStartTutorialView step;
	step.TutorialId = TutorialIndex;
	step.StepId = CurrentStage;
	
	FName QueueName = *FString::Printf(TEXT("UTutorialBase_%d"), CurrentStage);
	if (SMessageTip::Get()->GetQueueId() == QueueName) SMessageTip::Get()->ToggleWidget(false);
	else SMessageTip::RemoveQueue(QueueName);

	if (step.StepId < Stages.Num() && TutorialComponent)
	{
		TutorialComponent->SendRequestTutorialStep(step);
	}
}

bool UTutorialBase::EndTutorial_Implementation()
{
	if (bIsStarted == true)
	{
		if (NextTutorials.Num() || UGameSingleton::Get()->GetDataAssets<UQueueTutorialsData>().Num())
		{
			auto MyTutorials = UGameSingleton::Get()->GetSavedPlayerEntityInfo().Tutorials;
			auto MyQueue = UGameSingleton::Get()->GetDataAssets<UQueueTutorialsData>()[0]->Queue;

			FTutorialSelectorData localNextTutorials;
			TArray<int32> CompletedTutorials;
			for (auto tut : MyTutorials)
			{
				CompletedTutorials.Add(tut.TutorialId);
			}

			if (NextTutorials.Num())
			{
				for (auto i : NextTutorials)
				{
					if (CompletedTutorials.Contains(i.Key) == false)
					{
						localNextTutorials.Add(i.Key, i.Value);
						if (localNextTutorials.Num() >= 2) break;
					}
				}
			}
			else
			{
				for (auto i : MyQueue)
				{
					if (CompletedTutorials.Contains(i) == false && UGameSingleton::Get()->GetTutorial(i))
					{
						localNextTutorials.Add(i, UGameSingleton::Get()->GetTutorial(i)->GetTutorialName());
						if (localNextTutorials.Num() >= 2) break;
					}
				}
			}

			if (localNextTutorials.Num())
			{
				QueueMessageBegin("NextTutorials", td = localNextTutorials)
					SMessageBox::Get()->SetHeaderText(NSLOCTEXT("UTutorialBase", "UTutorialBase.Next.Title", "Choice continue"));
					SMessageBox::Get()->SetContent(SNew(STutorialSelector).Data(td).OnSelected_Lambda([&](int32 InValue)
					{
						if (TutorialComponent)
						{
							TutorialComponent->SendRequestTutorialStart(InValue);
						}

						SMessageBox::Get()->ToggleWidget(false);
					}));
					SMessageBox::Get()->SetButtonsText();
					SMessageBox::Get()->SetEnableClose(false);
					SMessageBox::Get()->ToggleWidget(true);
				QueueMessageEnd
			}
		}

		bIsStarted = false;
		if (GetCurrentStage() < Stages.Num())
		{
			EndCurrentStage();
		}
		return true;
	}
	return false;
}

void UTutorialBase::ForceHide_Implementation()
{
	FName QueueName = *FString::Printf(TEXT("UTutorialBase_%d"), CurrentStage);
	if (SMessageTip::Get()->GetQueueId() == QueueName) SMessageTip::Get()->ToggleWidget(false);
	if (SMessageBox::Get()->GetQueueId() == QueueName) SMessageBox::Get()->ToggleWidget(false);
}

UWorld* UTutorialBase::GetWorld() const
{
	UWorld* ThisWorld = nullptr;
	if (GEngine)
	{
		auto Worlds = GEngine->GetWorldContexts();
		for (auto Context : Worlds)
		{
			if (Context.World() && (Context.WorldType == EWorldType::Game || Context.WorldType == EWorldType::PIE))
			{
				ThisWorld = Context.World();
				break;
			}
		}
	}
	return ThisWorld;
}

void UTutorialBase::OnConfirmCurrentStage(const FConfirmTutorialStepResponse& InResponse)
{
	if (bIsStarted == true)
	{		
		if (InResponse.RewardReceived && TutorialServerData.Steps.IsValidIndex(InResponse.StepId) && TutorialServerData.Steps[InResponse.StepId].RewardCategoryId != CategoryTypeId::End)
		{
			auto step = TutorialServerData.Steps[InResponse.StepId];
			UItemBaseEntity* TargetItem = UGameSingleton::Get()->FindItem(step.RewardModelId, step.RewardCategoryId);

			if (TargetItem)
			{
				FNotifyInfo NotifyInfo;
				NotifyInfo.Title = FText::Format(NSLOCTEXT("UTutorialBase", "UTutorialBase.Reward.Title", "Reward from \"{0}\""), Stages[InResponse.StepId].Title.IsEmpty() ? TutorialName : Stages[InResponse.StepId].Title);
				NotifyInfo.Content = FText::Format(NSLOCTEXT("UTutorialBase", "UTutorialBase.Reward.Desc", "Your reward \"{0}\" with amount of {1} from current tutorial")
									, FRichHelpers::TextHelper_NotifyValue.SetValue(TargetItem->GetItemName()).ToText(), FRichHelpers::TextHelper_NotifyValue.SetValue(FText::AsNumber(step.RewardGive)).ToText());
				NotifyInfo.Image = TargetItem->GetImage();
				NotifyInfo.ShowTime = 20.0f;

				SNotifyList::Get()->AddNotify(NotifyInfo);
			}
		}

		if (InResponse.TutorialComplete)
		{
			EndTutorial();
		}
		else
		{
			ForceStage(InResponse.NextStepId);
		}
	}
}