// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "LokaGame.h"
#include "UTMultiKillMessage.h"
#include "UTSpreeMessage.h"
#include "Net/UnrealNetwork.h"
#include "UTTimerMessage.h"

#include "UTPickup.h"

#include "UTPainVolume.h"
#include "StatNames.h"
#include "UTGameEngine.h"
#include "UTGameMode.h"
#include "UTGameInstance.h"
#include "UTWorldSettings.h"
#include "Engine/DemoNetDriver.h"
#include "UTKillcamPlayback.h"

#include "ContentStreaming.h"
#include "UTInGameIntroZone.h"
#include "UTInGameIntroHelper.h"
#include "Runtime/Analytics/Analytics/Public/AnalyticsEventAttribute.h"
#include "MatchStateNames.h"
#include "UTGameState.h"
#include "UTLocalPlayer.h"
#include "UTCharacter.h"
#include "UTGameUserSettings.h"

#include "Engine/NetworkObjectList.h"
#include "NodeComponent/NodeSessionMatch.h"


void AUTGameState::InitGameState(const FNodeSessionMatch& information)
{
	Super::InitGameState(information);

	//========================================================================================================
	InitTeams(information);
  
	//========================================================================================================
	SetGoalScore(information.Options.GetScoreLimit());
  
	//========================================================================================================
	UE_LOG(LogInit, Display, TEXT(">> AUTGameState::InitGameState | GoalScore: %d | Time: %d| CurrentGameMode: %d [%s]"), information.Options.GetScoreLimit(), information.Options.RoundTimeInSeconds, information.Options.GetGameMode().Value, *information.Options.GetGameMode().ToString());

	//========================================================================================================
}

void AUTGameState::InitTeams(const FNodeSessionMatch& information)
{
	//========================================================================================================
	UE_LOG(LogInit, Display, TEXT("AUTGameState::InitTeams | Count: %d"), information.TeamList.Num());

	//========================================================================================================
	for (const auto t : information.TeamList)
	{
		AUTTeamInfo* instance = GetWorld()->SpawnActor<AUTTeamInfo>();
		if (auto team = GetValidObject(instance))
		{
			team->TeamIndex = Teams.Num();
			if (FGuid::Parse(information.TeamList[team->TeamIndex], team->TeamId))
			{
				team->TeamName = AUTTeamInfo::TeamNames[team->TeamIndex];
				team->TeamColor = AUTTeamInfo::TeamColors[team->TeamIndex];
				team->Score = information.Options.GetScoreLimit();
				Teams.Add(team);

				UE_LOG(LogInit, Display, TEXT("AUTGameState::InitTeams | Team: %d[%s]"), information.TeamList.Num(), *team->TeamId.ToString());
			}
		}
	}

	//========================================================================================================
	if (auto gm = GetOnlineGameMode())
	{
		gm->Teams = Teams;
	}
}

AUTGameState::AUTGameState()
: Super()
{
	MultiKillMessageClass = UUTMultiKillMessage::StaticClass();
	SpreeMessageClass = UUTSpreeMessage::StaticClass();
	MultiKillDelay = 3.0f;
	SpawnProtectionTime = 2.f;
	bWeaponStay = true;
	bAllowTeamSwitches = true;
	bCasterControl = false;
	bForcedBalance = false;
	TauntSelectionIndex = 0;
	bPersistentKillIconMessages = false;
	bTeamProjHits = false;

	// We want to be ticked.
	PrimaryActorTick.bCanEverTick = true;

	SecondaryAttackerStat = NAME_None;
	GoalScoreText = NSLOCTEXT("UTScoreboard", "GoalScoreFormat", "First to {0} Frags");

	GameScoreStats.Add(NAME_AttackerScore);
	GameScoreStats.Add(NAME_DefenderScore);
	GameScoreStats.Add(NAME_SupporterScore);
	GameScoreStats.Add(NAME_Suicides);

	GameScoreStats.Add(NAME_UDamageTime);
	GameScoreStats.Add(NAME_BerserkTime);
	GameScoreStats.Add(NAME_InvisibilityTime);
	GameScoreStats.Add(NAME_UDamageCount);
	GameScoreStats.Add(NAME_BerserkCount);
	GameScoreStats.Add(NAME_InvisibilityCount);
	GameScoreStats.Add(NAME_KegCount);
	GameScoreStats.Add(NAME_BootJumps);
	GameScoreStats.Add(NAME_ShieldBeltCount);
	GameScoreStats.Add(NAME_ArmorVestCount);
	GameScoreStats.Add(NAME_ArmorPadsCount);
	GameScoreStats.Add(NAME_HelmetCount);

	TeamStats.Add(NAME_TeamKills);
	TeamStats.Add(NAME_UDamageTime);
	TeamStats.Add(NAME_BerserkTime);
	TeamStats.Add(NAME_InvisibilityTime);
	TeamStats.Add(NAME_UDamageCount);
	TeamStats.Add(NAME_BerserkCount);
	TeamStats.Add(NAME_InvisibilityCount);
	TeamStats.Add(NAME_KegCount);
	TeamStats.Add(NAME_BootJumps);
	TeamStats.Add(NAME_ShieldBeltCount);
	TeamStats.Add(NAME_ArmorVestCount);
	TeamStats.Add(NAME_ArmorPadsCount);
	TeamStats.Add(NAME_HelmetCount);

	WeaponStats.Add(NAME_ImpactHammerKills);
	WeaponStats.Add(NAME_EnforcerKills);
	WeaponStats.Add(NAME_BioRifleKills);
	WeaponStats.Add(NAME_BioLauncherKills);
	WeaponStats.Add(NAME_ShockBeamKills);
	WeaponStats.Add(NAME_ShockCoreKills);
	WeaponStats.Add(NAME_ShockComboKills);
	WeaponStats.Add(NAME_LinkKills);
	WeaponStats.Add(NAME_LinkBeamKills);
	WeaponStats.Add(NAME_MinigunKills);
	WeaponStats.Add(NAME_MinigunShardKills);
	WeaponStats.Add(NAME_FlakShardKills);
	WeaponStats.Add(NAME_FlakShellKills);
	WeaponStats.Add(NAME_RocketKills);
	WeaponStats.Add(NAME_SniperKills);
	WeaponStats.Add(NAME_SniperHeadshotKills);
	WeaponStats.Add(NAME_RedeemerKills);
	WeaponStats.Add(NAME_InstagibKills);
	WeaponStats.Add(NAME_TelefragKills);

	WeaponStats.Add(NAME_ImpactHammerDeaths);
	WeaponStats.Add(NAME_EnforcerDeaths);
	WeaponStats.Add(NAME_BioRifleDeaths);
	WeaponStats.Add(NAME_BioLauncherDeaths);
	WeaponStats.Add(NAME_ShockBeamDeaths);
	WeaponStats.Add(NAME_ShockCoreDeaths);
	WeaponStats.Add(NAME_ShockComboDeaths);
	WeaponStats.Add(NAME_LinkDeaths);
	WeaponStats.Add(NAME_LinkBeamDeaths);
	WeaponStats.Add(NAME_MinigunDeaths);
	WeaponStats.Add(NAME_MinigunShardDeaths);
	WeaponStats.Add(NAME_FlakShardDeaths);
	WeaponStats.Add(NAME_FlakShellDeaths);
	WeaponStats.Add(NAME_RocketDeaths);
	WeaponStats.Add(NAME_SniperDeaths);
	WeaponStats.Add(NAME_SniperHeadshotDeaths);
	WeaponStats.Add(NAME_RedeemerDeaths);
	WeaponStats.Add(NAME_InstagibDeaths);
	WeaponStats.Add(NAME_TelefragDeaths);

	WeaponStats.Add(NAME_BestShockCombo);
	WeaponStats.Add(NAME_AirRox);
	WeaponStats.Add(NAME_AmazingCombos);
	WeaponStats.Add(NAME_FlakShreds);
	WeaponStats.Add(NAME_AirSnot);

	RewardStats.Add(NAME_MultiKillLevel0);
	RewardStats.Add(NAME_MultiKillLevel1);
	RewardStats.Add(NAME_MultiKillLevel2);
	RewardStats.Add(NAME_MultiKillLevel3);

	RewardStats.Add(NAME_SpreeKillLevel0);
	RewardStats.Add(NAME_SpreeKillLevel1);
	RewardStats.Add(NAME_SpreeKillLevel2);
	RewardStats.Add(NAME_SpreeKillLevel3);
	RewardStats.Add(NAME_SpreeKillLevel4);

	MovementStats.Add(NAME_RunDist);
	MovementStats.Add(NAME_SprintDist);
	MovementStats.Add(NAME_InAirDist);
	MovementStats.Add(NAME_SwimDist);
	MovementStats.Add(NAME_TranslocDist);
	MovementStats.Add(NAME_NumDodges);
	MovementStats.Add(NAME_NumWallDodges);
	MovementStats.Add(NAME_NumJumps);
	MovementStats.Add(NAME_NumLiftJumps);
	MovementStats.Add(NAME_NumFloorSlides);
	MovementStats.Add(NAME_NumWallRuns);
	MovementStats.Add(NAME_NumImpactJumps);
	MovementStats.Add(NAME_NumRocketJumps);
	MovementStats.Add(NAME_SlideDist);
	MovementStats.Add(NAME_WallRunDist);

	WeaponStats.Add(NAME_EnforcerShots);
	WeaponStats.Add(NAME_BioRifleShots);
	WeaponStats.Add(NAME_BioLauncherShots);
	WeaponStats.Add(NAME_ShockRifleShots);
	WeaponStats.Add(NAME_LinkShots);
	WeaponStats.Add(NAME_MinigunShots);
	WeaponStats.Add(NAME_FlakShots);
	WeaponStats.Add(NAME_RocketShots);
	WeaponStats.Add(NAME_SniperShots);
	WeaponStats.Add(NAME_RedeemerShots);
	WeaponStats.Add(NAME_InstagibShots);

	WeaponStats.Add(NAME_EnforcerHits);
	WeaponStats.Add(NAME_BioRifleHits);
	WeaponStats.Add(NAME_BioLauncherHits);
	WeaponStats.Add(NAME_ShockRifleHits);
	WeaponStats.Add(NAME_LinkHits);
	WeaponStats.Add(NAME_MinigunHits);
	WeaponStats.Add(NAME_FlakHits);
	WeaponStats.Add(NAME_RocketHits);
	WeaponStats.Add(NAME_SniperHits);
	WeaponStats.Add(NAME_RedeemerHits);
	WeaponStats.Add(NAME_InstagibHits);

	HighlightMap.Add(HighlightNames::TopScorer, NSLOCTEXT("AUTGameMode", "HighlightTopScore", "Top Score overall with <UT.MatchSummary.HighlightText.Value>{0}</> points."));
	HighlightMap.Add(HighlightNames::TopScorerRed, NSLOCTEXT("AUTGameMode", "HighlightTopScoreRed", "Red Team Top Score with <UT.MatchSummary.HighlightText.Value>{0}</> points."));
	HighlightMap.Add(HighlightNames::TopScorerBlue, NSLOCTEXT("AUTGameMode", "HighlightTopScoreBlue", "Blue Team Top Score with <UT.MatchSummary.HighlightText.Value>{0}</> points."));
	HighlightMap.Add(HighlightNames::MostKills, NSLOCTEXT("AUTGameMode", "MostKills", "Most Kills with <UT.MatchSummary.HighlightText.Value>{0}</>."));
	HighlightMap.Add(HighlightNames::LeastDeaths, NSLOCTEXT("AUTGameMode", "LeastDeaths", "Least Deaths with <UT.MatchSummary.HighlightText.Value>{0}</>."));
	HighlightMap.Add(HighlightNames::BestKD, NSLOCTEXT("AUTGameMode", "BestKD", "Best Kill/Death ratio <UT.MatchSummary.HighlightText.Value>{0}</>."));
	HighlightMap.Add(HighlightNames::MostWeaponKills, NSLOCTEXT("AUTGameMode", "MostWeaponKills", "Most Kills (<UT.MatchSummary.HighlightText.Value>{0}</>) with <UT.MatchSummary.HighlightText.Value>{1}</>"));
	HighlightMap.Add(HighlightNames::BestCombo, NSLOCTEXT("AUTGameMode", "BestCombo", "Most Impressive Shock Combo."));
	HighlightMap.Add(HighlightNames::MostHeadShots, NSLOCTEXT("AUTGameMode", "MostHeadShots", "Most Headshots (<UT.MatchSummary.HighlightText.Value>{0}</>)."));
	HighlightMap.Add(HighlightNames::MostAirRockets, NSLOCTEXT("AUTGameMode", "MostAirRockets", "Most Air Rockets (<UT.MatchSummary.HighlightText.Value>{0}</>)."));
	HighlightMap.Add(HighlightNames::KillsAward, NSLOCTEXT("AUTGameMode", "KillsAward", "<UT.MatchSummary.HighlightText.Value>{0}</> Kills."));
	HighlightMap.Add(HighlightNames::DamageAward, NSLOCTEXT("AUTGameMode", "DamageAward", "<UT.MatchSummary.HighlightText.Value>{0}</> Damage Done."));
	HighlightMap.Add(HighlightNames::ParticipationAward, NSLOCTEXT("AUTGameMode", "ParticipationAward", "Participation Award."));

	HighlightMap.Add(NAME_AmazingCombos, NSLOCTEXT("AUTGameMode", "AmazingCombosHL", "Amazing Combos (<UT.MatchSummary.HighlightText.Value>{0}</>)."));
	HighlightMap.Add(NAME_SniperHeadshotKills, NSLOCTEXT("AUTGameMode", "SniperHeadshotKillsHL", "Headshot Kills (<UT.MatchSummary.HighlightText.Value>{0}</>)."));
	HighlightMap.Add(NAME_AirRox, NSLOCTEXT("AUTGameMode", "AirRoxHL", "Air Rocket Kills (<UT.MatchSummary.HighlightText.Value>{0}</>)."));
	HighlightMap.Add(NAME_FlakShreds, NSLOCTEXT("AUTGameMode", "FlakShredsHL", "Flak Shred Kills (<UT.MatchSummary.HighlightText.Value>{0}</>)."));
	HighlightMap.Add(NAME_AirSnot, NSLOCTEXT("AUTGameMode", "AirSnotHL", "Air Snot Kills (<UT.MatchSummary.HighlightText.Value>{0}</>)."));
	HighlightMap.Add(NAME_MultiKillLevel0, NSLOCTEXT("AUTGameMode", "MultiKillLevel0", "Double Kill (<UT.MatchSummary.HighlightText.Value>{0}</>)."));
	HighlightMap.Add(NAME_MultiKillLevel1, NSLOCTEXT("AUTGameMode", "MultiKillLevel1", "Multi Kill (<UT.MatchSummary.HighlightText.Value>{0}</>)."));
	HighlightMap.Add(NAME_MultiKillLevel2, NSLOCTEXT("AUTGameMode", "MultiKillLevel2", "Ultra Kill (<UT.MatchSummary.HighlightText.Value>{0}</>)."));
	HighlightMap.Add(NAME_MultiKillLevel3, NSLOCTEXT("AUTGameMode", "MultiKillLevel3", "Monster Kill (<UT.MatchSummary.HighlightText.Value>{0}</>)."));
	HighlightMap.Add(NAME_SpreeKillLevel0, NSLOCTEXT("AUTGameMode", "SpreeKillLevel0", "Killing Spree (<UT.MatchSummary.HighlightText.Value>{0}</>)."));
	HighlightMap.Add(NAME_SpreeKillLevel1, NSLOCTEXT("AUTGameMode", "SpreeKillLevel1", "Rampage Spree (<UT.MatchSummary.HighlightText.Value>{0}</>)."));
	HighlightMap.Add(NAME_SpreeKillLevel2, NSLOCTEXT("AUTGameMode", "SpreeKillLevel2", "Dominating Spree (<UT.MatchSummary.HighlightText.Value>{0}</>)."));
	HighlightMap.Add(NAME_SpreeKillLevel3, NSLOCTEXT("AUTGameMode", "SpreeKillLevel3", "Unstoppable Spree (<UT.MatchSummary.HighlightText.Value>{0}</>)."));
	HighlightMap.Add(NAME_SpreeKillLevel4, NSLOCTEXT("AUTGameMode", "SpreeKillLevel4", "Godlike Spree (<UT.MatchSummary.HighlightText.Value>{0}</>)."));

	ShortHighlightMap.Add(HighlightNames::TopScorer, NSLOCTEXT("AUTGameMode", "ShortHighlightTopScore", "Top Score overall"));
	ShortHighlightMap.Add(HighlightNames::TopScorerRed, NSLOCTEXT("AUTGameMode", "ShortHighlightTopScoreRed", "Red Team Top Score"));
	ShortHighlightMap.Add(HighlightNames::TopScorerBlue, NSLOCTEXT("AUTGameMode", "ShortHighlightTopScoreBlue", "Blue Team Top Score"));
	ShortHighlightMap.Add(HighlightNames::MostKills, NSLOCTEXT("AUTGameMode", "ShortMostKills", "Most Kills"));
	ShortHighlightMap.Add(HighlightNames::LeastDeaths, NSLOCTEXT("AUTGameMode", "ShortLeastDeaths", "Least Deaths"));
	ShortHighlightMap.Add(HighlightNames::BestKD, NSLOCTEXT("AUTGameMode", "ShortBestKD", "Best Kill/Death ratio "));
	ShortHighlightMap.Add(HighlightNames::MostWeaponKills, NSLOCTEXT("AUTGameMode", "ShortMostWeaponKills", "Most Kills with {1}"));
	ShortHighlightMap.Add(HighlightNames::BestCombo, NSLOCTEXT("AUTGameMode", "ShortBestCombo", "Most Impressive Shock Combo."));
	ShortHighlightMap.Add(HighlightNames::MostHeadShots, NSLOCTEXT("AUTGameMode", "ShortMostHeadShots", "Most Headshots"));
	ShortHighlightMap.Add(HighlightNames::MostAirRockets, NSLOCTEXT("AUTGameMode", "ShortMostAirRockets", "Most Air Rockets"));
	ShortHighlightMap.Add(HighlightNames::ParticipationAward, NSLOCTEXT("AUTGameMode", "ShortParticipationAward", "Participated"));

	ShortHighlightMap.Add(NAME_AmazingCombos, NSLOCTEXT("AUTGameMode", "ShortAmazingCombos", "Amazing Combos"));
	ShortHighlightMap.Add(NAME_SniperHeadshotKills, NSLOCTEXT("AUTGameMode", "ShortSniperHeadshotKills", "{0} Headshot Kills"));
	ShortHighlightMap.Add(NAME_AirRox, NSLOCTEXT("AUTGameMode", "ShortAirRox", "{0}Air Rocket Kills"));
	ShortHighlightMap.Add(NAME_FlakShreds, NSLOCTEXT("AUTGameMode", "ShortFlakShreds", "{0} Flak Shred Kills"));
	ShortHighlightMap.Add(NAME_AirSnot, NSLOCTEXT("AUTGameMode", "ShortAirSnot", "{0} Air Snot Kills"));
	ShortHighlightMap.Add(NAME_MultiKillLevel0, NSLOCTEXT("AUTGameMode", "ShortMultiKillLevel0", "Double Kill"));
	ShortHighlightMap.Add(NAME_MultiKillLevel1, NSLOCTEXT("AUTGameMode", "ShortMultiKillLevel1", "Multi Kill"));
	ShortHighlightMap.Add(NAME_MultiKillLevel2, NSLOCTEXT("AUTGameMode", "ShortMultiKillLevel2", "Ultra Kill"));
	ShortHighlightMap.Add(NAME_MultiKillLevel3, NSLOCTEXT("AUTGameMode", "ShortMultiKillLevel3", "Monster Kill"));
	ShortHighlightMap.Add(NAME_SpreeKillLevel0, NSLOCTEXT("AUTGameMode", "ShortSpreeKillLevel0", "Killing Spree"));
	ShortHighlightMap.Add(NAME_SpreeKillLevel1, NSLOCTEXT("AUTGameMode", "ShortSpreeKillLevel1", "Rampage Spree"));
	ShortHighlightMap.Add(NAME_SpreeKillLevel2, NSLOCTEXT("AUTGameMode", "ShortSpreeKillLevel2", "Dominating Spree"));
	ShortHighlightMap.Add(NAME_SpreeKillLevel3, NSLOCTEXT("AUTGameMode", "ShortSpreeKillLevel3", "Unstoppable Spree"));
	ShortHighlightMap.Add(NAME_SpreeKillLevel4, NSLOCTEXT("AUTGameMode", "ShortSpreeKillLevel4", "Godlike Spree"));
	ShortHighlightMap.Add(HighlightNames::KillsAward, NSLOCTEXT("AUTGameMode", "ShortKillsAward", "{0} Kills"));
	ShortHighlightMap.Add(HighlightNames::DamageAward, NSLOCTEXT("AUTGameMode", "ShortDamageAward", "{0} Damage Done"));

	HighlightPriority.Add(HighlightNames::TopScorer, 10.f);
	HighlightPriority.Add(HighlightNames::TopScorerRed, 5.f);
	HighlightPriority.Add(HighlightNames::TopScorerBlue, 5.f);
	HighlightPriority.Add(HighlightNames::MostKills, 3.3f);
	HighlightPriority.Add(HighlightNames::LeastDeaths, 1.f);
	HighlightPriority.Add(HighlightNames::BestKD, 2.f);
	HighlightPriority.Add(HighlightNames::MostWeaponKills, 2.f);
	HighlightPriority.Add(HighlightNames::BestCombo, 2.f);
	HighlightPriority.Add(HighlightNames::MostHeadShots, 2.f);
	HighlightPriority.Add(HighlightNames::MostAirRockets, 2.f);

	HighlightPriority.Add(NAME_AmazingCombos, 1.f);
	HighlightPriority.Add(NAME_SniperHeadshotKills, 1.f);
	HighlightPriority.Add(NAME_AirRox, 1.f);
	HighlightPriority.Add(NAME_FlakShreds, 1.f);
	HighlightPriority.Add(NAME_AirSnot, 1.f);
	HighlightPriority.Add(NAME_MultiKillLevel0, 0.5f);
	HighlightPriority.Add(NAME_MultiKillLevel1, 0.5f);
	HighlightPriority.Add(NAME_MultiKillLevel2, 1.5f);
	HighlightPriority.Add(NAME_MultiKillLevel3, 2.5f);
	HighlightPriority.Add(NAME_SpreeKillLevel0, 2.f);
	HighlightPriority.Add(NAME_SpreeKillLevel1, 2.5f);
	HighlightPriority.Add(NAME_SpreeKillLevel2, 3.f);
	HighlightPriority.Add(NAME_SpreeKillLevel3, 3.5f);
	HighlightPriority.Add(NAME_SpreeKillLevel4, 4.f);
	HighlightPriority.Add(HighlightNames::KillsAward, 0.2f);
	HighlightPriority.Add(HighlightNames::DamageAward, 0.15f);
	HighlightPriority.Add(HighlightNames::ParticipationAward, 0.1f);

	GameOverStatus = NSLOCTEXT("UTGameState", "PostGame", "Game Over");
	MapVoteStatus = NSLOCTEXT("UTGameState", "Mapvote", "Map Vote");
	PreGameStatus = NSLOCTEXT("UTGameState", "PreGame", "Pre-Game");
	NeedPlayersStatus = NSLOCTEXT("UTGameState", "NeedPlayers", "Need {NumNeeded} More");
	OvertimeStatus = NSLOCTEXT("UTCTFGameState", "Overtime", "Overtime!");

	bWeightedCharacter = false;

	BoostRechargeMaxCharges = 1;
	BoostRechargeRateAlive = 1.0f;
	BoostRechargeRateDead = 2.0f;
	BoostRechargeTime = 0.0f; // off by default
	MusicVolume = 1.f;

	// more stringent by default
	UnplayableHitchThresholdInMs = 300;
	MaxUnplayableHitchesToTolerate = 1;
	bPlayStatusAnnouncements = false;
}

void AUTGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	
	DOREPLIFETIME(AUTGameState, RemainingTime);
	DOREPLIFETIME(AUTGameState, WinnerPlayerState);
	DOREPLIFETIME(AUTGameState, WinningTeam);
	DOREPLIFETIME(AUTGameState, bStopGameClock);
	DOREPLIFETIME(AUTGameState, TimeLimit);  
	DOREPLIFETIME(AUTGameState, RespawnWaitTime);  
	DOREPLIFETIME_CONDITION(AUTGameState, ForceRespawnTime, COND_InitialOnly);  
	DOREPLIFETIME_CONDITION(AUTGameState, bTeamGame, COND_InitialOnly);
	DOREPLIFETIME_CONDITION(AUTGameState, bRankedSession, COND_InitialOnly);
	DOREPLIFETIME(AUTGameState, TeamSwapSidesOffset);
	DOREPLIFETIME(AUTGameState, PlayersNeeded);  

	DOREPLIFETIME_CONDITION(AUTGameState, bAllowTeamSwitches, COND_InitialOnly);
	DOREPLIFETIME_CONDITION(AUTGameState, bWeaponStay, COND_InitialOnly);
	DOREPLIFETIME_CONDITION(AUTGameState, GoalScore, COND_InitialOnly);
	DOREPLIFETIME_CONDITION(AUTGameState, OverlayEffects, COND_InitialOnly);
	DOREPLIFETIME_CONDITION(AUTGameState, OverlayEffects1P, COND_InitialOnly);
	DOREPLIFETIME_CONDITION(AUTGameState, SpawnProtectionTime, COND_InitialOnly);
	DOREPLIFETIME_CONDITION(AUTGameState, NumTeams, COND_InitialOnly);


	DOREPLIFETIME(AUTGameState, NumWinnersToShow);


	DOREPLIFETIME_CONDITION(AUTGameState, bCasterControl, COND_InitialOnly);
	DOREPLIFETIME_CONDITION(AUTGameState, bPlayPlayerIntro, COND_InitialOnly);
	DOREPLIFETIME(AUTGameState, bForcedBalance);

	DOREPLIFETIME_CONDITION(AUTGameState, bWeightedCharacter, COND_InitialOnly);

	DOREPLIFETIME_CONDITION(AUTGameState, BoostRechargeTime, COND_InitialOnly);
	DOREPLIFETIME_CONDITION(AUTGameState, BoostRechargeMaxCharges, COND_InitialOnly);
	DOREPLIFETIME_CONDITION(AUTGameState, BoostRechargeRateAlive, COND_InitialOnly);
	DOREPLIFETIME_CONDITION(AUTGameState, BoostRechargeRateDead, COND_InitialOnly);

	DOREPLIFETIME(AUTGameState, bTeamProjHits);
}

void AUTGameState::PreReplication(IRepChangedPropertyTracker& ChangedPropertyTracker)
{
	NumTeams = Teams.Num();
	Super::PreReplication(ChangedPropertyTracker);
}

void AUTGameState::AddOverlayMaterial(UMaterialInterface* NewOverlay, UMaterialInterface* NewOverlay1P)
{
	AddOverlayEffect(FOverlayEffect(NewOverlay), FOverlayEffect(NewOverlay1P));
}

void AUTGameState::AddOverlayEffect(const FOverlayEffect& NewOverlay, const FOverlayEffect& NewOverlay1P)
{
	if (NewOverlay.IsValid() && Role == ROLE_Authority)
	{
		if (auto info = FindNetworkObjectInfo())
		{
			if (info->NextUpdateTime > 0.0f)
			{
				UE_LOG(UT, Warning, TEXT("UTGameState::AddOverlayMaterial() called after startup; may not take effect on clients"));
			}
		}

		for (int32 i = 0; i < ARRAY_COUNT(OverlayEffects); i++)
		{
			if (OverlayEffects[i] == NewOverlay)
			{
				OverlayEffects1P[i] = NewOverlay1P;
				return;
			}
			else if (!OverlayEffects[i].IsValid())
			{
				OverlayEffects[i] = NewOverlay;
				OverlayEffects1P[i] = NewOverlay1P;
				return;
			}
		}
		UE_LOG(UT, Warning, TEXT("UTGameState::AddOverlayMaterial(): Ran out of slots, couldn't add %s"), *NewOverlay.ToString());
	}
}

void AUTGameState::OnRep_OverlayEffects()
{
	for (FConstPawnIterator It = GetWorld()->GetPawnIterator(); It; ++It)
	{
		AUTCharacter* UTC = Cast<AUTCharacter>(It->Get());
		if (UTC != nullptr)
		{
			UTC->UpdateCharOverlays();
			UTC->UpdateWeaponOverlays();
		}
	}
}

void AUTGameState::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//AUTWorldSettings* Settings = Cast<AUTWorldSettings>(GetWorldSettings());
	//if (Settings && Settings->MusicComp)
	//{
	//	UUTGameUserSettings* UserSettings = Cast<UUTGameUserSettings>(GEngine->GetGameUserSettings()); 
	//	float DesiredVolume = IsMatchInProgress() && !IsMatchIntermission() ? UserSettings->GetSoundClassVolume(EUTSoundClass::GameMusic) : 1.f;
	//	MusicVolume = MusicVolume * (1.f - 0.5f*DeltaTime) + 0.5f*DeltaTime*DesiredVolume;
	//	Settings->MusicComp->SetVolumeMultiplier(MusicVolume);
	//}
}

void AUTGameState::BeginPlay()
{
	Super::BeginPlay();

	// HACK: temporary hack around config property replication bug; force to be different from defaults and clamp their sizes so they won't break networking



	if (GetNetMode() == NM_Client)
	{
		// hook up any TeamInfos that were received prior
		for (FActorIterator It(GetWorld()); It; ++It)
		{
			AUTTeamInfo* Team = Cast<AUTTeamInfo>(*It);
			if (Team != nullptr)
			{
				Team->ReceivedTeamIndex();
			}
		}
	}
	else
	{
		TArray<UObject*> AllInventory;
		GetObjectsOfClass(AUTInventory::StaticClass(), AllInventory, true, RF_NoFlags);
		for (int32 i = 0; i < AllInventory.Num(); i++)
		{
			if (AllInventory[i]->HasAnyFlags(RF_ClassDefaultObject))
			{
				checkSlow(AllInventory[i]->IsA(AUTInventory::StaticClass()));
				((AUTInventory*)AllInventory[i])->AddOverlayMaterials(this);
			}
		}

		TArray<UObject*> AllEffectVolumes;
		GetObjectsOfClass(AUTPainVolume::StaticClass(), AllEffectVolumes, true, RF_NoFlags);
		for (int32 i = 0; i < AllEffectVolumes.Num(); i++)
		{
			checkSlow(AllEffectVolumes[i]->IsA(AUTPainVolume::StaticClass()));
			((AUTPainVolume*)AllEffectVolumes[i])->AddOverlayMaterials(this);
		}

		//Register any overlays on the ActivatedPlaceholderClass
		AUTGameMode* GameMode = GetWorld()->GetAuthGameMode<AUTGameMode>();
		if (GameMode)
		{
			if (GameMode->GetActivatedPowerupPlaceholderClass())
			{
				GameMode->GetActivatedPowerupPlaceholderClass().GetDefaultObject()->AddOverlayMaterials(this);
			}
		}
	}
}


float AUTGameState::GetRespawnWaitTimeFor(AUTPlayerState* PS)
{
	return (PS != nullptr) ? FMath::Max(RespawnWaitTime, PS->RespawnWaitTime) : RespawnWaitTime;
}

void AUTGameState::SetRespawnWaitTime(float NewWaitTime)
{
	RespawnWaitTime = NewWaitTime;
}

//By default return nullptr, this should be overriden in other game modes.
TSubclassOf<class AUTInventory> AUTGameState::GetSelectableBoostByIndex(AUTPlayerState* PlayerState, int Index) const
{
	return nullptr;
}

//By default return false, this should be overriden in other game modes.
bool AUTGameState::IsSelectedBoostValid(AUTPlayerState* PlayerState) const
{
	return false;
}

void AUTGameState::SetRemainingTime(int32 NewRemainingTime)
{
	RemainingTime = NewRemainingTime;
}

void AUTGameState::DefaultTimer()
{
	Super::DefaultTimer();

	if (IsMatchInProgress())
	{
		for (int32 i = 0; i < PlayerArray.Num(); i++)
		{
			AUTPlayerState* PS = Cast<AUTPlayerState>(PlayerArray[i]);
			if (PS)
			{
				PS->ElapsedTime++;
			}
		}
	}

	if (GetWorld()->GetNetMode() == NM_Client)
	{
		// might have been deferred while waiting for teams to replicate
		if (TeamSwapSidesOffset != PrevTeamSwapSidesOffset)
		{
			OnTeamSideSwap();
		}
	}
}


bool AUTGameState::OnSameTeam(const AActor* Actor1, const AActor* Actor2)
{
	const IUTTeamInterface* TeamInterface1 = Cast<IUTTeamInterface>(Actor1);
	const IUTTeamInterface* TeamInterface2 = Cast<IUTTeamInterface>(Actor2);
	if (TeamInterface1 == nullptr || TeamInterface2 == nullptr)
	{
		return false;
	}
	else if (TeamInterface1->IsFriendlyToAll() || TeamInterface2->IsFriendlyToAll())
	{
		return true;
	}
	else
	{
		uint8 localTeamNum1 = TeamInterface1->GetTeamNum();
		uint8 localTeamNum2 = TeamInterface2->GetTeamNum();

		if (localTeamNum1 == IUTTeamInterface::NONE_TEAM_ID || localTeamNum2 == IUTTeamInterface::NONE_TEAM_ID)
		{
			return (Actor1 == Actor2);
		}
		else
		{
			return localTeamNum1 == localTeamNum2;
		}
	}
}

void AUTGameState::ChangeTeamSides(uint8 Offset)
{
	TeamSwapSidesOffset += Offset;
	OnTeamSideSwap();
}

void AUTGameState::OnTeamSideSwap()
{
	if (TeamSwapSidesOffset != PrevTeamSwapSidesOffset && Teams.Num() > 0 && (Role == ROLE_Authority || Teams.Num() == NumTeams))
	{
		uint8 TotalOffset;
		if (TeamSwapSidesOffset < PrevTeamSwapSidesOffset)
		{
			// rollover
			TotalOffset = uint8(uint32(TeamSwapSidesOffset + 255) - uint32(PrevTeamSwapSidesOffset));
		}
		else
		{
			TotalOffset = TeamSwapSidesOffset - PrevTeamSwapSidesOffset;
		}
		for (FActorIterator It(GetWorld()); It; ++It)
		{
			IUTTeamInterface* TeamObj = Cast<IUTTeamInterface>(*It);
			if (TeamObj != nullptr)
			{
				uint8 Team = TeamObj->GetTeamNum();
				if (Team != 255)
				{
					TeamObj->Execute_SetTeamForSideSwap(*It, (Team + TotalOffset) % Teams.Num());
				}
			}
			// check for script interface
			else if (It->GetClass()->ImplementsInterface(UUTTeamInterface::StaticClass()))
			{
				// a little hackery to ignore if relevant functions haven't been implemented
				static FName NAME_ScriptGetTeamNum(TEXT("ScriptGetTeamNum"));
				UFunction* GetTeamNumFunc = It->GetClass()->FindFunctionByName(NAME_ScriptGetTeamNum);
				if (GetTeamNumFunc != nullptr && GetTeamNumFunc->Script.Num() > 0)
				{
					uint8 Team = IUTTeamInterface::Execute_ScriptGetTeamNum(*It);
					if (Team != 255)
					{
						IUTTeamInterface::Execute_SetTeamForSideSwap(*It, (Team + TotalOffset) % Teams.Num());
					}
				}
				else
				{
					static FName NAME_SetTeamForSideSwap(TEXT("SetTeamForSideSwap"));
					UFunction* SwapFunc = It->GetClass()->FindFunctionByName(NAME_SetTeamForSideSwap);
					if (SwapFunc != nullptr && SwapFunc->Script.Num() > 0)
					{
						UE_LOG(UT, Warning, TEXT("Unable to execute SetTeamForSideSwap() for %s because GetTeamNum() must also be implemented"), *It->GetName());
					}
				}
			}
		}
		if (Role == ROLE_Authority)
		{
			// re-initialize all AI squads, in case objectives have changed sides
			for (AUTTeamInfo* Team : Teams)
			{
				Team->ReinitSquads();
			}
		}

		TeamSideSwapDelegate.Broadcast(TotalOffset);
		PrevTeamSwapSidesOffset = TeamSwapSidesOffset;
	}
}

void AUTGameState::SetTimeLimit(const int32& NewTimeLimit)
{
	TimeLimit = NewTimeLimit;
	RemainingTime = TimeLimit;

	ForceNetUpdate();
}

void AUTGameState::SetGoalScore(const int32& NewGoalScore)
{
	GoalScore = NewGoalScore;
	ForceNetUpdate();
}

void AUTGameState::SetWinner(AUTPlayerState* NewWinner)
{
	WinnerPlayerState = NewWinner;
	WinningTeam	= NewWinner != nullptr ?  NewWinner->Team : 0;
	ForceNetUpdate();
}

/** Returns true if P1 should be sorted before P2.  */
bool AUTGameState::InOrder( AUTPlayerState* P1, AUTPlayerState* P2 )
{
	// spectators are sorted last
    if( P1->bOnlySpectator )
    {
		return P2->bOnlySpectator;
    }
    else if ( P2->bOnlySpectator )
	{
		return true;
	}

	// sort by Score
    if( P1->Score < P2->Score )
	{
		return false;
	}
    if( P1->Score == P2->Score )
    {
		// if score tied, use deaths to sort
		if ( P1->Deaths > P2->Deaths )
			return false;

		// keep local player highest on list
		if ( (P1->Deaths == P2->Deaths) && (Cast<APlayerController>(P2->GetOwner()) != nullptr) )
		{
			ULocalPlayer* LP2 = Cast<ULocalPlayer>(Cast<APlayerController>(P2->GetOwner())->Player);
			if ( LP2 != nullptr )
			{
				// make sure ordering is consistent for splitscreen players
				ULocalPlayer* LP1 = Cast<ULocalPlayer>(Cast<APlayerController>(P2->GetOwner())->Player);
				return ( LP1 != nullptr );
			}
		}
	}
    return true;
}

/** Sort the PRI Array based on InOrder() prioritization.  */
void AUTGameState::SortPRIArray()
{
	for (int32 i=0; i<PlayerArray.Num()-1; i++)
	{
		AUTPlayerState* P1 = Cast<AUTPlayerState>(PlayerArray[i]);
		for (int32 j=i+1; j<PlayerArray.Num(); j++)
		{
			AUTPlayerState* P2 = Cast<AUTPlayerState>(PlayerArray[j]);
			if( !InOrder( P1, P2 ) )
			{
				PlayerArray[i] = P2;
				PlayerArray[j] = P1;
				P1 = P2;
			}
		}
	}
}

void AUTGameState::HandleMatchHasStarted()
{
	Super::HandleMatchHasStarted();

	AUTWorldSettings* WS = Cast<AUTWorldSettings>(GetWorld()->GetWorldSettings());
	if (WS != nullptr)
	{
		IUTResetInterface::Execute_Reset(WS);
	}
}

void AUTGameState::HandleMatchHasEnded()
{
	MatchEndTime = GetWorld()->TimeSeconds;
	Super::HandleMatchHasEnded();
}

void AUTGameState::OnHitchDetected(float DurationInSeconds)
{
	const float DurationInMs = DurationInSeconds * 1000.0f;
	if (IsRunningDedicatedServer())
	{
		if (DurationInMs >= UnplayableHitchThresholdInMs)
		{
			++UnplayableHitchesDetected;
			UnplayableTimeInMs += DurationInMs;

			if (UnplayableHitchesDetected > MaxUnplayableHitchesToTolerate)
			{
				// reset the counter, in case unplayable condition is going to be resolved by outside tools
				UnplayableHitchesDetected = 0;
				UnplayableTimeInMs = 0.0;
			}
		}
	}
}


void AUTGameState::OnWinnerReceived()
{
}

FName AUTGameState::OverrideCameraStyle(APlayerController* PCOwner, FName CurrentCameraStyle)
{
	if (HasMatchEnded())
	{
		return FName(TEXT("FreeCam"));
	}
	else
	{
		return CurrentCameraStyle;
	}
}

void AUTGameState::ReceivedGameModeClass()
{
	Super::ReceivedGameModeClass();

	TSubclassOf<AUTGameMode> UTGameClass(*GameModeClass);
	if (UTGameClass != nullptr)
	{
		// precache announcements
		for (FLocalPlayerIterator It(GEngine, GetWorld()); It; ++It)
		{
			AUTPlayerController* UTPC = Cast<AUTPlayerController>(It->PlayerController);
			if (UTPC != nullptr && UTPC->Announcer != nullptr)
			{
				UTGameClass.GetDefaultObject()->PrecacheAnnouncements(UTPC->Announcer);
			}
		}
	}

	UWorld* const World = GetWorld();
	UGameInstance* const GameInstance = GetGameInstance();

	// Don't record for killcam if this world is already playing back a replay.
	const UDemoNetDriver* const DemoDriver = World ? World->DemoNetDriver : nullptr;
	const bool bIsPlayingReplay = DemoDriver ? DemoDriver->IsPlaying() : false;
	if (!bIsPlayingReplay && GameInstance != nullptr && World->GetNetMode() == NM_Client && CVarUTEnableInstantReplay->GetInt() == 1)
	{
		// Since the killcam world will also have ReceivedGameModeClass() called in it, detect that and
		// don't try to start recording again. Killcam world contexts will have a valid PIEInstance for now.
		// Revisit when killcam is supported in PIE.
		FWorldContext* const Context = GEngine->GetWorldContextFromWorld(World);
		if (Context == nullptr || Context->PIEInstance != INDEX_NONE || Context->WorldType == EWorldType::PIE)
		{
			return;
		}

		const TCHAR* KillcamReplayName = TEXT("_DeathCam");

		// Start recording the replay for killcam, always using the in memory streamer.
		TArray<FString> AdditionalOptions;
		AdditionalOptions.Add(TEXT("ReplayStreamerOverride=InMemoryNetworkReplayStreaming"));
		//GameInstance->StartRecordingReplay(KillcamReplayName, KillcamReplayName, AdditionalOptions);

		// Start playback for each local player. Since we're using the in-memory streamer, the replay will
		// be available immediately.
		for (auto It = GameInstance->GetLocalPlayerIterator(); It; ++It)
		{
			UUTLocalPlayer* LocalPlayer = Cast<UUTLocalPlayer>(*It);
			if (LocalPlayer != nullptr && LocalPlayer->GetKillcamPlaybackManager() != nullptr)
			{
				if (LocalPlayer->GetKillcamPlaybackManager()->GetKillcamWorld() != World)
				{
					LocalPlayer->GetKillcamPlaybackManager()->CreateKillcamWorld(World->URL, *Context);
					LocalPlayer->GetKillcamPlaybackManager()->PlayKillcamReplay(KillcamReplayName);
				}
			}
		}
	}
}

void AUTGameState::OnRep_MatchState()
{
	Super::OnRep_MatchState();

	if (!InGameIntroHelper)
	{
		InGameIntroHelper = NewObject <UUTInGameIntroHelper>();
	}

	for (FLocalPlayerIterator It(GEngine, GetWorld()); It; ++It)
	{
		if (It->PlayerController != nullptr)
		{
			AUTHUD* Hud = Cast<AUTHUD>(It->PlayerController->MyHUD);
			if (Hud != nullptr)
			{
				Hud->NotifyMatchStateChange();
			}
		}
	}
	if ((Role < ROLE_Authority) && (GetMatchState() == MatchState::PlayerIntro))
	{
		// destroy torn off pawns
		TArray<APawn*> PawnsToDestroy;
		for (FConstPawnIterator It = GetWorld()->GetPawnIterator(); It; ++It)
		{
			if (It->Get() && !Cast<ASpectatorPawn>(It->Get()))
			{
				PawnsToDestroy.Add(It->Get());
			}
		}

		for (int32 i = 0; i<PawnsToDestroy.Num(); i++)
		{
			APawn* Pawn = PawnsToDestroy[i];
			if (Pawn != nullptr && !Pawn->IsPendingKill() && Pawn->GetTearOff())
			{
				Pawn->Destroy();
			}
		}
	}
}



void AUTGameState::AddPlayerState(APlayerState* PlayerState)
{
	// assign spectating ID to this player
	AUTPlayerState* PS = Cast<AUTPlayerState>(PlayerState);
	// NOTE: in the case of a rejoining player, this function gets called for both the original and new PLayerStates
	//		we will migrate the initially selected ID to avoid unnecessary ID shuffling
	//		and thus need to check here if that has happened and avoid assigning a new one
	if (PS != nullptr && !PS->bOnlySpectator && PS->SpectatingID == 0)
	{
		TArray<APlayerState*> PlayerArrayCopy = PlayerArray;
		PlayerArrayCopy.Sort([](const APlayerState& A, const APlayerState& B) -> bool
		{
			if (Cast<AUTPlayerState>(&A) == nullptr)
			{
				return false;
			}
			else if (Cast<AUTPlayerState>(&B) == nullptr)
			{
				return true;
			}
			else
			{
				return ((AUTPlayerState*)&A)->SpectatingID < ((AUTPlayerState*)&B)->SpectatingID;
			}
		});
		// find first gap in assignments from player leaving, give it to this player
		// if none found, assign PlayerArray.Num() + 1
		bool bFound = false;
		for (int32 i = 0; i < PlayerArrayCopy.Num(); i++)
		{
			AUTPlayerState* OtherPS = Cast<AUTPlayerState>(PlayerArrayCopy[i]);
			if (OtherPS == nullptr || OtherPS->bOnlySpectator || OtherPS->SpectatingID != uint8(i + 1))
			{
				PS->SpectatingID = uint8(i + 1);
				bFound = true;
				break;
			}
		}
		if (!bFound)
		{
			PS->SpectatingID = uint8(PlayerArrayCopy.Num() + 1);
		}
	}

	Super::AddPlayerState(PlayerState);
}

void AUTGameState::CompactSpectatingIDs()
{
	if (Role == ROLE_Authority)
	{
		// get sorted list of UTPlayerStates that have been assigned an ID
		TArray<AUTPlayerState*> PlayerArrayCopy;
		for (APlayerState* PS : PlayerArray)
		{
			AUTPlayerState* UTPS = Cast<AUTPlayerState>(PS);
			if (UTPS != nullptr && UTPS->SpectatingID > 0 && !UTPS->bOnlySpectator)
			{
				PlayerArrayCopy.Add(UTPS);
			}
		}
		PlayerArrayCopy.Sort([](const AUTPlayerState& A, const AUTPlayerState& B) -> bool
		{
			return A.SpectatingID < B.SpectatingID;
		});

		// fill in gaps from IDs at the end of the list
		for (int32 i = 0; i < PlayerArrayCopy.Num(); i++)
		{
			if (PlayerArrayCopy[i]->SpectatingID != uint8(i + 1))
			{
				AUTPlayerState* MovedPS = PlayerArrayCopy.Pop(false);
				MovedPS->SpectatingID = uint8(i + 1);
				PlayerArrayCopy.Insert(MovedPS, i);
			}
		}

		// now do the same for SpectatingIDTeam
		for (AUTTeamInfo* Team : Teams)
		{
			PlayerArrayCopy.Reset();
			const TArray<AController*> Members = Team->GetTeamMembers();
			for (AController* C : Members)
			{
				if (C)
				{
					AUTPlayerState* UTPS = C->GetPlayerState<AUTPlayerState>();
					if (UTPS != nullptr && UTPS->SpectatingIDTeam)
					{
						PlayerArrayCopy.Add(UTPS);
					}
				}
			}
			PlayerArrayCopy.Sort([](const AUTPlayerState& A, const AUTPlayerState& B) -> bool
			{
				return A.SpectatingIDTeam < B.SpectatingIDTeam;
			});

			for (int32 i = 0; i < PlayerArrayCopy.Num(); i++)
			{
				if (PlayerArrayCopy[i]->SpectatingIDTeam != uint8(i + 1))
				{
					AUTPlayerState* MovedPS = PlayerArrayCopy.Pop(false);
					MovedPS->SpectatingIDTeam = uint8(i + 1);
					PlayerArrayCopy.Insert(MovedPS, i);
				}
			}
		}
	}
}

int32 AUTGameState::GetMaxSpectatingId()
{
	int32 MaxSpectatingID = 0;
	for (int32 i = 0; i<PlayerArray.Num(); i++)
	{
		AUTPlayerState* PS = Cast<AUTPlayerState>(PlayerArray[i]);
		if (PS && (PS->SpectatingID > MaxSpectatingID))
		{
			MaxSpectatingID = PS->SpectatingID;
		}
	}
	return MaxSpectatingID;
}

int32 AUTGameState::GetMaxTeamSpectatingId(int32 TeamNum)
{
	int32 MaxSpectatingID = 0;
	for (int32 i = 0; i<PlayerArray.Num(); i++)
	{
		AUTPlayerState* PS = Cast<AUTPlayerState>(PlayerArray[i]);
		if (PS && (PS->GetTeamNum() == TeamNum) && (PS->SpectatingIDTeam > MaxSpectatingID))
		{
			MaxSpectatingID = PS->SpectatingIDTeam;
		}
	}
	return MaxSpectatingID;

}


bool AUTGameState::GetImportantPickups_Implementation(TArray<AUTPickup*>& PickupList)
{
	for (FActorIterator It(GetWorld()); It; ++It)
	{
		AUTPickup* Pickup = Cast<AUTPickup>(*It);
		AUTPickupInventory* PickupInventory = Cast<AUTPickupInventory>(*It);

		if ((PickupInventory && PickupInventory->GetInventoryType() && PickupInventory->GetInventoryType()->GetDefaultObject<AUTInventory>()->bShowPowerupTimer
			&& ((PickupInventory->GetInventoryType()->GetDefaultObject<AUTInventory>()->HUDIcon.Texture != nullptr)))
			|| (Pickup && Pickup->bDelayedSpawn && Pickup->RespawnTime >= 0.0f && Pickup->HUDIcon.Texture != nullptr))
		{
			if (!Pickup->bOverride_TeamSide)
			{
				Pickup->TeamSide = NearestTeamSide(Pickup);
			}
			PickupList.Add(Pickup);
		}
	}

	//Sort the list by by respawn time 
	//TODO: powerup priority so different armors sort properly
	PickupList.Sort([](const AUTPickup& A, const AUTPickup& B) -> bool
	{
		return A.RespawnTime > B.RespawnTime;
	});

	return true;
}


float AUTGameState::GetStatsValue(FName StatsName)
{
	return StatsData.FindRef(StatsName);
}

void AUTGameState::SetStatsValue(FName StatsName, float NewValue)
{
	LastScoreStatsUpdateTime = GetWorld()->GetTimeSeconds();
	StatsData.Add(StatsName, NewValue);
}

void AUTGameState::ModifyStatsValue(FName StatsName, float Change)
{
	LastScoreStatsUpdateTime = GetWorld()->GetTimeSeconds();
	float CurrentValue = StatsData.FindRef(StatsName);
	StatsData.Add(StatsName, CurrentValue + Change);
}

bool AUTGameState::AreAllPlayersReady()
{
	if (!HasMatchStarted())
	{
		for (int32 i = 0; i < PlayerArray.Num(); i++)
		{
			AUTPlayerState* PS = Cast<AUTPlayerState>(PlayerArray[i]);
			if (PS != nullptr && !PS->bOnlySpectator && !PS->bReadyToPlay)
			{
				return false;
			}
		}
	}
	return true;
}

bool AUTGameState::IsAllowedSpawnPoint_Implementation(AUTPlayerState* Chooser, APlayerStart* DesiredStart) const
{
	return true;
}

float AUTGameState::MatchHighlightScore(AUTPlayerState* PS)
{
	float BestHighlightScore = 0.f;
	for (int32 i = 0; i < 5; i++)
	{
		if (PS->MatchHighlights[i] != NAME_None)
		{
			BestHighlightScore = FMath::Max(BestHighlightScore, HighlightPriority.FindRef(PS->MatchHighlights[i]));
		}
	}
	return BestHighlightScore;
}

bool AUTGameState::AllowMinimapFor(AUTPlayerState* PS)
{
	return PS && (PS->bOnlySpectator);
}

bool AUTGameState::ShouldUseInGameSummary(InGameIntroZoneTypes SummaryType)
{
	if ((GetWorld() == nullptr) || (SummaryType == InGameIntroZoneTypes::Invalid))
	{
		return false;
	}

	for (TActorIterator<AUTInGameIntroZone> It(GetWorld()); It; ++It)
	{
		if (It->ZoneType == SummaryType)
		{
			int RedTeamPlayerCount = 0;
			int BlueTeamPlayerCount = 0;
			int OtherTeamPlayerCount = 0;

			const int RedTeam = 0;
			const int BlueTeam = 1;

			for (int index = 0; index < PlayerArray.Num(); ++index)
			{
				AUTPlayerState* UTPS = Cast<AUTPlayerState>(PlayerArray[index]);
				if (UTPS)
				{
					if (UTPS->GetTeamNum() == RedTeam)
					{
						++RedTeamPlayerCount;
					}
					else if (UTPS->GetTeamNum() == BlueTeam)
					{
						++BlueTeamPlayerCount;
					}
					else
					{
						++OtherTeamPlayerCount;
					}
				}
			}

			bool bIsRedTeamSizeLimitMet = RedTeamPlayerCount > 0 ? It->RedTeamSpawnLocations.Num() >= RedTeamPlayerCount : true;
			bool bIsBlueTeamSizeLimitMet = BlueTeamPlayerCount > 0 ? It->BlueTeamSpawnLocations.Num() >= BlueTeamPlayerCount : true;
			bool bIsOtherTeamPlayerCountMet = OtherTeamPlayerCount > 0 ? It->FFATeamSpawnLocations.Num() >= OtherTeamPlayerCount :  true;

			return bIsRedTeamSizeLimitMet && bIsBlueTeamSizeLimitMet && bIsOtherTeamPlayerCountMet;
		}
	}

	return false;
}

void AUTGameState::ResetTeamsScore()
{
	for (auto team : Teams)
	{
		if (team) team->Score = 0;
	}
}

void AUTGameState::SetTeamScore(const int32& InTeamIndex, const float InScore)
{
	if (Teams.Num() && Teams.IsValidIndex(InTeamIndex) && Teams[InTeamIndex])
	{
		Teams[InTeamIndex]->Score = InScore;
	}
}

void AUTGameState::GiveTeamScore(const int32& InTeamIndex, const float InScore)
{
	if (Teams.Num() && Teams.IsValidIndex(InTeamIndex) && Teams[InTeamIndex])
	{
		Teams[InTeamIndex]->Score += InScore;
	}
}

float AUTGameState::GetTeamScore(const int32& InTeamIndex) const
{
	if (Teams.Num() && Teams.IsValidIndex(InTeamIndex) && Teams[InTeamIndex])
	{
		return Teams[InTeamIndex]->Score;
	}

	return .0f;
}