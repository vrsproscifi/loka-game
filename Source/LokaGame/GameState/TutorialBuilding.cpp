// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "BuildSystem/BuildPlacedComponent.h"
#include "Build/ItemBuildEntity.h"
#include "LKBlueprintFunctionLibrary.h"
#include "TutorialBuilding.h"
#include "PlayerInventoryItem.h"

UTutorialBuilding::UTutorialBuilding()
	: Super()
{

}

void UTutorialBuilding::OnBuildingSelected(ABuildPlacedComponent* InComponent)
{
	if (IsValidBuildingCheck(InComponent, ETutorialAction::BuildingSelect))
	{
		EndCurrentStage();
	}
}

void UTutorialBuilding::OnBuildingPlaced(ABuildPlacedComponent* InComponent)
{
	if (IsValidBuildingCheck(InComponent, ETutorialAction::BuildingPlace))
	{
		EndCurrentStage();
	}
}

void UTutorialBuilding::OnBuildingPreRemove(ABuildPlacedComponent* InComponent)
{
	if (IsValidBuildingCheck(InComponent, ETutorialAction::BuildingRemove))
	{
		EndCurrentStage();
	}
}

bool UTutorialBuilding::IsValidBuildingCheck(ABuildPlacedComponent* InComponent, const ETutorialAction::Type InCheck) const
{
	if (bIsStarted && Stages.IsValidIndex(GetCurrentStage()) && Stages[GetCurrentStage()].TargetAction == InCheck)
	{
		if (InComponent && InComponent->GetInstance() && InComponent->GetInstance()->GetEntity())
		{
			auto MyEntityItem = InComponent->GetInstance()->GetEntity();

			if (Stages[GetCurrentStage()].CompleteSpeciferSecondary == INDEX_NONE)
			{
				return true;
			}
			else if (MyEntityItem->GetModelId() == Stages[GetCurrentStage()].CompleteSpeciferSecondary)
			{
				return true;
			}
		}
	}

	return false;
}