#pragma once

//==============================================
#include "GameFramework/GameState.h"
#include "GameModeTypeId.h"
#include "OnlineGameState.generated.h"

//==============================================
class UNodeComponent;
struct FNodeSessionMatch;
class AOnlineGameMode;

//==============================================
UCLASS()
class LOKAGAME_API AOnlineGameState : public AGameState
{
	GENERATED_BODY()
public:
	AOnlineGameState();
	void HandleBeginPlay() override;


	//====================================================
	//	NodeComponent
protected:
	UPROPERTY(Transient) UNodeComponent* NodeComponent;
	virtual void OnGetMatchInformation_Successfully(const FNodeSessionMatch& information);
	virtual void InitGameState(const FNodeSessionMatch& information);


public:
	AGameMode* GetBasicGameMode() const;
	AOnlineGameMode* GetOnlineGameMode() const;

	template<typename TGameMode = AOnlineGameMode>
	TGameMode* GetGameMode() const
	{
		return Cast<TGameMode>(GetBasicGameMode());
	}
  
	UWorld* GetValidWorld() const;

public:
	virtual TAttribute<FGuid> GetSessionToken() const;

	//UFUNCTION(BlueprintCallable, BlueprintPure, Category = Online)
	FNodeSessionMatch GetMatchInformation() const;
	
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Online)
	UNodeComponent* GetNodeComponent() const;

	EGameMode GetGameModeType() const;
	EGameMap GetGameMapType() const;
};

typedef AOnlineGameState ABaseGameState;