// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once

#include "UTGameUserSettings.generated.h"

UENUM(BlueprintType)
namespace EUTSoundClass
{
	enum Type
	{
		Master UMETA(DisplayName = "Master"),
		Music UMETA(DisplayName = "Music"),
		SFX UMETA(DisplayName = "SFX"),
		Voice UMETA(DisplayName = "Voice"),
		VOIP UMETA(DisplayName = "VOIP"),
		Music_Stingers UMETA(DisplayName = "Stingers"),
		GameMusic UMETA(DisplayName = "Game Music"),
		// should always be last, used to size arrays
		MAX UMETA(Hidden)
	};
}

UENUM(BlueprintType)
namespace SoundClassType
{
	enum Type
	{
		Master,
		SFX,
		UI,
		Music,
		Voice,
		VOIP,
		End
	};
}

typedef SoundClassType::Type ESoundClassType;

UENUM()
enum EBotSpeechOption
{
	// bots are silent
	BSO_None,
	// bots send status messages as text only
	BSO_StatusTextOnly,
	// bots send voice taunts and status messages
	BSO_All,
};

enum class EQualityLevels : uint8;

UCLASS()
class LOKAGAME_API UUTGameUserSettings : public UGameUserSettings, public FTickableGameObject
{
	GENERATED_UCLASS_BODY()

public:
	virtual void SetToDefaults() override;
	virtual void ApplySettings(bool bCheckForCommandLineOverrides) override;
	virtual void UpdateVersion() override;
	virtual bool IsVersionValid() override;

	virtual void SetSoundClassVolume(EUTSoundClass::Type Category, float NewValue);
	virtual float GetSoundClassVolume(EUTSoundClass::Type Category);

	virtual bool SetSoundClassVolumeByName(FString ClassName, float Volume);
	virtual float GetSoundClassVolumeByName(FString ClassName);

	virtual int32 GetAAMode();
	virtual void SetAAMode(int32 NewAAMode);
	static int32 ConvertAAScalabilityQualityToAAMode(int32 AAScalabilityQuality);

	virtual EBotSpeechOption GetBotSpeech() const;
	virtual void SetBotSpeech(EBotSpeechOption NewSetting);

	virtual bool IsHRTFEnabled();
	virtual void SetHRTFEnabled(bool NewHRTFEnabled);
	
	virtual bool IsKeyboardLightingEnabled();
	virtual void SetKeyboardLightingEnabled(bool NewKeyboardLightingEnabled);

#if !UE_SERVER
	DECLARE_EVENT_OneParam(UUTGameUserSettings, FSettingsAutodetected, const Scalability::FQualityLevels& /*DetectedQuality*/);
	virtual FSettingsAutodetected& OnSettingsAutodetected() { return SettingsAutodetectedEvent; }

	void BenchmarkDetailSettingsIfNeeded(class UUTLocalPlayer* LocalPlayer);
	void BenchmarkDetailSettings(class UUTLocalPlayer* LocalPlayer, bool bSaveSettingsOnceDetected);
#endif // !UE_SERVER

	static int32 GetCurrentLanguage();
	static FString GetCurrentLanguageTwoISO();
	void SetCurrentLanguage(const int32 &_lcid);

	FORCEINLINE bool IsShowPing() const { return bShowPing; }
	FORCEINLINE bool IsShowFPS() const { return bShowFPS; }

	FORCEINLINE void SetShowPing(const bool InShow) { bShowPing = InShow; }
	FORCEINLINE void SetShowFPS(const bool InShow) { bShowFPS = InShow; }
	
	void InitSettings();

	void ResetInputSettings();

	FORCEINLINE bool IsEnabledAmbientOcclusion()	{ return bAmbientOcclusion; }
	FORCEINLINE bool IsEnabledMotionBlur()			{ return bMotionBlur; }
	FORCEINLINE bool IsEnabledBloom()				{ return bBloom; }
	FORCEINLINE bool IsEnabledDamageNumbers()		{ return bShowDamageNumbers; }

	void SetEnabledAmbientOcclusion(const bool);
	void SetEnabledMotionBlur(const bool);
	void SetEnabledBloom(const bool);
	FORCEINLINE void SetEnabledDamageNumbers(const bool InShowDamageNumbers) { bShowDamageNumbers = InShowDamageNumbers; }

protected:
	static uint32 CurrentInputVersion;
	static uint32 CurrentGraphicVersion;

	UPROPERTY(config)
	uint32 InputVersion;

	UPROPERTY(config)
	uint32 GraphicVersion;

	UPROPERTY(config)
	float SoundClassVolumes[ESoundClassType::End];

	UPROPERTY(config)
	int32 AAMode;

	UPROPERTY(config)
	bool bAmbientOcclusion;

	UPROPERTY(config)
	bool bMotionBlur;

	UPROPERTY(config)
	bool bBloom;

	/**
	 * Current state of the initial benchmark
	 *  -1: Not run yet
	 *   0: Started, but not completed
	 *   1: Completed
	 */
	UPROPERTY(config)
	int32 InitialBenchmarkState;

	UPROPERTY(config)
	TEnumAsByte<EBotSpeechOption> BotSpeech;

	UPROPERTY(config)
	bool bHRTFEnabled;

	UPROPERTY(config)
	bool bDisableKeyboardLighting;

	UPROPERTY(config)
	bool bAllowChangeViewByAbility;

	FScreenResolutionArray ScreenResolutionArray;
	TArray<FCultureRef> SupportedLanguagesArray;

public:

	virtual void LoadSettings(bool bForceReload = false) override;


	UPROPERTY(config)
	bool bIsFirstRun;

	UPROPERTY(config)
	EQualityLevels GeneralQualityLevel;

	UPROPERTY(config)
	FString Language;

	UPROPERTY(config)
	bool bShowPing;

	UPROPERTY(config)
	bool bShowFPS;

	UPROPERTY(config)
	bool bShowDamageNumbers;

	UPROPERTY(config)
	bool bShouldSuppressLanWarning;

	bool bRequestBenchmark;
	bool bBenchmarkInProgress;
	bool bBenchmarkSaveSettingsOnceDetected;
	int32 TickCount;

	FORCEINLINE FScreenResolutionArray GetScreenResolutions() const
	{
		return ScreenResolutionArray;
	}

	FORCEINLINE TArray<FCultureRef> GetLanguages() const
	{
		return SupportedLanguagesArray;
	}


	virtual TStatId GetStatId() const override;
	virtual bool IsTickable() const override;
	virtual bool IsTickableWhenPaused() const override;
	virtual void Tick( float DeltaTime ) override;


private:
#if !UE_SERVER
	void RunSynthBenchmark(bool bSaveSettingsOnceDetected);
	void CorrectScreenPercentageOnHighResLowGPU(Scalability::FQualityLevels& DetectedLevels) const;

	FSettingsAutodetected SettingsAutodetectedEvent;
	TSharedPtr<class SUTDialogBase> AutoDetectingSettingsDialog;

	TWeakObjectPtr<UUTLocalPlayer> BenchmarkingLocalPlayer;
#endif // !UE_SERVER

public:
	// UT Profile

	UPROPERTY(Config, BlueprintReadOnly, Category = Character)
	float WeaponBob;

	UPROPERTY(Config, BlueprintReadOnly, Category = Character)
	float ViewBob;

	UPROPERTY(Config, BlueprintReadOnly, Category = System)
	uint32 bPushToTalk : 1;

	float GetSoundVolume(const ESoundClassType);
	void SetSoundVolume(const ESoundClassType, const float);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Key)
	static FKey GetKeyBinding(const FString& InAction, const bool IsAlternative = false);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Ability)
	FORCEINLINE bool IsAllowChangeViewByAbility() const { return bAllowChangeViewByAbility; }

	UFUNCTION(BlueprintCallable, Category = Ability)
	void SetAllowChangeViewByAbility(const bool InAllow) { bAllowChangeViewByAbility = InAllow; }

};