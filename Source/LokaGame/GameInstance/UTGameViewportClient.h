// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "Http.h"
#include "SlateBasics.h"
#include "Slate/SlateGameResources.h"
#include "UTATypes.h"
#include "UTGameViewportClient.generated.h"

// Used to hold a list of items for redirect download.
USTRUCT()
struct FPendingRedirect
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	FString FileURL;

	UPROPERTY()
	TEnumAsByte<ERedirectStatus::Type> Status;

	FHttpRequestPtr HttpRequest;

	FPendingRedirect()
		: FileURL(TEXT(""))
	{
	}
	
	FPendingRedirect(FString inFileURL)
	{
		FileURL = inFileURL;
		Status = ERedirectStatus::Pending;
	}
};


class SUsableCompoundWidget;

UCLASS()
class LOKAGAME_API UUTGameViewportClient : public UGameViewportClient
{
	GENERATED_UCLASS_BODY()

public:
	virtual void BeginDestroy() override;

	virtual void AddViewportWidgetContent(TSharedRef<class SWidget> ViewportContent, const int32 ZOrder = 0) override;
	virtual void AddViewportWidgetContent_AlwaysVisible(TSharedRef<class SWidget> ViewportContent, const int32 ZOrder = 0);

	virtual void AddUsableViewportWidgetContent(TSharedRef<SUsableCompoundWidget> ViewportContent, const int32 ZOrder = 0);
	virtual void AddUsableViewportWidgetContent_AlwaysVisible(TSharedRef<SUsableCompoundWidget> ViewportContent, const int32 ZOrder = 0);

	virtual void RemoveViewportWidgetContent(TSharedRef<class SWidget> ViewportContent) override;
	virtual void ToggleViewportWidgets(const bool);

	virtual void Draw(FViewport* InViewport, FCanvas* SceneCanvas) override;
	virtual void UpdateActiveSplitscreenType() override;
	virtual void PostRender(UCanvas* Canvas) override;
	virtual ULocalPlayer* SetupInitialLocalPlayer(FString& OutError) override;
	virtual void Tick(float DeltaSeconds) override;

	virtual bool CheckViewportWidgetContent(TSharedRef<class SWidget> ViewportContent) const;
	virtual bool CheckViewportWidgetContent(const FName& InWidgetType) const;
	void ToggleSlateControll(const bool InToggle) { OnToggleInteractiveAnyWidget(InToggle); }
	virtual void OnToggleInteractiveAnyWidget(const bool InToggle);
	virtual void RemoveAllViewportWidgetsAlt();
	
	TSharedRef<class SWidget> GetViewportWidgetContent(const FName& InWidgetType);

	template<typename WidgetType>
	TSharedRef<WidgetType> TGetViewportWidgetContent(const FName& InWidgetType)
	{
		return StaticCastSharedRef<WidgetType>(GetViewportWidgetContent(InWidgetType));
	}

	TSharedRef<SWidget> GetViewportWidgetByTag(const FName& InTag) const;
	TArray<TSharedRef<SWidget>> GetViewportWidgetsByTag(const FName& InTag) const;	

	void HighlightWidgetByTag(const FName& InTag, const float InTimeSeconds = 0.0f);
	void UnHighlightWidgetByTag(const FName InTag);

	/** panini project the given location using the player's view
	 * PaniniParamsMat will be used to grab parameters for the projection if available, otherwise reasonable default values are used
	 */
	FVector PaniniProjectLocation(const FSceneView* SceneView, const FVector& WorldLoc, UMaterialInterface* PaniniParamsMat = NULL) const;
	/** calls PaniniProjectLocation() with a SceneView representing the player's view (slower, don't use if SceneView is already available) */
	FVector PaniniProjectLocationForPlayer(ULocalPlayer* Player, const FVector& WorldLoc, UMaterialInterface* PaniniParamsMat = NULL) const;

protected:

	bool bAddedStaticWidgets;
	FVector2D LastCursorPosition;
	FTimerHandle timer_HighlightWidget;

	TArray<TSharedRef<class SWidget>> ViewportContentStack;
	TMap<TSharedRef<class SWidget>, int32> ViewportContentStackZOrder;
	TArray<TSharedRef<class SWidget>> ViewportContentStackAlwaysVisible;
	TArray<TSharedRef<class SWidget>> ViewportContentStackHidden;
	TArray<TSharedRef<SUsableCompoundWidget>> ViewportContentStackUsable;

	TSharedPtr<class SWidgetHighlighting> WidgetHighlightingPanel;
	
	TArray<TSharedRef<SWidget>> GetViewportWidgetsByTag_Internal(const TSharedRef<SWidget>& InWidget, const FName& InTag) const;

public:

	// ZOrder index to check widget class 
	static int32 ZOrder_CheckClass;


	virtual bool HideCursorDuringCapture() override;

	// Will be called when a client connects to a server.
	void ClientConnectedToServer();
	
	/** Allow the base World to be overridden, to support making the killcam the active world. */
	UPROPERTY()
	UWorld* ActiveWorldOverride;
	
	/**
	 * Fixes up the UPlayer::PlayerController pointers for ULocalPlayers to point to their associated PlayerController
	 * in the active world.
	 */
	void SetActiveLocalPlayerControllers();

	virtual UWorld* GetWorld() const override;
	void SetActiveWorldOverride(UWorld* WorldOverride);
	void ClearActiveWorldOverride();
	const UWorld* GetActiveWorldOverride() const { return ActiveWorldOverride; }
};

