// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "AssetRegistryModule.h"
#include "Engine/Texture2D.h"
#include "Engine/TextureRenderTarget2D.h"
#include "Notify/SMessageBox.h"
#include "Notify/SNotifyList.h"
#include "Widget.h"
#include "LKBlueprintFunctionLibrary.h"
#include "Item/ItemBaseEntity.h"
#include "GameSingleton.h"
#if WITH_EDITOR
#include "AssetToolsModule.h"
#include "KismetEditorUtilities.h"
#endif

FString ULKBlueprintFunctionLibrary::K2_Map_GetString(const EGameMapTypeId::Type& Id)
{
	return FGameMapTypeId::GetString(Id);
}

FString ULKBlueprintFunctionLibrary::K2_Mode_GetString(const EGameModeTypeId::Type& Id)
{
	return FGameModeTypeId::GetString(Id);
}

FText ULKBlueprintFunctionLibrary::K2_Map_GetText(const EGameMapTypeId::Type& Id)
{
	return FGameMapTypeId::GetText(Id);
}

FText ULKBlueprintFunctionLibrary::K2_Map_GetMapText(const FString& Id)
{
	return FGameMapTypeId::GetText(Id);
}

FText ULKBlueprintFunctionLibrary::K2_Mode_GetText(const EGameModeTypeId::Type& Id)
{
	return FGameModeTypeId::GetText(Id);
}

FText ULKBlueprintFunctionLibrary::K2_Mode_GetTextDesc(const EGameModeTypeId::Type& Id)
{
	return FGameModeTypeId::GetTextDesc(Id);
}

UTexture2D* ULKBlueprintFunctionLibrary::K2_CreateStaticTexture(UTextureRenderTarget2D* InRenderTarget, const FString& InRelativePath, const FString& InName)
{
	UTexture2D* NewObj = nullptr;

#if WITH_EDITOR
	if (InRenderTarget)
	{
		FString Name;
		FString PackageName;

		if (InRelativePath.IsEmpty() == false && InName.IsEmpty() == false)
		{
			PackageName = InRelativePath + InName;
			Name = InName;
		}
		else if (InName.IsEmpty() == false)
		{
			PackageName = InRenderTarget->GetOutermost()->GetName() + InName;
			Name = InName;
		}

		FAssetToolsModule& AssetToolsModule = FModuleManager::Get().LoadModuleChecked<FAssetToolsModule>("AssetTools");
		AssetToolsModule.Get().CreateUniqueAssetName(PackageName, "_ET", PackageName, Name);

		NewObj = InRenderTarget->ConstructTexture2D(CreatePackage(nullptr, *PackageName), Name, InRenderTarget->GetMaskedFlags(), CTF_Default, nullptr);

		if (NewObj)
		{
			NewObj->Modify();
			NewObj->MarkPackageDirty();
			NewObj->PostEditChange();
			NewObj->UpdateResource();
			
			// Notify the asset registry
			FAssetRegistryModule::AssetCreated(NewObj);
		}
	}
#endif

	return NewObj;
}

FNotifyInfo FBlueprintNotifyInfo::ToNotifyInfo() const
{
	FNotifyInfo NotifyInfo;

	if (this->ContentWidget)
	{
		NotifyInfo.ContentWidget = this->ContentWidget->TakeWidget();
	}

	NotifyInfo.Title				= this->Title;
	NotifyInfo.Content				= this->Content;
	NotifyInfo.Hyperlink			= this->Hyperlink;
	NotifyInfo.ShowTime				= (this->ShowTime <= 5.0f) ? 10.0f : this->ShowTime;
	NotifyInfo.Image				= new FSlateBrush(this->Image);

	return NotifyInfo;
}

void ULKBlueprintFunctionLibrary::K2_AddNotify(const FBlueprintNotifyInfo& NotifyInfo)
{
	SNotifyList::Get()->AddNotify(NotifyInfo.ToNotifyInfo());
}

FName ULKBlueprintFunctionLibrary::K2_ShowMessageBoxU(const UMessageBoxCfg* InConfiguration)
{
	return ULKBlueprintFunctionLibrary::K2_ShowMessageBox(InConfiguration->Message);
}

FName ULKBlueprintFunctionLibrary::K2_ShowMessageBox(const FMessageBoxMsg& InConfiguration)
{
	FName ReturnName = *FString::Printf(TEXT("ULKBlueprintFunctionLibrary_%f"), FSlateApplication::Get().GetCurrentTime());
	SMessageBox::AddQueueMessage(ReturnName, FOnClickedOutside::CreateLambda([cfg = InConfiguration]() {
		SMessageBox::Get()->SetFromCfg(cfg);
		SMessageBox::Get()->ToggleWidget(true);
	}));
	return ReturnName;
}

void ULKBlueprintFunctionLibrary::K2_HideMessageBox(const FName& InName)
{
	if (SMessageBox::Get()->GetQueueId() != NAME_None && SMessageBox::Get()->GetQueueId() == InName)
	{
		SMessageBox::Get()->ToggleWidget(false);
	}
}

FMessageBoxMsg& ULKBlueprintFunctionLibrary::K2_OnMessageBoxButton(FMessageBoxMsg& InStructure, FOnMessageBoxButton Event)
{
	InStructure.OnMessageBoxButton = Event;
	return InStructure;
}

FMessageBoxMsg& ULKBlueprintFunctionLibrary::K2_OnMessageBoxHidden(FMessageBoxMsg& InStructure, FOnMessageBoxHidden Event)
{
	InStructure.OnMessageBoxHidden = Event;
	return InStructure;
}

bool ULKBlueprintFunctionLibrary::K2_CreateTextureForEntity(UTextureRenderTarget2D* InRenderTarget, const FString& InRelativePath, const TSubclassOf<UItemBaseEntity>& InClass)
{
#if WITH_EDITOR
	auto ValidTarget = InClass.GetDefaultObject();
	if (ValidTarget && InRenderTarget)
	{
		UTexture2D* NewObj = nullptr;

		if (InRenderTarget)
		{
			FString Name;
			FString PackageName;

			if (InRelativePath.IsEmpty() == false)
			{
				PackageName = InRelativePath + ValidTarget->GetName();
				Name = ValidTarget->GetName();
			}
			else
			{
				PackageName = "/Game/1LOKAgame/UserInterface/EntityCaptures/" + ValidTarget->GetName() + "Texture";
				Name = ValidTarget->GetName() + "Texture";
			}

			auto MyPackage = CreatePackage(nullptr, *PackageName);
			NewObj = InRenderTarget->ConstructTexture2D(MyPackage, Name, InRenderTarget->GetMaskedFlags(), CTF_Default | CTF_RemapAlphaAsMasked, nullptr);

			if (NewObj && MyPackage)
			{
				FAssetRegistryModule::AssetCreated(NewObj);
				ValidTarget->EntityImage = FSlateImageBrush(NewObj, FVector2D(NewObj->GetSizeX(), NewObj->GetSizeY()));
				//UPackage::SavePackage(MyPackage, NewObj, EObjectFlags::RF_ArchetypeObject, *Name);
				FKismetEditorUtilities::CompileBlueprint(Cast<UBlueprint>(ValidTarget));

				NewObj->MarkPackageDirty();
				ValidTarget->MarkPackageDirty();

				return true;
			}
		}
	}
#endif

	return false;
}

UGameSingleton* ULKBlueprintFunctionLibrary::K2_GetGameSingleton()
{
	return UGameSingleton::Get();
}

//===============================================================================================================================
// Flags

bool ULKBlueprintFunctionLibrary::K2_HasAnyFlags(const int32& InSource, const int32& InFlags)
{
	return FFlagsHelper::HasAnyFlags(InSource, InFlags);
}

bool ULKBlueprintFunctionLibrary::K2_HasAllFlags(const int32& InSource, const int32& InFlags)
{
	return FFlagsHelper::HasAllFlags(InSource, InFlags);
}

void ULKBlueprintFunctionLibrary::K2_SetFlags(int32& InSource, const int32& InFlags)
{
	FFlagsHelper::SetFlags(InSource, InFlags);
}

void ULKBlueprintFunctionLibrary::K2_ToggleFlags(int32& InSource, const int32& InFlags)
{
	FFlagsHelper::ToggleFlags(InSource, InFlags);
}

void ULKBlueprintFunctionLibrary::K2_ClearFlags(int32& InSource, const int32& InFlags)
{
	FFlagsHelper::ClearFlags(InSource, InFlags);
}

int32 ULKBlueprintFunctionLibrary::K2_ToFlag(const int32& InSource)
{
	return FFlagsHelper::ToFlag(InSource);
}