// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "LokaGame.h"
#include "Engine/GameInstance.h"
#include "ShooterGameInstance.generated.h"

namespace EOnlineServerConnectionStatus { enum Type : unsigned char; }

class FVariantData;
class UTutorialBase;
class UEntityRepository;


USTRUCT()
struct FBoneDamageRate
{
	GENERATED_USTRUCT_BODY();

	UPROPERTY(EditDefaultsOnly)
	FName Name;

	UPROPERTY(EditDefaultsOnly)
	float Rate;
};

struct FStartMessages
{
	bool bIsEarlyAccessShowed;
	bool bIsFeedbackShowed;
	bool bIsAcceptedEula;
	bool bIsTournamentRulesShowed;

	void Default()
	{
		bIsEarlyAccessShowed = false;
		bIsFeedbackShowed = false;
		bIsAcceptedEula = false;
		bIsTournamentRulesShowed = false;
	}

	void Load()
	{
		//auto SaveFile = FConfigFile();
		//SaveFile.Read(FPaths::GeneratedConfigDir() + "StartMessages.ini");
		//SaveFile.GetInt64(TEXT("StartMessages"), TEXT("IsEarlyAccessShowed"), bIsEarlyAccessShowed);
		//SaveFile.GetInt64(TEXT("StartMessages"), TEXT("IsFeedbackShowed"), bIsFeedbackShowed);
		//SaveFile.GetInt64(TEXT("StartMessages"), TEXT("IsAcceptedEula"), bIsAcceptedEula);
		//SaveFile.GetInt64(TEXT("StartMessages"), TEXT("bIsTournamentRulesShowed"), bIsTournamentRulesShowed);
	}

	void Save()
	{
		//const auto SaveFileName = FPaths::GeneratedConfigDir() + "StartMessages.ini";
		//auto SaveFile = FConfigFile();
		//SaveFile.SetInt64(TEXT("StartMessages"), TEXT("IsEarlyAccessShowed"), bIsEarlyAccessShowed);
		//SaveFile.SetInt64(TEXT("StartMessages"), TEXT("IsFeedbackShowed"), bIsFeedbackShowed);
		//SaveFile.SetInt64(TEXT("StartMessages"), TEXT("IsAcceptedEula"), bIsAcceptedEula);
		//SaveFile.SetInt64(TEXT("StartMessages"), TEXT("bIsTournamentRulesShowed"), bIsTournamentRulesShowed);

		//if (!SaveFile.Write(SaveFileName))
		//{
		//	SaveFile.UpdateSections(*SaveFileName, TEXT("StartMessages"));
		//}
	}
};

UCLASS(config=Game)
class UShooterGameInstance : public UGameInstance
{
public:
	GENERATED_UCLASS_BODY()
	//=======================================================================================================
	//												[ Client ]

public:	

	UPROPERTY(BlueprintReadOnly, Category = HUD)
	TArray<FString> ServerRandomNames;

	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	TArray<FBoneDamageRate> BonesDamageRate;

	FStartMessages StartMessages;

	TMap<FName, float> BonesDamageRateMap;

	bool Tick(float DeltaSeconds);

	virtual void Init() override;
	virtual void Shutdown() override;
	virtual void StartGameInstance() override;

	bool PlayDemo(ULocalPlayer* LocalPlayer, const FString& DemoName);
	
	/** Travel directly to the named session */
	void TravelToSession(const FName& SessionName);



	void RemoveExistingLocalPlayer(ULocalPlayer* ExistingPlayer);

	void RemoveSplitScreenPlayers();

	TSharedPtr<const FUniqueNetId > GetUniqueNetIdFromControllerId( const int ControllerId );

	/** Returns true if the game is in online mode */
	bool GetIsOnline() const { return bIsOnline; }

	/** Sets the online mode of the game */
	void SetIsOnline(bool bInIsOnline);

	/** Sets the controller to ignore for pairing changes. Useful when we are showing external UI for manual profile switching. */
	void SetIgnorePairingChangeForControllerId( const int32 ControllerId );

	/** Returns true if the passed in local player is signed in and online */
	bool IsLocalPlayerOnline(ULocalPlayer* LocalPlayer);

	/** Returns true if owning player is online. Displays proper messaging if the user can't play */
	bool ValidatePlayerForOnlinePlay(ULocalPlayer* LocalPlayer);

	/** Shuts down the session, and frees any net driver */
	void CleanupSessionOnReturnToMenu();

	/** Flag the local player when they quit the game */
	void LabelPlayerAsQuitter(ULocalPlayer* LocalPlayer) const;

	// Generic confirmation handling (just hide the dialog)
	FReply OnConfirmGeneric();
	bool HasLicense() const { return bIsLicensed; }

private:

	UPROPERTY(config)
	FString WelcomeScreenMap;

	UPROPERTY(config)
	FString MainMenuMap;



	/** URL to travel to after pending network operations */
	FString TravelURL;

	/** Whether the match is online or not */
	bool bIsOnline;

	/** If true, enable splitscreen when map starts loading */
	bool bPendingEnableSplitscreen;

	/** Whether the user has an active license to play the game */
	bool bIsLicensed;

	/** Controller to ignore for pairing changes. -1 to skip ignore. */
	int32 IgnorePairingChangeForControllerId;

	/** Last connection status that was passed into the HandleNetworkConnectionStatusChanged hander */
	//EOnlineServerConnectionStatus::Type	CurrentConnectionStatus;

	/** Delegate for callbacks to Tick */
	FTickerDelegate TickDelegate;

	/** Handle to various registered delegates */
	FDelegateHandle TickDelegateHandle;
	FDelegateHandle TravelLocalSessionFailureDelegateHandle;

	//void HandleNetworkConnectionStatusChanged(  EOnlineServerConnectionStatus::Type LastConnectionStatus, EOnlineServerConnectionStatus::Type ConnectionStatus );

	//void HandleSessionFailure( const FUniqueNetId& NetId, ESessionFailure::Type FailureType );
	
public:
	void OnPreLoadMap(const FString& InMap = "");
	void OnPostLoadMap(class UWorld*);
	
private:

	void AddNetworkFailureHandlers();
	void RemoveNetworkFailureHandlers();

	/** Called when there is an error trying to travel to a local session */
	void TravelLocalSessionFailure(UWorld *World, ETravelFailure::Type FailureType, const FString& ErrorString);

	/**
	* Creates the message menu, clears other menus and sets the KingState to Message.
	*
	* @param	Message				Main message body
	* @param	OKButtonString		String to use for 'OK' button
	* @param	CancelButtonString	String to use for 'Cancel' button
	*/
	void ShowMessageThenGoMain(const FText& Message, const FText& OKButtonString, const FText& CancelButtonString);

	bool LoadFrontEndMap(const FString& MapName);

	/** Sets a rich presence string for all local players. */
	void SetPresenceForLocalPlayers(const FVariantData& PresenceData);

	// OSS delegates to handle
	//void HandleUserLoginChanged(int32 GameUserIndex, ELoginStatus::Type PreviousLoginStatus, ELoginStatus::Type LoginStatus, const FUniqueNetId& UserId);

	// Callback to pause the game when the OS has constrained our app.
	void HandleAppWillDeactivate();

	// Callback occurs when game being suspended
	void HandleAppSuspend();

	// Callback occurs when game resuming
	void HandleAppResume();

	// Callback to process game licensing change notifications.
	void HandleAppLicenseUpdate();

	// Callback to handle safe frame size changes.
	void HandleSafeFrameChanged();

	// Callback to handle controller connection changes.
	void HandleControllerConnectionChange(bool bIsConnection, int32 Unused, int32 GameUserIndex);

protected:
	//===========================================================
	//						[ Loading ]
	void OnMovieEnd();

	TSharedPtr<class SLoadingScreen> loadingScreen;
#if !UE_SERVER && !WITH_EDITOR	
	class IGameMoviePlayer* moviePlayer;
	TSharedPtr<struct FLoadingScreenAttributes> loadingAttributes;
#endif
	FDelegateHandle moviePlayerDelegate;
	FStreamableManager AssetLoader;

public:

	enum ELoadingScreenMode
	{
		Auto,
		CustomShow,
		CustomHide,
		End
	};

	void SetLoadingScreenMode(const int32 InMode);
	bool ShowLoadingScreen();
	bool HideLoadingScreen();

	FORCEINLINE int32 GetLoadingScreenMode() const { return CurrentLoadingScreenMode; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Tutorials)
	FORCEINLINE TMap<int32, UTutorialBase*> GetTutorials() const { return TutorialsInstance; }

	UPROPERTY(BlueprintReadOnly, Category = Tutorials)
	int32 CurrentTutorialId;

	UFUNCTION(BlueprintCallable, Category = Tutorials)
	void InitWorldForTutorials(UWorld* InWorld);

protected:

	int32 CurrentLoadingScreenMode;

	UPROPERTY(EditDefaultsOnly, Category = Tutorials)
	TMap<int32, TSubclassOf<UTutorialBase>> TutorialsClass;

	UPROPERTY(BlueprintReadOnly, Category = Tutorials)
	TMap<int32, UTutorialBase*> TutorialsInstance;
};


