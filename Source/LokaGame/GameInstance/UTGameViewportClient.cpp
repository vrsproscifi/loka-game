// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "LokaGame.h"
#include "UTGameViewportClient.h"
#include "Base/SUTDialogBase.h"
#include "Engine/GameInstance.h"
#include "UTGameEngine.h"
#include "Engine/Console.h"
#include "UTLocalPlayer.h"
#include "UTHUD.h"
#include "UTPlayerController.h"
#include "Utilities/SUsableCompoundWidget.h"
#include "SMessageBox.h"
#include "SNotifyList.h"
#include "Utilities/SWidgetHighlighting.h"
#include "SMessageTip.h"

int32 UUTGameViewportClient::ZOrder_CheckClass = 707;

UUTGameViewportClient::UUTGameViewportClient(const class FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	bAddedStaticWidgets = false;

	MaxSplitscreenPlayers = 6;

	SplitscreenInfo.SetNum(10); // we are hijacking entries 8 and 9 for 5 and 6 players
	
	SplitscreenInfo[8].PlayerData.Add(FPerPlayerSplitscreenData(0.33f, 0.5f, 0.0f, 0.0f));
	SplitscreenInfo[8].PlayerData.Add(FPerPlayerSplitscreenData(0.33f, 0.5f, 0.33f, 0.0f));
	SplitscreenInfo[8].PlayerData.Add(FPerPlayerSplitscreenData(0.33f, 0.5f, 0.0f, 0.5f));
	SplitscreenInfo[8].PlayerData.Add(FPerPlayerSplitscreenData(0.33f, 0.5f, 0.33f, 0.5f));
	SplitscreenInfo[8].PlayerData.Add(FPerPlayerSplitscreenData(0.33f, 1.0f, 0.66f, 0.0f));

	const float OneThird = 1.0f / 3.0f;
	const float TwoThirds = 2.0f / 3.0f;
	SplitscreenInfo[9].PlayerData.Add(FPerPlayerSplitscreenData(OneThird, 0.5f, 0.0f, 0.0f));
	SplitscreenInfo[9].PlayerData.Add(FPerPlayerSplitscreenData(OneThird, 0.5f, OneThird, 0.0f));
	SplitscreenInfo[9].PlayerData.Add(FPerPlayerSplitscreenData(OneThird, 0.5f, 0.0f, 0.5f));
	SplitscreenInfo[9].PlayerData.Add(FPerPlayerSplitscreenData(OneThird, 0.5f, OneThird, 0.5f));
	SplitscreenInfo[9].PlayerData.Add(FPerPlayerSplitscreenData(OneThird, 0.5f, TwoThirds, 0.0f));
	SplitscreenInfo[9].PlayerData.Add(FPerPlayerSplitscreenData(OneThird, 0.5f, TwoThirds, 0.5f));

	LastCursorPosition = FVector2D::ZeroVector;
}

void UUTGameViewportClient::BeginDestroy()
{
	//RemoveViewportWidgetContent(SMessageBox::Get());
	//RemoveViewportWidgetContent(SNotifyList::Get());

	//SMessageBox::Reset();
	//SNotifyList::Reset();

	Super::BeginDestroy();
}

void UUTGameViewportClient::AddViewportWidgetContent(TSharedRef<SWidget> ViewportContent, const int32 ZOrder)
{
#if !UE_SERVER
	int32 AltZOrder = ZOrder;

	if (AltZOrder >= UUTGameViewportClient::ZOrder_CheckClass)
	{
		if (CheckViewportWidgetContent(ViewportContent)) return;
		else AltZOrder -= UUTGameViewportClient::ZOrder_CheckClass;
	}

	Super::AddViewportWidgetContent(ViewportContent, AltZOrder);
	ViewportContentStack.Add(ViewportContent);
	ViewportContentStackZOrder.Add(ViewportContent, AltZOrder);

	if (bAddedStaticWidgets == false)
	{
		bAddedStaticWidgets = true;

		SAssignNew(WidgetHighlightingPanel, SWidgetHighlighting);
		AddViewportWidgetContent_AlwaysVisible(WidgetHighlightingPanel.ToSharedRef(), 2000);
		AddUsableViewportWidgetContent_AlwaysVisible(SMessageBox::Get(), 1000);
		AddViewportWidgetContent(SMessageTip::Get(), 500);		
	}
#endif
}

void UUTGameViewportClient::AddViewportWidgetContent_AlwaysVisible(TSharedRef<SWidget> ViewportContent, const int32 ZOrder)
{
#if !UE_SERVER
	int32 AltZOrder = ZOrder;

	if (AltZOrder >= UUTGameViewportClient::ZOrder_CheckClass)
	{
		if (CheckViewportWidgetContent(ViewportContent)) return;
		else AltZOrder -= UUTGameViewportClient::ZOrder_CheckClass;
	}

	Super::AddViewportWidgetContent(ViewportContent, AltZOrder);
	ViewportContentStackAlwaysVisible.Add(ViewportContent);
	ViewportContentStackZOrder.Add(ViewportContent, AltZOrder);
#endif
}

void UUTGameViewportClient::AddUsableViewportWidgetContent(TSharedRef<SUsableCompoundWidget> ViewportContent, const int32 ZOrder)
{
	AddViewportWidgetContent(ViewportContent, ZOrder);
	ViewportContentStackUsable.Add(ViewportContent);
	ViewportContent->OnToggleInteractive.BindUObject(this, &UUTGameViewportClient::OnToggleInteractiveAnyWidget);
}

void UUTGameViewportClient::AddUsableViewportWidgetContent_AlwaysVisible(TSharedRef<SUsableCompoundWidget> ViewportContent, const int32 ZOrder)
{
	AddViewportWidgetContent_AlwaysVisible(ViewportContent, ZOrder);
	ViewportContentStackUsable.Add(ViewportContent);
	ViewportContent->OnToggleInteractive.BindUObject(this, &UUTGameViewportClient::OnToggleInteractiveAnyWidget);
}

void UUTGameViewportClient::RemoveViewportWidgetContent(TSharedRef<SWidget> ViewportContent)
{
#if !UE_SERVER
	Super::RemoveViewportWidgetContent(ViewportContent);
	ViewportContentStack.Remove(ViewportContent);
	ViewportContentStackHidden.Remove(ViewportContent);
	ViewportContentStackAlwaysVisible.Remove(ViewportContent);
	ViewportContentStackZOrder.Remove(ViewportContent);
#endif
}

void UUTGameViewportClient::OnToggleInteractiveAnyWidget(const bool InToggle)
{
	// LastCursorPosition

	bool bIsFoundInteractive = false;
	bool bIsFoundInteractiveMultiply = false;

	for (auto vMap : ViewportContentStackUsable)
	{
		if (ViewportContentStack.Contains(vMap) || ViewportContentStackAlwaysVisible.Contains(vMap))
		{
			if (vMap->IsInInteractiveMode())
			{
				bIsFoundInteractive = true;
			}
			else if (bIsFoundInteractive)
			{
				bIsFoundInteractiveMultiply = true;
				break;
			}
		}
	}

	APlayerController* PlayerCtrl = (GetWorld() && GetWorld()->GetFirstPlayerController()) ? GetWorld()->GetFirstPlayerController() : nullptr;
	ACharacter* PlayerChar = PlayerCtrl ? PlayerCtrl->GetCharacter() : nullptr;

	if (InToggle)
	{
		if (PlayerCtrl)
		{
			PlayerCtrl->bShowMouseCursor = true;

			if (PlayerChar)
			{
				PlayerCtrl->SetInputMode(FInputModeGameAndUI().SetHideCursorDuringCapture(false));
				PlayerChar->DisableInput(PlayerCtrl);
			}
			else
			{
				PlayerCtrl->SetInputMode(FInputModeUIOnly());
			}
		}

		SetCaptureMouseOnClick(EMouseCaptureMode::CaptureDuringMouseDown);

		if (bIsFoundInteractiveMultiply == false)
		{
			if (LastCursorPosition == FVector2D::ZeroVector)
			{
				LastCursorPosition = FSlateApplication::Get().GetCursorPos();
			}

			FSlateApplication::Get().SetCursorPos(LastCursorPosition);
		}
	}
	else if(bIsFoundInteractive == false)
	{
		if (PlayerCtrl)
		{
			PlayerCtrl->bShowMouseCursor = false;

			if (PlayerChar)
			{
				PlayerChar->EnableInput(PlayerCtrl);
			}

			PlayerCtrl->SetInputMode(FInputModeGameOnly());
		}

		SetCaptureMouseOnClick(EMouseCaptureMode::CapturePermanently);

		LastCursorPosition = FSlateApplication::Get().GetCursorPos();
	}
}

void UUTGameViewportClient::ToggleViewportWidgets(const bool InToggle)
{
#if !UE_SERVER
	if (InToggle)
	{
		if (ViewportContentStackHidden.Num() > 0)
		{
			for (auto Widget : ViewportContentStackHidden)
			{
				ViewportContentStack.Add(Widget);
				if (ViewportContentStackZOrder.Contains(Widget))
				{
					Super::AddViewportWidgetContent(Widget, ViewportContentStackZOrder[Widget]);
				}
				else
				{
					Super::AddViewportWidgetContent(Widget, 0);
				}
			}

			ViewportContentStackHidden.Empty();
		}
	}
	else
	{
		if (ViewportContentStackHidden.Num() == 0)
		{
			for (auto Widget : ViewportContentStack)
			{
				ViewportContentStackHidden.Add(Widget);
				ViewportContentStack.Remove(Widget);
				Super::RemoveViewportWidgetContent(Widget);
			}
		}
	}	
#endif
}

bool UUTGameViewportClient::CheckViewportWidgetContent(TSharedRef<SWidget> ViewportContent) const
{
#if !UE_SERVER
	for (auto Widget : ViewportContentStack)
	{
		if (Widget == ViewportContent)
		{
			return true;
		}
	}

	for (auto Widget : ViewportContentStackHidden)
	{
		if (Widget == ViewportContent)
		{
			return true;
		}
	}

	for (auto Widget : ViewportContentStackAlwaysVisible)
	{
		if (Widget == ViewportContent)
		{
			return true;
		}
	}
#endif
	return false;
}

bool UUTGameViewportClient::CheckViewportWidgetContent(const FName& InWidgetType) const
{
#if !UE_SERVER
	for (auto Widget : ViewportContentStack)
	{
		if (Widget->GetType() == InWidgetType)
		{
			return true;
		}
	}

	for (auto Widget : ViewportContentStackHidden)
	{
		if (Widget->GetType() == InWidgetType)
		{
			return true;
		}
	}

	for (auto Widget : ViewportContentStackAlwaysVisible)
	{
		if (Widget->GetType() == InWidgetType)
		{
			return true;
		}
	}
#endif
	return false;
}

TSharedRef<SWidget> UUTGameViewportClient::GetViewportWidgetContent(const FName& InWidgetType)
{
#if !UE_SERVER
	if (CheckViewportWidgetContent(InWidgetType))
	{
		for (auto Widget : ViewportContentStack)
		{
			if (Widget->GetType() == InWidgetType)
			{
				return Widget;
			}
		}

		for (auto Widget : ViewportContentStackHidden)
		{
			if (Widget->GetType() == InWidgetType)
			{
				return Widget;
			}
		}

		for (auto Widget : ViewportContentStackAlwaysVisible)
		{
			if (Widget->GetType() == InWidgetType)
			{
				return Widget;
			}
		}
	}
#endif
	return SNullWidget::NullWidget;
}

void UUTGameViewportClient::RemoveAllViewportWidgetsAlt()
{
	RemoveAllViewportWidgets();

	ViewportContentStack.Empty();
	ViewportContentStackZOrder.Empty();
	ViewportContentStackAlwaysVisible.Empty();
	ViewportContentStackHidden.Empty();
	ViewportContentStackUsable.Empty();

	AddUsableViewportWidgetContent_AlwaysVisible(SMessageBox::Get(), 1000);
	AddViewportWidgetContent(SMessageTip::Get(), 500);
	if (WidgetHighlightingPanel.IsValid())
	{
		AddViewportWidgetContent_AlwaysVisible(WidgetHighlightingPanel.ToSharedRef(), 2000);
	}
}

TSharedRef<SWidget> UUTGameViewportClient::GetViewportWidgetByTag(const FName& InTag) const
{
	TSharedRef<SWidget> ReturnWidget = SNullWidget::NullWidget;

	auto Widgets = GetViewportWidgetsByTag(InTag);
	if (Widgets.Num())
	{
		ReturnWidget = Widgets[0];
	}

	return ReturnWidget;
}

TArray<TSharedRef<SWidget>> UUTGameViewportClient::GetViewportWidgetsByTag(const FName& InTag) const
{
	TArray<TSharedRef<SWidget>> ReturnWidgets;

	for (auto Widget : ViewportContentStack)
	{
		if (Widget->GetTag().IsEqual(InTag))
		{
			ReturnWidgets.Add(Widget);
		}
		else if(WidgetHighlightingPanel.IsValid())
		{
			FGeometry NullGeomatry = WidgetHighlightingPanel->GetCachedGeometry();
			FArrangedChildren Childs(EVisibility::Visible);
			Widget->ArrangeChildren(NullGeomatry, Childs);

			if (Childs.Num())
			{
				for (SIZE_T i = 0; i < Childs.Num(); ++i)
				{
					auto SubChilds = GetViewportWidgetsByTag_Internal(Childs[i].Widget, InTag);
					if (Childs[i].Widget->GetTag().IsEqual(InTag))
					{
						ReturnWidgets.Add(Childs[i].Widget);
					}
					
					ReturnWidgets.Append(SubChilds);
				}
			}
		}
	}

	return ReturnWidgets;
}

TArray<TSharedRef<SWidget>> UUTGameViewportClient::GetViewportWidgetsByTag_Internal(const TSharedRef<SWidget>& InWidget, const FName& InTag) const
{
	TArray<TSharedRef<SWidget>> lReturnWidgets;

	FArrangedChildren ArrangedChildren(EVisibility::All);
	InWidget->ArrangeChildren(InWidget->GetCachedGeometry(), ArrangedChildren);

	for (int32 WidgetIndex = 0; WidgetIndex < ArrangedChildren.Num(); ++WidgetIndex)
	{
		if (ArrangedChildren[WidgetIndex].Widget->GetTag().IsEqual(InTag))
		{
			lReturnWidgets.Add(ArrangedChildren[WidgetIndex].Widget);
		}

		auto Childs = GetViewportWidgetsByTag_Internal(ArrangedChildren[WidgetIndex].Widget, InTag);
		for (auto child : Childs)
		{
			if (child->GetTag().IsEqual(InTag))
			{
				lReturnWidgets.Add(child);
			}
		}
	}

	return lReturnWidgets;
}

void UUTGameViewportClient::HighlightWidgetByTag(const FName& InTag, const float InTimeSeconds)
{
	auto FoundWidget = GetViewportWidgetByTag(InTag);
	if (FoundWidget != SNullWidget::NullWidget && WidgetHighlightingPanel.IsValid())
	{
		WidgetHighlightingPanel->AddWidget(FoundWidget);

		if (FMath::IsNearlyEqual(InTimeSeconds, .0f) == false && GetWorld())
		{
			FTimerHandle _th;
			GetWorld()->GetTimerManager().SetTimer(_th, FTimerDelegate::CreateUObject(this, &UUTGameViewportClient::UnHighlightWidgetByTag, InTag), InTimeSeconds, false);
		}
	}
}

void UUTGameViewportClient::UnHighlightWidgetByTag(const FName InTag)
{
	auto FoundWidget = GetViewportWidgetByTag(InTag);
	if (FoundWidget != SNullWidget::NullWidget && WidgetHighlightingPanel.IsValid())
	{
		WidgetHighlightingPanel->RemoveWidget(FoundWidget);
	}
}

static FVector2D PaniniProjection(FVector2D OM, float d, float s)
{
	float PaniniDirectionXZInvLength = 1.0f / FMath::Sqrt(1.0f + OM.X * OM.X);
	float SinPhi = OM.X * PaniniDirectionXZInvLength;
	float TanTheta = OM.Y * PaniniDirectionXZInvLength;
	float CosPhi = FMath::Sqrt(1.0f - SinPhi * SinPhi);
	float S = (d + 1.0f) / (d + CosPhi);

	return S * FVector2D(SinPhi, FMath::Lerp(TanTheta, TanTheta / CosPhi, s));
}

static FName NAME_d(TEXT("d"));
static FName NAME_s(TEXT("s"));
static FName NAME_FOVMulti(TEXT("FOV Multi"));
static FName NAME_Scale(TEXT("Scale"));

FVector UUTGameViewportClient::PaniniProjectLocation(const FSceneView* SceneView, const FVector& WorldLoc, UMaterialInterface* PaniniParamsMat) const
{
	float d = 1.0f;
	float s = 0.0f;
	float FOVMulti = 0.5f;
	float Scale = 1.0f;
	if (PaniniParamsMat != nullptr)
	{
		PaniniParamsMat->GetScalarParameterValue(NAME_d, d);
		PaniniParamsMat->GetScalarParameterValue(NAME_s, s);
		PaniniParamsMat->GetScalarParameterValue(NAME_FOVMulti, FOVMulti);
		PaniniParamsMat->GetScalarParameterValue(NAME_Scale, Scale);
	}

	FVector ViewSpaceLoc = SceneView->ViewMatrices.GetViewMatrix().TransformPosition(WorldLoc);
	FVector2D PaniniResult = PaniniProjection(FVector2D(ViewSpaceLoc.X / ViewSpaceLoc.Z, ViewSpaceLoc.Y / ViewSpaceLoc.Z), d, s);

	FMatrix ClipToView = SceneView->ViewMatrices.GetInvProjectionMatrix();// GetInvProjMatrix();
	float ScreenSpaceScaleFactor = (ClipToView.M[0][0] / PaniniProjection(FVector2D(ClipToView.M[0][0], ClipToView.M[1][1]), d, 0.0f).X) * Scale;
	float FOVModifier = ((ClipToView.M[0][0] - 1.0f) * FOVMulti + 1.0f) * ScreenSpaceScaleFactor;

	PaniniResult *= (ViewSpaceLoc.Z * FOVModifier);

	return SceneView->ViewMatrices.GetInvViewMatrix().TransformPosition(FVector(PaniniResult.X, PaniniResult.Y, ViewSpaceLoc.Z));
}

FVector UUTGameViewportClient::PaniniProjectLocationForPlayer(ULocalPlayer* Player, const FVector& WorldLoc, UMaterialInterface* PaniniParamsMat) const
{
	FSceneViewFamilyContext ViewFamily(FSceneViewFamily::ConstructionValues(Viewport, GetWorld()->Scene, EngineShowFlags).SetRealtimeUpdate(true));

	FVector ViewLocation;
	FRotator ViewRotation;
	FSceneView* SceneView = Player->CalcSceneView(&ViewFamily, ViewLocation, ViewRotation, Viewport);
	return (SceneView != nullptr) ? PaniniProjectLocation(SceneView, WorldLoc, PaniniParamsMat) : WorldLoc;
}

void UUTGameViewportClient::Draw(FViewport* InViewport, FCanvas* SceneCanvas)
{
	// apply panini projection to first person weapon

	struct FSavedTransform
	{
		USceneComponent* Component;
		FTransform Transform;
		FSavedTransform(USceneComponent* InComp, const FTransform& InTransform)
			: Component(InComp), Transform(InTransform)
		{}
	};
	TArray<FSavedTransform> SavedTransforms;

	//for (FLocalPlayerIterator It(GEngine, GetWorld()); It; ++It)
	//{
	//	AUTPlayerController* UTPC = Cast<AUTPlayerController>(It->PlayerController);
	//	if (UTPC != nullptr && !UTPC->IsBehindView())
	//	{
	//		AUTCharacter* UTC = Cast<AUTCharacter>(It->PlayerController->GetViewTarget());
	//		if (UTC != nullptr && UTC->GetWeapon() != nullptr && UTC->GetWeapon()->GetMesh() != nullptr)
	//		{
	//			FSceneViewFamilyContext ViewFamily(FSceneViewFamily::ConstructionValues(Viewport, GetWorld()->Scene, EngineShowFlags).SetRealtimeUpdate(true));

	//			FVector ViewLocation;
	//			FRotator ViewRotation;
	//			FSceneView* SceneView = It->CalcSceneView(&ViewFamily, ViewLocation, ViewRotation, Viewport);
	//			if (SceneView != nullptr)
	//			{
	//				TArray<UMeshComponent*> WeaponMeshes = UTC->GetWeapon()->Get1PMeshes();
	//				TArray<USceneComponent*> Children = UTC->GetWeapon()->GetMesh()->GetAttachChildren(); // make a copy in case something below causes it to change
	//				for (USceneComponent* Attachment : Children)
	//				{
	//					// any additional weapon meshes are assumed to be projected in the shader if desired
	//					if (!Attachment->IsPendingKill() && !WeaponMeshes.Contains(Attachment) && !SavedTransforms.ContainsByPredicate([Attachment](const FSavedTransform& TestItem) { return TestItem.Component == Attachment; }))
	//					{
	//						FVector AdjustedLoc = PaniniProjectLocation(SceneView, Attachment->GetComponentLocation(), UTC->GetWeapon()->GetMesh()->GetMaterial(0));

	//						new(SavedTransforms) FSavedTransform(Attachment, Attachment->GetComponentTransform());
	//						Attachment->SetWorldLocation(AdjustedLoc);
	//						// hacky solution to attached spline mesh beams that need to update their endpoint
	//						if (Attachment->GetOwner() != nullptr && Attachment->GetOwner() != UTC->GetWeapon())
	//						{
	//							Attachment->GetOwner()->Tick(0.0f);
	//						}
	//						if (!Attachment->IsPendingKill())
	//						{
	//							Attachment->DoDeferredRenderUpdates_Concurrent();
	//						}
	//					}
	//				}
	//			}
	//		}
	//	}
	//}

	Super::Draw(InViewport, SceneCanvas);

	// revert components to their normal location
	for (const FSavedTransform& RestoreItem : SavedTransforms)
	{
		RestoreItem.Component->SetWorldTransform(RestoreItem.Transform);
	}
}

void UUTGameViewportClient::UpdateActiveSplitscreenType()
{
	int32 NumPlayers = GEngine->GetNumGamePlayers(GetWorld());
	if (NumPlayers <= 4)
	{
		Super::UpdateActiveSplitscreenType();
	}
	else
	{
		ActiveSplitscreenType = ESplitScreenType::Type(7 + (NumPlayers - 4));
	}
}

void UUTGameViewportClient::PostRender(UCanvas* Canvas)
{
#if WITH_EDITORONLY_DATA
	if (!GIsEditor)
#endif
	{
		// work around bug where we can end up with no PlayerController during initial connection (or when losing connection) by rendering an overlay to explain to the user what's going on
		TArray<ULocalPlayer*> GamePlayers;
		if (GameInstance != nullptr)
		{
			GamePlayers = GameInstance->GetLocalPlayers();
			// remove players that aren't currently set up for proper rendering (no PlayerController or currently using temp proxy while waiting for server)
			for (int32 i = GamePlayers.Num() - 1; i >= 0; i--)
			{
				if (GamePlayers[i]->PlayerController == nullptr || GamePlayers[i]->PlayerController->Player == nullptr)
				{
					GamePlayers.RemoveAt(i);
				}
			}
		}
		if (GamePlayers.Num() > 0)
		{
			Super::PostRender(Canvas);
		}
		else
		{
			UFont* Font = GetDefault<AUTHUD>()->MediumFont;
			FText Message = NSLOCTEXT("UTHUD", "WaitingForServer", "Waiting for server to respond...");
			float XL, YL;
			Canvas->SetLinearDrawColor(FLinearColor::White);
			Canvas->TextSize(Font, Message.ToString(), XL, YL);
			Canvas->DrawText(Font, Message, (Canvas->ClipX - XL) * 0.5f, (Canvas->ClipY - YL) * 0.5f);
		}
	}
}


void UUTGameViewportClient::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}

bool UUTGameViewportClient::HideCursorDuringCapture()
{
	// workaround for Slate bug where toggling the pointer visibility on/off in the PlayerController while this is enabled can cause the pointer to get locked hidden
	if (!Super::HideCursorDuringCapture())
	{
		return false;
	}
	else if (GameInstance == nullptr)
	{
		return true;
	}
	else
	{
		const TArray<ULocalPlayer*> GamePlayers = GameInstance->GetLocalPlayers();
		return (GamePlayers.Num() == 0 || GamePlayers[0] == nullptr || GamePlayers[0]->PlayerController == nullptr || !GamePlayers[0]->PlayerController->ShouldShowMouseCursor());
	}
}

void UUTGameViewportClient::ClientConnectedToServer()
{
}


ULocalPlayer* UUTGameViewportClient::SetupInitialLocalPlayer(FString& OutError)
{
	// Work around shipping not having a console
#if UE_BUILD_SHIPPING
	// Create the viewport's console.
	ViewportConsole = NewObject<UConsole>(this, GetOuterUEngine()->ConsoleClass);
	// register console to get all log messages
	GLog->AddOutputDevice(ViewportConsole);
#endif // !UE_BUILD_SHIPPING

	return Super::SetupInitialLocalPlayer(OutError);
}

void UUTGameViewportClient::SetActiveWorldOverride(UWorld* WorldOverride)
{
	ActiveWorldOverride = WorldOverride;
	SetActiveLocalPlayerControllers();
}

void UUTGameViewportClient::ClearActiveWorldOverride()
{
	ActiveWorldOverride = nullptr;
	SetActiveLocalPlayerControllers();
}

void UUTGameViewportClient::SetActiveLocalPlayerControllers()
{
	// Switch the local player's controller to the controller in the active world.
	// Not calling UPlayer::SwitchController because we don't want to null out the APlayerController::Player pointer here.
	if (GetWorld() != nullptr && GetGameInstance() != nullptr)
	{
		for (ULocalPlayer* LocalPlayer : GetGameInstance()->GetLocalPlayers())
		{
			for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
			{
				APlayerController* Controller = It->Get();
				if (Controller->Player == LocalPlayer)
				{
					LocalPlayer->PlayerController = Controller;
				}
			}
		}
	}
}

UWorld* UUTGameViewportClient::GetWorld() const
{
	if (ActiveWorldOverride != nullptr)
	{
		return ActiveWorldOverride;
	}

	return Super::GetWorld();
}