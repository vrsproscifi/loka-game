// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#include "LokaGame.h"
#include "UTGameEngine.h"

#include "Runtime/Core/Public/Features/IModularFeatures.h"
#include "AssetRegistryModule.h"
#include "UTLevelSummary.h"
#include "Engine/Console.h"
#include "Runtime/Launch/Resources/Version.h"
#include "Net/UnrealNetwork.h"
#include "UTConsole.h"
#include "UTConsole.h"


#include "UTGameInstance.h"
#if !UE_SERVER
#include "SlateBasics.h"
#include "MoviePlayer.h"
#include "SUWindowsStyle.h"
#endif

#include "Components/NodeComponent/NodeSessionMatch.h"


// prevent setting MipBias to an intentionally broken value to make textures turn solid color
//static void MipBiasClamp()
//{
//	IConsoleVariable* MipBiasVar = IConsoleManager::Get().FindConsoleVariable(TEXT("r.Streaming.MipBias"));
//	if (MipBiasVar != nullptr)
//	{
//		TConsoleVariableData<float>* FloatVar = MipBiasVar->AsVariableFloat();
//		if (FloatVar->GetValueOnGameThread() > 1.0f)
//		{
//			MipBiasVar->ClearFlags(ECVF_SetByConsole);
//			MipBiasVar->Set(1.0f);
//		}
//	}
//}
//FAutoConsoleVariableSink MipBiasSink(FConsoleCommandDelegate::CreateStatic(&MipBiasClamp));

UUTGameEngine::UUTGameEngine(const FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{
	
	static ConstructorHelpers::FObjectFinder<UTexture2D> DefaultLevelScreenshotObj(TEXT("Texture2D'/Game/RestrictedAssets/Textures/_T_NSA_2_D._T_NSA_2_D'"));
	DefaultLevelScreenshot = DefaultLevelScreenshotObj.Object;

	bFirstRun = true;
	bAllowClientNetProfile = false;

	SmoothedDeltaTime = 0.01f;

	HitchTimeThreshold = 0.05f;
	HitchScaleThreshold = 2.f;
	HitchSmoothingRate = 0.5f;
	NormalSmoothingRate = 0.1f;
	MaximumSmoothedTime = 0.04f;

	ServerMaxPredictionPing = 120.f;
	VideoRecorder = nullptr;

#if !UE_SERVER
	//ConstructorHelpers::FObjectFinder<UClass> TutorialMenuFinder(TEXT("/Game/RestrictedAssets/Tutorials/Blueprints/TutMainMenuWidget.TutMainMenuWidget_C"));
	//TutorialMenuClass = TutorialMenuFinder.Object;
#endif
}

EBrowseReturnVal::Type UUTGameEngine::Browse(FWorldContext& WorldContext, FURL URL, FString& Error)
{
  const auto omap = TEXT("omap");
  const auto ogame = TEXT("ogame");
  const auto game = TEXT("game");

  if(URL.HasOption(omap))
  {
    auto ops = const_cast<TCHAR*>(URL.GetOption(omap, nullptr));
    const auto opi = FCString::Atoi(++ops);
    const auto ope = EGameMap(opi);
    const auto map = ope.GetName();

    UE_LOG(LogInit, Display, TEXT("UUTGameEngine::Browse | Override Map | From %s to %s"), *URL.Map, *map);
    URL.Map = FString::Printf(TEXT("/Game/1LOKAgame/Maps/%s"), *map);
  }


  if (URL.HasOption(ogame))
  {
    auto ops = const_cast<TCHAR*>(URL.GetOption(ogame, nullptr));
    const auto opi = FCString::Atoi(++ops);
    const auto ope = EGameMode(opi);
    const auto gm = ope.GetName();

    const auto opt = FString::Printf(TEXT("%s=%s"), game, *gm);
    UE_LOG(LogInit, Display, TEXT("UUTGameEngine::Browse | Override GameMode | From %s to %s"), URL.GetOption(game, nullptr), *opt);

    URL.RemoveOption(game);
    URL.AddOption(*opt);
  }

    
  return Super::Browse(WorldContext, URL, Error);
}

void UUTGameEngine::Init(IEngineLoop* InEngineLoop)
{
	const FString Commandline = FCommandLine::Get();

#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
	FCommandLine::Append(TEXT(" -ddc=noshared"));
#endif

#if UE_SERVER
	if (Commandline.Contains(TEXT("nodeId=")))
	{
		FString nodeStringId;
		if (FParse::Value(FCommandLine::Get(), TEXT("nodeId="), nodeStringId))
		{
			FGuid nodeId;
			if (FGuid::Parse(nodeStringId, nodeId))
			{
				FNodeSessionMatch::TokenId = nodeId;
				const auto name = FString::Printf(TEXT("LOKA_%s"), *nodeId.ToString(EGuidFormats::Digits));
				ServerMatchSingleton = new FSystemWideCriticalSection(name);
				if (ServerMatchSingleton && ServerMatchSingleton->IsValid())
				{
					UE_LOG(LogInit, Warning, TEXT("Node Start was successfully: %s"), *nodeStringId);
				}
				else UE_LOG(LogInit, Fatal, TEXT("Node Start, Node %s was already started"), *nodeStringId);
			}
			else UE_LOG(LogInit, Fatal, TEXT("Node Start, Failed Parse GUID: %s"), *nodeStringId);
		}
		else UE_LOG(LogInit, Fatal, TEXT("Node Start, Failed Parse nodeId: %s"), *nodeStringId);
	}
	else UE_LOG(LogInit, Fatal, TEXT("Node Start, Failed Find nodeId"));
#endif

	// workaround for engine bugs when loading classes that reference UMG on a dedicated server (i.e. mutators)
	FModuleManager::Get().LoadModule("Foliage");
	FModuleManager::Get().LoadModule("BlueprintContext");

	if (bFirstRun)
	{
		#if !UE_SERVER
		if (GetMoviePlayer()->IsMovieCurrentlyPlaying())
		{
			// When the movie ends, ask if the user accepts the eula.  
			// Note we cannot prompt while the movie is playing due to threading issues
			GetMoviePlayer()->OnMoviePlaybackFinished().AddUObject(this, &UUTGameEngine::OnLoadingMoviePlaybackFinished);
		}
		#endif
	}

	UniqueAnalyticSessionGuid = FGuid::NewGuid();

	Super::Init(InEngineLoop);




	// HACK: UGameUserSettings::ApplyNonResolutionSettings() isn't virtual so we need to force our settings to be applied...
	GetGameUserSettings()->ApplySettings(true);

	UE_LOG(UT, Log, TEXT("Running %d processors (%d logical cores)"), FPlatformMisc::NumberOfCores(), FPlatformMisc::NumberOfCoresIncludingHyperthreads());
	if (FPlatformMisc::NumberOfCoresIncludingHyperthreads() < ParallelRendererProcessorRequirement)
	{
		UE_LOG(UT, Log, TEXT("Enabling r.RHICmdBypass due to not having %d logical cores"), ParallelRendererProcessorRequirement);
		IConsoleVariable* BypassVar = IConsoleManager::Get().FindConsoleVariable(TEXT("r.RHICmdBypass"));
		BypassVar->Set(1);
	}

	FParse::Value(FCommandLine::Get(), TEXT("ClientProcID="), OwningProcessID);

	if (!IsRunningDedicatedServer())
	{
		static const FName VideoRecordingFeatureName("VideoRecording");
		if (IModularFeatures::Get().IsModularFeatureAvailable(VideoRecordingFeatureName))
		{
			VideoRecorder = &IModularFeatures::Get().GetModularFeature<UTVideoRecordingFeature>(VideoRecordingFeatureName);
		}
	}
}

void UUTGameEngine::PreExit()
{
	Super::PreExit();
}


bool UUTGameEngine::GetMonitorRefreshRate(int32& MonitorRefreshRate)
{
//#if PLATFORM_WINDOWS && !UE_SERVER
//	DEVMODE DeviceMode;
//	FMemory::Memzero(DeviceMode);
//	DeviceMode.dmSize = sizeof(DEVMODE);
//
//	if (EnumDisplaySettings(NULL, -1, &DeviceMode) != 0)
//	{
//		MonitorRefreshRate = DeviceMode.dmDisplayFrequency;
//		return true;
//	}
//#endif

	return false;
}

bool UUTGameEngine::Exec(UWorld* InWorld, const TCHAR* Cmd, FOutputDevice& Out)
{
#if UE_BUILD_SHIPPING
	// make these debug commands illegal in shipping builds
	if (FParse::Command(&Cmd, TEXT("SET")) || FParse::Command(&Cmd, TEXT("SETNOPEC")) || 
		FParse::Command(&Cmd, TEXT("DISPLAYALL")) || FParse::Command(&Cmd, TEXT("DISPLAYALLLOCATION")) ||
		FParse::Command(&Cmd, TEXT("GET")) || FParse::Command(&Cmd, TEXT("GETALL")))
	{
		return true;
	}
#endif
	if (FParse::Command(&Cmd, TEXT("START")))
	{
		FWorldContext &WorldContext = GetWorldContextFromWorldChecked(InWorld);
		FURL TestURL(&WorldContext.LastURL, Cmd, TRAVEL_Absolute);
		// make sure the file exists if we are opening a local file
		if (TestURL.IsLocalInternal() && !MakeSureMapNameIsValid(TestURL.Map))
		{
			Out.Logf(TEXT("ERROR: The map '%s' does not exist."), *TestURL.Map);
			return true;
		}
		else
		{
			SetClientTravel(InWorld, Cmd, TRAVEL_Absolute);
			return true;
		}
	}
	else if (FParse::Command(&Cmd, TEXT("MONITORREFRESH")))
	{
		FString OutputString;
#if PLATFORM_WINDOWS && !UE_SERVER

		int32 CurrentRefreshRate = 0;
		if (GetMonitorRefreshRate(CurrentRefreshRate))
		{
			OutputString = FString::Printf(TEXT("Monitor frequency is %dhz"), CurrentRefreshRate);
		}
		else
		{
			OutputString = TEXT("Could not detect monitor refresh frequency");
		}
		Out.Log(OutputString);

		//int32 NewRefreshRate = FCString::Atoi(Cmd);
		//if (NewRefreshRate > 0 && NewRefreshRate != CurrentRefreshRate)
		//{
		//	DEVMODE NewSettings;
		//	FMemory::Memzero(NewSettings);
		//	NewSettings.dmSize = sizeof(NewSettings);
		//	NewSettings.dmDisplayFrequency = NewRefreshRate;
		//	NewSettings.dmFields = DM_DISPLAYFREQUENCY;
		//	LONG ChangeDisplayReturn = ChangeDisplaySettings(&NewSettings, CDS_GLOBAL | CDS_UPDATEREGISTRY);
		//	if (ChangeDisplayReturn == DISP_CHANGE_SUCCESSFUL)
		//	{
		//		OutputString = FString::Printf(TEXT("Monitor frequency was changed to %dhz"), NewSettings.dmDisplayFrequency);
		//	}
		//	else if (ChangeDisplayReturn == DISP_CHANGE_BADMODE)
		//	{
		//		OutputString = FString::Printf(TEXT("Your monitor does not support running at %dhz"), NewSettings.dmDisplayFrequency);
		//	}
		//	else
		//	{
		//		OutputString = TEXT("Error setting monitor refresh frequency");
		//	}
		//	Out.Log(OutputString);
		//}
#else
		OutputString = TEXT("Could not detect monitor refresh frequency");
		Out.Log(OutputString);
#endif

		return true;
	}
	else
	{
		return Super::Exec(InWorld, Cmd, Out);
	}

}

static TAutoConsoleVariable<int32> CVarUnsteadyFPS(
	TEXT("ut.UnsteadyFPS"), 0,
	TEXT("Causes FPS to bounce around randomly in the 85-120 range."));

static TAutoConsoleVariable<int32> CVarSmoothFrameRate(
	TEXT("ut.SmoothFrameRate"), 1,
	TEXT("Enable frame rate smoothing."));

float UUTGameEngine::GetMaxTickRate(float DeltaTime, bool bAllowFrameRateSmoothing) const
{
	float MaxTickRate = 0;
	UWorld* World = NULL;

	for (int32 WorldIndex = 0; WorldIndex < WorldList.Num(); ++WorldIndex)
	{
		if (WorldList[WorldIndex].WorldType == EWorldType::Game)
		{
			World = WorldList[WorldIndex].World();
			break;
		}
	}

	// Don't smooth here if we're a dedicated server
	if (IsRunningDedicatedServer())
	{
		if (World)
		{
			UNetDriver* NetDriver = World->GetNetDriver();
			// In network games, limit framerate to not saturate bandwidth.
			if (NetDriver && (NetDriver->GetNetMode() == NM_DedicatedServer || (NetDriver->GetNetMode() == NM_ListenServer && NetDriver->bClampListenServerTickRate)))
			{
				// We're a dedicated server, use the LAN or Net tick rate.
				MaxTickRate = FMath::Clamp(NetDriver->NetServerMaxTickRate, 10, 120);

				// Allow hubs to override the tick rate
				//AUTLobbyGameMode* LobbyGame = Cast<AUTLobbyGameMode>(World->GetAuthGameMode());
				//if (LobbyGame)
				//{
				//	MaxTickRate = LobbyGame->LobbyMaxTickRate;
				//}
			}
		}
		return MaxTickRate;
	}

	if (VideoRecorder)
	{
		uint32 VideoRecorderTickRate = 0;
		if (VideoRecorder->IsRecording(VideoRecorderTickRate))
		{
			return VideoRecorderTickRate;
		}
	}

	if (CVarUnsteadyFPS.GetValueOnGameThread())
	{
		float RandDelta = 0.f;
		// random variation in frame time because of effects, etc.
		if (FMath::FRand() < 0.5f)
		{
			RandDelta = (1.f+FMath::FRand()) * DeltaTime;
		}
		// occasional large hitches
		if (FMath::FRand() < 0.002f)
		{
			RandDelta += 0.1f;
		}

		DeltaTime += RandDelta;
		MaxTickRate = 1.f / DeltaTime;
		//UE_LOG(UT, Warning, TEXT("FORCING UNSTEADY FRAME RATE desired delta %f adding %f maxtickrate %f"),  DeltaTime, RandDelta, MaxTickRate);
	}

	if (bSmoothFrameRate && bAllowFrameRateSmoothing && CVarSmoothFrameRate.GetValueOnGameThread())
	{
		MaxTickRate = 1.f / SmoothedDeltaTime;
	}

	// clamp frame rate based on negotiated net speed
	if (World)
	{
		UNetDriver* NetDriver = World->GetNetDriver();
		if (NetDriver && NetDriver->GetNetMode() == NM_Client)
		{
			UPlayer * LocalPlayer = World->GetFirstLocalPlayerFromController();
			if (LocalPlayer)
			{
				float NetRateClamp = float(LocalPlayer->CurrentNetSpeed) / 100.f;
				MaxTickRate = MaxTickRate > 0 ? FMath::Min(MaxTickRate, NetRateClamp) : NetRateClamp;
			}
		}
	}

	// Hard cap at frame rate cap
	if (FrameRateCap > 0)
	{
		if (MaxTickRate > 0)
		{
			MaxTickRate = FMath::Min(FrameRateCap, MaxTickRate);
		}
		else
		{
			MaxTickRate = FrameRateCap;
		}
	}
	
	return MaxTickRate;
}

void UUTGameEngine::UpdateRunningAverageDeltaTime(float DeltaTime, bool bAllowFrameRateSmoothing)
{
	if (DeltaTime > SmoothedDeltaTime)
	{
		// can't slow down drop
		SmoothedDeltaTime = DeltaTime;
		// return 0.f; // @TODO FIXMESTEVE - not doing this so unsteady FPS is valid
	}
	else if (SmoothedDeltaTime > FMath::Max(HitchTimeThreshold, HitchScaleThreshold * DeltaTime))
	{
		// fast return from hitch - smoothing them makes game crappy a long time
		HitchSmoothingRate = FMath::Clamp(HitchSmoothingRate, 0.f, 1.f);
		SmoothedDeltaTime = (1.f - HitchSmoothingRate)*SmoothedDeltaTime + HitchSmoothingRate*DeltaTime;
	}
	else
	{
		// simply smooth the trajectory back up to limit mouse input variations because of difference in sampling frame time and this frame time
		NormalSmoothingRate = FMath::Clamp(NormalSmoothingRate, 0.f, 1.f);

		SmoothedDeltaTime = (1.f - NormalSmoothingRate)*SmoothedDeltaTime + NormalSmoothingRate*DeltaTime;
	}

	// Make sure that we don't try to smooth below a certain minimum
	SmoothedDeltaTime = FMath::Clamp(SmoothedDeltaTime, 0.001f, MaximumSmoothedTime);

	//UE_LOG(UT, Warning, TEXT("SMOOTHED TO %f"), SmoothedDeltaTime);
}


void UUTGameEngine::AddAssetRegistry(const FString& PakFilename)
{
	FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>("AssetRegistry");

	int32 DashPosition = PakFilename.Find(TEXT("-"), ESearchCase::CaseSensitive, ESearchDir::FromEnd);
	if (DashPosition != -1)
	{
		FString DashlessPakFilename = PakFilename.Left(DashPosition);
		FString AssetRegistryName = DashlessPakFilename + TEXT("-AssetRegistry.bin");
		FArrayReader SerializedAssetData;
		if (FFileHelper::LoadFileToArray(SerializedAssetData, *(FPaths::ProjectDir() / AssetRegistryName)))
		{
			// serialize the data with the memory reader (will convert FStrings to FNames, etc)
			AssetRegistryModule.Get().Serialize(SerializedAssetData);
			UE_LOG(UT, Log, TEXT("%s merged into the asset registry"), *AssetRegistryName);
		}
		else
		{
			UE_LOG(UT, Warning, TEXT("%s could not be found"), *AssetRegistryName);
		}
	}
}

void UUTGameEngine::SetupLoadingScreen()
{
#if !UE_SERVER
/*
	if (IsMoviePlayerEnabled())
	{
		FLoadingScreenAttributes LoadingScreen;
		LoadingScreen.bAutoCompleteWhenLoadingCompletes = true;
		// TODO: probably need to do something to handle aspect ratio
		//LoadingScreen.WidgetLoadingScreen = SNew(SImage).Image(&LoadingScreenImage);
		LoadingScreen.WidgetLoadingScreen = SNew(SImage).Image(SUWindowsStyle::Get().GetBrush("LoadingScreen"));
		GetMoviePlayer()->SetupLoadingScreen(LoadingScreen);
		FCoreUObjectDelegates::PreLoadMap.Broadcast(TEXT(""));
	}
*/
#endif
}

static FName UT_DEFAULT_LOADING(TEXT("UT.LoadingScreen"));


bool UUTGameEngine::IsCloudAndLocalContentInSync()
{
	bool bFoundFileInCloud = false;

	for (auto LocalIt = LocalContentChecksums.CreateConstIterator(); LocalIt; ++LocalIt)
	{
		bFoundFileInCloud = false;
		for (auto CloudIt = CloudContentChecksums.CreateConstIterator(); CloudIt; ++CloudIt)
		{
			if (CloudIt.Key() == LocalIt.Key())
			{
				if (CloudIt.Value() == LocalIt.Value())
				{
					bFoundFileInCloud = true;
					UE_LOG(UT, Log, TEXT("%s is properly synced in the cloud"), *LocalIt.Key());
				}
				else
				{
					UE_LOG(UT, Log, TEXT("%s is on the cloud but out of date"), *LocalIt.Key());
				}
			}
		}

		if (!bFoundFileInCloud)
		{
			UE_LOG(UT, Log, TEXT("%s is not synced on the cloud"), *LocalIt.Key());
			return false;
		}
	}

	return true;
}

FString UUTGameEngine::MD5Sum(const TArray<uint8>& Data)
{
	uint8 Digest[16];

	FMD5 Md5Gen;

	Md5Gen.Update((uint8*)Data.GetData(), Data.Num());
	Md5Gen.Final(Digest);

	FString MD5;
	for (int32 i = 0; i < 16; i++)
	{
		MD5 += FString::Printf(TEXT("%02x"), Digest[i]);
	}

	return MD5;
}

void UUTGameEngine::HandleNetworkFailure(UWorld* World, UNetDriver* NetDriver, ENetworkFailure::Type FailureType, const FString& ErrorString)
{
	// ignore connection loss on game net driver if we have a pending one; this happens during standard blocking server travel
	if (NetDriver == nullptr || NetDriver->NetDriverName != NAME_GameNetDriver || GEngine->GetWorldContextFromWorld(World)->PendingNetGame == nullptr ||
		(FailureType != ENetworkFailure::ConnectionLost && FailureType != ENetworkFailure::FailureReceived) )
	{
		Super::HandleNetworkFailure(World, NetDriver, FailureType, ErrorString);
	}
}

bool UUTGameEngine::ShouldShutdownWorldNetDriver()
{
	return false;
}

void UUTGameEngine::OnLoadingMoviePlaybackFinished()
{
}
