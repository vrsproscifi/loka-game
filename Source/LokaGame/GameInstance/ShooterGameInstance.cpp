﻿// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

/*=============================================================================
	ShooterGameInstance.cpp
=============================================================================*/

#include "LokaGame.h"
#include "GameInstance/ShooterGameInstance.h"
#include "OnlineKeyValuePair.h"

#include "Slate/SLoadingScreen.h"
#include "UTGameUserSettings.h"
#include "SNotifyList.h"
#include "SMessageTip.h"
#include "Localization/BaseStringsTable.h"
#if !UE_SERVER && !WITH_EDITOR	
#include "MoviePlayer.h"
#include "MoviePlayerClasses.h"
#endif

#if !UE_SERVER
#include "Notify/SMessageBox.h"
#endif

namespace ShooterGameInstanceState
{
	const FName None = FName(TEXT("None"));
	const FName PendingInvite = FName(TEXT("PendingInvite"));
	const FName WelcomeScreen = FName(TEXT("WelcomeScreen"));
	const FName MainMenu = FName(TEXT("MainMenu"));
	const FName MessageMenu = FName(TEXT("MessageMenu"));
	const FName Playing = FName(TEXT("Playing"));
}


UShooterGameInstance::UShooterGameInstance(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
	, bIsOnline(true) // Default to online
	, bIsLicensed(true) // Default to licensed (should have been checked by OS on boot)
	, CurrentTutorialId(INDEX_NONE)
{
#if !UE_SERVER && !WITH_EDITOR	
	loadingAttributes = MakeShareable(new FLoadingScreenAttributes());
	
	loadingAttributes->bAutoCompleteWhenLoadingCompletes = false;
	loadingAttributes->bMoviesAreSkippable = false;
	loadingAttributes->MinimumLoadingScreenDisplayTime = 100.0f;
	loadingAttributes->bWaitForManualStop = true;
	loadingAttributes->PlaybackType = EMoviePlaybackType::MT_Looped;
#endif
	StartMessages.Default();

	if (!HasAnyFlags(RF_ClassDefaultObject | RF_ArchetypeObject))
	{
#if !UE_SERVER && !WITH_EDITOR		
		moviePlayer = GetMoviePlayer();

		SAssignNew(loadingScreen, SLoadingScreen);		

		//if (moviePlayer.IsValid() && moviePlayer->IsMovieCurrentlyPlaying())
		//{
		//	moviePlayerDelegate = moviePlayer->OnMoviePlaybackFinished().AddUObject(this, &UShooterGameInstance::OnMovieEnd);
		//}
#endif
	}
}




void UShooterGameInstance::Init()
{
	Super::Init();

#if !UE_SERVER && !WITH_EDITOR	
	moviePlayer = GetMoviePlayer();
#endif

	IgnorePairingChangeForControllerId = -1;
	//CurrentConnectionStatus = EOnlineServerConnectionStatus::Connected;

	//// game requires the ability to ID users.
	//const auto OnlineSub = IOnlineSubsystem::Get();
	//check(OnlineSub);
	//const auto IdentityInterface = OnlineSub->GetIdentityInterface();
	//check(IdentityInterface.IsValid());
	//
	//const auto SessionInterface = OnlineSub->GetSessionInterface();
	//check(SessionInterface.IsValid());
	//
	//// bind any OSS delegates we needs to handle
	//for (int i = 0; i < MAX_LOCAL_PLAYERS; ++i)
	//{
	//	IdentityInterface->AddOnLoginStatusChangedDelegate_Handle(i, FOnLoginStatusChangedDelegate::CreateUObject(this, &UShooterGameInstance::HandleUserLoginChanged));
	//}
	//

	FCoreDelegates::ApplicationWillDeactivateDelegate.AddUObject(this, &UShooterGameInstance::HandleAppWillDeactivate);

	FCoreDelegates::ApplicationWillEnterBackgroundDelegate.AddUObject(this, &UShooterGameInstance::HandleAppSuspend);
	FCoreDelegates::ApplicationHasEnteredForegroundDelegate.AddUObject(this, &UShooterGameInstance::HandleAppResume);

	FCoreDelegates::OnSafeFrameChangedEvent.AddUObject(this, &UShooterGameInstance::HandleSafeFrameChanged);
	FCoreDelegates::OnControllerConnectionChange.AddUObject(this, &UShooterGameInstance::HandleControllerConnectionChange);
	FCoreDelegates::ApplicationLicenseChange.AddUObject(this, &UShooterGameInstance::HandleAppLicenseUpdate);

	FCoreUObjectDelegates::PreLoadMap.AddUObject(this, &UShooterGameInstance::OnPreLoadMap);
	FCoreUObjectDelegates::PostLoadMapWithWorld.AddUObject(this, &UShooterGameInstance::OnPostLoadMap);


	bPendingEnableSplitscreen = false;

	// Register delegate for ticker callback
	TickDelegate = FTickerDelegate::CreateUObject(this, &UShooterGameInstance::Tick);
	TickDelegateHandle = FTicker::GetCoreTicker().AddTicker(TickDelegate);

	//=========================================================================
	FMath::RandInit(FDateTime::Now().ToUnixTimestamp());

	//=========================================================================

	for (auto bone : BonesDamageRate)
	{
		BonesDamageRateMap.Add(bone.Name, bone.Rate);
	}
}

void UShooterGameInstance::Shutdown()
{
#if !UE_SERVER
	SMessageBox::ResetQueueMessages();
	SMessageBox::Reset();
	SNotifyList::Reset();

	SMessageTip::ResetQueue();
	SMessageTip::Reset();

	FLokaBaseStringsTable::Instance.Reset();
#endif
	Super::Shutdown();

	// Unregister ticker delegate
	FTicker::GetCoreTicker().RemoveTicker(TickDelegateHandle);
}


//void UShooterGameInstance::HandleNetworkConnectionStatusChanged(  EOnlineServerConnectionStatus::Type LastConnectionStatus, EOnlineServerConnectionStatus::Type ConnectionStatus )
//{
//	UE_LOG( LogOnlineGame, Warning, TEXT( "UShooterGameInstance::HandleNetworkConnectionStatusChanged: %s" ), EOnlineServerConnectionStatus::ToString( ConnectionStatus ) );
//
//#if SHOOTER_CONSOLE_UI
//	// If we are disconnected from server, and not currently at (or heading to) the welcome screen
//	// then display a message on consoles
//	if (	bIsOnline && 
//			PendingState != ShooterGameInstanceState::WelcomeScreen &&
//			CurrentState != ShooterGameInstanceState::WelcomeScreen && 
//			ConnectionStatus != EOnlineServerConnectionStatus::Connected )
//	{
//		UE_LOG( LogOnlineGame, Log, TEXT( "UShooterGameInstance::HandleNetworkConnectionStatusChanged: Going to main menu" ) );
//
//		// Display message on consoles
//#if PLATFORM_XBOXONE
//		const FText ReturnReason	= NSLOCTEXT( "NetworkFailures", "ServiceUnavailable", "Connection to Xbox LIVE has been lost." );
//#elif PLATFORM_PS4
//		const FText ReturnReason	= NSLOCTEXT( "NetworkFailures", "ServiceUnavailable", "Connection to \"PSN\" has been lost." );
//#else
//		const FText ReturnReason	= NSLOCTEXT( "NetworkFailures", "ServiceUnavailable", "Connection has been lost." );
//#endif
//		const FText OKButton		= NSLOCTEXT( "DialogButtons", "OKAY", "OK" );
//		
//		ShowMessageThenGotoState( ReturnReason, OKButton, FText::GetEmpty(), ShooterGameInstanceState::MainMenu );
//	}
//
//	CurrentConnectionStatus = ConnectionStatus;
//#endif
//}

//void UShooterGameInstance::HandleSessionFailure( const FUniqueNetId& NetId, OnUserCanPlay::Type FailureType )
//{
//	UE_LOG( LogOnlineGame, Warning, TEXT( "UShooterGameInstance::HandleSessionFailure: %u" ), (uint32)FailureType );
//
//#if SHOOTER_CONSOLE_UI
//	// If we are not currently at (or heading to) the welcome screen then display a message on consoles
//	if (	bIsOnline && 
//			PendingState != ShooterGameInstanceState::WelcomeScreen &&
//			CurrentState != ShooterGameInstanceState::WelcomeScreen )
//	{
//		UE_LOG( LogOnlineGame, Log, TEXT( "UShooterGameInstance::HandleSessionFailure: Going to main menu" ) );
//
//		// Display message on consoles
//#if PLATFORM_XBOXONE
//		const FText ReturnReason	= NSLOCTEXT( "NetworkFailures", "ServiceUnavailable", "Connection to Xbox LIVE has been lost." );
//#elif PLATFORM_PS4
//		const FText ReturnReason	= NSLOCTEXT( "NetworkFailures", "ServiceUnavailable", "Connection to PSN has been lost." );
//#else
//		const FText ReturnReason	= NSLOCTEXT( "NetworkFailures", "ServiceUnavailable", "Connection has been lost." );
//#endif
//		const FText OKButton		= NSLOCTEXT( "DialogButtons", "OKAY", "OK" );
//		
//		ShowMessageThenGotoState( ReturnReason, OKButton,  FText::GetEmpty(), ShooterGameInstanceState::MainMenu );
//	}
//#endif
//}

void UShooterGameInstance::OnPreLoadMap(const FString& InMap)
{
#if !UE_SERVER && !WITH_EDITOR	
	moviePlayer = GetMoviePlayer();
	if (moviePlayer && !moviePlayerDelegate.IsValid() && FFlagsHelper::HasAnyFlags(CurrentLoadingScreenMode, FFlagsHelper::ToFlag<int32>(ELoadingScreenMode::CustomShow)) == false)
	{
		if (IsMoviePlayerEnabled() && loadingScreen.IsValid())
		{
			loadingScreen->SetLoadingTitle(NSLOCTEXT("Instance", "Instance.Loading", "Loading"));
			loadingScreen->SetRandomImageAndTip();
			loadingAttributes->WidgetLoadingScreen = loadingScreen;
			moviePlayer->SetupLoadingScreen(*loadingAttributes);
			moviePlayer->PlayMovie();
		}
	}		
#endif
}

void UShooterGameInstance::OnPostLoadMap(UWorld* InWorld)
{
#if !UE_SERVER && !WITH_EDITOR	
	if (FFlagsHelper::HasAnyFlags(CurrentLoadingScreenMode, FFlagsHelper::ToFlag<int32>(ELoadingScreenMode::CustomHide)) == false)
	{
		if (moviePlayer && !moviePlayerDelegate.IsValid())
		{
			moviePlayer->StopMovie();
		}
	}
#endif
}

void UShooterGameInstance::InitWorldForTutorials(UWorld* InWorld)
{
	//for (auto TargetInstancePair : TutorialsInstance)
	//{
	//	if (TargetInstancePair.Value)
	//	{
	//		TargetInstancePair.Value->World = InWorld;
	//	}
	//}
}

// TODO
//void UShooterGameInstance::InitTutorialsData(const FListOfInstance& InData)
//{
//	if (TutorialsInstance.Num() != TutorialsClass.Num())
//	{
//		for (auto TargetClassPair : TutorialsClass)
//		{
//			if (TargetClassPair.Value)
//			{
//				auto TutorialInstance = NewObject<UTutorialBase>(this, TargetClassPair.Value);
//				TutorialInstance->TutorialIndex = TargetClassPair.Key;
//				TutorialsInstance.Add(TargetClassPair.Key, TutorialInstance);
//			}
//		}
//	}
//
//	for (auto tut : InData.Tutorials)
//	{
//		if (TutorialsInstance.Contains(tut.TutorialId))
//		{
//			TutorialsInstance.FindChecked(tut.TutorialId)->TutorialServerData = tut;
//			TutorialsInstance.FindChecked(tut.TutorialId)->TutorialServerData.Steps.Sort([](const FTutorialStepView& A, const FTutorialStepView& B) { return A.StepId < B.StepId; });
//		}
//	}
//}

void UShooterGameInstance::OnMovieEnd()
{
#if !UE_SERVER && !WITH_EDITOR	
	if(moviePlayer)
	{
		moviePlayer->OnMoviePlaybackFinished().Remove(moviePlayerDelegate);
		moviePlayerDelegate.Reset();

		if (!moviePlayer->IsLoadingFinished())
		{
			OnPreLoadMap("");
		}
	}
#endif
}



void UShooterGameInstance::StartGameInstance()
{
	Super::StartGameInstance();
#if !WITH_EDITOR
	GetMutableDefault<UUTGameUserSettings>()->InitSettings();
#endif
	//=========================================================================
	//								Loading Screen
#if !UE_SERVER && !WITH_EDITOR	
	moviePlayer = GetMoviePlayer();
	if (moviePlayer && moviePlayer->IsMovieCurrentlyPlaying() && !moviePlayerDelegate.IsValid())
	{
		moviePlayerDelegate = moviePlayer->OnMoviePlaybackFinished().AddUObject(this, &UShooterGameInstance::OnMovieEnd);
	}	
#endif
}


bool UShooterGameInstance::LoadFrontEndMap(const FString& MapName)
{
	bool bSuccess = true;

	// if already loaded, do nothing
	UWorld* const World = GetWorld();
	if (World)
	{
		FString const CurrentMapName = *World->PersistentLevel->GetOutermost()->GetName();
		//if (MapName.Find(TEXT("Highrise")) != -1)
		if (CurrentMapName == MapName)
		{
			return bSuccess;
		}
	}

	FString Error;
	EBrowseReturnVal::Type BrowseRet = EBrowseReturnVal::Failure;
	FURL URL(
		*FString::Printf(TEXT("%s"), *MapName)
		);

	if (URL.Valid && !HasAnyFlags(RF_ClassDefaultObject)) //CastChecked<UEngine>() will fail if using Default__ShooterGameInstance, so make sure that we're not default
	{
		BrowseRet = GetEngine()->Browse(*WorldContext, URL, Error);

		// Handle failure.
		if (BrowseRet != EBrowseReturnVal::Success)
		{
			UE_LOG(LogLoad, Fatal, TEXT("%s"), *FString::Printf(TEXT("Failed to enter %s: %s. Please check the log for errors."), *MapName, *Error));
			bSuccess = false;
		}
	}
	return bSuccess;
}

void UShooterGameInstance::TravelLocalSessionFailure(UWorld *World, ETravelFailure::Type FailureType, const FString& ReasonString)
{
	//TODO:SeNTike
	//ALobbyPlayerCharacter* const FirstPC = Cast<ALobbyPlayerCharacter>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
	//if (FirstPC != nullptr)
	//{
	//	FText ReturnReason = NSLOCTEXT("NetworkErrors", "JoinSessionFailed", "Join Session failed.");
	//	if (ReasonString.IsEmpty() == false)
	//	{
	//		ReturnReason = FText::Format(NSLOCTEXT("NetworkErrors", "JoinSessionFailedReasonFmt", "Join Session failed. {0}"), FText::FromString(ReasonString));
	//	}
	//
	//	FText OKButton = NSLOCTEXT("DialogButtons", "OKAY", "OK");
	//	//TODO: Added error message window
	//	ShowMessageThenGoMain(ReturnReason, OKButton, FText::GetEmpty());
	//}
}


void UShooterGameInstance::SetPresenceForLocalPlayers(const FVariantData& PresenceData)
{
	//const auto Presence = Online::GetPresenceInterface();
	//if (Presence.IsValid())
	//{
	//	for (int i = 0; i < LocalPlayers.Num(); ++i)
	//	{
	//		const TSharedPtr<const FUniqueNetId> UserId = LocalPlayers[i]->GetPreferredUniqueNetId();
	//
	//		if (UserId.IsValid())
	//		{
	//			FOnlineUserPresenceStatus PresenceStatus;
	//			PresenceStatus.Properties.Add(DefaultPresenceKey, PresenceData);
	//
	//			Presence->SetPresence(*UserId, PresenceStatus);
	//		}
	//	}
	//}
}

void UShooterGameInstance::CleanupSessionOnReturnToMenu()
{
	bool bPendingOnlineOp = false;

}

void UShooterGameInstance::LabelPlayerAsQuitter(ULocalPlayer* LocalPlayer) const
{
	//AShooterPlayerState* const PlayerState = LocalPlayer && LocalPlayer->PlayerController ? Cast<AShooterPlayerState>(LocalPlayer->PlayerController->GetPlayerState()) : nullptr;	
	//if(PlayerState)
	//{
	//	PlayerState->SetQuitter(true);
	//}
}

void UShooterGameInstance::RemoveNetworkFailureHandlers()
{
	// Remove the local travel failure bindings if they exist
	if (GEngine->OnTravelFailure().IsBoundToObject(this) == true)
	{
		GEngine->OnTravelFailure().Remove(TravelLocalSessionFailureDelegateHandle);
	}
}

void UShooterGameInstance::AddNetworkFailureHandlers()
{
	// Add network/travel error handlers (if they are not already there)
	if (GEngine->OnTravelFailure().IsBoundToObject(this) == false)
	{
		TravelLocalSessionFailureDelegateHandle = GEngine->OnTravelFailure().AddUObject(this, &UShooterGameInstance::TravelLocalSessionFailure);
	}
}

bool UShooterGameInstance::PlayDemo(ULocalPlayer* LocalPlayer, const FString& DemoName)
{
	ShowLoadingScreen();

	// Play the demo
	PlayReplay(DemoName);
	
	return true;
}

bool UShooterGameInstance::Tick(float DeltaSeconds)
{
	// Dedicated server doesn't need to worry about game state
	if (IsRunningDedicatedServer() == true)
	{
		return true;
	}

	//MaybeChangeState();


	return true;
}

//void UShooterGameInstance::HandleUserLoginChanged(int32 GameUserIndex, ELoginStatus::Type PreviousLoginStatus, ELoginStatus::Type LoginStatus, const FUniqueNetId& UserId)
//{
//	const bool bDowngraded = (LoginStatus == ELoginStatus::NotLoggedIn && !GetIsOnline()) || (LoginStatus != ELoginStatus::LoggedIn && GetIsOnline());	
//
//	UE_LOG( LogOnline, Log, TEXT( "HandleUserLoginChanged: bDownGraded: %i" ), (int)bDowngraded );
//
//	TSharedPtr<GenericApplication> GenericApplication = FSlateApplication::Get().GetPlatformApplication();
//	bIsLicensed = GenericApplication->ApplicationLicenseValid();
//
//	// Find the local player associated with this unique net id
//	ULocalPlayer * LocalPlayer = FindLocalPlayerFromUniqueNetId( UserId );
//
//	// If this user is signed out, but was previously signed in, punt to welcome (or remove splitscreen if that makes sense)
//	if ( LocalPlayer != nullptr )
//	{
//		if (bDowngraded)
//		{
//			UE_LOG( LogOnline, Log, TEXT( "HandleUserLoginChanged: Player logged out: %s" ), *UserId.ToString() );
//
//			LabelPlayerAsQuitter(LocalPlayer);
//
//			// Check to see if this was the master, or if this was a split-screen player on the client
//			if ( LocalPlayer == GetFirstGamePlayer() || GetIsOnline() )
//			{
//				HandleSignInChangeMessaging();
//			}
//			else
//			{
//				// Remove local split-screen players from the list
//				RemoveExistingLocalPlayer( LocalPlayer );
//			}
//		}
//	}
//}

void UShooterGameInstance::HandleAppWillDeactivate()
{
	//if (CurrentState == ShooterGameInstanceState::Playing)
	//{
	//	// Just have the first player controller pause the game.
	//	UWorld* const GameWorld = GetWorld();
	//	if (GameWorld)
	//	{
	//		// protect against a second pause menu loading on top of an existing one if someone presses the Jewel / PS buttons.
	//		bool bNeedsPause = true;
	//		for (FConstControllerIterator It = GameWorld->GetControllerIterator(); It; ++It)
	//		{
	//			AShooterPlayerController* Controller = Cast<AShooterPlayerController>(*It);
	//			if (Controller && (Controller->IsPaused() || Controller->IsGameMenuVisible()))
	//			{
	//				bNeedsPause = false;
	//				break;
	//			}
	//		}
	//
	//		if (bNeedsPause)
	//		{
	//			AShooterPlayerController* const Controller = Cast<AShooterPlayerController>(GameWorld->GetFirstPlayerController());
	//			if (Controller)
	//			{
	//				Controller->ShowInGameMenu();
	//			}
	//		}
	//	}
	//}
}

void UShooterGameInstance::HandleAppSuspend()
{
	// Players will lose connection on resume. However it is possible the game will exit before we get a resume, so we must kick off round end events here.
	//UE_LOG( LogOnline, Warning, TEXT( "UShooterGameInstance::HandleAppSuspend" ) );
	//UWorld* const World = GetWorld(); 
	//AShooterGameState* const GameState = World != nullptr ? World->GetGameState<AShooterGameState>() : NULL;
	//
	//if ( CurrentState != ShooterGameInstanceState::None && CurrentState != GetInitialState() )
	//{
	//	UE_LOG( LogOnline, Warning, TEXT( "UShooterGameInstance::HandleAppSuspend: Sending round end event for players" ) );
	//
	//	// Send round end events for local players
	//	for (int i = 0; i < LocalPlayers.Num(); ++i)
	//	{
	//		auto ShooterPC = Cast<AShooterPlayerController>(LocalPlayers[i]->PlayerController);
	//		if (ShooterPC)
	//		{
	//			// Assuming you can't win if you quit early
	//			ShooterPC->ClientSendRoundEndEvent(false, GameState->ElapsedTime);
	//		}
	//	}
	//}
}

void UShooterGameInstance::HandleAppResume()
{
	//UE_LOG( LogOnline, Log, TEXT( "UShooterGameInstance::HandleAppResume" ) );
	//
	//if ( CurrentState != ShooterGameInstanceState::None && CurrentState != GetInitialState() )
	//{
	//	UE_LOG( LogOnline, Warning, TEXT( "UShooterGameInstance::HandleAppResume: Attempting to sign out players" ) );
	//
	//	for ( int32 i = 0; i < LocalPlayers.Num(); ++i )
	//	{
	//		if ( LocalPlayers[i]->GetCachedUniqueNetId().IsValid() && !IsLocalPlayerOnline( LocalPlayers[i] ) )
	//		{
	//			UE_LOG( LogOnline, Log, TEXT( "UShooterGameInstance::HandleAppResume: Signed out during resume." ) );
	//			HandleSignInChangeMessaging();
	//			break;
	//		}
	//	}
	//}
}

void UShooterGameInstance::HandleAppLicenseUpdate()
{
	TSharedPtr<GenericApplication> GenericApplication = FSlateApplication::Get().GetPlatformApplication();
	bIsLicensed = GenericApplication->ApplicationLicenseValid();
}

void UShooterGameInstance::HandleSafeFrameChanged()
{
	UCanvas::UpdateAllCanvasSafeZoneData();
}

void UShooterGameInstance::RemoveExistingLocalPlayer(ULocalPlayer* ExistingPlayer)
{
	check(ExistingPlayer);
	//if (ExistingPlayer->PlayerController != nullptr)
	//{
	//	// Kill the player
	//	AShooterCharacter* MyPawn = Cast<AShooterCharacter>(ExistingPlayer->PlayerController->GetPawn());
	//	if ( MyPawn )
	//	{
	//		MyPawn->KilledBy(NULL);
	//	}
	//}

	// Remove local split-screen players from the list
	RemoveLocalPlayer( ExistingPlayer );
}

void UShooterGameInstance::RemoveSplitScreenPlayers()
{
	// if we had been split screen, toss the extra players now
	// remove every player, back to front, except the first one
	while (LocalPlayers.Num() > 1)
	{
		ULocalPlayer* const PlayerToRemove = LocalPlayers.Last();
		RemoveExistingLocalPlayer(PlayerToRemove);
	}
}


void UShooterGameInstance::HandleControllerConnectionChange( bool bIsConnection, int32 Unused, int32 GameUserIndex )
{
	//UE_LOG(LogOnlineGame, Log, TEXT("UShooterGameInstance::HandleControllerConnectionChange bIsConnection %d GameUserIndex %d"),
	//	bIsConnection, GameUserIndex);

	if(!bIsConnection)
	{
		// Controller was disconnected

		// Find the local player associated with this user index
		ULocalPlayer * LocalPlayer = FindLocalPlayerFromControllerId( GameUserIndex );

		if ( LocalPlayer == nullptr )
		{
			return;		// We don't care about players we aren't tracking
		}

		// Invalidate this local player's controller id.
		LocalPlayer->SetControllerId(-1);
	}
}


TSharedPtr<const FUniqueNetId > UShooterGameInstance::GetUniqueNetIdFromControllerId( const int ControllerId )
{
	//IOnlineIdentityPtr OnlineIdentityInt = Online::GetIdentityInterface();
	//
	//if ( OnlineIdentityInt.IsValid() )
	//{
	//	TSharedPtr<const FUniqueNetId> UniqueId = OnlineIdentityInt->GetUniquePlayerId( ControllerId );
	//
	//	if ( UniqueId.IsValid() )
	//	{
	//		return UniqueId;
	//	}
	//}

	return nullptr;
}

void UShooterGameInstance::SetIsOnline(bool bInIsOnline)
{
	bIsOnline = bInIsOnline;
	//IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
	//
	//if (OnlineSub)
	//{
	//	for (int32 i = 0; i < LocalPlayers.Num(); ++i)
	//	{
	//		ULocalPlayer* LocalPlayer = LocalPlayers[i];
	//
	//		TSharedPtr<const FUniqueNetId> PlayerId = LocalPlayer->GetPreferredUniqueNetId();
	//		if (PlayerId.IsValid())
	//		{
	//			OnlineSub->SetUsingMultiplayerFeatures(*PlayerId, bIsOnline);
	//		}
	//	}		
	//}
}

void UShooterGameInstance::TravelToSession(const FName& SessionName)
{
	// Added to handle failures when joining using quickmatch (handles issue of joining a game that just ended, i.e. during game ending timer)
	AddNetworkFailureHandlers();
	ShowLoadingScreen();
}

void UShooterGameInstance::SetIgnorePairingChangeForControllerId( const int32 ControllerId )
{
	IgnorePairingChangeForControllerId = ControllerId;
}

bool UShooterGameInstance::IsLocalPlayerOnline(ULocalPlayer* LocalPlayer)
{
	if (LocalPlayer == nullptr)
	{
		return false;
	}
	//const auto OnlineSub = IOnlineSubsystem::Get();
	//if(OnlineSub)
	//{
	//	const auto IdentityInterface = OnlineSub->GetIdentityInterface();
	//	if(IdentityInterface.IsValid())
	//	{
	//		auto UniqueId = LocalPlayer->GetCachedUniqueNetId();
	//		if (UniqueId.IsValid())
	//		{
	//			const auto LoginStatus = IdentityInterface->GetLoginStatus(*UniqueId);
	//			if(LoginStatus == ELoginStatus::LoggedIn)
	//			{
	//				return true;
	//			}
	//		}
	//	}
	//}

	return false;
}

bool UShooterGameInstance::ValidatePlayerForOnlinePlay(ULocalPlayer* LocalPlayer)
{
	// Get the viewport
//	UShooterGameViewportClient * ShooterViewport = Cast<UShooterGameViewportClient>(GetGameViewportClient());

//#if PLATFORM_XBOXONE
//	if (CurrentConnectionStatus != EOnlineServerConnectionStatus::Connected)
//	{
//		// Don't let them play online if they aren't connected to Xbox LIVE
//		if (ShooterViewport != nullptr)
//		{
//			const FText Msg				= NSLOCTEXT("NetworkFailures", "ServiceDisconnected", "You must be connected to the Xbox LIVE service to play online.");
//			const FText OKButtonString	= NSLOCTEXT("DialogButtons", "OKAY", "OK");
//
//			ShooterViewport->ShowDialog( 
//				NULL,
//				EShooterDialogType::Generic,
//				Msg,
//				OKButtonString,
//				FText::GetEmpty(),
//				FOnClicked::CreateUObject(this, &UShooterGameInstance::OnConfirmGeneric),
//				FOnClicked::CreateUObject(this, &UShooterGameInstance::OnConfirmGeneric)
//			);
//		}
//
//		return false;
//	}
//#endif

	//if (!IsLocalPlayerOnline(LocalPlayer))
	//{
	//	// Don't let them play online if they aren't online
	//	if (ShooterViewport != nullptr)
	//	{
	//		const FText Msg				= NSLOCTEXT("NetworkFailures", "MustBeSignedIn", "You must be signed in to play online");
	//		const FText OKButtonString	= NSLOCTEXT("DialogButtons", "OKAY", "OK");
	//
	//		//ShooterViewport->ShowDialog( 
	//		//	NULL,
	//		//	EShooterDialogType::Generic,
	//		//	Msg,
	//		//	OKButtonString,
	//		//	FText::GetEmpty(),
	//		//	FOnClicked::CreateUObject(this, &UShooterGameInstance::OnConfirmGeneric),
	//		//	FOnClicked::CreateUObject(this, &UShooterGameInstance::OnConfirmGeneric)
	//		//);
	//	}
	//
	//	return false;
	//}

	return true;
}

FReply UShooterGameInstance::OnConfirmGeneric()
{
	//UShooterGameViewportClient * ShooterViewport = Cast<UShooterGameViewportClient>(GetGameViewportClient());
	//if(ShooterViewport)
	//{
	//	ShooterViewport->HideDialog();
	//}

	return FReply::Handled();
}

//========================================================================================[ Loading screen custom functions, need refactoring loading screen

void UShooterGameInstance::SetLoadingScreenMode(const int32 InMode)
{
	CurrentLoadingScreenMode = InMode;
}

bool UShooterGameInstance::ShowLoadingScreen()
{
#if !UE_SERVER && !WITH_EDITOR	
	if (FFlagsHelper::HasAnyFlags(CurrentLoadingScreenMode, FFlagsHelper::ToFlag<int32>(ELoadingScreenMode::CustomShow)))
	{
		moviePlayer = GetMoviePlayer();
		if (moviePlayer && !moviePlayerDelegate.IsValid())
		{
			if (IsMoviePlayerEnabled() && loadingScreen.IsValid())
			{
				loadingScreen->SetLoadingTitle(NSLOCTEXT("Instance", "Instance.Loading", "Loading"));
				loadingScreen->SetRandomImageAndTip();
				loadingAttributes->WidgetLoadingScreen = loadingScreen;
				moviePlayer->SetupLoadingScreen(*loadingAttributes);
				return moviePlayer->PlayMovie();
			}
		}
	
	}
#endif	
	return false;
}

bool UShooterGameInstance::HideLoadingScreen()
{
#if !UE_SERVER && !WITH_EDITOR	
	if (FFlagsHelper::HasAnyFlags(CurrentLoadingScreenMode, FFlagsHelper::ToFlag<int32>(ELoadingScreenMode::CustomHide)))
	{
		if (moviePlayer && !moviePlayerDelegate.IsValid())
		{
			moviePlayer->StopMovie();
			return true;
		}
	}
#endif	
	return false;
}