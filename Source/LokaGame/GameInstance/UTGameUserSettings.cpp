// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "LokaGame.h"
#include "UTGameEngine.h"
#include "UTGameUserSettings.h"
#include "UTLocalPlayer.h"
#include "LokaGameUserSettings.h"
#include "AudioDevice.h"

namespace EUTGameUserSettingsVersion
{
	enum Type
	{
		UT_GAMEUSERSETTINGS_VERSION = 1
	};
}

uint32 UUTGameUserSettings::CurrentInputVersion = 5;
uint32 UUTGameUserSettings::CurrentGraphicVersion = 1;


UUTGameUserSettings::UUTGameUserSettings(const class FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
	, AAMode(0)
	, InitialBenchmarkState(-1)
	, BotSpeech(BSO_All)
	, bHRTFEnabled(false)
	, bDisableKeyboardLighting(false)
	, bShowPing(true)
	, bShowFPS(true)
	, bShouldSuppressLanWarning(false)
	, bRequestBenchmark(false)
	, bBenchmarkInProgress(false)
	, bBenchmarkSaveSettingsOnceDetected(false)
	, TickCount(0)
	, bShowDamageNumbers(true)
{
	SetToDefaults();

	RHIGetAvailableResolutions(ScreenResolutionArray, true);
}

bool UUTGameUserSettings::IsVersionValid()
{
	return Super::IsVersionValid();
}

void UUTGameUserSettings::UpdateVersion()
{
	Super::UpdateVersion();
}


void UUTGameUserSettings::SetToDefaults()
{
	Super::SetToDefaults();


	GeneralQualityLevel = EQualityLevels::Low;
	bIsFirstRun = true;
	AAMode = EAntiAliasingMethod::AAM_TemporalAA;

	SoundClassVolumes[ESoundClassType::Master] = 1.0f;
	SoundClassVolumes[ESoundClassType::SFX] = 1.0f;
	SoundClassVolumes[ESoundClassType::UI] = 1.0f;
	SoundClassVolumes[ESoundClassType::Music] = 0.5f;	
	SoundClassVolumes[ESoundClassType::Voice] = 1.0f;
	SoundClassVolumes[ESoundClassType::VOIP] = 1.0f;
	FullscreenMode = EWindowMode::Fullscreen;
	InitialBenchmarkState = -1;
	InputVersion = -1;
	GraphicVersion = -1;

	bAllowChangeViewByAbility = true;
	
	Language = (FInternationalization::Get().GetDefaultCulture()->GetTwoLetterISOLanguageName() == "ru") ? "ru" : "en";
	UE_LOG(LogCore, Display, TEXT("UUTGameUserSettings::SetToDefaults >> Language: %s"), *Language);
	FInternationalization::Get().SetCurrentCulture(Language);
}

void UUTGameUserSettings::InitSettings()
{
	//======================================================================================================================================================

	SupportedLanguagesArray.Empty();

	auto internationalizationInstance = &FInternationalization::Get();
	checkf(internationalizationInstance, TEXT("internationalizationInstance was nullptr"));

	if (internationalizationInstance)
	{
		internationalizationInstance->GetCulturesWithAvailableLocalization(FPaths::GetGameLocalizationPaths(), SupportedLanguagesArray, false);
		internationalizationInstance->SetCurrentCulture(Language);
		//identityController.ChangeRegionAccount.Request(FChangeRegionAccountView(Language));
	}

	//======================================================================================================================================================

	if (bIsFirstRun || GraphicVersion < UUTGameUserSettings::CurrentGraphicVersion)
	{
		bIsFirstRun = false;
		GeneralQualityLevel = EQualityLevels::Personal;
		ScalabilityQuality = Scalability::BenchmarkQualityLevels(10);
		Scalability::SetQualityLevels(ScalabilityQuality);
		Scalability::SaveState(GGameUserSettingsIni);
		
		SetAAMode(EAntiAliasingMethod::AAM_TemporalAA);
		SetEnabledAmbientOcclusion((ScalabilityQuality.GetSingleQualityLevel() > 1));
		SetEnabledMotionBlur((ScalabilityQuality.GetSingleQualityLevel() > 1));
		SetEnabledBloom((ScalabilityQuality.GetSingleQualityLevel() > 1));

		ApplyResolutionSettings(false);

		GraphicVersion = UUTGameUserSettings::CurrentGraphicVersion;

		SaveConfig(CPF_Config, *GGameUserSettingsIni);
	}

	if (EQualityLevels::Personal != GeneralQualityLevel)
	{
		ScalabilityQuality.SetFromSingleQualityLevel(static_cast<int32>(GeneralQualityLevel));
		Scalability::SetQualityLevels(ScalabilityQuality);
	}

	if (InputVersion < UUTGameUserSettings::CurrentInputVersion) // Reset if updated
	{
		ResetInputSettings();
	}

	Super::ApplySettings(true);
}

int32 UUTGameUserSettings::GetCurrentLanguage()
{
	return FInternationalization::Get().GetCurrentCulture()->GetLCID();
}

FString UUTGameUserSettings::GetCurrentLanguageTwoISO()
{
	return FInternationalization::Get().GetCurrentCulture()->GetTwoLetterISOLanguageName();
}

void UUTGameUserSettings::SetCurrentLanguage(const int32 &localeId)
{
	for (auto &locale : SupportedLanguagesArray)
	{
		if (locale->GetLCID() == localeId)
		{
			Language = locale->GetTwoLetterISOLanguageName();
			FInternationalization::Get().SetCurrentCulture(Language);

			break;
		}
	}
}

void UUTGameUserSettings::LoadSettings(bool bForceReload)
{
	Super::LoadSettings(bForceReload);

	Scalability::LoadState(*GGameUserSettingsIni);
	ScalabilityQuality = Scalability::GetQualityLevels();
}

void UUTGameUserSettings::ApplySettings(bool bCheckForCommandLineOverrides)
{
	Super::ApplySettings(bCheckForCommandLineOverrides);

	for (int32 i = 0; i < ARRAY_COUNT(SoundClassVolumes); i++)
	{
		SetSoundVolume(ESoundClassType(i), SoundClassVolumes[i]);
	}

	SetAAMode(AAMode);
	SetEnabledAmbientOcclusion(bAmbientOcclusion);
	SetEnabledMotionBlur(bMotionBlur);
	SetEnabledBloom(bBloom);
	//SetScreenPercentage(ScreenPercentage);
	SetHRTFEnabled(bHRTFEnabled);
	SetBotSpeech(BotSpeech);
}

void UUTGameUserSettings::SetSoundClassVolume(EUTSoundClass::Type Category, float NewValue)
{
	if (Category < ARRAY_COUNT(SoundClassVolumes))
	{
		SoundClassVolumes[Category] = NewValue;

		const UEnum* EUTSoundClassPtr = FindObject<UEnum>(ANY_PACKAGE, TEXT("EUTSoundClass"), true);
		checkSlow(EUTSoundClassPtr != nullptr);
		if (EUTSoundClassPtr == nullptr)
		{
			// add logging?
			return;
		}
		FString EnumValueDisplayName = EUTSoundClassPtr->GetNameStringByIndex(Category);
		SetSoundClassVolumeByName(EnumValueDisplayName, NewValue);
	}
}
float UUTGameUserSettings::GetSoundClassVolume(EUTSoundClass::Type Category)
{
	return (Category < ARRAY_COUNT(SoundClassVolumes)) ? SoundClassVolumes[Category] : 0.0f;
}

bool UUTGameUserSettings::SetSoundClassVolumeByName(FString ClassName, float Volume)
{
	FAudioDevice* AudioDevice = GEngine->GetMainAudioDevice();
	
	if (!AudioDevice) return false;
	
	FAudioThreadSuspendContext AudioThreadSuspend;
	PRAGMA_DISABLE_DEPRECATION_WARNINGS
	for (TMap<USoundClass*, FSoundClassProperties>::TConstIterator i(AudioDevice->GetSoundClassPropertyMap()); i; ++i)
	PRAGMA_ENABLE_DEPRECATION_WARNINGS
	{
		USoundClass* SoundClass = i.Key();
		FString SoundClassFullName = SoundClass->GetFullName();
		FString SoundClassName;
		if (SoundClassFullName.Split(".", nullptr, &SoundClassName, ESearchCase::IgnoreCase, ESearchDir::FromEnd))
		{
			if (SoundClassFullName.Contains("Game") && SoundClassName.Equals(ClassName, ESearchCase::IgnoreCase))
			{
				//AudioDevice->SetSoundMixClassOverride()
				SoundClass->Properties.Volume = Volume;
				return true;
			}
		}
	}

	return false;
}

float UUTGameUserSettings::GetSoundClassVolumeByName(FString ClassName)
{
	FAudioDevice* AudioDevice = GEngine->GetMainAudioDevice();

	if (!AudioDevice) return -1.0f;

	FAudioThreadSuspendContext AudioThreadSuspend;
	PRAGMA_DISABLE_DEPRECATION_WARNINGS
	for (TMap<USoundClass*, FSoundClassProperties>::TConstIterator i(AudioDevice->GetSoundClassPropertyMap()); i; ++i)
	PRAGMA_ENABLE_DEPRECATION_WARNINGS
	{
		USoundClass* SoundClass = i.Key();
		FString SoundClassName;
	
		if (SoundClassName.Contains("Game") && SoundClassName.Contains("." + ClassName))
		{
			return SoundClass->Properties.Volume;
		}
	}

	return -1.0f;
}

int32 UUTGameUserSettings::GetAAMode()
{
	return AAMode;
}

void UUTGameUserSettings::SetAAMode(int32 NewAAMode)
{
	AAMode = NewAAMode;
	if (auto AAModeCVar = IConsoleManager::Get().FindConsoleVariable(TEXT("r.DefaultFeature.AntiAliasing")))
	{
		if (AAMode >= EAntiAliasingMethod::AAM_MSAA && IsForwardShadingEnabled(GMaxRHIShaderPlatform))
		{
			AAModeCVar->Set(EAntiAliasingMethod::AAM_MSAA);
			if (auto CVar = IConsoleManager::Get().FindConsoleVariable(TEXT("r.MSAACount")))
			{
				CVar->Set((AAMode == EAntiAliasingMethod::AAM_MSAA) ? 2 : 4);
			}
		}
		else
		{
			AAModeCVar->Set((AAMode >= EAntiAliasingMethod::AAM_MSAA) ? EAntiAliasingMethod::AAM_TemporalAA : AAMode);
		}
	}
}

int32 UUTGameUserSettings::ConvertAAScalabilityQualityToAAMode(int32 AAScalabilityQuality)
{
	const int32 AAScalabilityQualityToModeLookup[] = { 0, 2, 2, 2 };
	int32 AAQuality = FMath::Clamp(AAScalabilityQuality, 0, 3);

	return AAScalabilityQualityToModeLookup[AAQuality];
}

EBotSpeechOption UUTGameUserSettings::GetBotSpeech() const
{
	return BotSpeech;
}

void UUTGameUserSettings::SetBotSpeech(EBotSpeechOption NewSetting)
{
	BotSpeech = NewSetting;
}

bool UUTGameUserSettings::IsHRTFEnabled()
{
	return bHRTFEnabled;
}

void UUTGameUserSettings::SetHRTFEnabled(bool NewHRTFEnabled)
{
	bHRTFEnabled = NewHRTFEnabled;
#if 0
	FAudioDevice* AudioDevice = GEngine->GetMainAudioDevice();
	if (AudioDevice)
	{
		AudioDevice->SetSpatializationExtensionEnabled(bHRTFEnabled);
		AudioDevice->SetHRTFEnabledForAll(bHRTFEnabled);
	}
#endif
}

bool UUTGameUserSettings::IsKeyboardLightingEnabled()
{
	return !bDisableKeyboardLighting;
}

void UUTGameUserSettings::SetKeyboardLightingEnabled(bool NewKeyboardLightingEnabled)
{
	bDisableKeyboardLighting = !NewKeyboardLightingEnabled;
}

#if !UE_SERVER
void UUTGameUserSettings::BenchmarkDetailSettingsIfNeeded(UUTLocalPlayer* LocalPlayer)
{
	if (ensure(LocalPlayer))
	{
		if (InitialBenchmarkState == -1)
		{
			InitialBenchmarkState = 0;
			BenchmarkDetailSettings(LocalPlayer, true);
		}
		else if (InitialBenchmarkState == 0)
		{
			// Benchmark was previously run but didn't complete. 
			// Perhaps prompt the user?
			SetBenchmarkFallbackValues();
		}
		else
		{
			//  Must be successfully completed
			ensure(InitialBenchmarkState == 1);
		}
	}
}
#endif // !UE_SERVER

TStatId UUTGameUserSettings::GetStatId() const
{
	RETURN_QUICK_DECLARE_CYCLE_STAT(UUTGameUserSetting, STATGROUP_Tickables);
}

bool UUTGameUserSettings::IsTickable() const
{
	return true;
}

bool UUTGameUserSettings::IsTickableWhenPaused() const
{
	return true;
}


void UUTGameUserSettings::Tick( float DeltaTime )
{
#if !UE_SERVER
	if (bRequestBenchmark)
	{
		TickCount++;
		if (TickCount > 5)
		{
			bRequestBenchmark = false;
			RunSynthBenchmark(bBenchmarkSaveSettingsOnceDetected);
		}
	}
#endif // !UE_SERVER
}

#if !UE_SERVER
void UUTGameUserSettings::BenchmarkDetailSettings(UUTLocalPlayer* LocalPlayer, bool bSaveSettingsOnceDetected)
{
	if (ensure(LocalPlayer))
	{
		//AutoDetectingSettingsDialog = LocalPlayer->ShowMessage(
		//	NSLOCTEXT("UTLocalPlayer", "BenchmarkingTitle", "Finding Settings"),
		//	NSLOCTEXT("UTLocalPlayer", "BenchmarkingText", "Automatically finding the settings that will give you the best experience.\n\nThis could take some time..."),
		//	0
		//	);

		TickCount = 0;
		bBenchmarkSaveSettingsOnceDetected = bSaveSettingsOnceDetected;
		bRequestBenchmark = true;
		BenchmarkingLocalPlayer = LocalPlayer;
		bBenchmarkInProgress = true;
	}
}

void UUTGameUserSettings::RunSynthBenchmark(bool bSaveSettingsOnceDetected)
{
	Scalability::FQualityLevels DetectedLevels = Scalability::BenchmarkQualityLevels();

	if (bSaveSettingsOnceDetected)
	{
		// If monitor refresh rate is under 120, we're going to cap framerate at 60 by default, else 120
		UUTGameEngine* UTEngine = Cast<UUTGameEngine>(GEngine);
		if (UTEngine != nullptr)
		{
			int32 RefreshRate;
			if (UTEngine->GetMonitorRefreshRate(RefreshRate))
			{
				if (RefreshRate >= 120)
				{
					UTEngine->FrameRateCap = 120;
				}
				else
				{
					UTEngine->FrameRateCap = 60;
				}

				UTEngine->SaveConfig();
			}
		}

		CorrectScreenPercentageOnHighResLowGPU(DetectedLevels);

		ScalabilityQuality = DetectedLevels;
		Scalability::SetQualityLevels(ScalabilityQuality);
		Scalability::SaveState(GGameUserSettingsIni);

		// Mark initial benchmark state as being complete. Even if this benchmark wasn't triggered by the initial run system, it
		// is still valid to disable any future benchmarking now.
		InitialBenchmarkState = 1;

		SaveSettings();
	}

	SettingsAutodetectedEvent.Broadcast(DetectedLevels);
	bBenchmarkInProgress = false;
	if (AutoDetectingSettingsDialog.IsValid() && BenchmarkingLocalPlayer != nullptr)
	{
//		BenchmarkingLocalPlayer->CloseDialog(AutoDetectingSettingsDialog.ToSharedRef());
	}
}

/**
 Looks for cases with High Res machines with low/mid hardware and lowers the ScreenPercentage to accommodate
*/
void UUTGameUserSettings::CorrectScreenPercentageOnHighResLowGPU(Scalability::FQualityLevels& DetectedLevels) const
{
	const int MaxResolutionForLowerEndSystems = 1080;
	const int ValueOfHighSetting = 2;
	const int ScreenResY = UUTGameUserSettings::GetScreenResolution().Y;

	const float NumberOfScalabilitySettings = 6;
	const float AverageScalabilitySetting = static_cast<float>(DetectedLevels.AntiAliasingQuality +
															   DetectedLevels.EffectsQuality +
															   DetectedLevels.PostProcessQuality +
															   DetectedLevels.ShadowQuality +
															   DetectedLevels.TextureQuality +
															   DetectedLevels.ViewDistanceQuality)
															   / NumberOfScalabilitySettings;

	//our resolution is >1080p but we don't have a system good enough to run everything on high, we need to turn down screen res!
	if ((ScreenResY > MaxResolutionForLowerEndSystems) && (AverageScalabilitySetting < ValueOfHighSetting))
	{
		const float ResPercentage = static_cast<float>(MaxResolutionForLowerEndSystems) / static_cast<float>(ScreenResY);
		const int32 NewScreenPercentage = static_cast<int32>(ResPercentage * 100);

		//If we already set it to something lower, no point in changing it
		if (NewScreenPercentage < DetectedLevels.ResolutionQuality)
		{
			DetectedLevels.ResolutionQuality = NewScreenPercentage;
		}
	}

}
#endif // !UE_SERVER

float UUTGameUserSettings::GetSoundVolume(const ESoundClassType SoundType)
{
	if (SoundType >= 0 && SoundType < ARRAY_COUNT(SoundClassVolumes))
	{
		return SoundClassVolumes[SoundType];
	}

	return -1.0f;
}

void UUTGameUserSettings::SetSoundVolume(const ESoundClassType SoundType, const float Volume)
{
	if (SoundType >= 0 && SoundType < ARRAY_COUNT(SoundClassVolumes))
	{
		SoundClassVolumes[SoundType] = Volume;

		switch (SoundType)
		{
			case ESoundClassType::Master:
				SetSoundClassVolumeByName("Master", SoundClassVolumes[SoundType]);
				break;
			case ESoundClassType::SFX:
				SetSoundClassVolumeByName("SFX", SoundClassVolumes[SoundType]);
				break;
			case ESoundClassType::UI:
				SetSoundClassVolumeByName("UI", SoundClassVolumes[SoundType]);
				break;
			case ESoundClassType::Music:
				SetSoundClassVolumeByName("Music", SoundClassVolumes[SoundType]);
				break;
			case ESoundClassType::Voice:
				SetSoundClassVolumeByName("Voice", SoundClassVolumes[SoundType]);
				break;
			case ESoundClassType::VOIP:
				SetSoundClassVolumeByName("VOIP", SoundClassVolumes[SoundType]);
				break;
		}
	}
}

void UUTGameUserSettings::ResetInputSettings()
{
	auto InputSettings = GetMutableDefault<UInputSettings>();

	if (InputSettings && GConfig)
	{
		InputVersion = UUTGameUserSettings::CurrentInputVersion;

		if (GConfig->AreFileOperationsDisabled())
		{
			GConfig->EnableFileOperations();
		}

		GConfig->LoadGlobalIniFile(GInputIni, TEXT("Input"), nullptr, true, true);

		InputSettings->ReloadConfig(UInputSettings::StaticClass(), *GInputIni);

		SaveConfig(CPF_Config, *GGameUserSettingsIni);
	}
}

FKey UUTGameUserSettings::GetKeyBinding(const FString& InAction, const bool IsAlternative)
{
	auto InputSettings = GetDefault<UInputSettings>();

	auto Input_ActionMappings = InputSettings->ActionMappings;
	auto Input_AxisMappings = InputSettings->AxisMappings;

	bool IsFound = false;

	if (InAction.Contains("+") || InAction.Contains("-"))
	{
		float Scale = 1.0f;
		FString KeyString = InAction;

		if (KeyString.Contains("-"))
		{
			KeyString.RemoveAt(KeyString.Find("-"));
			Scale = -1.0f;
		}
		else
		{
			KeyString.RemoveAt(KeyString.Find("+"));
		}

		for (SSIZE_T i = 0; i < Input_AxisMappings.Num(); ++i)
		{
			if (Input_AxisMappings[i].AxisName.ToString().Equals(KeyString) && FMath::IsNearlyEqual(Input_AxisMappings[i].Scale, Scale))
			{
				if (!IsAlternative || IsFound)
				{
					return Input_AxisMappings[i].Key;
				}
				else
				{
					IsFound = true;
				}
			}
		}
	}
	else
	{
		for (SSIZE_T i = 0; i < Input_ActionMappings.Num(); ++i)
		{
			if (Input_ActionMappings[i].ActionName.ToString().Equals(InAction))
			{
				if (!IsAlternative || IsFound)
				{
					return Input_ActionMappings[i].Key;
				}
				else
				{
					IsFound = true;
				}
			}
		}
	}

	return EKeys::Invalid;
}

void UUTGameUserSettings::SetEnabledAmbientOcclusion(const bool InIsEnabled)
{
	bAmbientOcclusion = InIsEnabled;
	if (auto CVar = IConsoleManager::Get().FindConsoleVariable(TEXT("r.DefaultFeature.AmbientOcclusion")))
	{
		CVar->Set(bAmbientOcclusion);
	}
}

void UUTGameUserSettings::SetEnabledMotionBlur(const bool InIsEnabled)
{
	bMotionBlur = InIsEnabled;
	if (auto CVar = IConsoleManager::Get().FindConsoleVariable(TEXT("r.DefaultFeature.MotionBlur")))
	{
		CVar->Set(bMotionBlur);
	}
}

void UUTGameUserSettings::SetEnabledBloom(const bool InIsEnabled)
{
	bBloom = InIsEnabled;
	if (auto CVar = IConsoleManager::Get().FindConsoleVariable(TEXT("r.DefaultFeature.Bloom")))
	{
		CVar->Set(bBloom);
	}
}