// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "GameModeTypeId.h"
#include "Notify/MessageBoxCfg.h"
#include "LKBlueprintFunctionLibrary.generated.h"

struct FNotifyInfo;
class UWidget;

USTRUCT(BlueprintType)
struct FBlueprintNotifyInfo
{
	GENERATED_USTRUCT_BODY();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UWidget* ContentWidget;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText Title;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText Content;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText Hyperlink;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float ShowTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FSlateBrush Image;

	FNotifyInfo ToNotifyInfo() const;
};

/**
 * 
 */
UCLASS()
class LOKAGAME_API ULKBlueprintFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:
	
	UFUNCTION(BlueprintPure, BlueprintCallable, Category = GameMap, meta = (DisplayName = "GetString", Depricated))
	static FString K2_Map_GetString(const EGameMapTypeId::Type& Id);

	UFUNCTION(BlueprintPure, BlueprintCallable, Category = GameMode, meta = (DisplayName = "GetString", Depricated))
	static FString K2_Mode_GetString(const EGameModeTypeId::Type& Id);

	UFUNCTION(BlueprintPure, BlueprintCallable, Category = GameMap, meta = (DisplayName = "GetText", Depricated))
	static FText K2_Map_GetText(const EGameMapTypeId::Type& Id);

	UFUNCTION(BlueprintPure, BlueprintCallable, Category = GameMap, meta = (DisplayName = "GetText", Depricated))
	static FText K2_Map_GetMapText(const FString& Id);

	UFUNCTION(BlueprintPure, BlueprintCallable, Category = GameMode, meta = (DisplayName = "GetText", Depricated))
	static FText K2_Mode_GetText(const EGameModeTypeId::Type& Id);

	UFUNCTION(BlueprintPure, BlueprintCallable, Category = GameMode, meta = (DisplayName = "GetTextDesc", Depricated))
	static FText K2_Mode_GetTextDesc(const EGameModeTypeId::Type& Id);

	UFUNCTION(BlueprintCallable, Category = Editor, meta = (DisplayName = "CreateStaticTexture"))
	static UTexture2D* K2_CreateStaticTexture(UTextureRenderTarget2D* InRenderTarget, const FString& InRelativePath, const FString& InName);

	UFUNCTION(BlueprintCallable, Category = Notify, meta = (DisplayName = "AddNotify", WorldContext = "WorldContextObject"))
	static void K2_AddNotify(const FBlueprintNotifyInfo& NotifyInfo);

	UFUNCTION(BlueprintCallable, Category = Notify, meta = (DisplayName = "ShowMessageBox"))
	static FName K2_ShowMessageBoxU(const UMessageBoxCfg* InConfiguration);

	UFUNCTION(BlueprintCallable, Category = Notify, meta = (DisplayName = "ShowMessageBox"))
	static FName K2_ShowMessageBox(const FMessageBoxMsg& InConfiguration);

	UFUNCTION(BlueprintCallable, Category = Notify, meta = (DisplayName = "HideMessageBox"))
	static void K2_HideMessageBox(const FName& InName);

	UFUNCTION(BlueprintPure, BlueprintCallable, Category = Notify, meta = (DisplayName = "EventMessageBoxButton"))
	static FMessageBoxMsg& K2_OnMessageBoxButton(UPARAM(ref) FMessageBoxMsg& InStructure, FOnMessageBoxButton Event);

	UFUNCTION(BlueprintPure, BlueprintCallable, Category = Notify, meta = (DisplayName = "EventMessageBoxHidden"))
	static FMessageBoxMsg& K2_OnMessageBoxHidden(UPARAM(ref) FMessageBoxMsg& InStructure, FOnMessageBoxHidden Event);

	UFUNCTION(BlueprintCallable, Category = Editor, meta = (DisplayName = "CreateEntityTexture"))
	static bool K2_CreateTextureForEntity(UTextureRenderTarget2D* InRenderTarget, const FString& InRelativePath, const TSubclassOf<UItemBaseEntity>& InClass);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Editor, meta = (DisplayName = "Get Game Singleton"))
	static UGameSingleton* K2_GetGameSingleton();

	//===============================================================================================================================
	// Flags 

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Flags, meta = (DisplayName = "HasAnyFlags"))
	static bool K2_HasAnyFlags(const int32& InSource, const int32& InFlags);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Flags, meta = (DisplayName = "HasAllFlags"))
	static bool K2_HasAllFlags(const int32& InSource, const int32& InFlags);

	UFUNCTION(BlueprintCallable, Category = Flags, meta = (DisplayName = "SetFlags"))
	static void K2_SetFlags(UPARAM(ref) int32& InSource, const int32& InFlags);

	UFUNCTION(BlueprintCallable, Category = Flags, meta = (DisplayName = "ToggleFlags"))
	static void K2_ToggleFlags(UPARAM(ref) int32& InSource, const int32& InFlags);

	UFUNCTION(BlueprintCallable, Category = Flags, meta = (DisplayName = "ClearFlags"))
	static void K2_ClearFlags(UPARAM(ref) int32& InSource, const int32& InFlags);

	UFUNCTION(BlueprintCallable, Category = Flags, meta = (DisplayName = "ToFlag"))
	static int32 K2_ToFlag(const int32& InSource);
};
