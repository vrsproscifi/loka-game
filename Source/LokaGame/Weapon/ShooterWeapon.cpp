// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "LokaGame.h"
#include "ShooterWeapon.h"
#include "Particles/ParticleSystemComponent.h"

#include "ShooterImpactEffect.h"
#include "UTCharacter.h"
#include "UTProjectile.h"
#include "UTPlayerController.h"
#include "UTGameState.h"
#include "UTBot.h"

#include "Engine/ActorChannel.h"
#include "Slate/Components/SSniperScope.h"

#include "Module/ItemModuleScopeSniperEntity.h"
#include "Module/ItemModuleTrunkEntity.h"
#include "Module/ItemModuleScopeOpticalEntity.h"

#include "Material/ItemMaterialEntity.h"
#include "Weapon/ItemWeaponEntity.h"
#include "Ammo/ItemAmmoEntity.h"

#include "ShooterGameInstance.h"
#include "Animation/AnimMontage.h"
#include "Animation/AnimInstance.h"
#include "PlayerInventoryItemWeapon.h"

AShooterWeapon::AShooterWeapon(const FObjectInitializer& ObjectInitializer) 
	: Super(ObjectInitializer)
{
	Mesh1P = ObjectInitializer.CreateDefaultSubobject<USkeletalMeshComponent>(this, TEXT("WeaponMesh1P"));
	Mesh1P->VisibilityBasedAnimTickOption = EVisibilityBasedAnimTickOption::OnlyTickPoseWhenRendered;
	Mesh1P->bReceivesDecals = false;
	Mesh1P->CastShadow = false;
	Mesh1P->SetCollisionObjectType(ECC_WorldDynamic);
	Mesh1P->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	Mesh1P->SetCollisionResponseToAllChannels(ECR_Ignore);
	Mesh1P->bEnableUpdateRateOptimizations = true;
	RootComponent = Mesh1P;

	Mesh3P = ObjectInitializer.CreateDefaultSubobject<USkeletalMeshComponent>(this, TEXT("WeaponMesh3P"));
	Mesh3P->VisibilityBasedAnimTickOption = EVisibilityBasedAnimTickOption::OnlyTickPoseWhenRendered;
	Mesh3P->bReceivesDecals = false;
	Mesh3P->CastShadow = true;
	Mesh3P->SetCollisionObjectType(ECC_WorldDynamic);
	Mesh3P->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	Mesh3P->SetCollisionResponseToAllChannels(ECR_Ignore);
	Mesh3P->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Block);
	Mesh3P->SetCollisionResponseToChannel(ECC_Visibility, ECR_Block);
	Mesh3P->SetCollisionResponseToChannel(COLLISION_PROJECTILE, ECR_Block);
	Mesh3P->bEnableUpdateRateOptimizations = true;
	Mesh3P->SetupAttachment(Mesh1P);

	for (int32 i = 0; i < 3; ++i)
	{
		auto module1P = ObjectInitializer.CreateDefaultSubobject<USkeletalMeshComponent>(this, *FString::Printf(TEXT("Module1P_%d"), i));

		module1P->SetupAttachment(RootComponent);
		module1P->bReceivesDecals = false;
		module1P->bComponentUseFixedSkelBounds = true;
		module1P->SetVisibility(false, true);
		module1P->bEnableUpdateRateOptimizations = true;
		module1P->SetComponentTickEnabled(false);
		InstalledModulesMesh1P.Add(module1P);

		auto module3P = ObjectInitializer.CreateDefaultSubobject<USkeletalMeshComponent>(this, *FString::Printf(TEXT("Module3P_%d"), i));

		module3P->SetupAttachment(RootComponent);
		module3P->bReceivesDecals = false;
		module3P->bComponentUseFixedSkelBounds = true;
		module3P->SetVisibility(false, true);
		module3P->bEnableUpdateRateOptimizations = true;
		module3P->SetComponentTickEnabled(false);
		InstalledModulesMesh3P.Add(module3P);
	}

	CaptureComp = ObjectInitializer.CreateDefaultSubobject<USceneCaptureComponent2D>(this, TEXT("CaptureComponent2D"));
	CaptureComp->SetupAttachment(Mesh1P);
	CaptureComp->bCaptureEveryFrame = false;
	CaptureComp->SetComponentTickEnabled(false);

	bPlayingFireAnim = false;
	bIsEquipped = false;
	bWantsToFire = false;
	bPendingReload = false;
	bPendingEquip = false;
	CurrentState = EWeaponState::Idle;
	bIsCancelReload = false;

	CurrentAmmoInClip = 0;
	BurstCounter = 0;
	LastFireTime = 0.0f;

	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.TickGroup = TG_PrePhysics;
	SetRemoteRoleForBackwardsCompat(ROLE_SimulatedProxy);
	bReplicates = true;
	bNetLoadOnClient = false;
	bNetUseOwnerRelevancy = true;
}

void AShooterWeapon::OnRep_Instance()
{
	auto EntityInstance = Instance ? Instance->GetEntityWeapon() : nullptr;
	if (EntityInstance)
	{
		for (SIZE_T i = 0; i < EAmmoSlot::End; ++i)
		{
			if (EntityInstance->AmmoInstance[i] && EntityInstance->AmmoInstance[i]->IsValidLowLevel())
			{
				AmmoInstance[i] = EntityInstance->Ammo[i]; //NewObject<UItemAmmoEntity>(this, Instance->AmmoInstance[i]);
			}
		}

		CurrentAmmoInClip = GetWeaponProperty().AmmoPerClip;
		CountAmmo[0] = GetWeaponProperty().AmmoPerClip * 10;
		CountAmmo[1] = GetWeaponProperty().AmmoPerClip * 10;

		Mesh1P->SetSkeletalMesh(EntityInstance->GetSkeletalMesh());
		Mesh1P->SetAnimInstanceClass(EntityInstance->GetAnimClass());
		Mesh1P->SetMaterial(0, EntityInstance->GetMaterialByModel(Instance->GetInstalledMaterialModelId()));
		Mesh3P->SetSkeletalMesh(EntityInstance->GetSkeletalMesh());
		Mesh3P->SetAnimInstanceClass(EntityInstance->GetAnimClass());
		Mesh3P->SetMaterial(0, EntityInstance->GetMaterialByModel(Instance->GetInstalledMaterialModelId()));

		FirstPMeshOffset = FVector::ZeroVector;

		for (auto &m : Instance->GetInstalledAddons())
		{
			UItemModuleEntity* localModule = m ? m->GetEntity<UItemModuleEntity>() : nullptr;
			if (localModule)
			{
				for (SIZE_T i = 0; i < 3; ++i)
				{
					if (!InstalledModulesMesh1P[i]->IsVisible())
					{
						InstalledModulesMeshMap.Add(localModule->TargetSocket, InstalledModulesMesh1P[i]);
						for (SIZE_T x = 0; x < 2; ++x) // Init both 1P & 3P View
						{
							const bool IsFirstViewTarget = (x == 0);
							auto c = IsFirstViewTarget ? InstalledModulesMesh1P[i] : InstalledModulesMesh3P[i];

							c->AttachToComponent(IsFirstViewTarget ? Mesh1P : Mesh3P, FAttachmentTransformRules::SnapToTargetIncludingScale, localModule->TargetSocket);
							c->SetSkeletalMesh(localModule->GetSkeletalMesh());
							c->CastShadow = !IsFirstViewTarget;
							c->SetVisibility(true, true);
							c->SetComponentTickEnabled(true);

							if (auto TargetModule = Cast<UItemModuleScopeOpticalEntity>(localModule))
							{
								ScopeModule = TargetModule;
								if (TargetModule->IsUseRenderTarget() && IsFirstViewTarget)
								{
									CaptureComp->AttachToComponent(c, FAttachmentTransformRules::SnapToTargetIncludingScale, TargetModule->GetRenderTargetSocket());
									CaptureComp->TextureTarget = TargetModule->RenderTextureScope;
									CaptureComp->FOVAngle = TargetModule->GetScopeRenderTargetFOV(90.0f);
									CaptureComp->bCaptureEveryFrame = true;
									CaptureComp->SetComponentTickEnabled(true);

									for (auto o : TargetModule->ReplaceIndexes)
									{
										auto MID = c->CreateAndSetMaterialInstanceDynamic(o);
										MID->SetTextureParameterValue(TargetModule->GetTextureParameterName(), TargetModule->RenderTextureScope);
									}
								}

								if (TargetModule->IsSupportLiveParams() && IsFirstViewTarget)
								{
									for (auto o : TargetModule->ReplaceIndexes)
									{
										ScopeModuleMIDS.Add(c->CreateAndSetMaterialInstanceDynamic(o));
									}
								}
							}
						}
						break;
					}
				}
			}
		}

		if (Role == ROLE_Authority) MakeFeedback();
	}
}

void AShooterWeapon::InitializeInstance(UPlayerInventoryItemWeapon* InInstance)
{
	Instance = InInstance;
	OnRep_Instance();
}

void AShooterWeapon::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	DetachMeshFromPawn();
}

void AShooterWeapon::Destroyed()
{
	if (MyPawn && MyPawn->IsLocallyControlled())
	{
		ToggleAimingParams(false);
	}

	Super::Destroyed();

	StopSimulatingWeaponFire();
}

//////////////////////////////////////////////////////////////////////////
// Inventory

void AShooterWeapon::OnEquip()
{
	bReplicates = true;
	SetActorTickEnabled(true);

	AttachMeshToPawn();

	bPendingEquip = true;
	DetermineWeaponState();
	
	if (Instance)
	{
		float Duration = PlayWeaponAnimation(GetWeaponConfiguration().EquipAnim);
		if (Duration <= 0.0f)
		{
			// failsafe
			Duration = 0.5f;
		}
		EquipStartedTime = GetWorld()->GetTimeSeconds();
		EquipDuration = Duration;

		GetWorldTimerManager().SetTimer(TimerHandle_OnEquipFinished, this, &AShooterWeapon::OnEquipFinished, Duration, false);

		if (Role == ROLE_Authority)
		{
			UUTGameplayStatics::NotifyBotsSound(this, MyPawn, GetWeaponConfiguration().EquipSound);
		}
	}
	else
	{
		OnEquipFinished();
	}

	if (MyPawn && MyPawn->IsLocallyControlled() && GetInstance())
	{
		PlayWeaponSound(GetWeaponConfiguration().EquipSound);
	}
}

void AShooterWeapon::OnEquipFinished()
{
	AttachMeshToPawn();

	bIsEquipped = true;
	bPendingEquip = false;

	// Determine the state so that the can reload checks will work
	DetermineWeaponState(); 
	
	if (MyPawn)
	{
		// try to reload empty clip
		if (MyPawn->IsLocallyControlled() &&
			CurrentAmmoInClip <= 0 &&
			CanReload())
		{
			StartReload();
		}
	}

	
}

void AShooterWeapon::OnUnEquip()
{
	UE_LOG(LogShooterWeapon, Log, TEXT("OnUnEquip >> HasAuthority = %s, MyPawn = %s"), HasAuthority() ? TEXT("True") : TEXT("False"), MyPawn ? *MyPawn->GetName() : TEXT("nullptr"));

	DetachMeshFromPawn();
	bIsEquipped = false;
	StopFire();

	if (bPendingReload)
	{
		StopWeaponAnimation(GetWeaponConfiguration().ReloadAnim);
		bPendingReload = false;

		GetWorldTimerManager().ClearTimer(TimerHandle_StopReload);
		GetWorldTimerManager().ClearTimer(TimerHandle_ReloadWeapon);
	}

	if (bPendingEquip)
	{
		StopWeaponAnimation(GetWeaponConfiguration().EquipAnim);
		bPendingEquip = false;

		GetWorldTimerManager().ClearTimer(TimerHandle_OnEquipFinished);
	}

	DetermineWeaponState();

	if (MyPawn && MyPawn->IsLocallyControlled())
	{
		ToggleAimingParams(false);

		//if (GetCurrentAmmo() == 0 && !bRefiring)
		{
			AUTPlayerController* MyPC = Cast<AUTPlayerController>(MyPawn->Controller);
			//if (MyPC && MyPC->UI_GameHUD.IsValid())
			//{
			//	MyPC->UI_GameHUD->ToggleBottomText(false);
			//}
		}
	}

	SetActorTickEnabled(false);
	bReplicates = false;
}

void AShooterWeapon::OnEnterInventory(AUTCharacter* NewOwner)
{
	SetOwningPawn(NewOwner);
}

void AShooterWeapon::OnLeaveInventory()
{
	UE_LOG(LogShooterWeapon, Log, TEXT("OnLeaveInventory >> HasAuthority = %s, MyPawn = %s"), HasAuthority() ? TEXT("True") : TEXT("False"), MyPawn ? *MyPawn->GetName() : TEXT("nullptr"));

	if (Role == ROLE_Authority)
	{
		SetOwningPawn(nullptr);
		SetLifeSpan(5.0f); // For unquip anim
	}

	if (IsAttachedToPawn())
	{
		OnUnEquip();
	}
}

void AShooterWeapon::AttachMeshToPawn()
{
	if (MyPawn)
	{
		FAttachmentTransformRules Rules(EAttachmentRule::SnapToTarget, false);

		// Remove and hide both first and third person meshes
		DetachMeshFromPawn();

		// For locally controller players we attach both weapons and let the bOnlyOwnerSee, bOwnerNoSee flags deal with visibility.
		if( MyPawn->IsLocallyControlled() == true )
		{
			USkeletalMeshComponent* PawnMesh1p = MyPawn->GetSpecifcPawnMesh(true);
			USkeletalMeshComponent* PawnMesh3p = MyPawn->GetSpecifcPawnMesh(false);
			Mesh1P->SetHiddenInGame(false);
			Mesh3P->SetHiddenInGame(false);
			Mesh1P->AttachToComponent(PawnMesh1p, Rules, MyPawn->GetFPPWeaponAttachPoint());
			Mesh3P->AttachToComponent(PawnMesh3p, Rules, MyPawn->GetTPPWeaponAttachPoint());
		}
		else
		{
			USkeletalMeshComponent* UseWeaponMesh = GetWeaponMesh();
			USkeletalMeshComponent* UsePawnMesh = MyPawn->GetPawnMesh();
			UseWeaponMesh->AttachToComponent(UsePawnMesh, Rules, MyPawn->GetTPPWeaponAttachPoint());
			UseWeaponMesh->SetHiddenInGame( false );
		}
	}
}

void AShooterWeapon::DetachMeshFromPawn()
{
	FDetachmentTransformRules Rules(EDetachmentRule::KeepRelative, false);

	Mesh1P->DetachFromComponent(Rules);
	Mesh1P->SetHiddenInGame(true);

	Mesh3P->DetachFromComponent(Rules);
	Mesh3P->SetHiddenInGame(true);
}


//////////////////////////////////////////////////////////////////////////
// Input

void AShooterWeapon::StartFire()
{
	if (CurrentState == EWeaponState::Reloading)
	{
		bIsCancelReload = true;
	}

	if (Role < ROLE_Authority)
	{
		ServerStartFire();
	}

	if (!bWantsToFire)
	{
		bWantsToFire = true;
		DetermineWeaponState();
	}
}

void AShooterWeapon::StopFire()
{
	if (Role < ROLE_Authority)
	{
		ServerStopFire();
	}

	if (bWantsToFire)
	{
		bWantsToFire = false;
		DetermineWeaponState();
	}
}

void AShooterWeapon::StartReload(bool bFromReplication)
{
	if (!bFromReplication && Role < ROLE_Authority)
	{
		if (MyPawn)
		{
			MyPawn->SetTargeting(false);
			ServerStartReload();
		}
	}

	if (GetInstance())
	{
		if (bFromReplication || CanReload())
		{
			bPendingReload = true;
			DetermineWeaponState();

			const float AnimDuration = GetWeaponProperty().ReloadDuration;

			PlayMontageWithDuration(Mesh1P, GetWeaponConfiguration().WeaponMontageReload, AnimDuration);

			if (MyPawn)
			{
				if (MyPawn->IsFirstPerson())
				{
					PlayMontageWithDuration(MyPawn->GetMesh1P(), GetWeaponConfiguration().PawnFPPMontageReload, AnimDuration);
				}
				else
				{
					PlayMontageWithDuration(MyPawn->GetMesh(), GetWeaponConfiguration().PawnTPPMontageReload, AnimDuration);
				}
			}

			GetWorldTimerManager().SetTimer(TimerHandle_StopReload, this, &AShooterWeapon::StopReload, AnimDuration, false);
			if (Role == ROLE_Authority)
			{
				GetWorldTimerManager().SetTimer(TimerHandle_ReloadWeapon, this, &AShooterWeapon::ReloadWeapon, FMath::Max(0.1f, AnimDuration - 0.1f), false);
				MakeFeedback();

				if (GetInstance())
				{
					UUTGameplayStatics::NotifyBotsSound(this, MyPawn, GetWeaponConfiguration().ReloadSound);
				}
			}

			if ((GetNetMode() == NM_Client || GetNetMode() == NM_Standalone) && GetWeaponProperty().IsPartlyReload)
			{
				if (GetCurrentAmmoInClip() < GetWeaponProperty().AmmoPerClip && bIsCancelReload == false)
				{
					GetWorldTimerManager().SetTimer(TimerHandle_ReloadWeapon, FTimerDelegate::CreateUObject(this, &AShooterWeapon::StartReload, false), FMath::Max(0.1f, AnimDuration + 0.1f), false);
				}

				if (bIsCancelReload)
				{
					bIsCancelReload = false;
				}
			}

			if (MyPawn && MyPawn->IsLocallyControlled())
			{
				PlayWeaponSound(GetWeaponConfiguration().ReloadSound);
				AUTPlayerController* MyPC = Cast<AUTPlayerController>(MyPawn->Controller);
				if (MyPC && MyPC->UI_GameHUD.IsValid())
				{
					MyPC->UI_GameHUD->SetBottomText(NSLOCTEXT("SHUD", "ReloadingWeapon", "Reloading..."));
					MyPC->UI_GameHUD->ToggleBottomText(true);
				}
			}
		}
	}
}

void AShooterWeapon::StopReload()
{
	if (CurrentState == EWeaponState::Reloading)
	{
		bPendingReload = false;
		DetermineWeaponState();
		StopWeaponAnimation(GetWeaponConfiguration().ReloadAnim);

		if (MyPawn && MyPawn->IsLocallyControlled())
		{
			AUTPlayerController* MyPC = Cast<AUTPlayerController>(MyPawn->Controller);
			if (MyPC && MyPC->UI_GameHUD.IsValid())
			{
				MyPC->UI_GameHUD->ToggleBottomText(false);
			}
		}
	}
}

bool AShooterWeapon::ServerStartFire_Validate()
{
	return true;
}

void AShooterWeapon::ServerStartFire_Implementation()
{
	StartFire();
}

bool AShooterWeapon::ServerStopFire_Validate()
{
	return true;
}

void AShooterWeapon::ServerStopFire_Implementation()
{
	StopFire();
}

bool AShooterWeapon::ServerStartReload_Validate()
{
	return true;
}

void AShooterWeapon::ServerStartReload_Implementation()
{
	StartReload();
}

bool AShooterWeapon::ServerStopReload_Validate()
{
	return true;
}

void AShooterWeapon::ServerStopReload_Implementation()
{
	StopReload();
}

void AShooterWeapon::ClientStartReload_Implementation()
{
	StartReload();
}

//////////////////////////////////////////////////////////////////////////
// Control

bool AShooterWeapon::CanFire() const
{
	bool bCanFire = MyPawn && MyPawn->CanFire();
	bool bStateOKToFire = ( ( CurrentState ==  EWeaponState::Idle ) || ( CurrentState == EWeaponState::Firing) );	
	return (( bCanFire == true ) && ( bStateOKToFire == true ) && ( bPendingReload == false ) );
}

bool AShooterWeapon::CanReload() const
{
	if (GetInstance())
	{
		bool bCanReload = (!MyPawn || MyPawn->CanReload());
		bool bGotAmmo = (CurrentAmmoInClip < GetWeaponProperty().AmmoPerClip) && (CountAmmo[CurrentAmmo] - CurrentAmmoInClip > 0 || HasInfiniteClip());
		bool bStateOKToReload = ((CurrentState == EWeaponState::Idle) || (CurrentState == EWeaponState::Firing));
		return ((bCanReload == true) && (bGotAmmo == true) && (bStateOKToReload == true));
	}

	return false;
}


//////////////////////////////////////////////////////////////////////////
// Weapon usage

void AShooterWeapon::GiveAmmo(int AddAmount)
{
	//const int32 MissingAmmo = FMath::Max(0, GetWeaponProperty().MaxAmmo - CurrentAmmo);
	//AddAmount = FMath::Min(AddAmount, MissingAmmo);
	CountAmmo[CurrentAmmo] += AddAmount;

	//AUTBot* BotAI = MyPawn ? Cast<AUTBot>(MyPawn->GetController()) : NULL;
	//if (BotAI)
	//{
	//	BotAI->CheckAmmo(this);
	//}
	
	// start reload if clip was empty
	if (GetCurrentAmmoInClip() <= 0 &&
		CanReload() &&
		MyPawn->GetWeapon() == this)
	{
		ClientStartReload();
	}

	OnRep_CountAmmo();
}

void AShooterWeapon::SetAmmo(int AddAmount)
{
	CurrentAmmoInClip = 0;
	CountAmmo[CurrentAmmo] = AddAmount;

	//AUTBot* BotAI = MyPawn ? Cast<AUTBot>(MyPawn->GetController()) : NULL;
	//if (BotAI)
	//{
	//	BotAI->CheckAmmo(this);
	//}

	if (CountAmmo[CurrentAmmo] >= GetAmmoPerClip())
	{
		CountAmmo[CurrentAmmo] -= GetAmmoPerClip();
		CurrentAmmoInClip = GetAmmoPerClip();
	}

	OnRep_CountAmmo();
}

int32 AShooterWeapon::GetAmmo() const
{
	if (GetInstance())
	{
		return CountAmmo[CurrentAmmo];
	}

	return 1;
}

void AShooterWeapon::UseAmmo()
{
	if (!HasInfiniteAmmo())
	{
		CurrentAmmoInClip--;
	}

	if (!HasInfiniteAmmo() && !HasInfiniteClip())
	{
		CountAmmo[CurrentAmmo]--;
		OnRep_CountAmmo();
	}

	if (MyPawn && FeedbackData.IsValidIndex(CurrentAmmoInClip))
	{
		TargetRecoilX = FeedbackData[CurrentAmmoInClip].Feedback.X;
		TargetRecoilY = FeedbackData[CurrentAmmoInClip].Feedback.Y;

		if (MyPawn->bIsCrouched)
		{
			TargetRecoilX *= FeedbackData[CurrentAmmoInClip].ModiferCrouched;
			TargetRecoilY *= FeedbackData[CurrentAmmoInClip].ModiferCrouched;
		}

		if (MyPawn->IsTargeting())
		{
			TargetRecoilX *= FeedbackData[CurrentAmmoInClip].ModiferTargeting;
			TargetRecoilY *= FeedbackData[CurrentAmmoInClip].ModiferTargeting;
		}
	}

	//AUTBot* BotAI = MyPawn ? Cast<AUTBot>(MyPawn->GetController()) : NULL;	
	//AUTPlayerController* PlayerController = MyPawn ? Cast<AUTPlayerController>(MyPawn->GetController()) : NULL;
	//if (BotAI)
	//{
	//	BotAI->CheckAmmo(this);
	//}
}

void AShooterWeapon::HandleFiring()
{
	if ((GetCurrentAmmoInClip() > 0 || HasInfiniteClip() || HasInfiniteAmmo()) && CanFire())
	{
		if (GetNetMode() != NM_DedicatedServer)
		{
			SimulateWeaponFire();
		}

		if (MyPawn && MyPawn->IsLocallyControlled())
		{
			FireWeapon();

			UseAmmo();
			
			// update firing FX on remote clients if function was called on server
			BurstCounter++;
		}

		if (GetInstance())
		{
			UUTGameplayStatics::NotifyBotsSound(this, MyPawn, GetWeaponConfiguration().bLoopedFireSound ? GetWeaponConfiguration().FireLoopSound : GetWeaponConfiguration().FireSound);
		}
	}
	else if (CanReload())
	{
		StartReload();
	}
	else if (MyPawn && MyPawn->IsLocallyControlled())
	{
		if (GetCurrentAmmo() == 0 && !bRefiring)
		{
			PlayWeaponSound(GetWeaponConfiguration().OutOfAmmoSound);
			AUTPlayerController* MyPC = Cast<AUTPlayerController>(MyPawn->Controller);
			//if (MyPC && MyPC->UI_GameHUD.IsValid())
			//{
			//	MyPC->UI_GameHUD->SetBottomText(NSLOCTEXT("SHUD", "OutOfAmmo", "Out Of Ammo\r\nRefill in cartridge-boxes,\r\n they are marked orange on the radar."));
			//	MyPC->UI_GameHUD->ToggleBottomText(true);
			//}
			//AShooterHUD* MyHUD = MyPC ? Cast<AShooterHUD>(MyPC->GetHUD()) : NULL;
			//if (MyHUD)
			//{
			//	MyHUD->NotifyOutOfAmmo();
			//}
		}
		
		// stop weapon fire FX, but stay in Firing state
		if (BurstCounter > 0)
		{
			OnBurstFinished();
		}
	}

	if (MyPawn && MyPawn->IsLocallyControlled())
	{
		// local client will notify server
		if (Role < ROLE_Authority)
		{
			ServerHandleFiring();
		}

		// reload after firing last round
		if (CurrentAmmoInClip <= 0 && CanReload())
		{
			StartReload();
		}

		// setup refire timer
		bRefiring = (CurrentState == EWeaponState::Firing && GetInstance() && GetWeaponProperty().TimeBetweenShots() > 0.0f);
		if (bRefiring)
		{
			GetWorldTimerManager().SetTimer(TimerHandle_HandleFiring, this, &AShooterWeapon::HandleFiring, GetWeaponProperty().TimeBetweenShots(), false);
		}
	}	

	LastFireTime = GetWorld()->GetTimeSeconds();

	if (GetInstance() && GetInstance()->GetProperty().IsAutomaticFire == false)
	{
		StopFire();
	}
}

bool AShooterWeapon::ServerHandleFiring_Validate()
{
	return true;
}

void AShooterWeapon::ServerHandleFiring_Implementation()
{
	const bool bShouldUpdateAmmo = (CurrentAmmoInClip > 0 && CanFire());

	HandleFiring();

	if (bShouldUpdateAmmo)
	{
		// update ammo
		UseAmmo();

		// update firing FX on remote clients
		BurstCounter++;				
	}
}

void AShooterWeapon::ReloadWeapon()
{
	const bool bIsPartly = GetWeaponProperty().IsPartlyReload;
	int32 ClipDelta = bIsPartly ? 1 : FMath::Min<int32>(GetWeaponProperty().AmmoPerClip - CurrentAmmoInClip, CountAmmo[CurrentAmmo] - CurrentAmmoInClip);

	if (HasInfiniteClip())
	{
		ClipDelta = GetWeaponProperty().AmmoPerClip - CurrentAmmoInClip;
	}

	if (ClipDelta > 0)
	{
		CurrentAmmoInClip += ClipDelta;
	}

	if (HasInfiniteClip())
	{
		CountAmmo[CurrentAmmo] = FMath::Max(CurrentAmmoInClip, CountAmmo[CurrentAmmo]);
		OnRep_CountAmmo();
	}

	if (bIsPartly)
	{
		//StopReload();
		//ClientStartReload();
		//GetWorldTimerManager().SetTimer(TimerHandle_ReloadWeapon, this, &AShooterWeapon::ClientStartReload, 0.2f);
		//bPendingReload = false;
		//ClientStartReload();
		//StartReload();
	}
}

void AShooterWeapon::SetWeaponState(EWeaponState::Type NewState)
{
	const EWeaponState::Type PrevState = CurrentState;

	if (PrevState == EWeaponState::Firing && NewState != EWeaponState::Firing)
	{
		OnBurstFinished();
	}

	CurrentState = NewState;

	if (PrevState != EWeaponState::Firing && NewState == EWeaponState::Firing)
	{
		OnBurstStarted();
	}
}

void AShooterWeapon::DetermineWeaponState()
{
	EWeaponState::Type NewState = EWeaponState::Idle;

	if (bIsEquipped)
	{
		if( bPendingReload  )
		{
			if( CanReload() == false )
			{
				NewState = CurrentState;
			}
			else
			{
				NewState = EWeaponState::Reloading;
			}
		}		
		else if ( (bPendingReload == false ) && ( bWantsToFire == true ) && ( CanFire() == true ))
		{
			NewState = EWeaponState::Firing;
		}
	}
	else if (bPendingEquip)
	{
		NewState = EWeaponState::Equipping;
	}

	SetWeaponState(NewState);
}

void AShooterWeapon::OnBurstStarted()
{
	// start firing, can be delayed to satisfy TimeBetweenShots()
	const float GameTime = GetWorld()->GetTimeSeconds();
	if (GetInstance() && LastFireTime > 0 && GetWeaponProperty().TimeBetweenShots() > 0.0f &&
		LastFireTime + GetWeaponProperty().TimeBetweenShots() > GameTime)
	{
		GetWorldTimerManager().SetTimer(TimerHandle_HandleFiring, this, &AShooterWeapon::HandleFiring, LastFireTime + GetWeaponProperty().TimeBetweenShots() - GameTime, false);
	}
	else
	{
		HandleFiring();
	}
}

void AShooterWeapon::OnBurstFinished()
{
	// stop firing FX on remote clients
	BurstCounter = 0;
	CurrentFiringSpread = .0f;

	// stop firing FX locally, unless it's a dedicated server
	if (GetNetMode() != NM_DedicatedServer)
	{
		StopSimulatingWeaponFire();
	}
	
	GetWorldTimerManager().ClearTimer(TimerHandle_HandleFiring);
	bRefiring = false;
}


//////////////////////////////////////////////////////////////////////////
// Weapon usage helpers

UAudioComponent* AShooterWeapon::PlayWeaponSound(USoundCue* Sound)
{
	UAudioComponent* AC = NULL;
	if (Sound && MyPawn)
	{
		AC = UGameplayStatics::SpawnSoundAttached(Sound, MyPawn->GetRootComponent());
	}

	return AC;
}

float AShooterWeapon::PlayWeaponAnimation(const FWeaponAnim& Animation)
{
	float Duration = 0.0f;
	if (MyPawn)
	{
		UAnimMontage* UseAnim = MyPawn->IsFirstPerson() ? Animation.Pawn1P : Animation.Pawn3P;
		if (UseAnim)
		{
			Duration = MyPawn->PlayAnimMontage(UseAnim);
		}
	}

	return Duration;
}

void AShooterWeapon::StopWeaponAnimation(const FWeaponAnim& Animation)
{
	if (MyPawn)
	{
		UAnimMontage* UseAnim = MyPawn->IsFirstPerson() ? Animation.Pawn1P : Animation.Pawn3P;
		if (UseAnim)
		{
			MyPawn->StopAnimMontage(UseAnim);
		}
	}
}

FVector AShooterWeapon::GetCameraAim() const
{
	AUTPlayerController* const PlayerController = Instigator ? Cast<AUTPlayerController>(Instigator->Controller) : NULL;
	FVector FinalAim = FVector::ZeroVector;

	if (PlayerController)
	{
		FVector CamLoc;
		FRotator CamRot;
		PlayerController->GetPlayerViewPoint(CamLoc, CamRot);
		FinalAim = CamRot.Vector();
	}
	else if (Instigator)
	{
		FinalAim = Instigator->GetBaseAimRotation().Vector();		
	}

	return FinalAim;
}

FVector AShooterWeapon::GetAdjustedAim() const
{
	AUTPlayerController* const PlayerController = Instigator ? Cast<AUTPlayerController>(Instigator->Controller) : NULL;
	FVector FinalAim = FVector::ZeroVector;
	// If we have a player controller use it for the aim
	if (PlayerController)
	{
		FVector CamLoc;
		FRotator CamRot;
		PlayerController->GetPlayerViewPoint(CamLoc, CamRot);
		FinalAim = CamRot.Vector();
	}
	else if (Instigator)
	{
		// Now see if we have an AI controller - we will want to get the aim from there if we do
		AUTBot* AIController = MyPawn ? Cast<AUTBot>(MyPawn->Controller) : NULL;
		if(AIController != nullptr )
		{
			FinalAim = AIController->GetControlRotation().Vector();
		}
		else
		{			
			FinalAim = Instigator->GetBaseAimRotation().Vector();
		}
	}

	return FinalAim;
}

FVector AShooterWeapon::GetCameraDamageStartLocation(const FVector& AimDir) const
{
	AUTPlayerController* PC = MyPawn ? Cast<AUTPlayerController>(MyPawn->Controller) : NULL;
	AUTBot* AIPC = MyPawn ? Cast<AUTBot>(MyPawn->Controller) : NULL;
	FVector OutStartTrace = FVector::ZeroVector;

	if (PC)
	{
		// use player's camera
		FRotator UnusedRot;
		PC->GetPlayerViewPoint(OutStartTrace, UnusedRot);

		// Adjust trace so there is nothing blocking the ray between the camera and the pawn, and calculate distance from adjusted start
		OutStartTrace = OutStartTrace + AimDir * ((Instigator->GetActorLocation() - OutStartTrace) | AimDir);
	}
	else if (AIPC)
	{
		OutStartTrace = GetMuzzleLocation();
	}

	return OutStartTrace;
}

FVector AShooterWeapon::GetMuzzleLocation() const
{
	USceneComponent* UseMesh = GetMuzzleSocketComponent((MyPawn != nullptr && MyPawn->IsFirstPerson()));

	if (GetMuzzleSocketName() != NAME_None)
	{	
		return UseMesh->GetSocketLocation(GetMuzzleSocketName());
	}

	return UseMesh->GetComponentLocation() + UseMesh->GetForwardVector() * 100.0f;
}

FVector AShooterWeapon::GetMuzzleDirection() const
{
	USceneComponent* UseMesh = GetMuzzleSocketComponent((MyPawn != nullptr && MyPawn->IsFirstPerson()));

	if (GetMuzzleSocketName() != NAME_None)
	{		
		return UseMesh->GetSocketRotation(GetMuzzleSocketName()).Vector();
	}

	return UseMesh->GetForwardVector();
}

FName AShooterWeapon::GetMuzzleSocketName() const
{
	if (GetInstance() && GetCurrentAmmoInstance())
	{
		int32 TargetMeshIdx = INDEX_NONE;
		UItemModuleTrunkEntity* ModuleTrunk = Cast<UItemModuleTrunkEntity>(FindInstalledModuleEntity(UItemModuleTrunkEntity::StaticClass(), TargetMeshIdx));
		if (ModuleTrunk && TargetMeshIdx != INDEX_NONE)
		{
			return (GetCurrentAmmoInstance()->GetAmmoType() == EItemAmmoType::Projectile) ? ModuleTrunk->GetMuzzleSocketProj() : ModuleTrunk->GetMuzzleSocketInstant();
		}
		else
		{
			return (GetCurrentAmmoInstance()->GetAmmoType() == EItemAmmoType::Projectile) ? GetWeaponConfiguration().Name_ProjMuzzle : GetWeaponConfiguration().Name_InstantMuzzle;
		}
	}

	return NAME_None;
}

USceneComponent* AShooterWeapon::GetMuzzleSocketComponent(const bool IsFirstView) const
{
	int32 TargetMeshIdx = INDEX_NONE;
	UItemModuleTrunkEntity* ModuleTrunk = Cast<UItemModuleTrunkEntity>(FindInstalledModuleEntity(UItemModuleTrunkEntity::StaticClass(), TargetMeshIdx));
	if (ModuleTrunk && TargetMeshIdx != INDEX_NONE)
	{
		return IsFirstView ? InstalledModulesMesh1P[TargetMeshIdx] : InstalledModulesMesh3P[TargetMeshIdx];
	}
	else
	{
		return IsFirstView ? Mesh1P : Mesh3P;
	}
}

void AShooterWeapon::SetTempMaterial(UMaterialInterface* InMaterial)
{
	auto FPV_Mesh = GetMesh1P();
	auto TPV_Mesh = GetMesh3P();

	if (InMaterial)
	{
		if (FPV_Mesh)
		{
			for (int32 i = 0; i < FPV_Mesh->GetNumMaterials(); i++)
			{
				FPV_Mesh->SetMaterial(i, InMaterial);
			}
		}

		if (TPV_Mesh)
		{
			for (int32 i = 0; i < TPV_Mesh->GetNumMaterials(); i++)
			{
				TPV_Mesh->SetMaterial(i, InMaterial);
			}
		}

		for (auto mesh : InstalledModulesMesh1P)
		{
			for (int32 i = 0; i < mesh->GetNumMaterials(); i++)
			{
				if (mesh->OverrideMaterials.IsValidIndex(i) && Cast<UMaterialInstanceDynamic>(mesh->OverrideMaterials[i]))
				{

				}
				else
				{
					mesh->SetMaterial(i, InMaterial);
				}
			}
		}

		for (auto mesh : InstalledModulesMesh3P)
		{
			for (int32 i = 0; i < mesh->GetNumMaterials(); i++)
			{
				if (mesh->OverrideMaterials.IsValidIndex(i) && Cast<UMaterialInstanceDynamic>(mesh->OverrideMaterials[i]))
				{

				}
				else
				{
					mesh->SetMaterial(i, InMaterial);
				}
			}
		}
	}
	else
	{
		auto EntityInstance = Instance->GetEntityWeapon();

		if (FPV_Mesh && EntityInstance)
		{
			for (int32 i = 0; i < FPV_Mesh->GetNumMaterials(); i++)
			{
				if (i == 0) FPV_Mesh->SetMaterial(0, EntityInstance->GetMaterialByModel(Instance->GetInstalledMaterialModelId()));
				else FPV_Mesh->SetMaterial(i, nullptr);
			}
		}

		if (TPV_Mesh && EntityInstance)
		{
			for (int32 i = 0; i < TPV_Mesh->GetNumMaterials(); i++)
			{
				if (i == 0) TPV_Mesh->SetMaterial(0, EntityInstance->GetMaterialByModel(Instance->GetInstalledMaterialModelId()));
				else TPV_Mesh->SetMaterial(i, nullptr);
			}
		}

		for (auto mesh : InstalledModulesMesh1P)
		{
			for (int32 i = 0; i < mesh->GetNumMaterials(); i++)
			{
				if (mesh->OverrideMaterials.IsValidIndex(i) && Cast<UMaterialInstanceDynamic>(mesh->OverrideMaterials[i]))
				{

				}
				else
				{
					mesh->SetMaterial(i, nullptr);
				}
			}
		}

		for (auto mesh : InstalledModulesMesh3P)
		{
			for (int32 i = 0; i < mesh->GetNumMaterials(); i++)
			{
				if (mesh->OverrideMaterials.IsValidIndex(i) && Cast<UMaterialInstanceDynamic>(mesh->OverrideMaterials[i]))
				{

				}
				else
				{
					mesh->SetMaterial(i, nullptr);
				}
			}
		}
	}
}

FHitResult AShooterWeapon::WeaponTrace(const FVector& StartTrace, const FVector& EndTrace) const
{
	static FName WeaponFireTag = FName(TEXT("WeaponTrace"));

	// Perform trace to retrieve hit info
	FCollisionQueryParams TraceParams(WeaponFireTag, true, Instigator);
	TraceParams.bTraceAsyncScene = true;
	TraceParams.bReturnPhysicalMaterial = true;
	TraceParams.MobilityType = EQueryMobilityType::Any;
	TraceParams.bFindInitialOverlaps = true;
	
	FHitResult Hit(ForceInit);
	GetWorld()->LineTraceSingleByChannel(Hit, StartTrace, EndTrace, COLLISION_TRACE_WEAPONNOCHARACTER /*COLLISION_WEAPON*/, TraceParams);

	return Hit;
}

void AShooterWeapon::SetOwningPawn(AUTCharacter* NewOwner)
{
	if (MyPawn != NewOwner)
	{
		Instigator = NewOwner;
		MyPawn = NewOwner;
		// net owner for RPC calls
		SetOwner(NewOwner);
	}	
}

//////////////////////////////////////////////////////////////////////////
// Replication & effects

void AShooterWeapon::OnRep_MyPawn()
{
	if (MyPawn)
	{
		UE_LOG(LogShooterWeapon, Log, TEXT("OnRep_MyPawn >> MyPawn = %s"), *MyPawn->GetName());
		OnEnterInventory(MyPawn);
	}
	else
	{
		UE_LOG(LogShooterWeapon, Log, TEXT("OnRep_MyPawn >> MyPawn = nullptr"));
		OnLeaveInventory();
	}
}

void AShooterWeapon::OnRep_BurstCounter()
{
	if (BurstCounter > 0)
	{
		SimulateWeaponFire();
	}
	else
	{
		StopSimulatingWeaponFire();
	}
}

void AShooterWeapon::OnRep_Reload()
{
	if (bPendingReload)
	{
		StartReload(true);
	}
	else
	{
		StopReload();
	}
}

void AShooterWeapon::SimulateWeaponFire()
{
	if (Role == ROLE_Authority && CurrentState != EWeaponState::Firing)
	{
		return;
	}

	if (GetInstance())
	{
		bool bIsScopeTargeting = false;

		auto MyCtrl = MyPawn ? Cast<AUTBasePlayerController>(MyPawn->GetController()) : nullptr;

		if (MyCtrl && MyCtrl->Slate_SniperScope.IsValid())
		{
			if (MyCtrl->Slate_SniperScope->GetVisibility() == EVisibility::HitTestInvisible)
			{
				bIsScopeTargeting = true;
			}
		}

		int32 TargetMeshIdx = INDEX_NONE;
		UItemModuleTrunkEntity* ModuleTrunk = Cast<UItemModuleTrunkEntity>(FindInstalledModuleEntity(UItemModuleTrunkEntity::StaticClass(), TargetMeshIdx));

		if (GetWeaponConfiguration().MuzzleFX)
		{
			USkeletalMeshComponent* UseWeaponMesh = GetWeaponMesh();
			if (!GetWeaponConfiguration().bLoopedMuzzleFX || MuzzlePSC == nullptr)
			{
				// Split screen requires we create 2 effects. One that we see and one that the other player sees.
				if ((MyPawn != nullptr) && (MyPawn->IsLocallyControlled() == true))
				{
					AController* PlayerCon = MyPawn->GetController();
					if (PlayerCon != nullptr)
					{
						if (bIsScopeTargeting == false)
						{
							MuzzlePSC = UGameplayStatics::SpawnEmitterAttached(GetWeaponConfiguration().MuzzleFX, GetMuzzleSocketComponent(true), GetMuzzleSocketName());
							MuzzlePSC->bOwnerNoSee = false;
							MuzzlePSC->bOnlyOwnerSee = true;

							if (ModuleTrunk)
							{
								MuzzlePSC->SetFloatParameter("Intensity", ModuleTrunk->GetFlameIntensity());
								MuzzlePSC->SetVectorParameter("Intensity", FVector(ModuleTrunk->GetFlameIntensity()));
							}
						}

						MuzzlePSCSecondary = UGameplayStatics::SpawnEmitterAttached(GetWeaponConfiguration().MuzzleFX, GetMuzzleSocketComponent(false), GetMuzzleSocketName());
						MuzzlePSCSecondary->bOwnerNoSee = true;
						MuzzlePSCSecondary->bOnlyOwnerSee = false;

						if (ModuleTrunk)
						{
							MuzzlePSCSecondary->SetFloatParameter("Intensity", ModuleTrunk->GetFlameIntensity());
							MuzzlePSCSecondary->SetVectorParameter("Intensity", FVector(ModuleTrunk->GetFlameIntensity()));
						}
					}
				}
				else
				{
					MuzzlePSC = UGameplayStatics::SpawnEmitterAttached(GetWeaponConfiguration().MuzzleFX, GetMuzzleSocketComponent(false), GetMuzzleSocketName());

					if (ModuleTrunk)
					{
						MuzzlePSC->SetFloatParameter("Intensity", ModuleTrunk->GetFlameIntensity());
						MuzzlePSC->SetVectorParameter("Intensity", FVector(ModuleTrunk->GetFlameIntensity()));
					}
				}
			}
		}

		if (!GetWeaponConfiguration().bLoopedFireAnim || !bPlayingFireAnim)
		{
			PlayWeaponAnimation(GetWeaponConfiguration().FireAnim);
			bPlayingFireAnim = true;
		}

		PlayMontageWithDuration(Mesh1P, GetWeaponConfiguration().WeaponMontageFire, GetWeaponProperty().TimeBetweenShots());

		if (MyPawn)
		{
//			MyPawn->OnWeaponSimulateFired();

			if (MyPawn->IsTargeting())
			{
				if (MyPawn->IsFirstPerson())
				{
					PlayMontageWithDuration(MyPawn->GetMesh1P(), GetWeaponConfiguration().FireInTargeting.Pawn1P, GetWeaponProperty().TimeBetweenShots());
				}
				else
				{
					PlayMontageWithDuration(MyPawn->GetMesh(), GetWeaponConfiguration().FireInTargeting.Pawn3P, GetWeaponProperty().TimeBetweenShots());
				}
			}
			else
			{
				if (MyPawn->IsFirstPerson())
				{
					PlayMontageWithDuration(MyPawn->GetMesh1P(), GetWeaponConfiguration().PawnFPPMontageFire, GetWeaponProperty().TimeBetweenShots());
				}
				else
				{
					PlayMontageWithDuration(MyPawn->GetMesh(), GetWeaponConfiguration().PawnTPPMontageFire, GetWeaponProperty().TimeBetweenShots());
				}
			}
		}


		if (GetWeaponConfiguration().bLoopedFireSound)
		{
			if (FireAC == nullptr)
			{
				FireAC = PlayWeaponSound(GetWeaponConfiguration().FireLoopSound);

				if (ModuleTrunk)
				{
					FireAC->SetVolumeMultiplier(ModuleTrunk->GetSoundIntensity());
				}
			}
		}
		else
		{
			auto NewSound = PlayWeaponSound(GetWeaponConfiguration().FireSound);

			if (ModuleTrunk && NewSound)
			{
				NewSound->SetVolumeMultiplier(ModuleTrunk->GetSoundIntensity());
			}
		}

		AUTPlayerController* PC = (MyPawn != nullptr) ? Cast<AUTPlayerController>(MyPawn->Controller) : NULL;
		if (PC != nullptr && PC->IsLocalController())
		{
			if (GetWeaponConfiguration().FireCameraShake != nullptr)
			{
				PC->ClientPlayCameraShake(GetWeaponConfiguration().FireCameraShake);
			}
			//else
			//{
			//	if (MyPawn->IsTargeting())
			//	{
			//		float Recoil = GetWeaponProperty().Feedback / 10;
			//		PC->AddPitchInput(-(GetWeaponProperty().Feedback / 5));
			//		PC->AddYawInput(FMath::FRandRange(Recoil, -Recoil));
			//	}
			//	else
			//	{
			//		float Recoil = GetWeaponProperty().Feedback / 2;
			//		PC->AddPitchInput(-GetWeaponProperty().Feedback);
			//		PC->AddYawInput(FMath::FRandRange(Recoil, -Recoil));
			//	}
			//}

			if (GetWeaponConfiguration().FireForceFeedback != nullptr)
			{
				PC->ClientPlayForceFeedback(GetWeaponConfiguration().FireForceFeedback, false, false, "Weapon");
			}
		}
	}
}

UItemModuleEntity* AShooterWeapon::FindInstalledModuleEntity(const UClass* InTargetClass, int32& OutMeshIndex) const
{
	UItemModuleEntity* TargetModule = nullptr;

	if (GetInstance() && GetInstance()->GetInstalledAddons().Num())
	{
		for (auto &m : GetInstance()->GetInstalledAddons())
		{
			auto tm = (m && m->IsValidLowLevel()) ? m->GetEntity<UItemModuleEntity>() : nullptr;
			if (tm && tm->IsA(InTargetClass))
			{
				TargetModule = tm;

				if (InstalledModulesMeshMap.Contains(TargetModule->TargetSocket))
				{
					int32 mesh_count = 0;
					for (auto &mesh : InstalledModulesMesh1P)
					{
						if (mesh == InstalledModulesMeshMap.FindChecked(TargetModule->TargetSocket))
						{
							OutMeshIndex = mesh_count;
							break;
						}

						++mesh_count;
					}
				}
				break;
			}
		}
	}

	return TargetModule;
}

void AShooterWeapon::PlayMontageWithDuration(USkeletalMeshComponent* PlayTarget, UAnimMontage* Montage, float Duration)
{
	if (PlayTarget && PlayTarget->AnimScriptInstance && Montage)
	{
		float playRate = FMath::Max(0.2f, Montage->SequenceLength / Duration);
		PlayTarget->AnimScriptInstance->Montage_Play(Montage, playRate);
	}
}

void AShooterWeapon::StopSimulatingWeaponFire()
{
	if (Instance)
	{
		if (GetWeaponConfiguration().bLoopedMuzzleFX)
		{
			if (MuzzlePSC != nullptr)
			{
				MuzzlePSC->DeactivateSystem();
				MuzzlePSC = NULL;
			}
			if (MuzzlePSCSecondary != nullptr)
			{
				MuzzlePSCSecondary->DeactivateSystem();
				MuzzlePSCSecondary = NULL;
			}
		}

		if (GetWeaponConfiguration().bLoopedFireAnim && bPlayingFireAnim)
		{
			StopWeaponAnimation(GetWeaponConfiguration().FireAnim);

			if (Mesh1P && Mesh1P->AnimScriptInstance && GetWeaponConfiguration().WeaponMontageFire)
			{
				Mesh1P->AnimScriptInstance->Montage_Stop(.1f, GetWeaponConfiguration().WeaponMontageFire);
			}			

			bPlayingFireAnim = false;
		}

		if (MyPawn && MyPawn->GetMesh1P() && MyPawn->GetMesh1P()->AnimScriptInstance && GetWeaponConfiguration().PawnFPPMontageFire)
		{
			MyPawn->GetMesh1P()->AnimScriptInstance->Montage_Stop(.1f, GetWeaponConfiguration().PawnFPPMontageFire);
		}

		if (MyPawn && MyPawn->GetMesh() && MyPawn->GetMesh()->AnimScriptInstance && GetWeaponConfiguration().PawnTPPMontageFire)
		{
			MyPawn->GetMesh()->AnimScriptInstance->Montage_Stop(.1f, GetWeaponConfiguration().PawnTPPMontageFire);
		}

		if (FireAC)
		{
			FireAC->FadeOut(0.1f, 0.0f);
			FireAC = NULL;

			PlayWeaponSound(GetWeaponConfiguration().FireFinishSound);
		}
	}
}

void AShooterWeapon::GetLifetimeReplicatedProps( TArray< FLifetimeProperty > & OutLifetimeProps ) const
{
	Super::GetLifetimeReplicatedProps( OutLifetimeProps );

	DOREPLIFETIME(AShooterWeapon, Instance);
	DOREPLIFETIME(AShooterWeapon, MyPawn);

	DOREPLIFETIME_CONDITION( AShooterWeapon, CurrentAmmoInClip, COND_OwnerOnly );

	DOREPLIFETIME(AShooterWeapon, CurrentAmmo);

	DOREPLIFETIME_CONDITION(AShooterWeapon, CountAmmo, COND_OwnerOnly);

	DOREPLIFETIME_CONDITION(AShooterWeapon, FeedbackData, COND_OwnerOnly);

	DOREPLIFETIME_CONDITION( AShooterWeapon, BurstCounter,		COND_SkipOwner );
	DOREPLIFETIME_CONDITION( AShooterWeapon, bPendingReload,	COND_SkipOwner );	

	DOREPLIFETIME_CONDITION(AShooterWeapon, ShootNotifyInstant, COND_SkipOwner);
}

void AShooterWeapon::OnRep_CountAmmo()
{
	if (ScopeModule && MyPawn)
	{
		for (auto MID : ScopeModuleMIDS)
		{
			if (MID && MID->IsValidLowLevel())
			{
				MID->SetScalarParameterValue(ScopeModule->TPN_CurrentAmmo, GetCurrentAmmoInClip());
				MID->SetScalarParameterValue(ScopeModule->TPN_AllAmmo, GetAmmo());
				MID->SetScalarParameterValue(ScopeModule->TPN_MaxAmmo, GetAmmoPerClip());
			}
		}
	}
}

USkeletalMeshComponent* AShooterWeapon::GetWeaponMesh() const
{
	return (MyPawn != nullptr && MyPawn->IsFirstPerson()) ? Mesh1P : Mesh3P;
}

class AUTCharacter* AShooterWeapon::GetPawnOwner() const
{
	return MyPawn;
}

bool AShooterWeapon::IsEquipped() const
{
	return bIsEquipped;
}

bool AShooterWeapon::IsAttachedToPawn() const
{
	return bIsEquipped || bPendingEquip;
}

EWeaponState::Type AShooterWeapon::GetCurrentState() const
{
	return CurrentState;
}

int32 AShooterWeapon::GetCurrentAmmo() const
{
	if (Instance)
	{
		return CountAmmo[CurrentAmmo];
	}

	return 1;
}

int32 AShooterWeapon::GetCurrentAmmoInClip() const
{
	return CurrentAmmoInClip;
}

int32 AShooterWeapon::GetAmmoPerClip() const
{
	if (Instance)
	{
		return GetWeaponProperty().AmmoPerClip;
	}

	return 1;
}

int32 AShooterWeapon::GetMaxAmmo() const
{
	return 600;// GetWeaponProperty().MaxAmmo;
}

bool AShooterWeapon::HasInfiniteAmmo() const
{
	const AUTPlayerController* MyPC = (MyPawn != nullptr) ? Cast<const AUTPlayerController>(MyPawn->Controller) : NULL;
	return false;// (MyPC && MyPC->HasInfiniteAmmo());
}

bool AShooterWeapon::HasInfiniteClip() const
{
	const AUTPlayerController* MyPC = (MyPawn != nullptr) ? Cast<const AUTPlayerController>(MyPawn->Controller) : NULL;
	return false;// (MyPC && MyPC->HasInfiniteClip());
}

float AShooterWeapon::GetEquipStartedTime() const
{
	return EquipStartedTime;
}

float AShooterWeapon::GetEquipDuration() const
{
	return EquipDuration;
}

void AShooterWeapon::ToggleAimingParams(const bool IsAiming)
{
	if (MyPawn)
	{
		int32 TargetMeshIdx = INDEX_NONE;
		if (auto TargetModule = Cast<UItemModuleScopeOpticalEntity>(FindInstalledModuleEntity(UItemModuleScopeOpticalEntity::StaticClass(), TargetMeshIdx)))
		{
			if (TargetMeshIdx != INDEX_NONE && !TargetModule->IsUseRenderTarget() && !TargetModule->IsSupportLiveParams() && MyPawn->IsInvisible() == false)
			{
				for (auto &i : TargetModule->ReplaceIndexes)
				{
					if (IsAiming)
					{
						InstalledModulesMesh1P[TargetMeshIdx]->SetMaterial(i, TargetModule->ReplaceToMaterial);
						InstalledModulesMesh3P[TargetMeshIdx]->SetMaterial(i, TargetModule->ReplaceToMaterial);
					}
					else
					{
						InstalledModulesMesh1P[TargetMeshIdx]->SetMaterial(i, nullptr);
						InstalledModulesMesh3P[TargetMeshIdx]->SetMaterial(i, nullptr);
					}
				}
			}
		}
		else
		{
			ToggleSniperScope(IsAiming);
		}
	}
}

void AShooterWeapon::ToggleSniperScope(const bool toggle)
{
	bool bEnableSniperScope = false;
	const FSlateBrush* ScopeImage = new FSlateNoResource();
	FSlateColor ScopeBackground;

	int32 TargetMeshIdx = INDEX_NONE;
	if (auto TargetModule = Cast<UItemModuleScopeSniperEntity>(FindInstalledModuleEntity(UItemModuleScopeSniperEntity::StaticClass(), TargetMeshIdx)))
	{
		bEnableSniperScope = true;

		ScopeImage = TargetModule->GetScope();
		ScopeBackground = TargetModule->GetBackground();
	}
	
	if (bEnableSniperScope && MyPawn)
	{
		if (auto MyCtrl = Cast<AUTBasePlayerController>(MyPawn->GetController()))
		{
			if (MyCtrl->Slate_SniperScope.IsValid() || MyCtrl->TryCreateSniperScopeWidget())
			{
				if (toggle)
				{
					MyCtrl->Slate_SniperScope->SetImage(ScopeImage);
					MyCtrl->Slate_SniperScope->SetColor(ScopeBackground);
					MyCtrl->Slate_SniperScope->SetVisibility(EVisibility::HitTestInvisible);
					Mesh1P->SetVisibility(false, true);
					MyPawn->GetMesh1P()->SetVisibility(false, true);
				}
				else
				{
					MyCtrl->Slate_SniperScope->SetVisibility(EVisibility::Hidden);
					Mesh1P->SetVisibility(true, true);
					MyPawn->GetMesh1P()->SetVisibility(true, true);
				}
			}
		}
	}
}

void AShooterWeapon::ApplyOffsetIfAI(FVector &Origin)
{
	//if (auto AIPC = Cast<AUTBot>(MyPawn->Controller))
	//{
	//	Origin.X += FMath::FRandRange(-AIPC->AimRateOffset, AIPC->AimRateOffset);
	//	Origin.Y += FMath::FRandRange(-AIPC->AimRateOffset, AIPC->AimRateOffset);
	//	Origin.Z += FMath::FRandRange(-(AIPC->AimRateOffset / 2), AIPC->AimRateOffset / 2);

	//	//const int32 RandomSeed = FMath::Rand();
	//	//FRandomStream WeaponRandomStream(RandomSeed);		
	//	//Origin = WeaponRandomStream.VRandCone(Origin, FMath::DegreesToRadians(AIPC->AimRateOffset), FMath::DegreesToRadians(AIPC->AimRateOffset / 2));
	//}
}

USkeletalMeshComponent* AShooterWeapon::GetMesh1P() const { return Mesh1P; }
USkeletalMeshComponent* AShooterWeapon::GetMesh3P() const { return Mesh3P; }

float AShooterWeapon::GetWeaponMass() const
{
	if (Instance)
	{
		return Instance->GetMass();
	}
	
	return .0f;
}

float AShooterWeapon::GetWeaponCoefficient() const
{	
	if (GetInstance())
	{
		const float FireRate = 60000.0f / FTimespan::FromSeconds(GetWeaponProperty().TimeBetweenShots()).GetTotalMilliseconds();
		return (GetWeaponProperty().Damage * FireRate) / 2660;
	}

	return .0f;
}

// Ammo specific usage

void AShooterWeapon::SetCurrentAmmo(const TEnumAsByte<EAmmoSlot::Type> Ammo)
{
	if (Role != ROLE_Authority)
	{
		ServerSetCurrentAmmo(CurrentAmmo);
	}
	else
	{
		if (AmmoInstance[Ammo])
		{
			CurrentAmmo = Ammo;
		}
	}
}

bool AShooterWeapon::ServerSetCurrentAmmo_Validate(const EAmmoSlot::Type Ammo)
{
	return true;
}

void AShooterWeapon::ServerSetCurrentAmmo_Implementation(const EAmmoSlot::Type Ammo)
{
	SetCurrentAmmo(TEnumAsByte<EAmmoSlot::Type>(Ammo));
}


UItemAmmoEntity* AShooterWeapon::GetCurrentAmmoInstance() const
{
	//return (CurrentAmmo == EAmmoSlot::Primary) ? GetInstance()->GetPrimaryAmmo() : GetInstance()->GetSecondaryAmmo();
	return AmmoInstance[CurrentAmmo];
}

void AShooterWeapon::TickActor(float DeltaTime, enum ELevelTick TickType, FActorTickFunction& ThisTickFunction)
{
	Super::TickActor(DeltaTime, TickType, ThisTickFunction);

	ProccesFeedback(DeltaTime);
}

void AShooterWeapon::ProccesFeedback(float DeltaTime)
{
	if (MyPawn)
	{
		TargetRecoilX = FMath::FInterpTo(TargetRecoilX, .0f, DeltaTime, 10.0f);
		TargetRecoilY = FMath::FInterpTo(TargetRecoilY, .0f, DeltaTime, 10.0f);

		MyPawn->AddControllerPitchInput(TargetRecoilY);
		MyPawn->AddControllerYawInput(TargetRecoilX);
	}
}

void AShooterWeapon::MakeFeedback()
{
	FeedbackData.Empty();

	float FeedBack = GetWeaponProperty().Feedback;

	for (int32 i = GetAmmoPerClip(); i >= 0; --i)
	{
		FFeedbackData Data;

		Data.Feedback.Y = -FeedBack;
		Data.Feedback.X = FMath::FRandRange(Data.Feedback.Y, Data.Feedback.Y * -1.0f);
		
		Data.ModiferTargeting = FMath::FRandRange(.5f, .8f);
		Data.ModiferCrouched = FMath::FRandRange(.4f, .6f);

		FeedbackData.Add(Data);
	}
}

void AShooterWeapon::UpdateViewBob(float DeltaTime)
{
	AUTPlayerController* MyPC = MyPawn ? Cast<AUTPlayerController>(MyPawn->Controller) : nullptr;
	if (MyPC != nullptr && Mesh1P != nullptr)
	{
		// if weapon is up in first person, view bob with movement
		USkeletalMeshComponent* BobbedMesh = MyPawn->FirstPersonMesh;
		if (FirstPMeshOffset.IsZero())
		{
			FirstPMeshOffset = BobbedMesh->GetRelativeTransform().GetLocation();
			//FirstPMeshRotation = BobbedMesh->GetRelativeTransform().Rotator();
		}
		//if (GetWeaponHand() != EWeaponHand::HAND_Hidden)
		{
			FVector ScaledMeshOffset = FirstPMeshOffset;
			const float FOVScaling = ((MyPC->PlayerCameraManager->GetFOVAngle() - 100.f) * 0.05f);
			if (FOVScaling > 0.f)
			{
				ScaledMeshOffset.X *= (1.f * FOVScaling);
				ScaledMeshOffset.Y *= (1.f * FOVScaling);
				ScaledMeshOffset.Z *= (1.f * FOVScaling);
			}

			BobbedMesh->SetRelativeLocation(ScaledMeshOffset);
			FVector BobOffset = MyPawn->GetWeaponBobOffset(DeltaTime, this);
			BobbedMesh->SetWorldLocation(BobbedMesh->GetComponentLocation() + BobOffset);

			//FRotator NewRotation = MyPawn ? MyPawn->GetControlRotation() : FRotator(0.f, 0.f, 0.f);
			//FRotator FinalRotation = NewRotation;

			// Add some rotation leading
			//if (UTOwner && UTOwner->Controller)
			//{
			//	FinalRotation.Yaw = LagWeaponRotation(NewRotation.Yaw, LastRotation.Yaw, DeltaTime, MaxYawLag, 0);
			//	FinalRotation.Pitch = LagWeaponRotation(NewRotation.Pitch, LastRotation.Pitch, DeltaTime, MaxPitchLag, 1);
			//	FinalRotation.Roll = NewRotation.Roll;
			//}
			//LastRotation = NewRotation;
			//BobbedMesh->SetRelativeRotation(FinalRotation + FirstPMeshRotation);
		}
		//else
		//{
		//	// for first person footsteps
		//	MyPawn->GetWeaponBobOffset(DeltaTime, this);
		//	BobbedMesh->SetRelativeLocation(FirstPMeshOffset + MyPawn->EyeOffset);
		//}
	}
}

bool AShooterWeapon::CanAttack_Implementation(AActor* Target, const FVector& TargetLoc, bool bDirectOnly, FVector& OptimalTargetLoc)
{
	OptimalTargetLoc = TargetLoc;
	bool bVisible = false;
	AUTBot* B = Cast<AUTBot>(MyPawn->Controller);
	if (B != nullptr)
	{
		APawn* TargetPawn = Cast<APawn>(Target);
		if (TargetPawn != nullptr && TargetLoc == Target->GetActorLocation() && B->IsEnemyVisible(TargetPawn))
		{
			bVisible = true;
		}
		else
		{
			// by default bots do not try shooting enemies when the enemy info is stale
			// since even if the target location is visible the enemy is probably not near there anymore
			// subclasses can override if their fire mode(s) are suitable for speculative or predictive firing
			const FBotEnemyInfo* EnemyInfo = (TargetPawn != nullptr) ? B->GetEnemyInfo(TargetPawn, true) : NULL;
			if (EnemyInfo != nullptr && GetWorld()->TimeSeconds - EnemyInfo->LastFullUpdateTime > 1.0f)
			{
				bVisible = false;
			}
			else
			{
				bVisible = B->UTLineOfSightTo(Target, FVector::ZeroVector, false, TargetLoc);
			}
		}
	}
	else
	{
		const FVector StartLoc = GetMuzzleLocation();// GetFireStartLoc();
		FCollisionQueryParams Params(FName(TEXT("CanAttack")), false, Instigator);
		Params.AddIgnoredActor(Target);
		bVisible = !GetWorld()->LineTraceTestByChannel(StartLoc, TargetLoc, COLLISION_TRACE_WEAPON, Params);
	}

	return bVisible;

	//if (bVisible)
	//{
	//	// skip zoom modes by default
	//	TArray< uint8, TInlineAllocator<4> > ValidAIModes;
	//	for (uint8 i = 0; i < GetNumFireModes(); i++)
	//	{
	//		if (Cast<UUTWeaponStateZooming>(FiringState[i]) == nullptr)
	//		{
	//			ValidAIModes.Add(i);
	//		}
	//	}
	//	if (!bPreferCurrentMode && ValidAIModes.Num() > 0)
	//	{
	//		BestFireMode = ValidAIModes[FMath::RandHelper(ValidAIModes.Num())];
	//	}
	//	return true;
	//}
	//else
	//{
	//	return false;
	//}
}

float AShooterWeapon::SuggestAttackStyle_Implementation()
{
	return 0.0f;
}

float AShooterWeapon::SuggestDefenseStyle_Implementation()
{
	return 0.0f;
}

float AShooterWeapon::GetAISelectRating_Implementation()
{
	return (GetCurrentAmmo() > 0) ? BaseAISelectRating : 0.0f;
}

bool AShooterWeapon::IsMeleeWeapon() const
{
	if (GetInstanceEntity())
	{
		return (GetInstanceEntity()->GetWeaponType() == EShooterWeaponType::Shootgun || GetInstanceEntity()->GetWeaponType() == EShooterWeaponType::Pistol);
	}

	return false;
}

bool AShooterWeapon::IsPrioritizeAccuracy() const
{
	if (GetInstanceEntity())
	{
		return (GetInstanceEntity()->GetWeaponType() == EShooterWeaponType::LightMachineGun || GetInstanceEntity()->GetWeaponType() == EShooterWeaponType::SniperRifle || GetInstanceEntity()->GetWeaponType() == EShooterWeaponType::RocketLauncher);
	}

	return false;
}

bool AShooterWeapon::IsRecommendSplashDamage() const
{
	if (GetInstanceEntity())
	{
		return (GetInstanceEntity()->GetWeaponType() == EShooterWeaponType::GrenadeLauncher || GetInstanceEntity()->GetWeaponType() == EShooterWeaponType::RocketLauncher);
	}

	return false;
}

bool AShooterWeapon::IsRecommendSuppressiveFire() const
{
	if (GetInstanceEntity())
	{
		return (GetInstanceEntity()->GetWeaponType() == EShooterWeaponType::GrenadeLauncher);
	}

	return false;
}

bool AShooterWeapon::IsSniping() const
{
	if (GetInstanceEntity())
	{
		return (GetInstanceEntity()->GetWeaponType() == EShooterWeaponType::SniperRifle || GetInstanceEntity()->GetWeaponType() == EShooterWeaponType::RocketLauncher);
	}

	return false;
}

FTransform AShooterWeapon::GetAimOffsetTransform() const
{
	FTransform ReturnValue(FTransform::Identity);

	if (GetInstanceEntity())
	{
		const auto Config = GetInstanceEntity()->GetWeaponConfiguration();
		if (MyPawn && MyPawn->IsTargeting())
		{
			if (ScopeModule)
			{
				ReturnValue = Config.AnimOffsetTransform;
				ReturnValue.SetTranslation(ReturnValue.GetTranslation() + ScopeModule->GetAimOffset());
			}
			else
			{
				ReturnValue = Config.AnimOffsetTransform;
				ReturnValue.SetTranslation(ReturnValue.GetTranslation() + Config.DefaultOffsetTransform.GetTranslation());
			}
		}
		else
		{
			ReturnValue = Config.IdleOffsetTransform;
		}		
	}

	return ReturnValue;
}










void AShooterWeapon::FireWeapon()
{
	if (GetInstance() && GetCurrentAmmoInstance())
	{
		switch (GetCurrentAmmoInstance()->GetAmmoType())
		{
			case EItemAmmoType::Instant: FireWeapon_Instant(); break;
			case EItemAmmoType::Scintilla: FireWeapon_Shotgun(); break;
			case EItemAmmoType::Projectile: FireWeapon_Projectile(); break;
		}
	}
}

void AShooterWeapon::FireWeapon_Instant()
{
	const auto Config = GetCurrentAmmoInstance()->GetPropertyInstant();

	const int32 RandomSeed = FMath::Rand();
	FRandomStream WeaponRandomStream(RandomSeed);
	const float CurrentSpread = GetCurrentSpread();
	const float ConeHalfAngle = FMath::DegreesToRadians(CurrentSpread * 0.5f);

	FVector AimDir = GetAdjustedAim();

	ApplyOffsetIfAI(AimDir);

	const FVector StartTrace = GetCameraDamageStartLocation(AimDir);
	const FVector ShootDir = WeaponRandomStream.VRandCone(AimDir, ConeHalfAngle, ConeHalfAngle);
	const FVector EndTrace = StartTrace + ShootDir * (Config.Distance * 100.0f);

	const FHitResult Impact = WeaponTrace(StartTrace, EndTrace);

	TArray<FInstantShootData> ShootData;
	ShootData.Add(FInstantShootData(Impact, ShootDir, FMath::Lerp<FVector>(Impact.ImpactPoint, EndTrace, .0001f)));

	ProcessInstantHit(ShootData);

	CurrentFiringSpread = FMath::Min(GetWeaponProperty().Spread, CurrentFiringSpread + (GetWeaponProperty().Spread / 10.0f));
}

void AShooterWeapon::FireWeapon_Shotgun()
{
	const auto Config = GetCurrentAmmoInstance()->GetPropertyScintilla();

	const int32 RandomSeed = FMath::Rand();
	FRandomStream WeaponRandomStream(RandomSeed);
	const float CurrentSpread = GetCurrentSpread();
	const float ConeHalfAngle = FMath::DegreesToRadians((Config.Grouping + CurrentSpread) * 0.5f);

	FVector AimDir = GetAdjustedAim();

	ApplyOffsetIfAI(AimDir);

	const int32 CountShots = FMath::RandRange(Config.Min, Config.Max);
	const FVector StartTrace = GetCameraDamageStartLocation(AimDir);

	TArray<FInstantShootData> ShotgunShootData;

	for (SSIZE_T i = 0; i < CountShots; ++i)
	{
		const FVector ShootDir = WeaponRandomStream.VRandCone(AimDir, ConeHalfAngle, ConeHalfAngle);
		const FVector EndTrace = StartTrace + ShootDir * (Config.Distance * 100.0f);

		const FHitResult Impact = WeaponTrace(StartTrace, EndTrace);

		ShotgunShootData.Add(FInstantShootData(Impact, ShootDir, FMath::Lerp<FVector>(Impact.ImpactPoint, EndTrace, .0001f)));
	}

	ProcessInstantHit(ShotgunShootData);
	CurrentFiringSpread = FMath::Min(GetWeaponProperty().Spread, CurrentFiringSpread + (GetWeaponProperty().Spread / 10.0f));
}

void AShooterWeapon::FireWeapon_Projectile()
{
	FVector ShootDir = GetAdjustedAim();
	FVector Origin = GetMuzzleLocation();

	ApplyOffsetIfAI(ShootDir);

	// trace from camera to check what's under crosshair
	const float ProjectileAdjustRange = 10000.0f;
	const FVector StartTrace = GetCameraDamageStartLocation(ShootDir);
	FVector EndTrace = StartTrace + (ShootDir * ProjectileAdjustRange);

	FHitResult Impact = WeaponTrace(StartTrace, EndTrace);

	// and adjust directions to hit that actor
	if (Impact.bBlockingHit)
	{
		const FVector AdjustedDir = (Impact.ImpactPoint - Origin).GetSafeNormal();
		bool bWeaponPenetration = false;

		const float DirectionDot = FVector::DotProduct(AdjustedDir, ShootDir);
		if (DirectionDot < 0.0f)
		{
			// shooting backwards = weapon is penetrating
			bWeaponPenetration = true;
		}
		else if (DirectionDot < 0.5f)
		{
			// check for weapon penetration if angle difference is big enough
			// raycast along weapon mesh to check if there's blocking hit

			FVector MuzzleStartTrace = Origin - GetMuzzleDirection() * 150.0f;
			FVector MuzzleEndTrace = Origin;
			FHitResult MuzzleImpact = WeaponTrace(MuzzleStartTrace, MuzzleEndTrace);

			if (MuzzleImpact.bBlockingHit)
			{
				bWeaponPenetration = true;
			}
		}

		if (bWeaponPenetration)
		{
			// spawn at crosshair position
			Origin = Impact.ImpactPoint - ShootDir * 10.0f;
		}
		else
		{
			// adjust direction to hit
			ShootDir = AdjustedDir;
		}
	}

	//DrawDebugLine(GetWorld(), Origin, Impact.Location, FColor::Cyan, false, 10.0f);

	ServerFireProjectile(Origin, ShootDir);
}

bool AShooterWeapon::ServerNotifyInstantShoot_Validate(const TArray<FInstantShootData>& InShootData)
{
	return true;
}

void AShooterWeapon::ServerNotifyInstantShoot_Implementation(const TArray<FInstantShootData>& InShootData)
{
	const float WeaponAngleDot = FMath::Abs(FMath::Sin(GetCurrentSpread() * PI / 180.f));

	if (Instigator && InShootData.Num())
	{
		TArray<FInstantShootData> ShootDataConfirmed;
		const FVector Origin = GetMuzzleLocation();

		for (auto ShootData : InShootData)
		{			
			if (ShootData.Impact.GetActor())
			{				
				const FVector ViewDir = (ShootData.TraceEnd - Origin).GetSafeNormal();
				const float ViewDotHitDir = FVector::DotProduct(Instigator->GetViewRotation().Vector(), ViewDir);
				bool bEqualViewDot = (ViewDotHitDir > .8f - WeaponAngleDot);
				if (bEqualViewDot == false)
				{
					const FVector OriginCamera = GetCameraDamageStartLocation(ViewDir);
					const FVector ViewDirCamera = (ShootData.TraceEnd - OriginCamera).GetSafeNormal();
					const float ViewDotHitDirCamera = FVector::DotProduct(Instigator->GetViewRotation().Vector(), ViewDirCamera);
					bEqualViewDot = (ViewDotHitDirCamera > .8f - WeaponAngleDot);
				}

				if (bEqualViewDot)
				{
					if (CurrentState != EWeaponState::Idle || GetWeaponProperty().IsAutomaticFire == false)
					{
						if (ShootData.Impact.GetActor()->IsRootComponentStatic() || ShootData.Impact.GetActor()->IsRootComponentStationary())
						{
							ShootDataConfirmed.Add(ShootData);
						}
						else
						{
							// Get the component bounding box
							const FBox HitBox = ShootData.Impact.GetActor()->GetComponentsBoundingBox();

							// calculate the box extent, and increase by a leeway
							FVector BoxExtent = 0.5 * (HitBox.Max - HitBox.Min);
							BoxExtent *= 200.0f;

							// avoid precision errors with really thin objects
							BoxExtent.X = FMath::Max(20.0f, BoxExtent.X);
							BoxExtent.Y = FMath::Max(20.0f, BoxExtent.Y);
							BoxExtent.Z = FMath::Max(20.0f, BoxExtent.Z);

							// Get the box center
							const FVector BoxCenter = (HitBox.Min + HitBox.Max) * 0.5;

							// if we are within client tolerance
							if (FMath::Abs(ShootData.Impact.Location.Z - BoxCenter.Z) < BoxExtent.Z &&
								FMath::Abs(ShootData.Impact.Location.X - BoxCenter.X) < BoxExtent.X &&
								FMath::Abs(ShootData.Impact.Location.Y - BoxCenter.Y) < BoxExtent.Y)
							{
								ShootDataConfirmed.Add(ShootData);
							}
							else
							{
								UE_LOG(LogShooterWeapon, Error, TEXT("%s Rejected client side hit of %s (outside bounding box tolerance)"), *GetNameSafe(this), *GetNameSafe(ShootData.Impact.GetActor()));
							}
						}
					}
				}
				else
				{
					UE_LOG(LogShooterWeapon, Error, TEXT("%s Rejected client side hit of %s (outside view cone tolerance)"), *GetNameSafe(this), *GetNameSafe(ShootData.Impact.GetActor()));
				}
			}
			else
			{
				ShootDataConfirmed.Add(ShootData);
			}
		}

		ProcessInstantHit_Confirmed(ShootDataConfirmed);
	}
}

void AShooterWeapon::ProcessInstantHit(const TArray<FInstantShootData>& InShootData)
{
	if (MyPawn && MyPawn->IsLocallyControlled() && GetNetMode() == NM_Client)
	{
		ServerNotifyInstantShoot(InShootData);
	}

	ProcessInstantHit_Confirmed(InShootData);
}

void AShooterWeapon::ProcessInstantHit_Confirmed(const TArray<FInstantShootData>& InShootData)
{
	if (Role == ROLE_Authority)
	{
		DealDamage(InShootData);
		ShootNotifyInstant = InShootData;

		if (GetInstance() && MyPawn)
		{
			for (auto ShootData : ShootNotifyInstant)
			{
				if (ShootData.Impact.PhysMaterial.IsValid() && ShootData.Impact.PhysMaterial.Get())
				{
					UPhysicalMaterial* HitPhysMat = ShootData.Impact.PhysMaterial.Get();
					AActor* HitActor = ShootData.Impact.GetActor();
					EPhysicalSurface HitSurfaceType = UPhysicalMaterial::DetermineSurfaceType(HitPhysMat);

					if (auto MyState = Cast<AUTPlayerState>(MyPawn->GetPlayerState()))
					{
						FPlayerShootData PlayerShootData;
						// TODO: ��������� �����������
						PlayerShootData.Weapon = FWeaponHelper(GetInstanceEntity());
						PlayerShootData.Surface = HitSurfaceType;

						if (auto HitPawn = Cast<AUTCharacter>(HitActor))
						{
							auto MyGameState = GetWorld()->GetGameState<AUTGameState>();
							auto HitState = Cast<AUTPlayerState>(HitPawn->GetPlayerState());

							if (MyGameState && HitState)
							{
								PlayerShootData.IsPawn_Friendly = (MyGameState && FFlagsHelper::HasAnyFlags(MyGameState->GetGameModeType().Value, EGameMode::LostDeadMatch.ToFlag() | EGameMode::DuelMatch.ToFlag())) ? false : MyGameState->OnSameTeam(HitState, MyState);
								PlayerShootData.IsPawn_Enemy = (MyGameState && FFlagsHelper::HasAnyFlags(MyGameState->GetGameModeType().Value, EGameMode::LostDeadMatch.ToFlag() | EGameMode::DuelMatch.ToFlag())) ? true : MyGameState->OnSameTeam(HitState, MyState) == false;
							}
						}

						MyState->AddShootData(PlayerShootData);
					}
				}
			}
		}
	}

	// play FX locally
	if (GetNetMode() != NM_DedicatedServer)
	{
		for (auto ShootData : InShootData)
		{
			FHitResult Impact = WeaponTrace(GetMuzzleLocation(), ShootData.TraceEnd);
			if (Impact.bBlockingHit)
			{
				SpawnImpactEffects(Impact);
				SpawnTrailEffect(Impact.ImpactPoint);
			}
			else
			{
				SpawnTrailEffect(ShootData.TraceEnd);
			}
		}
	}
}

bool AShooterWeapon::ShouldDealDamage(AActor* TestActor) const
{
	if (TestActor)
	{
		return (GetNetMode() != NM_Client || TestActor->Role == ROLE_Authority || TestActor->GetTearOff());
	}

	return false;
}

void AShooterWeapon::DealDamage(const TArray<FInstantShootData>& InShootData)
{
	TMap<AActor*, float> DamageMap;
	TMap<AActor*, FHitResult> ImpactMap;

	for (auto ShootData : InShootData)
	{
		if (ShootData.Impact.GetActor())
		{
			float& Value = DamageMap.FindOrAdd(ShootData.Impact.GetActor());
			Value += GetWeaponProperty().Damage;

			ImpactMap.Add(ShootData.Impact.GetActor(), ShootData.Impact);
		}
	}

	for (auto actor : DamageMap)
	{
		FPointDamageEvent PointDmg;
		PointDmg.DamageTypeClass = GetCurrentAmmoInstance()->GetPropertyInstant().DamageType;
		PointDmg.HitInfo = ImpactMap[actor.Key];
		PointDmg.ShotDirection = InShootData[0].ShootDir;
		PointDmg.Damage = actor.Value;

		actor.Key->TakeDamage(PointDmg.Damage, PointDmg, MyPawn->Controller, this);
	}
}

void AShooterWeapon::OnRep_ShootNotifyInstant()
{
	for (auto ShootData : ShootNotifyInstant)
	{
		FHitResult Impact = WeaponTrace(GetMuzzleLocation(), ShootData.TraceEnd);
		if (Impact.bBlockingHit)
		{
			SpawnImpactEffects(Impact);
			SpawnTrailEffect(Impact.ImpactPoint);
		}
		else
		{
			SpawnTrailEffect(ShootData.TraceEnd);
		}
	}
}

void AShooterWeapon::SpawnImpactEffects(const FHitResult& Impact)
{
	TSubclassOf<AShooterImpactEffect> ImpactEffect;
	if (GetCurrentAmmoInstance() && GetCurrentAmmoInstance()->GetPropertyInstant().ImpactEffect)
	{
		ImpactEffect = GetCurrentAmmoInstance()->GetPropertyInstant().ImpactEffect;
	}
	else if (GetCurrentAmmoInstance() && GetCurrentAmmoInstance()->GetPropertyScintilla().ImpactEffect)
	{
		ImpactEffect = GetCurrentAmmoInstance()->GetPropertyScintilla().ImpactEffect;
	}

	if (ImpactEffect && Impact.bBlockingHit)
	{
		FHitResult UseImpact = Impact;

		// trace again to find component lost during replication
		if (!Impact.Component.IsValid())
		{
			const FVector StartTrace = Impact.ImpactPoint + Impact.ImpactNormal * 10.0f;
			const FVector EndTrace = Impact.ImpactPoint - Impact.ImpactNormal * 10.0f;
			FHitResult Hit = WeaponTrace(StartTrace, EndTrace);
			UseImpact = Hit;
		}

		FTransform const SpawnTransform(Impact.ImpactNormal.Rotation(), Impact.ImpactPoint);
		//GetCurrentAmmoInstance()->GetPropertyInstant().ImpactEffect.GetDefaultObject()->SpawnEffect(GetWorld(), FTransform(Impact.Normal.Rotation(), Impact.Location), Impact.Component.Get(), nullptr, MyPawn->Controller);

		AShooterImpactEffect* EffectActor = GetWorld()->SpawnActorDeferred<AShooterImpactEffect>(ImpactEffect, SpawnTransform);
		if (EffectActor)
		{
			EffectActor->SurfaceHit = UseImpact;
			UGameplayStatics::FinishSpawningActor(EffectActor, SpawnTransform);
		}
	}
}

void AShooterWeapon::SpawnTrailEffect(const FVector& EndPoint)
{
	UParticleSystem* TraceFX = nullptr;
	FVector TraceFXScale = FVector::OneVector;
	if (GetCurrentAmmoInstance() && GetCurrentAmmoInstance()->GetPropertyInstant().TraceFX)
	{
		TraceFX = GetCurrentAmmoInstance()->GetPropertyInstant().TraceFX;
		TraceFXScale = GetCurrentAmmoInstance()->GetPropertyInstant().TraceFXScale;
	}
	else if (GetCurrentAmmoInstance() && GetCurrentAmmoInstance()->GetPropertyScintilla().TraceFX)
	{
		TraceFX = GetCurrentAmmoInstance()->GetPropertyScintilla().TraceFX;
		TraceFXScale = GetCurrentAmmoInstance()->GetPropertyScintilla().TraceFXScale;
	}

	if (TraceFX && MyPawn)
	{
		auto Trans = GetMuzzleSocketComponent(MyPawn->IsFirstPerson())->GetSocketTransform(GetMuzzleSocketName());

		UParticleSystemComponent* TrailPSC = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), TraceFX, Trans);
		if (TrailPSC)
		{
			TrailPSC->SetRelativeScale3D(TraceFXScale);
			TrailPSC->SetVectorParameter(TEXT("HitLocation"), EndPoint);
		}
	}
}

float AShooterWeapon::GetCurrentSpread() const
{
	float FinalSpread = GetWeaponProperty().Spread + CurrentFiringSpread;

	if (MyPawn && MyPawn->bIsCrouched)
	{
		FinalSpread *= .8f;
	}

	if (MyPawn && MyPawn->IsTargeting())
	{
		FinalSpread *= .5f;
	}

	return FinalSpread;
}

bool AShooterWeapon::ServerFireProjectile_Validate(FVector Origin, FVector_NetQuantizeNormal ShootDir)
{
	return true;
}

void AShooterWeapon::ServerFireProjectile_Implementation(FVector Origin, FVector_NetQuantizeNormal ShootDir)
{
	if (GetInstance() && GetCurrentAmmoInstance())
	{
		auto ProjTemplate = GetCurrentAmmoInstance()->GetPropertyProjectile().Projectile;
		FTransform ProjTransform(FRotationMatrix::MakeFromX(ShootDir).Rotator(), Origin);

		AUTProjectile* Projectile = GetWorld()->SpawnActorDeferred<AUTProjectile>(ProjTemplate, ProjTransform, this, Instigator, ESpawnActorCollisionHandlingMethod::AlwaysSpawn);

		auto localEntityInstance = GetInstanceEntity();
		auto localWeaponInstance = GetInstance();
		auto localAmmoInstance = GetCurrentAmmoInstance();

		if (Projectile && localEntityInstance && localWeaponInstance && localAmmoInstance)
		{
			Projectile->SetIgnoreActor(this);
			Projectile->SetIgnoreActor(GetOwner());
			Projectile->ShooterLocation = Origin;

			Projectile->DamageParams.BaseDamage = localWeaponInstance->GetProperty().Damage;
			Projectile->DamageParams.OuterRadius = localAmmoInstance->GetPropertyProjectile().Radius * 100.0f;
			Projectile->DamageParams.InnerRadius = Projectile->DamageParams.OuterRadius / 3.5f;

			Projectile->OwnerGategory = localEntityInstance->GetDescription().CategoryType;
			Projectile->OwnerModel = localEntityInstance->GetModelId();

			Projectile->FinishSpawning(ProjTransform);
		}
	}
}

UItemWeaponEntity* AShooterWeapon::GetInstanceEntity() const
{
	return Instance ? Instance->GetEntityWeapon() : nullptr;
}

FWeaponPropertyBase AShooterWeapon::GetWeaponProperty() const
{
	return GetInstance()->GetProperty();
}

FWeaponConfiguration AShooterWeapon::GetWeaponConfiguration() const
{
	if (auto localEntityInstance = GetInstanceEntity())
	{
		return localEntityInstance->GetWeaponConfiguration();
	}

	return FWeaponConfiguration();
}