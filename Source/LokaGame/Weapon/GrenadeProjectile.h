// VRSPRO

#pragma once

#include "GameFramework/Actor.h"
#include "GrenadeProjectile.generated.h"

class UItemGrenadeEntity;

UCLASS()
class LOKAGAME_API AGrenadeProjectile : public AActor
{
	friend class AShooterWeapon_Projectile;
	friend class AShooterCharacter;

	friend class AShooterWeapon_Projectile;
	friend class AUTCharacter;

	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGrenadeProjectile();

	/** initial setup */
	virtual void PostInitializeComponents() override;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	virtual bool ReplicateSubobjects(UActorChannel *Channel, class FOutBunch *Bunch, FReplicationFlags *RepFlags) override;

	void Explode();

	UFUNCTION()
	void OnBounce(const FHitResult& ImpactResult, const FVector& ImpactVelocity);

	UPROPERTY(VisibleDefaultsOnly, Category = Projectile)
	USkeletalMeshComponent* MeshComp;

protected:

	UPROPERTY(VisibleDefaultsOnly, Category = Projectile)
	UProjectileMovementComponent* MovementComp;

	UPROPERTY(VisibleDefaultsOnly, Category = Projectile)
	UParticleSystemComponent* ParticleComp;

	UPROPERTY(Transient, ReplicatedUsing=OnRep_Instance)
	UItemGrenadeEntity* Instance;

	UFUNCTION()
	void OnRep_Instance();

	FTimerHandle timer_Detonation;
	FTimerHandle timer_Phisycs;

	UPROPERTY(Transient, ReplicatedUsing = OnRep_Explode)
	uint32 bExploded : 1;

	UFUNCTION()
	void OnRep_Explode();

};
