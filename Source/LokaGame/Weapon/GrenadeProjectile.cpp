// VRSPRO

#include "LokaGame.h"
#include "Particles/ParticleSystemComponent.h"
#include "Grenade/ItemGrenadeEntity.h"
#include "GrenadeProjectile.h"
#include "Engine/ActorChannel.h"


// Sets default values
AGrenadeProjectile::AGrenadeProjectile()
	: Super()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComp = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("MeshComp"));
	MeshComp->bTraceComplexOnMove = true;
	MeshComp->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	MeshComp->SetCollisionObjectType(COLLISION_PROJECTILE);
	MeshComp->SetCollisionResponseToAllChannels(ECR_Block);
	MeshComp->SetCollisionResponseToChannel(COLLISION_PLAYERBUILDING_BOX, ECR_Ignore);
	MeshComp->bAllowAnyoneToDestroyMe = false;
	RootComponent = MeshComp;

	//ParticleComp = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("ParticleComp"));
	//ParticleComp->bAutoActivate = false;
	//ParticleComp->bAutoDestroy = false;
	//ParticleComp->AttachParent = MeshComp;

	MovementComp = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("MovementComp"));
	MovementComp->UpdatedComponent = MeshComp;
	MovementComp->InitialSpeed = 2000.0f;
	MovementComp->MaxSpeed = 10000.0f;
	MovementComp->bRotationFollowsVelocity = false;
	MovementComp->bInitialVelocityInLocalSpace = true;
	MovementComp->ProjectileGravityScale = 0.5f;
	MovementComp->bShouldBounce = true;
	MovementComp->bBounceAngleAffectsFriction = true;
	MovementComp->Bounciness = .2f;
	MovementComp->Friction = 1.0f;
	MovementComp->BounceVelocityStopSimulatingThreshold = .1f;
	MovementComp->Velocity = FVector(1000.0f, -0.4f, 0.0f);
	MovementComp->bAllowAnyoneToDestroyMe = false;
	MovementComp->bAutoActivate = true;
	MovementComp->OnProjectileBounce.AddDynamic(this, &AGrenadeProjectile::OnBounce);
	
	bExploded = false;
	bReplicates = true;
	bReplicateMovement = true;
}

void AGrenadeProjectile::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	MeshComp->IgnoreActorWhenMoving(Instigator, true);

	if (Role == ROLE_Authority)
	{
		MeshComp->SetSkeletalMesh(Instance->GetSkeletalMesh());		

		if (Instance->GetGrenadeProperty().DetonationIn > .0f)
		{
			GetWorldTimerManager().SetTimer(timer_Detonation, FTimerDelegate::CreateUObject(this, &AGrenadeProjectile::Explode), Instance->GetGrenadeProperty().DetonationIn, false);
		}
	}
}

// Called when the game starts or when spawned
void AGrenadeProjectile::BeginPlay()
{
	Super::BeginPlay();	
}

// Called every frame
void AGrenadeProjectile::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void AGrenadeProjectile::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AGrenadeProjectile, Instance);
	DOREPLIFETIME(AGrenadeProjectile, bExploded);
}

bool AGrenadeProjectile::ReplicateSubobjects(UActorChannel *Channel, class FOutBunch *Bunch, FReplicationFlags *RepFlags)
{
	bool WroteSomething = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	if (Instance != nullptr)
	{
		WroteSomething |= Channel->ReplicateSubobject(Instance, *Bunch, *RepFlags);
	}

	return WroteSomething;
}

void AGrenadeProjectile::OnRep_Instance()
{
	MeshComp->SetSkeletalMesh(Instance->GetSkeletalMesh());
}

void AGrenadeProjectile::OnBounce(const FHitResult& ImpactResult, const FVector& ImpactVelocity)
{
	if (Role == ROLE_Authority)
	{
		if (!timer_Phisycs.IsValid())
		{
			GetWorldTimerManager().SetTimer(timer_Phisycs, FTimerDelegate::CreateLambda([&]() {
				MeshComp->SetSimulatePhysics(true);
				MeshComp->SetAngularDamping(.01f);
				MeshComp->SetEnableGravity(true);
			}), .1f, false);
		}

		if (Instance->GetGrenadeProperty().DetonationIn <= 0.1f && bExploded == false)
		{
			Explode();
		}
	}
}

void AGrenadeProjectile::Explode()
{
	if (Role == ROLE_Authority)
	{
		if (Instance)
		{
			bExploded = true;

			const auto Property = Instance->GetGrenadeProperty();

			TArray<AActor*> IgnoreActors;
			UGameplayStatics::ApplyRadialDamage(GetWorld(), Property.Damage, GetActorLocation(), Property.Radius, Property.DamageType, IgnoreActors, this, Instigator->GetController(), false, COLLISION_PROJECTILE);
		}

		SetLifeSpan(2.0f);
	}
}

void AGrenadeProjectile::OnRep_Explode()
{
	if (Instance)
	{
		const auto Property = Instance->GetGrenadeProperty();

		if (Property.ExplosionFX)
		{
			UGameplayStatics::SpawnEmitterAtLocation(this, Property.ExplosionFX, MeshComp->GetComponentLocation(), MeshComp->GetComponentRotation());
		}

		if (Property.ExplosionSound)
		{
			UGameplayStatics::PlaySoundAtLocation(this, Property.ExplosionSound, MeshComp->GetComponentLocation());
		}

		MeshComp->SetVisibility(false);
	}
}