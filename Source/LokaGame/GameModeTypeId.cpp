#include "LokaGame.h"
#include "GameModeTypeId.h"

const EGameMode EGameMode::None(EBlueprintGameMode::None);
const EGameMode EGameMode::TeamDeadMatch(EBlueprintGameMode::TeamDeadMatch);
const EGameMode EGameMode::LostDeadMatch(EBlueprintGameMode::LostDeadMatch);
const EGameMode EGameMode::ResearchMatch(EBlueprintGameMode::ResearchMatch);
const EGameMode EGameMode::DuelMatch(EBlueprintGameMode::DuelMatch);
const EGameMode EGameMode::Attack(EBlueprintGameMode::Attack);
const EGameMode EGameMode::PVE_Waves(EBlueprintGameMode::PVE_Waves);
const EGameMode EGameMode::Construction(EBlueprintGameMode::Construction);

const EGameMode EGameMode::AllFight(TeamDeadMatch.ToFlag() | ResearchMatch.ToFlag() | DuelMatch.ToFlag() | LostDeadMatch.ToFlag());

EGameMode::EGameMode()
	: Value(0)
{
	
}

EGameMode::EGameMode(const int32 InValue)
	: Value(InValue)
{
	
}

EGameMode::EGameMode(EBlueprintGameMode InValue)
	: Value(1 << static_cast<int32>(InValue))
{
	
}

FText EGameMode::GetDisplayName() const
{
	if (*this == None)			return NSLOCTEXT("EGameModeTypeId", "EGameModeTypeId.None", "New Game Mode");
	if (*this == TeamDeadMatch) return NSLOCTEXT("EGameModeTypeId", "EGameModeTypeId.TeamDeadMatch", "Team Fight");
	if (*this == LostDeadMatch) return NSLOCTEXT("EGameModeTypeId", "EGameModeTypeId.LostDeadMatch", "Last Hero");	
	if (*this == ResearchMatch) return NSLOCTEXT("EGameModeTypeId", "EGameModeTypeId.ResearchMatch", "Research");
	if (*this == DuelMatch)		return NSLOCTEXT("EGameModeTypeId", "EGameModeTypeId.DuelMatch", "Duel");
	if (*this == Construction)	return NSLOCTEXT("EGameModeTypeId", "EGameModeTypeId.Building", "Construction mode");
	if (*this == PVE_Waves)		return NSLOCTEXT("EGameModeTypeId", "EGameModeTypeId.PVE_Waves", "Waves");
	if (*this == Attack)		return NSLOCTEXT("EGameModeTypeId", "EGameModeTypeId.Attack", "Attack");
	if (*this == AllFight)		return NSLOCTEXT("EGameModeTypeId", "EGameModeTypeId.AllFight", "All Fight");

	return NSLOCTEXT("EGameModeTypeId", "EGameModeTypeId.Mixed", "Mixed");
}

FText EGameMode::GetDescription() const
{
	if (*this == None)			return NSLOCTEXT("EGameModeTypeId", "EGameModeTypeId.None.Desc", "Coming Soon");
	if (*this == TeamDeadMatch) return NSLOCTEXT("EGameModeTypeId", "EGameModeTypeId.TeamDeadMatch.Desc", "In this mode you need to kill enemies from the other team while trying not to die as each death is minus a point to your team, the winner is determined by highest points.");
	if (*this == LostDeadMatch) return NSLOCTEXT("EGameModeTypeId", "EGameModeTypeId.LostDeadMatch.Desc", "In this mode you must kill the opponents while trying not to die yourself because you have a limited amount of lives, the winner is determined by highest points and it is necessary to preserve life.");
	if (*this == ResearchMatch) return NSLOCTEXT("EGameModeTypeId", "EGameModeTypeId.ResearchMatch.Desc", "In this mode you must capture and hold control points to achieve the highest percentage of research, the winner is determined by highest percentage of the study.");
	if (*this == DuelMatch)		return NSLOCTEXT("EGameModeTypeId", "EGameModeTypeId.DuelMatch.Desc", "In this mode, two players compete with a limited number of lives for their prizes.");
	if (*this == Construction)	return NSLOCTEXT("EGameModeTypeId", "EGameModeTypeId.Building.Desc", "Construct your own house.");
	if (*this == PVE_Waves)		return NSLOCTEXT("EGameModeTypeId", "EGameModeTypeId.PVE_Waves.Desc", "In this game mode you have to survive and kill in the allotted time all enemies.");
	if (*this == Attack)		return NSLOCTEXT("EGameModeTypeId", "EGameModeTypeId.Attack.Desc", "In this game mode, if you are the owner then you need to fend off the attackers, if you are an attacker then your task is to destroy and rob the owner.");
	if (*this == AllFight)		return NSLOCTEXT("EGameModeTypeId", "EGameModeTypeId.AllFight.Desc", "All fight game modes");

	return NSLOCTEXT("EGameModeTypeId", "EGameModeTypeId.Mixed", "Mixed");
}

FString EGameMode::GetName() const
{
	if (*this == TeamDeadMatch) return	FString("TeamDeadMatch");
	if (*this == LostDeadMatch) return	FString("LostDeadMatch");
	if (*this == ResearchMatch) return	FString("ResearchMatch");
	if (*this == DuelMatch)		return	FString("DuelMatch");
	if (*this == Construction)	return	FString("LobbyGame");
	if (*this == PVE_Waves)		return	FString("PVE_Waves");
	if (*this == Attack)		return	FString("Attack");

	return FString::Printf(TEXT("Unkown: %d"), Value);
}

FString EGameMode::ToString() const
{
	FString OutString;

	const auto array = ToArray();
	for (auto i : array)
	{
		OutString.Append(i.GetName() + " | ");
	}

	OutString.RemoveFromEnd(" | ");
	return OutString;
}

int32 EGameMode::ToFlag() const
{
	return static_cast<int32>(Value);
}

bool EGameMode::HasAnyFlags(const int32 InValue) const
{
	return FFlagsHelper::HasAnyFlags<int32>(Value, InValue);
}

//int32 EGameMode::ToFlags() const
//{
//	int32 OutValue = 0;
//
//	for (auto aValue : ToArray())
//	{
//		FFlagsHelper::SetFlags(OutValue, OutValue | aValue.ToFlag());
//	}
//
//	return OutValue;
//}

TArray<EGameMode> EGameMode::ToArray() const
{
	TArray<EGameMode> OutArray;

	for (int32 i = static_cast<int32>(EBlueprintGameMode::None); i < static_cast<int32>(EBlueprintGameMode::End); ++i)
	{
		int32 localValue = static_cast<int32>(Value);
		if (FFlagsHelper::HasAnyFlags<int32>(localValue, FFlagsHelper::ToFlag<int32>(i)))
		{
			OutArray.Add(EGameMode(static_cast<EBlueprintGameMode>(i)));
		}
	}

	return OutArray;
}

bool EGameMode::operator==(const EGameMode& Other) const
{
	return this->Value == Other.Value;
}

bool EGameMode::operator!=(const EGameMode& Other) const
{
	return this->Value != Other.Value;
}

EGameMode& EGameMode::operator=(const int32 InValue)
{
	this->Value = InValue;
	return *this;
}


const EGameMap EGameMap::None(EBlueprintGameMap::None);
const EGameMap EGameMap::PvP(EBlueprintGameMap::PvP);
const EGameMap EGameMap::Outpost(EBlueprintGameMap::Outpost);
const EGameMap EGameMap::Hangar(EBlueprintGameMap::Hangar);
const EGameMap EGameMap::MilitaryBase(EBlueprintGameMap::MilitaryBase);
const EGameMap EGameMap::Tropic(EBlueprintGameMap::Tropic);
const EGameMap EGameMap::DesertTown(EBlueprintGameMap::DesertTown);
const EGameMap EGameMap::Lobby(EBlueprintGameMap::Lobby);

const EGameMap EGameMap::AllFight(static_cast<EBlueprintGameMap>(PvP.ToFlag() | Hangar.ToFlag() | MilitaryBase.ToFlag() | DesertTown.ToFlag()));


EGameMap::EGameMap(): Value(0)
{
	
}

EGameMap::EGameMap(const int32 InValue)
	: Value(InValue)
{
	
}

EGameMap::EGameMap(EBlueprintGameMap InValue)
	: Value(1 << static_cast<int32>(InValue))
{
	
}

FText EGameMap::GetDisplayName() const
{

	if (*this == PvP) return NSLOCTEXT("EGameMapTypeId", "EGameMapTypeId.1x1", "1x1 [WIP]");
	if (*this == Outpost) return NSLOCTEXT("EGameMapTypeId", "EGameMapTypeId.Outpost", "Outpost");
	if (*this == Hangar) return NSLOCTEXT("EGameMapTypeId", "EGameMapTypeId.Hangar", "Hangar");
	if (*this == MilitaryBase) return NSLOCTEXT("EGameMapTypeId", "EGameMapTypeId.MilitaryBase", "Military Base");
	if (*this == Tropic) return NSLOCTEXT("EGameMapTypeId", "EGameMapTypeId.Tropic", "Tropic");
	if (*this == DesertTown) return NSLOCTEXT("EGameMapTypeId", "EGameMapTypeId.DesertTown", "Desert Town");
	if (*this == Lobby) return NSLOCTEXT("EGameMapTypeId", "EGameMapTypeId.Lobby", "Lobby");

	return *this == AllFight ? NSLOCTEXT("EGameMapTypeId", "EGameMapTypeId.AllFight", "All Fight") : NSLOCTEXT("EGameMapTypeId", "EGameMapTypeId.Mixed", "Mixed");
}

FText EGameMap::GetDescription() const
{
	if (*this == PvP) return NSLOCTEXT("EGameMapTypeId", "EGameMapTypeId.1x1", "1x1 [WIP]");
	if (*this == Outpost) return NSLOCTEXT("EGameMapTypeId", "EGameMapTypeId.Outpost", "Outpost");
	if (*this == Hangar) return NSLOCTEXT("EGameMapTypeId", "EGameMapTypeId.Hangar", "Hangar");
	if (*this == MilitaryBase) return NSLOCTEXT("EGameMapTypeId", "EGameMapTypeId.MilitaryBase", "Military Base");
	if (*this == Tropic) return NSLOCTEXT("EGameMapTypeId", "EGameMapTypeId.Tropic", "Tropic");
	if (*this == DesertTown) return NSLOCTEXT("EGameMapTypeId", "EGameMapTypeId.DesertTown", "Desert Town");
	if (*this == Lobby) return NSLOCTEXT("EGameMapTypeId", "EGameMapTypeId.Lobby", "Lobby");

	return *this == AllFight ? NSLOCTEXT("EGameMapTypeId", "EGameMapTypeId.AllFight", "All Fight") : FText::GetEmpty();
}

FString EGameMap::GetName() const
{
	if (*this == PvP) return FString("1x1");
	if (*this == Outpost) return FString("Outpost");
	if (*this == Hangar) return FString("Hangar2");
	if (*this == MilitaryBase) return FString("MilitaryBase");
	if (*this == Tropic) return FString("Tropic");
	if (*this == DesertTown) return FString("DesertTown");
	if (*this == Lobby) return FString("LobbyMap");

	return FString::Printf(TEXT("Unkown: %d"), Value);
}

FString EGameMap::ToString() const
{
	FString OutString;

	const auto array = ToArray();

	for (auto i : array)
	{
		OutString.Append(i.GetName() + " | ");
	}

	OutString.RemoveFromEnd(" | ");
	return OutString;
}

int32 EGameMap::ToFlag() const
{
	return Value;
}

TArray<EGameMap> EGameMap::ToArray() const
{
	TArray<EGameMap> OutArray;

	for (int32 i = static_cast<int32>(EBlueprintGameMap::None); i < static_cast<int32>(EBlueprintGameMap::End); ++i)
	{
		int32 localValue = static_cast<int32>(Value);
		if (FFlagsHelper::HasAnyFlags<int32>(localValue, FFlagsHelper::ToFlag<int32>(i)))
		{
			OutArray.Add(EGameMap(static_cast<EBlueprintGameMap>(i)));
		}
	}

	return OutArray;
}

bool EGameMap::operator==(const EGameMap& Other) const
{
	return this->Value == Other.Value;
}

bool EGameMap::operator!=(const EGameMap& Other) const
{
	return this->Value != Other.Value;
}

EGameMap& EGameMap::operator=(int32 InValue)
{
	this->Value = InValue;
	return *this;
}

FText FGameModeTypeId::GetText(const EGameModeTypeId::Type& Id)
{
	switch (Id)
	{
		case EGameModeTypeId::None: return NSLOCTEXT("EGameModeTypeId", "EGameModeTypeId.None", "New Game Mode");
		case EGameModeTypeId::TeamDeadMatch: return NSLOCTEXT("EGameModeTypeId", "EGameModeTypeId.TeamDeadMatch", "Team Fight");
		case EGameModeTypeId::LostDeadMatch: return NSLOCTEXT("EGameModeTypeId", "EGameModeTypeId.LostDeadMatch", "Last Hero");
		case EGameModeTypeId::ResearchMatch: return NSLOCTEXT("EGameModeTypeId", "EGameModeTypeId.ResearchMatch", "Research");
		case EGameModeTypeId::DuelMatch: return NSLOCTEXT("EGameModeTypeId", "EGameModeTypeId.DuelMatch", "Duel");
		case EGameModeTypeId::Building: return NSLOCTEXT("EGameModeTypeId", "EGameModeTypeId.Building", "Construction mode");
		case EGameModeTypeId::Lobby: return NSLOCTEXT("EGameModeTypeId", "EGameModeTypeId.Lobby", "Lobby");
		case EGameModeTypeId::PVE_Waves: return NSLOCTEXT("EGameModeTypeId", "EGameModeTypeId.PVE_Waves", "Waves");
		case EGameModeTypeId::Attack: return NSLOCTEXT("EGameModeTypeId", "EGameModeTypeId.Attack", "Attack");
		default: return NSLOCTEXT("EGameModeTypeId", "EGameModeTypeId.All", "All");
	}
}

FText FGameModeTypeId::GetTextDesc(const EGameModeTypeId::Type& Id)
{
	switch (Id)
	{
		case EGameModeTypeId::None: return NSLOCTEXT("EGameModeTypeId", "EGameModeTypeId.None.Desc", "Coming Soon");
		case EGameModeTypeId::TeamDeadMatch: return NSLOCTEXT("EGameModeTypeId", "EGameModeTypeId.TeamDeadMatch.Desc",
				"In this mode you need to kill enemies from the other team while trying not to die as each death is minus a point to your team, the winner is determined by highest points."
			);
		case EGameModeTypeId::LostDeadMatch: return NSLOCTEXT("EGameModeTypeId", "EGameModeTypeId.LostDeadMatch.Desc",
				"In this mode you must kill the opponents while trying not to die yourself because you have a limited amount of lives, the winner is determined by highest points and it is necessary to preserve life."
			);
		case EGameModeTypeId::ResearchMatch: return NSLOCTEXT("EGameModeTypeId", "EGameModeTypeId.ResearchMatch.Desc",
				"In this mode you must capture and hold control points to achieve the highest percentage of research, the winner is determined by highest percentage of the study."
			);
		case EGameModeTypeId::DuelMatch: return NSLOCTEXT("EGameModeTypeId", "EGameModeTypeId.DuelMatch.Desc",
				"In this mode, two players compete with a limited number of lives for their prizes.");
		case EGameModeTypeId::Building: return NSLOCTEXT("EGameModeTypeId", "EGameModeTypeId.Building.Desc",
				"Construct your own house.");
		case EGameModeTypeId::PVE_Waves: return NSLOCTEXT("EGameModeTypeId", "EGameModeTypeId.PVE_Waves.Desc",
				"In this game mode you have to survive and kill in the allotted time all enemies.");
		case EGameModeTypeId::Attack: return NSLOCTEXT("EGameModeTypeId", "EGameModeTypeId.Attack.Desc",
				"In this game mode, if you are the owner then you need to fend off the attackers, if you are an attacker then your task is to destroy and rob the owner."
			);
	}
	return NSLOCTEXT("EGameModeTypeId", "EGameModeTypeId.All", "All");
}

FString FGameModeTypeId::GetString(const EGameModeTypeId::Type& Id)
{
	switch (Id)
	{
		case EGameModeTypeId::TeamDeadMatch: return FString("TeamDeadMatch");
		case EGameModeTypeId::LostDeadMatch: return FString("LostDeadMatch");
		case EGameModeTypeId::ResearchMatch: return FString("ResearchMatch");
		case EGameModeTypeId::DuelMatch: return FString("DuelMatch");
		case EGameModeTypeId::Building: return FString("BuildGame");
		case EGameModeTypeId::Attack: return FString("Attack");
		case EGameModeTypeId::Lobby: return FString("LobbyGame");
		case EGameModeTypeId::PVE_Waves: return FString("PVE_Waves");
	}
	return FString();
}
