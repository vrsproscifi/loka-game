// VRSPRO

#include "LokaGame.h"
#include "AnalyticsInterface.h"

FLokaAnalytics::FLokaAnalytics()
{
	Actions.Empty();
}

FLokaAnalytics* FLokaAnalytics::Get()
{
	static FLokaAnalytics* Instance;
	if (Instance == nullptr)
	{
		Instance = new FLokaAnalytics();
	}

	return Instance;
}

TSharedRef<FAnalyticAction> FLokaAnalytics::MakeAction(const FAnalyticAction& InAction)
{
	Actions.Add(MakeShareable(new FAnalyticAction(InAction)));
	return Actions.Last().ToSharedRef();
}

TSharedRef<FAnalyticAction> FLokaAnalytics::MakeActionAndStart(FLokaAnalyticsHelper* InOwner, const FAnalyticAction& InAction)
{	
	auto Action = MakeAction(InAction);
	InOwner->StartAnalyticAction(Action);
	return Action;
}

void FLokaAnalytics::ClearActions()
{
	Actions.Empty();
}

TArray<TSharedRef<FAnalyticAction>> FLokaAnalytics::GetActions() const
{
	TArray<TSharedRef<FAnalyticAction>> ReturnActions;
	for (auto a : Actions)
	{
		ReturnActions.Add(a.ToSharedRef());
	}

	return ReturnActions;
}

void FLokaAnalyticsHelper::StartAnalyticAction(TSharedRef<FAnalyticAction> InAction)
{
	AnalyticsMap.Add(InAction->Action, InAction);
	InAction->Start();
}

bool FLokaAnalyticsHelper::StopAnalyticAction(TSharedRef<FAnalyticAction> InAction)
{
	if (AnalyticsMap.Contains(InAction->Action))
	{
		InAction->Stop();
		AnalyticsMap.Remove(InAction->Action);
		return true;
	}

	return false;
}

bool FLokaAnalyticsHelper::StopAnalyticAction(const int32& InAction)
{
	if (AnalyticsMap.Contains(InAction))
	{
		AnalyticsMap.FindChecked(InAction)->Stop();
		AnalyticsMap.Remove(InAction);
		return true;
	}

	return false;
}