#include "LokaGame.h"
#include "PlayerInventoryItem.h"
#include "Item/ItemBaseEntity.h"
#include "GameSingleton.h"
#include "Models/InvertoryItemWrapper.h"
#include "Module/ItemModuleEntity.h"

void UPlayerInventoryItem::InitializeInstanceDynamic(UItemBaseEntity* InInstance, const bool InIsReplicated)
{	
	SetItemEntity(InInstance);

	if (IsRegistered() == false)
	{
		RegisterComponent();
	}

	SetIsReplicated(InIsReplicated);
}

void UPlayerInventoryItem::InitializeAddons(const FInvertoryItemWrapper& MyData, const TArray<UPlayerInventoryItem*>& InAddonsList)
{
	// Initialize Available Addons
	for (auto AvailableAddon : MyData.AvalibleAddons)
	{
		if (auto FoundTarget = InAddonsList.FindByPredicate([id = AvailableAddon.EntityId](UPlayerInventoryItem* InSource) { return InSource->IsEqual(id); }))
		{
			if (auto item = GetValidObject(*FoundTarget))
			{
				AvailableAddons.AddUnique(item);
			}
		}
	}

	// Initialize Installed Addons
	for (auto InstalledAddon : MyData.InstalledAddons)
	{
		if (InstalledAddon.AddonSlot == 100)
		{
			if(auto result = InAddonsList.FindByPredicate([id = InstalledAddon.AddonId](const UPlayerInventoryItem* InSource) { return InSource->IsEqual(id); }))
			{
				if(auto item = GetValidObject(*result))
				{
					InstalledMaterial = item->GetEntityModel().ModelId;
				}
			}	
		}
		else
		{
			if (auto result = InAddonsList.FindByPredicate([id = InstalledAddon.AddonId](const UPlayerInventoryItem* InSource) { return InSource->IsEqual(id); }))
			{
				if (auto item = GetValidObject(*result))
				{
					if (auto entity = item->GetEntity<UItemModuleEntity>())
					{
						for (auto localInstalledAddon : GetInstalledAddons())
						{
							if (const auto localhEntity = localInstalledAddon->GetEntity<UItemModuleEntity>())
							{
								if (localhEntity->TargetSocket == entity->TargetSocket)
								{
									InstalledAddons.Remove(localInstalledAddon);
								}
							}
						}

						InstalledAddons.AddUnique(item);
					}
				}
			}
		}
	}
}

void UPlayerInventoryItem::SetItemEntity(UItemBaseEntity* entity)
{
	InstalledMaterial = 0;
	AvailableAddons.Empty();
	InstalledAddons.Empty();

	EntityInstance = entity;
	if (const auto e = GetEntity())
	{
		EntityModel = e->GetModelInfo();
	}
}

USkeletalMesh* UPlayerInventoryItem::GetPreviewMesh() const
{
	if (const auto e = GetEntity())
	{
		return e->GetSkeletalMesh();
	}
	return nullptr;
}

UItemBaseEntity* UPlayerInventoryItem::GetEntityBase() const
{
	return GetEntity();
}

float UPlayerInventoryItem::GetMass() const
{
	float CalculatedMass = .0f;

	if (const auto e = GetEntity())
	{
		CalculatedMass += e->GetMass();

		for (auto Addon : InstalledAddons)
		{
			if (Addon && Addon->IsValidLowLevel())
			{
				CalculatedMass += Addon->GetMass();
			}
		}
	}

	return CalculatedMass;
}

void UPlayerInventoryItem::OnRep_Instance()
{
	EntityInstance = UGameSingleton::Get()->FindItem(EntityModel);
}


void UPlayerInventoryItem::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UPlayerInventoryItem, EntityModel);
	DOREPLIFETIME(UPlayerInventoryItem, Count);
	DOREPLIFETIME(UPlayerInventoryItem, InstalledAddons);
	DOREPLIFETIME(UPlayerInventoryItem, AvailableAddons);
	DOREPLIFETIME(UPlayerInventoryItem, InstalledMaterial);
}