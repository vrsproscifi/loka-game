#include "LokaGame.h"
#include "PlayerDataEntity.h"
#include "BasicGameComponent.h"

UPlayerDataEntity::UPlayerDataEntity()
{
	SetIsReplicated(true);
}

UBasicGameComponent* UPlayerDataEntity::GetOwnerComponent() const
{
	return Cast<UBasicGameComponent>(GetOwner());
}

void UPlayerDataEntity::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UPlayerDataEntity, EntityId);
}