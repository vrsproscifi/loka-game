
#include "LokaGame.h"
#include "LokaGuid.h"

static UScriptStruct* StaticGetBaseStructureInternalEx(const TCHAR* Name)
{
	static auto* CoreUObjectPkg = FindObjectChecked<UPackage>(nullptr, TEXT("/Script/CoreUObject"));
	return FindObjectChecked<UScriptStruct>(CoreUObjectPkg, Name);
}

bool FLokaGuid::ExportTextItem(FString& ValueStr, FLokaGuid const& DefaultValue, UObject* Parent, int32 PortFlags, UObject* ExportRootScope) const
{
	if (0 != (PortFlags & EPropertyPortFlags::PPF_ExportCpp)) { return false; }
	ValueStr += ToString();
	return true;
}

bool FLokaGuid::ImportTextItem(const TCHAR*& Buffer, int32 PortFlags, UObject* Parent, FOutputDevice* ErrorText)
{
	if (FPlatformString::Strlen(Buffer) < 32)
	{
		return false;
	}

	if (FGuid::ParseExact(FString(Buffer).Left(68), EGuidFormats::HexValuesInBraces, Source))
	{
		Buffer += 68;
		return true;
	}
	else if (FGuid::ParseExact(FString(Buffer).Left(38), EGuidFormats::DigitsWithHyphensInBraces, Source) || FGuid::ParseExact(FString(Buffer).Left(38), EGuidFormats::DigitsWithHyphensInParentheses, Source))
	{
		Buffer += 38;
		return true;
	}
	else if (FGuid::ParseExact(FString(Buffer).Left(36), EGuidFormats::DigitsWithHyphens, Source))
	{
		Buffer += 36;
		return true;
	}
	else if (FGuid::ParseExact(FString(Buffer).Left(35), EGuidFormats::UniqueObjectGuid, Source))
	{
		Buffer += 35;
		return true;
	}
	else if (FGuid::ParseExact(FString(Buffer).Left(32), EGuidFormats::Digits, Source))
	{
		Buffer += 32;
		return true;
	}	

	return false;
}

FString FLokaGuid::ToString(const EGuidFormats InFormat) const
{
	return Source.ToString(InFormat);
}

FLokaGuid FLokaGuid::FromString(const FString& InGuidStr)
{
	FLokaGuid OutGuid;
	FGuid::Parse(InGuidStr, OutGuid.Source);
	return OutGuid;
}

UScriptStruct* TBaseStructure<FLokaGuid>::Get()
{
	static auto ScriptStruct = StaticGetBaseStructureInternalEx(TEXT("LokaGuid"));
	return ScriptStruct;
}