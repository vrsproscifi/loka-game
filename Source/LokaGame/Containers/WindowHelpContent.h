// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "WindowHelpContent.generated.h"

/**
 * 
 */
UCLASS()
class LOKAGAME_API UWindowHelpContent : public UDataAsset
{
	GENERATED_BODY()
public:

	UPROPERTY(EditDefaultsOnly, Category = Help)
	TArray<FText> Category;

	UPROPERTY(EditDefaultsOnly, Category = Help, meta = (MultiLine = true))
	TMap<int32, FText> Content;
	
	
};
