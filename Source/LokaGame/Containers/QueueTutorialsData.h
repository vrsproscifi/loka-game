// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "QueueTutorialsData.generated.h"

/**
 * 
 */
UCLASS()
class LOKAGAME_API UQueueTutorialsData : public UDataAsset
{
	GENERATED_BODY()
	
public:

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Queue)
	TArray<int32> Queue;
	
};
