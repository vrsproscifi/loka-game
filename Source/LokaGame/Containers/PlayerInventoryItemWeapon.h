// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Containers/PlayerInventoryItem.h"
#include "Weapon/ItemWeaponEntity.h"
#include "PlayerInventoryItemWeapon.generated.h"

/**
 * 
 */
UCLASS()
class LOKAGAME_API UPlayerInventoryItemWeapon : public UPlayerInventoryItem
{
	GENERATED_BODY()
	
public:
	
	virtual void SetItemEntity(UItemBaseEntity* InEntity) override;
	virtual void InitializeAddons(const FInvertoryItemWrapper& MyData, const TArray<UPlayerInventoryItem*>& InAddonsList) override;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Inventory)
	UItemWeaponEntity* GetEntityWeapon() const { return GetEntity<UItemWeaponEntity>();	}

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Inventory)
	FORCEINLINE FWeaponPropertyBase GetProperty() const { return WeaponProperty; }

	// Temp here
	static bool ApplyModifers(void* InTargetStruct, UScriptStruct* InTargetClass, const TMap<FString, float>& InModifers); 

protected:

	UPROPERTY(Replicated)			FWeaponPropertyBase		WeaponProperty;	
};
