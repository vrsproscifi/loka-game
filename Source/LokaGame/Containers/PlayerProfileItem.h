#pragma once

//=======================================================
//							[ Headers ]
#include "PlayerDataEntity.h"
#include "PlayerProfileItem.generated.h"

class UPlayerInventoryItem;
class UItemBaseEntity;

UCLASS()
class UPlayerProfileItem : public UPlayerDataEntity
{
	GENERATED_BODY()

public:
	UPlayerProfileItem();
	void SetItemEntity(UPlayerInventoryItem* entity);

	UPlayerInventoryItem* GetEntity();

	UPlayerInventoryItem* GetEntity() const;

	USkeletalMesh* GetMesh() 
	{
		if (const auto mesh = GetValidObject(Mesh))
		{
			return GetValidObject(mesh->SkeletalMesh);
		}
		return nullptr;
	}

	USkeletalMesh* GetMesh() const
	{
		if (const auto mesh = GetValidObject(Mesh))
		{
			return GetValidObject(mesh->SkeletalMesh);
		}
		return nullptr;
	}

protected:

	UFUNCTION()									void OnRep_Instance() const;

	//=========================================
	UPROPERTY()									USkeletalMeshComponent* Mesh; // WTF?
	UPROPERTY(ReplicatedUsing = OnRep_Instance)	UPlayerInventoryItem*	Entity;
};