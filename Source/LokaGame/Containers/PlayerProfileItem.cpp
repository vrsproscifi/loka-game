#include "LokaGame.h"
#include "PlayerProfileItem.h"
#include "PlayerInventoryItem.h"

UPlayerProfileItem::UPlayerProfileItem()
{
	Mesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMesh"));
}

void UPlayerProfileItem::SetItemEntity(UPlayerInventoryItem* entity)
{
	Entity = entity;
	OnRep_Instance();
}

UPlayerInventoryItem* UPlayerProfileItem::GetEntity()
{
	return GetValidObject(Entity);
}

UPlayerInventoryItem* UPlayerProfileItem::GetEntity() const
{
	return GetValidObject(Entity);
}

void UPlayerProfileItem::OnRep_Instance() const
{
	if (auto e = GetEntity())
	{
		Mesh->SetSkeletalMesh(e->GetPreviewMesh());
	}
}

void UPlayerProfileItem::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UPlayerProfileItem, Entity);	
}
