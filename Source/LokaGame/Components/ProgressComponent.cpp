#include "LokaGame.h"
#include "ProgressComponent.h"
#include "Models/PlayerEntityInfo.h"
#include "GameSingleton.h"
#include "Containers/PlayerFractionEntity.h"
#include "Containers/PlayerAchievementEntity.h"
#include "Components/RequestManager/RequestManager.h"
#include "SNotifyList.h"

#include "Item/ItemBaseEntity.h"
#include "RichTextHelpers.h"

#define LOCTEXT_NAMESPACE "Progress"

UProgressComponent::UProgressComponent()
	: CurrentFraction(nullptr)
{
	//=========================================================
	//					[ SwitchFraction ]
	SwitchFraction = CreateRequest(FServerHost::MasterClient, "Identity/OnSwitchFraction");
	SwitchFraction->BindObject<FLokaGuid>(200).AddUObject(this, &UProgressComponent::OnSwitchFractionSuccessfully_Server);
	SwitchFraction->OnFailedResponse.BindUObject(this, &UProgressComponent::OnSwitchFractionFailed);


	//=========================================================
	//					[ GetOrderNotifyRequest ]
	GetOrderNotifyRequest = CreateRequest(FServerHost::MasterClient, "Store/OnOrderDeliveryNotify", 15.0f, true, FTimerDelegate::CreateLambda([&]{
		if(GetOrderNotifyRequest.IsValid())
		{
			GetOrderNotifyRequest->GetRequest();
		}
	}));

	GetOrderNotifyRequest->BindArray<TArray<FOrderDeliveryNotifyModel>>(200).AddUObject(this, &UProgressComponent::OnGetOrderNotifySuccessfully);

	//=========================================================
	//					[ GetStoreNotifyRequest ]
	GetStoreNotifyRequest = CreateRequest(FServerHost::MasterClient, "Store/OnStoreDeliveryNotify", 15.0f, true, FTimerDelegate::CreateLambda([&] {
		if (GetStoreNotifyRequest.IsValid())
		{
			GetStoreNotifyRequest->GetRequest();
		}
	}));

	GetStoreNotifyRequest->BindArray<TArray<FStoreDeliveryNotifyModel>>(200).AddUObject(this, &UProgressComponent::OnGetStoreNotifySuccessfully);
}


void UProgressComponent::AddFraction(UPlayerFractionEntity* fraction)
{
	if(auto f = GetValidObject(fraction))
	{
		FractionList.Add(f);
		ConstFractionList.Add(f);

		FractionIdMap.Add(f->GetEntityId(), f);
		FractionModelMap.Add(f->GetModelId(), f);
		f->SetCurrentFractionReference(CurrentFraction);
		f->RegisterComponent();
	}
}

void UProgressComponent::OnRequestClientDataSuccessfully(const FPlayerEntityInfo& response)
{
	auto gs = CastChecked<UGameSingleton>(GEngine->GameSingleton);
	//===================================================
	if(GetMatchNotifyRequest.IsValid() == false)
	{
		auto url = FString::Printf(TEXT("Match/CheckMatchInformation?id=%s"), *response.PlayerId.ToString());
		GetMatchNotifyRequest = CreateRequest(FServerHost::ClusterServer, url, 15.0f, true, FTimerDelegate::CreateLambda([&] {
			if (GetMatchNotifyRequest.IsValid())
			{
				GetMatchNotifyRequest->GetRequest();
			}
		}));

		GetMatchNotifyRequest->BindArray<TArray<FMatchInformationPreviewModel>>(200).AddUObject(this, &UProgressComponent::OnGetMatchNotifySuccessfully);
		StartTimer(GetMatchNotifyRequest);
		StartTimer(GetOrderNotifyRequest);
	}


	//=================================================================
	//	Fraction
	for(auto f : response.FractionExperience)
	{
		//=========================================
		const auto instance = gs->FindFraction(f.Id);
		if (instance == nullptr)
		{
			continue;
		}

		//=========================================
		auto fraction = GetFraction(f.Id);
		if (fraction == nullptr)
		{
			fraction = NewObject<UPlayerFractionEntity>(this);
		}
		//
		fraction->SetProgress(f);
		fraction->SetFractionEntity(instance);
		fraction->SetEntityId(f.EntityId);
		AddFraction(fraction);

	}

	//=================================================================
	//	Achievement
	//for (auto f : response.Archievements)
	//{
	//	//========================
	//	auto instance = gs->FindAchievement(f.Id);
	//	if (instance == nullptr)
	//	{
	//		continue;
	//	}
	//
	//	//========================
	//	auto achievement = NewObject<UPlayerAchievementEntity>(this);
	//	Achievement.Add(achievement);
	//
	//	//========================
	//	achievement->RegisterComponent();
	//}

	//=================================================================
	//	SetCurrentFraction
	{
		const auto fraction = GetFraction(response.FractionTypeId);
		SetCurrentFraction(fraction);
	}

	//=================================================================
	//	Delegates
	//OnLoadFractionProgressComplete.ExecuteIfBound(ConstFractionList);
}


void UProgressComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UProgressComponent, CurrentFraction);
	DOREPLIFETIME(UProgressComponent, FractionList);
	DOREPLIFETIME(UProgressComponent, ConstFractionList);

	DOREPLIFETIME(UProgressComponent, ConstAchievement);
	DOREPLIFETIME(UProgressComponent, Achievement);

}

void UProgressComponent::CreateGameWidgets()
{
}


const UPlayerFractionEntity* UProgressComponent::GetFraction(const FGuid& fraction) const
{
	if (FractionIdMap.Contains(fraction))
	{
		return GetValidObject(FractionIdMap[fraction]);
	}
	return nullptr;
}

UPlayerFractionEntity* UProgressComponent::GetFraction(const FGuid& fraction)
{
	if (FractionIdMap.Contains(fraction))
	{
		return GetValidObject(FractionIdMap[fraction]);
	}
	return nullptr;
}

const UPlayerFractionEntity* UProgressComponent::GetFraction(const FractionTypeId& fraction) const
{
	if (FractionModelMap.Contains(fraction))
	{
		return GetValidObject(FractionModelMap[fraction]);
	}
	return nullptr;
}

UPlayerFractionEntity* UProgressComponent::GetFraction(const FractionTypeId& fraction)
{
	if (FractionModelMap.Contains(fraction))
	{
		return GetValidObject(FractionModelMap[fraction]);
	}
	return nullptr;
}

void UProgressComponent::SetCurrentFraction(const UPlayerFractionEntity* fraction)
{
	if (auto ff = GetValidObject(fraction))
	{
		CurrentFraction = ff;
		for(auto f : FractionList)
		{
			if(auto fr = GetValidObject(f))
			{
				fr->SetCurrentFractionReference(CurrentFraction);
			}
		}
	}
}

void UProgressComponent::OnSwitchFractionRequest_Implementation(const UPlayerFractionEntity* fraction)
{
	SwitchFraction->SendRequestObject(fraction->GetEntityId());
}

//=========================================================
//					[ SwitchFraction ]
void UProgressComponent::OnSwitchFractionSuccessfully_Client_Implementation(const FLokaGuid& fraction)
{
	if (auto f = GetFraction(fraction))
	{
		FNotifyInfo info;
		info.Title = FText::Format(NSLOCTEXT("UProgressComponent", "Notify.OnSwitchFraction.Title", "You joined the faction {0}"), f->GetFractionName());
		info.Content = FText::Format(NSLOCTEXT("UProgressComponent", "Notify.OnSwitchFraction.Content", "Your rank is {0} ({1} level), until the next rank you need {0} reputation"), f->GetFractionRank(), FText::AsNumber(f->GetProgress().CurrentRank), f->GetFractionReputation());
		SNotifyList::Get()->AddNotify(info);
	}
}

void UProgressComponent::OnSwitchFractionSuccessfully_Server(const FLokaGuid& fraction)
{
	OnSwitchFractionSuccessfully_Client_Implementation(fraction);
	if (auto f = GetFraction(fraction))
	{
		SetCurrentFraction(f);
	}

}



void UProgressComponent::OnSwitchFractionFailed(const FRequestExecuteError& error) const
{

}

void UProgressComponent::OnGetOrderNotifySuccessfully_Implementation(const TArray<struct FOrderDeliveryNotifyModel>& orders)
{
	auto gs = UGameSingleton::Get();
	for (auto order : orders)
	{
		FNotifyInfo info;

		info.Title = NSLOCTEXT("UProgressComponent", "Notify.OnGetOrderNotifySuccessfully.Title", "Your order has been delivered");
		
		if (auto item = gs->FindItem(order.ModelId, static_cast<CategoryTypeId::Type>(order.CategoryTypeId)))
		{
			info.Content = FText::Format(NSLOCTEXT("UProgressComponent", "Notify.OnGetOrderNotifySuccessfully", "{0} x {1}"), item->GetItemName(), FText::AsNumber(order.Amount));
		}
		else
		{
			info.Content = FText::Format(NSLOCTEXT("UProgressComponent", "Notify.OnGetOrderNotifySuccessfully.ItemNotFound", "ItemNotFound: ModelId: {0} | CategoryTypeId{1} x {2}"), FText::AsNumber(order.ModelId), FText::AsNumber(order.CategoryTypeId), FText::AsNumber(order.Amount));
		}

		AddNotify(info);
	}

}

void UProgressComponent::OnGetStoreNotifySuccessfully_Implementation(const TArray<FStoreDeliveryNotifyModel>& orders)
{
	auto gs = UGameSingleton::Get();
	for (auto order : orders)
	{
		FNotifyInfo info;

		info.Title = FText::Format(NSLOCTEXT("UProgressComponent", "Notify.OnGetOrderNotifySuccessfully.Title", "{0} bought for you"), FText::FromString(order.SernderName));

		if (auto item = gs->FindItem(order.ModelId, static_cast<CategoryTypeId::Type>(order.CategoryTypeId)))
		{
			info.Content = FText::Format(NSLOCTEXT("UProgressComponent", "Notify.OnGetStoreNotifySuccessfully", "{0} x {1}"), item->GetItemName(), FText::AsNumber(order.Amount));
		}
		else
		{
			info.Content = FText::Format(NSLOCTEXT("UProgressComponent", "Notify.OnGetStoreNotifySuccessfully.ItemNotFound", "ItemNotFound: ModelId: {0} | CategoryTypeId{1} x {2}"), FText::AsNumber(order.ModelId), FText::AsNumber(order.CategoryTypeId), FText::AsNumber(order.Amount));
		}

		AddNotify(info);
	}
}

void UProgressComponent::OnGetMatchNotifySuccessfully_Implementation(const TArray<FMatchInformationPreviewModel>& matches)
{
	for (const auto match : matches)
	{
		FNotifyInfo NotifyInfo;
		FFormatNamedArguments args;
		//======================================
		const auto gMode = match.GetGameMode().GetDisplayName();
		const auto gMap = match.GetGameMap().GetDisplayName();

		//======================================
		//const auto Victory = NSLOCTEXT("Notify", "Notify.Victory", "Victory");
		//const auto Defeat = NSLOCTEXT("Notify", "Notify.Defeat", "Defeat");
		//const auto WinState = NSLOCTEXT("Notify", "Notify.Match", "Match");

		FRichHelpers::FResourceHelper Resource;		

		args.Add("Mode", FRichHelpers::TextHelper_NotifyValue.SetValue(gMode).ToText());
		args.Add("Map", FRichHelpers::TextHelper_NotifyValue.SetValue(gMap).ToText());
		args.Add("Money", Resource.SetType(EResourceTextBoxType::Money).SetValue(match.Money).ToText());
		args.Add("Donate", Resource.SetType(EResourceTextBoxType::Donate).SetValue(match.Donate).ToText());
		args.Add("Coin", Resource.SetType(EResourceTextBoxType::Coin).SetValue(match.Bitcoin).ToText());
		args.Add("Exp", FRichHelpers::TextHelper_NotifyValue.SetValue(FText::AsNumber(match.Experience)).ToText());
		args.Add("Rep", FRichHelpers::TextHelper_NotifyValue.SetValue(FText::AsNumber(match.Reputation)).ToText());
		args.Add("Kills", FRichHelpers::TextHelper_NotifyValue.SetValue(FText::AsNumber(match.Kills)).ToText());
		args.Add("Deaths", FRichHelpers::TextHelper_NotifyValue.SetValue(FText::AsNumber(match.Deads)).ToText());

		NotifyInfo.Title = NSLOCTEXT("Squad", "Notify.MatchMaking.Title", "Match information");
		NotifyInfo.Content = FText::Format(LOCTEXT("Notify.LastMatchInfo.Desc", "Your personal fight result:\nGame mode,map: {Mode}, {Map}\nCurrency's: {Money} {Donate} {Coin}\nExperience: {Exp}\nReputation: {Rep}\nKills: {Kills}, Deaths: {Deaths}"), args);
		
		AddNotify(NotifyInfo);
	}
}

#undef LOCTEXT_NAMESPACE