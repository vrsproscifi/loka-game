#include "LokaGame.h"
#include "InventoryComponent.h"

#include "GameSingleton.h"
#include "PlayerInventoryItem.h"
#include "Item/ItemBaseEntity.h"
#include "Models/UnlockItemContainer.h"
#include "RequestManager/RequestManager.h"
#include "ConstructionGameState.h"

UInventoryComponent::UInventoryComponent()
{
	RequestLootData = CreateRequest(FServerHost::MasterClient, "Identity/RequestLootData");
	RequestLootData->BindArray<TArray<FInvertoryItemWrapper>>(200).AddUObject(this, &UInventoryComponent::OnLootData);
	//RequestLootData->OnFailedResponse.BindLambda([this](const FRequestExecuteError& error) {
	//	OnRequestLootDataFailed.ExecuteIfBound(error);
	//});
}

bool UInventoryComponent::SendRequestLootData_Validate() { return true; }
void UInventoryComponent::SendRequestLootData_Implementation()
{
	RequestLootData->GetRequest();
}

void UInventoryComponent::OnLootData(const TArray<FInvertoryItemWrapper>& InData)
{
	if (GetUserOwner() && GetUserOwner()->IsAllowUserInventory())
	{
		OnLoadPlayerInventorySuccessfully(InData);
	}
}

void UInventoryComponent::CreateGameWidgets()
{
}

void UInventoryComponent::OnUnlockItemSuccessfully(const FUnlockItemResponseContainer& response)
{
	auto MyGameState = GetWorldGameState<AConstructionGameState>(GetWorld());
	if (MyGameState && MyGameState != GetOwner())
	{
		MyGameState->GetInventoryComponent()->OnUnlockItemSuccessfully(response);
	}

	CreateAndInsertItem(response);
	OnRep_Items();
}

void UInventoryComponent::OnSellItemHandle(const FGuid& InItemId)
{
	auto MyGameState = GetWorldGameState<AConstructionGameState>(GetWorld());
	if (MyGameState && MyGameState != GetOwner())
	{
		MyGameState->GetInventoryComponent()->SendRequestLootData();
	}
	else
	{
		SendRequestLootData();
	}
}

UPlayerInventoryItem* UInventoryComponent::CreateAndInsertItem(const FInvertoryItemWrapper& i)
{
	//========================
	auto instance = UGameSingleton::Get()->FindItem(i.ModelId, i.CategoryTypeId);
	if (instance == nullptr)
	{
		return nullptr;
	}

	if (auto FoundItem = FindItem(i.EntityId))
	{
		FoundItem->SetCount(i.Count);
		return FoundItem;
	}
	else
	{
		if (i.CategoryTypeId == ECategoryTypeId::Building)
		{
			if (auto FoundBuildingItem = FindItem(i.ModelId, i.CategoryTypeId))
			{
				FoundBuildingItem->SetEntityId(i.EntityId);
				FoundBuildingItem->SetCount(i.Count);
				return FoundBuildingItem;
			}
		}
		else
		{
			auto item = NewObject<UPlayerInventoryItem>(this, instance->GetInventoryClass());
			if (i.EntityId.IsValid())
			{
				item->SetEntityId(i.EntityId);
				item->InitializeInstanceDynamic(instance);
				item->SetCount(i.Count);
				Items.AddUnique(item);
				return item;
			}
		}
	}

	return nullptr;
}

void UInventoryComponent::OnLoadPlayerInventorySuccessfully(const TArray<FInvertoryItemWrapper>& response)
{
	//=============================================================================
	// Добавляем так потомучто по другому это полное извращение хотя и это тоже, P.S. Спасибо SeNTike
	if (Cast<AConstructionGameState>(GetOwner())) // Проверям т.к. в бою другой класс и там этот хлам не к чему
	{
		auto BuildingList = UGameSingleton::Get()->FindList(ECategoryTypeId::Building);
		for (auto Building : BuildingList)
		{
			if (FindItem(Building->GetModelId(), Building->GetCategory()) == nullptr)
			{
				auto item = NewObject<UPlayerInventoryItem>(this, UPlayerInventoryItem::StaticClass());
				item->InitializeInstanceDynamic(Building);
				item->SetCount(0);
				Items.AddUnique(item);
			}
		}
	}

	//=============================================================================
	for (auto i : response)
	{
		CreateAndInsertItem(i);
	}

	Items.Sort([](const UPlayerInventoryItem& InA, const UPlayerInventoryItem& InB)
	{
		if (InA.GetEntity() && InB.GetEntity())
		{
			return InA.GetEntity()->GetCategory() > InB.GetEntity()->GetCategory();
		}

		return false;
	});

	OnRep_Items();
	OnPostLoadHandle(response);	
}

void UInventoryComponent::OnLoadPlayerInventoryFailed(const FRequestExecuteError& error) const
{
	
}

void UInventoryComponent::OnPostLoadHandle(const TArray<FInvertoryItemWrapper>& InLoadData)
{
	auto TargetList = FindList(ECategoryTypeId::Addon);
	TargetList.Append(FindList(ECategoryTypeId::Material));

	for (auto ItemData : InLoadData)
	{
		if (ItemData.InstalledAddons.Num() || ItemData.AvalibleAddons.Num())
		{
			if (auto Target = FindItem(ItemData.EntityId))
			{
				Target->InitializeAddons(ItemData, TargetList);
			}
		}
	}
}

void UInventoryComponent::OnAuthorizationComplete()
{
	if (HasAuthority())
	{
		SendRequestLootData();
	}
}

UPlayerInventoryItem* UInventoryComponent::FindItem(const int32& InModelId, const CategoryTypeId::Type& InCategory) const
{
	for (auto item : Items)
	{
		if (const auto i = GetValidObject(item))
		{
			if (i && i->GetEntityModel().ModelId == InModelId && i->GetEntityModel().CategoryTypeId == InCategory)
			{
				return i;
			}
		}
	}

	return nullptr;
}

UPlayerInventoryItem* UInventoryComponent::FindItem(const FItemModelInfo& InModelInfo) const
{
	return FindItem(InModelInfo.ModelId, InModelInfo.CategoryTypeId);
}

UPlayerInventoryItem* UInventoryComponent::FindItem(const FString& InIndex) const
{
	FGuid id;
	if(FGuid::Parse(InIndex, id))
	{
		return FindItem(id);
	}
	return nullptr;
}

UPlayerInventoryItem* UInventoryComponent::FindItem(const FGuid& InIndex) const
{
	if (ItemMap.Contains(InIndex))
	{
		return GetValidObject(ItemMap.FindChecked(InIndex));
	}

	return nullptr;
}

TArray<UPlayerInventoryItem*> UInventoryComponent::FindList(const CategoryTypeId::Type& InCategory) const
{
	return Items.FilterByPredicate([c = InCategory](const UPlayerInventoryItem* item) { return (item && item->IsValidLowLevel() && item->GetEntityModel().CategoryTypeId == c); });
}

void UInventoryComponent::OnRep_Items()
{
	ItemMap.Empty();

	for (auto item : Items)
	{
		if (item && item->IsValidLowLevel())
		{
			ItemMap.Add(item->GetEntityId(), item);
		}
	}

	OnInventoryUpdate.Broadcast(Items);
}

void UInventoryComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UInventoryComponent, Items);
}
