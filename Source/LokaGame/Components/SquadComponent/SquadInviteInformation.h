#pragma once


#include "Shared/SessionMatchOptions.h"
#include "SquadInviteInformation.generated.h"

UENUM(Blueprintable)
enum class ESquadInviteState : uint8
{
	None,
	Submitted = 1,
	Received = 2,

	Approved = 4,
	Rejected = 8,

	Notified = 16,
};
ENUM_CLASS_FLAGS(ESquadInviteState);

UENUM(Blueprintable)
enum class ESquadInviteDirection : uint8
{
	Invite,
	Request
};


USTRUCT(Blueprintable)
struct FSquadInviteInformation
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadOnly)	FLokaGuid				InviteId;
	UPROPERTY(BlueprintReadOnly)	FLokaGuid				PlayerId;
	UPROPERTY(BlueprintReadOnly)	FString					PlayerName;
	UPROPERTY()						int64					SubmittedDate;
	UPROPERTY(BlueprintReadOnly)	ESquadInviteState		State;
	UPROPERTY(BlueprintReadOnly)	FSessionMatchOptions	Options;
	UPROPERTY(BlueprintReadOnly)	ESquadInviteDirection	Direction;

	FSquadInviteInformation(){}
	FSquadInviteInformation(const FLokaGuid& player, const FSessionMatchOptions& options, const ESquadInviteDirection& direction = ESquadInviteDirection::Invite)
		: PlayerId(player)
		, Options(options)
		, State(ESquadInviteState::Submitted)
		, Direction(direction)
	{

	}

	FSquadInviteInformation(const FSquadInviteInformation& information, const bool& response)
		: InviteId(information.InviteId)
		, State(response ? ESquadInviteState::Approved : ESquadInviteState::Rejected)
	{

	}

};