#pragma once

#include "Components/Shared/PlayerOnlineStatus.h"
#include "SqaudMemberInformation.generated.h"


USTRUCT(BlueprintType)
struct FQueueMemberContainer
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadOnly)						FLokaGuid								PlayerId;
	UPROPERTY(BlueprintReadOnly)						FString									Name;
	UPROPERTY(BlueprintReadOnly, meta = (Bitmask))		TEnumAsByte<PlayerFriendStatus::Type>	Status;

	UPROPERTY()											APlayerState*							PlayerState; // Local state
};