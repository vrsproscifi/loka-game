#pragma once
#include "CheckExistAccount.generated.h"

USTRUCT()
struct FCheckExistAccountRequest
{
	GENERATED_USTRUCT_BODY()

	//==========================
	//			[ Property ]
	UPROPERTY()	FString Login;

	//==========================
	//			[ Constructor ]

	FCheckExistAccountRequest()
	{
	}

	FCheckExistAccountRequest(const FString& login)
		: Login(login)
	{

	}

};

USTRUCT()
struct FCheckExistAccountResponse
{
	GENERATED_USTRUCT_BODY()


};