#pragma once

#include "BasicGameComponent.h"
#include "Models/LobbyClientPreSetData.h"
#include "PlayerPreSetId.h"
#include "HangarController/Models/PreSetEquip.h"
#include "ProfileComponent.generated.h"

class FOperationRequest;
class APlayerProfileActor;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnProfileData, const TArray<FClientPreSetData>&, InResponse);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnProfileDataUpdated);

UCLASS(Transient)
class UProfileComponent : public UBasicGameComponent
{
	GENERATED_BODY()
public:
	UProfileComponent();

	UPROPERTY(BlueprintAssignable, Category = Profiles)
	FOnProfileData OnProfileDataEvent;

	UPROPERTY(BlueprintAssignable, Category = Profiles)
	FOnProfileDataUpdated OnProfileDataUpdated;

	//============================================
	UFUNCTION(Reliable, Server, WithValidation) void SendRequestProfilesData();
	UFUNCTION(Reliable, Server, WithValidation)	void SendRequestItemEquip(const FPreSetEquipRequest& InData);
	UFUNCTION(Reliable, Server, WithValidation)	void SendRequestItemUnEquip(const FGuid& InProfileId, const uint32& InSlot);

	//============================================
	//
	TSharedPtr<FOperationRequest> AddonEquip;

	TSharedPtr<FOperationRequest> RequestProfilesData;
	TSharedPtr<FOperationRequest> RequestItemEquip;
	TSharedPtr<FOperationRequest> RequestItemUnEquip;
	
	TSharedPtr<FOperationRequest> ItemModifyRequest;
	TSharedPtr<FOperationRequest> ItemModifyConfirm;

	UFUNCTION()
	void OnProfilesUpdateHandle(const TArray<FClientPreSetData>& InData);

	void CreateGameWidgets() override;

	UPROPERTY()	APlayerProfileActor* Profiles[5];

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Profile)
	TArray<FClientPreSetData> GetProfilesData() const { return ProfilesData; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Profile)
	FClientPreSetData GetProfileData(const TEnumAsByte<EPlayerPreSetId::Type> InTargetProfile) const;
	const FClientPreSetData* GetProfileDataPtr(const TEnumAsByte<EPlayerPreSetId::Type> InTargetProfile) const;

protected:

	UFUNCTION() void OnProfiles(const TArray<FClientPreSetData>& InData);

	UFUNCTION() void OnItemEquip(const struct FPreSetEquipResponse& InResponse);
	UFUNCTION() void OnItemEquip_Failed(const FRequestExecuteError& error);
	UFUNCTION() void OnItemEquip_ProfileNotFound();

	UFUNCTION() void OnItemUnEquip(const struct FPreSetEquipResponse& InResponse);
	UFUNCTION() void OnItemUnEquip_Failed(const FRequestExecuteError& error);
	UFUNCTION() void OnItemUnEquip_ProfileNotFound();

	UPROPERTY(Replicated, BlueprintReadOnly, Category = Profile)
	TArray<FClientPreSetData> ProfilesData;
};