#include "LokaGame.h"
#include "ProfileComponent.h"
#include "OperationContainer.h"
#include "RequestManager/RequestManager.h"
#include "HangarController/Models/PreSetEquip.h"
#include "BasePlayerState.h"

UProfileComponent::UProfileComponent()
{
	//==========================================
	RequestProfilesData = CreateRequest(FServerHost::MasterClient, "Identity/RequestPreSetData");
	RequestProfilesData->BindArray<TArray<FClientPreSetData>>(200).AddUObject(this, &UProfileComponent::OnProfiles);

	//==========================================
	RequestItemEquip = CreateRequest(FServerHost::MasterClient, "Hangar/ItemEquip");
	RequestItemEquip->BindObject<FPreSetEquipResponse>(200).AddUObject(this, &UProfileComponent::OnItemEquip);
	RequestItemEquip->OnFailedResponse.BindUObject(this, &UProfileComponent::OnItemEquip_Failed);
	RequestItemEquip->Bind(400).AddUObject(this, &UProfileComponent::OnItemEquip_ProfileNotFound);

	//==========================================
	RequestItemUnEquip = CreateRequest(FServerHost::MasterClient, "Hangar/ItemEquip");
	RequestItemUnEquip->BindObject<FPreSetEquipResponse>(200).AddUObject(this, &UProfileComponent::OnItemUnEquip);
	RequestItemUnEquip->OnFailedResponse.BindUObject(this, &UProfileComponent::OnItemUnEquip_Failed);
	RequestItemUnEquip->Bind(400).AddUObject(this, &UProfileComponent::OnItemUnEquip_ProfileNotFound);
}

bool UProfileComponent::SendRequestProfilesData_Validate() { return true; }
void UProfileComponent::SendRequestProfilesData_Implementation()
{
	RequestProfilesData->GetRequest();
}

bool UProfileComponent::SendRequestItemEquip_Validate(const FPreSetEquipRequest& InData) { return true; }
void UProfileComponent::SendRequestItemEquip_Implementation(const FPreSetEquipRequest& InData)
{
	RequestItemEquip->SendRequestObject(InData);
}

bool UProfileComponent::SendRequestItemUnEquip_Validate(const FGuid& InProfileId, const uint32& InSlot) { return true; }
void UProfileComponent::SendRequestItemUnEquip_Implementation(const FGuid& InProfileId, const uint32& InSlot)
{
	FPreSetEquipRequest r;
	r.IsEquip = false;
	r.SetSlotId = InSlot;
	r.PreSetId = InProfileId;
	RequestItemUnEquip->SendRequestObject(r);
}

void UProfileComponent::OnProfiles(const TArray<FClientPreSetData>& InData)
{
	UE_LOG(LogEntityPlayer, Display, TEXT("UIdentityComponent::OnRequestPreSetDataSuccessfully[%s][%d]"), *GetIdentityToken().Get().ToString(), static_cast<int32>(GetNetMode()));

	if (GetOwnerState() && GetOwnerState()->bIsAllowUserInventory)
	{
		OnProfileDataEvent.Broadcast(InData);
		OnProfilesUpdateHandle(InData);
	}
}

void UProfileComponent::OnItemEquip(const FPreSetEquipResponse& InResponse)
{
	SendRequestProfilesData();
}

void UProfileComponent::OnItemUnEquip(const FPreSetEquipResponse& InResponse)
{
	SendRequestProfilesData();
}

void UProfileComponent::OnItemEquip_Failed(const FRequestExecuteError& error)
{
	SendRequestProfilesData();
}

void UProfileComponent::OnItemEquip_ProfileNotFound()
{
	SendRequestProfilesData();
}

void UProfileComponent::OnItemUnEquip_ProfileNotFound()
{
	SendRequestProfilesData();
}

void UProfileComponent::OnItemUnEquip_Failed(const FRequestExecuteError& error)
{
	SendRequestProfilesData();
}

void UProfileComponent::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UProfileComponent, ProfilesData);
}

void UProfileComponent::CreateGameWidgets()
{
}

void UProfileComponent::OnProfilesUpdateHandle(const TArray<FClientPreSetData>& InData)
{
	ProfilesData = InData;
	for (auto &MyProfile : ProfilesData)
	{
		MyProfile.Items.Sort([](const FPlayerInventoryItemContainer& A, const FPlayerInventoryItemContainer& B) { return A.SlotId < B.SlotId; });
	}

	OnProfileDataUpdated.Broadcast();
}

FClientPreSetData UProfileComponent::GetProfileData(const TEnumAsByte<EPlayerPreSetId::Type> InTargetProfile) const
{
	FClientPreSetData Data;
	if (ProfilesData.Num() && ProfilesData.IsValidIndex(InTargetProfile))
	{
		Data = ProfilesData[InTargetProfile];
	}

	return Data;
}

const FClientPreSetData* UProfileComponent::GetProfileDataPtr(const TEnumAsByte<EPlayerPreSetId::Type> InTargetProfile) const
{
	if (ProfilesData.Num() && ProfilesData.IsValidIndex(InTargetProfile))
	{
		return &ProfilesData[InTargetProfile];
	}

	return nullptr;
}
