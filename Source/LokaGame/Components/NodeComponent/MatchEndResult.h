#pragma once

#include "MatchMember.h"
#include "MatchEndResult.generated.h"

USTRUCT()
struct FMatchEndResult
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	FString WinnerTeamId;

	UPROPERTY()
	TArray<FMatchMember> Members;
};