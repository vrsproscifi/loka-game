#pragma once
#include "Achievement/ItemAchievementTypeId.h"
#include "MatchMemberAchievement.generated.h"

USTRUCT()
struct FMatchMemberAchievement
{
	GENERATED_USTRUCT_BODY()
	
	UPROPERTY()
	TEnumAsByte<ItemAchievementTypeId::Type> AchievementTypeId;

	UPROPERTY()
	int32 Count;
};