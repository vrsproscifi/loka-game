#pragma once

#include "ConfirmMemberContainer.generated.h"

UENUM()
namespace MemberJoinResponseStatus
{
	enum Type
	{
		None,
		Allow,
		DisAllow,
		End
	};
}


USTRUCT()
struct FConfirmMemberContainer
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
		FLokaGuid MemberId;

	UPROPERTY()
		TEnumAsByte<MemberJoinResponseStatus::Type> Status;
};


USTRUCT()
struct FConfirmMemberRequest
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	TArray<FConfirmMemberContainer> Members;

	FConfirmMemberRequest() : Members() {}
	FConfirmMemberRequest(const TArray<FConfirmMemberContainer>& In) : Members(In) {}
};
