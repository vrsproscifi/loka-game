#include "LokaGame.h"
#include "SquadComponent.h"
#include "RequestManager/RequestManager.h"
#include <OnlineSubsystemTypes.h>
#include "BasePlayerController.h"
#include "ConstructionGameMode.h"
#include "SMessageBox.h"
#include "RichTextHelpers.h"
#include "Weapon/WeaponContainer.h"
#include "SNotifyList.h"
#include "NodeComponent/NodeSessionMatch.h"
#include "StringTableRegistry.h"

DEFINE_LOG_CATEGORY(LogSquadComponent);

#define LOCTEXT_NAMESPACE "Squad"

#define OnClientAdd_PlayerNotFound				702
#define OnClientAdd_InviteYourself				700
#define OnClientAdd_YouAreNotLeader				706
#define OnClientAdd_InviteTimeoutSent			708
#define OnClientAdd_YouAreNotEnouthMoney		709

#define OnClientAdd_PlayerInSquad				703
#define OnClientAdd_PlayerNotOnline				705
#define OnClientAdd_PlayerNotEnouthMoney		704
#define OnClientAdd_InviteAlreadySent			701


#define OnClientRespond_InviteNotFound			700
#define OnClientRespond_InviteAreRecived		701
#define OnClientRespond_YouAreInSquad			706
#define OnClientRespond_SenderInSquad			703
#define OnClientRespond_NotEnouthMoney			702

#define OnClientRespond_SenderNotEnouthMoney	704

USquadComponent::USquadComponent()
{	
	//=========================================================
	//					[ GetSqaudInformation ]
	RequestSquadInformation = CreateRequest(FServerHost::MasterClient, "Squad/GetSqaudInformation", 2.0f, true, FTimerDelegate::CreateUObject(this, &USquadComponent::SendRequestSquadInformation));
	RequestSquadInformation->BindObject<FSqaudInformation>(200).AddUObject(this, &USquadComponent::OnSquadInformation);

	//=========================================================
	//					[ JoinToSearchQueue ]
	RequestStartSearch = CreateRequest(FServerHost::MasterClient, "Squad/JoinToSearchQueue");
	RequestStartSearch->Bind(200).AddUObject(this, &USquadComponent::OnStartSearch);
	RequestStartSearch->Bind(900).AddUObject(this, &USquadComponent::OnRequestStartSearch_NotHavePrimaryWeapon);
	RequestStartSearch->Bind(901).AddUObject(this, &USquadComponent::OnRequestStartSearch_NotHaveEnabledProfiles);
	RequestStartSearch->Bind(903).AddUObject(this, &USquadComponent::OnRequestStartSearch_MembersNotReady);
	RequestStartSearch->Bind(906).AddUObject(this, &USquadComponent::OnRequestStartSearch_ErrorOnDataBase);
	RequestStartSearch->OnFailedResponse.BindUObject(this, &USquadComponent::OnRequestStartSearch_Failed);

	//=========================================================
	//					[ LeaveFromSearchQueue ]
	RequestStopSearch = CreateRequest(FServerHost::MasterClient, "Squad/LeaveFromSearchQueue");
	RequestStopSearch->Bind(200).AddUObject(this, &USquadComponent::OnStopSearch);

	//=========================================================
	//					[ GetSquadInvites ]
	RequestInvites = CreateRequest(FServerHost::MasterClient, "Squad/GetSquadInvites", 5.0f, true, FTimerDelegate::CreateUObject(this, &USquadComponent::SendRequestInvites));
	RequestInvites->BindArray<TArray<FSquadInviteInformation>>(200).AddUObject(this, &USquadComponent::OnInvites);

	//=========================================================
	//					[ SquadInvite ]
	RequestAdd = CreateRequest(FServerHost::MasterClient, "Squad/SendSquadInvite");
	RequestAdd->BindObject<FSquadInviteInformation>(200).AddUObject(this, &USquadComponent::OnAdd);

	RequestAdd->Bind(702).AddUObject(this, &USquadComponent::OnAddErrorCode, OnClientAdd_PlayerNotFound);
	RequestAdd->Bind(700).AddUObject(this, &USquadComponent::OnAddErrorCode, OnClientAdd_InviteYourself);
	RequestAdd->Bind(706).AddUObject(this, &USquadComponent::OnAddErrorCode, OnClientAdd_YouAreNotLeader);
	RequestAdd->Bind(708).AddUObject(this, &USquadComponent::OnAddErrorCode, OnClientAdd_InviteTimeoutSent);
	RequestAdd->Bind(709).AddUObject(this, &USquadComponent::OnAddErrorCode, OnClientAdd_YouAreNotEnouthMoney);

	RequestAdd->BindObject<FString>(703).AddUObject(this, &USquadComponent::OnAddErrorCodeWithString, OnClientAdd_PlayerInSquad);
	RequestAdd->BindObject<FString>(705).AddUObject(this, &USquadComponent::OnAddErrorCodeWithString, OnClientAdd_PlayerNotOnline);
	RequestAdd->BindObject<FString>(704).AddUObject(this, &USquadComponent::OnAddErrorCodeWithString, OnClientAdd_PlayerNotEnouthMoney);
	RequestAdd->BindObject<FString>(701).AddUObject(this, &USquadComponent::OnAddErrorCodeWithString, OnClientAdd_InviteAlreadySent);


	//=========================================================
	//					[ RespondSqaudInvite ]
	RequestRespondInvite = CreateRequest(FServerHost::MasterClient, "Squad/RespondSqaudInvite");
	RequestRespondInvite->BindObject<FSquadInviteInformation>(200).AddUObject(this, &USquadComponent::OnRespondInvite);
	
	RequestRespondInvite->Bind(700).AddUObject(this, &USquadComponent::OnRespondInviteErrorCode, OnClientRespond_InviteNotFound);
	RequestRespondInvite->Bind(701).AddUObject(this, &USquadComponent::OnRespondInviteErrorCode, OnClientRespond_InviteAreRecived);
	RequestRespondInvite->Bind(706).AddUObject(this, &USquadComponent::OnRespondInviteErrorCode, OnClientRespond_YouAreInSquad);
	RequestRespondInvite->Bind(703).AddUObject(this, &USquadComponent::OnRespondInviteErrorCode, OnClientRespond_SenderInSquad);
	RequestRespondInvite->Bind(702).AddUObject(this, &USquadComponent::OnRespondInviteErrorCode, OnClientRespond_NotEnouthMoney);

	RequestRespondInvite->BindObject<FString>(704).AddUObject(this, &USquadComponent::OnRespondInviteErrorCodeWithString, OnClientRespond_SenderNotEnouthMoney);


	//=========================================================
	//					[ RemoveFromSquad ]
	RequestRemove = CreateRequest(FServerHost::MasterClient, "Squad/RemoveFromSquad");
	RequestRemove->BindObject<FString>(200).AddUObject(this, &USquadComponent::OnRemove);
}

void USquadComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(USquadComponent, SqaudInformation);
	DOREPLIFETIME_CONDITION(USquadComponent, LastSqaudInformation, COND_OwnerOnly);
}

void USquadComponent::OnAuthorizationComplete()
{
	StartTimer(RequestSquadInformation);
	StartTimer(RequestInvites);
}

void USquadComponent::SendRequestToggleSearch_Implementation(const FSessionMatchOptions& InOptions)
{
	//=======================================================
	if (SqaudInformation.IsInConstructionMode())
	{
		SqaudInformation.Status = ESearchSessionStatus::None;
	}

	//=======================================================
	if ((SqaudInformation.Status == ESearchSessionStatus::None || SqaudInformation.Status == ESearchSessionStatus::SearchPrepare) && (SqaudInformation.IsReady == false || SqaudInformation.IsLeader))
	{
		SqaudInformation.Status = ESearchSessionStatus::SearchStarting;
		RequestStartSearch->SendRequestObject(InOptions);
	}
	else if (SqaudInformation.IsAllowCancelSearch())
	{
		SqaudInformation.Status = ESearchSessionStatus::SearchStopping;
		RequestStopSearch->GetRequest();
	}

	UE_LOG(LogSquadComponent, Log, TEXT("OldStatus: %d, NewStatus: %d"), static_cast<int32>(LastSqaudInformation.Status), static_cast<int32>(SqaudInformation.Status));
}

void USquadComponent::SendRequestSquadInformation_Implementation()
{
	RequestSquadInformation->GetRequest();
}

void USquadComponent::SendRequestInvites_Implementation()
{
	RequestInvites->GetRequest();
}

void USquadComponent::SendRequestAdd_Implementation(const FGuid& InPlayerId, const FSessionMatchOptions& InOptions, const ESquadInviteDirection& InDirection)
{
	UE_LOG(LogSquadComponent, Display, TEXT("SendRequestAdd: %s | %s"), *InPlayerId.ToString(), *InOptions.GetGameMode().GetName());
	RequestAdd->SendRequestObject(FSquadInviteInformation(InPlayerId, InOptions, InDirection));
}

void USquadComponent::SendRequestRespondInvite_Implementation(const FSquadInviteInformation& InInformation, const bool InAproved)
{
	UE_LOG(LogSquadComponent, Display, TEXT("SendRequestRespondInvite: %s | %d"), *InInformation.InviteId.ToString(), InAproved);
	RequestRespondInvite->SendRequestObject(FSquadInviteInformation(InInformation, InAproved));
}

void USquadComponent::SendRequestRemove_Implementation(const FLokaGuid& InMemberId)
{
	RequestRemove->SendRequestObject(InMemberId);
}

void USquadComponent::OnSquadInformation(const FSqaudInformation& InInformation)
{
	if (SqaudInformation != InInformation)
	{
		LastSqaudInformation = SqaudInformation;
		SqaudInformation = InInformation;
		OnRep_SqaudInformation();

		if (SqaudInformation.IsAllowJoinMatch() && IsAllowServerQueries() && IsAllowTravel())
		{
			if (auto MyController = GetOwnerController())
			{
				MyController->PlayerTravel(SqaudInformation.Host, SqaudInformation.Token);
			}
		}
	}
}

bool USquadComponent::IsAllowTravel() const
{
	UE_LOG(LogInit, Display, TEXT("USquadComponent::IsAllowTravel | ServerId[%s] | TokenId[%s] | Alllow: %d"), *SqaudInformation.ServerId.ToString(), *FNodeSessionMatch::TokenId.ToString(), SqaudInformation.ServerId.Source != FNodeSessionMatch::TokenId);
	return SqaudInformation.ServerId.Source != FNodeSessionMatch::TokenId;
}

void USquadComponent::OnStartSearch()
{
	OnClientStartSearch();
}

void USquadComponent::OnStopSearch()
{
	OnClientStopSearch();
}

void USquadComponent::OnInvites(const TArray<FSquadInviteInformation>& InInvites)
{
	UE_LOG(LogSquadComponent, Display, TEXT("USquadComponent::OnInvites: %d"), InInvites.Num());
	OnClientInvites(InInvites);
}

void USquadComponent::OnAdd(const FSquadInviteInformation& InInformation)
{
	UE_LOG(LogSquadComponent, Display, TEXT("USquadComponent::OnAdd: %d | %s[%s]"), *InInformation.InviteId.ToString(), *InInformation.PlayerName, *InInformation.PlayerId.ToString());
	OnClientAdd(InInformation);
}

void USquadComponent::OnAddErrorCode(const int32 InCode)
{
	OnClientAddErrorCode(InCode);
}

void USquadComponent::OnAddErrorCodeWithString(const FString& InPlayerName, const int32 InCode)
{
	OnClientAddErrorCodeWithString(InPlayerName, InCode);
}

void USquadComponent::OnRespondInvite(const FSquadInviteInformation& InInformation)
{
	UE_LOG(LogSquadComponent, Display, TEXT("USquadComponent::OnRespondInvite: %d | %s[%s]"), *InInformation.InviteId.ToString(), *InInformation.PlayerName, *InInformation.PlayerId.ToString());
	OnClientRespondInvite(InInformation);
}

void USquadComponent::OnRespondInviteErrorCode(const int32 InCode)
{
	OnClientRespondInviteErrorCode(InCode);
}

void USquadComponent::OnRespondInviteErrorCodeWithString(const FString& InPlayerName, const int32 InCode)
{
	OnClientRespondInviteErrorCodeWithString(InPlayerName, InCode);
}

void USquadComponent::OnRemove(const FString& InPlayerName)
{
	OnClientRemove(InPlayerName);
}

void USquadComponent::OnClientStartSearch_Implementation()
{
	FNotifyInfo NotifyInfo;
	NotifyInfo.Title = LOCTEXT("Notify.MatchMaking.Title", "Match information");

	if (SqaudInformation.IsLeader)
	{
		NotifyInfo.Content = LOCTEXT("Notify.OnClientStartSearch.Content", "The search of fight is started");
	}
	else
	{
		NotifyInfo.Content = LOCTEXT("Notify.OnClientStartSearch_Member.Content", "You have activated the ready for battle mode. Expect as squad leader will start searching for a fight!");
	}
	AddNotify(NotifyInfo);
}

void USquadComponent::OnRequestStartSearch_Failed_Implementation(const FRequestExecuteError &error)
{
	FNotifyInfo NotifyInfo;
	NotifyInfo.Title = LOCTEXT("Notify.MatchMaking.Title", "Match information");
	NotifyInfo.Content = LOCTEXT("Notify.OnRequestStartSearch.Failed.Content", "Could not start the search game");
	UE_LOG(LogSquadComponent, Display, TEXT("USquadComponent::OnRequestStartSearch_Failed_Implementation: %s"), *error.ToString());
	AddNotify(NotifyInfo);
}


void USquadComponent::OnRequestStartSearch_ErrorOnDataBase_Implementation()
{
	FNotifyInfo NotifyInfo;
	NotifyInfo.Title = LOCTEXT("Notify.MatchMaking.Title", "Match information");
	NotifyInfo.Content = LOCTEXT("Notify.OnRequestStartSearch.ErrorOnDataBase.Content", "During the match search, there were problems with the database");
	AddNotify(NotifyInfo);
}


void USquadComponent::OnRequestStartSearch_NotHaveEnabledProfiles_Implementation()
{
	FNotifyInfo NotifyInfo;
	NotifyInfo.Title = LOCTEXT("Notify.MatchMaking.Title", "Match information");
	NotifyInfo.Content = LOCTEXT("Notify.OnRequestStartSearch.NotHaveEnabledProfiles.Content", "To enter the game, enable at least one profile");
	AddNotify(NotifyInfo);
}


void USquadComponent::OnRequestStartSearch_NotHavePrimaryWeapon_Implementation()
{
	FNotifyInfo NotifyInfo;
	NotifyInfo.Title = LOCTEXT("Notify.MatchMaking.Title", "Match information");
	NotifyInfo.Content = LOCTEXT("Notify.OnRequestStartSearch.NotHavePrimaryWeapon.Content", "You have not selected the primary weapon");
	AddNotify(NotifyInfo);
}


void USquadComponent::OnRequestStartSearch_MembersNotReady_Implementation()
{
	FNotifyInfo NotifyInfo;
	NotifyInfo.Title = LOCTEXT("Notify.MatchMaking.Title", "Match information");
	NotifyInfo.Content = LOCTEXT("Notify.OnRequestStartSearch.MembersNotReady.Content", "One or more members of the squad are not ready for battle");
	AddNotify(NotifyInfo);
}




void USquadComponent::OnClientStopSearch_Implementation()
{
	FNotifyInfo NotifyInfo;
	NotifyInfo.Title = LOCTEXT("Notify.MatchMaking.Title", "Match information");
	NotifyInfo.Content = LOCTEXT("Notify.OnClientStopSearch.Content", "The search of fight is canceled");
	AddNotify(NotifyInfo);
}

void USquadComponent::OnClientInvites_Implementation(const TArray<FSquadInviteInformation>& InInvites)
{
	for (auto MyInvite : InInvites)
	{
		FFormatNamedArguments FormatNamedArguments;
		FormatNamedArguments.Add("PlayerName", FRichHelpers::TextHelper_NotifyValue.SetValue(MyInvite.PlayerName).ToText());
		FormatNamedArguments.Add("Weapon", FRichHelpers::TextHelper_NotifyValue.SetValue(MyInvite.Options.GetWeaponType().GetDisplayName()).ToText());
		FormatNamedArguments.Add("Map", FRichHelpers::TextHelper_NotifyValue.SetValue(MyInvite.Options.GetGameMap().GetDisplayName()).ToText());
		FormatNamedArguments.Add("Bet", MyInvite.Options.Bet.ToResourse());
		FormatNamedArguments.Add("Lifes", FRichHelpers::TextHelper_NotifyValue.SetValue(FText::AsNumber(MyInvite.Options.GetNumberOfLives())).ToText());

		if (FFlagsHelper::HasAnyFlags(MyInvite.State, ESquadInviteState::Submitted))
		{
			QueueMessageBegin(*FString("Invite_" + MyInvite.InviteId.ToString()), Invite = MyInvite, fna = FormatNamedArguments)
				SMessageBox::Get()->SetHeaderText(FText::Format(LOCTEXT("Squad.Invite.Title", "Invite from \"{PlayerName}\""), fna));

				FText ContentText;
				if (Invite.Direction == ESquadInviteDirection::Invite)
				{
					if (Invite.Options.GetGameMode() == EGameMode::Construction)
					{
						ContentText = FText::Format(LOCTEXT("Squad.Invite.Construction", "Invite from \"{PlayerName}\" to the cooperative construction, are you accept invite?"), fna);
					}
					else if (Invite.Options.GetGameMode() == EGameMode::DuelMatch)
					{
						ContentText = FText::Format(LOCTEXT("Squad.Invite.Duel", "Invite from \"{PlayerName}\" to the duel fight, are you accept invite?\nFight properties\nWeapon: {Weapon} Map: {Map} Bet: {Bet} Number of lives: {Lifes}"), fna);
					}
					else
					{
						ContentText = FText::Format(LOCTEXT("Squad.Invite.Squad", "Invite from \"{PlayerName}\" to the squad, are you accept invite?"), fna);
					}
				}
				else
				{
					if (Invite.Options.GetGameMode() == EGameMode::Construction)
					{
						ContentText = FText::Format(LOCTEXT("Squad.Requests.Construction", "\"{PlayerName}\" wants to join the cooperative construction, are you accept request?"), fna);
					}
					else if (Invite.Options.GetGameMode() == EGameMode::DuelMatch)
					{
						ContentText = FText::Format(LOCTEXT("Squad.Invite.Duel", "Invite from \"{PlayerName}\" to the duel fight, are you accept invite?\nFight properties\nWeapon: {Weapon} Map: {Map} Bet: {Bet} Number of lives: {Lifes}"), fna);
					}
					else
					{
						ContentText = FText::Format(LOCTEXT("Squad.Requests.Squad", "\"{PlayerName}\" wants to join your squad, are you accept request?"), fna);
					}
				}

				SMessageBox::Get()->SetContent(ContentText, ETextJustify::Center);
				SMessageBox::Get()->SetButtonsText(LOCTABLE("BaseStrings", "All.Yes"), LOCTABLE("BaseStrings", "All.No"));
				SMessageBox::Get()->SetEnableClose(false);
				SMessageBox::Get()->SetIsOnceClickButtons(true);
				SMessageBox::Get()->OnMessageBoxButton.AddLambda([&, i = Invite](const EMessageBoxButton& InButton) {
					if (InButton == EMessageBoxButton::Left)
					{
						SendRequestRespondInvite(i, true);
					}
					else
					{
						SendRequestRespondInvite(i, false);
					}

					SMessageBox::Get()->ToggleWidget(false);
				});
				SMessageBox::Get()->ToggleWidget(true);
			QueueMessageEnd
		}

		if (FFlagsHelper::HasAnyFlags(MyInvite.State, ESquadInviteState::Approved | ESquadInviteState::Rejected))
		{
			FNotifyInfo NotifyInfo;
			NotifyInfo.ShowTime = 20;
			
			if (MyInvite.Options.GetGameMode() == EGameMode::Construction)
			{
				NotifyInfo.Title = LOCTEXT("Squad.Title", "Squad Information");
				if (FFlagsHelper::HasAnyFlags(MyInvite.State, ESquadInviteState::Approved))
					NotifyInfo.Content = FText::Format(LOCTEXT("Squad.Invite.Construction.Approved", "\"{PlayerName}\" has accepted your invite into cooperative construction."), FormatNamedArguments);
				else
					NotifyInfo.Content = FText::Format(LOCTEXT("Squad.Invite.Construction.Rejected", "\"{PlayerName}\" has declined your invite into cooperative construction."), FormatNamedArguments);
			}
			else if (MyInvite.Options.GetGameMode() == EGameMode::DuelMatch)
			{
				NotifyInfo.Title = LOCTEXT("Squad.Title.Duel", "Duel Information");
				if (FFlagsHelper::HasAnyFlags(MyInvite.State, ESquadInviteState::Approved))
					NotifyInfo.Content = FText::Format(LOCTEXT("Squad.Invite.Duel.Approved", "\"{PlayerName}\" has accepted your invite into the duel fight.\nFight properties\nWeapon: {Weapon} Map: {Map} Bet: {Bet} Number of lives: {Lifes}"), FormatNamedArguments);
				else	
					NotifyInfo.Content = FText::Format(LOCTEXT("Squad.Invite.Duel.Rejected", "\"{PlayerName}\" has declined your invite into the duel fight.\nFight properties\nWeapon: {Weapon} Map: {Map} Bet: {Bet} Number of lives: {Lifes}"), FormatNamedArguments);
			}
			else
			{
				NotifyInfo.Title = LOCTEXT("Squad.Title", "Squad Information");
				if (FFlagsHelper::HasAnyFlags(MyInvite.State, ESquadInviteState::Approved))
					NotifyInfo.Content = FText::Format(LOCTEXT("Squad.Invite.Squad.Approved", "\"{PlayerName}\" has accepted your invite into the squad."), FormatNamedArguments);
				else	
					NotifyInfo.Content = FText::Format(LOCTEXT("Squad.Invite.Squad.Rejected", "\"{PlayerName}\" has declined your invite into the squad."), FormatNamedArguments);
			}

			SNotifyList::Get()->AddNotify(NotifyInfo);
		}
	}
}

void USquadComponent::OnClientAdd_Implementation(const FSquadInviteInformation& InInformation)
{
	FNotifyInfo NotifyInfo;

	const auto localGameMode = InInformation.Options.GetGameMode();
	const auto localPlayerName = FRichHelpers::TextHelper_NotifyValue.SetValue(InInformation.PlayerName).ToText();
	
	if (InInformation.Direction == ESquadInviteDirection::Invite)
	{
		if (localGameMode == EGameMode::DuelMatch)
		{
			NotifyInfo.Title = LOCTEXT("Squad.Title.Duel", "Duel Information");
			NotifyInfo.Content = FText::Format(LOCTEXT("Notify.OnClientAdd.Duel.Content", "You summon \"{0}\" on duel fight."), localPlayerName);
		}
		else if (localGameMode == EGameMode::Construction)
		{
			NotifyInfo.Title = LOCTEXT("Squad.Title", "Squad Information");
			NotifyInfo.Content = FText::Format(LOCTEXT("Notify.OnClientAdd.Construction.Content", "You have invited \"{0}\" into the construction mode."), localPlayerName);
		}
		else
		{
			NotifyInfo.Title = LOCTEXT("Squad.Title", "Squad Information");
			NotifyInfo.Content = FText::Format(LOCTEXT("Notify.OnClientAdd.Squad.Content", "You have invited \"{0}\" in the squad."), localPlayerName);
		}
	}
	else
	{
		if (localGameMode == EGameMode::DuelMatch)
		{
			NotifyInfo.Title = LOCTEXT("Squad.Title.Duel", "Duel Information");
			NotifyInfo.Content = FText::Format(LOCTEXT("Notify.OnClientAdd.Duel.Content", "You summon \"{0}\" on duel fight."), localPlayerName);
		}
		else if (localGameMode == EGameMode::Construction)
		{
			NotifyInfo.Title = LOCTEXT("Squad.Title", "Squad Information");
			NotifyInfo.Content = FText::Format(LOCTEXT("Notify.OnClientAdd.ConstructionRequest.Content", "You have requested \"{0}\" into the construction mode."), localPlayerName);
		}
		else
		{
			NotifyInfo.Title = LOCTEXT("Squad.Title", "Squad Information");
			NotifyInfo.Content = FText::Format(LOCTEXT("Notify.OnClientAdd.SquadRequest.Content", "You have requested \"{0}\" in the squad."), localPlayerName);
		}
	}
	AddNotify(NotifyInfo);
}

void USquadComponent::OnClientAddErrorCode_Implementation(const int32 InCode)
{
	FNotifyInfo NotifyInfo;
	NotifyInfo.Title = LOCTEXT("Squad.Title", "Squad Information");

	switch (InCode)
	{
		case OnClientAdd_PlayerNotFound: 
			NotifyInfo.Content = LOCTEXT("Notify.OnClientAdd_PlayerNotFound.Content", "The selected player was not found.");
			break;
		case OnClientAdd_InviteYourself:
			NotifyInfo.Content = LOCTEXT("Notify.OnClientAdd_InviteYourself.Content", "You can not invite yourself in the squad.");
			break;
		case OnClientAdd_YouAreNotLeader:
			NotifyInfo.Content = LOCTEXT("Notify.OnClientAdd_YouAreNotLeader.Content", "You can not invite another player into the squad because you are not the squad leader.");
			break;
		case OnClientAdd_InviteTimeoutSent:
			NotifyInfo.Content = LOCTEXT("Notify.OnClientAdd_InviteTimeoutSent.Content", "You too often try to invite players into the squad.");
			break;
		case OnClientAdd_YouAreNotEnouthMoney:
			NotifyInfo.Title = LOCTEXT("Squad.Title.Duel", "Duel Information");
			NotifyInfo.Content = LOCTEXT("Notify.OnClientAdd_YouAreNotEnouthMoney.Content", "You do not have enough money to call a player to a duel fight.");
			break;
	}

	AddNotify(NotifyInfo);
}

void USquadComponent::OnClientAddErrorCodeWithString_Implementation(const FString& InPlayerName, const int32 InCode)
{
	const auto localPlayerName = FRichHelpers::TextHelper_NotifyValue.SetValue(InPlayerName).ToText();

	FNotifyInfo NotifyInfo;
	NotifyInfo.Title = LOCTEXT("Squad.Title", "Squad Information");

	switch (InCode)
	{
		case OnClientAdd_PlayerInSquad:
			NotifyInfo.Content = FText::Format(LOCTEXT("Notify.OnClientAdd_PlayerInSquad.Content", "\"{0}\" is already in the squad."), localPlayerName);
			break;
		case OnClientAdd_PlayerNotOnline:
			NotifyInfo.Content = FText::Format(LOCTEXT("Notify.OnClientAdd_PlayerNotOnline.Content", "\"{0}\" is offline."), localPlayerName);
			break;
		case OnClientAdd_PlayerNotEnouthMoney:
			NotifyInfo.Title = LOCTEXT("Squad.Title.Duel", "Duel Information");
			NotifyInfo.Content = FText::Format(LOCTEXT("Notify.OnClientAdd_PlayerNotEnouthMoney.Content", "\"{0}\" does not have enough money to participate in the duel fight."), localPlayerName);
			break;
		case OnClientAdd_InviteAlreadySent:
			NotifyInfo.Content = FText::Format(LOCTEXT("Notify.OnClientAdd_InviteAlreadySent.Content", "You have already sent the invite for \"{0}\"."), localPlayerName);
			break;
	}

	AddNotify(NotifyInfo);
}

void USquadComponent::OnClientRespondInvite_Implementation(const FSquadInviteInformation& InInformation)
{
	FNotifyInfo NotifyInfo;

	const auto localGameMode = InInformation.Options.GetGameMode();
	const auto localPlayerName = FRichHelpers::TextHelper_NotifyValue.SetValue(InInformation.PlayerName).ToText();

	if (localGameMode == EGameMode::DuelMatch)
	{
		NotifyInfo.Title = LOCTEXT("Squad.Title.Duel", "Duel Information");
		NotifyInfo.Content = FText::Format(LOCTEXT("Notify.OnClientRespondInvite.Duel.Content", "You accepted the invitation to a duel from \"{0}\"."), localPlayerName);
	}
	else if (localGameMode == EGameMode::Construction)
	{
		NotifyInfo.Title = LOCTEXT("Squad.Title", "Squad Information");
		NotifyInfo.Content = FText::Format(LOCTEXT("Notify.OnClientRespondInvite.Construction.Content", "You accepted the invitation to a construction mode from \"{0}\"."), localPlayerName);
	}
	else
	{
		NotifyInfo.Title = LOCTEXT("Squad.Title", "Squad Information");
		NotifyInfo.Content = FText::Format(LOCTEXT("Notify.OnClientRespondInvite.Squad.Content", "You accepted the invitation to a squad from \"{0}\"."), localPlayerName);
	}

	AddNotify(NotifyInfo);
}

void USquadComponent::OnClientRespondInviteErrorCode_Implementation(const int32 InCode)
{
	FNotifyInfo NotifyInfo;
	NotifyInfo.Title = LOCTEXT("Squad.Title", "Squad Information");

	switch (InCode)
	{
		case OnClientRespond_InviteNotFound:
			NotifyInfo.Content = LOCTEXT("Notify.OnClientRespond_InviteNotFound.Content", "Failed to respond to invitation, invitation not found.");
			break;
		case OnClientRespond_InviteAreRecived:
			NotifyInfo.Content = LOCTEXT("Notify.OnClientRespond_InviteAreRecived.Content", "You have already answered this request.");
			break;
		case OnClientRespond_YouAreInSquad:
			NotifyInfo.Content = LOCTEXT("Notify.OnClientRespond_YouAreInSquad.Content", "You are already in the squad.");
			break;
		case OnClientRespond_SenderInSquad:
			NotifyInfo.Content = LOCTEXT("Notify.OnClientRespond_SenderInSquad.Content", "The sender is already in the squad.");
			break;
		case OnClientRespond_NotEnouthMoney:
			NotifyInfo.Title = LOCTEXT("Squad.Title.Duel", "Duel Information");
			NotifyInfo.Content = LOCTEXT("Notify.OnClientRespond_NotEnouthMoney.Content", "You do not have enough money to accept an invitation to a duel fight.");
			break;
	}

	AddNotify(NotifyInfo);
}

void USquadComponent::OnClientRespondInviteErrorCodeWithString_Implementation(const FString& InPlayerName, const int32 InCode)
{
	FNotifyInfo NotifyInfo;
	NotifyInfo.Title = LOCTEXT("Squad.Title.Duel", "Duel Information");
	NotifyInfo.Content = FText::Format(LOCTEXT("Notify.OnClientRespond_SenderNotEnouthMoney.Content", "By \"{0}\" is does not have enough money to participate in the duel fight."), FRichHelpers::TextHelper_NotifyValue.SetValue(InPlayerName).ToText());
	//================================
	AddNotify(NotifyInfo);
}

void USquadComponent::OnClientRemove_Implementation(const FString& InPlayerName)
{
	FNotifyInfo NotifyInfo;
	NotifyInfo.Title = LOCTEXT("Squad.Title", "Squad Information");
	NotifyInfo.Content = FText::Format(LOCTEXT("Notify.OnClientRemove.Squad.Content", "You kicked \"{0}\" out of the squad."), FRichHelpers::TextHelper_NotifyValue.SetValue(InPlayerName).ToText());
	AddNotify(NotifyInfo);
}

static TArray<FString> GetSquadMemberNames(const TArray<FQueueMemberContainer>& members)
{
	TArray<FString> names;
	for(const auto &m : members)
	{
		names.Add(FRichHelpers::TextHelper_NotifyValue.SetValue(m.Name).ToString());
	}
	return names;
}

static FString GetSquadMemberName(const TArray<FQueueMemberContainer>& members)
{
	return FString::Join(GetSquadMemberNames(members), TEXT(","));
}

void USquadComponent::OnClientMatchMakingProgress_Implementation()
{

	//==========================================================
	{
		TArray<FQueueMemberContainer> join, leave;
		//----------------------------------------
		auto& current = SqaudInformation.Members;
		auto& last = LastSqaudInformation.Members;

		//----------------------------------------
		for (const auto &m : current)
		{
			if (last.ContainsByPredicate([&, m](const FQueueMemberContainer& mm) { return mm.PlayerId == m.PlayerId; }) == false)
			{
				join.Add(m);
			}
		}

		//----------------------------------------
		for (const auto &m : last)
		{
			if (current.ContainsByPredicate([&, m](const FQueueMemberContainer& mm) { return mm.PlayerId == m.PlayerId; }) == false)
			{
				leave.Add(m);
			}
		}

		//----------------------------------------
		if (!!join.Num())
		{
			FNotifyInfo NotifyInfo;
			const auto name = GetSquadMemberName(join);
			NotifyInfo.Title = LOCTEXT("Notify.Squad.Join.Title", "Squad information");
			NotifyInfo.Content = FText::Format(LOCTEXT("Notify.Squad.Join.Content", "\"{0}\" joined in the squad"), FText::FromString(name));
			AddNotify(NotifyInfo);
		}

		//----------------------------------------
		if (!!leave.Num())
		{
			FNotifyInfo NotifyInfo;
			const auto name = GetSquadMemberName(leave);
			NotifyInfo.Title = LOCTEXT("Notify.Squad.Join.Title", "Squad information");
			NotifyInfo.Content = FText::Format(LOCTEXT("Notify.Squad.Leave.Content", "\"{0}\" leaves from the squad"), FText::FromString(name));
			AddNotify(NotifyInfo);
		}
	}

	//==========================================================
	if (SqaudInformation.Status == ESearchSessionStatus::None || SqaudInformation.Status == ESearchSessionStatus::End)
	{
		return;
	}

	//==========================================================
	const auto localGameMap = FRichHelpers::TextHelper_NotifyValue.SetValue(SqaudInformation.Options.GetGameMap().GetDisplayName()).ToText();
	const auto localGameMode = FRichHelpers::TextHelper_NotifyValue.SetValue(SqaudInformation.Options.GetGameMode().GetDisplayName()).ToText();

	FNotifyInfo NotifyInfo;
	//==========================================================
	NotifyInfo.Title = LOCTEXT("Notify.MatchMaking.Title", "Match information");

	switch (SqaudInformation.Status)
	{
	case ESearchSessionStatus::SearchPrepare:
		if (SqaudInformation.IsLeader)
		{
			NotifyInfo.Content = LOCTEXT("Notify.SearchPrepare.Content.Leader", "Waiting answer from squad members, not all squad members is ready.");
		}
		else
		{
			NotifyInfo.Content = LOCTEXT("Notify.SearchPrepare.Content.Memeber", "Squad leader requested a search of the fight.");
		}
		break;
	case ESearchSessionStatus::FoundJoin:
		NotifyInfo.Content = FText::Format(LOCTEXT("Notify.OnClientMatchMakingProgress.FoundJoin.Content", "Entering into the fight.\nGame mode: {0}\nGame map: {1}\n\nStart the loading should happen within few seconds."), localGameMode, localGameMap);
		break;
	case ESearchSessionStatus::FoundPrepare:
		LoadPackageAsync(FString::Printf(TEXT("Game/1LOKAgame/Maps/%s.umap"), *SqaudInformation.Options.GetGameMap().GetName()), FLoadPackageAsyncDelegate(), 0, EPackageFlags::PKG_Need);
		NotifyInfo.Content = FText::Format(LOCTEXT("Notify.OnClientMatchMakingProgress.FoundPrepare.Content", "Search game ended, preparation of entering into the fight.\nGame mode: {0}\nGame map: {1}"), localGameMode, localGameMap);
		break;
	case ESearchSessionStatus::FoundError:
		NotifyInfo.Content = LOCTEXT("Notify.OnClientMatchMakingProgress.FoundError.Content", "An error occurred while searching of the fight.");
		break;
	case ESearchSessionStatus::NotEnouthMoney:
		NotifyInfo.Content = LOCTEXT("Notify.OnClientMatchMakingProgress.NotEnouthMoney.Content", "Not enough money to participate in the fight.");
		break;
	default:
		return;
	}

	AddNotify(NotifyInfo);

	if (SqaudInformation.IsLeader == false && SqaudInformation.IsInSquad() && SqaudInformation.Status == ESearchSessionStatus::SearchPrepare)
	{
		QueueBegin(SMessageBox, "SearchPrepare.Member")
			SMessageBox::Get()->SetHeaderText(LOCTEXT("SearchPrepare.Member.Title", "Preparing"));
			SMessageBox::Get()->SetContent(LOCTEXT("SearchPrepare.Member.Content", "Squad leader requested a search of the fight, you can select accept for continue searching of fight or decline for leave a squad."));
			SMessageBox::Get()->SetButtonsText(LOCTABLE("BaseStrings", "All.Accept"), LOCTABLE("BaseStrings", "All.Decline"));
			SMessageBox::Get()->SetEnableClose(false);
			SMessageBox::Get()->OnMessageBoxButton.AddLambda([&] (const EMessageBoxButton InButton) -> void
			{
				if (InButton == EMessageBoxButton::Left)
				{
					SendRequestToggleSearch(FSessionMatchOptions());
				}
				else
				{
					SendRequestRemove(FLokaGuid());
				}

				SMessageBox::Get()->ToggleWidget(false);
			});
			SMessageBox::Get()->ToggleWidget(true);
		QueueEnd
	}
}

void USquadComponent::OnRep_SqaudInformation()
{
	OnSquadUpdate.Broadcast(SqaudInformation);

	if (HasLocalClientComponent())
	{
		OnClientMatchMakingProgress();
	}
}

bool USquadComponent::IsAllowServerQueries() const
{
	if (const auto gm = GetBasicGameMode())
	{
		return gm->IsA<AConstructionGameMode>();
	}
	return false;
}



#undef LOCTEXT_NAMESPACE