#pragma once


#include "Components/Shared/PlayerOnlineStatus.h"
#include "ChatMemberModel.generated.h"

USTRUCT()
struct FChatMemberModel
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()					FLokaGuid									ChanelId;
	UPROPERTY()					FLokaGuid									MemberId;
	UPROPERTY()					FLokaGuid									PlayerId;
	UPROPERTY()					FString										PlayerName;
	UPROPERTY(meta = (Bitmask))	TEnumAsByte<PlayerFriendStatus::Type>		Status;
};
