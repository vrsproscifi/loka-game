#pragma once
#include "ChatMessageContainer.generated.h"

USTRUCT()
struct FChatMessageContainer
{
	GENERATED_USTRUCT_BODY()

		UPROPERTY()
		FString MessageId;

	UPROPERTY()
		FString SenderId;

	UPROPERTY()
		FString SenderLeague;

	UPROPERTY()
		FString SenderName;

	UPROPERTY()
		FString Message;

	UPROPERTY()
		int32 UnixDate;
};