// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "LookOnComponent.h"


ULookOnComponent::ULookOnComponent()
{
	PrimaryComponentTick.bCanEverTick = true;

	SetFilterClass(APawn::StaticClass());
	SetTargetRange(250.0f);
	SetTargetVisionAngle(60.0f);
	SetTargetCollisionChannel(ECC_Pawn);
}

void ULookOnComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (IsActive() && TargetActor)
	{
		auto OwnerAsPawn = Cast<APawn>(GetOwner());
		if (OwnerAsPawn && OwnerAsPawn->Controller)
		{
			auto TargetRotation = FRotationMatrix::MakeFromX(TargetActor->GetActorLocation() - GetOwner()->GetActorLocation()).Rotator();
			auto CurrentRotation = OwnerAsPawn->GetControlRotation();
			auto Dist = FVector::Dist(TargetActor->GetActorLocation(), GetOwner()->GetActorLocation());
			auto DistFactor = 1.0f - (Dist / GetSourceRange());

			if ((FMath::IsNearlyEqual(CurrentRotation.Pitch, TargetRotation.Pitch, 2.0f * DistFactor) && FMath::IsNearlyEqual(CurrentRotation.Yaw, TargetRotation.Yaw, 2.0f * DistFactor) && FMath::IsNearlyEqual(CurrentRotation.Roll, TargetRotation.Roll, 2.0f * DistFactor)) == false)
			{
				TargetRotation = FMath::RInterpTo(CurrentRotation, TargetRotation, DeltaTime, FMath::Max(3.5f, DistFactor));
				OwnerAsPawn->Controller->SetControlRotation(TargetRotation);
			}			
		}
	}
}

void ULookOnComponent::SetFilterClass(TSubclassOf<AActor> InClass)
{
	FilterClass = InClass;
}

void ULookOnComponent::SetTargetRange(const float InDistance)
{
	TraceDistance = InDistance;
}

void ULookOnComponent::SetTargetVisionAngle(const float InDegress)
{
	PeripheralVisionAngle = InDegress;
	PeripheralVisionCosine = FMath::Cos(FMath::DegreesToRadians(GetSourceAngle()));
}

void ULookOnComponent::SetTargetCollisionChannel(const ECollisionChannel InCollisionChannel)
{
	TargetCollisionChannel = InCollisionChannel;
}

void ULookOnComponent::RequestFindingTarget()
{
	TargetActor = FindTarget();
}

AActor* ULookOnComponent::FindTarget()
{
	// Sphere sweep for find targets on forward vector
	// Trace's on found actor to detect is real visibily
	// Set variable to closest actor and help rotate to target

	AActor* Target = nullptr;

	TArray<FOverlapResult> OverlapResults;
	TArray<FHitResult> HitResults;
	FCollisionQueryParams localQueryParams = FCollisionQueryParams(TEXT("LookOn_Main"), false, GetOwner());
	FCollisionResponseParams localResponseParams = FCollisionResponseParams::DefaultResponseParam;
	localResponseParams.CollisionResponse.SetAllChannels(ECR_Ignore);
	localResponseParams.CollisionResponse.SetResponse(TargetCollisionChannel, ECR_Overlap);

	FVector localOwnerLocation = GetOwner()->GetActorLocation();
	FVector localStartTrace = localOwnerLocation;
	FCollisionShape localShape = FCollisionShape::MakeSphere(GetSourceRange());
	
	//float savedDistance = GetSourceRange();
	float savedDot = .0f;
	
	GetWorld()->OverlapMultiByChannel(OverlapResults, localStartTrace, FQuat::Identity, TargetCollisionChannel, localShape, localQueryParams, localResponseParams);

	for (auto HitResult : OverlapResults)
	{
		if (HitResult.GetActor() && HitResult.GetActor()->GetClass()->IsChildOf(FilterClass))
		{
			FHitResult SubHitResult;
			FCollisionQueryParams SubQueryParams(TEXT("LookOn_Visibility"), true, GetOwner());
			if (GetWorld()->LineTraceSingleByChannel(SubHitResult, localOwnerLocation, HitResult.GetActor()->GetActorLocation(), TargetCollisionChannel, SubQueryParams))
			{
				if (SubHitResult.GetActor())
				{
					const auto TargetActorLocation = SubHitResult.GetActor()->GetActorLocation();
					const float DotProduct = ((TargetActorLocation - localOwnerLocation).GetSafeNormal() | GetOwner()->GetActorRotation().Vector());
					const bool IsInVisionAngle = (DotProduct >= PeripheralVisionCosine);
					//auto Dist = FVector::Dist(SubHitResult.GetActor()->GetActorLocation(), localOwnerLocation);

					if (IsInVisionAngle && DotProduct > savedDot && SubHitResult.GetActor() != GetOwner())
					{
						savedDot = DotProduct;
						Target = SubHitResult.GetActor();
					}
				}
			}
		}
	}

	return Target;
}

