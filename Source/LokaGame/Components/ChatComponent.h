#pragma once
#include "BasicGameComponent.h"
#include "ChatComponent/ChatMemberModel.h"
#include "ChatComponent/ChatMessageReciveModel.h"
#include "ChatComponent/ChatChanelModel.h"
#include "ChatComponent/ChatMessageSendModel.h"
#include "ChatComponent.generated.h"


DECLARE_MULTICAST_DELEGATE_OneParam(FOnChannelsUpdate, const TArray<FChatChanelModel>&);
DECLARE_MULTICAST_DELEGATE_OneParam(FOnMessagesUpdate, const TArray<FChatMessageReciveModel>&);

UCLASS(Transient)
class UChatComponent : public UBasicGameComponent
{
	GENERATED_BODY()

public:
	UChatComponent();

	void StartTimers();

	UFUNCTION(Reliable, Server, WithValidation) void SendRequestGetListOfChanels();
	UFUNCTION(Reliable, Server, WithValidation) void SendRequestGetListOfMembers();
	UFUNCTION(Reliable, Server, WithValidation) void SendRequestGetListOfMessages();
	UFUNCTION(Reliable, Server, WithValidation) void SendRequestPostMessageTo(const FChatMessageSendModel& InMessage);

	FOnChannelsUpdate OnChannelsUpdate;
	FOnMessagesUpdate OnMessagesUpdate;

protected:
	TSharedPtr<FOperationRequest> RequestGetListOfChanels;
	TSharedPtr<FOperationRequest> RequestGetListOfMembers;
	TSharedPtr<FOperationRequest> RequestGetListOfMessages;
	TSharedPtr<FOperationRequest> RequestPostMessageTo;

	UFUNCTION() void OnChannels(const TArray<FChatChanelModel>& InChannels);
	UFUNCTION() void OnMembers(const TArray<FChatMemberModel>& InMembers);
	UFUNCTION() void OnMessages(const TArray<FChatMessageReciveModel>& InMessages);

public:

	FORCEINLINE const TArray<FChatChanelModel>& GetChannels() const { return Channels; }
	FORCEINLINE const TArray<FChatMessageReciveModel>& GetMessages() const { return Messages; }

protected:

	UFUNCTION()
	void OnRep_Channels();

	UFUNCTION()
	void OnRep_Messages();

	UPROPERTY(ReplicatedUsing = OnRep_Channels)
	TArray<FChatChanelModel> Channels;

	UPROPERTY()
	TArray<FChatMemberModel> Members;

	UPROPERTY(ReplicatedUsing = OnRep_Messages)
	TArray<FChatMessageReciveModel> Messages;
};