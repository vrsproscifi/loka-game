#pragma once
#include "Item/ItemBaseEntity.h"
#include "Fraction/FractionEntity.h"
#include "MarketCatalogStyle.h"
#include <SScaleBox.h>
#include "SResourceTextBox.h"
#include "FractionItemLevelComponent.h"

DECLARE_DELEGATE_OneParam(FOnUniversalItemAction, const UItemBaseEntity*);


class SMarketCatalogProduct
		: public SButton
{
public:
	SLATE_BEGIN_ARGS(SMarketCatalogProduct) {}
		SLATE_EVENT(FOnUniversalItemAction, OnSelectItem)
	SLATE_END_ARGS()

	FOnUniversalItemAction OnSelectItem;

	FReply OnItemClicked() const
	{
		OnSelectItem.ExecuteIfBound(Item);
		return FReply::Handled();
	}


	void Construct(const FArguments& InArgs, const UItemBaseEntity* item)
	{
		OnSelectItem = InArgs._OnSelectItem;
		Item = item;

		SButton::Construct(
			SButton::FArguments()
			.OnClicked(this, &SMarketCatalogProduct::OnItemClicked)
			.ContentPadding(FMargin(0))
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot()
				.AutoHeight()
				[
					SNew(SBorder)
					.Padding(Style->CatalogItem.HeaderBackground.Padding)
					.BorderImage(&Style->CatalogItem.HeaderBackground.Brush)
					[
						SNew(STextBlock)
						.Text_UObject(Item, &UItemBaseEntity::GetItemName)
						.TextStyle(&Style->CatalogItem.HeaderStyle)
						.Justification(Style->CatalogItem.HeaderJustify)
					]
				]
				+ SVerticalBox::Slot()
				[
					SNew(SBorder)
					[
						SNew(SOverlay)
						+ SOverlay::Slot()
						[
							SNew(SScaleBox)
							.VAlign(VAlign_Center)
							.HAlign(HAlign_Center)
							.Stretch(EStretch::ScaleToFit)
							[
								SAssignNew(Image, SImage)
								.Image_UObject(Item, &UItemBaseEntity::GetImage)
							]
						]
						+ SOverlay::Slot()
						[
							SNew(STextBlock)
							.Visibility_Lambda([this]
								{
									if(Image->IsHovered())
									{
										return EVisibility::Visible;
									}
									return EVisibility::Hidden;
								})
						]
					]
				]
				+ SVerticalBox::Slot()
				.VAlign(VAlign_Center)
				.AutoHeight()
				[
					SNew(SHorizontalBox)
					+ SHorizontalBox::Slot()
					.AutoWidth()
					[
						SNew(SFractionItemLevelComponent, &Item)
					]
					+ SHorizontalBox::Slot()
					[
						SNew(SBorder)
						.Padding(Style->CatalogItem.CostBackground.Padding)
						.BorderImage(&Style->CatalogItem.CostBackground.Brush)
						[
							SNew(SResourceTextBox).CustomSize(FVector(16, 16, 12))
							.TypeAndValue(this, &SMarketCatalogProduct::GetCost)
						]
					]	
				]
			]
		);
	}

	FResourceTextBoxValue GetCost() const
	{
		if (auto i = GetValidObject(Item))
		{
			return i->GetCostAsResource();
		}
		return FResourceTextBoxValue();
	}

private:
	SlateStylizedWidget(FMarketCatalogStyle, "MarketCatalogStyle");
	TSharedPtr<SImage> Image;
	const UItemBaseEntity* Item;
};
