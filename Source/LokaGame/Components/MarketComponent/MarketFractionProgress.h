#pragma once

class UPlayerFractionEntity;


class SMarketFractionProgress
	: public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SMarketFractionProgress) {}
	SLATE_EVENT(FOnMarketFractionSwitch, OnPlayerFractionSwitch)
	SLATE_END_ARGS()

	FOnMarketFractionSwitch OnPlayerFractionSwitch;

	const UPlayerFractionEntity* GetFraction() const
	{
		if (SelectedFraction)
		{
			if (auto f = GetValidObject(*SelectedFraction))
			{
				return f;
			}
		}
		return nullptr;
	}

	FText GetFractionName() const
	{
		if(auto f = GetFraction())
		{
			return f->GetFractionName();
		}
		return FText::GetEmpty();
	}

	FText GetFractionRank() const
	{
		if (auto f = GetFraction())
		{
			return f->GetFractionRank();
		}
		return FText::GetEmpty();
	}

	FText GetFractionReputation() const
	{
		if (auto f = GetFraction())
		{
			return f->GetFractionReputation();
		}
		return FText::GetEmpty();
	}

	void Construct(const FArguments& InArgs, const UPlayerFractionEntity** selectedFraction)
	{
		OnPlayerFractionSwitch = InArgs._OnPlayerFractionSwitch;
		SelectedFraction = selectedFraction;

		ChildSlot
		[
			SNew(SBorder)
			.BorderImage(&Style->FractionProgress.Border.Brush)
			.Padding(TAttribute<FMargin>(Style->FractionProgress.Border.Padding))
			[
				SNew(SBorder)
				.BorderImage(&Style->FractionProgress.Background.Brush)
				.Padding(TAttribute<FMargin>(Style->FractionProgress.Background.Padding))
				[
					SNew(SHorizontalBox)
					+ SHorizontalBox::Slot()
					[
						SNew(STextBlock)
						.Text(this, &SMarketFractionProgress::GetFractionName)
						.TextStyle(&Style->FractionProgress.FractionNameStyle)
					]
					+ SHorizontalBox::Slot()
					[
						SNew(SVerticalBox)
						+ SVerticalBox::Slot()
						.AutoHeight()
						[
							SNew(STextBlock)
							.Text(this, &SMarketFractionProgress::GetFractionRank)
							.TextStyle(&Style->FractionProgress.FractionRankStyle)
						]
						+ SVerticalBox::Slot()
						.AutoHeight()
						[
							SNew(STextBlock)
							.Text_UObject(GetFraction(), &UPlayerFractionEntity::GetFractionReputation)
							.TextStyle(&Style->FractionProgress.FractionReputationStyle)
						]
					]
					+ SHorizontalBox::Slot()
					[
						SNew(SButton)
						.IsEnabled_Lambda([this]
						{
							if(auto f = GetFraction())
							{
								return f->IsCurrentFraction() == false;
							}
							return false;
						})
						.ContentPadding(Style->FractionProgress.JoinFraction.Padding)
						.ButtonStyle(&Style->FractionProgress.JoinFraction.ButtonStyle)
						.Text(NSLOCTEXT("SMarketFractionProgress", "JoinFraction", "Join Fraction"))
						.TextStyle(&Style->FractionProgress.JoinFraction.TextStyle)
						.OnClicked(this, &SMarketFractionProgress::OnJoinFractionClicked)
					]
				]
			]
		];
	}

private:
	SlateStylizedWidget(FMarketCatalogStyle, "MarketCatalogStyle");

	FReply OnJoinFractionClicked() const
	{
		if (auto f = GetFraction())
		{
			OnPlayerFractionSwitch.ExecuteIfBound(f);
		}
		return FReply::Handled();
	}

	const UPlayerFractionEntity** SelectedFraction;
};