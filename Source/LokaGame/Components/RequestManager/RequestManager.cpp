#include "LokaGame.h"
#include "RequestManager.h"

bool FBaseRequestHandler::OnDeSerializeProxy(const FString& objectStr) const
{
	LastHandleContent = objectStr;
	LastHandleDate = FDateTime::Now();
	return OnDeSerialize(objectStr);
}

FBaseRequestHandler::FBaseRequestHandler(const FOperationRequest* operation)
	: Operation(operation)
{
	//OnRequestHandlerUnbind.BindRaw(this, &FBaseRequestHandler::Unbind);
}

FBaseRequestHandler::~FBaseRequestHandler()
{
	//OnRequestHandlerUnbind.ExecuteIfBound();
}

void FBaseRequestHandler::Unbind()
{

}

bool FBaseRequestHandler::OnDeSerialize(const FString& objectStr) const
{
	return false;
}


FString FOperationRequest::GetItterationString() const
{
	return FString::Printf(TEXT("%d of %d)"), GetItterationNum(), GetItterationLimit());
}

uint64 FOperationRequest::GetItterationLimit() const
{
	return TimerLimit;
}

uint64 FOperationRequest::GetItterationNum() const
{
	return TimerItter;
}

bool FOperationRequest::ItterationInc()
{
	if (IsValidTimer())
	{
		TimerItter++;
		return true;
	}
	return false;
}

void FOperationRequest::ResetItterations()
{
	TimerItter = 0;
}

bool FOperationRequest::IsValidTimer() const
{
	return TimerValidate == false || TimerValidate && TimerItter < TimerLimit;
}

void FOperationRequest::StartTimer(FTimerManager* InManager, const float& InStartDelay)
{
	checkf(InManager, TEXT("[StartTimer] Timer manager is null for %s"), *OperationName);
	TimerStartDate = FDateTime::Now();
	TimerManager = InManager;
	InManager->SetTimer(TimerHandle, TimerProxy, TimerRate, TimerLoop, InStartDelay);
}

void FOperationRequest::StopTimer(FTimerManager* InManager)
{
	checkf(InManager, TEXT("[StopTimer] Timer manager is null for %s"), *OperationName);
	if (InManager && TimerHandle.IsValid()) { InManager->ClearTimer(TimerHandle); }
}

void FOperationRequest::OnHttpRequestComplete(FHttpRequestPtr HttpRequest, FHttpResponsePtr HttpResponse, bool bSucceeded) const
{
	FRequestExecuteError error(OperationName);
	if (HttpResponse.IsValid())
	{
		//==================================================
		const auto code = HttpResponse->GetResponseCode();
		const auto content = HttpResponse->GetContentAsString().TrimQuotes().ReplaceEscapedCharWithChar();
		//==================================================
		error.Status = code;
		error.Content = content;
		//==================================================
		UE_LOG(LogHttp, Warning, TEXT("Request: %s -> [%s][%d] %s"), *OperationName, *Token.Get().ToString(), code, *content);
		//==================================================
		if (Handlers.Contains(code) && Handlers[code].IsValid())
		{
			if (Handlers[code]->OnDeSerializeProxy(content)) { return; }
			error.Error = ERequestErrorCode::SerializationFailed;
		}
		else if (code == 401 || code >= 900) { error.Error = ERequestErrorCode::Unauthorized; }
		else if (code == 500) { error.Error = ERequestErrorCode::ServerError; }
		else if (code == 200)
		{
			UE_LOG(LogHttp, Warning, TEXT("OnHttpRequest[Operation: %s][Code: %d] not bind!"), *OperationName, error.Status);
			return;
		}
		else { error.Error = ERequestErrorCode::HandlerNotFound; }
	}
	else
	{
		error.Content = TEXT("HttpResponse is invalid");
		error.Error = ERequestErrorCode::BadResponse;
	}
	UE_LOG(LogHttp, Error, TEXT("OnHttpRequestComplete[Operation: %s][Code: %d][Token: %s]"), *OperationName, error.Status, *Token.Get().ToString());
	OnFailedResponse.ExecuteIfBound(error);
	PrewExecuteError = LastExecuteError;
	LastExecuteError = error;
}

void FOperationRequest::OnTimerItterationProxy()
{
	if (IsValidTimer()) { TimerDelegate.ExecuteIfBound(); }
	else { OnLoseAttemps.ExecuteIfBound(LastExecuteError); }
	TimerLastExecuteDate = FDateTime::Now();
}
