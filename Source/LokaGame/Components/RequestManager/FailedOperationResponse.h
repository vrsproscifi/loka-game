#pragma once
#include "FailedOperationResponse.generated.h"


UENUM()
enum class ERequestErrorCode : uint8
{
	None,

	BadResponse,
	HandlerNotFound,
	SerializationFailed,
	ConnectionFailed,
	Unauthorized,
	ServerError,
};

//	���������� �� ������ ��� ���������� �������
USTRUCT()
struct FRequestExecuteError
{
	GENERATED_USTRUCT_BODY()

	//	���� ��������� ������
	UPROPERTY() FDateTime HandledDate;

	//	���������� ��� ������
	UPROPERTY() ERequestErrorCode Error;

	//	�������� ��������, � ������� ��������� ������
	UPROPERTY() FString Operation;

	//	�������, ���������� �� ������
	UPROPERTY() FString Content;

	//	�����, ���������� �� ������
	UPROPERTY() uint32 Status;

	FRequestExecuteError()
		: Error(ERequestErrorCode::None)
		, Status(0)
	{
		HandledDate = FDateTime::Now();
	}

	FRequestExecuteError(const FString& operation)
		: Error(ERequestErrorCode::None)
		, Operation(operation)
		, Status(0)
	{
		HandledDate = FDateTime::Now();
	}

	FString ToString() const
	{
		TMap<FString, FStringFormatArg> args;
		args.Add("Operation", FStringFormatArg(Operation));
		args.Add("Status", FStringFormatArg(Status));
		args.Add("Error", FStringFormatArg(static_cast<int32>(Error)));

		args.Add("Content", FStringFormatArg(Content));
		args.Add("HandledDate", FStringFormatArg(HandledDate.ToString()));

		args.Add("Header", FStringFormatArg("================= RequestExecuteError ========== \n"));

		args.Add("ContentBegin", FStringFormatArg("\n================= ContentBegin ========== \n\n"));

		args.Add("ContentEnd", FStringFormatArg("\n\n================= ContentEnd ========== \n"));

		return FString::Format(TEXT("{Header} Operation: {Operation} -- Status: {Status} / {Error}\n HandledDate: {HandledDate}\n {ContentBegin} {Content} {ContentEnd}"), args);
	}

};

DECLARE_DELEGATE_OneParam(FOnFailedOperationResponse, const FRequestExecuteError&);
