#pragma once
#include "PlayerOnlineStatus.generated.h"


UENUM(meta = (Bitflags))
namespace PlayerFriendStatus
{
	enum Type
	{
		None,

		Offline = 1,
		Online = 2,
		SearchGame = 4,
		PlayGame = 8,

		Ready = 16,
		Leader = 32,
		Global = 64,

		Building = 128,
	};
}

ENUM_CLASS_FLAGS(PlayerFriendStatus::Type)

typedef PlayerFriendStatus::Type EPlayerFriendStatus;

UENUM()
namespace ActionPlayerRequestState
{
	enum Type
	{
		Waiting,
		Confirmed,
		ConfirmedWaiting,
		Rejected,
		Blocked,
		End
	};
}