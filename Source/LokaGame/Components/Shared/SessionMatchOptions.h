#pragma once


#include "TypeCost.h"
#include "DuelStructures.h"
#include "GameModeTypeId.h"
#include "Weapon/WeaponContainer.h"
#include "SessionMatchOptions.generated.h"



USTRUCT(BlueprintType)
struct FSessionMatchOptions
{
	GENERATED_USTRUCT_BODY()

	FSessionMatchOptions()
		: GameMap(0)
		, GameMode(0)
		, UniversalId()
		, RoundTimeInSeconds(0)
		, Bet(0, EGameCurrency::Money)
		, WeaponType(EDuelWeaponTypes::All)
		, Difficulty(0)
		, AdditionalFlag(0)
		, NumberOfBots(0)
		, ArmourLevel(0)
	{

		
	}

	FString ToString() const
	{
		TMap<FString, FStringFormatArg> args;
		args.Add("Header", FString("\n ============= Options ============= \n"));

		args.Add("GameMap", FStringFormatArg(FString::Printf(TEXT("%d | %s"), static_cast<int32>(GameMap), *EGameMap(GameMap).ToString())));
		args.Add("GameMode", FStringFormatArg(FString::Printf(TEXT("%d | %s"), static_cast<int32>(GameMode), *EGameMode(GameMode).ToString())));

		args.Add("UniversalId", UniversalId.ToString());
		args.Add("RoundTime", RoundTimeInSeconds);

		args.Add("Difficulty", Difficulty);
		args.Add("AdditionalFlag", AdditionalFlag);
		
		args.Add("NumberOfBots", NumberOfBots);
		args.Add("ArmourLevel", ArmourLevel);

		args.Add("Weapon", FStringFormatArg(FString::Printf(TEXT("%d | %s"), WeaponType, *EWeaponType(WeaponType).ToString())));

		return FString::Format(TEXT("{Header} GameMap: {GameMap}\n GameMode: {GameMode}\n UniversalId: {UniversalId}\n RoundTime: {RoundTime}\n Difficulty: {Difficulty}\n AdditionalFlag: {AdditionalFlag}\n NumberOfBots: {NumberOfBots}\n ArmourLevel: {ArmourLevel}\n Weapon: {Weapon}"), args);
	}

	//============================================
	/// <summary>
	/// ������� �������
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (Bitmask, BitmaskEnum="EBlueprintGameMap")) int32 GameMap;

	/// <summary>
	/// ����� ����
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (Bitmask, BitmaskEnum = "EBlueprintGameMode")) int32 GameMode;

	/// <summary>
	/// ������������� ������ �� �������� ��������, �������� ���� ����������� ����������� �� �����
	/// </summary>
	UPROPERTY() FLokaGuid UniversalId;

	/// <summary>
	/// ������������ ������ � ��������
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadWrite) int32 RoundTimeInSeconds;

	//============================================
	/// <summary>
	/// ������ ��� �����
	/// </summary>
	UPROPERTY()		FTypeCost Bet;

	/// <summary>
	/// ��� ������ ��� ����� / ����������
	/// </summary>
	UPROPERTY()		int32 WeaponType;

	/// <summary>
	/// ������� ��������� �����, ���� ��� ����
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadWrite) float Difficulty;

	float GetDifficulty() const
	{
		return FMath::Clamp<float>(Difficulty, 0.5, 7.9);
	}

	/// <summary>
	/// �������������� ��������� �����
	/// <para>
	///  <see cref="GetNumberOfLives"/> - ���������� ������ ��� �����
	/// </para> 
	/// <para>
	///  <see cref="GetWaveNumber"/> - ���������� ���� � PvE - ������
	/// </para>         
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadWrite) int32 AdditionalFlag;

	/// <summary>
	/// ���������� ���������� ������ ��� �����, ���������� �� AdditionalFlag
	/// </summary>
	int32 GetNumberOfLives() const
	{
		return AdditionalFlag;
	}

	UPROPERTY(EditAnywhere, BlueprintReadWrite) uint8 NumberOfBots;

	uint8 GetNumberOfBots() const
	{
		return NumberOfBots + 1;
	}

	/// <summary>
	/// ������� �������� �������
	/// </summary>
	UPROPERTY() uint8 ArmourLevel;


	/// <summary>
	/// ���������� ������������ ���������� �����, ���������� �� AdditionalFlag
	/// </summary>
	int32 GetScoreLimit() const
	{
		return AdditionalFlag;
	}

	/// <summary>
	/// ���������� ����� �����, ���������� �� AdditionalFlag
	/// </summary>
	int32 GetWaveNumber() const
	{
		return AdditionalFlag;
	}

	FORCEINLINE EGameMap GetGameMap() const
	{
		return EGameMap(GameMap);
	}

	FORCEINLINE EGameMode GetGameMode() const
	{
		return EGameMode(GameMode);
	}

	FORCEINLINE EWeaponType GetWeaponType() const
	{
		return EWeaponType(WeaponType);
	}
};