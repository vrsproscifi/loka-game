#pragma once

#include "BasicGameComponent.h"
#include "Models/UnlockItemContainer.h"
#include "Item/ItemModelInfo.h"
#include "MarketComponent/SellItemRequest.h"
#include "MarketComponent/SellItemResponse.h"
#include "MarketComponent.generated.h"


class UItemBaseEntity;
struct FUnlockItemRequestContainer;
class UPlayerFractionEntity;
class SMarketPlaceWindow;
class FOperationRequest;
class UPlayerInventoryItem;

DECLARE_MULTICAST_DELEGATE_OneParam(FOnUnlockItemSuccessfully, const FUnlockItemResponseContainer&);
DECLARE_DELEGATE_OneParam(FOnSwitctPlayerFraction, const UPlayerFractionEntity*);

DECLARE_MULTICAST_DELEGATE_OneParam(FOnItemUnlocked, UItemBaseEntity*);

DECLARE_DELEGATE_RetVal_OneParam(UPlayerInventoryItem*, FOnGetInventoryItem, const FGuid&);
DECLARE_MULTICAST_DELEGATE_OneParam(FOnSellItem, const FGuid&);

UCLASS(Transient)
class UMarketComponent : public UBasicGameComponent
{
	GENERATED_BODY()

public:
	void OnLoadFractionProgressComplete(const TArray<const UPlayerFractionEntity*>& progress);

	//============================================
	//								[ OnBuy ]
private:
	TSharedPtr<FOperationRequest>				OnUnlockItem;
	TSharedPtr<FOperationRequest>				RequestSellItem;

	void OnUnlockItemSuccessfully(const FUnlockItemResponseContainer& response);
	void OnUnlockItemFailed(const FRequestExecuteError& error) const;

public:

	void UnlockItem(const UItemBaseEntity* item, const uint32& amount, const FGuid& recipient);
	void UnlockItem(const FUnlockItemRequestContainer& item);
	FOnUnlockItemSuccessfully OnUnlockItemSuccessfullyHandler;
	FOnItemUnlocked OnItemUnlocked;
	FOnSellItem OnSellItemEvent;
	FOnGetInventoryItem OnGetInventoryItem;

	//============================================
	UFUNCTION(Server, Reliable, WithValidation) void SendRequestSellItem(const FSellItemRequest& request);

public:

	UMarketComponent();

	void CreateGameWidgets() override;
	TSharedPtr<SMarketPlaceWindow> MarketPlaceWindow;

	FORCEINLINE FItemModelInfo GetSelectedItem() const { return SelectedItem; }

	UFUNCTION(Client, Reliable)						void ToggleMarketWindow(const bool InToggle);

protected:

	void OnSelectedItem(const UItemBaseEntity* InItem);
	UFUNCTION(Server, Reliable, WithValidation)		void OnServerSelectedItem(const FItemModelInfo& InItem);

	UFUNCTION(Server, Reliable, WithValidation)		void ServerUnlockItem(const FUnlockItemRequestContainer& item);
	UFUNCTION(Client, Reliable)						void ClientNotifyUnlockItem(const FUnlockItemResponseContainer& response);

	UFUNCTION()										void OnSellItem(const FSellItemResponse& response);
	UFUNCTION()										void OnSellItem_Failed(const FRequestExecuteError& error);

	UFUNCTION(Client, Reliable)						void ClientNotifySellItem(const FSellItemResponse& response);

	UPROPERTY(Replicated)							FItemModelInfo				SelectedItem;

};