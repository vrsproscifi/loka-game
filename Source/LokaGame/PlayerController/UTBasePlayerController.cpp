// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#include "LokaGame.h"
#include "Engine/Console.h"


#include "UnrealNetwork.h"
#include "UTGameViewportClient.h"

#include "UTLocalPlayer.h"

#include "UTGameInstance.h"
#include "ShooterSpectatorPawn.h"


#include "UTGameState.h"
#include "UTHUD.h"
#include "UTTeamInfo.h"
#include "UTGameMode.h"
#include "UTGameUserSettings.h"
#include "UTPlayerState.h"
#include "UTCharacter.h"
#include "UTPlayerController.h"


//===================================]
//		LOKA HUD
#include "Slate/Game/SScoreboard.h"
#include "Slate/Game/SGameTimer.h"
#include "Slate/Game/SPresetSelector.h"
#include "Slate/Game/SGameResults.h"
#include "Slate/Notify/SAboutKiller.h"
#include "Slate/Components/SSniperScope.h"

//===================================]
//		LOKA Inventory

#include "Weapon/ItemWeaponEntity.h"
#include "Achievement/ItemAchievementEntity.h"
#include "CharacterAbility.h"

//===================================]
//		LOKA Other
#include "GameSingleton.h"
#include "InventoryHacks.h"
#include "Game/SGameContainer.h"

AUTBasePlayerController::AUTBasePlayerController(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
	, bRequestShowMenu(false)
	, bSpectatorMouseChangesView(false)
	, MyLastKiller(nullptr)
	, LastCharacter(nullptr)
{
	ChatOverflowTime = 0.0f;
	bOverflowed = false;
	SpamText = NSLOCTEXT("AUTBasePlayerController", "SpamMessage",
		"You must wait a few seconds before sending another message.");
}

void AUTBasePlayerController::Destroyed()
{
	GetWorldTimerManager().ClearAllTimersForObject(this);
	if (MyHUD)
	{
		GetWorldTimerManager().ClearAllTimersForObject(MyHUD);
	}
	if (PlayerCameraManager != nullptr)
	{
		GetWorldTimerManager().ClearAllTimersForObject(PlayerCameraManager);
	}
	if (PlayerInput != nullptr)
	{
		GetWorldTimerManager().ClearAllTimersForObject(PlayerInput);
	}
	Super::Destroyed();
}

void AUTBasePlayerController::GetLifetimeReplicatedProps(TArray<class FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(AUTBasePlayerController, MyLastKiller, COND_OwnerOnly);
}

void AUTBasePlayerController::InitInputSystem()
{
	Super::InitInputSystem();

	// read profile on every level change so we can detect updates
	UUTLocalPlayer* LP = Cast<UUTLocalPlayer>(Player);

#if WITH_PROFILE
	if (LP != nullptr && LP->IsLoggedIn() && LP->GetMcpProfileManager() && LP->GetMcpProfileManager()->GetMcpProfileAs<UUtMcpProfile>(EUtMcpProfile::Profile))
	{
		FClientUrlContext QueryContext = FClientUrlContext::Default; // IMPORTANT to make a copy!
		LP->GetMcpProfileManager()->GetMcpProfileAs<UUtMcpProfile>(EUtMcpProfile::Profile)->ForceQueryProfile(QueryContext);
	}
#endif

	// Let the viewport client know we have connected to a server.
	if (GetWorld()->GetNetMode() == ENetMode::NM_Client)
	{
		if (LP && LP->ViewportClient)
		{
			UUTGameViewportClient* VC = Cast<UUTGameViewportClient>(LP->ViewportClient);
			if (VC)
			{
				VC->ClientConnectedToServer();
			}
		}
	}
}

void AUTBasePlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
}


// LEAVE ME for quick debug commands when we need them.
void AUTBasePlayerController::DebugTest(FString TestCommand)
{
}

void AUTBasePlayerController::ServerDebugTest_Implementation(const FString& TestCommand)
{
}

bool AUTBasePlayerController::ServerDebugTest_Validate(const FString& TestCommand) {return true;}

void AUTBasePlayerController::InitPlayerState()
{
	Super::InitPlayerState();

	if (auto ps = GetPlayerState<AUTPlayerState>())
	{
		ps->ChatDestination = ChatDestinations::Local;

#if WITH_EDITOR
		auto FoundHacks = UGameSingleton::Get()->GetDataAssets<UInventoryHack>();
		if (FoundHacks.IsValidIndex(0) && FoundHacks[0])
		{
			ps->InitializeProfiles(FoundHacks[0]->HackedProfiles);
		}
#endif
	}
}


void AUTBasePlayerController::Talk()
{
	UUTLocalPlayer* LP = Cast<UUTLocalPlayer>(Player);
	if (LP != nullptr && LP->ViewportClient->ViewportConsole != nullptr)
	{
//	LP->ShowQuickChat(ChatDestinations::Local);
	}
}

void AUTBasePlayerController::TeamTalk()
{
	UUTLocalPlayer* LP = Cast<UUTLocalPlayer>(Player);
	if (LP != nullptr && LP->ViewportClient->ViewportConsole != nullptr)
	{
//		LP->ShowQuickChat(ChatDestinations::Team);
	}
}

bool AUTBasePlayerController::AllowTextMessage(const FString& Msg)
{
	const float TIME_PER_MSG = 2.0f;
	const float MAX_OVERFLOW = 4.0f;

	if (GetNetMode() == NM_Standalone || (GetNetMode() == NM_ListenServer && Role == ROLE_Authority))
	{
		return true;
	}

	ChatOverflowTime = FMath::Max(ChatOverflowTime, GetWorld()->RealTimeSeconds);

	//When overflowed, wait till the time is back to 0
	if (bOverflowed && ChatOverflowTime > GetWorld()->RealTimeSeconds)
	{
		return false;
	}
	bOverflowed = false;

	//Accumulate time for each message, double for a duplicate message
	ChatOverflowTime += (LastChatMessage == Msg) ? TIME_PER_MSG * 2 : TIME_PER_MSG;
	LastChatMessage = Msg;

	if (ChatOverflowTime - GetWorld()->RealTimeSeconds <= MAX_OVERFLOW)
	{
		return true;
	}

	bOverflowed = true;
	return false;
}

//void AUTBasePlayerController::Say(FString Message)
//{
//	// clamp message length; aside from troll prevention this is needed for networking reasons
//	Message = Message.Left(MAX_CHAT_TEXT_SIZE);
//	if (AllowTextMessage(Message))
//	{
//		ServerSay(Message, false);
//	}
//	else
//	{
//		//Display spam message to the player
//		ClientSay_Implementation(nullptr, SpamText.ToString(), ChatDestinations::System);
//	}
//}
//
//void AUTBasePlayerController::TeamSay(FString Message)
//{
//	// clamp message length; aside from troll prevention this is needed for networking reasons
//	Message = Message.Left(MAX_CHAT_TEXT_SIZE);
//	if (AllowTextMessage(Message))
//	{
//		ServerSay(Message, true);
//	}
//	else
//	{
//		//Display spam message to the player
//		ClientSay_Implementation(nullptr, SpamText.ToString(), ChatDestinations::System);
//	}
//}
//
//bool AUTBasePlayerController::ServerSay_Validate(const FString& Message, bool bTeamMessage) { return true; }
//
//void AUTBasePlayerController::ServerSay_Implementation(const FString& Message, bool bTeamMessage)
//{
//	if (AllowTextMessage(Message) && PlayerState != nullptr)
//	{
//		// Look to see if this message is a direct message to a given player.
//
//		if (Message.Left(1) == TEXT("@"))
//		{
//			// Remove the @
//			FString TrimmedMessage = Message.Right(Message.Len()-1);
//			DirectSay(TrimmedMessage);
//			return;
//		}
//
//		bool bSpectatorMsg = PlayerState->bOnlySpectator;
//
//		for (FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
//		{
//			AUTBasePlayerController* UTPC = Cast<AUTBasePlayerController>(*Iterator);
//			if (UTPC != nullptr)
//			{
//				if (!bTeamMessage || UTPC->GetTeamNum() == GetTeamNum())
//				{
//					TSharedPtr<const FUniqueNetId> Id = PlayerState->UniqueId.GetUniqueNetId();
//					bool bIsMuted =  UTPC->MuteList.VoiceMuteList.IndexOfByPredicate(FUniqueNetIdMatcher(*Id)) != INDEX_NONE;
//
//					// Dont send spectator chat to players
//					if (UTPC->GetPlayerState() != nullptr && (!bSpectatorMsg || UTPC->GetPlayerState()->bOnlySpectator) && !bIsMuted)
//					{
//						UTPC->ClientSay(UTPlayerState, Message, (bTeamMessage ? ChatDestinations::Team : ChatDestinations::Local));
//					}
//				}
//			}
//		}
//	}
//}

void AUTBasePlayerController::DirectSay(const FString& Message)
{
	// Figure out who we are talking too...

	AUTGameState* UTGameState = GetWorld()->GetGameState<AUTGameState>();
	if (UTGameState)
	{
		FString TargetPlayerName;
		FString FinalMessage = Message;

		bool bSent = false;

		// Look for a local player controller to send to...
		for (FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
		{
			AUTBasePlayerController* UTPC = Cast<AUTBasePlayerController>(*Iterator);
			if (UTPC != nullptr && UTPC->GetPlayerState() != nullptr)
			{
				AUTPlayerState* TargetPlayerState = Cast<AUTPlayerState>(UTPC->GetPlayerState());
				if (TargetPlayerState != nullptr)
				{
					TargetPlayerName = TargetPlayerState->GetPlayerName();

					if (Message.Left(TargetPlayerName.Len()).Equals(TargetPlayerName, ESearchCase::IgnoreCase))
					{
						FinalMessage = FinalMessage.Right(FinalMessage.Len() - TargetPlayerName.Len()).TrimStart();
						bSent = true;
		
						TSharedPtr<const FUniqueNetId> Id = UTPC->GetPlayerState()->UniqueId.GetUniqueNetId();
						bool bIsMuted = Id.IsValid() && IsPlayerMuted(Id.ToSharedRef().Get());

						if (!bIsMuted)
						{
							//UTPC->ClientSay(UTPlayerState, FinalMessage, ChatDestinations::Whisper);
						}
						FinalMessage = FString::Printf(TEXT("to %s \"%s\""), *TargetPlayerName, *FinalMessage);
						break;
					}
				}
			}
		}

		// If we haven't sent the message. Look to see if we need to forward this message elsewhere
		if (!bSent)
		{
			bSent = ForwardDirectSay(GetPlayerState<AUTPlayerState>(), FinalMessage);
		}

		if (bSent)
		{
			// Make sure I see that I sent it..
			//ClientSay(UTPlayerState, FinalMessage, ChatDestinations::Whisper);
		}
	}
}

bool AUTBasePlayerController::ForwardDirectSay(AUTPlayerState* SenderPlayerState, FString& FinalMessage)
{
	return false;
}


//void AUTBasePlayerController::ClientSay_Implementation(AUTPlayerState* Speaker, const FString& Message, FName Destination)
//{
//	FClientReceiveData ClientData;
//	ClientData.LocalPC = this;
//	ClientData.MessageIndex = (Destination == ChatDestinations::Team) ? 1 : 0;
//	ClientData.RelatedPlayerState_1 = Speaker;
//	ClientData.MessageString = Message;
//
//	UUTChatMessage::StaticClass()->GetDefaultObject<UUTChatMessage>()->ClientReceiveChat(ClientData, Destination);
//}

uint8 AUTBasePlayerController::GetTeamNum() const
{
	if (const auto ps = GetPlayerState<AUTPlayerState>())
	{
		if(const auto team = GetValidObject(ps->Team))
		{
			return team->TeamIndex;
		}
	}
	return 255;
}


UUTLocalPlayer* AUTBasePlayerController::GetUTLocalPlayer()
{
	return Cast<UUTLocalPlayer>(Player);
}



void AUTBasePlayerController::HandleNetworkFailureMessage(enum ENetworkFailure::Type FailureType, const FString& ErrorString)
{
}

#if !UE_SERVER
void AUTBasePlayerController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

#endif


//void AUTBasePlayerController::LobbySay(FString Message)
//{
//	// clamp message length; aside from troll prevention this is needed for networking reasons
//	Message = Message.Left(MAX_CHAT_TEXT_SIZE);
//	if (AllowTextMessage(Message))
//	{
//		ServerLobbySay(Message);
//	}
//	else
//	{
//		//Display spam message to the player
//		ClientSay_Implementation(nullptr, SpamText.ToString(), ChatDestinations::System);
//	}
//}
//
//bool AUTBasePlayerController::ServerLobbySay_Validate(const FString& Message) { return true; }
//void AUTBasePlayerController::ServerLobbySay_Implementation(const FString& Message)
//{
//	AUTGameMode * GameMode = GetWorld()->GetAuthGameMode<AUTGameMode>();
//	if (GameMode && GameMode->IsGameInstanceServer()  && AllowTextMessage(Message) && PlayerState != nullptr)
//	{
//		GameMode->SendLobbyMessage(Message, Cast<AUTPlayerState>(PlayerState));
//	}
//}

void AUTBasePlayerController::ClientEnableNetworkVoice_Implementation(bool bEnable)
{

}

void AUTBasePlayerController::StartVOIPTalking()
{
	UUTGameUserSettings* GUSettings = Cast<UUTGameUserSettings>(UGameUserSettings::GetGameUserSettings());
	if (GUSettings && GUSettings->bPushToTalk)
	{
		ToggleSpeaking(true);
	}
}

void AUTBasePlayerController::StopVOIPTalking()
{
	UUTGameUserSettings* GUSettings = Cast<UUTGameUserSettings>(UGameUserSettings::GetGameUserSettings());
	if (GUSettings && GUSettings->bPushToTalk)
	{
		ToggleSpeaking(false);
	}
}

void AUTBasePlayerController::SetPlayer(UPlayer* InPlayer)
{
	Super::SetPlayer(InPlayer);

	SetInputMode(FInputModeGameOnly());

	UGameViewportClient* GameViewportClient = GetWorld()->GetGameViewport();
	if (GameViewportClient)
	{
		GameViewportClient->SetCaptureMouseOnClick(EMouseCaptureMode::CapturePermanently);
	}
}

void AUTBasePlayerController::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);

	if (InPawn)
	{
		LastCharacter = InPawn;
	}

	if (GetNetMode() == NM_Client || GetNetMode() == NM_Standalone || GetNetMode() == NM_ListenServer)
	{
		if (UI_GameHUD.IsValid() || TryCreateHUD())
		{
			if (auto MyCharacter = Cast<AUTCharacter>(InPawn))
			{
				UI_GameHUD->SetCharacter(MyCharacter);
				UI_GameHUD->ToggleWidget(true);
			}
			else
			{
				UI_GameHUD->ToggleWidget(false);
			}
		}

		if (UI_Scoreboard.IsValid() || TryCreateScoreboard())
		{
			ToggleGameTimer(Cast<AUTCharacter>(InPawn) != nullptr);
		}	
	}
}

void AUTBasePlayerController::SetSpectatorPawn(class ASpectatorPawn* NewSpectatorPawn)
{
	Super::SetSpectatorPawn(NewSpectatorPawn);

	if (GetNetMode() != NM_DedicatedServer)
	{
		auto MyGameState = GetWorldGameState<AUTGameState>(GetWorld());
		if ((Slate_PresetSelector.IsValid() || TryCreatePresetSelector()) && MyGameState && MyGameState->GetMatchState() != MatchState::WaitingPostMatch)
		{
			Slate_PresetSelector->ToggleWidget(NewSpectatorPawn != nullptr);
		}
	}
}

void AUTBasePlayerController::OnScrollProfile(const bool IsNext) const
{
	auto MyPlayerState = Cast<ABasePlayerState>(PlayerState);
	if (MyPlayerState && GetCharacter() == nullptr)
	{
		const auto TargetProfile = IsNext ? MyPlayerState->GetCurrentProfile() + 1 : MyPlayerState->GetCurrentProfile() - 1;
		MyPlayerState->ChangeProfile(TargetProfile);
	}
}

bool AUTBasePlayerController::TryCreateHUD()
{
	if (UI_GameHUD.IsValid())
	{
		return false;
	}

	SAssignNew(UI_GameHUD, SGameHUD).Controller(this).IsUnion(true).OnMessageSend_Lambda([&](const FText &Message, const ESendMessageTo SendTo) {
		if (Message.ToString().Len())
		{
			ServerChatEvent(Message.ToString(), SendTo);
		}
	});

	if (auto MyGameViewport = GetGameViewportClient())
	{
		MyGameViewport->AddUsableViewportWidgetContent(UI_GameHUD.ToSharedRef(), 1);		
	}

	UI_GameHUD->SetIsAutoHideChat(true);
	UI_GameHUD->SetIsAutoClearInput(true);

	return true;
}

bool AUTBasePlayerController::TryCreateScoreboard()
{
	if (UI_Scoreboard.IsValid())
	{
		return false;
	}

	if (GetWorld()->GetGameState<AUTGameState>() && Cast<AUTPlayerState>(PlayerState))
	{
		PLACE_WIDGET(SAssignNew(UI_Scoreboard, SScoreboard).GameState(GetWorld()->GetGameState<AUTGameState>()).LocalPlayerState(Cast<AUTPlayerState>(PlayerState)), 2);
		return true;
	}

	return false;
}

bool AUTBasePlayerController::TryCreatePresetSelector()
{
	if (Slate_PresetSelector.IsValid())
	{
		return false;
	}

	PLACE_WIDGET(SAssignNew(Slate_PresetSelector, SPresetSelector)
						.GameState_Lambda([&]() 
						{	
							return GetWorld()->GetGameState<AUTGameState>(); 
						})
						 .InventoryComponent_Lambda([&]()
						{
							if (auto MyPlayerState = Cast<ABasePlayerState>(PlayerState))
							{
								return MyPlayerState->GetInventoryComponent();
							}

							return (UInventoryComponent*)nullptr;
						})
						.ProfileComponent_Lambda([&]()
						{
							if (auto MyPlayerState = Cast<ABasePlayerState>(PlayerState))
							{
								return MyPlayerState->GetProfileComponent();
							}

							return (UProfileComponent*)nullptr;
						})
						.Selected_Lambda([&]()
						{
							if (auto MyPlayerState = Cast<ABasePlayerState>(PlayerState))
							{
								return static_cast<int32>(MyPlayerState->GetCurrentProfile());
							}

							return 0;
						}), 0);

	return true;
}

bool AUTBasePlayerController::TryCreateGameMenu()
{
	auto MyGameViewport = GetGameViewportClient();
	if (MyGameViewport && Slate_GameContainer.IsValid() == false)
	{
		SAssignNew(Slate_GameContainer, SGameContainer).OnRequestLeave_Lambda([&] () -> void
		{
			ServerRequestLeave(true);
		});

		MyGameViewport->AddUsableViewportWidgetContent_AlwaysVisible(Slate_GameContainer.ToSharedRef(), 100);

		return true;
	}

	return false;
}

bool AUTBasePlayerController::TryCreateAboutKiller()
{
	if (UI_AboutKiller.IsValid())
	{
		return false;
	}

	PLACE_WIDGET(SAssignNew(UI_AboutKiller, SAboutKiller), 5);
	return true;
}

bool AUTBasePlayerController::TryCreateSniperScopeWidget()
{
	if (GEngine && GEngine->GameViewport && !Slate_SniperScope.IsValid())
	{
		if (auto UTViewport = Cast<UUTGameViewportClient>(GEngine->GameViewport))
		{
			UTViewport->AddViewportWidgetContent_AlwaysVisible(SAssignNew(Slate_SniperScope, SSniperScope), 0);
			return true;
		}
	}

	return false;
}

void AUTBasePlayerController::ToggleGameHUD(const bool Toggle)
{
	if (UI_GameHUD.IsValid())
	{
		UI_GameHUD->ToggleWidget(Toggle);
	}
}

void AUTBasePlayerController::ToggleScoreboard(const bool Toggle)
{
	if (UI_Scoreboard.IsValid())
	{
		UI_Scoreboard->ToggleTable(Toggle);
	}
}

void AUTBasePlayerController::ToggleGameTimer(const bool Toggle)
{
	if (UI_Scoreboard.IsValid() && UI_Scoreboard->GameTimer.IsValid())
	{
		UI_Scoreboard->GameTimer->ToggleWidget(Toggle);
	}
}

void AUTBasePlayerController::ToggleGameMenu(const bool Toggle)
{
	if (Slate_GameContainer.IsValid() || TryCreateGameMenu())
	{
		Slate_GameContainer->ToggleWidget(Toggle);
	}
}

void AUTBasePlayerController::OnToggleInGameMenu()
{
	if (Slate_GameContainer.IsValid() || TryCreateGameMenu())
	{
		Slate_GameContainer->ToggleWidget(!Slate_GameContainer->IsInInteractiveMode());
	}	
}

void AUTBasePlayerController::ClientToggleGameHUD_Implementation(const bool Toggle)
{
	ToggleGameHUD(Toggle);
}

void AUTBasePlayerController::ClientToggleScoreboard_Implementation(const bool Toggle)
{
	ToggleScoreboard(Toggle);
}

void AUTBasePlayerController::ClientSetLiveBarFlashing_Implementation(const bool isHealth)
{
	if (UI_GameHUD.IsValid() || TryCreateHUD())
	{
		UI_GameHUD->SetLiveBarFleshing(isHealth);
	}
}

void AUTBasePlayerController::ClientShowTextTimed_Implementation(const int32 Time, const ETimedTextMessage Message)
{
	if (UI_GameHUD.IsValid() || TryCreateHUD())
	{
		FText TargetMessage;

		switch (Message)
		{
			case ETimedTextMessage::WaitingPlayers: TargetMessage = NSLOCTEXT("SHUD", "WaitingPlayers", "Waiting players: "); break;
			case ETimedTextMessage::WaitingStart: TargetMessage = NSLOCTEXT("SHUD", "WaitingStart", "Start match in: "); break;
			case ETimedTextMessage::WaitingEnemy: TargetMessage = NSLOCTEXT("SHUD", "WaitingEnemy", "Waiting enemy, end match in: "); break;
			default: TargetMessage = NSLOCTEXT("SHUD", "RespawnIn", "Respawn in: "); break;
		}

		UI_GameHUD->SetBottomText(TargetMessage, true);
		UI_GameHUD->SetBottomTextTime(Time);
	}
}

void AUTBasePlayerController::ClientShowActionMessage_Implementation(const EActionMessage MessageID, const int32& Score)
{
	if (UI_GameHUD.IsValid() || TryCreateHUD())
	{
		UI_GameHUD->AppendAction(MessageID, Score);

		if (MessageID == EActionMessage::Death && Slate_SniperScope.IsValid())
		{
			Slate_SniperScope->SetVisibility(EVisibility::Hidden);
		}
	}
}

void AUTBasePlayerController::ClientShowAchievement_Implementation(const uint8 AchievementId)
{
	if (UI_GameHUD.IsValid() && AchievementId < ItemAchievementTypeId::End)
	{
		auto TargetAchievement = UGameSingleton::Get()->FindAchievement(static_cast<ItemAchievementTypeId::Type>(AchievementId));
		if (TargetAchievement)
		{
			UI_GameHUD->GetMedalsBar()->AddMedal(TargetAchievement->GetIcon(), TargetAchievement->GetDescription().Name, &TargetAchievement->Sound);
		}
	}
}

void AUTBasePlayerController::ToggleChatWindow()
{
	if (UI_GameHUD.IsValid() && !UI_GameHUD->IsChatOpened())
	{
		UI_GameHUD->ToggleChat(true);
	}
}

void AUTBasePlayerController::ClientChatEvent_Implementation(const FString &Name, const FString &Message, const FLinearColor &Color, const ESendMessageTo SendTo)
{
	if (UI_GameHUD.IsValid())
	{
		UI_GameHUD->AppendMessage(FText::FromString(Name), Color, FText::FromString(Message), SendTo);
	}
}

bool AUTBasePlayerController::ServerChatEvent_Validate(const FString &Message, const ESendMessageTo SendTo)
{
	return true;
}

void AUTBasePlayerController::ServerChatEvent_Implementation(const FString &Message, const ESendMessageTo SendTo)
{
	auto MyPlayerState = Cast<AUTPlayerState>(PlayerState);
	if (MyPlayerState)
	{
		for (auto It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
		{
			AUTPlayerController* Controller = Cast<AUTPlayerController>(*It);
			if (Controller)
			{
				if (auto OtherState = Cast<AUTPlayerState>(Controller->GetPlayerState()))
				{
					Controller->ClientChatEvent(MyPlayerState->GetPlayerName(), Message, (OtherState->GetTeamNum() == MyPlayerState->GetTeamNum()) ? FColor::Green : FColor::Red, SendTo);
				}
			}
		}
	}
}

void AUTBasePlayerController::ClientAppendKillRow_Implementation(const AUTPlayerState* Killer, const AUTPlayerState* Target, const uint8 weaponCategory, const uint8 weaponModel, const bool IsLocal)
{
	if (UI_Scoreboard.IsValid())
	{
		if (weaponCategory != ECategoryTypeId::End)
		{
			auto localFoindInstance = (weaponCategory == ECategoryTypeId::Weapon) ? UGameSingleton::Get()->FindItem(weaponModel, ECategoryTypeId::Weapon) : UGameSingleton::Get()->FindItem(weaponModel, ECategoryTypeId::Grenade);
			if (localFoindInstance)
			{
				UI_Scoreboard->AppendKillRow(Killer, Target, localFoindInstance->GetIcon());

				if (IsLocal && (UI_AboutKiller.IsValid() || TryCreateAboutKiller()))
				{
					UI_AboutKiller->ShowAboutKiller(FText::FromString(Killer->GetPlayerName()), localFoindInstance->GetIcon());
				}
			}
		}
		else
		{
			UI_Scoreboard->AppendKillRow(Killer, Target, new FSlateNoResource());
		}
	}
}

void AUTBasePlayerController::OnSelectPreset(const int32 PressetId)
{
	auto MyPlayerState = Cast<ABasePlayerState>(PlayerState);
	if (MyPlayerState && GetCharacter() == nullptr)
	{
		MyPlayerState->ChangeProfile(PressetId);
	}
}

void AUTBasePlayerController::OnRep_MyLastKiller()
{
	auto MySpectatorPawn = Cast<AShooterSpectatorPawn>(GetSpectatorPawn());
	if (MySpectatorPawn && MyLastKiller && LastCharacter)
	{
		if (auto MyLastChar = Cast<AUTCharacter>(LastCharacter))
		{
			MyLastChar->GetMesh()->SetCollisionResponseToChannel(ECC_Camera, ECR_Ignore);
			MyLastChar->GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Camera, ECR_Ignore);
		}

		MySpectatorPawn->InitializeTarget();
	}
}

void AUTBasePlayerController::ClientShowMatchResults_Implementation(const int32 &MatchWinner)
{
	ToggleGameTimer(false);
	ToggleScoreboard(false);
	ToggleGameHUD(false);
	ToggleGameMenu(false);

	if (Slate_SniperScope.IsValid())
	{
		Slate_SniperScope->SetVisibility(EVisibility::Hidden);
	}

	if (Slate_PresetSelector.IsValid())
	{
		Slate_PresetSelector->ToggleWidget(false);
	}

	if (UI_GameHUD.IsValid())
	{
		UI_GameHUD->SetBottomTextTime(0);
	}

	auto MyPlayerState = Cast<AUTPlayerState>(PlayerState);
	if (!UI_GameResults.IsValid() && MyPlayerState)
	{
		SAssignNew(UI_GameResults, SGameResults)
			.GameState(GetWorld()->GetGameState<AUTGameState>())
			.PlayerState(MyPlayerState)
			.MatchWinner(MatchWinner)
			.BackgroundArguments(SGeneralBackground::FArguments().OnGeneralButtonClick_Lambda([&](const EButtonType::Type Button) {
				if (Button == EButtonType::Type::Close)
				{
#if !WITH_EDITOR
					ServerRequestLeave();
#else
					ConsoleCommand("quit");
#endif
				}
			}));

		if (GEngine && GEngine->GameViewport)
		{
			if (auto UTViewport = Cast<UUTGameViewportClient>(GEngine->GameViewport))
			{
				UTViewport->AddUsableViewportWidgetContent_AlwaysVisible(UI_GameResults.ToSharedRef(), 0);
			}
		}

		UI_GameResults->SetShowAnimation(EGBAnimation::Type::UpFade);
		UI_GameResults->SetHideAnimation(EGBAnimation::Type::ZoomIn);

		FTimerHandle _th;
		GetWorldTimerManager().SetTimer(_th, FTimerDelegate::CreateLambda([&, s = MyPlayerState]() 
		{
			for (auto localCurrency : s->Cash)
			{
				FString name = "";
				if (localCurrency.Currency == EGameCurrency::Money) name = "Money";
				else if (localCurrency.Currency == EGameCurrency::Donate) name = "Donate";
				else if (localCurrency.Currency == EGameCurrency::Coin) name = "Coin";

				if (localCurrency.Amount != 0)
				{
					UI_GameResults->AddAchievement(name, localCurrency.Amount + s->TakePremiumCash(localCurrency.Currency)->Amount);
				}
			}

			if (s->Experience != 0) UI_GameResults->AddAchievement("Exp", s->Experience + s->PremiumExperience);

			TMap<ItemAchievementTypeId::Type, int32> AchievementsMap;
			for (auto &a : s->Achievements)
			{
				auto &Value = AchievementsMap.FindOrAdd(a);
				++Value;
			}

			for (auto &a : AchievementsMap)
			{
				if (auto TargetAchievement = UGameSingleton::Get()->FindAchievement(a.Key))
				{
					if (a.Key == ItemAchievementTypeId::Medal_Match_Best_Hero
						|| a.Key == ItemAchievementTypeId::Medal_Match_Best_Killer
						|| a.Key == ItemAchievementTypeId::Medal_Match_Best_Player
						|| a.Key == ItemAchievementTypeId::Medal_Match_Best_Shooter
						|| a.Key == ItemAchievementTypeId::Medal_Match_FirstBlood)
					{
						UI_GameResults->AddAchievementCustom(TargetAchievement->GetIcon(), TargetAchievement->GetDescription().Name);
					}
					else
					{
						UI_GameResults->AddAchievementCustom(TargetAchievement->GetIcon(), FText::Format(FText::FromString("{Name} x{Num}"), TargetAchievement->GetDescription().Name, FText::AsNumber(a.Value)));
					}
				}
			}

		}), 2.0f, false);

		UI_GameResults->ToggleWidget(true);
	}
}

void AUTBasePlayerController::MakeLastAliveShow_Implementation()
{
	if (auto HUDC = Cast<AUTHUD>(GetHUD()))
	{
		HUDC->MakeLastAlivePositions();
	}
}