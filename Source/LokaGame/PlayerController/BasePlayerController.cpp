// VRSPRO

#include "LokaGame.h"
#include "BasePlayerController.h"
#include "Sound/SoundNodeLocalPlayer.h"
#include "AudioThread.h"
#include "BasePlayerState.h"
#include "BaseCharacter.h"
#include "Transport/TransportBase.h"
#include <OnlineSubsystemTypes.h>
#include "AnalogCursor.h"
#include "OnlineGameMode.h"
#include "Messages/BaseLocalMessage.h"
#include "BaseHUD.h"
#include "UTGameViewportClient.h"

void ABasePlayerController::TravelToLobbyRoom()
{
	ClientTravel(TEXT("/Game/1LOKAgame/Maps/LobbyRoom"), TRAVEL_Absolute);
}

ABasePlayerController::ABasePlayerController(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	InputProccesor = MakeShareable(new FAnalogCursor());

	//bAutoManageActiveCameraTarget = false;
}

void ABasePlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAction("UseAny", IE_Pressed, this, &ABasePlayerController::OnUseAny_Helper<true>).bConsumeInput = false;
	InputComponent->BindAction("UseAny", IE_Released, this, &ABasePlayerController::OnUseAny_Helper<false>).bConsumeInput = false;

	InputComponent->BindAction("InGameESC", IE_Pressed, this, &ABasePlayerController::OnEscape);
	InputComponent->BindAction("ToggleHUD", IE_Pressed, this, &ABasePlayerController::OnToggleVisibilityUI).bConsumeInput = false;
}

void ABasePlayerController::OnUseAny(const bool InValue)
{
	if (auto MyCharacter = GetBaseCharacter())
	{
		// Call both client & server for best implementation;
		if (GetNetMode() == NM_Client)
		{
			MyCharacter->OnUseAny(InValue);
		}
		MyCharacter->OnUseAny_Implementation(InValue);
	}
}

void ABasePlayerController::OnToggleVisibilityUI()
{
	auto MyViewportClient = GetGameViewportClient();
	auto localMyHud = Cast<ABaseHUD>(GetHUD());

	if (MyViewportClient && localMyHud)
	{
		localMyHud->bShowHUD = !localMyHud->bShowHUD;
		MyViewportClient->ToggleViewportWidgets(localMyHud->bShowHUD);
	}
}

void ABasePlayerController::Possess(APawn* aPawn)
{
	Super::Possess(aPawn);

	if (auto MyCharacter = Cast<ABaseCharacter>(aPawn))
	{
		ControlledCharacter = MyCharacter;

		if (auto MyPlayerState = Cast<ABasePlayerState>(PlayerState))
		{
			MyPlayerState->InitializeSelectedProfile();
		}
	}
	else if (auto MyTransport = Cast<ATransportBase>(aPawn))
	{
		ControlledTransport = MyTransport;
	}
}

// From ShooterGame new project
void ABasePlayerController::TickActor(float DeltaTime, enum ELevelTick TickType, FActorTickFunction& ThisTickFunction)
{
	Super::TickActor(DeltaTime, TickType, ThisTickFunction);

	const bool bLocallyControlled = IsLocalController();
	const uint32 UniqueID = GetUniqueID();
	FAudioThread::RunCommandOnAudioThread([UniqueID, bLocallyControlled]() {
		USoundNodeLocalPlayer::GetLocallyControlledActorCache().Add(UniqueID, bLocallyControlled);
	});

#if !UE_SERVER
	static bool IsGamepad;
	if (FSlateApplication::Get().GetPlatformApplication()->IsGamepadAttached() && bShowMouseCursor)
	{
		if (IsGamepad == false)
		{
			IsGamepad = true;			
			FSlateApplication::Get().RegisterInputPreProcessor(InputProccesor);
		}
	}
	else if(IsGamepad && bShowMouseCursor == false)
	{
		IsGamepad = false;
		//FSlateApplication::Get().UnregisterAllInputPreProcessors();
	}
#endif

#if WITH_EDITOR
	if (GetPawnOrSpectator())
	{
		FString Prefix;
		switch (GetNetMode())
		{
		case NM_Client:
			Prefix = TEXT("[Client]");
			break;
		case NM_DedicatedServer:
		case NM_ListenServer:
			Prefix = TEXT("[Server]");
			break;
		case NM_Standalone:
			break;
		}

		uint8 MessageId = 10;
		if (Prefix.IsEmpty() == false)
		{
			MessageId += GetNetMode() * 2;
		}

		GEngine->AddOnScreenDebugMessage(MessageId, 1, FColorList::LimeGreen, *FString::Printf(TEXT("%sPawn Location: %s"), *Prefix, *GetPawnOrSpectator()->GetActorLocation().ToString()));
		FVector ViewLocation;
		FRotator ViewRotation;
		GetPlayerViewPoint(ViewLocation, ViewRotation);
		GEngine->AddOnScreenDebugMessage(MessageId + 1, 1, FColorList::LightWood, *FString::Printf(TEXT("%sView Location: %s"), *Prefix, *ViewLocation.ToString()));
	}
#endif
};
// From ShooterGame new project
void ABasePlayerController::BeginDestroy()
{
	Super::BeginDestroy();

	//FSlateApplication::Get().UnregisterAllInputPreProcessors();

	if (!GExitPurge)
	{
		const uint32 UniqueID = GetUniqueID();
		FAudioThread::RunCommandOnAudioThread([UniqueID]() {
			USoundNodeLocalPlayer::GetLocallyControlledActorCache().Remove(UniqueID);
		});
	}
}

void ABasePlayerController::PlayerTravel_Implementation(const FString& URL, const FString& Token)
{
	auto SharedToken = MakeShareable(new FUniqueNetIdString(Token, NULL_SUBSYSTEM));
	if(auto lp = GetValidObject(GetLocalPlayer()))
	{
		lp->SetCachedUniqueNetId(SharedToken);
		ClientTravel(URL, TRAVEL_Absolute);
	}
}

void ABasePlayerController::ReceiveLocalizaedMessage_Implementation(TSubclassOf<UBaseLocalMessage> InClass, FBaseLocalMessageData InData)
{
	InData.LocalController = this;
	InData.ReInitOptionalData();
	InClass->GetDefaultObject<UBaseLocalMessage>()->OnReceive(InData);
}

bool ABasePlayerController::ServerRequestLeave_Validate(const bool IsLeaveGame)
{
	return true;
}

void ABasePlayerController::ServerRequestLeave_Implementation(const bool IsLeaveGame)
{
	if (const auto MyGameMode = GetWorld()->GetAuthGameMode<AOnlineGameMode>())
	{
		if (MyGameMode->GetMatchState() != MatchState::WaitingToStart)
		{
			const auto MyState = GetValidObject<ABasePlayerState, APlayerState>(PlayerState);
			if (IsLeaveGame && MyState)
			{
				#if UE_SERVER
				MyGameMode->SendLeaveSessionRequest(MyState->MemberId);
				#endif
			}

			if (auto session = GetValidObject(MyGameMode->GameSession))
			{
				session->KickPlayer(this, FText::FromString("RequestLeave"));
			}
			else
			{
				TravelToLobbyRoom();
			}
		}
	}
}

void ABasePlayerController::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ABasePlayerController, ControlledCharacter);
	DOREPLIFETIME(ABasePlayerController, ControlledTransport);
}
