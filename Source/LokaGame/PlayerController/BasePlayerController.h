// VRSPRO

#pragma once
#include "GameFramework/PlayerController.h"
#include "Messages/BaseLocalMessage.h"
#include "BasePlayerController.generated.h"

class ATransportBase;
class ABasePlayerCharacter;
class ABaseCharacter;
class FAnalogCursor;

UCLASS(MinimalAPI)
class ABasePlayerController 
	: public APlayerController
{
	GENERATED_BODY()

public:
	void TravelToLobbyRoom();

public:
	ABasePlayerController(const FObjectInitializer& ObjectInitializer);

	virtual void SetupInputComponent() override;

	void OnUseAny(const bool InValue);

	template<bool Value>
	void OnUseAny_Helper() { OnUseAny(Value); }

	UFUNCTION(BlueprintCallable, Category = Input)
	virtual void OnEscape() {}

	UFUNCTION(BlueprintCallable, Category = Input)
	virtual void OnToggleVisibilityUI();

	virtual void Possess(APawn* aPawn) override;

protected:
	
	virtual void TickActor(float DeltaTime, enum ELevelTick TickType, FActorTickFunction& ThisTickFunction) override;
	virtual void BeginDestroy() override;

	TSharedPtr<FAnalogCursor> InputProccesor;

public:
	UFUNCTION(Reliable, Client)  
	virtual void PlayerTravel(const FString& URL, const FString& Token);
	virtual void PlayerTravel_Implementation(const FString& URL, const FString& Token);

	template<typename T = ABaseCharacter>
	T* GetBaseCharacter() const
	{
		return GetValidObject<T, ACharacter>(GetCharacter());
	}

	template<typename T = ABasePlayerCharacter>
	T* GetBasePlayerCharacter() const
	{
		return GetValidObject<T, ACharacter>(GetCharacter());
	}

	template<typename T = APlayerState>
	T* GetPlayerState()
	{
		return GetValidObject<T, APlayerState>(PlayerState);
	}

	template<typename T = APlayerState>
	T* GetPlayerState() const
	{
		return GetValidObject<T, APlayerState>(PlayerState);
	}

	UFUNCTION(reliable, server, WithValidation)
	void ServerRequestLeave(const bool IsLeaveGame = false);

	UFUNCTION(Reliable, Client)
	void ReceiveLocalizaedMessage(TSubclassOf<UBaseLocalMessage> InClass, FBaseLocalMessageData InData);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Character)
	ABaseCharacter* GetControlledCharacter() const { return ControlledCharacter; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Transport)
	ATransportBase* GetControlledTransport() const { return ControlledTransport; }

protected:

	UPROPERTY(BlueprintReadWrite, Replicated, Category = Character)		ABaseCharacter*		ControlledCharacter;
	UPROPERTY(BlueprintReadWrite, Replicated, Category = Transport)		ATransportBase*		ControlledTransport;
};
