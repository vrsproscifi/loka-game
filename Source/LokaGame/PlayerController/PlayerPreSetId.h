#pragma once
#include "PlayerPreSetId.generated.h"

UENUM()
namespace EPlayerPreSetId
{
	enum Type
	{
		One,
		Two,
		Three,
		Four,
		Five,
		End
	};
}