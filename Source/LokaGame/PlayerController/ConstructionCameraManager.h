// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Camera/PlayerCameraManager.h"
#include "ConstructionCameraManager.generated.h"

/**
 * 
 */
UCLASS()
class LOKAGAME_API AConstructionCameraManager : public APlayerCameraManager
{
	GENERATED_BODY()
	
public:
	
	AConstructionCameraManager();
	
};
