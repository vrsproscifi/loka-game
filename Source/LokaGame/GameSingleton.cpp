#include "LokaGame.h"
#include "GameSingleton.h"
#include "Item/ItemBaseEntity.h"
#include "Fraction/FractionEntity.h"
#include "Models/ListOfInstance.h"
#include "Achievement/ItemAchievementEntity.h"
#include "RequestManager/RequestManager.h"
#include "Components/IdentityComponent.h"
#include "IAssetRegistry.h"
#include "AssetRegistryModule.h"
#include "TutorialBase.h"
#if WITH_EDITOR
#include "MessageLog.h"
#endif

#include "LKBlueprintFunctionLibrary.h"
#include "WorldPreviewItem.h"
#include "SlateMaterialBrush.h"

#if WITH_EDITOR
#include "AssetToolsModule.h"
#include "Factories/MaterialInstanceConstantFactoryNew.h"
#include "ContentBrowserModule.h"
#include "IContentBrowserSingleton.h"
#include "LokaEditorSettings.h"
#include "KismetEditorUtilities.h"
#include "FileHelpers.h"
#endif

UGameSingleton::UGameSingleton()
{
	ListOfProducts = FOperationRequest::Factory(this, FServerHost::MasterClient, "Store/ListOfInstance");
	ListOfProducts->BindObject<FListOfInstance>(200).AddUObject(this, &UGameSingleton::OnListOfProductsSuccessfully);
	ListOfProducts->OnFailedResponse.BindUObject(this, &UGameSingleton::OnListOfProductsFailed);
}

template<typename TObject>
TObject* LoadGameAsset(UObject* context, const FAssetData& asset)
{
	//=============================================
	static FName NAME_GeneratedClass(TEXT("GeneratedClass"));
	auto ClassPath = asset.TagsAndValues.Find(NAME_GeneratedClass);

	//=============================================	
	UClass* ObjectClass = LoadObject<UClass>(context, **ClassPath);
	if (ObjectClass && ObjectClass->IsValidLowLevel())
	{
		TObject* ObjectInstance = NewObject<TObject>(context, ObjectClass);
		if(ObjectInstance && ObjectInstance->IsValidLowLevel())
		{
			return ObjectInstance;
		}
	}
	return nullptr;
}


template<typename TObject>
TObject* CreateGameAsset(UObject* context, UClass* ObjectClass)
{
	//=============================================	
	if (ObjectClass && ObjectClass->IsValidLowLevel())
	{
		TObject* ObjectInstance = NewObject<TObject>(context, ObjectClass);
		if (ObjectInstance && ObjectInstance->IsValidLowLevel())
		{
			return ObjectInstance;
		}
	}
	return nullptr;
}


struct FBlueprintAssetContainer
{
	UClass* Class;
	TArray<FAssetData> Assets;

	FBlueprintAssetContainer()
		: Class(nullptr)
	{
		
	}

	FBlueprintAssetContainer(UClass* c)
		: Class(c)
	{
		Assets.Reserve(2048);
	}
};

void GetAllBlueprintAssetData(UObject* context, TArray<FBlueprintAssetContainer*>& containers)
{
	//=================================================================
	TArray<FAssetData> LocalAssetList;

	//=================================================================
	FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>(TEXT("AssetRegistry"));
	IAssetRegistry& AssetRegistry = AssetRegistryModule.Get();

	//=================================================================
	FARFilter ARFilter;
	ARFilter.bRecursivePaths = true;
	ARFilter.bRecursiveClasses = true;
	ARFilter.bIncludeOnlyOnDiskAssets = true;
	ARFilter.ClassNames.Add(UBlueprint::StaticClass()->GetFName());

	ARFilter.PackagePaths.Append(TArray<FName>({
		//TEXT("/Game/1LOKAgame/Blueprints/Shop/Build/Base_Cube/Cube")
		TEXT("/Game/1LOKAgame/Blueprints/Shop"),
		TEXT("/Game/1LOKAgame/Blueprints/Fractions"),
		TEXT("/Game/1LOKAgame/Blueprints/Achievements"),
		TEXT("/Game/1LOKAgame/Blueprints/Tutorials")
	}));

	//=================================================================
	AssetRegistry.ScanPathsSynchronous({
		TEXT("/Game/1LOKAgame/Blueprints/Shop"),
		TEXT("/Game/1LOKAgame/Blueprints/Fractions"),
		TEXT("/Game/1LOKAgame/Blueprints/Achievements"), 
		TEXT("/Game/1LOKAgame/Blueprints/Tutorials")
	});

	//=================================================================
	AssetRegistry.GetAssets(ARFilter, LocalAssetList);

	for (auto asset : LocalAssetList)
	{
		const FString* LoadedParentClass = asset.TagsAndValues.Find("NativeParentClass");
		if (LoadedParentClass != nullptr && !LoadedParentClass->IsEmpty())
		{
			UClass* Class = FindObject<UClass>(ANY_PACKAGE, **LoadedParentClass);
			if (Class == nullptr)
			{
				FString ParentPackage = *LoadedParentClass;
				ConstructorHelpers::StripObjectClass(ParentPackage);
				if (ParentPackage.StartsWith(TEXT("/Script/")))
				{
					ParentPackage = ParentPackage.LeftChop(ParentPackage.Len() - ParentPackage.Find(TEXT(".")));
					if (FindObject<UPackage>(nullptr, *ParentPackage))
					{
						Class = LoadObject<UClass>(nullptr, **LoadedParentClass, nullptr, LOAD_NoWarn | LOAD_Quiet);
					}
				}
			}

			if (Class)
			{
				for(auto &c : containers)
				{
					if(Class->IsChildOf(c->Class))
					{
						c->Assets.Add(asset);
					}
				}
			}
		}
	}
}

void UGameSingleton::LoadGameData()
{
	//======================================================================
	//
	AsyncTask(ENamedThreads::GameThread, [&, GameItemInstanceList = &GameItemInstanceList, r = ListOfProducts, GameAchievementList = &GameAchievementList, GameAchievementMap = &GameAchievementMap, DataAssets = &DataAssets, GameFractionList = &GameFractionList, GameFractionMap = &GameFractionMap]()
	{			
		//===================================================
		
		FBlueprintAssetContainer data(UDataAsset::StaticClass());
		FBlueprintAssetContainer items(UItemBaseEntity::StaticClass());
		FBlueprintAssetContainer achievements(UItemAchievementEntity::StaticClass());
		FBlueprintAssetContainer fractions(UFractionEntity::StaticClass());
		FBlueprintAssetContainer TutorialsContainer(UTutorialBase::StaticClass());
		TArray<FBlueprintAssetContainer*> containers =
		{
			&items, &achievements, &fractions, &TutorialsContainer
		};

		//===================================================
		GetAllBlueprintAssetData(this, containers);
		GetAllAssetData(data.Class, data.Assets);

		//===================================================
#if WITH_EDITOR
		TMap<FString, FString> InstanceNameToFile;
#endif
		TGuardValue<bool> GuardValue(GIsReconstructingBlueprintInstances, true);

		for (auto item : items.Assets)
		{
			auto instance = LoadGameAsset<UItemBaseEntity>(this, item);
			if (instance)
			{
#if WITH_EDITOR
				InstanceNameToFile.Add(instance->GetClass()->GetName() + ".Name", instance->GetItemName().ToString());
				InstanceNameToFile.Add(instance->GetClass()->GetName() + ".Description", instance->GetItemDescription().ToString());
#endif
				GameItemInstanceList->Add(instance);
				if (instance->GetModelId() == INDEX_NONE)
				{
#if WITH_EDITOR
					FMessageLog SingletonLog("LokaErrors");
					auto MessageRow = SingletonLog.Warning();
					MessageRow->AddToken(FTextToken::Create(NSLOCTEXT("LokaEditorErrors", "Singleton.INDEX_NONE.Prefix", "Item ModelId == INDEX_NONE ")));
					MessageRow->AddToken(FAssetNameToken::Create(instance->GetClass()->GetPathName(), FText::FromString(instance->GetClass()->GetPathName())));
					MessageRow->AddToken(FTextToken::Create(NSLOCTEXT("LokaEditorErrors", "Singleton.INDEX_NONE.Suffix", ", maybe fix it?")));
					SingletonLog.SuppressLoggingToOutputLog(true);
					SingletonLog.Open();
#else
					UE_LOG(LogCore, Error, TEXT("Item ModelId == INDEX_NONE %s"), *instance->GetClass()->GetPathName());
#endif
				}
			}
		}

#if WITH_EDITOR
		FStringOutputDevice Output;

		Output.Logf(TEXT("Key,SourceString") LINE_TERMINATOR);
		for (auto MyPair : InstanceNameToFile)
		{
			Output.Logf(TEXT("\"%s\",\"%s\"") LINE_TERMINATOR, *MyPair.Key, *MyPair.Value);
		}

		FFileHelper::SaveStringToFile(Output, *FString(FPaths::ProjectDir() / "ItemsNameAndDesc.txt"), FFileHelper::EEncodingOptions::ForceUTF8WithoutBOM);
#endif
		//-----------------------------------------------
		//	AchivementList
		for (auto item : achievements.Assets)
		{
			auto instance = LoadGameAsset<UItemAchievementEntity>(this, item);
			if (instance)
			{
				GameAchievementList->Add(instance);
				GameAchievementMap->Add(instance->GetModelId(), instance);
			}
		}


		//-----------------------------------------------
		for (auto TargetAsset : data.Assets)
		{
			if (auto CastedAsset = Cast<UDataAsset>(TargetAsset.GetAsset()))
			{
				DataAssets->Add(CastedAsset);
			}
		}

		//-----------------------------------------------
		for (auto item : fractions.Assets)
		{
			auto instance = LoadGameAsset<UFractionEntity>(this, item);
			if (instance)
			{
				GameFractionList->Add(instance);
				GameFractionMap->Add(instance->GetModelId(), instance);
			}
		}

		//-----------------------------------------------
		//	TutorialList
		for (auto item : TutorialsContainer.Assets)
		{
			auto instance = LoadGameAsset<UTutorialBase>(this, item);
			if (instance)
			{
				TutorialList.Add(instance);
			}
		}

		//-----------------------------------------------
		r->GetRequest();
	});
}

void UGameSingleton::ReloadGameData()
{
#if WITH_EDITOR
	FNotificationInfo NotificationInfo(FText::FromString("Process of reload singleton"));
	NotificationInfo.ExpireDuration = TNumericLimits<float>().Max();
	NotificationInfo.bUseThrobber = true;
	NotificationInfo.bUseSuccessFailIcons = true;

	MyNotify = FSlateNotificationManager::Get().AddNotification(NotificationInfo);
	MyNotify->SetCompletionState(SNotificationItem::ECompletionState::CS_Pending);
#endif
	//======================================================================
	GameItemObjectList.Empty(1024);
	GameItemInstanceList.Empty(1024);
	//GameItemInstanceMap.Empty(1024);
	
	GameFractionList.Empty(4);
	GameFractionMap.Empty(4);

	GameAchievementList.Empty(512);
	GameAchievementMap.Empty(512);

	DataAssets.Empty();
	TutorialList.Empty();

	//======================================================================
	LoadGameData();
}

bool UGameSingleton::IsLoadedData() const
{
	return (GameItemInstanceList.Num() >= 100 && LastListOfInstance.Items.Num()); // Магическое число т.к. хз как по другому
}

//UItemBaseEntity* UGameSingleton::FindItem(const int32& id) const
//{
//	if(GameItemInstanceMap.Contains(id))
//	{
//		return GameItemInstanceMap[id];
//	}
//
//	return nullptr;
//}

UItemBaseEntity* UGameSingleton::FindItem(const FItemModelInfo& model) const
{
	return FindItem(model.ModelId, model.CategoryTypeId);
}

UItemBaseEntity* UGameSingleton::FindItem(const int32 InModel, const TEnumAsByte<CategoryTypeId::Type> InCategory) const
{
	for(auto item : GameItemInstanceList)
	{
		if (const auto i = GetValidObject(item))
		{
			if (i->IsObjectEqual(InCategory, InModel))
			{
				return i;
			}
		}
	}

	return nullptr;
}

TArray<UItemBaseEntity*> UGameSingleton::FindList(const CategoryTypeId::Type& InCategory) const
{
	TArray<UItemBaseEntity*> ReturnValue;

	for (auto item : GameItemInstanceList)
	{
		if (const auto i = GetValidObject(item))
		{
			if (i->IsObjectEqual(InCategory))
			{
				ReturnValue.Add(i);
			}
		}
	}

	return ReturnValue;
}

UFractionEntity* UGameSingleton::FindFraction(const FractionTypeId& fraction) const
{
	if (GameFractionMap.Contains(fraction))
	{
		return GetValidObject(GameFractionMap[fraction]);
	}
	return nullptr;
}

UItemAchievementEntity* UGameSingleton::FindAchievement(const TEnumAsByte<ItemAchievementTypeId::Type>& achievement) const
{
	if (GameAchievementMap.Contains(achievement))
	{
		return GetValidObject(GameAchievementMap[achievement]);
	}
	return nullptr;
}

UTutorialBase* UGameSingleton::GetTutorial(const int32 InIndex) const
{
	for (auto MyTutorial : TutorialList)
	{
		if (MyTutorial->GetTutorialIndex() == InIndex)
		{
			return MyTutorial;
		}
	}

	return nullptr;
}

UGameSingleton::ThisClass* UGameSingleton::Get()
{
	return CastChecked<UGameSingleton>(GEngine->GameSingleton);
}

void UGameSingleton::OnListOfProductsSuccessfully(const FListOfInstance& data)
{
	LastListOfInstance = data;

	//==============================================
	for(auto i : data.Items)
	{
		//=================================
		auto instance = FindItem(i.ModelId, i.CategoryTypeId);
		if(instance == nullptr)
		{
			UE_LOG(LogInit, Error, TEXT("[OnListOfProductsSuccessfully][Item: %d | Cat: %d][Instance was nullptr]"), i.ModelId, static_cast<int32>(i.CategoryTypeId));
			continue;
		}

		//=================================
		instance->Initialize(i);

		//=================================
		auto fraction = FindFraction(i.FractionId);
		if (fraction)
		{
			fraction->Items.Add(instance);
			instance->SetFraction(fraction);
		}
		else
		{
			UE_LOG(LogInit, Error, TEXT("[OnListOfProductsSuccessfully][Item: %d | Cat: %d][fraction %d was nullptr]"), i.ModelId, static_cast<int32>(i.CategoryTypeId), static_cast<int32>(i.FractionId));
		}

	}

	//==============================================
	for (auto i : data.Achievements)
	{
		auto instance = FindAchievement(i.Id);
		if (instance == nullptr)
		{
			continue;
		}
		instance->Initialize(i);
	}

	//===============================================================[ Tutorials
	
	for (auto i : data.Tutorials)
	{
		auto FoundInstance = GetTutorial(i.TutorialId);
		if (FoundInstance && FoundInstance->IsValidLowLevel())
		{
			FoundInstance->TutorialServerData = i;
			FoundInstance->TutorialServerData.Steps.Sort([](const FTutorialStepView& A, const FTutorialStepView& B) { return A.StepId < B.StepId; });
		}
		else
		{
			UE_LOG(LogInit, Error, TEXT("[OnListOfProductsSuccessfully][Tutorial: %d][Instance was nullptr]"), i.TutorialId);
		}
	}

	//===============================================================[
	UE_LOG(LogInit, Error, TEXT("[OnListOfProductsSuccessfully][Items: %d][Achievements: %d][Tutorials: %d]"), data.Items.Num(), data.Achievements.Num(), data.Tutorials.Num());


#if WITH_EDITOR
	if (MyNotify.IsValid())
	{
		MyNotify->SetText(FText::FromString("Singleton reloaded successfully."));
		MyNotify->SetCompletionState(SNotificationItem::ECompletionState::CS_Success);
		MyNotify->SetExpireDuration(10.0f);
		MyNotify->ExpireAndFadeout();
		MyNotify.Reset();
	}
#endif
}

void UGameSingleton::OnListOfProductsFailed(const FRequestExecuteError& error)
{
	UE_LOG(LogInit, Error, TEXT("[OnListOfProductsFailed][Error]: %s"), *error.ToString());


#if WITH_EDITOR
	if (MyNotify.IsValid())
	{
		MyNotify->SetText(FText::FromString("Singleton reloaded failed."));
		MyNotify->SetCompletionState(SNotificationItem::ECompletionState::CS_Fail);
		MyNotify->SetExpireDuration(10.0f);
		MyNotify->ExpireAndFadeout();
		MyNotify.Reset();
	}
#endif
}

void UGameSingleton::PostInitProperties()
{
	Super::PostInitProperties();

	TickDelegate = FTickerDelegate::CreateUObject(this, &UGameSingleton::Tick);
	TickDelegateHandle = FTicker::GetCoreTicker().AddTicker(TickDelegate);

#if !WITH_EDITOR
	LoadGameData();
#else
	if (!HasAnyFlags(RF_ArchetypeObject | RF_ClassDefaultObject))
	{
		static TSharedPtr<SNotificationItem> ThisNotify;

		FNotificationInfo NotificationInfo(FText::FromString(TEXT("For 'Editor' disabled auto-loading singleton, please load manually if need!")));
		NotificationInfo.ExpireDuration = TNumericLimits<float>().Max();

		FNotificationButtonInfo NotificationButtonClose(FText::FromString("Close"), FText::FromString("Understandably"), FSimpleDelegate::CreateLambda([&]()
		{
			ThisNotify->Fadeout();
		}), SNotificationItem::CS_None);

		FNotificationButtonInfo NotificationButtonLoad(FText::FromString("Load"), FText::FromString("Load singleton"), FSimpleDelegate::CreateLambda([&]()
		{
			ReloadGameData();
			ThisNotify->Fadeout();
		}), SNotificationItem::CS_None);

		NotificationInfo.ButtonDetails.Add(NotificationButtonClose);
		NotificationInfo.ButtonDetails.Add(NotificationButtonLoad);

		ThisNotify = FSlateNotificationManager::Get().AddNotification(NotificationInfo);
	}
#endif
}

FPlayerEntityInfo UGameSingleton::GetSavedPlayerEntityInfo() const
{
	return LocalPlayerEntityInfo;
}

void UGameSingleton::UpdateSavedPlayerEntityInfo(const UIdentityComponent* InComponent)
{
	if (InComponent)
	{
		LocalPlayerEntityInfo = InComponent->GetPlayerInfo();
	}
}


void UGameSingleton::DoCaptureEntityImages()
{
#if WITH_EDITOR
	if (CaptureQueue.IsEmpty() == false)
	{
		CaptureQueue.Empty();
		FSlateNotificationManager::Get().AddNotification(FNotificationInfo(FText::FromString("Capture entity images was stoped.")));
		return;
	}


	FSlateNotificationManager::Get().AddNotification(FNotificationInfo(FText::FromString("Capture entity images was started.")));

	UWorld* TargetWorld = GetCaptureWorld();

	auto MySettings = GetDefault<ULokaEditorSettings>();

	auto MyTexture = Cast<UTextureRenderTarget2D>(MySettings->Asset_SourceTexture.TryLoad());
	DynamicMaterialForCapture = UMaterialInstanceDynamic::Create(Cast<UMaterialInterface>(MySettings->Asset_SourceMaterial.TryLoad()), this);
	DynamicMaterialForCapture->SetTextureParameterValue(TEXT("Source"), MyTexture);

	if (TargetWorld)
	{
		FActorSpawnParameters SpawnParameters;
		SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		PreviewItem = TargetWorld->SpawnActor<AWorldPreviewItem>(AWorldPreviewItem::StaticClass(), SpawnParameters);
		CaptureWorker = TargetWorld->SpawnActor<ASceneCapture2D>(ASceneCapture2D::StaticClass(), FVector(-100, 0, 0), FRotator::ZeroRotator, SpawnParameters);
		PointLight = TargetWorld->SpawnActor<ADirectionalLight>(ADirectionalLight::StaticClass(), FVector(-100, 0, 0), MySettings->Conf_LightRotation, SpawnParameters);
		PointLight->GetLightComponent()->Mobility = EComponentMobility::Movable;
		// Sky light for best quality
		SkyLight = NewObject<USkyLightComponent>(GetTransientPackage(), NAME_None, RF_Transient);
		SkyLight->Mobility = EComponentMobility::Movable;
		SkyLight->SourceType = ESkyLightSourceType::SLS_SpecifiedCubemap;
		SkyLight->SetCubemap(Cast<UTextureCube>(MySettings->Asset_SkyCubemap.TryLoad()));		
		SkyLight->SourceCubemapAngle = MySettings->Conf_SkyCubemapAngle;
		SkyLight->SetIntensity(MySettings->Conf_SkyIntensity);
		SkyLight->SanitizeCubemapSize();
		SkyLight->SetCaptureIsDirty();
		SkyLight->SetRelativeTransform(FTransform::Identity);
		SkyLight->RegisterComponentWithWorld(TargetWorld);

		if (PreviewItem && CaptureWorker)
		{
			CaptureWorker->GetCaptureComponent2D()->bCaptureEveryFrame = false;
			CaptureWorker->GetCaptureComponent2D()->TextureTarget = MyTexture;
			CaptureWorker->GetCaptureComponent2D()->CaptureSource = ESceneCaptureSource::SCS_SceneColorHDR;
			CaptureWorker->GetCaptureComponent2D()->CompositeMode = ESceneCaptureCompositeMode::SCCM_Overwrite;
			CaptureWorker->GetCaptureComponent2D()->ShowOnlyActorComponents(PreviewItem);

			auto TargetList = FindList(ECategoryTypeId::Weapon);
			TargetList.Append(FindList(ECategoryTypeId::Character));
			TargetList.Append(FindList(ECategoryTypeId::Building));

			for (auto item : TargetList)
			{
				if (CheckIsExistPackage(item->GetArchetype()) == false)
				{
					CaptureQueue.Enqueue(Cast<UItemBaseEntity>(item->GetArchetype()));
				}
			}

			cTimer = .0f;
			cState = 0;
		}		
	}
#endif
}


bool UGameSingleton::Tick(float DeltaSeconds)
{
#if WITH_EDITOR
	if (CaptureQueue.IsEmpty() == false)
	{
		cTimer += DeltaSeconds;

		auto MySettings = GetDefault<ULokaEditorSettings>();

		if (cTimer > 0.2f && cState == 0)
		{
			UItemBaseEntity* Target = nullptr;
			CaptureQueue.Peek(Target);

			if (Target && Target->IsValidLowLevel())
			{
				PreviewItem->InitializeInstance(Target);
			}

			cState = 1;
			cTimer = .0f;
		}
		else if (cTimer > 0.2f && cState == 1)
		{
			PreviewItem->ForceTextureStream();
			PreviewItem->CalculateAndApplyCenter();

			cState = 2;
			cTimer = .0f;
		}
		else if (cTimer > 0.4f && cState == 2)
		{
			UItemBaseEntity* Target = nullptr;
			CaptureQueue.Peek(Target);

			if (Target && Target->IsValidLowLevel())
			{
				FVector BoundOrigin;
				FVector BoundExtent;

				PreviewItem->SetActorRelativeTransform(Target->GetCaptureImageSettings().IsValid() ? Target->GetCaptureImageSettings().TransformOffsets : FTransform::Identity);

				if (auto TargetAsChild = PreviewItem->GetChildActor())
				{
					TargetAsChild->GetActorBounds(false, BoundOrigin, BoundExtent);
					CaptureWorker->SetActorLocation(FVector(-BoundExtent.GetMax() * (Target->GetCaptureImageSettings().IsValid() ? Target->GetCaptureImageSettings().DistanceMultipler : MySettings->Conf_DistanceMultipler), 0, 0));
					CaptureWorker->GetCaptureComponent2D()->ShowOnlyActorComponents(TargetAsChild);
				}
				else
				{
					PreviewItem->GetActorBounds(false, BoundOrigin, BoundExtent);
					CaptureWorker->SetActorLocation(FVector(-BoundExtent.GetMax() * (Target->GetCaptureImageSettings().IsValid() ? Target->GetCaptureImageSettings().DistanceMultipler : MySettings->Conf_DistanceMultipler), 0, 0));
					CaptureWorker->GetCaptureComponent2D()->ShowOnlyActorComponents(PreviewItem);
				}

				cState = 3;
				cTimer = .0f;
			}
		}
		else if (cTimer > 0.1f && cState == 3)
		{
			GetCaptureWorld()->UpdateWorldComponents(false, true);

			cState = 4;
			cTimer = .0f;
		}
		else if (cTimer > 0.1f && cState == 4)
		{
			CaptureWorker->GetCaptureComponent2D()->CaptureScene();

			cState = 5;
			cTimer = .0f;
		}
		else if (cTimer > 0.4f && cState == 5)
		{
			UItemBaseEntity* Target = nullptr;
			CaptureQueue.Dequeue(Target);

			if (Target && Target->IsValidLowLevel())
			{
				FString LeftS, RightS;
				Target->GetFullName().Split(".", &LeftS, &RightS);
				LeftS = RightS;

				if (auto CreatedTexture = ULKBlueprintFunctionLibrary::K2_CreateStaticTexture(CaptureWorker->GetCaptureComponent2D()->TextureTarget, MySettings->Path_CapturedStaticTexture.Path, RightS))
				{
					CreatedTexture->CompressionSettings = TextureCompressionSettings::TC_Default;
					CreatedTexture->SRGB = true;
					CreatedTexture->Modify();
					CreatedTexture->PostEditChange();

					FString PackageName = CreatedTexture->GetOutermost()->GetName();

					FAssetToolsModule& AssetToolsModule = FModuleManager::Get().LoadModuleChecked<FAssetToolsModule>("AssetTools");
					AssetToolsModule.Get().CreateUniqueAssetName(MySettings->Path_CapturedStaticTexture.Path + LeftS, "_EM", PackageName, LeftS);
					
					UMaterialInstanceConstant* TempMat = NewObject<UMaterialInstanceConstant>(CreatePackage(nullptr, *PackageName), *LeftS, DynamicMaterialForCapture->Parent->GetMaskedFlags());
					TempMat->SetParentEditorOnly(DynamicMaterialForCapture->Parent);
					TempMat->SetTextureParameterValueEditorOnly(TEXT("Source"), CreatedTexture);

					if (TempMat)
					{
						TempMat->Modify();
						TempMat->PostEditChange();

						// Notify the asset registry
						FAssetRegistryModule::AssetCreated(TempMat);
					}

					UBlueprint* Blueprint = Cast<UBlueprint>(Target->GetClass()->ClassGeneratedBy);
					if (Blueprint && MySettings->Conf_IfSaveCaptures)
					{
						Target->EntityImage = FSlateMaterialBrush(*TempMat, FVector2D(CreatedTexture->GetSizeX(), CreatedTexture->GetSizeY()));

						TArray<UPackage*> PackagesToSave;
						PackagesToSave.Add(CreatedTexture->GetOutermost());
						PackagesToSave.Add(Target->GetOutermost());
						PackagesToSave.Add(TempMat->GetOutermost());

						FKismetEditorUtilities::CompileBlueprint(Blueprint);
						FEditorFileUtils::PromptForCheckoutAndSave(PackagesToSave, /*bCheckDirty*/false, /*bPromptToSave*/false);
					}

					FNotificationInfo NotificationInfo(FText::FromString("Captured image: " + CreatedTexture->GetFullName()));
					NotificationInfo.bUseLargeFont = false;

					auto localMyNotifyEc = FSlateNotificationManager::Get().AddNotification(NotificationInfo);
					localMyNotifyEc->SetExpireDuration(10);
					localMyNotifyEc->ExpireAndFadeout();
				}
			}

			cTimer = .0f;
			cState = 0;

			if (CaptureQueue.IsEmpty())
			{
				FSlateNotificationManager::Get().AddNotification(FNotificationInfo(FText::FromString("Capture entity images is completed! Destination folder: " + MySettings->Path_CapturedStaticTexture.Path)));
			}
		}
	}
#endif

	return true;
}

bool UGameSingleton::CheckIsExistPackage(const UObject* InObject) const
{
#if WITH_EDITOR
	auto MySettings = GetDefault<ULokaEditorSettings>();

	if (InObject && MySettings)
	{
		FString LeftS, RightS;
		InObject->GetFullName().Split(".", &LeftS, &RightS);

		return FindPackage(nullptr, *FString(MySettings->Path_CapturedStaticTexture.Path + RightS + "_ET")) || FindPackage(nullptr, *FString(MySettings->Path_CapturedStaticTexture.Path + RightS + "_EM"));
	}	
#endif

	return false;
}

UWorld* UGameSingleton::GetCaptureWorld()
{
#if WITH_EDITOR
	if (CaptureWorld == nullptr)
	{
		CaptureWorld = NewObject<UWorld>(GetTransientPackage(), TEXT("CaptureEntityImagesWorld"), RF_Standalone | RF_Transient);
		CaptureWorld->WorldType = EWorldType::EditorPreview;

		FWorldContext& WorldContext = GEngine->CreateNewWorldContext(CaptureWorld->WorldType);
		WorldContext.SetCurrentWorld(CaptureWorld);
		//WorldContext.AddRef(CaptureWorld);

		CaptureWorld->InitializeNewWorld(UWorld::InitializationValues()
			.AllowAudioPlayback(false)
			.CreatePhysicsScene(false)
			.RequiresHitProxies(true)
			.CreateNavigation(false)
			.CreateAISystem(false)
			.ShouldSimulatePhysics(false)
			.SetTransactional(true));
		CaptureWorld->InitializeActorsForPlay(FURL());
		CaptureWorld->SetShouldTick(true);
	}

	return CaptureWorld;
#else
	return nullptr;
#endif
}
