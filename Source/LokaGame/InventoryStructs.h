#pragma once
#include "Weapon/ItemWeaponModelId.h"
#include "Material/ItemMaterialModelId.h"
#include "Module/ItemModuleModelId.h"
#include "Grenade/ItemGrenadeModelId.h"
#include "Character/CharacterModelId.h"
#include "Armour/ItemArmourModelId.h"
#include "Weapon/ItemWeaponModelId.h"
#include "InventoryStructs.generated.h"

USTRUCT(BlueprintType)
struct FWeaponModifer
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere)
	FString Key;

	UPROPERTY(EditAnywhere)
	float Value;

	FWeaponModifer() : Key(), Value() {}
	FWeaponModifer(FString k, float v) : Key(k), Value(v) {}
};

USTRUCT(BlueprintType)
struct FWeaponPresetData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere)
	TEnumAsByte<ItemWeaponModelId::Type> Item;

	UPROPERTY(EditAnywhere)
	TEnumAsByte<ItemMaterialModelId::Type> Skin;

	UPROPERTY(EditAnywhere)
	TArray<TEnumAsByte<ItemModuleModelId::Type>> Modules;

	UPROPERTY(EditAnywhere)
	TArray<FWeaponModifer> Modifers;

	TMap<FString, float> GetModifers() const
	{
		TMap<FString, float> m;

		for (auto &i : Modifers)
		{
			m.Add(i.Key, i.Value);
		}

		return m;
	}

	FWeaponPresetData() : Item(ItemWeaponModelId::End), Modifers(), Modules(), Skin() {}
	FWeaponPresetData(uint8 i) : Item(i), Modifers(), Modules(), Skin() {}
	FWeaponPresetData(uint8 i, TArray<FWeaponModifer>& m) : Item(i), Modifers(m), Modules(), Skin() {}
	FWeaponPresetData(uint8 i, TArray<FWeaponModifer>& m, TArray<uint8>& md) : Item(i), Modifers(m), Modules(md), Skin() {}
	FWeaponPresetData(uint8 i, TArray<uint8>& md) : Item(i), Modifers(), Modules(md), Skin() {}
	FWeaponPresetData(uint8 i, TArray<uint8>& md, const uint8& s) : Item(i), Modifers(), Modules(md), Skin(s) {}
};

USTRUCT(BlueprintType)
struct FArmourPresetData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere)
	TEnumAsByte<ItemArmourModelId::Type> Item;

	FArmourPresetData() : Item(ItemArmourModelId::End) {}
	FArmourPresetData(uint8 i) : Item(i) {}
};

USTRUCT(BlueprintType)
struct FPresetData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere)
	bool IsAllow;

	UPROPERTY(EditAnywhere)
	FString Name;

	UPROPERTY(EditAnywhere)
	TArray<FWeaponPresetData> Weapons;

	UPROPERTY(EditAnywhere)
	TArray<FArmourPresetData> Armour;

	UPROPERTY(EditAnywhere)
	TArray<TEnumAsByte<ItemGrenadeModelId::Type>> Grenades;

	UPROPERTY(EditAnywhere)
	TEnumAsByte<ECharacterModelId::Type> Character;

	FPresetData() : IsAllow(false), Name("Unknown"), Weapons(), Armour(), Grenades(), Character(ECharacterModelId::End) {}
	FPresetData(const int32& InIndex) : IsAllow(false), Name(FString("Equipment set " + FString::FromInt(InIndex + 1))), Weapons(), Armour(), Grenades(), Character(ECharacterModelId::End) {}
};