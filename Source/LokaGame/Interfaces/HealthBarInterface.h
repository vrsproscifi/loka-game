// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "DestroyableActorInterface.h"
#include "HealthBarInterface.generated.h"

DECLARE_DYNAMIC_DELEGATE_RetVal(bool, FGetIsVisibily);

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UHealthBarInterface : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

/**
 * 
 */
class LOKAGAME_API IHealthBarInterface
{
	GENERATED_IINTERFACE_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = Gameplay)
	void SetDestroyableObject(UObject* InObject);

};
