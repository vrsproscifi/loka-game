// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "UsableActorInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UUsableActorInterface : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

/**
 * 
 */
class LOKAGAME_API IUsableActorInterface
{
	GENERATED_IINTERFACE_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = Gameplay)
	void OnFocusStart(AActor* InInstigator);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = Gameplay)
	void OnFocusLost(AActor* InInstigator);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = Gameplay)
	void OnInteractStart(AActor* InInstigator);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = Gameplay)
	void OnInteractLost(AActor* InInstigator);

};
