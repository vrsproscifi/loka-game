// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "LokaNetUserInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class ULokaNetUserInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class LOKAGAME_API ILokaNetUserInterface
{
	GENERATED_BODY()

public:
	
	virtual FGuid GetIdentityToken() const = 0;

	virtual bool SetIdentityTokenString(const FString& InTokenString) = 0;
	virtual void SetIdentityToken(const FGuid& InToken) = 0;

	virtual bool IsAllowUserInventory() const = 0;
};
