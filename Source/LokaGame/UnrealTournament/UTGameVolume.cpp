// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#include "LokaGame.h"
#include "UTGameVolume.h"
#include "UTDmgType_Suicide.h"
#include "UTGameState.h"

#include "UTPlayerState.h"
#include "UTTeleporter.h"

#include "UTCharacterVoice.h"

#include "UTTeamInfo.h"
#include "UTPlayerController.h"



AUTGameVolume::AUTGameVolume(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	VolumeName = FText::GetEmpty();
	TeamIndex = 255;
	bShowOnMinimap = true;
	bIsNoRallyZone = false;
	bIsTeamSafeVolume = false;
	bIsTeleportZone = false;
	RouteID = -1;
	bReportDefenseStatus = false;
	bHasBeenEntered = false;

	static ConstructorHelpers::FObjectFinder<USoundBase> AlarmSoundFinder(TEXT("SoundCue'/Game/RestrictedAssets/Audio/Gameplay/A_FlagRunBaseAlarm.A_FlagRunBaseAlarm'"));
	AlarmSound = AlarmSoundFinder.Object;

	static ConstructorHelpers::FObjectFinder<USoundBase> EarnedSoundFinder(TEXT("SoundCue'/Game/RestrictedAssets/Pickups/Health/Asset/A_Pickups_Health_Small_Cue_Modulated.A_Pickups_Health_Small_Cue_Modulated'"));
	HealthSound = EarnedSoundFinder.Object;
}

void AUTGameVolume::Reset_Implementation()
{
	bHasBeenEntered = false;
}

void AUTGameVolume::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (bIsTeamSafeVolume && (Role == ROLE_Authority))
	{
		//GetWorldTimerManager().SetTimer(HealthTimerHandle, this, &AUTGameVolume::HealthTimer, 1.f, true);
	}
	else if ((VoiceLinesSet == NAME_None) && !VolumeName.IsEmpty() && !bIsTeleportZone)
	{
		VoiceLinesSet = UUTCharacterVoice::StaticClass()->GetDefaultObject<UUTCharacterVoice>()->GetFallbackLines(FName(*VolumeName.ToString()));
		if (VoiceLinesSet == NAME_None)
		{
			UE_LOG(UT, Warning, TEXT("No voice lines found for %s"), *VolumeName.ToString());
		}
	}
}

void AUTGameVolume::HealthTimer()
{
	TArray<AActor*> TouchingActors;
	GetOverlappingActors(TouchingActors, AUTCharacter::StaticClass());

	for (int32 iTouching = 0; iTouching < TouchingActors.Num(); ++iTouching)
	{
		AUTCharacter* const P = Cast<AUTCharacter>(TouchingActors[iTouching]);
		if (P && (GetWorld()->GetTimeSeconds() - P->EnteredSafeVolumeTime) > 1.f)
		{
			if (P->Health.Amount < P->Health.MaxAmount)
			{
				P->Health.Amount = FMath::Max<float>(P->Health.Amount, FMath::Min<int32>(P->Health.Amount + 20, P->Health.MaxAmount));
				AUTPlayerController* PC = Cast<AUTPlayerController>(P->GetController());
				if (PC)
				{
					PC->ClientPlaySound(HealthSound);
				}
			}
			//if ((TeamLockers.Num() > 0) && TeamLockers[0] && P->bHasLeftSafeVolume)
			//{
			//	TeamLockers[0]->ProcessTouch(P);
			//}
		}
	}
}

void AUTGameVolume::ActorEnteredVolume(class AActor* Other)
{
	if ((Role == ROLE_Authority) && Cast<AUTCharacter>(Other))
	{
		AUTCharacter* P = ((AUTCharacter*)(Other));
		P->LastGameVolume = this;
		AUTGameState* GS = GetWorld()->GetGameState<AUTGameState>();
		if (GS != nullptr && P->GetPlayerState<AUTPlayerState>() != nullptr  && GS->IsMatchInProgress())
		{
			if (bIsTeamSafeVolume)
			{
				// friendlies are invulnerable, enemies must die
				if (!GS->OnSameTeam(this, P))
				{
					P->TakeDamage(1000.f, FDamageEvent(UUTDmgType_Suicide::StaticClass()), nullptr, this);
				}
				else
				{
					P->bDamageHurtsHealth = false;
					P->EnteredSafeVolumeTime = GetWorld()->GetTimeSeconds();
					if ((P->Health.Amount < 80) && Cast<AUTPlayerState>(P->GetPlayerState<AUTPlayerState>()) && (GetWorld()->GetTimeSeconds() - ((AUTPlayerState*)(P->GetPlayerState<AUTPlayerState>()))->LastNeedHealthTime > 20.f))
					{
						((AUTPlayerState*)(P->GetPlayerState<AUTPlayerState>()))->LastNeedHealthTime = GetWorld()->GetTimeSeconds();
						((AUTPlayerState*)(P->GetPlayerState<AUTPlayerState>()))->AnnounceStatus(StatusMessage::NeedHealth, 0, true);
					}
				}
			}
			if (bIsTeleportZone)
			{
				if (AssociatedTeleporter == nullptr)
				{
					for (FActorIterator It(GetWorld()); It; ++It)
					{
						AssociatedTeleporter = Cast<AUTTeleporter>(*It);
						if (AssociatedTeleporter)
						{
							break;
						}
					}
				}
				if (AssociatedTeleporter)
				{
					AssociatedTeleporter->OnOverlapBegin(this, P);
				}
			}
			else if (P->GetCarriedObject())
			{
				/*				if (VoiceLinesSet != NAME_None)
								{
									UE_LOG(UT, Warning, TEXT("VoiceLineSet %s for %s location %s"), *VoiceLinesSet.ToString(), *GetName(), *VolumeName.ToString());
									((AUTPlayerState *)(P->GetPlayerState<AUTPlayerState>()))->AnnounceStatus(VoiceLinesSet, 1);
								}
								else
								{
									UE_LOG(UT, Warning, TEXT("No VoiceLineSet for %s location %s"), *GetName(), *VolumeName.ToString());
								}
								return;*/
				if (bIsNoRallyZone && !P->GetCarriedObject()->bWasInEnemyBase)
				{
					if (GetWorld()->GetTimeSeconds() - P->GetCarriedObject()->EnteredEnemyBaseTime > 10.f)
					{
						// play alarm
						UUTGameplayStatics::UTPlaySound(GetWorld(), AlarmSound, P, SRT_All, false, FVector::ZeroVector, NULL, NULL, false);
					}
					P->GetCarriedObject()->EnteredEnemyBaseTime = GetWorld()->GetTimeSeconds();
				}

				// possibly announce flag carrier changed zones
				if (bIsNoRallyZone && !P->GetCarriedObject()->bWasInEnemyBase && (GetWorld()->GetTimeSeconds() - FMath::Max(GS->LastEnemyEnteringBaseTime, GS->LastEnteringEnemyBaseTime) > 10.f))
				{
					if ((GetWorld()->GetTimeSeconds() - GS->LastEnteringEnemyBaseTime > 6.f) && P->GetPlayerState<AUTPlayerState>())
					{
						if (VoiceLinesSet != NAME_None)
						{
							P->GetPlayerState<AUTPlayerState>()->AnnounceStatus(VoiceLinesSet, 1);
						}
						
						P->GetPlayerState<AUTPlayerState>()->AnnounceStatus(StatusMessage::ImGoingIn);
						GS->LastEnteringEnemyBaseTime = GetWorld()->GetTimeSeconds();
					}
					if (GetWorld()->GetTimeSeconds() - GS->LastEnemyEnteringBaseTime > 10.f)
					{
						AUTPlayerState* PS = P->GetCarriedObject()->LastPinger;
						if (!PS)
						{
							for (FConstControllerIterator Iterator = GetWorld()->GetControllerIterator(); Iterator; ++Iterator)
							{
								AController* C = Iterator->Get();
								if (C && !GS->OnSameTeam(P, C) && C->GetPlayerState<AUTPlayerState>())
								{
									PS = C->GetPlayerState<AUTPlayerState>();
									break;
								}
							}
						}
						if (PS)
						{
							if (VoiceLinesSet != NAME_None)
							{
								PS->AnnounceStatus(VoiceLinesSet, 0);
							}
							PS->AnnounceStatus(StatusMessage::BaseUnderAttack);
							GS->LastEnemyEnteringBaseTime = GetWorld()->GetTimeSeconds();
						}
					}
				}
				else if ((GetWorld()->GetTimeSeconds() - FMath::Max(GS->LastFriendlyLocationReportTime, GS->LastEnemyLocationReportTime) > 3.f) || bIsWarningZone)
				{
					if ((VoiceLinesSet != NAME_None) && (GetWorld()->GetTimeSeconds() - GS->LastFriendlyLocationReportTime > 3.f) &&P->GetPlayerState<AUTPlayerState>() && (GS->LastFriendlyLocationName != VoiceLinesSet))
					{
						P->GetPlayerState<AUTPlayerState>()->AnnounceStatus(VoiceLinesSet, 1);
						GS->LastFriendlyLocationReportTime = GetWorld()->GetTimeSeconds();
						GS->LastFriendlyLocationName = VoiceLinesSet;
					}
					if ((VoiceLinesSet != NAME_None) && P->GetCarriedObject()->bCurrentlyPinged && P->GetCarriedObject()->LastPinger && (GetWorld()->GetTimeSeconds() - GS->LastEnemyLocationReportTime > 3.f) && (GS->LastEnemyLocationName != VoiceLinesSet))
					{
						P->GetCarriedObject()->LastPinger->AnnounceStatus(VoiceLinesSet, 0);
						GS->LastEnemyLocationReportTime = GetWorld()->GetTimeSeconds();
						GS->LastEnemyLocationName = VoiceLinesSet;
					}
					else if (bIsWarningZone && !P->bWasInWarningZone && !bIsNoRallyZone)
					{
						// force ping if important zone, wasn't already in important zone, report only if 3 seconds since last report
						// also do if no pinger if important zone
						P->GetCarriedObject()->LastPingedTime = FMath::Max(P->GetCarriedObject()->LastPingedTime, GetWorld()->GetTimeSeconds() - P->GetCarriedObject()->PingedDuration + 1.f);
						if (VoiceLinesSet != NAME_None)
						{
							AUTPlayerState* Warner = P->GetCarriedObject()->LastPinger;
							if (Warner == nullptr)
							{
								for (FConstControllerIterator Iterator = GetWorld()->GetControllerIterator(); Iterator; ++Iterator)
								{
									AController* C = Iterator->Get();
									if (C && !GS->OnSameTeam(P, C) && C->GetPlayerState<AUTPlayerState>())
									{
										Warner = C->GetPlayerState<AUTPlayerState>();
										break;
									}
								}
							}
							if (Warner && (GetWorld()->GetTimeSeconds() - GS->LastEnemyLocationReportTime > 3.f))
							{
								Warner->AnnounceStatus(VoiceLinesSet, 0);
								GS->LastEnemyLocationReportTime = GetWorld()->GetTimeSeconds();
								GS->LastEnemyLocationName = VoiceLinesSet;
							}
						}
					}
				}
				P->bWasInWarningZone = bIsWarningZone;
				P->GetCarriedObject()->bWasInEnemyBase = bIsNoRallyZone;
			}
		}
		bHasBeenEntered = true;
	}

	if (!VolumeName.IsEmpty())
	{
		AUTCharacter* P = Cast<AUTCharacter>(Other);
		if (P != nullptr)
		{
			//P->LastKnownLocation = this;
			AUTPlayerState* PS = Cast<AUTPlayerState>(P->GetPlayerState<AUTPlayerState>());
			if (PS != nullptr)
			{
				PS->LastKnownLocation = this;
			}
		}
	}
}

void AUTGameVolume::ActorLeavingVolume(class AActor* Other)
{
	AUTCharacter* UTCharacter = Cast<AUTCharacter>(Other);
	if (UTCharacter)
	{
		if (bIsTeamSafeVolume)
		{
			UTCharacter->bDamageHurtsHealth = true;
			UTCharacter->bHasLeftSafeVolume = true;
			//if (UTCharacter->GetController() && (TeamLockers.Num() > 0) && TeamLockers[0])
			//{
			//	TeamLockers[0]->ProcessTouch(UTCharacter);
			//}
			AUTPlayerController* PC = Cast<AUTPlayerController>(UTCharacter->GetController());
			if (PC)
			{
				PC->LeftSpawnVolumeTime = GetWorld()->GetTimeSeconds();
			}
		}
	}
}



void AUTGameVolume::SetTeamForSideSwap_Implementation(uint8 NewTeamNum)
{
	TeamIndex = NewTeamNum;
}





