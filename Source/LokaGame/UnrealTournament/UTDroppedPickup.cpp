// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#include "LokaGame.h"
#include "UTPickup.h"
#include "UTPickupInventory.h"
#include "UTDroppedPickup.h"
#include "UnrealNetwork.h"
#include "UTPickupMessage.h"
#include "UTWorldSettings.h"
#include "UTGameMode.h"

#include "UTBot.h"
#include "UTCharacter.h"
#include "UTCharacterMovement.h"
#include "UTGameplayStatics.h"


AUTDroppedPickup::AUTDroppedPickup(const FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{
	Collision = ObjectInitializer.CreateDefaultSubobject<UCapsuleComponent>(this, TEXT("Capsule"));
	Collision->SetCollisionProfileName(FName(TEXT("Pickup")));
	Collision->InitCapsuleSize(64.0f, 30.0f);
	Collision->OnComponentBeginOverlap.AddDynamic(this, &AUTDroppedPickup::OnOverlapBegin);
	RootComponent = Collision;

	Movement = ObjectInitializer.CreateDefaultSubobject<UUTProjectileMovementComponent>(this, TEXT("Movement"));
	Movement->HitZStopSimulatingThreshold = 0.7f;
	Movement->UpdatedComponent = Collision;
	Movement->OnProjectileStop.AddDynamic(this, &AUTDroppedPickup::PhysicsStopped);

	//bCollideWhenPlacing = true; // causes too many false positives at the moment, re-evaluate later
	InitialLifeSpan = 15.0f;
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	SetReplicates(true);
	bReplicateMovement = true;
	NetUpdateFrequency = 1.0f;
}

void AUTDroppedPickup::BeginPlay()
{
	Super::BeginPlay();

	if (!IsPendingKillPending())
	{
		// don't allow Instigator to touch until a little time has passed so a live player throwing an item doesn't immediately pick it back up again
		GetWorld()->GetTimerManager().SetTimer(EnableInstigatorTouchHandle, this, &AUTDroppedPickup::EnableInstigatorTouch, 1.0f, false);
	}
}

void AUTDroppedPickup::EnableInstigatorTouch()
{
	if (Instigator != nullptr)
	{
		CheckTouching();
	}
}

void AUTDroppedPickup::CheckTouching()
{
	TArray<AActor*> Overlaps;
	GetOverlappingActors(Overlaps, APawn::StaticClass());
	for (AActor* TestActor : Overlaps)
	{
		APawn* P = Cast<APawn>(TestActor);
		if (P != nullptr && P->GetMovementComponent() != nullptr)
		{
			FHitResult UnusedHitResult;
			OnOverlapBegin(Collision, P, Cast<UPrimitiveComponent>(P->GetMovementComponent()->UpdatedComponent), 0, false, UnusedHitResult);
		}
	}
}

void AUTDroppedPickup::SetInventory(AUTInventory* NewInventory)
{
	Inventory = NewInventory;
	InventoryType = (NewInventory != nullptr) ? NewInventory->GetClass() : NULL;
	InventoryTypeUpdated();

	bFullyInitialized = true;
	CheckTouching();
}

void AUTDroppedPickup::InventoryTypeUpdated_Implementation()
{
	AUTPickupInventory::CreatePickupMesh(this, Mesh, InventoryType, 0.0f, FRotator::ZeroRotator, false);
}

void AUTDroppedPickup::PhysicsStopped(const FHitResult& ImpactResult)
{
	// if we landed on a mover, attach to it
	if (ImpactResult.Component != nullptr && ImpactResult.Component->Mobility == EComponentMobility::Movable)
	{
		Collision->AttachToComponent(ImpactResult.Component.Get(), FAttachmentTransformRules::KeepWorldTransform, NAME_None);
	}
	AUTRecastNavMesh* NavData = GetUTNavData(GetWorld());
	if (NavData != nullptr)
	{
		NavData->AddToNavigation(this);
	}
}

void AUTDroppedPickup::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
	AUTRecastNavMesh* NavData = GetUTNavData(GetWorld());
	if (NavData != nullptr)
	{
		NavData->RemoveFromNavigation(this);
	}
	if (Inventory != nullptr && !Inventory->IsPendingKillPending())
	{
		Inventory->Destroy();
	}
	GetWorldTimerManager().ClearAllTimersForObject(this);
}

void AUTDroppedPickup::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (bFullyInitialized && (OtherActor != Instigator || !GetWorld()->GetTimerManager().IsTimerActive(EnableInstigatorTouchHandle)))
	{
		APawn* P = Cast<APawn>(OtherActor);
		if (P != nullptr && !P->GetTearOff() && !GetWorld()->LineTraceTestByChannel(P->GetActorLocation(), GetActorLocation(), ECC_Pawn, FCollisionQueryParams(), WorldResponseParams))
		{
			ProcessTouch(P);
		}
	}
}

bool AUTDroppedPickup::AllowPickupBy_Implementation(APawn* Other, bool bDefaultAllowPickup)
{
	AUTCharacter* UTC = Cast<AUTCharacter>(Other);
	bDefaultAllowPickup = bDefaultAllowPickup && UTC != nullptr && UTC->bCanPickupItems && !UTC->IsRagdoll();
	bool bAllowPickup = bDefaultAllowPickup;
	AUTGameMode* UTGameMode = GetWorld()->GetAuthGameMode<AUTGameMode>();
	return (UTGameMode == nullptr || !UTGameMode->OverridePickupQuery(Other, InventoryType, this, bAllowPickup)) ? bDefaultAllowPickup : bAllowPickup;
}

void AUTDroppedPickup::ProcessTouch_Implementation(APawn* TouchedBy)
{
	if (Role == ROLE_Authority && TouchedBy->Controller != nullptr && AllowPickupBy(TouchedBy, true))
	{
		PlayTakenEffects(TouchedBy); // first allows PlayTakenEffects() to work off Inventory instead of InventoryType if it wants
		GiveTo(TouchedBy);
		AUTGameMode* UTGame = GetWorld()->GetAuthGameMode<AUTGameMode>();
		if (UTGame != nullptr && UTGame->NumBots > 0)
		{
			float Radius = 0.0f;
			USoundBase* PickupSound = GetPickupSound();
			if (PickupSound)
			{
				Radius = PickupSound->GetMaxDistance();
				const FSoundAttenuationSettings* Settings = PickupSound->GetAttenuationSettingsToApply();
				if (Settings != nullptr)
				{
					Radius = FMath::Max<float>(Radius, Settings->GetMaxDimension());
				}
			}
			for (FConstControllerIterator It = GetWorld()->GetControllerIterator(); It; ++It)
			{
				if (It->IsValid())
				{
					AUTBot* B = Cast<AUTBot>(It->Get());
					if (B != nullptr)
					{
						B->NotifyPickup(TouchedBy, this, Radius);
					}
				}
			}
		}
		Destroy();
	}
}

void AUTDroppedPickup::GiveTo_Implementation(APawn* Target)
{
	if (Inventory != nullptr && !Inventory->IsPendingKillPending())
	{
		AUTCharacter* C = Cast<AUTCharacter>(Target);
		if (C != nullptr)
		{
			if (Inventory->HandleGivenTo(C))
			{
				Inventory->Destroy();
			}
			else
			{
				AUTInventory* Duplicate = C->FindInventoryType<AUTInventory>(Inventory->GetClass(), true);
				if (Duplicate == nullptr || !Duplicate->StackPickup(Inventory))
				{
					C->AddInventory(Inventory, true);
				}
				else
				{
					Inventory->Destroy();
				}
			}
			//if (Cast<APlayerController>(Target->GetController()) && (!Cast<AUTWeapon>(Inventory) || !C->GetPendingWeapon() || (C->GetPendingWeapon()->GetClass() != Inventory->GetClass())))
			//{
			//	Cast<APlayerController>(Target->GetController())->ClientReceiveLocalizedMessage(UUTPickupMessage::StaticClass(), 0, NULL, NULL, Inventory->GetClass());
			//}
			Inventory = NULL;
		}
	}
}

USoundBase* AUTDroppedPickup::GetPickupSound_Implementation() const
{
	if (Inventory != nullptr)
	{
		return Inventory->PickupSound;
	}
	else if (InventoryType != nullptr)
	{
		return InventoryType.GetDefaultObject()->PickupSound;
	}
	else
	{
		return NULL;
	}
}

void AUTDroppedPickup::PlayTakenEffects_Implementation(APawn* TakenBy)
{
	USoundBase* PickupSound = GetPickupSound();
	if (PickupSound != nullptr)
	{
		UUTGameplayStatics::UTPlaySound(GetWorld(), PickupSound, TakenBy, SRT_All, false, GetActorLocation(), NULL, NULL, false);
	}
	if (GetNetMode() != NM_DedicatedServer)
	{
		UParticleSystem* ParticleTemplate = NULL;
		if (Inventory != nullptr)
		{
			ParticleTemplate = Inventory->PickupEffect;
		}
		else if (InventoryType != nullptr)
		{
			ParticleTemplate = InventoryType.GetDefaultObject()->PickupEffect;
		}
		if (ParticleTemplate != nullptr)
		{
			AUTWorldSettings* WS = Cast<AUTWorldSettings>(GetWorld()->GetWorldSettings());
			if (WS == nullptr || WS->EffectIsRelevant(this, GetActorLocation(), true, false, 10000.0f, 1000.0f))
			{
				UGameplayStatics::SpawnEmitterAtLocation(this, ParticleTemplate, GetActorLocation(), GetActorRotation());
			}
		}
	}
}

void AUTDroppedPickup::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME_CONDITION(AUTDroppedPickup, InventoryType, COND_None);
}


float AUTDroppedPickup::BotDesireability_Implementation(APawn* Asker, AController* RequestOwner, float PathDistance)
{
	if (InventoryType == nullptr)
	{
		return 0.0f;
	}
	else
	{
		// make sure Asker can actually get here before the pickup times out
		float LifeSpan = GetLifeSpan();
		if (LifeSpan > 0.0)
		{
			AUTCharacter* C = Cast<AUTCharacter>(Asker);
			return (C == nullptr || PathDistance / C->UTCharacterMovement->GetMaxWalkSpeed() > LifeSpan) ? 0.0f : InventoryType.GetDefaultObject()->BotDesireability(Asker, RequestOwner, this, PathDistance);
		}
		else
		{
			return InventoryType.GetDefaultObject()->BotDesireability(Asker, RequestOwner, this, PathDistance);
		}
	}
}
float AUTDroppedPickup::DetourWeight_Implementation(APawn* Asker, float PathDistance)
{
	if (InventoryType == nullptr)
	{
		return 0.0f;
	}
	else
	{
		// make sure Asker can actually get here before the pickup times out
		float LifeSpan = GetLifeSpan();
		if (LifeSpan > 0.0)
		{
			AUTCharacter* C = Cast<AUTCharacter>(Asker);
			return (C == nullptr || PathDistance / C->UTCharacterMovement->GetMaxWalkSpeed() > LifeSpan) ? 0.0f : InventoryType.GetDefaultObject()->DetourWeight(Asker, this, PathDistance);
		}
		else
		{
			return InventoryType.GetDefaultObject()->DetourWeight(Asker, this, PathDistance);
		}
	}
}