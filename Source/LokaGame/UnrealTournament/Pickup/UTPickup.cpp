// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "LokaGame.h"
#include "UTPickup.h"
#include "UnrealNetwork.h"
#include "UTRecastNavMesh.h"
#include "UTPickupMessage.h"
#include "UTWorldSettings.h"
#include "UTGameMode.h"
#include "UTGameplayStatics.h"
#include "UTPlayerController.h"
#include "UTBot.h"

FName NAME_Progress(TEXT("Progress"));
FName NAME_RespawnTime(TEXT("RespawnTime"));

void AUTPickup::PostEditImport()
{
	Super::PostEditImport();

	if (!IsPendingKill())
	{
		Collision->OnComponentBeginOverlap.Clear();
		Collision->OnComponentBeginOverlap.AddDynamic(this, &AUTPickup::OnOverlapBegin);
	}
}

AUTPickup::AUTPickup(const FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{
	bCanBeDamaged = false;

	Collision = ObjectInitializer.CreateDefaultSubobject<UCapsuleComponent>(this, TEXT("Capsule"));
	Collision->SetCollisionProfileName(FName(TEXT("Pickup")));
	Collision->InitCapsuleSize(64.0f, 75.0f);
	Collision->SetShouldUpdatePhysicsVolume(false);
	Collision->Mobility = EComponentMobility::Static;
	Collision->OnComponentBeginOverlap.AddDynamic(this, &AUTPickup::OnOverlapBegin);
	RootComponent = Collision;

	TimerEffect = ObjectInitializer.CreateDefaultSubobject<UParticleSystemComponent>(this, TEXT("TimerEffect"));
	if (TimerEffect != nullptr)
	{
		TimerEffect->SetHiddenInGame(true);
		TimerEffect->SetupAttachment(RootComponent);
		TimerEffect->LDMaxDrawDistance = 1024.0f;
		TimerEffect->RelativeLocation.Z = 40.0f;
		TimerEffect->Mobility = EComponentMobility::Static;
		TimerEffect->SetCastShadow(false);
	}
	BaseEffect = ObjectInitializer.CreateOptionalDefaultSubobject<UParticleSystemComponent>(this, TEXT("BaseEffect"));
	if (BaseEffect != nullptr)
	{
		BaseEffect->SetupAttachment(RootComponent);
		BaseEffect->LDMaxDrawDistance = 2048.0f;
		BaseEffect->RelativeLocation.Z = -58.0f;
		BaseEffect->Mobility = EComponentMobility::Static;
	}
	TakenEffectTransform.SetScale3D(FVector(1.0f, 1.0f, 1.0f));
	RespawnEffectTransform.SetScale3D(FVector(1.0f, 1.0f, 1.0f));

	State.bActive = true;
	RespawnTime = 30.0f;
	SetReplicates(true);
	bAlwaysRelevant = true;
	NetUpdateFrequency = 1.0f;
	PrimaryActorTick.bCanEverTick = true;
	PickupMessageString = NSLOCTEXT("PickupMessage", "ItemPickedUp", "Item snagged.");
	bHasTacComView = false;
	TeamSide = 255;
	bOverride_TeamSide = false;
	IconColor = FLinearColor::White;
}

void AUTPickup::SetTacCom(bool bTacComEnabled)
{
	if (bHasTacComView && TimerEffect != nullptr)
	{
		TimerEffect->LDMaxDrawDistance = bTacComEnabled ? 50000.f : 1024.f;
		TimerEffect->CachedMaxDrawDistance = TimerEffect->LDMaxDrawDistance;
		TimerEffect->MarkRenderStateDirty();
	}
}

void AUTPickup::BeginPlay()
{
	Super::BeginPlay();

	if (BaseEffect != nullptr && BaseTemplateAvailable != nullptr)
	{
		BaseEffect->SetTemplate(BaseTemplateAvailable);
	}

	AUTRecastNavMesh* NavData = GetUTNavData(GetWorld());
	if (NavData != nullptr)
	{
		NavData->AddToNavigation(this);
	}
}

void AUTPickup::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	GetWorldTimerManager().ClearAllTimersForObject(this);
	AUTRecastNavMesh* NavData = GetUTNavData(GetWorld());
	if (NavData != nullptr)
	{
		NavData->RemoveFromNavigation(this);
	}
}

FCanvasIcon AUTPickup::GetMinimapIcon() const
{
	return (MinimapIcon.Texture != nullptr) ? MinimapIcon : HUDIcon;
}

void AUTPickup::Reset_Implementation()
{
	GetWorld()->GetTimerManager().ClearTimer(WakeUpTimerHandle);
	if (bDelayedSpawn)
	{
		State.bRepTakenEffects = false;
		StartSleeping();
	}
	else if (!State.bActive)
	{
		WakeUp();
	}
	bReplicateReset = !bReplicateReset;
}

void AUTPickup::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepHitResult)
{
	APawn* P = Cast<APawn>(OtherActor);
	if (P != nullptr && !P->GetTearOff() && !GetWorld()->LineTraceTestByChannel(P->GetActorLocation(), GetActorLocation(), ECC_Pawn, FCollisionQueryParams(), WorldResponseParams))
	{
		ProcessTouch(P);
	}
}

bool AUTPickup::FlashOnMinimap_Implementation()
{
	return false;
}

bool AUTPickup::AllowPickupBy_Implementation(APawn* Other, bool bDefaultAllowPickup)
{
	AUTCharacter* UTC = Cast<AUTCharacter>(Other);
	bDefaultAllowPickup = bDefaultAllowPickup && (UTC == nullptr || UTC->bCanPickupItems);
	bool bAllowPickup = bDefaultAllowPickup;
	AUTGameMode* UTGameMode = GetWorld()->GetAuthGameMode<AUTGameMode>();
	return (UTGameMode == nullptr || !UTGameMode->OverridePickupQuery(Other, NULL, this, bAllowPickup)) ? bDefaultAllowPickup : bAllowPickup;
}

void AUTPickup::ProcessTouch_Implementation(APawn* TouchedBy)
{
	if (Role == ROLE_Authority && State.bActive && TouchedBy->Controller != nullptr && AllowPickupBy(TouchedBy, true))
	{
		GiveTo(TouchedBy);
		AUTGameMode* UTGameMode = GetWorld()->GetAuthGameMode<AUTGameMode>();
		if (UTGameMode != nullptr)
		{
			AUTPlayerState* PickedUpBy = Cast<AUTPlayerState>(TouchedBy->GetPlayerState());
			UTGameMode->ScorePickup(this, PickedUpBy, LastPickedUpBy);
			LastPickedUpBy = PickedUpBy;

			if (UTGameMode->NumBots > 0)
			{
				float Radius = 0.0f;
				if (TakenSound != nullptr)
				{
					Radius = TakenSound->GetMaxDistance();
					const FSoundAttenuationSettings* Settings = TakenSound->GetAttenuationSettingsToApply();
					if (Settings != nullptr)
					{
						Radius = FMath::Max<float>(Radius, Settings->GetMaxDimension());
					}
				}
				for (FConstControllerIterator It = GetWorld()->GetControllerIterator(); It; ++It)
				{
					if (It->IsValid())
					{
						AUTBot* B = Cast<AUTBot>(It->Get());
						if (B != nullptr)
						{
							B->NotifyPickup(TouchedBy, this, Radius);
						}
					}
				}
			}
		}

		PlayTakenEffects(true);
		StartSleeping();
	}
}

void AUTPickup::GiveTo_Implementation(APawn* Target)
{
	AUTPlayerController* UTPC = (Target != nullptr) ? Cast<AUTPlayerController>(Target->GetController()) : nullptr;
	if (UTPC)
	{
		UTPC->ClientReceiveLocalizedMessage(UUTPickupMessage::StaticClass(), 0, NULL, NULL, GetClass());
	}
}

void AUTPickup::SetPickupHidden(bool bNowHidden)
{
	if (TakenHideTags.Num() == 0 || RootComponent == nullptr)
	{
		SetActorHiddenInGame(bNowHidden);
	}
	else
	{
		TArray<USceneComponent*> Components;
		RootComponent->GetChildrenComponents(true, Components);
		for (int32 i = 0; i < Components.Num(); i++)
		{
			for (int32 j = 0; j < TakenHideTags.Num(); j++)
			{
				if (Components[i]->ComponentHasTag(TakenHideTags[j]))
				{
					Components[i]->SetVisibility(!bNowHidden);
				}
			}
		}
	}
}

void AUTPickup::StartSleeping_Implementation()
{
	SetPickupHidden(true);
	SetActorEnableCollision(false);
	if (RespawnTime > 0.0f)
	{
		if (!bFixedRespawnInterval || !GetWorld()->GetTimerManager().IsTimerActive(WakeUpTimerHandle))
		{
			GetWorld()->GetTimerManager().SetTimer(WakeUpTimerHandle, this, &AUTPickup::WakeUpTimer, RespawnTime, false);
		}
		if (TimerEffect != nullptr && TimerEffect->Template != nullptr)
		{
			// FIXME: workaround for particle bug; screen facing particles don't handle negative scale correctly
			FVector FixedScale = TimerEffect->GetComponentScale();
			FixedScale = FVector(FMath::Abs<float>(FixedScale.X), FMath::Abs<float>(FixedScale.Y), FMath::Abs<float>(FixedScale.Z));
			TimerEffect->SetWorldScale3D(FixedScale);

			TimerEffect->SetFloatParameter(NAME_Progress, 0.0f);
			TimerEffect->SetFloatParameter(NAME_RespawnTime, RespawnTime);
			TimerEffect->SetHiddenInGame(false);
			PrimaryActorTick.SetTickFunctionEnable(true);
		}
	}

	// this needs to be done redundantly because not all paths for all pickups call both StartSleeping() and PlayTakenEffects()
	if (BaseEffect != nullptr && BaseTemplateTaken != nullptr)
	{
		BaseEffect->SetTemplate(BaseTemplateTaken);
	}

	if (Role == ROLE_Authority)
	{
		State.bActive = false;
		State.ChangeCounter++;
		ForceNetUpdate();
	}
}
void AUTPickup::PlayTakenEffects(bool bReplicate)
{
	if (bReplicate && Role == ROLE_Authority)
	{
		State.bRepTakenEffects = true;
		ForceNetUpdate();
	}
	if (GetNetMode() != NM_DedicatedServer)
	{
		AUTWorldSettings* WS = Cast<AUTWorldSettings>(GetWorld()->GetWorldSettings());
		if (WS == nullptr || WS->EffectIsRelevant(this, GetActorLocation(), true, false, 10000.0f, 1000.0f, false))
		{
			UParticleSystemComponent* PSC = UGameplayStatics::SpawnEmitterAttached(TakenParticles, RootComponent, NAME_None, TakenEffectTransform.GetLocation(), TakenEffectTransform.GetRotation().Rotator());
			if (PSC != nullptr)
			{
				PSC->SetRelativeScale3D(TakenEffectTransform.GetScale3D());
			}
		}
		if (BaseEffect != nullptr && BaseTemplateTaken != nullptr)
		{
			BaseEffect->SetTemplate(BaseTemplateTaken);
		}
		UUTGameplayStatics::UTPlaySound(GetWorld(), TakenSound, this, SRT_None, false, FVector::ZeroVector, NULL, NULL, false);
	}
}
void AUTPickup::WakeUp_Implementation()
{
	SetPickupHidden(false);
	GetWorld()->GetTimerManager().ClearTimer(WakeUpTimerHandle);
	if (bFixedRespawnInterval)
	{
		// start timer for next time
		GetWorld()->GetTimerManager().SetTimer(WakeUpTimerHandle, this, &AUTPickup::WakeUpTimer, RespawnTime, false);
		if (bFixedRespawnInterval && Role == ROLE_Authority)
		{
			bReplicateReset = !bReplicateReset;
		}
	}

	PrimaryActorTick.SetTickFunctionEnable(GetClass()->GetDefaultObject<AUTPickup>()->PrimaryActorTick.bStartWithTickEnabled);
	if (TimerEffect != nullptr)
	{
		TimerEffect->SetHiddenInGame(true);
	}

	if (Role == ROLE_Authority)
	{
		State.bActive = true;
		State.bRepTakenEffects = false;
		State.ChangeCounter++;
		ForceNetUpdate();
		LastRespawnTime = GetWorld()->TimeSeconds;
	}

	PlayRespawnEffects();

	// last so if a player is already touching we're fully ready to act on it
	SetActorEnableCollision(true);
}
void AUTPickup::WakeUpTimer()
{
	if (Role == ROLE_Authority)
	{
		if (!bFixedRespawnInterval || !State.bActive)
		{
			WakeUp();
		}
	}
	else
	{
		// it's possible we're out of sync, so set up a state that indicates the pickup should respawn any time now, but isn't yet available
		if (TimerEffect != nullptr)
		{
			TimerEffect->SetFloatParameter(NAME_Progress, 0.99f);
		}
	}
}
void AUTPickup::PlayRespawnEffects()
{
	// TODO: EffectIsRelevant() ?
	if (GetNetMode() != NM_DedicatedServer)
	{
		UParticleSystemComponent* PSC = UGameplayStatics::SpawnEmitterAttached(RespawnParticles, RootComponent, NAME_None, RespawnEffectTransform.GetLocation(), RespawnEffectTransform.GetRotation().Rotator());
		if (PSC != nullptr)
		{
			PSC->SetRelativeScale3D(RespawnEffectTransform.GetScale3D());
		}
		if (BaseEffect != nullptr && BaseTemplateAvailable != nullptr)
		{
			BaseEffect->SetTemplate(BaseTemplateAvailable);
		}
		UUTGameplayStatics::UTPlaySound(GetWorld(), RespawnSound, this, SRT_None);
	}
}

float AUTPickup::GetRespawnTimeOffset(APawn* Asker) const
{
	if (State.bActive)
	{
		return LastRespawnTime - GetWorld()->TimeSeconds;
	}
	else
	{
		float OutRespawnTime = GetWorldTimerManager().GetTimerRemaining(WakeUpTimerHandle);
		return (OutRespawnTime <= 0.0f) ? FLT_MAX : OutRespawnTime;
	}
}

void AUTPickup::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	UWorld* World = GetWorld();
	if (RespawnTime > 0.0f && !State.bActive && World->GetTimerManager().IsTimerActive(WakeUpTimerHandle))
	{
		if (TimerEffect != nullptr)
		{
			TimerEffect->SetFloatParameter(NAME_Progress, 1.0f - World->GetTimerManager().GetTimerRemaining(WakeUpTimerHandle) / RespawnTime);
		}
	}
}

float AUTPickup::BotDesireability_Implementation(APawn* Asker, AController* RequestOwner, float PathDistance)
{
	return BaseDesireability;
}
float AUTPickup::DetourWeight_Implementation(APawn* Asker, float PathDistance)
{
	return 0.0f;
}
bool AUTPickup::IsSuperDesireable_Implementation(AController* RequestOwner, float CalculatedDesire)
{
	return FMath::Max<float>(BaseDesireability, CalculatedDesire) >= 1.0f;
}

static FPickupReplicatedState PreRepState;

void AUTPickup::PreNetReceive()
{
	PreRepState = State;
	Super::PreNetReceive();
}
void AUTPickup::PostNetReceive()
{
	Super::PostNetReceive();

	// make sure not to re-invoke WakeUp()/StartSleeping() if only bRepTakenEffects has changed
	// since that will reset timers on the client incorrectly
	if (PreRepState.bActive != State.bActive || PreRepState.ChangeCounter != State.ChangeCounter)
	{
		if (State.bActive)
		{
			WakeUp();
		}
		else
		{
			StartSleeping();
		}
	}
	if (!State.bActive && State.bRepTakenEffects)
	{
		PlayTakenEffects(true);
	}
}

void AUTPickup::OnRep_RespawnTimeRemaining()
{
	if (!State.bActive)
	{
		GetWorld()->GetTimerManager().SetTimer(WakeUpTimerHandle, this, &AUTPickup::WakeUpTimer, RespawnTimeRemaining, false);
	}
}

void AUTPickup::OnRep_Reset()
{
	// this is only important on non-initial cases as RespawnTimeRemaining handles the others
	if (bFixedRespawnInterval && CreationTime < GetWorld()->TimeSeconds)
	{
		GetWorld()->GetTimerManager().SetTimer(WakeUpTimerHandle, this, &AUTPickup::WakeUpTimer, RespawnTime, false);
	}
}

void AUTPickup::PreReplication(IRepChangedPropertyTracker & ChangedPropertyTracker)
{
	Super::PreReplication(ChangedPropertyTracker);

	RespawnTimeRemaining = GetWorld()->GetTimerManager().GetTimerRemaining(WakeUpTimerHandle);
}

void AUTPickup::GetLifetimeReplicatedProps(TArray<class FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AUTPickup, RespawnTime);
	DOREPLIFETIME(AUTPickup, bFixedRespawnInterval);
	// warning: we rely on this ordering
	DOREPLIFETIME(AUTPickup, bReplicateReset);
	DOREPLIFETIME(AUTPickup, State);
	DOREPLIFETIME_CONDITION(AUTPickup, RespawnTimeRemaining, COND_InitialOnly);
}

void AUTPickup::PrecacheTutorialAnnouncements(UUTAnnouncer* Announcer) const
{
	for (int32 i = 0; i < TutorialAnnouncements.Num(); i++)
	{
		Announcer->PrecacheAnnouncement(TutorialAnnouncements[i]);
	}
}

FName AUTPickup::GetTutorialAnnouncement(int32 Switch) const
{
	return (Switch < TutorialAnnouncements.Num()) ? TutorialAnnouncements[Switch] : NAME_None;
}
