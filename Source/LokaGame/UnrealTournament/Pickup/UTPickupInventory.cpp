// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#include "LokaGame.h"
#include "UTPickup.h"
#include "UTPickupInventory.h"
#include "UnrealNetwork.h"
#include "UTPickupMessage.h"
#include "UTWorldSettings.h"

#include "UTGameMode.h"
#include "UTGameState.h"


#include "UTTeamInfo.h"
#include "UTPlayerController.h"

AUTPickupInventory::AUTPickupInventory(const FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{
	FloatHeight = 50.0f;
	bAllowRotatingPickup = true;
	bHasTacComView = true;
	bHasEverSpawned = false;
	bNotifySpawnForOffense = true;
	bNotifySpawnForDefense = true;
}

void AUTPickupInventory::BeginPlay()
{
	AActor::BeginPlay(); // skip UTPickup as SetInventoryType() will handle delayed spawn

	if (BaseEffect != nullptr && BaseTemplateAvailable != nullptr)
	{
		BaseEffect->SetTemplate(BaseTemplateAvailable);
	}

	if (Role == ROLE_Authority)
	{
		SetInventoryType(InventoryType);
	}
	else
	{
		InventoryTypeUpdated();
	}

	AUTRecastNavMesh* NavData = GetUTNavData(GetWorld());
	if (NavData != nullptr)
	{
		NavData->AddToNavigation(this);
	}
}

#if WITH_EDITOR
void AUTPickupInventory::CreateEditorPickupMesh()
{
	if (GetWorld() != nullptr && GetWorld()->WorldType == EWorldType::Editor)
	{
		CreatePickupMesh(this, EditorMesh, InventoryType, FloatHeight, RotationOffset, false);
		if (EditorMesh != nullptr)
		{
			EditorMesh->SetHiddenInGame(true);
		}
	}
}
void AUTPickupInventory::PreEditUndo()
{
	UnregisterComponentTree(EditorMesh);
	EditorMesh = NULL;
	Super::PreEditUndo();
}
void AUTPickupInventory::PostEditUndo()
{
	Super::PostEditUndo();
	CreateEditorPickupMesh();
}
void AUTPickupInventory::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);
	CreateEditorPickupMesh();
}
void AUTPickupInventory::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);

	// we have a special class for weapons so don't allow setting them here
	if (PropertyChangedEvent.Property == nullptr || PropertyChangedEvent.Property->GetFName() == FName(TEXT("InventoryType")))
	{
		//if (InventoryType->IsChildOf(AUTWeapon::StaticClass()))
		//{
		//	InventoryType = NULL;
		//	FMessageDialog::Open(EAppMsgType::Ok, FText::FromString(TEXT("Use UTPickupWeapon for weapon pickups.")));
		//}
	}

	CreateEditorPickupMesh();
}
#endif

void AUTPickupInventory::SetInventoryType(TSubclassOf<AUTInventory> NewType)
{
	InventoryType = NewType;
	if (InventoryType != nullptr)
	{
		AUTGameMode* Game = GetWorld()->GetAuthGameMode<AUTGameMode>();
		RespawnTime = Game ? Game->OverrideRespawnTime(InventoryType) : InventoryType.GetDefaultObject()->RespawnTime;
		bDelayedSpawn = InventoryType.GetDefaultObject()->bDelayedSpawn;
		BaseDesireability = InventoryType.GetDefaultObject()->BasePickupDesireability;
		bFixedRespawnInterval = InventoryType.GetDefaultObject()->bFixedRespawnInterval;
	}
	else
	{
		RespawnTime = 0.0f;
		bFixedRespawnInterval = false;
	}
	InventoryTypeUpdated();
	if (Role == ROLE_Authority && GetWorld()->GetAuthGameMode<AUTGameMode>() != nullptr && GetWorld()->GetAuthGameMode<AUTGameMode>()->HasMatchStarted())
	{
		if (InventoryType == nullptr || bDelayedSpawn)
		{
			StartSleeping();
		}
		else
		{
			WakeUp();
		}
	}
}

/** recursively instance anything attached to the pickup mesh template */
static void CreatePickupMeshAttachments(AActor* Pickup, UClass* PickupInventoryType, USceneComponent* CurrentAttachment, FName TemplateName, const TArray<USceneComponent*>& NativeCompList, const TArray<USCS_Node*>& BPNodes)
{
	for (int32 i = 0; i < NativeCompList.Num(); i++)
	{
		if (NativeCompList[i]->GetAttachParent() == CurrentAttachment)
		{
			USceneComponent* NewComp = NewObject<USceneComponent>(Pickup, NativeCompList[i]->GetClass(), NAME_None, RF_NoFlags, NativeCompList[i]);			
			NewComp->DetachFromComponent(FDetachmentTransformRules::KeepRelativeTransform);
						
			TArray<USceneComponent*> ChildComps = NewComp->GetAttachChildren();
			for (USceneComponent* Child : ChildComps)
			{
				Child->DetachFromComponent(FDetachmentTransformRules::KeepRelativeTransform);
			}

			UPrimitiveComponent* Prim = Cast<UPrimitiveComponent>(NewComp);
			if (Prim != nullptr)
			{
				Prim->SetCollisionEnabled(ECollisionEnabled::NoCollision);
				Prim->SetShouldUpdatePhysicsVolume(false);
			}
			NewComp->RegisterComponent();
			NewComp->AttachToComponent(CurrentAttachment, FAttachmentTransformRules::KeepRelativeTransform, NewComp->GetAttachSocketName());
			// recurse
			CreatePickupMeshAttachments(Pickup, PickupInventoryType, NewComp, NativeCompList[i]->GetFName(), NativeCompList, BPNodes);
		}
	}
	for (int32 i = 0; i < BPNodes.Num(); i++)
	{
		USceneComponent* ComponentTemplate = Cast<USceneComponent>(BPNodes[i]->GetActualComponentTemplate(Cast<UBlueprintGeneratedClass>(PickupInventoryType)));
		if (BPNodes[i]->ComponentTemplate != nullptr && BPNodes[i]->ParentComponentOrVariableName == TemplateName)
		{
			USceneComponent* NewComp = NewObject<USceneComponent>(Pickup, BPNodes[i]->ComponentTemplate->GetClass(), NAME_None, RF_NoFlags, BPNodes[i]->ComponentTemplate);			
			NewComp->DetachFromComponent(FDetachmentTransformRules::KeepRelativeTransform);

			TArray<USceneComponent*> ChildComps = NewComp->GetAttachChildren();
			for (USceneComponent* Child : ChildComps)
			{
				Child->DetachFromComponent(FDetachmentTransformRules::KeepRelativeTransform);
			}

			UPrimitiveComponent* Prim = Cast<UPrimitiveComponent>(NewComp);
			if (Prim != nullptr)
			{
				Prim->SetCollisionEnabled(ECollisionEnabled::NoCollision);
				Prim->SetShouldUpdatePhysicsVolume(false);
			}
			NewComp->bAutoRegister = false;
			NewComp->RegisterComponent();
			NewComp->AttachToComponent(CurrentAttachment, FAttachmentTransformRules::KeepRelativeTransform, BPNodes[i]->AttachToName);
			// recurse
			CreatePickupMeshAttachments(Pickup, PickupInventoryType, NewComp, BPNodes[i]->GetVariableName(), NativeCompList, BPNodes);

			// The behavior of PIE has changed in 4.13. When the object was duplicated for PIE in previous versions, it would not have the particle system components.
			// Just make the editor ones invisible, it breaks real time preview, but would rather have PIE work.
			if (Cast<UParticleSystemComponent>(NewComp) && Pickup->GetWorld()->WorldType == EWorldType::Editor)
			{
				Cast<UParticleSystemComponent>(NewComp)->SetVisibility(false, true);
			}
		}
	}
}

void AUTPickupInventory::CreatePickupMesh(AActor* Pickup, UMeshComponent*& PickupMesh, TSubclassOf<AUTInventory> PickupInventoryType, float MeshFloatHeight, const FRotator& RotationOffset, bool bAllowRotating)
{
	if (PickupInventoryType == nullptr)
	{
		if (PickupMesh != nullptr)
		{
			UnregisterComponentTree(PickupMesh);
			PickupMesh = NULL;
		}
	}
	else
	{
		FVector OverrideScale(FVector::ZeroVector);
		UMeshComponent* NewMesh = PickupInventoryType.GetDefaultObject()->GetPickupMeshTemplate(OverrideScale);
		if (NewMesh == nullptr)
		{
			if (PickupMesh != nullptr)
			{
				UnregisterComponentTree(PickupMesh);
				PickupMesh = NULL;
			}
		}
		else
		{
			USkeletalMeshComponent* SkelTemplate = Cast<USkeletalMeshComponent>(NewMesh);
			UStaticMeshComponent* StaticTemplate = Cast<UStaticMeshComponent>(NewMesh);
			if (PickupMesh == nullptr || PickupMesh->GetClass() != NewMesh->GetClass() || PickupMesh->GetMaterials() != NewMesh->GetMaterials() ||
				(SkelTemplate != nullptr && ((USkeletalMeshComponent*)PickupMesh)->SkeletalMesh != SkelTemplate->SkeletalMesh) ||
				(StaticTemplate != nullptr && ((UStaticMeshComponent*)PickupMesh)->GetStaticMesh() != StaticTemplate->GetStaticMesh()))
			{
				if (PickupMesh != nullptr)
				{
					UnregisterComponentTree(PickupMesh);
					PickupMesh = NULL;
				}
				PickupMesh = NewObject<UMeshComponent>(Pickup, NewMesh->GetClass(), NAME_None, RF_NoFlags, NewMesh);
				PickupMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
				PickupMesh->SetShouldUpdatePhysicsVolume(false);
				PickupMesh->bUseAttachParentBound = false;
				PickupMesh->DetachFromComponent(FDetachmentTransformRules::KeepRelativeTransform);

				TArray<USceneComponent*> ChildComps = PickupMesh->GetAttachChildren();
				for (USceneComponent* Child : ChildComps)
				{
					Child->DetachFromComponent(FDetachmentTransformRules::KeepRelativeTransform);
				}

				PickupMesh->RelativeRotation = FRotator::ZeroRotator;
				PickupMesh->RelativeLocation.Z += MeshFloatHeight;
				if (!OverrideScale.IsZero())
				{
					PickupMesh->SetWorldScale3D(OverrideScale);
				}
				if (SkelTemplate != nullptr)
				{
					((USkeletalMeshComponent*)PickupMesh)->bForceRefpose = true;
				}
				PickupMesh->bAutoRegister = false;
				PickupMesh->RegisterComponent();
				PickupMesh->AttachToComponent(Pickup->GetRootComponent(), FAttachmentTransformRules::KeepRelativeTransform);
				FVector Offset = Pickup->GetRootComponent()->GetComponentTransform().InverseTransformVectorNoScale(PickupMesh->Bounds.Origin - PickupMesh->GetComponentToWorld().GetLocation());
				PickupMesh->SetRelativeLocation(PickupMesh->GetRelativeTransform().GetLocation() - Offset);
				PickupMesh->SetRelativeRotation(PickupMesh->RelativeRotation + RotationOffset);
				// if there's a rotation component, set it up to rotate the pickup mesh
				if (bAllowRotating && Pickup->GetWorld()->WorldType != EWorldType::Editor) // not if editor preview, because that will cause the preview component to exist in game and we want it to be editor only
				{
					TArray<URotatingMovementComponent*> RotationComps;
					Pickup->GetComponents<URotatingMovementComponent>(RotationComps);
					if (RotationComps.Num() > 0)
					{
						RotationComps[0]->SetUpdatedComponent(PickupMesh);
						RotationComps[0]->PivotTranslation = Offset;
					}
				}

				// see if the pickup mesh has any attached children we should also instance
				TArray<USceneComponent*> NativeCompList;
				PickupInventoryType.GetDefaultObject()->GetComponents<USceneComponent>(NativeCompList);
				TArray<USCS_Node*> ConstructionNodes;
				{
					TArray<const UBlueprintGeneratedClass*> ParentBPClassStack;
					UBlueprintGeneratedClass::GetGeneratedClassesHierarchy(PickupInventoryType, ParentBPClassStack);
					for (int32 i = ParentBPClassStack.Num() - 1; i >= 0; i--)
					{
						const UBlueprintGeneratedClass* CurrentBPGClass = ParentBPClassStack[i];
						if (CurrentBPGClass->SimpleConstructionScript)
						{
							ConstructionNodes += CurrentBPGClass->SimpleConstructionScript->GetAllNodes();
						}
					}
				}
				CreatePickupMeshAttachments(Pickup, PickupInventoryType, PickupMesh, NewMesh->GetFName(), NativeCompList, ConstructionNodes);
			}
			else if (PickupMesh->GetAttachParent() != Pickup->GetRootComponent())
			{
				PickupMesh->AttachToComponent(Pickup->GetRootComponent(), FAttachmentTransformRules::SnapToTargetNotIncludingScale);
				FVector Offset = Pickup->GetRootComponent()->GetComponentTransform().InverseTransformVectorNoScale(PickupMesh->Bounds.Origin - PickupMesh->GetComponentToWorld().GetLocation());
				PickupMesh->SetRelativeLocation(PickupMesh->GetRelativeTransform().GetLocation() - Offset + FVector(0.0f, 0.0f, MeshFloatHeight));
			}
		}
	}
}

void AUTPickupInventory::InventoryTypeUpdated_Implementation()
{
	CreatePickupMesh(this, Mesh, InventoryType, FloatHeight, RotationOffset, bAllowRotatingPickup);
	if (Mesh)
	{
		Mesh->SetShouldUpdatePhysicsVolume(false);
	}
	if (GhostMeshMaterial != nullptr)
	{
		if (GhostMesh != nullptr)
		{
			UnregisterComponentTree(GhostMesh);
			GhostMesh = NULL;
		}
		if (Mesh != nullptr)
		{
			GhostMesh = DuplicateObject<UMeshComponent>(Mesh, this);
			GhostMesh->DetachFromComponent(FDetachmentTransformRules::KeepRelativeTransform);

			TArray<USceneComponent*> ChildComps = GhostMesh->GetAttachChildren();
			for (USceneComponent* Child : ChildComps)
			{
				Child->DetachFromComponent(FDetachmentTransformRules::KeepRelativeTransform);
			}

			GhostMesh->bRenderCustomDepth = false;
			GhostMesh->bRenderInMainPass = true;
			GhostMesh->CastShadow = false;
			for (int32 i = 0; i < GhostMesh->GetNumMaterials(); i++)
			{
				GhostMesh->SetMaterial(i, GhostMeshMaterial);
				static FName NAME_Normal(TEXT("Normal"));
				UMaterialInterface* OrigMat = Mesh->GetMaterial(i);
				UTexture* NormalTex = NULL;
				if (OrigMat != nullptr && OrigMat->GetTextureParameterValue(NAME_Normal, NormalTex))
				{
					UMaterialInstanceDynamic* MID = GhostMesh->CreateAndSetMaterialInstanceDynamic(i);
					MID->SetTextureParameterValue(NAME_Normal, NormalTex);
				}
			}
			GhostMesh->RegisterComponent();
			GhostMesh->AttachToComponent(Mesh, FAttachmentTransformRules::SnapToTargetIncludingScale);
			if (GhostMesh->bAbsoluteScale) // SnapToTarget doesn't handle absolute...
			{
				GhostMesh->SetWorldScale3D(Mesh->GetComponentScale());
			}
			GhostMesh->SetVisibility(!State.bActive, true);
			GhostMesh->SetShouldUpdatePhysicsVolume(false);
		}
	}
	if (Mesh != nullptr && !State.bActive && Role < ROLE_Authority)
	{
		// if State was received first whole actor might have been hidden, reset everything
		StartSleeping();
		OnRep_RespawnTimeRemaining();
	}

	IconColor = (InventoryType != nullptr) ? InventoryType.GetDefaultObject()->IconColor : FLinearColor::White;
	HUDIcon = (InventoryType != nullptr) ? InventoryType.GetDefaultObject()->HUDIcon : FCanvasIcon();
	MinimapIcon = (InventoryType != nullptr) ? InventoryType.GetDefaultObject()->MinimapIcon : FCanvasIcon();
	TakenSound = (InventoryType != nullptr) ? TakenSound = InventoryType.GetDefaultObject()->PickupSound : GetClass()->GetDefaultObject<AUTPickupInventory>()->TakenSound;
}

void AUTPickupInventory::Reset_Implementation()
{
	bHasEverSpawned = false;
	GetWorldTimerManager().ClearTimer(SpawnVoiceLineTimer);
	if (InventoryType == nullptr)
	{
		StartSleeping();
	}
	else
	{
		Super::Reset_Implementation();
	}
}

void AUTPickupInventory::PlayRespawnEffects()
{
	Super::PlayRespawnEffects();
	if (InventoryType && (InventoryType.GetDefaultObject()->PickupSpawnAnnouncement || (InventoryType.GetDefaultObject()->PickupAnnouncementName != NAME_None)) && (Role==ROLE_Authority))
	{
		AUTGameMode* GM = GetWorld()->GetAuthGameMode<AUTGameMode>();
		if (GM && GM->bAllowPickupAnnouncements && GM->IsMatchInProgress())
		{
			if (InventoryType.GetDefaultObject()->PickupSpawnAnnouncement)
			{
				GM->BroadcastLocalized(this, InventoryType.GetDefaultObject()->PickupSpawnAnnouncement, InventoryType.GetDefaultObject()->PickupAnnouncementIndex, nullptr, nullptr, InventoryType.GetDefaultObject());
				bHasEverSpawned = true;
			}
			if (InventoryType.GetDefaultObject()->PickupAnnouncementName != NAME_None)
			{
				GetWorldTimerManager().SetTimer(SpawnVoiceLineTimer, this, &AUTPickupInventory::PlaySpawnVoiceLine, bHasEverSpawned ? 30.f : 10.f);
			}
			bHasEverSpawned = true;
		}
	}
}

void AUTPickupInventory::PlaySpawnVoiceLine()
{
	AUTGameState* GS = GetWorld()->GetGameState<AUTGameState>();
	if (GS && (!GS->IsMatchInProgress()))
	{
		return;
	}

	AUTGameMode* GM = GetWorld()->GetAuthGameMode<AUTGameMode>();
	if (GM && GM->bAllowPickupAnnouncements)
	{
		// find player to announce this pickup 
		AUTPlayerState* Speaker = nullptr;
		bool bHasPlayedForRed = false;
		bool bHasPlayedForBlue = false;

		for (FConstControllerIterator Iterator = GetWorld()->GetControllerIterator(); Iterator; ++Iterator)
		{
			AUTPlayerState* UTPS = Cast<AUTPlayerState>((*Iterator)->PlayerState);
			if (UTPS && UTPS->Team)
			{
				if (!bHasPlayedForRed && (UTPS->Team->TeamIndex == 0))
				{
					UTPS->AnnounceStatus(InventoryType.GetDefaultObject()->PickupAnnouncementName, 0);
					bHasPlayedForRed = true;
				}
				else if (!bHasPlayedForBlue && (UTPS->Team->TeamIndex == 1))
				{
					UTPS->AnnounceStatus(InventoryType.GetDefaultObject()->PickupAnnouncementName, 0);
					bHasPlayedForBlue = true;
				}
				if (bHasPlayedForRed && bHasPlayedForBlue)
				{
					break;
				}
			}
		}
		GetWorldTimerManager().SetTimer(SpawnVoiceLineTimer, this, &AUTPickupInventory::PlaySpawnVoiceLine, 25.f + 10.f*FMath::FRand());
	}
}

bool AUTPickupInventory::FlashOnMinimap_Implementation()
{
	return (State.bActive && InventoryType && InventoryType.GetDefaultObject()->PickupSpawnAnnouncement);
}

void AUTPickupInventory::SetPickupHidden(bool bNowHidden)
{
	if (GetNetMode() != NM_DedicatedServer)
	{
		if (Mesh != nullptr)
		{
			if (GhostMesh != nullptr)
			{
				Mesh->SetRenderInMainPass(!bNowHidden);
				Mesh->SetRenderCustomDepth(bNowHidden);
				Mesh->CastShadow = !bNowHidden;
				for (USceneComponent* Child : Mesh->GetAttachChildren())
				{
					Child->SetVisibility(!bNowHidden, true);
				}
				GhostMesh->SetVisibility(bNowHidden, true);
			}
			else
			{
				Mesh->SetHiddenInGame(bNowHidden, true);
				Mesh->SetVisibility(!bNowHidden, true);
			}

			// toggle audio components
			TArray<USceneComponent*> ChildComps;
			Mesh->GetChildrenComponents(true, ChildComps);
			for (int32 i = 0; i < ChildComps.Num(); i++)
			{
				UAudioComponent* AC = Cast<UAudioComponent>(ChildComps[i]);
				if (AC != nullptr)
				{
					if (bNowHidden)
					{
						AC->Stop();
					}
					else
					{
						AC->Play();
					}
				}
			}
			// if previously there was no InventoryType or no Mesh then the whole Actor might have been hidden
			SetActorHiddenInGame(false);
		}
		else
		{
			Super::SetPickupHidden(bNowHidden);
		}
	}
}

bool AUTPickupInventory::AllowPickupBy_Implementation(APawn* Other, bool bDefaultAllowPickup)
{
	// TODO: vehicle consideration
	bDefaultAllowPickup = bDefaultAllowPickup && Cast<AUTCharacter>(Other) != nullptr && !((AUTCharacter*)Other)->IsRagdoll() && ((AUTCharacter*)Other)->bCanPickupItems && ((InventoryType == nullptr) || InventoryType.GetDefaultObject()->AllowPickupBy(((AUTCharacter*)Other)));
	bool bAllowPickup = bDefaultAllowPickup;
	AUTGameMode* UTGameMode = GetWorld()->GetAuthGameMode<AUTGameMode>();
	return (UTGameMode == nullptr || !UTGameMode->OverridePickupQuery(Other, InventoryType, this, bAllowPickup)) ? bDefaultAllowPickup : bAllowPickup;
}

void AUTPickupInventory::GiveTo_Implementation(APawn* Target)
{
	AUTCharacter* P = Cast<AUTCharacter>(Target);
	if (P != nullptr && InventoryType != nullptr)
	{
		if (!InventoryType.GetDefaultObject()->HandleGivenTo(P))
		{
			AUTInventory* Existing = P->FindInventoryType(InventoryType, true);
			if (Existing == nullptr || !Existing->StackPickup(NULL))
			{
				FActorSpawnParameters Params;
				Params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				Params.Instigator = P;
				P->AddInventory(GetWorld()->SpawnActor<AUTInventory>(InventoryType, GetActorLocation(), GetActorRotation(), Params), true);
			}
		}
		P->DeactivateSpawnProtection();
		AnnouncePickup(P);

		//Add to the stats pickup count
		const AUTInventory* Inventory = InventoryType.GetDefaultObject();
		if (Inventory->StatsNameCount != NAME_None)
		{
			AUTPlayerState* PS = Cast<AUTPlayerState>(P->GetPlayerState());
			if (PS)
			{
				PS->ModifyStatsValue(Inventory->StatsNameCount, 1);
				if (PS->Team)
				{
					PS->Team->ModifyStatsValue(Inventory->StatsNameCount, 1);
				}

				AUTGameState* GS = GetWorld()->GetGameState<AUTGameState>();
				if (GS != nullptr)
				{
					GS->ModifyStatsValue(Inventory->StatsNameCount, 1);
				}

				//Send the pickup message to the spectators
				AUTGameMode* UTGameMode = GetWorld()->GetAuthGameMode<AUTGameMode>();
				if (UTGameMode != nullptr)
				{
					UTGameMode->BroadcastSpectatorPickup(PS, Inventory->StatsNameCount, Inventory->GetClass());
				}
			}
		}
	}
}

void AUTPickupInventory::PlayTakenEffects(bool bReplicate)
{
	Super::PlayTakenEffects(bReplicate);

	if (GetNetMode() != NM_DedicatedServer && InventoryType != nullptr && Mesh != nullptr)
	{
		AUTWorldSettings* WS = Cast<AUTWorldSettings>(GetWorld()->GetWorldSettings());
		if (WS == nullptr || WS->EffectIsRelevant(this, Mesh->GetComponentLocation(), true, false, 10000.0f, 1000.0f))
		{
			UGameplayStatics::SpawnEmitterAtLocation(this, InventoryType.GetDefaultObject()->PickupEffect, Mesh->GetComponentLocation(), Mesh->GetComponentRotation());
		}
	}
}

void AUTPickupInventory::AnnouncePickup(AUTCharacter* P)
{
	GetWorldTimerManager().ClearTimer(SpawnVoiceLineTimer);
	AUTGameMode* GM = GetWorld()->GetAuthGameMode<AUTGameMode>();
	AUTPlayerController* UTPC = (P != nullptr) ? Cast<AUTPlayerController>(P->GetController()) : nullptr;
	/*if (UTPC)
	{
		UTPC->ClientReceiveLocalizedMessage(UUTPickupMessage::StaticClass(), 0, P->GetPlayerState(), NULL, InventoryType);
		if (GM && GM->bBasicTrainingGame && !GM->bDamageHurtsHealth)
		{
			for (int32 Index = 0; Index < TutorialAnnouncements.Num(); Index++)
			{
				UTPC->PlayTutorialAnnouncement(Index, this);
			}
			for (int32 Index = 0; Index < InventoryType->GetDefaultObject<AUTInventory>()->TutorialAnnouncements.Num(); Index++)
			{
				UTPC->PlayTutorialAnnouncement(Index, InventoryType->GetDefaultObject());
			}
		}
	}*/
	if (GM && GM->bAllowPickupAnnouncements && InventoryType && (InventoryType.GetDefaultObject()->PickupAnnouncementName != NAME_None))
	{
		AUTPlayerState* PS = Cast<AUTPlayerState>(P->GetPlayerState());
		if (PS)
		{
			PS->AnnounceStatus(InventoryType.GetDefaultObject()->PickupAnnouncementName, 1, true);
		}
	}
}

void AUTPickupInventory::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME_CONDITION(AUTPickupInventory, InventoryType, COND_None);
}