// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Pickup/UTPickup.h"
#include "Weapon/ItemWeaponModelId.h"
#include "Material/ItemMaterialModelId.h"
#include "Module/ItemModuleModelId.h"
#include "Interfaces/IconsDrawInterface.h"
#include "UTPickup_Weapon.generated.h"


class UItemWeaponEntity;

UCLASS()
class LOKAGAME_API AUTPickup_Weapon : public AUTPickup, public IIconsDrawInterface
{
	GENERATED_BODY()
	
public:

	AUTPickup_Weapon(const FObjectInitializer& ObjectInitializer);
	virtual void PostInitializeComponents() override;

	virtual bool AllowPickupBy_Implementation(APawn* Other, bool bDefaultAllowPickup) override;
	virtual void GiveTo_Implementation(APawn* Target) override;

	virtual void StartSleeping_Implementation() override;
	virtual void WakeUp_Implementation() override;

	virtual float BotDesireability_Implementation(APawn* Asker, AController* RequestOwner, float TotalDistance) override;
	virtual float DetourWeight_Implementation(APawn* Asker, float TotalDistance) override;

protected:

	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	virtual void Tick(float DeltaTime) override;
	virtual void SetPickupHidden(bool bNowHidden) override;

	UFUNCTION(BlueprintCallable, Category = Effects)
	virtual void InitializeMesh();
	
	void GiveWeaponTo(class AUTCharacter* Pawn, class UItemWeaponEntity* InInstance, const TArray<TEnumAsByte<ItemModuleModelId::Type>> InModules, const TMap<FString, float> InModifers, const ItemMaterialModelId::Type InMaterial);

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Effects)
		USkeletalMeshComponent* WeaponMesh;

	UPROPERTY(EditAnywhere, Category = Pickup)
		TEnumAsByte<ItemWeaponModelId::Type> Weapon;

	UPROPERTY(EditAnywhere, Category = Pickup)
		TArray<TEnumAsByte<ItemModuleModelId::Type>> Modules;

	UPROPERTY(EditAnywhere, Category = Pickup)
		TMap<FString, float> Modifers;

	UPROPERTY(EditAnywhere, Category = Pickup)
		TEnumAsByte<ItemMaterialModelId::Type> Material;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Effects)
		UMaterialInterface* GhostMeshMaterial;

	UPROPERTY(BlueprintReadOnly, Category = Instance)
		UItemWeaponEntity* WeaponInstance;

public:

	//=====================================[ Alternative draw

	virtual void DrawMinimapIcon_Implementation(AHUD* InOwnerHud, UCanvas* InCanvas, FVector2D InPosition, const float InScale = 1.0f) override;
	virtual void DrawRadarIcon_Implementation(AHUD* InOwnerHud, UCanvas* InCanvas, FVector2D InPosition, const float InScale) override {}
	virtual void DrawWorldIcon_Implementation(AHUD* InOwnerHud, UCanvas* InCanvas, FVector2D InPosition, const float InScale) override {}
};
