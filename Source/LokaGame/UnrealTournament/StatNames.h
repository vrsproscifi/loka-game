// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once

extern LOKAGAME_API const FName NAME_AttackerScore;
extern LOKAGAME_API const FName NAME_DefenderScore;
extern LOKAGAME_API const FName NAME_SupporterScore;
extern LOKAGAME_API const FName NAME_TeamKills;

extern LOKAGAME_API const FName NAME_UDamageTime;
extern LOKAGAME_API const FName NAME_BerserkTime;
extern LOKAGAME_API const FName NAME_InvisibilityTime;
extern LOKAGAME_API const FName NAME_UDamageCount;
extern LOKAGAME_API const FName NAME_BerserkCount;
extern LOKAGAME_API const FName NAME_InvisibilityCount;
extern LOKAGAME_API const FName NAME_KegCount;
extern LOKAGAME_API const FName NAME_BootJumps;
extern LOKAGAME_API const FName NAME_ShieldBeltCount;
extern LOKAGAME_API const FName NAME_ArmorVestCount;
extern LOKAGAME_API const FName NAME_ArmorPadsCount;
extern LOKAGAME_API const FName NAME_HelmetCount;

extern LOKAGAME_API const FName NAME_SkillRating;
extern LOKAGAME_API const FName NAME_TDMSkillRating;
extern LOKAGAME_API const FName NAME_DMSkillRating;
extern LOKAGAME_API const FName NAME_CTFSkillRating;
extern LOKAGAME_API const FName NAME_ShowdownSkillRating;
extern LOKAGAME_API const FName NAME_FlagRunSkillRating;
extern LOKAGAME_API const FName NAME_RankedDuelSkillRating;
extern LOKAGAME_API const FName NAME_RankedCTFSkillRating;
extern LOKAGAME_API const FName NAME_RankedShowdownSkillRating;

extern LOKAGAME_API const FName NAME_MatchesPlayed;
extern LOKAGAME_API const FName NAME_MatchesQuit;
extern LOKAGAME_API const FName NAME_TimePlayed;
extern LOKAGAME_API const FName NAME_Wins;
extern LOKAGAME_API const FName NAME_Losses;
extern LOKAGAME_API const FName NAME_Kills;
extern LOKAGAME_API const FName NAME_Deaths;
extern LOKAGAME_API const FName NAME_Suicides;

extern LOKAGAME_API const FName NAME_MultiKillLevel0;
extern LOKAGAME_API const FName NAME_MultiKillLevel1;
extern LOKAGAME_API const FName NAME_MultiKillLevel2;
extern LOKAGAME_API const FName NAME_MultiKillLevel3;

extern LOKAGAME_API const FName NAME_SpreeKillLevel0;
extern LOKAGAME_API const FName NAME_SpreeKillLevel1;
extern LOKAGAME_API const FName NAME_SpreeKillLevel2;
extern LOKAGAME_API const FName NAME_SpreeKillLevel3;
extern LOKAGAME_API const FName NAME_SpreeKillLevel4;

extern LOKAGAME_API const FName NAME_ImpactHammerKills;
extern LOKAGAME_API const FName NAME_EnforcerKills;
extern LOKAGAME_API const FName NAME_BioRifleKills;
extern LOKAGAME_API const FName NAME_BioLauncherKills;
extern LOKAGAME_API const FName NAME_ShockBeamKills;
extern LOKAGAME_API const FName NAME_ShockCoreKills;
extern LOKAGAME_API const FName NAME_ShockComboKills;
extern LOKAGAME_API const FName NAME_LinkKills;
extern LOKAGAME_API const FName NAME_LinkBeamKills;
extern LOKAGAME_API const FName NAME_MinigunKills;
extern LOKAGAME_API const FName NAME_MinigunShardKills;
extern LOKAGAME_API const FName NAME_FlakShardKills;
extern LOKAGAME_API const FName NAME_FlakShellKills;
extern LOKAGAME_API const FName NAME_RocketKills;
extern LOKAGAME_API const FName NAME_SniperKills;
extern LOKAGAME_API const FName NAME_SniperHeadshotKills;
extern LOKAGAME_API const FName NAME_RedeemerKills;
extern LOKAGAME_API const FName NAME_InstagibKills;
extern LOKAGAME_API const FName NAME_TelefragKills;

extern LOKAGAME_API const FName NAME_ImpactHammerDeaths;
extern LOKAGAME_API const FName NAME_EnforcerDeaths;
extern LOKAGAME_API const FName NAME_BioRifleDeaths;
extern LOKAGAME_API const FName NAME_BioLauncherDeaths;
extern LOKAGAME_API const FName NAME_ShockBeamDeaths;
extern LOKAGAME_API const FName NAME_ShockCoreDeaths;
extern LOKAGAME_API const FName NAME_ShockComboDeaths;
extern LOKAGAME_API const FName NAME_LinkDeaths;
extern LOKAGAME_API const FName NAME_LinkBeamDeaths;
extern LOKAGAME_API const FName NAME_MinigunDeaths;
extern LOKAGAME_API const FName NAME_MinigunShardDeaths;
extern LOKAGAME_API const FName NAME_FlakShardDeaths;
extern LOKAGAME_API const FName NAME_FlakShellDeaths;
extern LOKAGAME_API const FName NAME_RocketDeaths;
extern LOKAGAME_API const FName NAME_SniperDeaths;
extern LOKAGAME_API const FName NAME_SniperHeadshotDeaths;
extern LOKAGAME_API const FName NAME_RedeemerDeaths;
extern LOKAGAME_API const FName NAME_InstagibDeaths;
extern LOKAGAME_API const FName NAME_TelefragDeaths;

extern LOKAGAME_API const FName NAME_PlayerXP;

extern LOKAGAME_API const FName NAME_BestShockCombo;
extern LOKAGAME_API const FName NAME_AmazingCombos;
extern LOKAGAME_API const FName NAME_AirRox;
extern LOKAGAME_API const FName NAME_FlakShreds;
extern LOKAGAME_API const FName NAME_AirSnot;

extern LOKAGAME_API const FName NAME_RunDist;
extern LOKAGAME_API const FName NAME_SprintDist;
extern LOKAGAME_API const FName NAME_InAirDist;
extern LOKAGAME_API const FName NAME_SwimDist;
extern LOKAGAME_API const FName NAME_TranslocDist;
extern LOKAGAME_API const FName NAME_NumDodges;
extern LOKAGAME_API const FName NAME_NumWallDodges;
extern LOKAGAME_API const FName NAME_NumJumps;
extern LOKAGAME_API const FName NAME_NumLiftJumps;
extern LOKAGAME_API const FName NAME_NumFloorSlides;
extern LOKAGAME_API const FName NAME_NumWallRuns;
extern LOKAGAME_API const FName NAME_NumImpactJumps;
extern LOKAGAME_API const FName NAME_NumRocketJumps;
extern LOKAGAME_API const FName NAME_SlideDist;
extern LOKAGAME_API const FName NAME_WallRunDist;

extern LOKAGAME_API const FName NAME_EnforcerShots;
extern LOKAGAME_API const FName NAME_BioRifleShots;
extern LOKAGAME_API const FName NAME_BioLauncherShots;
extern LOKAGAME_API const FName NAME_ShockRifleShots;
extern LOKAGAME_API const FName NAME_LinkShots;
extern LOKAGAME_API const FName NAME_MinigunShots;
extern LOKAGAME_API const FName NAME_FlakShots;
extern LOKAGAME_API const FName NAME_RocketShots;
extern LOKAGAME_API const FName NAME_SniperShots;
extern LOKAGAME_API const FName NAME_RedeemerShots;
extern LOKAGAME_API const FName NAME_InstagibShots;

extern LOKAGAME_API const FName NAME_EnforcerHits;
extern LOKAGAME_API const FName NAME_BioRifleHits;
extern LOKAGAME_API const FName NAME_BioLauncherHits;
extern LOKAGAME_API const FName NAME_ShockRifleHits;
extern LOKAGAME_API const FName NAME_LinkHits;
extern LOKAGAME_API const FName NAME_MinigunHits;
extern LOKAGAME_API const FName NAME_FlakHits;
extern LOKAGAME_API const FName NAME_RocketHits;
extern LOKAGAME_API const FName NAME_SniperHits;
extern LOKAGAME_API const FName NAME_RedeemerHits;
extern LOKAGAME_API const FName NAME_InstagibHits;

