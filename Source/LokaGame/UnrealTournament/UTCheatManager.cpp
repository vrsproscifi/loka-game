// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "LokaGame.h"
#include "UTLocalPlayer.h"
#include "UTPlayerState.h"
#include "UTPlayerController.h"
#include "UTHUDWidgetMessage.h"
#include "UTCheatManager.h"
#include "UTSpreeMessage.h"
#include "UTMultiKillMessage.h"

#include "UTGameMode.h"
#include "UTGameState.h"


#include "UTCharacterContent.h"
#include "UTImpactEffect.h"
#include "UTPathTestBot.h"


UUTCheatManager::UUTCheatManager(const class FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

void UUTCheatManager::Ann(int32 Switch)
{

}

void UUTCheatManager::AnnM(float F)
{
	AnnCount = 0;
	AnnDelay = F;
	NextAnn();
}

void UUTCheatManager::NextAnn()
{
	FTimerHandle TempHandle;
	if (AnnCount == 0)
	{
		GetOuterAPlayerController()->ClientReceiveLocalizedMessage(UUTMultiKillMessage::StaticClass(), 3, GetOuterAPlayerController()->GetPlayerState<AUTPlayerState>(), GetOuterAPlayerController()->GetPlayerState<AUTPlayerState>(), NULL);
		GetOuterAPlayerController()->GetWorld()->GetTimerManager().SetTimer(TempHandle, this, &UUTCheatManager::NextAnn, AnnDelay, false);
	}
	else if (AnnCount == 1)
	{
		GetOuterAUTPlayerController()->GetPlayerState<AUTPlayerState>()->AnnounceStatus(StatusMessage::ImOnDefense);
		GetOuterAPlayerController()->GetWorld()->GetTimerManager().SetTimer(TempHandle, this, &UUTCheatManager::NextAnn, AnnDelay, false);
	}
	else if (AnnCount == 2)
	{
		GetOuterAPlayerController()->ClientReceiveLocalizedMessage(UUTSpreeMessage::StaticClass(), 1, GetOuterAPlayerController()->GetPlayerState<AUTPlayerState>(), GetOuterAPlayerController()->GetPlayerState<AUTPlayerState>(), NULL);
		GetOuterAPlayerController()->GetWorld()->GetTimerManager().SetTimer(TempHandle, this, &UUTCheatManager::NextAnn, AnnDelay, false);
	}
	else if (AnnCount == 3)
	{
		GetOuterAUTPlayerController()->GetPlayerState<AUTPlayerState>()->AnnounceStatus(StatusMessage::IGotFlag);
		GetOuterAPlayerController()->GetWorld()->GetTimerManager().SetTimer(TempHandle, this, &UUTCheatManager::NextAnn, AnnDelay, false);
	}
	else if (AnnCount == 5)
	{
		GetOuterAUTPlayerController()->GetPlayerState<AUTPlayerState>()->AnnounceStatus(StatusMessage::NeedBackup);
//		GetOuterAPlayerController()->ClientReceiveLocalizedMessage(AUTProj_Rocket::StaticClass()->GetDefaultObject<AUTProj_Rocket>()->AirRocketRewardClass, 0, GetOuterAPlayerController()->GetPlayerState(), GetOuterAPlayerController()->GetPlayerState(), NULL);
	}
	AnnCount++;
}

void UUTCheatManager::Spread(float Scaling)
{
	AUTCharacter* MyPawn = Cast<AUTCharacter>(GetOuterAPlayerController()->GetPawn());
	if (!MyPawn)
	{
		return;
	}
	//for (TInventoryIterator<AUTWeapon> It(MyPawn); It; ++It)
	//{
	//	for (int32 i = 0; i < It->Spread.Num(); i++)
	//	{
	//		It->Spread[i] = It->GetClass()->GetDefaultObject<AUTWeapon>()->Spread[i] * Scaling;
	//		if (It->Spread[i] != 0.f)
	//		{
	//			UE_LOG(UT, Warning, TEXT("%s New Spread %d is %f"), *It->GetName(), i, It->Spread[i]);
	//		}
	//	}
	//}
}

void UUTCheatManager::Sum()
{

}

void UUTCheatManager::AllAmmo()
{
	AUTCharacter* MyPawn = Cast<AUTCharacter>(GetOuterAPlayerController()->GetPawn());
	if (MyPawn != nullptr)
	{
		MyPawn->AllAmmo();
	}
}

void UUTCheatManager::Gibs()
{
	AUTCharacter* MyPawn = Cast<AUTCharacter>(GetOuterAPlayerController()->GetPawn());
	if (MyPawn != nullptr)
	{
		if (MyPawn->CharacterData.GetDefaultObject()->GibExplosionEffect != nullptr)
		{
			MyPawn->CharacterData.GetDefaultObject()->GibExplosionEffect.GetDefaultObject()->SpawnEffect(MyPawn->GetWorld(), MyPawn->GetRootComponent()->GetComponentTransform(), MyPawn->GetMesh(), MyPawn, NULL, SRT_None);
		}
		for (const FGibSlotInfo& GibInfo : MyPawn->CharacterData.GetDefaultObject()->Gibs)
		{
			MyPawn->SpawnGib(GibInfo, *MyPawn->LastTakeHitInfo.DamageType);
		}
		MyPawn->TeleportTo(MyPawn->GetActorLocation() - 800.f * MyPawn->GetActorRotation().Vector(), MyPawn->GetActorRotation(), true);
	}
}

void UUTCheatManager::UnlimitedAmmo()
{
	//AUTGameMode* Game = GetWorld()->GetAuthGameMode<AUTGameMode>();
	//if (Game != nullptr)
	//{
	//	Game->bAmmoIsLimited = false;
	//}
}

void UUTCheatManager::Loaded()
{
	AUTCharacter* MyPawn = Cast<AUTCharacter>(GetOuterAPlayerController()->GetPawn());
	if (MyPawn != nullptr)
	{
		// grant all weapons that are in memory
		//for (TObjectIterator<UClass> It; It; ++It)
		//{
		//	// make sure we don't use abstract, deprecated, or blueprint skeleton classes
		//	//if (It->IsChildOf(AUTWeapon::StaticClass()) && !It->HasAnyClassFlags(CLASS_Abstract | CLASS_Deprecated | CLASS_NewerVersionExists) && !It->GetName().StartsWith(TEXT("SKEL_")) && !It->IsChildOf(AUTWeap_Translocator::StaticClass()))
		//	//{
		//	//	UClass* TestClass = *It;
		//	//	if (!MyPawn->FindInventoryType(TSubclassOf<AUTInventory>(*It), true))
		//	//	{
		//	//		MyPawn->AddInventory(MyPawn->GetWorld()->SpawnActor<AUTInventory>(*It, FVector(0.0f), FRotator(0, 0, 0)), true);
		//	//	}
		//	//}
		//}
		//AUTWeap_Enforcer* Enforcer = Cast<AUTWeap_Enforcer>(MyPawn->FindInventoryType(TSubclassOf<AUTInventory>(AUTWeap_Enforcer::StaticClass()), false));
		//if (Enforcer)
		//{
		//	Enforcer->BecomeDual();
		//}
		MyPawn->AllAmmo();
	}
}

void UUTCheatManager::ua()
{
	UnlimitedAmmo();
}



void UUTCheatManager::BugItWorker(FVector TheLocation, FRotator TheRotation)
{
	Super::BugItWorker(TheLocation, TheRotation);

	// The teleport doesn't play nice with our physics, so just go to walk mode rather than fall through the world
	Walk();
}


void UUTCheatManager::SetChar(const FString& NewChar)
{
	AUTGameState* GS = GetOuterAPlayerController()->GetWorld()->GetGameState<AUTGameState>();
	for (int32 i = 0; i < GS->PlayerArray.Num(); i++)
	{
		AUTPlayerState* PS = Cast<AUTPlayerState>(GS->PlayerArray[i]);
		if (PS)
		{
			bool bFoundCharacterContent = false;
			FString NewCharPackageName;
			if (FPackageName::SearchForPackageOnDisk(NewChar, &NewCharPackageName))
			{
				NewCharPackageName += TEXT(".") + NewChar + TEXT("_C");
				if (LoadClass<AUTCharacterContent>(NULL, *NewCharPackageName, NULL, LOAD_None, NULL))
				{
					bFoundCharacterContent = true;
				}
			}

			FString NewCharAlt = NewChar + TEXT("CharacterContent");
			if (!bFoundCharacterContent && FPackageName::SearchForPackageOnDisk(NewCharAlt, &NewCharPackageName))
			{
				NewCharPackageName += TEXT(".") + NewCharAlt + TEXT("_C");
				if (LoadClass<AUTCharacterContent>(NULL, *NewCharPackageName, NULL, LOAD_None, NULL))
				{
					bFoundCharacterContent = true;
				}
			}

			FString NewCharAlt2 = NewChar + TEXT("Character");
			if (!bFoundCharacterContent && FPackageName::SearchForPackageOnDisk(NewCharAlt2, &NewCharPackageName))
			{
				NewCharPackageName += TEXT(".") + NewCharAlt2 + TEXT("_C");
				if (LoadClass<AUTCharacterContent>(NULL, *NewCharPackageName, NULL, LOAD_None, NULL))
				{
					bFoundCharacterContent = true;
				}
			}

			if (bFoundCharacterContent)
			{
				PS->ServerSetCharacter(NewCharPackageName);
			}
		}
	}
}

void UUTCheatManager::God()
{
	AUTCharacter* UTChar = Cast<AUTCharacter>(GetOuterAPlayerController()->GetPawn());
	if (UTChar != nullptr)
	{
		if (UTChar->bDamageHurtsHealth)
		{
			UTChar->bDamageHurtsHealth = false;
			GetOuterAPlayerController()->ClientMessage(TEXT("God mode on"));
		}
		else
		{
			UTChar->bDamageHurtsHealth = true;
			GetOuterAPlayerController()->ClientMessage(TEXT("God Mode off"));
		}
	}
	else
	{
		Super::God();
	}
}

void UUTCheatManager::Teleport()
{
	AUTCharacter* UTChar = Cast<AUTCharacter>(GetOuterAPlayerController()->GetPawn());
	if (UTChar != nullptr)
	{
		FHitResult Hit(1.f);
		ECollisionChannel TraceChannel = COLLISION_TRACE_WEAPONNOCHARACTER;
		FCollisionQueryParams QueryParams(GetClass()->GetFName(), true, UTChar);
		FVector StartLocation(0.f);
		FRotator SpawnRotation(0.f);
		UTChar->GetActorEyesViewPoint(StartLocation, SpawnRotation);
		const FVector EndTrace = StartLocation + SpawnRotation.Vector() * 20000.f;

		if (!GetWorld()->LineTraceSingleByChannel(Hit, StartLocation, EndTrace, TraceChannel, QueryParams))
		{
			Hit.Location = EndTrace;
		}
		UTChar->TeleportTo(Hit.Location, SpawnRotation);
	}
}



#if WITH_PROFILE

void UUTCheatManager::LogWebResponse(const FMcpQueryResult& Response)
{
	/*
	if (!Response.bSucceeded)
	{
		GetOuterAPlayerController()->ClientMessage(TEXT("Cheat failed"));
		UE_LOG(UT, Warning, TEXT("%s"), *Response.ErrorMessage.ToString());
	}
	else
	{
		GetOuterAPlayerController()->ClientMessage(TEXT("Cheat succeeded"));
	}*/
}

#endif

void UUTCheatManager::MatchmakeMyParty(int32 PlaylistId)
{
	//UMatchmakingContext* MatchmakingContext = Cast<UMatchmakingContext>(UBlueprintContextLibrary::GetContext(GetWorld(), UMatchmakingContext::StaticClass()));
	//if (MatchmakingContext)
	//{
	//	MatchmakingContext->StartMatchmaking(PlaylistId);
	//}
}

void UUTCheatManager::TestPaths(bool bHighJumps, bool bWallDodges, bool bLifts, bool bLiftJumps)
{
	AUTGameMode* Game = GetWorld()->GetAuthGameMode<AUTGameMode>();
	if (Game != nullptr)
	{
		//Game->KillBots();
		AUTPathTestBot* NewBot = GetWorld()->SpawnActor<AUTPathTestBot>();
		if (NewBot != nullptr)
		{
			static int32 NameCount = 0;
			NewBot->PlayerState->SetPlayerName(FString(TEXT("PathTestBot")) + ((NameCount > 0) ? FString::Printf(TEXT("_%i"), NameCount) : TEXT("")));
			NameCount++;
			AUTPlayerState* PS = Cast<AUTPlayerState>(NewBot->PlayerState);
			if (PS != nullptr)
			{
				PS->bReadyToPlay = true;
			}
			Game->NumBots++;
			//Game->BotFillCount = Game->NumPlayers + Game->NumBots;
			Game->ChangeTeam(NewBot, 0);

			NewBot->GatherTestList(bHighJumps, bWallDodges, bLifts, bLiftJumps);

			GetOuterAPlayerController()->SetViewTarget(NewBot->PlayerState);
		}
	}
}


void UUTCheatManager::DebugAchievement(FString AchievementName)
{
//#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
//	UUTLocalPlayer* LP = Cast<UUTLocalPlayer>(GetOuterAPlayerController()->Player);
//	if (LP)
//	{
//		LP->AwardAchievement(FName(*AchievementName));
//		LP->SaveProgression();
//	}
//#endif
}

void UUTCheatManager::UnlimitedPowerupUses()
{
	AUTPlayerController* UTPlayerController = Cast<AUTPlayerController>(GetOuterAPlayerController());
	if (UTPlayerController && UTPlayerController->GetPlayerState<AUTPlayerState>())
	{
		UTPlayerController->GetPlayerState<AUTPlayerState>()->SetRemainingBoosts(255);
	}
}

void UUTCheatManager::ReportWaitTime(FString RatingType, int32 Seconds)
{
//#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
//	UUTMcpUtils* McpUtils = UUTMcpUtils::Get(GetWorld(), TSharedPtr<const FUniqueNetId>());
//	if (McpUtils == nullptr)
//	{
//		UE_LOG(UT, Warning, TEXT("Unable to load McpUtils. Will not be able to report wait time."));
//		return;
//	}
//
//	// tell MCP about the match to update players' MMRs
//	McpUtils->ReportWaitTime(RatingType, Seconds, [this, RatingType](const FOnlineError& Result) {
//		if (!Result.bSucceeded)
//		{
//			// best we can do is log an error
//			UE_LOG(UT, Warning, TEXT("Failed to report wait time to the server. (%d) %s %s"), Result.HttpResult, *Result.ErrorCode, *Result.ErrorMessage.ToString());
//		}
//		else
//		{
//			UE_LOG(UT, Log, TEXT("Wait time reported to backend"));
//		}
//	});
//#endif
}

void UUTCheatManager::EstimateWaitTimes()
{
//#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
//	UUTMcpUtils* McpUtils = UUTMcpUtils::Get(GetWorld(), TSharedPtr<const FUniqueNetId>());
//	if (McpUtils == nullptr)
//	{
//		UE_LOG(UT, Warning, TEXT("Unable to load McpUtils. Will not be able to report wait time."));
//		return;
//	}
//
//	McpUtils->GetEstimatedWaitTimes([this](const FOnlineError& Result, const FEstimatedWaitTimeInfo& EstimateWaitTimeInfo) {
//		if (!Result.bSucceeded)
//		{
//			// best we can do is log an error
//			UE_LOG(UT, Warning, TEXT("Failed to get estimated wait times from the server. (%d) %s %s"), Result.HttpResult, *Result.ErrorCode, *Result.ErrorMessage.ToString());
//		}
//		else
//		{
//			for (int32 i = 0; i < EstimateWaitTimeInfo.WaitTimes.Num(); i++)
//			{
//				UE_LOG(UT, Log, TEXT("%s %f"), *EstimateWaitTimeInfo.WaitTimes[i].RatingType, EstimateWaitTimeInfo.WaitTimes[i].AverageWaitTimeSecs);
//			}
//		}
//	});
//#endif

}