// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "LokaGame.h"
#include "UTBot.h"
#include "UTCharacter.h"
#include "UTCharacterMovement.h"
#include "UTReachSpec_WallDodge.h"

int32 UUTReachSpec_WallDodge::CostFor(int32 DefaultCost, const FUTPathLink& OwnerLink, APawn* Asker, const FNavAgentProperties& AgentProps, const FUTReachParams& ReachParams, AController* RequestOwner, NavNodeRef StartPoly, const class AUTRecastNavMesh* NavMesh)
{
	// low skill bots avoid wall dodge unless required to get to an area at all
	AUTBot* B = Cast<AUTBot>(RequestOwner);
	if (B != nullptr && B->Skill + B->Personality.MovementAbility < 2.0f)
	{
		DefaultCost += 100000;
	}
	// TODO: maybe some bots prioritize instead? possibly should consider actual difficulty of the jump and dodge instead of a flat number...
	return DefaultCost + 500; // extra for potential risk of fall
}

bool UUTReachSpec_WallDodge::WaitForMove(const FUTPathLink& OwnerLink, APawn* Asker, const FComponentBasedPosition& MovePos, const FRouteCacheItem& Target) const
{
	// check for wall dodge where the wall is actually on walkable space so we need to jump before going off any ledge
	AUTCharacter* UTC = Cast<AUTCharacter>(Asker);
	if ( UTC != nullptr && UTC->UTCharacterMovement->MovementMode == MOVE_Walking &&
			( ((MovePos.Get() - Target.GetLocation(Asker)).IsNearlyZero() && (UTC->GetActorLocation() - WallPoint).Size2D() < UTC->GetSimpleCollisionRadius() * 1.5f) ||
			(MovePos.Get() - WallPoint).Size2D() < UTC->GetSimpleCollisionRadius() * 1.5f ) )
	{
		UTC->UTCharacterMovement->Velocity = (WallPoint - UTC->GetActorLocation()).GetSafeNormal2D() * UTC->UTCharacterMovement->GetMaxWalkSpeed();
		UTC->UTCharacterMovement->DoJump(false);
	}
	return false;
}

bool UUTReachSpec_WallDodge::OverrideAirControl(const FUTPathLink& OwnerLink, APawn* Asker, const FComponentBasedPosition& MovePos, const FRouteCacheItem& Target) const
{
	AUTCharacter* UTC = Cast<AUTCharacter>(Asker);
	if (UTC != nullptr && (MovePos.Get() - Target.GetLocation(Asker)).IsNearlyZero())
	{
		if (!UTC->UTCharacterMovement->bIsDodging && (UTC->GetActorLocation() - WallPoint).Size2D() < UTC->GetSimpleCollisionRadius() * 1.5f)
		{
			// check for the wall dodge
			// ideally wall dodge completely in desired movement direction, but try increments closer to wall 
			FVector DesiredDir = (MovePos.Get() - UTC->GetActorLocation()).GetSafeNormal();
			if (UTC->Dodge(DesiredDir, (DesiredDir ^ FVector(0.0f, 0.0f, 1.0f)).GetSafeNormal()))
			{
				UTC->UTCharacterMovement->UpdateWallSlide(false);
				// end move after jump landing, success or failure, since it's unlikely we could reasonably try again
				AUTBot* B = Cast<AUTBot>(UTC->Controller);
				if (B != nullptr)
				{
					B->MoveTimer = -1.0f;
				}
				return false;
			}
			else
			{
				DesiredDir = (DesiredDir + WallNormal).GetSafeNormal();
				if (UTC->Dodge(DesiredDir, (DesiredDir ^ FVector(0.0f, 0.0f, 1.0f)).GetSafeNormal()) || UTC->Dodge(WallNormal, (WallNormal ^ FVector(0.0f, 0.0f, 1.0f)).GetSafeNormal()))
				{
					UTC->UTCharacterMovement->UpdateWallSlide(false);
					// end move after jump landing, success or failure, since it's unlikely we could reasonably try again
					AUTBot* B = Cast<AUTBot>(UTC->Controller);
					if (B != nullptr)
					{
						B->MoveTimer = -1.0f;
					}
					return false;
				}
				else
				{
					// air control into wall
					FVector AirControlDir = (WallPoint - UTC->GetActorLocation()).GetSafeNormal2D();
					if (UTC->UTCharacterMovement->bIsAgainstWall)
					{
						// already against wall so slide along it
						AirControlDir -= FMath::Max<float>(0.f, (AirControlDir | UTC->UTCharacterMovement->WallSlideNormal)) * UTC->UTCharacterMovement->WallSlideNormal;
					}
					UTC->UTCharacterMovement->AddInputVector(AirControlDir);
					UTC->UTCharacterMovement->UpdateWallSlide(true);
					return true;
				}
			}
		}
		else if (UTC->UTCharacterMovement->bIsDodging)
		{
			// adjust air control to counter lingering velocity from wallrun
			const FVector CurrentDir = UTC->UTCharacterMovement->Velocity.GetSafeNormal2D();
			const FVector DesiredDir = (MovePos.Get() - UTC->GetActorLocation()).GetSafeNormal2D();
			const FVector OrigJumpDir = (WallPoint - JumpStart).GetSafeNormal2D();
			if (((CurrentDir - DesiredDir).GetSafeNormal2D() | OrigJumpDir) > 0.0f)
			{
				UTC->UTCharacterMovement->AddInputVector((DesiredDir - OrigJumpDir).GetSafeNormal2D());
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			UTC->UTCharacterMovement->UpdateWallSlide(true);
			return false;
		}
	}
	else
	{
		UTC->UTCharacterMovement->UpdateWallSlide(true);
		return false;
	}
}

bool UUTReachSpec_WallDodge::GetMovePoints(const FUTPathLink& OwnerLink, const FVector& StartLoc, APawn* Asker, const FNavAgentProperties& AgentProps, const struct FRouteCacheItem& Target, const TArray<FRouteCacheItem>& FullRoute, const class AUTRecastNavMesh* NavMesh, TArray<FComponentBasedPosition>& MovePoints) const
{
	TArray<NavNodeRef> PolyRoute;
	if (NavMesh->FindPolyPath(StartLoc, AgentProps, FRouteCacheItem(NavMesh->GetPolySurfaceCenter(OwnerLink.StartEdgePoly), OwnerLink.StartEdgePoly), PolyRoute, false) && PolyRoute.Num() > 0 && NavMesh->DoStringPulling(StartLoc, PolyRoute, AgentProps, MovePoints))
	{
		MovePoints.Add(FComponentBasedPosition(JumpStart));
		AUTCharacter* UTC = Cast<AUTCharacter>(Asker);
		MovePoints.Add(FComponentBasedPosition(WallPoint + WallNormal * ((UTC != nullptr) ? (UTC->UTCharacterMovement->WallDodgeTraceDist * 0.5f) : (Asker->GetSimpleCollisionRadius() + 1.0f))));
		MovePoints.Add(FComponentBasedPosition(Target.GetLocation(Asker)));
		return true;
	}
	else
	{
		return false;
	}
}