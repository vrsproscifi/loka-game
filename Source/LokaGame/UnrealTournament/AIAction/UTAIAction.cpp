// a discrete action performed by AI, such as "hunt player" or "grab flag"
// replacement for what were AI states in UT3 AI code
// only one action may be in use at a time and an action is generally active until it returns true from Update() or is explicitly aborted/replaced by outside code
// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#include "LokaGame.h"
#include "UTBot.h"
#include "UTCharacter.h"
#include "UTAIAction.h"

UWorld* UUTAIAction::GetWorld() const
{
	return GetOuterAUTBot()->GetWorld();
}
	
APawn* UUTAIAction::GetPawn() const
{
	return GetOuterAUTBot()->GetPawn();
}

ACharacter* UUTAIAction::GetCharacter() const
{
	return GetOuterAUTBot()->GetCharacter();
}

AUTCharacter* UUTAIAction::GetUTChar() const
{
	return GetOuterAUTBot()->GetUTChar();
}

AUTSquadAI* UUTAIAction::GetSquad() const
{
	return GetOuterAUTBot()->GetSquad();
}

APawn* UUTAIAction::GetEnemy() const
{
	return GetOuterAUTBot()->GetEnemy();
}

AActor* UUTAIAction::GetTarget() const
{
	return GetOuterAUTBot()->GetTarget();
}

AShooterWeapon* UUTAIAction::GetWeapon() const
{
	return (GetUTChar() != nullptr) ? GetUTChar()->GetWeapon() : nullptr;
}

void UUTAIAction::Ended(bool bAborted)
{
	GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
}