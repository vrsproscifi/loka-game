// Copyright 1998 - 2015 Epic Games, Inc.All Rights Reserved.
#pragma once
#include "NavigationSystem.h"
#include "UTNavigationSystem.generated.h"

UCLASS()
class LOKAGAME_API UUTNavigationSystem : public UNavigationSystemV1
{
	GENERATED_BODY()
public:
	UUTNavigationSystem(const FObjectInitializer& OI)
		: Super(OI)
	{
		bAllowClientSideNavigation = true;
	}
};