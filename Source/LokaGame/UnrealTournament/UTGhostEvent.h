// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Object.h"
#include "UTAIAction_TacticalMove.h"
#include "UTCharacterStructs.h"
#include "UTGhostEvent.generated.h"

class AShooterWeapon;
class AUTCharacter;

/**
 * Baseclass for all ghost events
 */
UCLASS(Blueprintable)
class LOKAGAME_API UUTGhostEvent : public UObject
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Ghost)
	float Time;
	
	UFUNCTION(BlueprintNativeEvent)
	void ApplyEvent(class AUTCharacter* UTC);
};



UCLASS(Blueprintable)
class LOKAGAME_API UUTGhostEvent_Move : public UUTGhostEvent
{
	GENERATED_BODY()

public:
	UPROPERTY()
	FRepUTMovement RepMovement;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Ghost)
	uint8 CompressedFlags;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Ghost)
	uint32 bIsCrouched : 1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Ghost)
	uint32 bApplyWallSlide : 1;

	virtual void ApplyEvent_Implementation(class AUTCharacter* UTC) override;
};



UCLASS(Blueprintable)
class LOKAGAME_API UUTGhostEvent_MovementEvent : public UUTGhostEvent
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Ghost)
	FMovementEventInfo MovementEvent;

	virtual void ApplyEvent_Implementation(class AUTCharacter* UTC) override;
};



UCLASS(Blueprintable)
class LOKAGAME_API UUTGhostEvent_Input : public UUTGhostEvent
{
	GENERATED_BODY()

public:
	/** 1 bit for each firemode. 0 up 1 pressed*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Ghost)
	uint8 FireFlags;

	virtual void ApplyEvent_Implementation(class AUTCharacter* UTC) override;
};



UCLASS(Blueprintable)
class LOKAGAME_API UUTGhostEvent_Weapon : public UUTGhostEvent
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Ghost)
	TSubclassOf<AShooterWeapon> WeaponClass;

	virtual void ApplyEvent_Implementation(class AUTCharacter* UTC) override;
};


UCLASS(Blueprintable)
class LOKAGAME_API UUTGhostEvent_JumpBoots: public UUTGhostEvent
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Ghost)
	TSubclassOf<class AUTReplicatedEmitter> SuperJumpEffect;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Ghost)
	USoundBase* SuperJumpSound;

	virtual void ApplyEvent_Implementation(class AUTCharacter* UTC) override;
};