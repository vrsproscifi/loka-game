// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "LokaGame.h"
#include "UTGameInstance.h"
#include "UTDemoNetDriver.h"
#include "UTGameEngine.h"

#include "OnlineSubsystemUtils.h"
#include "UTLocalPlayer.h"
#include "UTGameUserSettings.h"

#if !UE_SERVER
#include "SUTStyle.h"
#include "SlateBasics.h"
#include "SlateExtras.h"
#endif

/* Delays for various timers during matchmaking */
#define DELETESESSION_DELAY 1.0f

UUTGameInstance::UUTGameInstance(const class FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	bDisablePerformanceCounters = false;
}

void UUTGameInstance::Init()
{
	Super::Init();
	InitPerfCounters();

	IPerfCounters* PerfCounters = IPerfCountersModule::Get().GetPerformanceCounters();
	if (PerfCounters)
	{
		// Attach a handler for exec commands passed in via the perf counter query port
		PerfCounters->OnPerfCounterExecCommand() = FPerfCounterExecCommandCallback::CreateUObject(this, &ThisClass::PerfExecCmd);
	}
}


bool UUTGameInstance::PerfExecCmd(const FString& ExecCmd, FOutputDevice& Ar)
{
	FWorldContext* CurrentWorldContext = GetWorldContext();
	if (CurrentWorldContext)
	{
		UWorld* World = CurrentWorldContext->World();
		if (World)
		{
			if (GEngine->Exec(World, *ExecCmd, Ar))
			{
				return true;
			}
			Ar.Log(FString::Printf(TEXT("ExecCmd %s not found"), *ExecCmd));
			return false;
		}
	}

	Ar.Log(FString::Printf(TEXT("WorldContext for ExecCmd %s not found"), *ExecCmd));
	return false;
}



void UUTGameInstance::HandleGameNetControlMessage(class UNetConnection* Connection, uint8 MessageByte, const FString& MessageStr)
{
	switch (MessageByte)
	{
		case UNMT_Redirect:
		{
			UE_LOG(UT, Verbose, TEXT("Received redirect request: %s"), *MessageStr);
			TArray<FString> Pieces;
			MessageStr.ParseIntoArray(Pieces, TEXT("\n"), false);
			if (Pieces.Num() == 3)
			{
				// 0: pak name, 1: URL, 2: checksum
				//RedirectDownload(Pieces[0], Pieces[1], Pieces[2]);
			}
			break;
		}
		default:
			UE_LOG(UT, Warning, TEXT("Unexpected net control message of type %i"), int32(MessageByte));
			break;
	}
}


void UUTGameInstance::StartRecordingReplay(const FString& Name, const FString& FriendlyName, const TArray<FString>& AdditionalOptions)
{
	if (FParse::Param(FCommandLine::Get(), TEXT("NOREPLAYS")))
	{
		UE_LOG(UT, Warning, TEXT("UGameInstance::StartRecordingReplay: Rejected due to -noreplays option"));
		return;
	}

	UWorld* CurrentWorld = GetWorld();

	if (CurrentWorld == nullptr)
	{
		UE_LOG(UT, Warning, TEXT("UGameInstance::StartRecordingReplay: GetWorld() is null"));
		return;
	}

	if (CurrentWorld->WorldType == EWorldType::PIE)
	{
		UE_LOG(UT, Warning, TEXT("UGameInstance::StartRecordingReplay: Function called while running a PIE instance, this is disabled."));
		return;
	}

	if (CurrentWorld->DemoNetDriver && CurrentWorld->DemoNetDriver->IsPlaying())
	{
		UE_LOG(UT, Warning, TEXT("UGameInstance::StartRecordingReplay: A replay is already playing, cannot begin recording another one."));
		return;
	}

	FURL DemoURL;
	FString DemoName = Name;

	DemoName.ReplaceInline(TEXT("%m"), *CurrentWorld->GetMapName());

	// replace the current URL's map with a demo extension
	DemoURL.Map = DemoName;
	DemoURL.AddOption(*FString::Printf(TEXT("DemoFriendlyName=%s"), *FriendlyName));
	DemoURL.AddOption(*FString::Printf(TEXT("Remote")));

	for (const FString& Option : AdditionalOptions)
	{
		DemoURL.AddOption(*Option);
	}

	CurrentWorld->DestroyDemoNetDriver();

	const FName NAME_DemoNetDriver(TEXT("DemoNetDriver"));

	if (!GEngine->CreateNamedNetDriver(CurrentWorld, NAME_DemoNetDriver, NAME_DemoNetDriver))
	{
		UE_LOG(UT, Warning, TEXT("RecordReplay: failed to create demo net driver!"));
		return;
	}

	CurrentWorld->DemoNetDriver = Cast< UDemoNetDriver >(GEngine->FindNamedNetDriver(CurrentWorld, NAME_DemoNetDriver));

	check(CurrentWorld->DemoNetDriver != nullptr);

	CurrentWorld->DemoNetDriver->SetWorld(CurrentWorld);
	/*
	if (DemoURL.Map == TEXT("_DeathCam"))
	{
		UUTDemoNetDriver* UTDemoNetDriver = Cast<UUTDemoNetDriver>(CurrentWorld->DemoNetDriver);
		if (UTDemoNetDriver)
		{
			UTDemoNetDriver->bIsLocalReplay = true;
		}
	}
	*/
	FString Error;

	if (!CurrentWorld->DemoNetDriver->InitListen(CurrentWorld, DemoURL, false, Error))
	{
		UE_LOG(UT, Warning, TEXT("Demo recording failed: %s"), *Error);
		CurrentWorld->DemoNetDriver = NULL;
	}
	else
	{
		//UE_LOG(UT, VeryVerbose, TEXT("Num Network Actors: %i"), CurrentWorld->DemoNetDriver->GetNetworkObjectList().GetObjects().Num());
	}
}
bool UUTGameInstance::PlayReplay(const FString& Name, UWorld* WorldOverride, const TArray<FString>& AdditionalOptions)
{
	UWorld* CurrentWorld = WorldOverride != nullptr ? WorldOverride : GetWorld();

	if (CurrentWorld == nullptr)
	{
		UE_LOG(UT, Warning, TEXT("UGameInstance::PlayReplay: GetWorld() is null"));
		return false;
	}

	if (CurrentWorld->WorldType == EWorldType::PIE)
	{
		UE_LOG(UT, Warning, TEXT("UUTGameInstance::PlayReplay: Function called while running a PIE instance, this is disabled."));
		return false;
	}

	CurrentWorld->DestroyDemoNetDriver();

	FURL DemoURL;
	UE_LOG(UT, Log, TEXT("PlayReplay: Attempting to play demo %s"), *Name);

	DemoURL.Map = Name;
	DemoURL.AddOption(*FString::Printf(TEXT("Remote")));

	for (const FString& Option : AdditionalOptions)
	{
		DemoURL.AddOption(*Option);
	}

	const FName NAME_DemoNetDriver(TEXT("DemoNetDriver"));

	if (!GEngine->CreateNamedNetDriver(CurrentWorld, NAME_DemoNetDriver, NAME_DemoNetDriver))
	{
		UE_LOG(UT, Warning, TEXT("PlayReplay: failed to create demo net driver!"));
		return false;
	}

	CurrentWorld->DemoNetDriver = Cast< UDemoNetDriver >(GEngine->FindNamedNetDriver(CurrentWorld, NAME_DemoNetDriver));

	check(CurrentWorld->DemoNetDriver != nullptr);

	CurrentWorld->DemoNetDriver->SetWorld(CurrentWorld);

	if (DemoURL.Map == TEXT("_DeathCam"))
	{
		UUTDemoNetDriver* UTDemoNetDriver = Cast<UUTDemoNetDriver>(CurrentWorld->DemoNetDriver);
		if (UTDemoNetDriver)
		{
			UTDemoNetDriver->bIsLocalReplay = true;
		}
	}
	FString Error;

	if (!CurrentWorld->DemoNetDriver->InitConnect(CurrentWorld, DemoURL, Error))
	{
		UE_LOG(UT, Warning, TEXT("Demo playback failed: %s"), *Error);
		CurrentWorld->DestroyDemoNetDriver();
		return false;
	}
	else
	{
		FCoreUObjectDelegates::PostDemoPlay.Broadcast();
		return true;
	}
}


void UUTGameInstance::Shutdown()
{

	Super::Shutdown();
}

/*static*/ UUTGameInstance* UUTGameInstance::Get(UObject* ContextObject)
{
	UUTGameInstance* GameInstance = NULL;
	UWorld* OwningWorld = GEngine->GetWorldFromContextObject(ContextObject, EGetWorldErrorMode::ReturnNull);

	if (OwningWorld)
	{
		GameInstance = Cast<UUTGameInstance>(OwningWorld->GetGameInstance());
	}
	return GameInstance;
}


bool UUTGameInstance::IsInSession(const FUniqueNetId& SessionId) const
{
	IOnlineSessionPtr SessionInt = Online::GetSessionInterface(/*World*/);
	if (SessionInt.IsValid())
	{
		FNamedOnlineSession* Session = SessionInt->GetNamedSession(GameSessionName);
		if (Session && Session->SessionInfo.IsValid() && Session->SessionInfo->GetSessionId() == SessionId)
		{
			return true;
		}
	}
	return false;
}

void UUTGameInstance::SafeSessionDelete(FName SessionName, FOnDestroySessionCompleteDelegate DestroySessionComplete)
{
	UWorld* World = GetWorld();
	check(World);

	IOnlineSessionPtr SessionInt = Online::GetSessionInterface(/*World*/);
	if (SessionInt.IsValid())
	{
		EOnlineSessionState::Type SessionState = SessionInt->GetSessionState(SessionName);
		if (SessionState != EOnlineSessionState::NoSession)
		{
			if (SessionState != EOnlineSessionState::Destroying)
			{
				if (SessionState != EOnlineSessionState::Creating &&
					SessionState != EOnlineSessionState::Ending)
				{
					SafeSessionDeleteTimerHandle.Invalidate();

					FOnDestroySessionCompleteDelegate CompletionDelegate;
					CompletionDelegate = FOnDestroySessionCompleteDelegate::CreateUObject(this, &UUTGameInstance::OnDeleteSessionComplete, DestroySessionComplete);

					DeleteSessionDelegateHandle = SessionInt->AddOnDestroySessionCompleteDelegate_Handle(CompletionDelegate);
					SessionInt->DestroySession(SessionName);
				}
				else
				{
					if (!SafeSessionDeleteTimerHandle.IsValid())
					{
						// Retry shortly
						FTimerDelegate RetryDelegate;
						RetryDelegate.BindUObject(this, &UUTGameInstance::SafeSessionDelete, SessionName, DestroySessionComplete);
						World->GetTimerManager().SetTimer(SafeSessionDeleteTimerHandle, RetryDelegate, DELETESESSION_DELAY, false);
					}
					else
					{
						// Timer already in flight
						TArray<FOnDestroySessionCompleteDelegate>& DestroyDelegates = PendingDeletionDelegates.FindOrAdd(SessionName);
						DestroyDelegates.Add(DestroySessionComplete);
					}
				}
			}
			else
			{
				// Destroy already in flight
				TArray<FOnDestroySessionCompleteDelegate>& DestroyDelegates = PendingDeletionDelegates.FindOrAdd(SessionName);
				DestroyDelegates.Add(DestroySessionComplete);
			}
		}
		else
		{
			////UE_LOG(LogOnlineGame, Verbose, TEXT("SafeSessionDelete called on session %s in state %s, skipping."), *SessionName.ToString(), EOnlineSessionState::ToString(SessionState));
			DestroySessionComplete.ExecuteIfBound(SessionName, true);
		}

		return;
	}

	// Non shipping builds can return success, otherwise fail
	bool bWasSuccessful = !UE_BUILD_SHIPPING;
	DestroySessionComplete.ExecuteIfBound(SessionName, bWasSuccessful);
}

void UUTGameInstance::OnDeleteSessionComplete(FName SessionName, bool bWasSuccessful, FOnDestroySessionCompleteDelegate DestroySessionComplete)
{
	////UE_LOG(LogOnlineGame, Verbose, TEXT("OnDeleteSessionComplete %s bSuccess: %d"), *SessionName.ToString(), bWasSuccessful);
	SafeSessionDeleteTimerHandle.Invalidate();

	IOnlineSessionPtr SessionInt = Online::GetSessionInterface(GetWorld());
	if (SessionInt.IsValid())
	{
		SessionInt->ClearOnDestroySessionCompleteDelegate_Handle(DeleteSessionDelegateHandle);
	}

	DestroySessionComplete.ExecuteIfBound(SessionName, bWasSuccessful);

	TArray<FOnDestroySessionCompleteDelegate> DelegatesCopy;
	if (PendingDeletionDelegates.RemoveAndCopyValue(SessionName, DelegatesCopy))
	{
		// All other delegates captured while destroy was in flight
		for (const FOnDestroySessionCompleteDelegate& ExtraDelegate : DelegatesCopy)
		{
			ExtraDelegate.ExecuteIfBound(SessionName, bWasSuccessful);
		}
	}
}

int32 UUTGameInstance::GetBotSkillForTeamElo(int32 TeamElo)
{
	if (TeamElo > 1500)
	{
		return 3 + FMath::Clamp((float(TeamElo) - 1500.f)/60.f , 0.f, 4.f);
	}
	else if (TeamElo > 1400)
	{
		return 2;
	}

	return 1;
}


