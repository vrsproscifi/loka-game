#include "LokaGame.h"
#include "MatchStateNames.h"


namespace MatchState
{
	const FName PlayerIntro = FName(TEXT("PlayerIntro"));
	const FName CountdownToBegin = FName(TEXT("CountdownToBegin"));
	const FName MatchEnteringOvertime = FName(TEXT("MatchEnteringOvertime"));
	const FName MatchIsInOvertime = FName(TEXT("MatchIsInOvertime"));
	const FName MapVoteHappening = FName(TEXT("MapVoteHappening"));
	const FName MatchIntermission = FName(TEXT("MatchIntermission"));
	const FName MatchExitingIntermission = FName(TEXT("MatchExitingIntermission"));
	const FName MatchRankedAbandon = FName(TEXT("MatchRankedAbandon"));
}