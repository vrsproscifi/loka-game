// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "LokaGame.h"
#include "UTProj_Rocket.h"
#include "UnrealNetwork.h"
#include "UTRewardMessage.h"
#include "StatNames.h"
#include "UTCharacter.h"
#include "UTPlayerController.h"


AUTProj_Rocket::AUTProj_Rocket(const class FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{
	DamageParams.BaseDamage = 100;
	DamageParams.OuterRadius = 360.f;  
	DamageParams.InnerRadius = 15.f; 
	DamageParams.MinimumDamage = 20.f;
	Momentum = 140000.0f;
	InitialLifeSpan = 10.f;
	ProjectileMovement->InitialSpeed = 2700.f;
	ProjectileMovement->MaxSpeed = 2700.f;
	ProjectileMovement->ProjectileGravityScale = 0.f;

	PrimaryActorTick.bCanEverTick = true;
	AdjustmentSpeed = 5000.0f;
	bLeadTarget = true;
}

void AUTProj_Rocket::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (TargetActor != nullptr)
	{
		FVector WantedDir = (TargetActor->GetActorLocation() - GetActorLocation()).GetSafeNormal();
		if (bLeadTarget)
		{
			WantedDir += TargetActor->GetVelocity() * WantedDir.Size() / ProjectileMovement->MaxSpeed;
		}

		ProjectileMovement->Velocity += WantedDir * AdjustmentSpeed * DeltaTime;
		ProjectileMovement->Velocity = ProjectileMovement->Velocity.GetSafeNormal() * ProjectileMovement->MaxSpeed;

		//If the rocket has passed the target stop following
		if (FVector::DotProduct(WantedDir, ProjectileMovement->Velocity) < 0.0f)
		{
			TargetActor = NULL;
		}
	}
}

void AUTProj_Rocket::OnRep_Instigator()
{
	Super::OnRep_Instigator();
	//AUTCharacter* UTChar = Cast<AUTCharacter>(Instigator);
	//if (UTChar && (UTChar->GetTeamColor() != FLinearColor::White))
	//{
	//	TArray<UStaticMeshComponent*> MeshComponents;
	//	GetComponents<UStaticMeshComponent>(MeshComponents);

	//	if (MeshComponents.Num())
	//	{
	//		static FName NAME_GunGlowsColor(TEXT("Gun_Glows_Color"));
	//		UMaterialInstanceDynamic* MeshMI = MeshComponents[0] ? MeshComponents[0]->CreateAndSetMaterialInstanceDynamic(1) : nullptr;
	//		if (MeshMI != nullptr)
	//		{
	//			MeshMI->SetVectorParameterValue(NAME_GunGlowsColor, UTChar->GetTeamColor());
	//		}
	//	}
	//}
}

void AUTProj_Rocket::GetLifetimeReplicatedProps(TArray<class FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AUTProj_Rocket, TargetActor);
}

void AUTProj_Rocket::DamageImpactedActor_Implementation(AActor* OtherActor, UPrimitiveComponent* OtherComp, const FVector& HitLocation, const FVector& HitNormal)
{
	AUTCharacter* HitCharacter = Cast<AUTCharacter>(OtherActor);

	bPendingSpecialReward = (HitCharacter && AirRocketRewardClass && HitCharacter->IsAlive() && HitCharacter->GetCharacterMovement() != nullptr && (HitCharacter->GetCharacterMovement()->MovementMode == MOVE_Falling) && (GetWorld()->GetTimeSeconds() - HitCharacter->FallingStartTime > 0.3f));

	Super::DamageImpactedActor_Implementation(OtherActor, OtherComp, HitLocation, HitNormal);
	if (bPendingSpecialReward && HitCharacter && HitCharacter->IsDead())
	{
		// Air Rocket reward
		AUTPlayerController* PC = Cast<AUTPlayerController>(InstigatorController);
		if (PC && PC->GetPlayerState<AUTPlayerState>())
		{
			int32 AirRoxCount = 0;
			PC->GetPlayerState<AUTPlayerState>()->ModifyStatsValue(NAME_AirRox, 1);
			PC->GetPlayerState<AUTPlayerState>()->AddCoolFactorMinorEvent();
			AirRoxCount = PC->GetPlayerState<AUTPlayerState>()->GetStatsValue(NAME_AirRox);
			PC->SendPersonalMessage(AirRocketRewardClass, AirRoxCount, PC->GetPlayerState<AUTPlayerState>(), HitCharacter->GetPlayerState());
		}
	}
	bPendingSpecialReward = false;
}

void AUTProj_Rocket::Explode_Implementation(const FVector& HitLocation, const FVector& HitNormal, UPrimitiveComponent* HitComp)
{
	AUTCharacter* HitCharacter = Cast<AUTCharacter>(ImpactedActor);
	bool bFollowersTrack = (!bExploded && (Role == ROLE_Authority) && (FollowerRockets.Num() > 0) && HitCharacter);

	Super::Explode_Implementation(HitLocation, HitNormal, HitComp);
	if (bFollowersTrack && HitCharacter && HitCharacter->IsAlive())
	{
		for (int32 i = 0; i < FollowerRockets.Num(); i++)
		{
			if (FollowerRockets[i] && !FollowerRockets[i]->IsPendingKillPending())
			{
				FollowerRockets[i]->TargetActor = HitCharacter;
				AdjustmentSpeed = 24000.f;
				bLeadTarget = true;
			}
		}
	}
}
