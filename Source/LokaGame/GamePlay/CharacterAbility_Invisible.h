// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GamePlay/CharacterAbility.h"
#include "CharacterAbility_Invisible.generated.h"

/**
 * 
 */
UCLASS(abstract)
class LOKAGAME_API ACharacterAbility_Invisible : public ACharacterAbility
{
	GENERATED_BODY()
	
public:

	ACharacterAbility_Invisible();
	
	virtual bool RequestStartActivate() override;
	virtual bool RequestEndActivate() override;

	virtual void RequestDeactivate(const bool IsDestroy = false) override;
	virtual void PostInitializeComponents() override;

protected:

	virtual void OnRep_IsActivated() override;

	UPROPERTY(EditDefaultsOnly, Category = Ability)
	UMaterialInterface* InvisibleMaterial;	
};
