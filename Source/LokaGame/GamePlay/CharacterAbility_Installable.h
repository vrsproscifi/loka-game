// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GamePlay/CharacterAbility.h"
#include "Interfaces/DestroyableActorInterface.h"
#include "CharacterAbility_Installable.generated.h"

class AUTPlayerState;
/**
 * 
 */
UCLASS()
class LOKAGAME_API ACharacterAbility_Installable 
	: public ACharacterAbility
	, public IDestroyableActorInterface
{
	GENERATED_BODY()

public:

	ACharacterAbility_Installable();

	virtual void PostInitializeComponents() override;

	virtual bool RequestStartActivate() override;
	virtual bool RequestEndActivate() override;
	virtual void RequestDeactivate(const bool IsDestroy = false) override;
	
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

	// IDestroyableActorInterface Begin
	virtual float GetCurrentHealth_Implementation() const override { return Health.X; }
	virtual FVector2D GetHealth_Implementation() const override { return Health; }
	virtual float GetHealthPercent_Implementation() const override { return Health.X / Health.Y; }
	virtual bool IsDying_Implementation() const override { return bIsDie; }
	virtual FText GetLocalizedName_Implementation() const override { return Name; }
	virtual bool GetIsVisiblyUserInterface_Implementation() const override;
	// IDestroyableActorInterface End

	UFUNCTION(BlueprintCallable, Category = Component)
	void SetIsUsedAsNonAbility(const bool IsAsNonAbility = false);

	UFUNCTION(BlueprintPure, BlueprintCallable, Category = Component)
	FORCEINLINE bool IsUsedAsNonAbility() const { return bIsUsedAsNonAbility; }

protected:

	UPROPERTY(EditDefaultsOnly, Category = Runtime)
	bool bIsUsedAsNonAbility;

	UPROPERTY()
	UMeshComponent* OuterMeshComponent;

	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	UBoxComponent* CollisionBoxComponent;

	UFUNCTION()
	virtual void OnRep_AbilityTransform();

	UPROPERTY(ReplicatedUsing = OnRep_AbilityTransform)
	FTransform AbilityTransform;

	UFUNCTION()
	bool TryPlacement();

	virtual void Tick(float DeltaSeconds) override;
	virtual void OnRep_IsActivated() override;

	/** X - Current, Y - Maximum */
	UPROPERTY(Replicated, EditDefaultsOnly, Category = Gameplay)
	FVector2D Health;

	/** Total score points to give all killers */
	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	float TotalScoreReward;

	UPROPERTY(Replicated, BlueprintReadWrite, Category = Gameplay)
	bool bIsDie;

	UPROPERTY()
	TMap<AUTPlayerState*, float> DamageByPlayers;

	UPROPERTY()
	TArray<UMaterialInstanceDynamic*> MIDs;

	UPROPERTY(EditDefaultsOnly, Category = Mesh)
	TArray<UMaterialInterface*> HoloMaterials;

	UFUNCTION()
	void OnRep_IsBeginPlace();

	UPROPERTY(ReplicatedUsing = OnRep_IsBeginPlace)
	bool IsBeginPlace;
};