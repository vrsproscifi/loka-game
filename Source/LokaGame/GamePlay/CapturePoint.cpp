// VRSPRO

#include "LokaGame.h"
#include "CapturePoint.h"

#include "UTCharacter.h"
#include "UTGameState.h"
#include "UTPlayerState.h"

#include "UTHUD.h"

ACapturePoint::ACapturePoint()
{
	PrimaryActorTick.bCanEverTick = true;
	
	Sphere = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
	Sphere->InitSphereRadius(500.0f);
	Sphere->SetGenerateOverlapEvents(true);
	Sphere->bAutoActivate = true;
	Sphere->bHiddenInGame = true;

	IconPoint = CreateDefaultSubobject<UVectorFieldComponent>(TEXT("IconPoint"));
	IconPoint->SetupAttachment(Sphere);
	IconPoint->bHiddenInGame = true;

	AroundSphere = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("AroundSphere"));
	AroundSphere->SetupAttachment(Sphere);
	AroundSphere->SetCollisionResponseToAllChannels(ECR_Ignore);
	AroundSphere->bAffectDistanceFieldLighting = false;
	AroundSphere->bAffectDynamicIndirectLighting = false;
	AroundSphere->bCastDynamicShadow = false;
	AroundSphere->bCastStaticShadow = false;

	//if (!HasAnyFlags(RF_ClassDefaultObject | RF_ArchetypeObject))
	{
		Sphere->OnComponentBeginOverlap.AddDynamic(this, &ACapturePoint::OnBeginOverlap);
		Sphere->OnComponentEndOverlap.AddDynamic(this, &ACapturePoint::OnEndOverlap);
#if !UE_SERVER
		static ConstructorHelpers::FObjectFinder<UMaterialInterface> MarkOb(TEXT("/Game/1LOKAgame/UserInterface/Materials/CapturePoint/M_PointMark_Inst"));
		MarkInst = MarkOb.Object;	

		static ConstructorHelpers::FObjectFinder<UMaterialInterface> MarkBGOb(TEXT("/Game/1LOKAgame/UserInterface/Materials/CapturePoint/M_PointMarkBackground_Inst"));
		MarkInstBG = MarkBGOb.Object;		

		static ConstructorHelpers::FObjectFinder<UMaterialInterface> AroundOb(TEXT("/Game/1LOKAgame/Materials/Gameplay/M_AroundCapturePoint_Inst"));
		AroundMaterialInstance = AroundOb.Object;

		static ConstructorHelpers::FObjectFinder<UStaticMesh> SphereOb(TEXT("/Engine/BasicShapes/Cylinder"));
		AroundSphere->SetStaticMesh(SphereOb.Object);

		static ConstructorHelpers::FObjectFinder<UFont> FontOb(TEXT("/Game/1LOKAgame/UserInterface/Fonts/Days"));
		IconFont = FSlateFontInfo(FontOb.Object, 10);
#endif
	}
	Sphere->SetCollisionResponseToAllChannels(ECR_Overlap);

	RootComponent = Sphere;

	CurrentPoints = .0f;
	TeamNum = NONE_TEAM_ID;
	ControllTeam = NONE_TEAM_ID;

	// Configure
	TeamPointsSpeedPerSecond = 8.0f;
	DownPointsSpeedPerSecond = 5.0f;
	PointName = "ACapturePoint";
	IsAllowCapturingUnder = false;

	CountCaptures.Empty();

	bReplicates = true;
}

void ACapturePoint::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ACapturePoint, CurrentPoints);
	DOREPLIFETIME(ACapturePoint, ControllTeam);	
}

void ACapturePoint::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	Mark = UMaterialInstanceDynamic::Create(MarkInst, this);
	MarkBG = UMaterialInstanceDynamic::Create(MarkInstBG, this);

	AroundMaterial = UMaterialInstanceDynamic::Create(AroundMaterialInstance, this);
	if (AroundMaterial)
	{
		AroundMaterial->SetVectorParameterValue(TEXT("Color"), FLinearColor::Gray);
	}

	const float Radius = Sphere->GetUnscaledSphereRadius() / 50.0f;
	AroundSphere->SetWorldScale3D(FVector(Radius, Radius, AroundSphere->GetComponentTransform().GetScale3D().Z));
	AroundSphere->SetMaterial(0, AroundMaterial);
}

void ACapturePoint::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (HasAuthority())
	{
		if (auto MyGameState = GetWorld()->GetGameState<AUTGameState>())
		{
			if (CountCaptures.Num() != MyGameState->GetNumTeams())
			{
				CountCaptures.Empty();
				CountCaptures.AddZeroed(MyGameState->GetNumTeams());
			}
		}

		if (CountCaptures.IsValidIndex(TeamNum))
		{
			const auto SmoothTeamPoints = TeamPointsSpeedPerSecond * DeltaTime;

			if (ControllTeam == TeamNum && TeamNum != NONE_TEAM_ID && CurrentPoints < 100.0f)
			{
				CurrentPoints += SmoothTeamPoints * CountCaptures[TeamNum].Num();

				for (auto c : CountCaptures[TeamNum])
				{
					if (auto PlayerState = Cast<AUTPlayerState>(c->GetPlayerState()))
					{
						float& Value = HoldPlayers.FindOrAdd(PlayerState);
						Value += SmoothTeamPoints;
					}
				}
			}
			else if (TeamNum == NONE_TEAM_ID && CurrentPoints < 100.0f)
			{
				CurrentPoints -= DownPointsSpeedPerSecond * DeltaTime;
			}
			else if (TeamNum != NONE_TEAM_ID && ControllTeam != TeamNum)
			{
				CurrentPoints -= SmoothTeamPoints * CountCaptures[TeamNum].Num();

				for (auto c : CountCaptures[TeamNum])
				{
					if (auto PlayerState = Cast<AUTPlayerState>(c->GetPlayerState()))
					{
						float& Value = HoldPlayers.FindOrAdd(PlayerState);
						Value += SmoothTeamPoints;
					}
				}
			}

			CurrentPoints = FMath::Clamp(CurrentPoints, .0f, 100.0f);

			if (CurrentPoints <= .0f)
			{
				ControllTeam = TeamNum;

				if (HoldPlayers.Num())
				{
					OnCapturePointHold.ExecuteIfBound(this, HoldPlayers);
					HoldPlayers.Empty();
				}
			}
			else if (IsHold() && HoldPlayers.Num())
			{
				OnCapturePointHold.ExecuteIfBound(this, HoldPlayers);
				HoldPlayers.Empty();
			}
		}
	}
	
	if (GetNetMode() != NM_DedicatedServer)
	{
		if (auto MyPlayerState = GetWorld()->GetFirstPlayerController()->GetPlayerState<AUTPlayerState>())
		{
			if (AroundMaterial)
			{
				AroundMaterial->SetVectorParameterValue(TEXT("Color"), (ControllTeam == NONE_TEAM_ID) ? FLinearColor::Gray : (ControllTeam == MyPlayerState->GetTeamNum() ? FLinearColor::Green : FLinearColor::Red));
			}

			GetMarkMaterial()->SetVectorParameterValue(TEXT("c"), (MyPlayerState->GetTeamNum() == GetHoldTeam()) ? FColor::Green.ReinterpretAsLinear() : FColor::Red.ReinterpretAsLinear());
			GetMarkMaterial()->SetScalarParameterValue(TEXT("Percent"), GetCapturingPercent());
		}
	}
}

bool ACapturePoint::IsInAnyTeamWithout(const int32& Team) const
{
	SIZE_T _count = 0;
	for (auto i : CountCaptures)
	{
		if (_count != Team && i.Num() > 0)
		{
			return true;
		}
		++_count;
	}

	return false;
}

bool ACapturePoint::IsHold() const
{
	return (CurrentPoints >= 100.0f);
}

bool ACapturePoint::IsCapturing() const
{
	return (TeamNum != NONE_TEAM_ID && !IsHold());
}

int32 ACapturePoint::GetCapturingTeam() const
{
	return TeamNum;
}

int32 ACapturePoint::GetHoldTeam() const
{
	return ControllTeam;
}

float ACapturePoint::GetCapturingPercent() const
{
	return CurrentPoints / 100.0f;
}

UMaterialInstanceDynamic* ACapturePoint::GetMarkMaterial() const
{
	return Mark;
}

UMaterialInstanceDynamic* ACapturePoint::GetMarkMaterialBG() const
{
	return MarkBG;
}

FString ACapturePoint::GetPointName() const
{
	return PointName;
}

FVector ACapturePoint::GetPointDrawPosition() const
{
	return IconPoint->GetComponentLocation();
}

float ACapturePoint::GetSize() const
{
	return Sphere->GetUnscaledSphereRadius();
}

void ACapturePoint::RefreshInSphereCharacters()
{
	TArray<AActor*> Actors;
	Sphere->GetOverlappingActors(Actors, AUTCharacter::StaticClass());

	for (auto &i : CountCaptures)
	{
		for (AUTCharacter* a : i)
		{
			a->CapturePoint = nullptr;
		}

		i.Empty();
	}

	for (auto a : Actors)
	{
		auto MyPawn = Cast<AUTCharacter>(a);
		auto MyPlayerState = Cast<AUTPlayerState>(MyPawn->GetPlayerState());

		if (MyPlayerState && MyPawn->IsAlive() && CountCaptures.Num())
		{
			if (IsAllowCapturingUnder || (IsAllowCapturingUnder == false && GetActorLocation().Z < MyPawn->GetActorLocation().Z))
			{
				MyPawn->CapturePoint = this;
				CountCaptures[MyPlayerState->GetTeamNum()].Add(MyPawn);
			}
		}
	}
}

void ACapturePoint::OnLeavePoint(class AUTCharacter* Character)
{
	if (auto MyPlayerState = Cast<AUTPlayerState>(Character->GetPlayerState()))
	{
		RefreshInSphereCharacters();

		SIZE_T _count = 0, _teams = 0, _lasfound = 0;
		for (auto i : CountCaptures)
		{
			if (i.Num())
			{
				_lasfound = _count;
				++_teams;
			}
			++_count;
		}

		if (_teams == 1)
		{
			TeamNum = _lasfound;
		}

		if (!IsInAnyTeamWithout(NONE_TEAM_ID))
		{
			TeamNum = NONE_TEAM_ID;
		}
	}
}

void ACapturePoint::OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult)
{
	if (GetNetMode() != NM_Client)
	{
		if (auto MyPawn = Cast<AUTCharacter>(OtherActor))
		{
			if (auto MyPlayerState = Cast<AUTPlayerState>(MyPawn->GetPlayerState()))
			{
				const int32 Team = MyPlayerState->GetTeamNum();			

				RefreshInSphereCharacters();

				if (IsInAnyTeamWithout(Team))
				{
					TeamNum = NONE_TEAM_ID;
				}
				else
				{
					TeamNum = Team;
				}
			}
		}
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, TEXT("Start"));
	}
}

void ACapturePoint::OnEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (GetNetMode() != NM_Client)
	{
		if (auto MyPawn = Cast<AUTCharacter>(OtherActor))
		{
			OnLeavePoint(MyPawn);
		}
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Stop"));
	}
}

void ACapturePoint::DrawRadarIcon_Implementation(AHUD* InOwnerHud, UCanvas* InCanvas, FVector2D InPosition, const float InScale)
{
	if (InOwnerHud && InOwnerHud->PlayerOwner && InCanvas)
	{
		const float SizeSafeZone = 20.0f * InScale; // Temp
		const auto MarkSize = SizeSafeZone * 1.5f;
		const auto MarPosition = FVector2D(InPosition.Y, InPosition.X) - FVector2D(MarkSize / 2, MarkSize / 2);

		FCanvasTileItem MarkBGItem(MarPosition, GetMarkMaterialBG()->GetRenderProxy(false), FVector2D(MarkSize, MarkSize));
		MarkBGItem.PivotPoint = FVector2D(.5f, .5f);
		MarkBGItem.Rotation = FRotator(.0f, 90.f, .0f);
		MarkBGItem.Z = 1;

		InCanvas->DrawItem(MarkBGItem);

		FCanvasTileItem MarkItem(MarPosition, GetMarkMaterial()->GetRenderProxy(false), FVector2D(MarkSize, MarkSize));
		MarkItem.PivotPoint = FVector2D(.5f, .5f);
		MarkItem.Rotation = FRotator(.0f, 90.f, .0f);
		MarkItem.Z = 2;

		InCanvas->DrawItem(MarkItem);

		FCanvasTextItem TextItem(FVector2D(InPosition.Y, InPosition.X), FText::FromString(GetPointName()), IconFont, FColor::White);
		TextItem.bCentreX = true;
		TextItem.bCentreY = true;
		TextItem.bOutlined = true;
		TextItem.OutlineColor = FColor::Black;
		TextItem.Scale = FVector2D(InScale, InScale);
		TextItem.Depth = 3.0f;

		InCanvas->DrawItem(TextItem);
	}
}

void ACapturePoint::DrawMinimapIcon_Implementation(AHUD* InOwnerHud, UCanvas* InCanvas, FVector2D InPosition, const float InScale)
{
	if (InOwnerHud && InCanvas && InOwnerHud->GetOwner())
	{
		FVector2D TargetSize(32.0f * InScale, 32.0f * InScale);
		FVector2D TargetPos(InPosition.X - TargetSize.X / 2, InPosition.Y - TargetSize.Y / 2);

		FCanvasTileItem MarkBGItem(TargetPos, GetMarkMaterialBG()->GetRenderProxy(false), TargetSize);
		MarkBGItem.PivotPoint = FVector2D(.5f, .5f);
		MarkBGItem.Rotation = FRotator(.0f, 90.f, .0f);

		InCanvas->DrawItem(MarkBGItem);

		FCanvasTileItem MarkItem(TargetPos, GetMarkMaterial()->GetRenderProxy(false), TargetSize);
		MarkItem.PivotPoint = FVector2D(.5f, .5f);
		MarkItem.Rotation = FRotator(.0f, 90.f, .0f);

		InCanvas->DrawItem(MarkItem);

		FCanvasTextItem TextItem(InPosition, FText::FromString(GetPointName()), IconFont, FColor::White);
		TextItem.bCentreX = true;
		TextItem.bCentreY = true;
		TextItem.bOutlined = true;
		TextItem.OutlineColor = FColor::Black;
		TextItem.Scale = FVector2D(InScale, InScale);

		InCanvas->DrawItem(TextItem);
	}
}

void ACapturePoint::DrawWorldIcon_Implementation(AHUD* InOwnerHud, UCanvas* InCanvas, FVector2D InPosition, const float InScale)
{
	if (InOwnerHud && InOwnerHud->PlayerOwner && InCanvas)
	{
		APawn* MyPawn = InOwnerHud->PlayerOwner->GetPawnOrSpectator();

		if (MyPawn)
		{
			const auto PawnLocation = MyPawn->GetActorLocation();
			const auto Distance = FVector::Dist(GetActorLocation(), PawnLocation);

			const float ForLerp = FMath::Clamp<float>((Distance - GetSize()) / GetSize(), .0f, 1.f);
			InPosition = FMath::Lerp(FVector2D(InCanvas->SizeX / 2, InCanvas->SizeY / 3), InPosition, ForLerp);

			const auto Size = FMath::Lerp(16.0f, 64.0f, FMath::Clamp<float>(1400.0f / FVector::Dist(PawnLocation, GetPointDrawPosition()), .0f, 1.0f));
			const auto MarkPosition = InPosition - FVector2D(Size / 2, Size / 2);

			FCanvasTileItem MarkBGItem(MarkPosition, GetMarkMaterialBG()->GetRenderProxy(false), FVector2D(Size, Size));
			MarkBGItem.PivotPoint = FVector2D(.5f, .5f);
			MarkBGItem.Rotation = FRotator(.0f, 90.f, .0f);
			MarkBGItem.Z = 1;

			InCanvas->DrawItem(MarkBGItem);

			FCanvasTileItem MarkItem(MarkPosition, GetMarkMaterial()->GetRenderProxy(false), FVector2D(Size, Size));
			MarkItem.PivotPoint = FVector2D(.5f, .5f);
			MarkItem.Rotation = FRotator(.0f, 90.f, .0f);
			MarkItem.Z = 2;

			InCanvas->DrawItem(MarkItem);

			FCanvasTextItem TextItem(InPosition, FText::FromString(GetPointName()), IconFont, FColor::White);
			TextItem.bCentreX = true;
			TextItem.bCentreY = true;
			TextItem.bOutlined = true;
			TextItem.OutlineColor = FColor::Black;
			TextItem.Depth = 3.0f;

			InCanvas->DrawItem(TextItem);

			FCanvasTextItem TextItemDistance(InPosition + FVector2D(0, -40), FText::Format(FText::FromString("{0}M"), FText::AsNumber(FMath::FloorToInt(Distance / 100.0f))), IconFont, FColor::White);
			TextItemDistance.bCentreX = true;
			TextItemDistance.bCentreY = true;
			TextItemDistance.bOutlined = true;
			TextItemDistance.OutlineColor = FColor::Black;

			InCanvas->DrawItem(TextItemDistance);
		}
	}
}

bool ACapturePoint::IsValidAltWorldIconCheck(AHUD* InOwnerHud) const
{
	APawn* MyPawn = (InOwnerHud && InOwnerHud->PlayerOwner) ? InOwnerHud->PlayerOwner->GetPawnOrSpectator() : nullptr;
	if (MyPawn)
	{
		return (FVector::Dist(GetActorLocation(), MyPawn->GetActorLocation()) < GetSize());
	}

	return false;
}