// VRSPRO

#include "LokaGame.h"
#include "UTGameState.h"
#include "UTPlayerState.h"
#include "UTCharacter.h"
#include "UTPlayerController.h"
#include "Effects/ShooterImpactEffect.h"
#include "CharacterAbility_Turret.h"


ACharacterAbility_Turret::ACharacterAbility_Turret()
	: Super()
{
	PrimaryActorTick.bCanEverTick = true;

	SphereRadius = CreateDefaultSubobject<USphereComponent>(TEXT("SphereAttackRadius"));
	SphereRadius->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	SphereRadius->SetCollisionObjectType(ECC_WorldStatic);
	SphereRadius->SetCollisionResponseToAllChannels(ECR_Ignore);
	SphereRadius->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	SphereRadius->SetGenerateOverlapEvents(true);

	RootComponent = SphereRadius;

	bIsDie = false;
	bCanFire = false;
	TriggerSphereRadius = 50.0f;
	TotalScoreReward = 100.0f;
	Health = FVector2D(1000, 1000);
	YawClamp = FVector2D(-180.0f, 180.0f);
	PitchClamp = FVector2D(-40.0f, 40.0f);
	CrankingSpeed = .0f;

	bReplicates = true;

	TurretMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("TurretMeshComp"));
	TurretMesh->SetGenerateOverlapEvents(false);
	TurretMesh->bTraceComplexOnMove = false;
	TurretMesh->bMultiBodyOverlap = false;
	TurretMesh->SetupAttachment(RootComponent);

	OuterMeshComponent = TurretMesh;
	CollisionBoxComponent->SetupAttachment(OuterMeshComponent);
}

void ACharacterAbility_Turret::PostActorCreated()
{
	Super::PostActorCreated();

	SphereRadius->InitSphereRadius(TriggerSphereRadius * 100.0f);
	SphereRadius->SetSphereRadius(TriggerSphereRadius * 100.0f);
}

void ACharacterAbility_Turret::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (!HasAnyFlags(RF_ClassDefaultObject | RF_ArchetypeObject))
	{
		SphereRadius->SetSphereRadius(TriggerSphereRadius * 100.0f);
	}
}

void ACharacterAbility_Turret::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (Role == ROLE_Authority)
	{
		if (IsActivated() && Characters.Num() && !IDestroyableActorInterface::Execute_IsDying(this))
		{
			if (TargetCharacter)
			{
				const FVector MyLocation = GetActorLocation() + RotateOffset;
				const auto TargetRotation = FRotationMatrix::MakeFromX(TargetCharacter->GetActorLocation() - MyLocation).Rotator().GetNormalized();

				TurretTargetRotation = FMath::RInterpTo(TurretTargetRotation, TargetRotation, DeltaSeconds, RotationInterp);
				TurretTargetRotation.Yaw = FMath::Clamp(TurretTargetRotation.Yaw, YawClamp.X, YawClamp.Y);
				TurretTargetRotation.Pitch = FMath::Clamp(TurretTargetRotation.Pitch, PitchClamp.X, PitchClamp.Y);
				TurretTargetRotation.Roll = .0f;

				bCanFire = TurretTargetRotation.Equals(TargetRotation, 10.0f);
				CrankingSpeed = FMath::FInterpTo(CrankingSpeed, bCanFire ? CrankingSpeedToFire : .0f, DeltaSeconds, CrankingSpeedInterp);

				if (bCanFire && CrankingSpeed >= CrankingSpeedToFire - (CrankingSpeedToFire * .8f))
				{
					if (!timer_HandleFire.IsValid())
						GetWorldTimerManager().SetTimer(timer_HandleFire, this, &ACharacterAbility_Turret::FireTurret, FireRate, true);
				}
				else
				{
					CurrentSpread = FMath::Max(FireSpread.X, CurrentSpread -= DeltaSeconds);
					CrankingSpeed = FMath::FInterpTo(CrankingSpeed, .0f, DeltaSeconds, CrankingSpeedInterp);

					if (timer_HandleFire.IsValid())
					{
						GetWorldTimerManager().ClearTimer(timer_HandleFire);
					}
				}
			}
			else
			{
				if (timer_HandleFire.IsValid())
				{
					GetWorldTimerManager().ClearTimer(timer_HandleFire);
				}

				CrankingSpeed = FMath::FInterpTo(CrankingSpeed, .0f, DeltaSeconds, CrankingSpeedInterp);
				TurretTargetRotation = FMath::RInterpTo(TurretTargetRotation, GetActorRotation(), DeltaSeconds, RotationInterp);
				bCanFire = false;
			}
		}
		else
		{
			if (timer_HandleFire.IsValid())
			{
				GetWorldTimerManager().ClearTimer(timer_HandleFire);
			}

			CrankingSpeed = FMath::FInterpTo(CrankingSpeed, .0f, DeltaSeconds, CrankingSpeedInterp);
			TurretTargetRotation = FMath::RInterpTo(TurretTargetRotation, GetActorRotation(), DeltaSeconds, RotationInterp);
			bCanFire = false;
		}
	}
}

void ACharacterAbility_Turret::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);

	if (Role == ROLE_Authority)
	{
		if (auto OtherCharacter = Cast<ACharacter>(OtherActor))
			Characters.AddUnique(OtherCharacter);
	}
}

void ACharacterAbility_Turret::NotifyActorEndOverlap(AActor* OtherActor)
{
	Super::NotifyActorEndOverlap(OtherActor);

	if (Role == ROLE_Authority)
	{
		if (auto OtherCharacter = Cast<ACharacter>(OtherActor))
			Characters.Remove(OtherCharacter);
	}
}

bool ACharacterAbility_Turret::RequestEndActivate()
{
	if (Super::RequestEndActivate())
	{
		TArray<AActor*> Actors;
		SphereRadius->GetOverlappingActors(Actors, AUTCharacter::StaticClass());

		if (Actors.Num())
		{
			for (auto a : Actors)
			{
				if (auto Char = Cast<AUTCharacter>(a))
				{
					Characters.Add(Char);
				}
			}
		}

		return true;
	}

	return false;
}

void ACharacterAbility_Turret::RequestDeactivate(const bool IsDestroy)
{
	Super::RequestDeactivate(IsDestroy);

	bCanFire = false;
	Characters.Empty();
}

void ACharacterAbility_Turret::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ACharacterAbility_Turret, bCanFire);
	DOREPLIFETIME(ACharacterAbility_Turret, TurretTargetRotation);
	DOREPLIFETIME(ACharacterAbility_Turret, HitNotify);
	DOREPLIFETIME(ACharacterAbility_Turret, CrankingSpeed);
}

void ACharacterAbility_Turret::OnRep_IsActivated()
{
	Super::OnRep_IsActivated();

	if (bIsActivated)
	{
		if (Role == ROLE_Authority)
		{
			GetWorldTimerManager().SetTimer(timer_HandleFindTarget, this, &ACharacterAbility_Turret::FindTarget, 0.5f, true);
		}
	}
	else
	{
		if (Role == ROLE_Authority)
		{
			GetWorldTimerManager().ClearTimer(timer_HandleFindTarget);
		}
	}
}

void ACharacterAbility_Turret::FindTarget()
{
	TargetCharacter = nullptr;
	const FVector TurretOffset = GetActorLocation() + RotateOffset;
	float DistanceTarget = TriggerSphereRadius * 100.0f;

	auto World = GetWorld();

	if (Characters.Num() && World)
	{
		if(auto MyGameState = World->GetGameState<AUTGameState>())
		{
			for (auto Char : Characters)
			{
				if (Char && Char != GetOwner())
				{
					FHitResult HitResult;

					FCollisionQueryParams Param;
					Param.AddIgnoredActor(this);
					Param.bTraceAsyncScene = true;
					Param.bTraceComplex = true;

					const FVector TargetLocation = Char->GetActorLocation();

					World->LineTraceSingleByChannel(HitResult, TurretOffset, TargetLocation, COLLISION_TRACE_WEAPONNOCHARACTER, Param);

					const auto CurrentDistance = FVector::Dist(TargetLocation, TurretOffset);

					if (HitResult.GetActor() && CurrentDistance < DistanceTarget && HitResult.GetActor() == Char)
					{
						if (auto TracedCharacter = Cast<AUTCharacter>(HitResult.GetActor()))
						{
							if (TracedCharacter->IsAlive())
							{
								DistanceTarget = FVector::Dist(TargetLocation, TurretOffset);

								if (MyGameState->OnSameTeam(GetOwner(), TracedCharacter) == false)
								{
									TargetCharacter = TracedCharacter;
								}
							}
						}
					}
				}
			}
		}
	}
}

void ACharacterAbility_Turret::FireTurret()
{
	const int32 RandomSeed = FMath::Rand();
	FRandomStream WeaponRandomStream(RandomSeed);
	const float ConeHalfAngle = FMath::DegreesToRadians(CurrentSpread * 0.5f);

	const auto ForwardVector = TurretTargetRotation.RotateVector(FVector::ForwardVector);
	const auto StartVector = TurretTargetRotation.RotateVector(FireOffset);

	const FVector StartTrace = GetActorLocation() + StartVector;
	const FVector ShootDir = WeaponRandomStream.VRandCone(ForwardVector, ConeHalfAngle, ConeHalfAngle);
	const FVector EndTrace = StartTrace + ForwardVector * TriggerSphereRadius * 100.0f;

	HitNotify.Origin = StartTrace;
	HitNotify.RandomSeed = RandomSeed;
	HitNotify.ReticleSpread = CurrentSpread;

	FCollisionQueryParams TraceParams(TEXT("Ability_TurretTrace"), true, this);
	TraceParams.bTraceAsyncScene = true;
	TraceParams.bReturnPhysicalMaterial = true;

	FHitResult HitResult(ForceInit);
	GetWorld()->LineTraceSingleByChannel(HitResult, StartTrace, EndTrace, COLLISION_TRACE_WEAPONNOCHARACTER, TraceParams);

	auto MyOwner = Cast<AUTCharacter>(GetOwner());
	if (HitResult.GetActor())
	{			
		HitResult.GetActor()->TakeDamage(Damage, FPointDamageEvent(Damage, HitResult, ShootDir, UDamageType::StaticClass()), MyOwner ? MyOwner->GetController() : nullptr, this);
	}

	CurrentSpread = FMath::Clamp(CurrentSpread + FireSpread.X * .1f, FireSpread.X, FireSpread.Y);
}

void ACharacterAbility_Turret::SimulateFireTurret()
{
	if (bCanFire)
	{
		FRandomStream WeaponRandomStream(HitNotify.RandomSeed);
		const float ConeHalfAngle = FMath::DegreesToRadians(HitNotify.ReticleSpread * 0.5f);
		const auto ForwardVector = TurretTargetRotation.RotateVector(FVector::ForwardVector);
		const FVector StartTrace = HitNotify.Origin;
		const FVector ShootDir = WeaponRandomStream.VRandCone(ForwardVector, ConeHalfAngle, ConeHalfAngle);
		const FVector EndTrace = StartTrace + ForwardVector * TriggerSphereRadius * 100.0f;

		FCollisionQueryParams TraceParams(TEXT("Ability_TurretTrace"), true, this);
		TraceParams.bTraceAsyncScene = true;
		TraceParams.bReturnPhysicalMaterial = true;

		FHitResult HitResult(ForceInit);
		GetWorld()->LineTraceSingleByChannel(HitResult, StartTrace, EndTrace, COLLISION_WEAPON, TraceParams);		

		FTransform const SpawnTransform(HitResult.ImpactNormal.Rotation(), HitResult.ImpactPoint);
		AShooterImpactEffect* EffectActor = GetWorld()->SpawnActorDeferred<AShooterImpactEffect>(ImpactEffect, SpawnTransform);

		if (EffectActor)
		{
			EffectActor->SurfaceHit = HitResult;
			EffectActor->FinishSpawning(SpawnTransform);

			FTransform const SpawnEffectTransform(TurretTargetRotation, StartTrace);
			//
			UGameplayStatics::SpawnSoundAtLocation(this, FireSound, StartTrace, TurretTargetRotation);
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FireEffect, SpawnEffectTransform, true);

			UParticleSystemComponent* TrailPSC = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), TraceFX, SpawnEffectTransform);
			if (TrailPSC)
			{
				TrailPSC->SetVectorParameter(TEXT("HitLocation"), HitResult.ImpactPoint);
				TrailPSC->SetVectorParameter(TEXT("LocalHitLocation"), SpawnEffectTransform.InverseTransformPositionNoScale(HitResult.ImpactPoint));
			}
		}
	}
}

void ACharacterAbility_Turret::OnRep_HitNotify()
{
	SimulateFireTurret();
}
