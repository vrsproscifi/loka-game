// VRSPRO

#pragma once

#include "UTGameObjective.h"
#include "Interfaces/IconsDrawInterface.h"
#include "CapturePoint.generated.h"


typedef TMap<class AUTPlayerState*, float> TOnHoldPlayers;

DECLARE_DELEGATE_TwoParams(FOnCapturePointHold, const class ACapturePoint*, TOnHoldPlayers);

UCLASS()
class ACapturePoint : public AUTGameObjective, public IIconsDrawInterface
{
	GENERATED_BODY()

public:	
	ACapturePoint();

	virtual void PostInitializeComponents() override;
	virtual void Tick(float DeltaSeconds) override;	

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	bool IsHold() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	bool IsCapturing() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	bool IsInAnyTeamWithout(const int32& Team) const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	int32 GetCapturingTeam() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	int32 GetHoldTeam() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	float GetCapturingPercent() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	float GetSize() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Display)
	UMaterialInstanceDynamic* GetMarkMaterial() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Display)
	UMaterialInstanceDynamic* GetMarkMaterialBG() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Display)
	FString GetPointName() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Display)
	FVector GetPointDrawPosition() const;

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	void OnLeavePoint(class AUTCharacter*);

	FOnCapturePointHold OnCapturePointHold;

	virtual bool IsUseAlternativeRadarMethod() const override { return false; }

protected:

	UFUNCTION()
	void RefreshInSphereCharacters();

	UPROPERTY(EditDefaultsOnly, Category = Default)
	UVectorFieldComponent* IconPoint;

	UPROPERTY(VisibleDefaultsOnly, Category = Default)
	USphereComponent* Sphere;

	UPROPERTY(EditDefaultsOnly, Category = Default)
	UMaterialInstanceDynamic* Mark;

	UPROPERTY(EditDefaultsOnly, Category = Default)
	UMaterialInstanceDynamic* MarkBG;

	UPROPERTY(EditDefaultsOnly, Category = Default)
	UMaterialInterface* MarkInst;

	UPROPERTY(EditDefaultsOnly, Category = Default)
	UMaterialInterface* MarkInstBG;

	UPROPERTY(EditDefaultsOnly, Category = Default)
	UMaterialInstanceDynamic* AroundMaterial;

	UPROPERTY(EditDefaultsOnly, Category = Default)
	UMaterialInterface* AroundMaterialInstance;

	UPROPERTY(EditDefaultsOnly, Category = Default)
	UStaticMeshComponent* AroundSphere;

	UPROPERTY(EditAnywhere, Category = Configuration)
	FSlateFontInfo IconFont;

	UPROPERTY(Transient, Replicated)
	float CurrentPoints;

	//UPROPERTY(Transient, Replicated)
	//int32 CurrentTeam;
	
	UPROPERTY(Transient, Replicated)
	int32 ControllTeam;

	UPROPERTY(EditAnywhere, Category = Configuration)
	float TeamPointsSpeedPerSecond;

	UPROPERTY(EditAnywhere, Category = Configuration)
	float DownPointsSpeedPerSecond;

	UPROPERTY(EditAnywhere, Category = Configuration)
	FString PointName;

	UPROPERTY(EditAnywhere, Category = Configuration)
	bool IsAllowCapturingUnder;	

	TArray<TArray<class AUTCharacter*>> CountCaptures;

	TOnHoldPlayers HoldPlayers;

	UFUNCTION()
	void OnBeginOverlap(class UPrimitiveComponent* OverlappedComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult);

	UFUNCTION()
	void OnEndOverlap(class UPrimitiveComponent* OverlappedComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

public:

	virtual void DrawRadarIcon_Implementation(AHUD* InOwnerHud, UCanvas* InCanvas, FVector2D InPosition, const float InScale = 1.0f) override;
	virtual void DrawMinimapIcon_Implementation(AHUD* InOwnerHud, UCanvas* InCanvas, FVector2D InPosition, const float InScale = 1.0f) override;
	virtual void DrawWorldIcon_Implementation(AHUD* InOwnerHud, UCanvas* InCanvas, FVector2D InPosition, const float InScale = 1.0f) override;

	virtual FVector GetWorldIconLocation() override { return GetPointDrawPosition(); }
	virtual bool IsValidAltWorldIconCheck(AHUD* InOwnerHud) const override;
};
