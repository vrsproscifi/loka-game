// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "UTGameState.h"
#include "UTPlayerState.h"
#include "UTCharacter.h"
#include "UTPlayerController.h"
#include "CharacterAbility_Installable.h"


ACharacterAbility_Installable::ACharacterAbility_Installable()
	: Super()
{
	CollisionBoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("CollisionBox"));
	CollisionBoxComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	CollisionBoxComponent->SetCollisionResponseToAllChannels(ECR_Ignore);

	bIsUsedAsNonAbility = false;
	bIsDie = false;
	TotalScoreReward = 100.0f;
	Health = FVector2D(1000, 1000);

	bReplicates = true;
}

void ACharacterAbility_Installable::SetIsUsedAsNonAbility(const bool IsAsNonAbility)
{
	bIsUsedAsNonAbility = IsAsNonAbility;

	if (bIsUsedAsNonAbility)
	{
		IsBeginPlace = false;
		bIsActivated = true;
		OnRep_IsActivated();
	}
}

void ACharacterAbility_Installable::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ACharacterAbility_Installable, AbilityTransform);
	DOREPLIFETIME(ACharacterAbility_Installable, Health);
	DOREPLIFETIME(ACharacterAbility_Installable, bIsDie);

	DOREPLIFETIME_CONDITION(ACharacterAbility_Installable, IsBeginPlace, COND_OwnerOnly);
}

void ACharacterAbility_Installable::OnRep_AbilityTransform()
{
	SetActorTransform(AbilityTransform);
}

void ACharacterAbility_Installable::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (!HasAnyFlags(RF_ClassDefaultObject | RF_ArchetypeObject) && !IsUsedAsNonAbility())
	{
		RequestDeactivate();
	}
	else if (IsUsedAsNonAbility())
	{
		SetIsUsedAsNonAbility(true);
	}
}

bool ACharacterAbility_Installable::RequestStartActivate()
{
	if (IsAllowActivate() && !IsUsedAsNonAbility())
	{
		IsBeginPlace = true;
		return true;
	}

	return false;
}

bool ACharacterAbility_Installable::RequestEndActivate()
{
	if (!IsUsedAsNonAbility())
	{
		if (IsAllowActivate() && IsBeginPlace)
		{
			IsBeginPlace = false;

			if (TryPlacement())
			{
				Super::RequestEndActivate();
				OnRep_IsActivated();

				AbilityTransform = GetActorTransform();
				return true;
			}
		}
	}
	return false;
}

void ACharacterAbility_Installable::RequestDeactivate(const bool IsDestroy)
{
	if (!IsUsedAsNonAbility())
	{
		Super::RequestDeactivate(IsDestroy);
		OnRep_IsActivated();
	}
}

void ACharacterAbility_Installable::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (Role != ROLE_Authority)
	{
		if (IsBeginPlace && MIDs.Num() && !IsUsedAsNonAbility())
		{
			const bool IsAllow = TryPlacement();
			for (auto mid : MIDs)
			{
				if (auto m = GetValidObject(mid))
				{
					m->SetScalarParameterValue(TEXT("IsRedHolo"), IsAllow ? 0.0f : 1.0f);
				}
			}
		}
	}
}

float ACharacterAbility_Installable::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
	if (Role == ROLE_Authority && !IDestroyableActorInterface::Execute_IsDying(this) && !IsUsedAsNonAbility())
	{
		Health.X -= DamageAmount;

		if (const auto instigator = GetValidObject(EventInstigator))
		{
			if (const auto DamagerPlayerState = GetValidObject(instigator->GetPlayerState<AUTPlayerState>()))
			{
				float& Value = DamageByPlayers.FindOrAdd(DamagerPlayerState);
				Value += DamageAmount;
			}
		}

		if (Health.X <= .0f)
		{
			if (auto MyGameState = GetWorldGameState<AUTGameState>(GetWorld()))
			{
				const auto MyOwner = GetValidObject<AUTCharacter, AActor>(GetOwner());
				if (MyOwner)
				{
					for (auto &i : DamageByPlayers)
					{
						if (i.Value > .0f)
						{
							const int32 ScaledScore = FMath::CeilToInt(TotalScoreReward * (i.Value / Health.Y));

							auto TargetPS = GetValidObject(i.Key);
							if (TargetPS)
							{
								auto TargetPC = GetValidObject<AUTPlayerController, AActor>(TargetPS->GetOwner());
								if (MyGameState->OnSameTeam(MyOwner->GetPlayerState(), TargetPC))
								{
									TargetPS->ScorePoints(-ScaledScore);
									TargetPC->ClientShowActionMessage(EActionMessage::AbilityKill, -ScaledScore);
								}
								else
								{
									TargetPS->ScorePoints(ScaledScore);
									TargetPC->ClientShowActionMessage(EActionMessage::AbilityKill, ScaledScore);
								}
							}
						}
					}
				}
			}			

			bIsDie = true;

			RequestDeactivate();
			IDestroyableActorInterface::Execute_OnDeath(this);
		}
	}

	return Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
}

void ACharacterAbility_Installable::OnRep_IsActivated()
{
	if (!IsUsedAsNonAbility())
	{
		if (auto mesh = GetValidObject(OuterMeshComponent))
		{
			if (bIsActivated)
			{
				mesh->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
				mesh->SetCollisionResponseToAllChannels(ECR_Block);
				mesh->SetVisibility(true, true);

				if (Role == ROLE_Authority)
				{
					DamageByPlayers.Empty();
					Health.X = Health.Y;
				}
			}
			else
			{
				mesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
				mesh->SetVisibility(false, true);
			}
		}
		Super::OnRep_IsActivated();
	}
}

void ACharacterAbility_Installable::OnRep_IsBeginPlace()
{
	if (!IsUsedAsNonAbility())
	{
		if (auto mesh = GetValidObject(OuterMeshComponent))
		{
			if (IsBeginPlace)
			{
				if (MIDs.Num())
				{
					for (int32 i = 0; i <= mesh->GetNumMaterials(); ++i)
					{
						if (const auto m = GetValidObject(MIDs[i]))
						{
							mesh->SetMaterial(i, m);
						}
					}
				}
				else
				{
					for (int32 i = 0; i <= mesh->GetNumMaterials(); ++i)
					{
						if (auto mid = mesh->CreateAndSetMaterialInstanceDynamicFromMaterial(i, HoloMaterials.Num() ? (HoloMaterials.IsValidIndex(i) ? HoloMaterials[i] : HoloMaterials[0]) : nullptr))
							MIDs.Add(mid);
					}
				}

				mesh->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
				mesh->SetCollisionResponseToAllChannels(ECR_Overlap);
				mesh->SetVisibility(true, true);
			}
			else
			{
				for (int32 i = 0; i <= mesh->GetNumMaterials(); ++i)
				{
					mesh->SetMaterial(i, nullptr);
				}

				if (!bIsActivated)
				{
					mesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
					mesh->SetVisibility(false, true);
				}
			}
		}
	}
}

#if WITH_EDITOR
void DrawDebugSweptBoxAbility(const UWorld* InWorld, FVector const& Start, FVector const& End, FRotator const & Orientation, FVector const & HalfSize, FColor const& Color, bool bPersistentLines = false, float LifeTime = -1.f, uint8 DepthPriority = 0)
{
	FVector const TraceVec = End - Start;
	float const Dist = TraceVec.Size();

	FVector const Center = Start + TraceVec * 0.5f;

	FQuat const CapsuleRot = Orientation.Quaternion();
	::DrawDebugBox(InWorld, Start, HalfSize, CapsuleRot, Color, bPersistentLines, LifeTime, DepthPriority);

	//now draw lines from vertices
	FVector Vertices[8];
	Vertices[0] = Start + CapsuleRot.RotateVector(FVector(-HalfSize.X, -HalfSize.Y, -HalfSize.Z));	//flt
	Vertices[1] = Start + CapsuleRot.RotateVector(FVector(-HalfSize.X, HalfSize.Y, -HalfSize.Z));	//frt
	Vertices[2] = Start + CapsuleRot.RotateVector(FVector(-HalfSize.X, -HalfSize.Y, HalfSize.Z));	//flb
	Vertices[3] = Start + CapsuleRot.RotateVector(FVector(-HalfSize.X, HalfSize.Y, HalfSize.Z));	//frb
	Vertices[4] = Start + CapsuleRot.RotateVector(FVector(HalfSize.X, -HalfSize.Y, -HalfSize.Z));	//blt
	Vertices[5] = Start + CapsuleRot.RotateVector(FVector(HalfSize.X, HalfSize.Y, -HalfSize.Z));	//brt
	Vertices[6] = Start + CapsuleRot.RotateVector(FVector(HalfSize.X, -HalfSize.Y, HalfSize.Z));	//blb
	Vertices[7] = Start + CapsuleRot.RotateVector(FVector(HalfSize.X, HalfSize.Y, HalfSize.Z));		//brb
	for (int32 VertexIdx = 0; VertexIdx < 8; ++VertexIdx)
	{
		::DrawDebugLine(InWorld, Vertices[VertexIdx], Vertices[VertexIdx] + TraceVec, Color, bPersistentLines, LifeTime, DepthPriority);
	}

	::DrawDebugBox(InWorld, End, HalfSize, CapsuleRot, Color, bPersistentLines, LifeTime, DepthPriority);
}
#endif

bool ACharacterAbility_Installable::TryPlacement()
{
	const auto owner = GetValidObject(GetOwner());
	if(owner == nullptr)
	{
		return false;
	}

	if (CollisionBoxComponent == nullptr)
	{
		return false;
	}

	const auto Location = owner->GetActorLocation();
	const auto Direction = owner->GetActorForwardVector();
	const auto CheckExtent = CollisionBoxComponent->GetScaledBoxExtent();
	const auto TargetLocation = Location + Direction * 300.0f;
	const FCollisionQueryParams QueryParams(TEXT("HealthPost_Check"), true, this);

	FHitResult HitResult;
	GetWorld()->LineTraceSingleByChannel(HitResult, TargetLocation + FVector(0, 0, 100.0f), TargetLocation + FVector(0, 0, -200.0f), ECC_WorldDynamic, QueryParams);

	if (HitResult.ImpactPoint.IsNearlyZero(1.0f))
	{
		return false;
	}

	SetActorLocation(HitResult.ImpactPoint);
	SetActorRotation(owner->GetActorRotation());

	GetWorld()->SweepSingleByChannel(HitResult, CollisionBoxComponent->GetComponentLocation(), CollisionBoxComponent->GetComponentLocation() + FVector(0, 0, CheckExtent.Z), FRotator::ZeroRotator.Quaternion(), ECC_WorldDynamic, FCollisionShape::MakeBox(CheckExtent), QueryParams);

#if WITH_EDITOR
	if (HitResult.bBlockingHit)
	{
		// Red up to the blocking hit, green thereafter
		::DrawDebugSweptBoxAbility(GetWorld(), HitResult.TraceStart, HitResult.Location, FRotator::ZeroRotator, CheckExtent, FColor::Green, false, .0f);
		::DrawDebugSweptBoxAbility(GetWorld(), HitResult.Location, HitResult.TraceEnd, FRotator::ZeroRotator, CheckExtent, FColor::Red, false, .0f);
		::DrawDebugPoint(GetWorld(), HitResult.ImpactPoint, 16.0f, FColor::Red, false, .0f);
	}
	else
	{
		// no hit means all red
		::DrawDebugSweptBoxAbility(GetWorld(), HitResult.TraceStart, HitResult.TraceEnd, FRotator::ZeroRotator, CheckExtent, FColor::Green, false, .0f);
	}
#endif

	return !HitResult.bBlockingHit;
}

bool ACharacterAbility_Installable::GetIsVisiblyUserInterface_Implementation() const
{
	if (GetNetMode() != NM_DedicatedServer)
	{		
		return OuterMeshComponent && (FMath::IsNearlyEqual(OuterMeshComponent->LastRenderTimeOnScreen, GetWorld()->GetTimeSeconds(), 2.0f) && !IsUsedAsNonAbility());
	}

	return false;
}