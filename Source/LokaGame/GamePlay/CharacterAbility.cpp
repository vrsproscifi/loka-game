// VRSPRO

#include "LokaGame.h"
#include "CharacterAbility.h"


ACharacterAbility::ACharacterAbility()
{
	bIsActivated = false;
	PrimaryActorTick.bCanEverTick = true;
	LifeTimeOwnerDeath = 2.0f;
	AuthorityTimeUpdateInterval = .0f;

	bReplicates = true;
}

void ACharacterAbility::BeginPlay()
{
	Super::BeginPlay();
	RequestDeactivate();	
#if !UE_SERVER
	auto InputSettings = const_cast<UInputSettings*>(GetDefault<UInputSettings>());
	for (auto Key : InputSettings->ActionMappings)
	{
		if (Key.ActionName == "UseAbility")
		{
			KeyToUse = Key.Key;
			break;
		}
	}
#endif
}

void ACharacterAbility::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (Role == ROLE_Authority)
	{
		AuthorityTimeUpdateInterval += DeltaSeconds;
		if (AuthorityTimeUpdateInterval > 0.2f)
		{
			AuthorityTimeUpdateInterval = .0f;
			AuthorityTime = GetWorld()->GetTimeSeconds();
		}
	}
	else
	{
		AuthorityTime += DeltaSeconds;
	}
}

bool ACharacterAbility::RequestStartActivate()
{
	return false;
}

bool ACharacterAbility::RequestEndActivate()
{
	ActivatedTime = AuthorityTime;
	GetWorldTimerManager().SetTimer(timer_Deactivate, FTimerDelegate::CreateUObject(this, &ACharacterAbility::RequestDeactivate, false), ActiveTime, false);
	bIsActivated = true;

	return false;
}

void ACharacterAbility::RequestDeactivate(const bool IsDestroy)
{
	if (GetWorld() && (IsAllowDeactivate() || IsDestroy))
	{
		DeactivatedTime = AuthorityTime;
		GetWorldTimerManager().ClearTimer(timer_Deactivate);
		bIsActivated = false;

		if (IsDestroy)
		{
			SetLifeSpan(LifeTimeOwnerDeath);
		}
	}
}

bool ACharacterAbility::IsAllowActivate() const
{
	return (GetWorld() && AuthorityTime > DeactivatedTime + IntervalTime && !bIsActivated);
}

bool ACharacterAbility::IsAllowDeactivate() const
{
	return (GetWorld() && AuthorityTime > ActivatedTime + IntervalDeactivate && bIsActivated);
}

void ACharacterAbility::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ACharacterAbility, bIsActivated);

	DOREPLIFETIME_CONDITION(ACharacterAbility, ActivatedTime, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(ACharacterAbility, DeactivatedTime, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(ACharacterAbility, AuthorityTime, COND_OwnerOnly);
}

void ACharacterAbility::OnRep_IsActivated()
{
	OnActiveStateChanged(bIsActivated);
}

float ACharacterAbility::GetAbilityReadyPercent() const
{
	if (GetWorld())
	{
		const float CurrentTime = AuthorityTime - DeactivatedTime;
		return FMath::Clamp<float>(CurrentTime / IntervalTime, 0.0f, 1.0f);
	}
	return .0f;
}

float ACharacterAbility::GetAbilityUsePercent() const
{
	if (GetWorld())
	{
		const float CurrentTime = (ActivatedTime + ActiveTime) - AuthorityTime;
		return FMath::Clamp<float>(CurrentTime / ActiveTime, 0.0f, 1.0f);
	}
	return .0f;
}

void ACharacterAbility::OnAbilityInput(const EAbilityInput InInput, const bool IsHold)
{
	if (Role != ROLE_Authority)
	{
		OnAbilityInputServer(InInput, IsHold);
	}

	K2_OnAbilityInput(InInput, IsHold);
}

bool ACharacterAbility::OnAbilityInputServer_Validate(const EAbilityInput InInput, const bool IsHold) { return true; }
void ACharacterAbility::OnAbilityInputServer_Implementation(const EAbilityInput InInput, const bool IsHold)
{
	OnAbilityInput(InInput, IsHold);
}