// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#include "LokaGame.h"
#include "UTCharacter.h"
#include "UTPlayerState.h"
#include "UTPlayerStart.h"

AUTPlayerStart::AUTPlayerStart(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	Capsule_Radius = CreateDefaultSubobject<UCapsuleComponent>("Capsule_Radius");
	Capsule_Radius->SetupAttachment(RootComponent);
	Capsule_Radius->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	Capsule_Radius->SetCollisionResponseToAllChannels(ECR_Ignore);
	Capsule_Radius->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
}

#if WITH_EDITOR
void AUTPlayerStart::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);

	if (PropertyChangedEvent.Property && PropertyChangedEvent.Property->GetFName() == GET_MEMBER_NAME_CHECKED(AUTPlayerStart, SupportGameModesFixed))
	{
		SupportGameModes = 0;
		for (auto support : SupportGameModesFixed)
		{
			SupportGameModes = SupportGameModes | support;
		}
	}
}
#endif

bool AUTPlayerStart::IsAnyCharacterOverlap() const
{
	if (GetWorld())
	{
		TArray<AActor*> Actors;
		Capsule_Radius->GetOverlappingActors(Actors, ACharacter::StaticClass());

		for (auto &Actor : Actors)
		{
			if (auto c = Cast<AUTCharacter>(Actor))
			{
				if (c && c->IsAlive())
				{
					return true;
				}
			}
		}
	}

	return false;
}

bool AUTPlayerStart::IsOtherTeamCharacterOverlap() const
{
	if (GetWorld())
	{
		TArray<AActor*> Actors;
		Capsule_Radius->GetOverlappingActors(Actors, ACharacter::StaticClass());

		if (Actors.Num())
		{
			for (auto &Actor : Actors)
			{
				if (auto c = Cast<AUTCharacter>(Actor))
				{
					if (c && c->IsAlive() && c->GetPlayerState())
					{
						if (auto p = Cast<AUTPlayerState>(c->GetPlayerState()))
						{
							if (p->GetTeamNum() != TeamNum)
							{
								return true;
							}
						}
					}
				}
			}
		}
	}

	return false;
}