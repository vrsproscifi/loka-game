// VRSPRO

#include "LokaGame.h"
#include "UTGameState.h"
#include "UTPlayerState.h"
#include "UTCharacter.h"
#include "UTPlayerController.h"
#include "CharacterAbility_HealthPost.h"


ACharacterAbility_HealthPost::ACharacterAbility_HealthPost()
	: Super()
{
	PostRoot = CreateDefaultSubobject<UVectorFieldComponent>(TEXT("PostRoot"));
	RootComponent = PostRoot;

	PostMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PostMeshComp"));
	PostMesh->SetGenerateOverlapEvents(true);
	PostMesh->bTraceComplexOnMove = false;
	PostMesh->bMultiBodyOverlap = false;
	PostMesh->SetupAttachment(RootComponent);

	PostSphere = CreateDefaultSubobject<USphereComponent>(TEXT("PostSphereComp"));
	PostSphere->OnComponentBeginOverlap.AddDynamic(this, &ACharacterAbility_HealthPost::OnBeginOverlapSphere);
	PostSphere->OnComponentEndOverlap.AddDynamic(this, &ACharacterAbility_HealthPost::OnEndOverlapSphere);
	PostSphere->SetGenerateOverlapEvents(true);
	PostSphere->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	PostSphere->SetCollisionResponseToAllChannels(ECR_Overlap);
	PostSphere->SetupAttachment(RootComponent);	

	OuterMeshComponent = PostMesh;
	CollisionBoxComponent->SetupAttachment(OuterMeshComponent);
	HealthRegenerationSpeed = .2f;
}

void ACharacterAbility_HealthPost::PostInitializeComponents()
{
	Super::PostInitializeComponents();
}

void ACharacterAbility_HealthPost::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (Role == ROLE_Authority)
	{
		if (IsActivated() && Characters.Num() && GetWorld())
		{
			if (auto MyGameState = GetWorld()->GetGameState<AUTGameState>())
			{
				if (const auto MyOwner = GetValidObject<AUTCharacter, AActor>(GetOwner()))
				{
					for (auto Char : Characters)
					{
						if (const auto c = GetValidObject(Char))
						{
							if (c->IsAlive())
							{
								if (MyGameState->OnSameTeam(MyOwner->GetPlayerState(), c->GetPlayerState()))
								{
									const auto OldHealth = c->Health.Amount;
									c->Health.Amount = FMath::FInterpTo(c->Health.Amount, c->Health.MaxAmount, DeltaSeconds, HealthRegenerationSpeed);
									auto &Value = RegeneratedHealths.FindOrAdd(c->GetPlayerState());
									Value += c->Health.Amount - OldHealth;
								}
								else
								{
									const auto OldHealth = c->Health.Amount;
									c->Health.Amount = FMath::FInterpTo(c->Health.Amount, 5.0f, DeltaSeconds, HealthRegenerationSpeed / 3.0f);
									auto &Value = RegeneratedHealths.FindOrAdd(c->GetPlayerState());
									Value += (c->Health.Amount - OldHealth) * -1.0f;
								}
							}
						}
					}
				}
			}
		}
	}
}

bool ACharacterAbility_HealthPost::RequestEndActivate()
{
	if (Super::RequestEndActivate())
	{
		RegeneratedHealths.Empty();

		TArray<AActor*> Actors;
		PostSphere->GetOverlappingActors(Actors, AUTCharacter::StaticClass());

		if (Actors.Num())
		{
			for (auto a : Actors)
			{
				if (auto Char = GetValidObject<AUTCharacter, AActor>(a))
				{
					Characters.Add(Char);
				}
			}
		}

		return true;		
	}

	return false;
}

void ACharacterAbility_HealthPost::RequestDeactivate(const bool IsDestroy)
{
	Super::RequestDeactivate(IsDestroy);

	for (auto i : RegeneratedHealths)
	{
		GiveRewardFrom(i.Key);
	}

	Characters.Empty();
}

void ACharacterAbility_HealthPost::OnBeginOverlapSphere(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if (Role == ROLE_Authority && IsActivated() && OtherActor)
	{
		if (auto Char = Cast<AUTCharacter>(OtherActor))
		{
			Characters.Add(Char);
		}
	}
}

void ACharacterAbility_HealthPost::OnEndOverlapSphere(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (Role == ROLE_Authority && IsActivated() && OtherActor)
	{
		if (auto Char = GetValidObject<AUTCharacter, AActor>(OtherActor))
		{
			GiveRewardFrom(Char->GetPlayerState());
			Characters.Remove(Char);
		}
	}
}

void ACharacterAbility_HealthPost::GiveRewardFrom(APlayerState* InState)
{
	if (InState && RegeneratedHealths.Contains(InState))
	{
		if (const auto MyOwner = GetValidObject<AUTCharacter, AActor>(GetOwner()))
		{
			auto ReHeal = RegeneratedHealths.FindChecked(InState);
			const int32 ScaledScore = FMath::CeilToInt(ReHeal / 2.5f);

			auto MyPlayerState = GetValidObject<AUTPlayerState, APlayerState>(MyOwner->GetPlayerState());
			auto MyCtrl = GetValidObject<AUTBasePlayerController, AController>(MyOwner->GetController());

			if (MyPlayerState)
			{
				MyPlayerState->ScorePoints(ScaledScore);
			}

			if (MyCtrl)
			{
				MyCtrl->ClientShowActionMessage(EActionMessage::AbilityAction, ScaledScore);
			}
		}

		RegeneratedHealths.Remove(InState);
	}
}