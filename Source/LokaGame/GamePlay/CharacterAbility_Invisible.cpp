// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "CharacterAbility_Invisible.h"

#include "UTCharacter.h"


ACharacterAbility_Invisible::ACharacterAbility_Invisible()
	: Super()
{

}

bool ACharacterAbility_Invisible::RequestStartActivate()
{
	if (IsAllowActivate())
	{
		return true;
	}

	return false;
}

bool ACharacterAbility_Invisible::RequestEndActivate()
{
	if (IsAllowActivate())
	{
		Super::RequestEndActivate();
		OnRep_IsActivated();

		return true;
	}

	return false;
}

void ACharacterAbility_Invisible::RequestDeactivate(const bool IsDestroy)
{
	Super::RequestDeactivate(IsDestroy);
	OnRep_IsActivated();
}

void ACharacterAbility_Invisible::PostInitializeComponents()
{
	Super::PostInitializeComponents();
}

void ACharacterAbility_Invisible::OnRep_IsActivated()
{
	if (auto MyCharacter = GetValidObject<AUTCharacter, AActor>(GetOwner()))
	{
		if (bIsActivated)
		{
			MyCharacter->SetInvisible(true);
			MyCharacter->SetSkin(InvisibleMaterial, InvisibleMaterial);
			MyCharacter->UpdateSkin();
		}
		else
		{
			MyCharacter->SetInvisible(false);
			MyCharacter->SetSkin(nullptr);
			MyCharacter->UpdateSkin();
		}
	}
}