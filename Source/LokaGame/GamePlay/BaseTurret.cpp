// VRSPRO

#include "LokaGame.h"
#include "BaseTurret.h"
#include "UTPlayerState.h"





ABaseTurret::ABaseTurret()
	: Super()
{
	PrimaryActorTick.bCanEverTick = true;

	SphereRadius = CreateDefaultSubobject<USphereComponent>(TEXT("SphereAttackRadius"));
	SphereRadius->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	SphereRadius->SetCollisionObjectType(ECC_WorldStatic);
	SphereRadius->SetCollisionResponseToAllChannels(ECR_Ignore);
	SphereRadius->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	SphereRadius->SetGenerateOverlapEvents(true);

	RootComponent = SphereRadius;

	bIsDie = false;
	bCanFire = false;
	TriggerSphereRadius = 50.0f;
	TotalScoreReward = 100.0f;
	Health = FVector2D(1000, 1000);

	bReplicates = true;
}

void ABaseTurret::PostActorCreated() 
{
	Super::PostActorCreated();

	SphereRadius->InitSphereRadius(TriggerSphereRadius * 100.0f);
	SphereRadius->SetSphereRadius(TriggerSphereRadius * 100.0f);
}

void ABaseTurret::BeginPlay()
{
	Super::BeginPlay();
	
	SphereRadius->SetSphereRadius(TriggerSphereRadius * 100.0f);
}

void ABaseTurret::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

float ABaseTurret::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
	if (Role == ROLE_Authority && !bIsDie)
	{
		Health.X -= DamageAmount;

		if (EventInstigator)
		{
			if (auto DamagerPlayerState = EventInstigator->GetPlayerState<AUTPlayerState>())
			{
				float& Damage = DamageByPlayers.FindOrAdd(DamagerPlayerState);
				Damage += DamageAmount;
			}
		}

		//if (Health.X <= .0f)
		//{
		//	for (auto &i : DamageByPlayers)
		//	{
		//		if (i.Value > .0f)
		//		{
		//			const int32 ScaledScore = FMath::CeilToInt(TotalScoreReward * (i.Value / Health.Y));
		//			i.Key->ScorePoints(ScaledScore);
		//
		//			if (auto TargetPC = i.Key->GetShooterController())
		//			{
		//				TargetPC->ClientShowActionMessage(EActionMessage::TurretKill, ScaledScore);
		//			}
		//		}
		//	}
		//
		//	bIsDie = true;
		//	bCanFire = false;
		//
		//	OnDeath();
		//}
	}

	return Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
}

void ABaseTurret::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);

	if (Role == ROLE_Authority)
	{
		if (auto OtherCharacter = Cast<ACharacter>(OtherActor))
			CharactersIntoRadius.AddUnique(OtherCharacter);
	}
}

void ABaseTurret::NotifyActorEndOverlap(AActor* OtherActor)
{
	Super::NotifyActorEndOverlap(OtherActor);

	if (Role == ROLE_Authority)
	{
		if (auto OtherCharacter = Cast<ACharacter>(OtherActor))
			CharactersIntoRadius.Remove(OtherCharacter);
	}
}

void ABaseTurret::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ABaseTurret, Health);
	DOREPLIFETIME(ABaseTurret, bIsDie);
	DOREPLIFETIME(ABaseTurret, bCanFire);
}

ACharacter *ABaseTurret::FindTarget(const FVector& TurretOffset, const ABaseTurret *Turret)
{
	ACharacter *OutCharacter = nullptr;

	if (Turret)
	{
		float DistanceTarget = Turret->TriggerSphereRadius * 100.0f;

		TArray<AActor*> Actors;
		auto World = Turret->GetWorld();
		Turret->SphereRadius->GetOverlappingActors(Actors, ACharacter::StaticClass());

		if (Actors.Num() && World)
		{
			for (auto a : Actors)
			{
				if (auto Character = Cast<ACharacter>(a))
				{					
					FHitResult HitResult;

					FCollisionQueryParams Param;
					Param.AddIgnoredActor(Turret);
					Param.bTraceAsyncScene = true;
					Param.bTraceComplex = true;

					const FVector TargetLocation = Character->GetActorLocation();
					
					World->LineTraceSingleByChannel(HitResult, TurretOffset, TargetLocation, COLLISION_WEAPON, Param);

					const auto CurrentDistance = FVector::Dist(TargetLocation, TurretOffset);

					if (HitResult.GetActor() && CurrentDistance < DistanceTarget)
					{
						// TODO: Replase to new character
						//if (auto TracedCharacter = Cast<AShooterCharacter>(HitResult.GetActor()))
						//{
						//	if (TracedCharacter->IsAlive())
						//	{
						//		DistanceTarget = FVector::Dist(TargetLocation, TurretOffset);
						//		OutCharacter = TracedCharacter;
						//	}
						//}
					}
				}
			}
		}
	}

	return OutCharacter;
}