// VRSPRO

#pragma once

#include "GameFramework/Actor.h"
#include "Interfaces/IconsDrawInterface.h"
#include "Interfaces/UsableActorInterface.h"
#include "UsableObject_AmmoBox.generated.h"

/**
 * 
 */
UCLASS()
class LOKAGAME_API AUsableObject_AmmoBox 
	: public AActor
	, public IIconsDrawInterface
	, public IUsableActorInterface
{
	GENERATED_BODY()
public:
	AUsableObject_AmmoBox();
	
	virtual void OnFocusStart_Implementation(AActor* InInstigator) override;
	virtual void OnFocusLost_Implementation(AActor* InInstigator) override;
	virtual void OnInteractStart_Implementation(AActor* InInstigator) override;
	virtual void OnInteractLost_Implementation(AActor* InInstigator) override;

	virtual void DrawMinimapIcon_Implementation(AHUD* InOwnerHud, UCanvas* InCanvas, FVector2D InPosition, const float InScale) override;
	virtual void DrawRadarIcon_Implementation(AHUD* InOwnerHud, UCanvas* InCanvas, FVector2D InPosition, const float InScale) override;
	virtual void DrawWorldIcon_Implementation(AHUD* InOwnerHud, UCanvas* InCanvas, FVector2D InPosition, const float InScale) override;

	virtual void Tick(float DeltaSeconds) override;
	virtual void PreReplication(IRepChangedPropertyTracker & ChangedPropertyTracker) override;

protected:
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Trigger)
	UStaticMeshComponent* StaticMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Trigger)
	UBoxComponent* PostProcessTrigger;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Trigger)
	UPostProcessComponent* PostProcess;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Gameplay)
	float TimeToFillCage;

	UFUNCTION()
	void StartFillCage(AActor* InInstigator);

	UFUNCTION()
	void StopFillCage(AActor* InInstigator);

	UFUNCTION()
	void OnFillCage(AActor* InInstigator);

	UPROPERTY()
	FTimerHandle timer_FillCage;

	UPROPERTY(BlueprintReadOnly, Replicated)
	bool bIsCurentlyUsed;

	UPROPERTY(BlueprintReadOnly, Replicated)
	float AuthorityTime;

	UPROPERTY(BlueprintReadOnly, Replicated)
	float ActivatedTime;

	UPROPERTY(BlueprintReadOnly, Replicated)
	AActor* User;

private:

	float AuthorityTimeUpdateInterval;
};
