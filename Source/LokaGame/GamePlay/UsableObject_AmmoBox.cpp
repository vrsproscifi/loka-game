// VRSPRO

#include "LokaGame.h"
#include "UsableObject_AmmoBox.h"

#include "UTCharacter.h"
#include "Weapon/ShooterWeapon.h"



AUsableObject_AmmoBox::AUsableObject_AmmoBox() 
	: Super()
{
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMesh->SetGenerateOverlapEvents(false);
	StaticMesh->bAutoActivate = true;
	StaticMesh->bHiddenInGame = false;
	StaticMesh->SetCollisionResponseToAllChannels(ECR_Block);
	StaticMesh->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	StaticMesh->SetCollisionObjectType(COLLISION_USABLE);
	StaticMesh->SetCustomDepthStencilValue(2);

	PostProcessTrigger = CreateDefaultSubobject<UBoxComponent>(TEXT("PostProcessTrigger"));
	PostProcessTrigger->SetCollisionResponseToAllChannels(ECR_Ignore);
	PostProcessTrigger->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	PostProcessTrigger->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	PostProcessTrigger->SetCollisionObjectType(ECC_WorldDynamic);
	PostProcessTrigger->SetupAttachment(StaticMesh);

	PostProcess = CreateDefaultSubobject<UPostProcessComponent>(TEXT("PostProcess"));
	PostProcess->SetupAttachment(PostProcessTrigger);
	PostProcess->bUnbound = false;

	RootComponent = StaticMesh;
	bReplicates = true;
}

void AUsableObject_AmmoBox::OnFocusStart_Implementation(AActor* InInstigator)
{

}

void AUsableObject_AmmoBox::OnFocusLost_Implementation(AActor* InInstigator)
{
	StopFillCage(InInstigator);
}

void AUsableObject_AmmoBox::OnInteractStart_Implementation(AActor* InInstigator)
{
	StartFillCage(InInstigator);
}

void AUsableObject_AmmoBox::OnInteractLost_Implementation(AActor* InInstigator)
{
	StopFillCage(InInstigator);
}

void AUsableObject_AmmoBox::DrawMinimapIcon_Implementation(AHUD* InOwnerHud, UCanvas* InCanvas, FVector2D InPosition, const float InScale)
{

}

void AUsableObject_AmmoBox::DrawRadarIcon_Implementation(AHUD* InOwnerHud, UCanvas* InCanvas, FVector2D InPosition, const float InScale)
{

}

void AUsableObject_AmmoBox::DrawWorldIcon_Implementation(AHUD* InOwnerHud, UCanvas* InCanvas, FVector2D InPosition, const float InScale)
{

}

void AUsableObject_AmmoBox::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	
	AuthorityTimeUpdateInterval += DeltaSeconds;
	if (AuthorityTimeUpdateInterval > 0.2f)
	{
		AuthorityTimeUpdateInterval = .0f;
	}

	if (HasAuthority())
	{
		AuthorityTime = GetWorld()->GetTimeSeconds();
	}
	else
	{
		AuthorityTime += DeltaSeconds;
	}
}

void AUsableObject_AmmoBox::PreReplication(IRepChangedPropertyTracker & ChangedPropertyTracker)
{
	Super::PreReplication(ChangedPropertyTracker);

	if (GetWorld())
	{
		DOREPLIFETIME_ACTIVE_OVERRIDE(AUsableObject_AmmoBox, AuthorityTime, AuthorityTimeUpdateInterval >= .18f);
	}
}

void AUsableObject_AmmoBox::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AUsableObject_AmmoBox, bIsCurentlyUsed);
	DOREPLIFETIME(AUsableObject_AmmoBox, AuthorityTime);
	DOREPLIFETIME(AUsableObject_AmmoBox, ActivatedTime);
	DOREPLIFETIME(AUsableObject_AmmoBox, User);
}


void AUsableObject_AmmoBox::StartFillCage(AActor* InInstigator)
{
	if (HasAuthority() && !bIsCurentlyUsed)
	{
		User = InInstigator;
		GetWorldTimerManager().SetTimer(timer_FillCage, FTimerDelegate::CreateUObject(this, &AUsableObject_AmmoBox::OnFillCage, InInstigator), TimeToFillCage, true);
		AuthorityTime = GetWorld()->GetTimeSeconds();
		ActivatedTime = GetWorld()->GetTimeSeconds();
		AuthorityTimeUpdateInterval = .18f;
		bIsCurentlyUsed = true;
	}
}

void AUsableObject_AmmoBox::StopFillCage(AActor* InInstigator)
{
	if (HasAuthority())
	{
		GetWorldTimerManager().ClearTimer(timer_FillCage);
		bIsCurentlyUsed = false;
	}
}

void AUsableObject_AmmoBox::OnFillCage(AActor* InInstigator)
{
	if (auto MyCharacter = Cast<AUTCharacter>(InInstigator))
	{
		if (MyCharacter->IsAlive())
		{
			bool IsDone = true;
			for (SIZE_T i = EShooterWeaponSlot::Primary; i < EShooterWeaponSlot::Grenade; ++i)
			{
				auto TargetWeapon = MyCharacter->Inventory[i];
				if (TargetWeapon)
				{
					if (TargetWeapon->GetCurrentAmmo() < TargetWeapon->GetMaxAmmo())
					{
						TargetWeapon->GiveAmmo(TargetWeapon->GetAmmoPerClip());
						IsDone = false;
					}
				}
			}

			if (IsDone)
			{
				StopFillCage(InInstigator);
			}
			else
			{
				ActivatedTime = GetWorld()->GetTimeSeconds();
			}
		}
	}
}