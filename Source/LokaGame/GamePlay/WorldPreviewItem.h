// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "WorldPreviewItem.generated.h"


class UItemBaseEntity;

UCLASS()
class LOKAGAME_API AWorldPreviewItem : public AActor
{
	GENERATED_BODY()
	
public:	

	AWorldPreviewItem();
	virtual void Tick(float DeltaTime) override;	
	
	UFUNCTION(BlueprintCallable, Category = Entity)
	virtual void InitializeInstance(UPlayerInventoryItem* InInstance, const bool IsControlled = false);
	virtual void InitializeInstance(UItemBaseEntity* InInstance, const bool IsControlled = false);
	virtual void InitializeModules();
	virtual void InitializeAsWeapon();

	UFUNCTION(BlueprintCallable, Category = Entity)
	virtual void AddRotationToMesh(const FRotator& InDelta);

	UFUNCTION(BlueprintCallable, Category = Visual)
	void ForceTextureStream();

	UFUNCTION(BlueprintCallable, Category = Visual)
	void CalculateAndApplyCenter();

	UFUNCTION(BlueprintCallable, Category = Visual)
	AActor* GetChildActor() const;

protected:

	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	USkeletalMeshComponent* MeshComponent;

	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	UChildActorComponent* ChildActorComponent;

	UPROPERTY()
	UPlayerInventoryItem* Instance;

	UPROPERTY()
	TArray<USkeletalMeshComponent*> InstalledModulesMesh;

};
