// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "CharacterAbility_Flying.h"

#include "UTCharacter.h"
#include "UTGameUserSettings.h"

ACharacterAbility_Flying::ACharacterAbility_Flying()
	: Super()
{
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));

	AirEngineParticleOne = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("AirEngineParticleOne"));
	AirEngineParticleOne->bIsActive = false;
	AirEngineParticleOne->bAutoActivate = false;

	AirEngineParticleTwo = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("AirEngineParticleTwo"));
	AirEngineParticleTwo->bIsActive = false;
	AirEngineParticleTwo->bAutoActivate = false;

	AirEngineBoostParticleOne = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("AirEngineBoostParticleOne"));
	AirEngineBoostParticleOne->bIsActive = false;
	AirEngineBoostParticleOne->bAutoActivate = false;

	AirEngineBoostParticleTwo = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("AirEngineBoostParticleTwo"));
	AirEngineBoostParticleTwo->bIsActive = false;
	AirEngineBoostParticleTwo->bAutoActivate = false;

	AirEngineOn = CreateDefaultSubobject<UAudioComponent>(TEXT("AirEngineOn"));
	AirEngineOn->bIsActive = false;
	AirEngineOn->bAutoActivate = false;

	AirEngineOff = CreateDefaultSubobject<UAudioComponent>(TEXT("AirEngineOff"));
	AirEngineOff->bIsActive = false;
	AirEngineOff->bAutoActivate = false;

	AirEngineLoopFlying = CreateDefaultSubobject<UAudioComponent>(TEXT("AirEngineLoopFlying"));
	AirEngineLoopFlying->bIsActive = false;
	AirEngineLoopFlying->bAutoActivate = false;

	AirEngineStart = CreateDefaultSubobject<UAudioComponent>(TEXT("AirEngineStart"));
	AirEngineStart->bIsActive = false;
	AirEngineStart->bAutoActivate = false;

	AirEngineLoop = CreateDefaultSubobject<UAudioComponent>(TEXT("AirEngineLoop"));
	AirEngineLoop->bIsActive = false;
	AirEngineLoop->bAutoActivate = false;

	AirEngineStop = CreateDefaultSubobject<UAudioComponent>(TEXT("AirEngineStop"));
	AirEngineStop->bIsActive = false;
	AirEngineStop->bAutoActivate = false;
}

bool ACharacterAbility_Flying::RequestStartActivate()
{
	return IsAllowActivate();
}

bool ACharacterAbility_Flying::RequestEndActivate()
{
	if (IsAllowActivate())
	{
		Super::RequestEndActivate();
		OnRep_IsActivated();

		return true;
	}

	return false;
}

void ACharacterAbility_Flying::RequestDeactivate(const bool IsDestroy)
{
	Super::RequestDeactivate(IsDestroy);
	OnRep_IsActivated();
}

void ACharacterAbility_Flying::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	AttachComponents();
}

void ACharacterAbility_Flying::AttachComponents()
{
	if (auto MyCharacter = Cast<AUTCharacter>(GetOwner()))
	{
		AirEngineParticleOne->AttachToComponent(MyCharacter->GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, SocketAirEngineOne);
		AirEngineParticleTwo->AttachToComponent(MyCharacter->GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, SocketAirEngineTwo);

		AirEngineBoostParticleOne->AttachToComponent(MyCharacter->GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, SocketAirEngineOne);
		AirEngineBoostParticleTwo->AttachToComponent(MyCharacter->GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, SocketAirEngineTwo);

		AirEngineOn->AttachToComponent(MyCharacter->GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, SocketAirEngineSound);
		AirEngineOff->AttachToComponent(MyCharacter->GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, SocketAirEngineSound);
		AirEngineStart->AttachToComponent(MyCharacter->GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, SocketAirEngineSound);
		AirEngineLoop->AttachToComponent(MyCharacter->GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, SocketAirEngineSound);
		AirEngineStop->AttachToComponent(MyCharacter->GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, SocketAirEngineSound);
		AirEngineLoopFlying->AttachToComponent(MyCharacter->GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, SocketAirEngineSound);
	}
}

void ACharacterAbility_Flying::OnRep_IsActivated() // Paste here effects later
{
	AttachComponents();

	if (auto MyCharacter = Cast<AUTCharacter>(GetOwner()))
	{
		auto MySettings = Cast<UUTGameUserSettings>(GEngine->GetGameUserSettings());
		if (bIsActivated)
		{
			// Activate
			if (MySettings && MySettings->IsAllowChangeViewByAbility() && GetNetMode() != NM_DedicatedServer)
			{
				MyCharacter->SetIsThirdView(true);
			}

			AirEngineParticleOne->Activate(true);
			AirEngineParticleTwo->Activate(true);

			AirEngineOn->Play();
			AirEngineLoop->Play();
		}
		else
		{
			// Deactivate
			if (MySettings && MySettings->IsAllowChangeViewByAbility() && GetNetMode() != NM_DedicatedServer)
			{
				MyCharacter->SetIsThirdView(false);
			}

			AirEngineParticleOne->Deactivate();
			AirEngineParticleTwo->Deactivate();

			AirEngineLoop->FadeOut(.2f, .0f);
			AirEngineLoopFlying->FadeOut(.2f, .0f);
			AirEngineOff->Play();
			
			bIsApplyUpMove = false;
		}
	}
}

void ACharacterAbility_Flying::OnAbilityInput(const EAbilityInput InInputAction, const bool IsHold)
{
	Super::OnAbilityInput(InInputAction, IsHold);

	if (Role == ROLE_Authority && IsActivated())
	{
		if (InInputAction == EAbilityInput::Jump)
		{
			bIsApplyUpMove = IsHold;
		}
	}
}

void ACharacterAbility_Flying::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ACharacterAbility_Flying, bIsApplyUpMove);
}

void ACharacterAbility_Flying::OnRep_IsApplyUpMove()
{
	if (bIsApplyUpMove)
	{
		AirEngineParticleOne->SetFloatParameter(AirEngineParticleParameter, AirEngineParticleValues.Y);

		AirEngineBoostParticleOne->Activate();
		AirEngineBoostParticleTwo->Activate();

		AirEngineStart->Play();
		AirEngineLoopFlying->Play();
	}
	else
	{
		AirEngineParticleOne->SetFloatParameter(AirEngineParticleParameter, AirEngineParticleValues.X);

		AirEngineBoostParticleOne->Deactivate();
		AirEngineBoostParticleTwo->Deactivate();

		AirEngineLoopFlying->FadeOut(.2f, .0f);
		AirEngineStop->Play();
	}
}