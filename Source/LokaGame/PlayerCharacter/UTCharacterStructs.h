#pragma once

#include "UTCharacterStructs.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FCharacterDiedSignature, class AController*, Killer, const class UDamageType*, DamageType);

/** Replicated movement data of our RootComponent.
* More efficient than engine's FRepMovement
*/
USTRUCT()
struct FRepUTMovement
{
	GENERATED_USTRUCT_BODY()

		/** @TODO FIXMESTEVE version that just replicates XY components, plus could quantize to tens easily */
		UPROPERTY()
		FVector_NetQuantize LinearVelocity;

	UPROPERTY()
		FVector_NetQuantize Location;

	/* Compressed acceleration direction: lowest 2 bits are forward/back, next 2 bits are left/right (-1, 0, 1) */
	UPROPERTY()
		uint8 AccelDir;

	UPROPERTY()
		FRotator Rotation;

	FRepUTMovement()
		: LinearVelocity(ForceInit)
		, Location(ForceInit)
		//, Acceleration(ForceInit)
		, Rotation(ForceInit)
	{}

	bool NetSerialize(FArchive& Ar, class UPackageMap* Map, bool& bOutSuccess)
	{
		bOutSuccess = true;

		bool bOutSuccessLocal = true;

		// update location, linear velocity
		Location.NetSerialize(Ar, Map, bOutSuccessLocal);
		bOutSuccess &= bOutSuccessLocal;
		Rotation.SerializeCompressed(Ar);
		LinearVelocity.NetSerialize(Ar, Map, bOutSuccessLocal);
		bOutSuccess &= bOutSuccessLocal;
		Ar.SerializeBits(&AccelDir, 8);

		return true;
	}

	bool operator==(const FRepUTMovement& Other) const
	{
		if (LinearVelocity != Other.LinearVelocity)
		{
			return false;
		}

		if (Location != Other.Location)
		{
			return false;
		}

		if (Rotation != Other.Rotation)
		{
			return false;
		}
		if (AccelDir != Other.AccelDir)
		{
			return false;
		}
		return true;
	}

	bool operator!=(const FRepUTMovement& Other) const
	{
		return !(*this == Other);
	}
};

USTRUCT(BlueprintType)
struct FTakeHitInfo
{
	GENERATED_USTRUCT_BODY()

		/** the amount of damage */
		UPROPERTY(BlueprintReadWrite, Category = TakeHitInfo)
		int32 Damage;
	/** the location of the hit (relative to Pawn center) */
	UPROPERTY(BlueprintReadWrite, Category = TakeHitInfo)
		FVector_NetQuantize RelHitLocation;
	/** how much momentum was imparted */
	UPROPERTY(BlueprintReadWrite, Category = TakeHitInfo)
		FVector_NetQuantize Momentum;
	/** the damage type we were hit with */
	UPROPERTY(BlueprintReadWrite, Category = TakeHitInfo)
		TSubclassOf<UDamageType> DamageType;
	/** shot direction pitch, manually compressed */
	UPROPERTY(BlueprintReadWrite, Category = TakeHitInfo)
		uint8 ShotDirPitch;
	/** shot direction yaw, manually compressed */
	UPROPERTY(BlueprintReadWrite, Category = TakeHitInfo)
		uint8 ShotDirYaw;
	/** number of near-simultaneous shots of same damage type
	* NOTE: on the server, on-hit functions (and effects, if applicable) will get called for every hit regardless of this value
	*		therefore, this value should generally only be considered on clients
	*/
	UPROPERTY(BlueprintReadWrite, Category = TakeHitInfo)
		uint8 Count;
	/** if damage was partially or completely absorbed by inventory, the item that did so
	* used to play different effects
	*/
	UPROPERTY(BlueprintReadWrite, Category = TakeHitInfo)
		TSubclassOf<class AUTInventory> HitArmor;
};

/** ammo counter */
//USTRUCT(BlueprintType)
//struct FStoredAmmo
//{
//	GENERATED_USTRUCT_BODY()
//
//		UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Ammo)
//		TSubclassOf<class AUTWeapon> Type;
//	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Ammo)
//		int32 Amount;
//};

USTRUCT(BlueprintType)
struct FSavedPosition
{
	GENERATED_USTRUCT_BODY()

		FSavedPosition() : Position(FVector(0.f)), Rotation(FRotator(0.f)), Velocity(FVector(0.f)), bTeleported(false), bShotSpawned(false), Time(0.f), TimeStamp(0.f) {};

	FSavedPosition(FVector InPos, FRotator InRot, FVector InVel, bool InTeleported, bool InShotSpawned, float InTime, float InTimeStamp) : Position(InPos), Rotation(InRot), Velocity(InVel), bTeleported(InTeleported), bShotSpawned(InShotSpawned), Time(InTime), TimeStamp(InTimeStamp) {};

	/** Position of player at time Time. */
	UPROPERTY()
		FVector Position;

	/** Rotation of player at time Time. */
	UPROPERTY()
		FRotator Rotation;

	/** Keep velocity also for bots to use in realistic reaction time based aiming error model. */
	UPROPERTY()
		FVector Velocity;

	/** true if teleport occurred getting to current position (so don't interpolate) */
	UPROPERTY()
		bool bTeleported;

	/** true if shot was spawned at this position */
	UPROPERTY()
		bool bShotSpawned;

	/** Current server world time when this position was updated. */
	float Time;

	/** Client timestamp associated with this position. */
	float TimeStamp;
};

UENUM(BlueprintType)
enum EAllowedSpecialMoveAnims
{
	EASM_Any,
	EASM_UpperBodyOnly,
	EASM_None,
};

UENUM(BlueprintType)
enum EMovementEvent
{
	EME_Jump,
	EME_Dodge,
	EME_WallDodge,
	EME_Slide,
};

USTRUCT(BlueprintType)
struct FMovementEventInfo
{
	GENERATED_USTRUCT_BODY()

		UPROPERTY(BlueprintReadOnly)
		TEnumAsByte<EMovementEvent> EventType;

	UPROPERTY(BlueprintReadOnly)
		uint8 EventCount;

	UPROPERTY(BlueprintReadOnly)
		FVector_NetQuantize EventLocation;
};

USTRUCT(BlueprintType)
struct FOverlayEffect
{
	GENERATED_USTRUCT_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UMaterialInterface* Material;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UParticleSystem* Particles;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FName ParticleAttachPoint;

	explicit FOverlayEffect(UMaterialInterface* InMaterial = NULL, UParticleSystem* InParticles = NULL, FName InParticleAttachPoint = NAME_None)
		: Material(InMaterial), Particles(InParticles), ParticleAttachPoint(InParticleAttachPoint)
	{}

	inline bool operator== (const FOverlayEffect& B) const
	{
		return Material == B.Material && Particles == B.Particles; // attach point irrelevant
	}
	inline bool IsValid() const
	{
		return Material != NULL || Particles != NULL;
	}
	inline FString ToString() const
	{
		return FString::Printf(TEXT("(Material=%s,Particles=%s)"), *GetFullNameSafe(Material), *GetFullNameSafe(Particles));
	}
};

USTRUCT(BlueprintType)
struct FPhysicalSoundResponse
{
	GENERATED_USTRUCT_BODY()

		/** the generic name of the sound that will be used to look up the audio */
		UPROPERTY(EditDefaultsOnly, Category = Sound)
		TEnumAsByte<EPhysicalSurface> SurfaceType;
	/** reference to the actual sound object */
	UPROPERTY(EditDefaultsOnly, Category = Sound)
		USoundBase* Sound;
	/** reference to the actual sound object */
	UPROPERTY(EditDefaultsOnly, Category = Sound)
		USoundBase* SoundOwner;
};