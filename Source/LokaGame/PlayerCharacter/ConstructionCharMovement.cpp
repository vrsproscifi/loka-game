// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "ConstructionCharacter.h"
#include "PlayerInventoryItem.h"
#include "Character/CharacterBaseEntity.h"
#include "ConstructionCharMovement.h"

UCharacterBaseEntity* UConstructionCharMovement::GetCharacterInstance() const
{
	auto MyCharacter = Cast<AConstructionCharacter>(GetCharacterOwner());
	return (MyCharacter && MyCharacter->GetInstance()) ? MyCharacter->GetInstance()->GetEntity<UCharacterBaseEntity>() : nullptr;
}

float UConstructionCharMovement::GetMaxSpeed() const
{
	float BaseMaxSpeed = Super::GetMaxSpeed();

	auto MyCharacter = Cast<AConstructionCharacter>(GetCharacterOwner());
	auto MyCharacterInstance = GetCharacterInstance();

	if (MyCharacter && MyCharacterInstance)
	{
		BaseMaxSpeed = MyCharacterInstance->Property.Speed.MaxAmount;
		auto SpeedModifers = MyCharacterInstance->GetCharacterSpeedModifers();

		if (MovementMode == MOVE_Walking || MovementMode == MOVE_NavWalking)
		{
			if (MyCharacter->IsRunning())
			{
				BaseMaxSpeed *= SpeedModifers.ModiferRunning;
			}
		}
	}

	return BaseMaxSpeed;
}

void UConstructionCharMovement::StopActiveMovement()
{
	Super::StopActiveMovement();
}

float UConstructionCharMovement::GetMaxAcceleration() const
{
	float BaseMaxAcceleration = Super::GetMaxAcceleration();

	//auto MyCharacterInstance = GetCharacterInstance();

	//if (MyCharacterInstance)
	//{
	//	return GetMaxSpeed() / 5;
	//}

	return BaseMaxAcceleration;
}