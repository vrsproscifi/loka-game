// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PlayerCharacter/BaseCharacter.h"
#include "Interfaces/IconsDrawInterface.h"
#include "ConstructionCharacter.generated.h"


class IUsableActorInterface;
class ABuildHelperComponent;

UCLASS()
class LOKAGAME_API AConstructionCharacter 
	: public ABaseCharacter
	, public IIconsDrawInterface
{
	friend class UConstructionCharMovement;

	GENERATED_BODY()

public:

	AConstructionCharacter(const class FObjectInitializer& ObjectInitializer);

	virtual void PostInitializeComponents() override;
	virtual void BeginPlay() override;
	void UpdateBuildHelper();
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	virtual void Tick(float DeltaTime) override;	

	template<bool Value>
	void OnBuildAction_Helper() { OnBuildAction(Value); }
	void OnBuildAction(const bool);

	template<bool Value>
	void OnRotateAction_Helper() { OnRotateAction(Value); }
	void OnRotateAction(const bool);

	template<bool Value>
	void OnSelectionActionList_Helper() { OnSelectionActionList(Value); }
	void OnSelectionActionList(const bool);

	template<int32 Value>
	void OnSelectionAction_Helper() { OnSelectionAction(Value); }
	void OnSelectionAction(const int32);

	template<int32 Value>
	void OnModeAction_Helper() { OnModeAction(Value); }
	void OnModeAction(const int32);

	void OnToggleInventory();
	void OnToggleMode();

	virtual void InitializeInstance(UPlayerInventoryItem* InInstance) override;
	virtual void OnRep_Instance() override;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Construction)
	FORCEINLINE ABuildHelperComponent* GetBuildHelper() const { return BuildHelper; }

	UFUNCTION(Reliable, Client)
	void ClientProxyBuyBuilding(UPlayerInventoryItem* InItem);

	UFUNCTION(Reliable, Client)
	void ClientProxySlotBuilding(const int32 InSlot, UPlayerInventoryItem* InItem);

	// IIconsDrawInterface Begin
	virtual void DrawMinimapIcon_Implementation(AHUD* InOwnerHud, UCanvas* InCanvas, FVector2D InPosition, const float InScale) override;
	virtual void DrawRadarIcon_Implementation(AHUD* InOwnerHud, UCanvas* InCanvas, FVector2D InPosition, const float InScale) override;
	virtual void DrawWorldIcon_Implementation(AHUD* InOwnerHud, UCanvas* InCanvas, FVector2D InPosition, const float InScale) override;

	virtual FVector GetWorldIconLocation() override;
	// IIconsDrawInterface End

protected:

	UPROPERTY(VisibleAnywhere)
	USpringArmComponent* CameraSpring;

	UPROPERTY(VisibleAnywhere)
	UCameraComponent* Camera;

	UFUNCTION()
	void OnRep_SelectedColor();

	virtual void OnRep_IsFarView() override;

	UPROPERTY(ReplicatedUsing = OnRep_SelectedColor)
	FLinearColor SelectedColor;

	UPROPERTY(EditDefaultsOnly, Category = System)
	TSubclassOf<ABuildHelperComponent> BuildHelperClass;

	UPROPERTY(Replicated)
	ABuildHelperComponent* BuildHelper;

	UPROPERTY()
	TArray<UMaterialInstanceDynamic*> MIDS;

	UPROPERTY(EditDefaultsOnly, Category = Appearance)
	TArray<FName> PN_SelectedColor;
	
	/*
	 * Paste values in meeters
	 * X - Near distance, Y - Far distance
	 */
	UPROPERTY(EditDefaultsOnly, Category = Configuration)
	FVector2D CameraDistances;

	UPROPERTY(EditDefaultsOnly, Category = Configuration)
	float CameraOffsetFromCenter;
};
