// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.


#include "LokaGame.h"
#include "ShooterSpectatorPawn.h"
#include "UTCharacter.h"
#include "UTPlayerController.h"

AShooterSpectatorPawn::AShooterSpectatorPawn(const FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{	
	SpringArm = ObjectInitializer.CreateDefaultSubobject<USpringArmComponent>(this, TEXT("SpringArmFollower"));
	SpringArm->SetupAttachment(GetRootComponent());
	SpringArm->TargetArmLength = 450.0f;
	SpringArm->TargetOffset = FVector(0, 0, 100.0f);
	SpringArm->ProbeSize = 35.0f;
	SpringArm->ProbeChannel = ECC_Camera;
	SpringArm->bDoCollisionTest = true;
	SpringArm->bUsePawnControlRotation = true;
	SpringArm->bEnableCameraLag = true;
	SpringArm->CameraLagSpeed = 10.0f;
}

void AShooterSpectatorPawn::SetupPlayerInputComponent(UInputComponent* InInputComponent)
{
	check(InInputComponent);
	
	InInputComponent->BindAxis("MoveForward", this, &AShooterSpectatorPawn::MoveForward);
	InInputComponent->BindAxis("MoveRight", this, &AShooterSpectatorPawn::MoveRight);
	InInputComponent->BindAxis("MoveUp", this, &AShooterSpectatorPawn::MoveUp_World);
	InInputComponent->BindAxis("Turn", this, &ADefaultPawn::AddControllerYawInput);
	InInputComponent->BindAxis("TurnRate", this, &ADefaultPawn::TurnAtRate);
	InInputComponent->BindAxis("LookUp", this, &ADefaultPawn::AddControllerPitchInput);
	InInputComponent->BindAxis("LookUpRate", this, &AShooterSpectatorPawn::LookUpAtRate);

	// spectate
	//InputComponent->BindAction("Fire", IE_Pressed, this, &AShooterSpectatorPawn::OnSpectateNextCharacter).bConsumeInput = false;
	//InputComponent->BindAction("InGameAiming", IE_Pressed, this, &AShooterSpectatorPawn::OnSpectatePrevCharacter).bConsumeInput = false;
	//InputComponent->BindAction("Jump", IE_Pressed, this, &AShooterSpectatorPawn::OnSpectateSwitchMode).bConsumeInput = false;
}

void AShooterSpectatorPawn::MoveForward(float Val)
{
	if (FMath::IsNearlyEqual(Val, .0f) == false && IsAllowControlling())
	{
		TargetView = nullptr;
		Super::MoveForward(Val);
	}
}

void AShooterSpectatorPawn::MoveRight(float Val)
{
	if (FMath::IsNearlyEqual(Val, .0f) == false && IsAllowControlling())
	{
		TargetView = nullptr;
		Super::MoveRight(Val);
	}
}

void AShooterSpectatorPawn::MoveUp_World(float Val)
{
	if (FMath::IsNearlyEqual(Val, .0f) == false && IsAllowControlling())
	{
		TargetView = nullptr;
		Super::MoveUp_World(Val);
	}
}

void AShooterSpectatorPawn::AddControllerYawInput(float Val)
{
	if (FMath::IsNearlyEqual(Val, .0f) == false && !TargetView)
	{
		Super::AddControllerYawInput(Val);
	}
}

void AShooterSpectatorPawn::AddControllerPitchInput(float Val)
{
	if (FMath::IsNearlyEqual(Val, .0f) == false && !TargetView)
	{
		Super::AddControllerPitchInput(Val);
	}
}

void AShooterSpectatorPawn::LookUpAtRate(float Val)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Val * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AShooterSpectatorPawn::InitializeTarget()
{
	if (auto MyCtrl = Cast<AUTPlayerController>(Controller))
	{
		TargetViewSetTime = GetWorld()->GetTimeSeconds() + 1.0f;
		TargetView = MyCtrl->MyLastKiller;
		LastPawn = MyCtrl->LastCharacter;

		if (LastPawn)
		{
			SpringArm->AttachToComponent(LastPawn->GetRootComponent(), FAttachmentTransformRules::SnapToTargetNotIncludingScale);
		}
	}
}

void AShooterSpectatorPawn::PossessedBy(class AController* NewController)
{
	Super::PossessedBy(NewController);
	InitializeTarget();	
}

void AShooterSpectatorPawn::UnPossessed()
{
	DisableInput(Cast<APlayerController>(GetController()));

	TargetView = nullptr;
	LastPawn = nullptr;	

	Super::UnPossessed();	
}

bool AShooterSpectatorPawn::IsAllowControlling() const
{
	return (TargetViewSetTime < GetWorld()->GetTimeSeconds());
}

void AShooterSpectatorPawn::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (TargetView && Controller)
	{			
		Controller->SetControlRotation(FMath::RInterpTo(Controller->GetControlRotation(), FRotationMatrix::MakeFromX(TargetView->GetActorLocation() - GetActorLocation()).Rotator(), DeltaSeconds, 20.0f));
		const auto loc = SpringArm->GetSocketTransform(NAME_None, RTS_World).GetLocation();
		SetActorLocation(loc);
	}
}