// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "Interfaces/UsableActorInterface.h"
#include "ConstructionCharMovement.h"
#include "Character/CharacterBaseEntity.h"
#include "ConstructionCharacter.h"
#include "BuildSystem/BuildHelperComponent.h"
#include "PlayerInventoryItem.h"
#include "GameSingleton.h"
#include "ConstructionGameState.h"
#include "ConstructionComponent.h"
#include "Build/ItemBuildEntity.h"
#include "ConstructionPlayerState.h"
#include "TutorialComponent.h"
#include "BasePlayerController.h"

AConstructionCharacter::AConstructionCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer.SetDefaultSubobjectClass<UConstructionCharMovement>(ACharacter::CharacterMovementComponentName))
{
	GetMesh()->bOwnerNoSee = true;

	CameraSpring = ObjectInitializer.CreateDefaultSubobject<USpringArmComponent>(this, TEXT("CameraSpring"));
	CameraSpring->SetupAttachment(RootComponent);

	Camera = ObjectInitializer.CreateDefaultSubobject<UCameraComponent>(this, TEXT("Camera"));
	Camera->SetupAttachment(CameraSpring);

	CameraDistances = FVector2D(0.0f, 5.0f);
	bIsFarView = true;

	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;

	OnRep_IsFarView();
}

void AConstructionCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (HasAuthority())
	{
		SelectedColor = FLinearColor::MakeRandomColor();
		OnRep_SelectedColor();
	}
}

void AConstructionCharacter::BeginPlay()
{
	Super::BeginPlay();
}

void AConstructionCharacter::UpdateBuildHelper()
{
	if (HasAuthority())
	{
		auto MyPlayerState = Cast<AConstructionPlayerState>(GetPlayerState());
		auto MyGameState = GetWorldGameState<AConstructionGameState>(GetWorld());

		if (MyGameState && MyGameState->GetConstructionComponent() && MyGameState->GetConstructionComponent()->IsPlayerOwnedWorld())
		{
			FActorSpawnParameters SpawnParameters;
			SpawnParameters.Instigator = this;
			SpawnParameters.Owner = this;
			SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			BuildHelper = GetWorld()->SpawnActor<ABuildHelperComponent>(BuildHelperClass, SpawnParameters);

			if (BuildHelper && MyGameState)
			{
				auto MyConstructionComponent = MyGameState->GetConstructionComponent();

				BuildHelper->OnBuildingPlaced.AddDynamic(MyConstructionComponent, &UConstructionComponent::RequestBuildingPlace);
				BuildHelper->OnBuildingPreRemove.AddDynamic(MyConstructionComponent, &UConstructionComponent::RequestBuildingRemove);
				BuildHelper->OnBuildItemSelected.AddDynamic(this, &AConstructionCharacter::ClientProxySlotBuilding);
				BuildHelper->OnBuildItemBuyRequest.AddDynamic(this, &AConstructionCharacter::ClientProxyBuyBuilding);

				if (MyPlayerState && MyPlayerState->GetTutorialComponent())
				{
					auto MyTutorialComponent = MyPlayerState->GetTutorialComponent();

					BuildHelper->OnBuildingPlaced.AddDynamic(MyTutorialComponent, &UTutorialComponent::OnEvent_PlaceBuilding);
					BuildHelper->OnBuildingPreRemove.AddDynamic(MyTutorialComponent, &UTutorialComponent::OnEvent_RemoveBuilding);
					BuildHelper->OnBuildingSelected.AddDynamic(MyTutorialComponent, &UTutorialComponent::OnEvent_SelectBuilding);
				}
			}

			BuildHelper->StartLoadSavedInventory();
		}
		else if (BuildHelper && BuildHelper->IsValidLowLevel())
		{
			BuildHelper->Destroy(true);
			BuildHelper = nullptr;
		}
	}
}

void AConstructionCharacter::InitializeInstance(UPlayerInventoryItem* InInstance)
{
	Super::InitializeInstance(InInstance);	

	OnRep_Instance();
}

void AConstructionCharacter::OnRep_Instance()
{
	auto BaseInstance = Instance ? Instance->GetEntity<UCharacterBaseEntity>() : nullptr;
	if (BaseInstance && BaseInstance->IsValidLowLevel())
	{
		GetMesh()->SetSkeletalMesh(BaseInstance->GetConstructionMeshData().Mesh3P);
		GetMesh()->SetAnimInstanceClass(BaseInstance->GetConstructionMeshData().MeshAnim3P);

		OnRep_SelectedColor();
	}
}

void AConstructionCharacter::DrawMinimapIcon_Implementation(AHUD* InOwnerHud, UCanvas* InCanvas, FVector2D InPosition, const float InScale)
{
	
}

void AConstructionCharacter::DrawRadarIcon_Implementation(AHUD* InOwnerHud,	UCanvas* InCanvas, FVector2D InPosition, const float InScale)
{
	
}

void AConstructionCharacter::DrawWorldIcon_Implementation(AHUD* InOwnerHud, UCanvas* InCanvas, FVector2D InPosition, const float InScale)
{
#if !UE_SERVER
	ABasePlayerController* MyLocalController = InOwnerHud ? Cast<ABasePlayerController>(InOwnerHud->PlayerOwner) : nullptr;
	AConstructionCharacter* MyLocalCharacter = MyLocalController ? Cast<AConstructionCharacter>(MyLocalController->GetControlledCharacter()) : nullptr;
	ABasePlayerState* MyPlayerState = Cast<ABasePlayerState>(GetPlayerState());
	ABuildHelperComponent* MyBuildHelper = GetBuildHelper();

	if (MyLocalCharacter && MyLocalCharacter != this && MyBuildHelper && MyPlayerState)
	{
		FColor TargetColor = SelectedColor.ToFColor(true);
		const auto MyDist = FVector::Dist(MyLocalCharacter->GetActorLocation(), GetActorLocation());
		TargetColor = TargetColor.WithAlpha(FMath::Lerp(64, 255, FMath::Clamp(100.0f * 25.0f / MyDist, .0f, 1.0f)));

		// Icon_Name
		// Icon_Awesome

		MAKE_UTF8_SYMBOL(sNone, 0xf05e);
		MAKE_UTF8_SYMBOL(sSelecting, 0xf12d);
		MAKE_UTF8_SYMBOL(sBuilding, 0xf1b2);

		FString TargetIcon = sNone;
		if (MyBuildHelper->GetCurrentMode() != EBuildHelperMode::None)
		{
			TargetIcon = (MyBuildHelper->GetCurrentMode() == EBuildHelperMode::Building) ? sBuilding : sSelecting;
		}		

		float NameWidth, Trash;
		FCanvasTextItem CanvasAwesomeItem(InPosition, FText::FromString(TargetIcon), Icon_Awesome, TargetColor);
		CanvasAwesomeItem.bCentreX = true;
		CanvasAwesomeItem.bCentreY = true;
		CanvasAwesomeItem.bOutlined = true;
		CanvasAwesomeItem.OutlineColor = FColor::Black.WithAlpha(TargetColor.A);
		CanvasAwesomeItem.Scale = FVector2D(InScale, InScale);
		InCanvas->DrawItem(CanvasAwesomeItem);

		InCanvas->TextSize(Icon_Awesome, TargetIcon, NameWidth, Trash, InScale, InScale);
		FCanvasTextItem CanvasNameItem(FVector2D(NameWidth, .0f) + InPosition, FText::FromString(MyPlayerState->GetPlayerName()), Icon_Name, TargetColor);
		CanvasNameItem.bCentreX = false;
		CanvasNameItem.bCentreY = true;
		CanvasNameItem.bOutlined = true;
		CanvasNameItem.OutlineColor = FColor::Black.WithAlpha(TargetColor.A);
		CanvasNameItem.Scale = FVector2D(InScale, InScale);
		CanvasNameItem.FontRenderInfo.bClipText = true;
		InCanvas->DrawItem(CanvasNameItem);
	}
#endif
}

FVector AConstructionCharacter::GetWorldIconLocation()
{
	return GetMesh()->GetComponentLocation() + FVector(0.f, 0.f, GetCapsuleComponent()->GetUnscaledCapsuleHalfHeight() * 2.25f);
}

void AConstructionCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction(TEXT("BuildActionPlace"), IE_Pressed, this, &AConstructionCharacter::OnBuildAction_Helper<true>);
	PlayerInputComponent->BindAction(TEXT("BuildActionCancel"), IE_Pressed, this, &AConstructionCharacter::OnBuildAction_Helper<false>);

	PlayerInputComponent->BindAction(TEXT("BuildRotateLeft"), IE_Pressed, this, &AConstructionCharacter::OnRotateAction_Helper<false>);
	PlayerInputComponent->BindAction(TEXT("BuildRotateRight"), IE_Pressed, this, &AConstructionCharacter::OnRotateAction_Helper<true>);

	PlayerInputComponent->BindAction(TEXT("BuildSelection_1"), IE_Pressed, this, &AConstructionCharacter::OnSelectionAction_Helper<1>);
	PlayerInputComponent->BindAction(TEXT("BuildSelection_2"), IE_Pressed, this, &AConstructionCharacter::OnSelectionAction_Helper<2>);
	PlayerInputComponent->BindAction(TEXT("BuildSelection_3"), IE_Pressed, this, &AConstructionCharacter::OnSelectionAction_Helper<3>);
	PlayerInputComponent->BindAction(TEXT("BuildSelection_4"), IE_Pressed, this, &AConstructionCharacter::OnSelectionAction_Helper<4>);
	PlayerInputComponent->BindAction(TEXT("BuildSelection_5"), IE_Pressed, this, &AConstructionCharacter::OnSelectionAction_Helper<5>);
	PlayerInputComponent->BindAction(TEXT("BuildSelection_6"), IE_Pressed, this, &AConstructionCharacter::OnSelectionAction_Helper<6>);
	PlayerInputComponent->BindAction(TEXT("BuildSelection_7"), IE_Pressed, this, &AConstructionCharacter::OnSelectionAction_Helper<7>);
	PlayerInputComponent->BindAction(TEXT("BuildSelection_8"), IE_Pressed, this, &AConstructionCharacter::OnSelectionAction_Helper<8>);
	PlayerInputComponent->BindAction(TEXT("BuildSelection_9"), IE_Pressed, this, &AConstructionCharacter::OnSelectionAction_Helper<9>);

	PlayerInputComponent->BindAction(TEXT("BuildSelectionLeft"), IE_Pressed, this, &AConstructionCharacter::OnSelectionActionList_Helper<false>);
	PlayerInputComponent->BindAction(TEXT("BuildSelectionRight"), IE_Pressed, this, &AConstructionCharacter::OnSelectionActionList_Helper<true>);

	PlayerInputComponent->BindAction(TEXT("BuildMode_0"), IE_Pressed, this, &AConstructionCharacter::OnModeAction_Helper<EBuildHelperMode::None>);
	PlayerInputComponent->BindAction(TEXT("BuildMode_1"), IE_Pressed, this, &AConstructionCharacter::OnModeAction_Helper<EBuildHelperMode::Building>);
	PlayerInputComponent->BindAction(TEXT("BuildMode_2"), IE_Pressed, this, &AConstructionCharacter::OnModeAction_Helper<EBuildHelperMode::Selecting>);
	
	PlayerInputComponent->BindAction(TEXT("BuildMode"), IE_Pressed, this, &AConstructionCharacter::OnToggleMode);
}

void AConstructionCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	CameraSpring->TargetArmLength = FMath::FInterpTo(CameraSpring->TargetArmLength, (bIsFarView ? CameraDistances.Y : CameraDistances.X) * 100.0f, DeltaTime, 5.0f);
	CameraSpring->SocketOffset.Y = FMath::FInterpTo(CameraSpring->SocketOffset.Y, bIsFarView ? CameraOffsetFromCenter * 100.0f : .0f, DeltaTime, 3.0f);
}

void AConstructionCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AConstructionCharacter, SelectedColor);
	DOREPLIFETIME(AConstructionCharacter, BuildHelper);
}

void AConstructionCharacter::OnRep_SelectedColor()
{
	if (Instance)
	{
		MIDS.Empty();

		for (SIZE_T i = 0; i < GetMesh()->GetNumMaterials(); ++i)
		{
			GetMesh()->SetMaterial(i, nullptr);
		}

		SIZE_T _count = 0;
		for (auto Mat : GetMesh()->GetMaterials())
		{
			MIDS.Add(GetMesh()->CreateDynamicMaterialInstance(_count, Mat));
			++_count;
		}
	}

	for (auto MID : MIDS)
	{
		if (MID && MID->IsValidLowLevel())
		{
			for (auto name : PN_SelectedColor)
			{
				MID->SetVectorParameterValue(name, SelectedColor);
			}
		}
	}
}

void AConstructionCharacter::OnRep_IsFarView()
{
	//if (BuildHelper)
	//{
	//	BuildHelper->SetAllowAnyMode(!bIsFarView);
	//}
	
	GetMesh()->SetOwnerNoSee(!bIsFarView);

	bUseControllerRotationYaw = !bIsFarView;

	GetCharacterMovement()->bOrientRotationToMovement = bIsFarView;;
}

void AConstructionCharacter::OnBuildAction(const bool IsPlace)
{
	if (BuildHelper)
	{
		BuildHelper->ProcessActionPlace(IsPlace);
	}
}

void AConstructionCharacter::OnRotateAction(const bool IsRight)
{
	if (BuildHelper)
	{
		BuildHelper->ProcessActionRotate(IsRight);
	}
}

void AConstructionCharacter::OnSelectionAction(const int32 InNumber)
{
	if (BuildHelper)
	{
		BuildHelper->ProcessActionSelecting(InNumber);
	}
}

void AConstructionCharacter::OnSelectionActionList(const bool IsRight)
{
	if (BuildHelper)
	{
		int32 LastSelectionNum = BuildHelper->GetSelectedNum();
		int32 TargetSelectionNum = LastSelectionNum;

		SIZE_T _count = 0;
		while ((BuildHelper->GetBuildItemFromSlot(TargetSelectionNum) && BuildHelper->GetBuildItemFromSlot(TargetSelectionNum)->GetEntity<UItemBuildEntity>()) == false || LastSelectionNum == TargetSelectionNum)
		{
			TargetSelectionNum = IsRight ? TargetSelectionNum + 1 : TargetSelectionNum - 1;
			if (TargetSelectionNum >= 9) TargetSelectionNum = 1;
			else if (TargetSelectionNum < 1) TargetSelectionNum = 9;

			if (_count > 10) break;
			++_count;
		}

		BuildHelper->ProcessActionSelecting(TargetSelectionNum);
	}
}

void AConstructionCharacter::OnModeAction(const int32 InNumber)
{
	if (BuildHelper)
	{
		BuildHelper->SetCurrentMode(static_cast<EBuildHelperMode::Type>(InNumber));
	}
}

void AConstructionCharacter::OnToggleInventory()
{
	if (auto MyPlayerState = GetValidObject<AConstructionPlayerState>(GetPlayerState()))
	{
		static bool IsToggledInventory;
		IsToggledInventory = !IsToggledInventory;
		MyPlayerState->ToggleConstructionInventory(IsToggledInventory);
	}
}

void AConstructionCharacter::OnToggleMode()
{
	if (BuildHelper)
	{
		const int32 CurrentMode = BuildHelper->GetCurrentMode().GetValue();
		BuildHelper->SetCurrentMode(static_cast<EBuildHelperMode::Type>((CurrentMode + 1) % EBuildHelperMode::End));
	}
}

void AConstructionCharacter::ClientProxyBuyBuilding_Implementation(UPlayerInventoryItem* InItem)
{
	auto MyPlayerState = Cast<AConstructionPlayerState>(GetPlayerState());
	if (MyPlayerState && InItem && InItem->GetEntity())
	{
		MyPlayerState->ClientShowBuyRequest(InItem->GetEntity()->GetModelInfo());
	}
}

void AConstructionCharacter::ClientProxySlotBuilding_Implementation(const int32 InSlot, UPlayerInventoryItem* InItem)
{
	auto MyPlayerState = Cast<AConstructionPlayerState>(GetPlayerState());
	if (MyPlayerState && InItem && InItem->GetEntity())
	{
		MyPlayerState->ClientPasteItemIntoSlot(InSlot, InItem);
	}
}