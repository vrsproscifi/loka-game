#pragma once

#include "Entity/Item/CategoryTypeId.h"
#include "Models/InvertoryItemWrapper.h"
#include "UnlockItemContainer.generated.h"

USTRUCT()
struct FUnlockItemRequestContainer
{
	GENERATED_USTRUCT_BODY()

	FUnlockItemRequestContainer()
	{

	}

	FUnlockItemRequestContainer(const int32& itemId, const CategoryTypeId::Type& itemType, const uint8& amount = 1, const FString& target = "")
		: ItemId(itemId)
		, ItemType(itemType)
		, Amount(amount)
	{

	}

	//	������ �������
	UPROPERTY()		int32 ItemId;

	//	���������
	UPROPERTY()		int32 ItemType;

	//	����������
	UPROPERTY()		uint8 Amount;

	//	�� ������ �������� ���������� �������
	UPROPERTY()		FLokaGuid Target;
};

USTRUCT()
struct FUnlockItemResponseContainer : public FInvertoryItemWrapper
{
	GENERATED_USTRUCT_BODY()
	//	���������� ������ ����� ����� ������
	UPROPERTY()	int32 AmountRequest;

	//	���������� �� ������� ������� �����
	UPROPERTY()	int32 AmountBought;

	//	�� ������, �������� ��� ������ �������
	UPROPERTY()	FLokaGuid TargetId;

	//	��� ������, �������� ��� ������ �������
	UPROPERTY()	FString TargetName;
};