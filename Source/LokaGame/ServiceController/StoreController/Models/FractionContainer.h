#pragma once
#include "Entity/Fraction/FractionTypeId.h"
#include "Entity/Types/TypeBonus.h"
#include "FractionContainer.generated.h"


USTRUCT()
struct FFractionContainer
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	FractionTypeId ModelId;

	UPROPERTY()
	FTypeBonus Bonus;

	//FTypeDiscount Discount;
};
