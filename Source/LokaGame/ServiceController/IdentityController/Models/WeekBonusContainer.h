#pragma once
#include "WeekBonusContainer.generated.h"

USTRUCT()
struct FWeekBonusContainer
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
		int32 Notify;

	UPROPERTY()
		int32 Stage;
};

