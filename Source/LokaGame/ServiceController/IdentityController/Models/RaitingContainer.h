#pragma once
#include "RaitingContainer.generated.h"

USTRUCT()
struct FRaitingContainer
{
	GENERATED_USTRUCT_BODY()

		UPROPERTY()
		int32 Position;

	UPROPERTY()
		int32 Experience;

	UPROPERTY()
		int32 ConductedTime;
};