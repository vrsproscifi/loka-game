#pragma once
#include <Types/TypeCash.h>
#include "WeekBonusContainer.h"
#include "Containers/LokaGuid.h"
#include "PlayeAchievementContainer.h"
#include "RaitingContainer.h"
#include "Models/PlayerRatingTopContainer.h"
#include "PlayerStatisticView.h"
#include "PlayerUsingServiceView.h"
#include "AccountContactView.h"
#include "PlayerTutorialView.h"
#include "PlayerExperience.h"
#include "PlayerAccountRole.h"
#include "Fraction/FractionReputation.h"
#include "PlayerEntityInfo.generated.h"


USTRUCT(Blueprintable)
struct FPlayerEntityInfo
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()						FAccountContactView							Email;
	UPROPERTY()						FAccountContactView							Phone;
	UPROPERTY(BlueprintReadOnly)	TEnumAsByte<PlayerAccountRole::Type>		PlayerRole;
	UPROPERTY(BlueprintReadOnly)	FLokaGuid									HoverBoardId;

	
	UPROPERTY(BlueprintReadOnly)	FLokaGuid									TutorialId;
	UPROPERTY(BlueprintReadOnly)	FLokaGuid									PlayerId;
	UPROPERTY(BlueprintReadOnly)	FString										Login;
	UPROPERTY(BlueprintReadOnly)	TArray<FTypeCash>							Cash;
	UPROPERTY()						FPlayerExperience							Experience;
	UPROPERTY()						FPlayerStatisticView						Statistic;
	UPROPERTY()						TArray<FPlayerTutorialView>					Tutorials;	
	UPROPERTY()						TArray<FPlayerUsingServiceView>				UsingService;
	UPROPERTY()						TArray<FFractionReputation>					FractionExperience;
	UPROPERTY()						TArray<FPlayeAchievementContainer>			Archievements;	
	UPROPERTY()						TArray<FPlayerPvEMatchStatisticView>		PvEPlayerRating;
	UPROPERTY(BlueprintReadOnly)	FractionTypeId								FractionTypeId;
	UPROPERTY(BlueprintReadOnly)	FLokaGuid									LeagueId;
	UPROPERTY()						FWeekBonusContainer							WeekBonus;
	UPROPERTY()						FRaitingContainer							Raiting;
	UPROPERTY(BlueprintReadOnly)	bool										IsSquadLeader;
	UPROPERTY(BlueprintReadOnly)	bool										IsAllowVoteImprovement;		
	UPROPERTY()						int64										DateOfNextAvailableProposal;	
	UPROPERTY()						int64										DefenceExpiryDate;

	int64 TakeCash(const EGameCurrency::Type& currency) const
	{
		auto c = Cash.FindByPredicate([cc = currency](const FTypeCash& cash)
		{
			return cash.Currency == cc;
		});

		if (c == nullptr) return 0;
		return c->Amount;
	}
};