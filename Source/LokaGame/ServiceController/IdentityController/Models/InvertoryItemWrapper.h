#pragma once

#include "Item/CategoryTypeId.h"
#include "../Models/ModificationItemWrapper.h"
#include "../Models/AddonItemWrapper.h"
#include "InvertoryItemWrapper.generated.h"

USTRUCT()
struct FInvertoryAddonContainer
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
		FLokaGuid EntityId;

	UPROPERTY()
		int32 ModelId;

	UPROPERTY()
		TEnumAsByte<CategoryTypeId::Type> CategoryTypeId;

};

#define AddonSlotSkin 100

USTRUCT()
struct FInvertoryInstalledAddonContainer
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
		FLokaGuid AddonId;

	UPROPERTY()
		int32 AddonSlot;
};


USTRUCT()
struct FInvertoryItemWrapper
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()		FLokaGuid EntityId;

	UPROPERTY()		int32 ModelId;

	UPROPERTY()		TEnumAsByte<CategoryTypeId::Type> CategoryTypeId;

	UPROPERTY()		int32 Count;

	UPROPERTY()		TArray<FItemModification> Modifications;

	UPROPERTY()		TArray<FInvertoryAddonContainer> AvalibleAddons;
	
	UPROPERTY()		TArray<FInvertoryInstalledAddonContainer> InstalledAddons;
};