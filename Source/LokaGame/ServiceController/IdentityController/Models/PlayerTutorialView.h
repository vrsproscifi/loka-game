#pragma once

#include "Containers/LokaGuid.h"
#include "PlayerTutorialView.generated.h"

USTRUCT()
struct FPlayerTutorialView
{
	GENERATED_USTRUCT_BODY()
	UPROPERTY()	FLokaGuid Id;
	UPROPERTY() int64 TutorialId;
	UPROPERTY() int64 StepId;
	UPROPERTY() bool TutorialComplete;
};