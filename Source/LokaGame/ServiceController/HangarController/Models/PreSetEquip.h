#pragma once
#include "Entity/Item/ItemSetSlotId.h"
#include "Entity/Item/CategoryTypeId.h"
#include "PreSetEquip.generated.h"




USTRUCT()
struct FPreSetEquipRequest
{
	GENERATED_USTRUCT_BODY()
		
		FPreSetEquipRequest()
		: CategoryId(0)
		, SetSlotId(0)
		, IsEquip(false)
	{

	}

	FPreSetEquipRequest(
		const FGuid& itemId,
		const CategoryTypeId::Type& categoryId,
		const int32& setSlotId,
		const FGuid& preSetId,
		const bool isEquip = false)
		: ItemId(itemId)
		, CategoryId(categoryId)
		, SetSlotId(setSlotId)
		, PreSetId(preSetId)
		, IsEquip(isEquip)
	{

	}


	UPROPERTY()
		FLokaGuid ItemId;

	UPROPERTY()
		TEnumAsByte<CategoryTypeId::Type> CategoryId;

	UPROPERTY()
		int32 SetSlotId;
	
	UPROPERTY()
		FLokaGuid PreSetId;

	UPROPERTY()
		bool IsEquip;
};

USTRUCT()
struct FPreSetEquipResponse
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
		FLokaGuid ItemId;

	UPROPERTY()
		TEnumAsByte<CategoryTypeId::Type> CategoryId;

	UPROPERTY()
		int32 SetSlotId;

		EItemSetSlotId::Type GetSetSlotId() const { return static_cast<EItemSetSlotId::Type>(SetSlotId); }

	UPROPERTY()
		int32 Count;

	UPROPERTY()
		FLokaGuid PreSetId;

	UPROPERTY()
		bool IsEquip;
};
