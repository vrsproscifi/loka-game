#pragma once
#include "NodeComponent/MatchMemberAward.h"
#include "LobbyMatchResultPreview.generated.h"

namespace EGameModeTypeId {	enum Type : int32; }
namespace EGameMapTypeId { enum Type : int32; }

USTRUCT()
struct FLobbyMatchResultPreview
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()		FString MatchId;
	UPROPERTY()		FString UniversalId;
	UPROPERTY()		FString UniversalName;

	UPROPERTY()		TEnumAsByte<EGameModeTypeId::Type> GameModeTypeId;
	UPROPERTY()		TEnumAsByte<EGameMapTypeId::Type> GameMapTypeId;
	UPROPERTY()		FMatchMemberAward Award;
	UPROPERTY()		bool IsWin;
};