#pragma once
#include "WorldBuildingRepairView.generated.h"

USTRUCT()
struct FWorldBuildingRepairView
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()	FString ItemId;
	UPROPERTY()	uint64 Health;
};

USTRUCT()
struct FWorldBuildingRepairContainer
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()	TArray<FWorldBuildingRepairView> Items;
};
