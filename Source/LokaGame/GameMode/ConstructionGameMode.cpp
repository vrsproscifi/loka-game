#include "LokaGame.h"
#include "ConstructionGameMode.h"
#include "ConstructionPlayerController.h"
#include "ConstructionGameHUD.h"
#include "ConstructionPlayerState.h"
#include "ConstructionGameState.h"
#include "ConstructionCharacter.h"
#include "SquadComponent.h"
#include "GameSingleton.h"

#include "BuildSystem/ShopBuildPlacedComponent.h"
#include "NodeComponent/NodeSessionMatch.h"
#include "IdentityComponent.h"
#include "RichTextHelpers.h"
#include "ConstructionComponent.h"

AConstructionGameMode::AConstructionGameMode()
	: Super()
	, TimeBeforeEndMatch(90)
{
	HUDClass = AConstructionGameHUD::StaticClass();
	PlayerStateClass = AConstructionPlayerState::StaticClass();
	GameStateClass = AConstructionGameState::StaticClass();
	PlayerControllerClass = AConstructionPlayerController::StaticClass();
	DefaultPawnClass = AConstructionCharacter::StaticClass();

	TimeCounter = 0;
	CounterBeforeEndMatch = 0;
}

void AConstructionGameMode::DefaultTimer()
{
	Super::DefaultTimer();

	if (GetMatchState() == MatchState::InProgress)
	{
		++TimeCounter;

		if (GetNetMode() == NM_DedicatedServer)
		{
			if (NumPlayers < 2)
			{
				++CounterBeforeEndMatch;
				if (CounterBeforeEndMatch > TimeBeforeEndMatch)
				{
#if !WITH_EDITOR
					EndMatch();
#endif
				}
				else if (CounterBeforeEndMatch % 10 == 0)
				{
					FBaseLocalMessageData MessageData;					
					MessageData.AddOptionalData("TimeIn", FResourceTextBoxValue(EResourceTextBoxType::Time, TimeBeforeEndMatch - CounterBeforeEndMatch));
					BroadcastLocalizedMessage(ServerShutdownIn, MessageData);
				}
			}
			else
			{
				CounterBeforeEndMatch = 0;
			}

			auto MyGameState = GetGameState<AConstructionGameState>();
			if (MyGameState && MyGameState->OwnerState && MyGameState->OwnerState->GetSquadComponent())
			{
				auto Members = MyGameState->OwnerState->GetSquadComponent()->GetSqaudInformation().Members;

				for (auto pState : MyGameState->PlayerArray)
				{
					if (auto cState = Cast<AConstructionPlayerState>(pState))
					{
						bool bIsFound = false;
						for (auto Member : Members)
						{
							if (Member.PlayerId == cState->GetPlayerInfo().PlayerId)
							{
								if (KickCounter.Contains(cState))
								{
									KickCounter.Remove(cState);
								}

								bIsFound = true;
								break;
							}
						}

						if (bIsFound == false)
						{
							auto& Value = KickCounter.FindOrAdd(cState);
							++Value;
							auto MyCtrl = cState->GetPlayerController<ABasePlayerController>();
							if (Value > 10 && MyCtrl)
							{
								MyCtrl->ServerRequestLeave(true);
							}
							else if(Value == 5 && MyCtrl)
							{
								MyCtrl->ReceiveLocalizaedMessage(NotSquadMessage, FBaseLocalMessageData());
							}
						}
					}
				}
			}
		}
	}
}

void AConstructionGameMode::OnInitializeMatchInformation()
{
	Super::OnInitializeMatchInformation();
	StartMatch();
	//if (auto MyGameState = GetValidGameState<AConstructionGameState>())
	//{
	//	FGuid::Parse(GetMatchInformation().Options.UniversalId, MyGameState->OwnerToken);
	//}
}

void AConstructionGameMode::StartPlay()
{
	Super::StartPlay();
}

void AConstructionGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);

	auto MyGameState = GetValidGameState<AConstructionGameState>();
	auto TargetState = Cast<AConstructionPlayerState>(NewPlayer->GetPlayerState<AConstructionPlayerState>());

#if WITH_EDITOR
	if (MyGameState && TargetState)
	{
		MyGameState->OwnerState = TargetState;
		FFlagsHelper::SetFlags(MyGameState->OwnerState->AllowFlags, EAllowConstructionMode::Building | EAllowConstructionMode::Selecting);
		TargetState->GetIdentityComponent()->SendRequestExist();
		MyGameState->GetConstructionComponent()->UpdateBuildingsOwner(MyGameState->OwnerState);
	}
#else
	if (auto LocalPlayer = Cast<UNetConnection>(NewPlayer->Player))
	{		
		const auto TargetMember = FindMatchMember(LocalPlayer->PlayerId->ToString());		

		if (MyGameState && TargetState && TargetMember)
		{
			if (TargetMember->PlayerId == GetMatchInformation().Options.UniversalId)
			{
				MyGameState->OwnerState = TargetState;
				FFlagsHelper::SetFlags(MyGameState->OwnerState->AllowFlags, EAllowConstructionMode::Building | EAllowConstructionMode::Selecting);
				MyGameState->OwnerPlayerId = TargetMember->PlayerId;
				MyGameState->OwnerToken = GetMatchInformation().Options.UniversalId;
				MyGameState->GetConstructionComponent()->UpdateBuildingsOwner(MyGameState->OwnerState);
			}
		}		
	}
	else if (GetNetMode() != NM_DedicatedServer)
	{
		if (MyGameState && TargetState)
		{
			MyGameState->OwnerState = TargetState;
			FFlagsHelper::SetFlags(MyGameState->OwnerState->AllowFlags, EAllowConstructionMode::Building | EAllowConstructionMode::Selecting);
			MyGameState->GetConstructionComponent()->UpdateBuildingsOwner(MyGameState->OwnerState);
		}
	}	
#endif
}

void AConstructionGameMode::Logout(AController* Exiting)
{
	if (Exiting)
	{
		auto MyGameState = GetValidGameState<AConstructionGameState>();
		auto LogoutState = (Exiting->GetPlayerState<AConstructionPlayerState>());
		if (MyGameState && LogoutState && MyGameState->OwnerState == LogoutState)
		{
			MyGameState->GetConstructionComponent()->UpdateBuildingsOwner(MyGameState);
		}
	}

	Super::Logout(Exiting);
}

AActor* AConstructionGameMode::ChoosePlayerStart_Implementation(AController* Player)
{
	TArray<AActor*> Actors;
	UGameplayStatics::GetAllActorsWithTag(GetWorld(), TEXT("BuildingStart"), Actors);
	if (Actors.Num())
	{
		return (Actors.Num() >= 2) ? Actors[FMath::RandRange(0, Actors.Num() - 1)] : Actors[0];
	}

	return Super::ChoosePlayerStart_Implementation(Player);
}

void AConstructionGameMode::EndMatch()
{
	Super::EndMatch();

	for (FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
	{
		if(auto TargetCtrl = Cast<ABasePlayerController>(Iterator->Get()))
		{
			TargetCtrl->ServerRequestLeave(true);
		}
	}
}
