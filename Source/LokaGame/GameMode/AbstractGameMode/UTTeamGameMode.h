// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once

#include "UTGameMode.h"
#include "UTTeamGameMode.generated.h"

UCLASS(Abstract)
class LOKAGAME_API AUTTeamGameMode : public AUTGameMode
{
  GENERATED_BODY()

public:
  AUTTeamGameMode();

	/** whether the URL can override the number of teams */
	UPROPERTY(EditDefaultsOnly, Category = TeamGame)
	bool bAllowURLTeamCountOverride;

	/** percentage of damage applied for friendly fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = TeamGame)
	float TeamDamagePct;

	/** percentage of momentum applied for friendly fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = TeamGame)
	float TeamMomentumPct;

	/** Addition scaling applied for friendly fire momentum on wall running character. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = TeamGame)
		float WallRunMomentumPct;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = TeamGame)
	bool bHighScorerPerTeamBasis;

	virtual APlayerController* Login(class UPlayer* NewPlayer, ENetRole RemoteRole, const FString& Portal, const FString& Options, const FUniqueNetIdRepl& UniqueId, FString& ErrorMessage) override;
	virtual bool ModifyDamage_Implementation(int32& Damage, FVector& Momentum, APawn* Injured, AController* InstigatedBy, const FHitResult& HitInfo, AActor* DamageCauser, TSubclassOf<UDamageType> DamageType) override;
	virtual float RatePlayerStart(AUTPlayerStart* P, AController* Player) override;
	virtual bool CheckScore_Implementation(AUTPlayerState* Scorer) override;
	virtual void PlayEndOfMatchMessage() override;
	virtual void FindAndMarkHighScorer() override;
	virtual bool AvoidPlayerStart(class AUTPlayerStart* InPlayerStart) override;

	virtual bool CanSpectate_Implementation(APlayerController* Viewer, APlayerState* ViewTarget) override;

	virtual void CheckBotCount() override;
	virtual void DefaultTimer() override;

	/** whether we should force teams to be balanced right now
	 * @param bInitialTeam - if true, request comes from a player requesting its initial team (not a team switch)
	 */
	virtual bool ShouldBalanceTeams(bool bInitialTeam) const;
	/** Process team change request.  May fail based on team sizes and balancing rules. */
	virtual bool ChangeTeam(AController* Player, uint8 NewTeam = 255, bool bBroadcast = true);

	/** Put player on new team if it is valid, return true if successful. */
	virtual bool MovePlayerToTeam(AController* Player, AUTPlayerState* PS, uint8 NewTeam) override;

	virtual uint8 GetTeamIndexById(const FGuid& teamId) override;

	/** pick the best team to place this player to keep the teams as balanced as possible
	 * passed in team number is used as tiebreaker if the teams would be just as balanced either way
	 */
	virtual uint8 PickBalancedTeam(AUTPlayerState* PS, uint8 RequestedTeam);

	/**  Find the best player on a given team */
	virtual AUTPlayerState* FindBestPlayerOnTeam(int32 TeamNumToTest);
	
	/** Only broadcast "dominating" message once. */
	UPROPERTY()
		bool bHasBroadcastDominating;

	/** Broadcast a message when team scores */
	virtual void BroadcastScoreUpdate(APlayerState* ScoringPlayer, AUTTeamInfo* ScoringTeam, int32 OldScore = 0);

	virtual void SendComsMessage( AUTPlayerController* Sender, AUTPlayerState* Target, int32 Switch) override;

protected:
	virtual UUTBotCharacter* ChooseRandomCharacter(uint8 TeamNum) override;

public:

	virtual void Killed(class AController* Killer, class AController* KilledPlayer, class APawn* KilledPawn, TSubclassOf<UDamageType> DamageType) override;
	virtual void DetermineMatchWinner() override;
};
