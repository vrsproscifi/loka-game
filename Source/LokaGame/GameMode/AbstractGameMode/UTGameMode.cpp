#include "LokaGame.h"
#include "GameFramework/GameMode.h"

#include "UTGameMessage.h"
#include "UTVictoryMessage.h"
#include "UTFirstBloodMessage.h"
#include "UTSpectatorPickupMessage.h"
#include "ShooterGameInstance.h"


#include "UTBot.h"
#include "UTSquadAI.h"

#include "UTBotCharacter.h"



#include "Engine/DemoNetDriver.h"
#include "UTEngineMessage.h"

#include "DataChannel.h"
#include "UTGameInstance.h"
#include "UTDemoRecSpectator.h"
#include "UTGameObjective.h"



#include "UTPlayerStart.h"


#include "UTInGameIntroZone.h"
#include "UTInGameIntroHelper.h"
#include "MatchStateNames.h"
#include "UTGameMode.h"

#include "UTGameState.h"

#include "UTGameSession.h"
#include "UTPlayerState.h"
#include "UTPlayerController.h"
#include "ShooterSpectatorPawn.h"
#include "UTHUD.h"


#include "NodeComponent/NodeSessionMatch.h"
#include "NodeComponent/MatchHeartBeatView.h"
#include "NodeComponent/MatchEndResult.h"
#include "IdentityComponent.h"
#include "NodeComponent.h"
#include "Entity/Service/ItemServiceModelId.h"



DEFINE_LOG_CATEGORY(LogUTGame);

UUTResetInterface::UUTResetInterface(const FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{
	
}


AUTGameMode::AUTGameMode()
	: Super()
{
	PlayerPawnObject = FStringAssetReference(TEXT("/Game/RestrictedAssets/Blueprints/DefaultCharacter.DefaultCharacter_C"));

	// use our custom HUD class
	HUDClass = AUTHUD::StaticClass();
	ReplaySpectatorPlayerControllerClass = AUTDemoRecSpectator::StaticClass();
	GameStateClass = AUTGameState::StaticClass();
	PlayerStateClass = AUTPlayerState::StaticClass();
	EngineMessageClass = UUTEngineMessage::StaticClass();

	PlayerControllerClass = AUTPlayerController::StaticClass();
	BotClass = AUTBot::StaticClass();
	SpectatorClass = AShooterSpectatorPawn::StaticClass();

	bUseSeamlessTravel = false;
	CountDown = 3;
	bPauseable = false;
	bWeaponStayActive = true;
	VictoryMessageClass = UUTVictoryMessage::StaticClass();
	GameMessageClass = UUTGameMessage::StaticClass();
	SquadType = AUTSquadAI::StaticClass();
	MaxSquadSize = 3;
	bDelayedStart = true;
	StartPlayTime = 10000000.f;
	bRemovePawnsAtStart = true;

	DefaultPlayerName = FText::FromString(TEXT("Player"));
	DemoFilename = TEXT("%m-%td");

	LastGlobalTauntTime = -1000.f;

	bSpeedHackDetection = false;
	MaxTimeMargin = 0.25f;
	MinTimeMargin = -0.25f;
	TimeMarginSlack = 0.1f;

	AntiCheatEngine = nullptr;
	EndOfMatchMessageDelay = 1.f;
	bAllowAllArmorPickups = true;

	KillScore = 10;
	DeathScore = -5;
	MinRespawnDelay = 8.0f;
	MatchWinner = INDEX_NONE;
	bDelayedStart = true;
	MinSpawnAttempts = 7;
	MaxSpawnAttempts = 10;
	Time_WaitingPlayers = 120;
	Time_WaitingStartMatch = 15;

	ServerShutdownTimerDelegate.BindLambda([&]() 
	{
		GEngine->Exec(GetWorld(), TEXT("EXIT"));
	});
#if WITH_EDITOR
	DeveloperTeamNum = 0;
#endif
}


float AUTGameMode::OverrideRespawnTime(TSubclassOf<AUTInventory> InventoryType)
{
	return InventoryType ? InventoryType.GetDefaultObject()->RespawnTime : 0.f;
}

void AUTGameMode::NotifySpeedHack(ACharacter* Character)
{
	AUTPlayerController* PC = Character ? Cast < AUTPlayerController>(Character->GetController()) : NULL;
	if (PC)
	{
		PC->ClientReceiveLocalizedMessage(GameMessageClass, 15);
	}
}

bool AUTGameMode::AllowCheats(APlayerController* P)
{
	return (GetNetMode() == NM_Standalone || GIsEditor);
}

// Parse options for this game...
void AUTGameMode::InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage)
{
	if (!PlayerPawnObject.IsNull())
	{
		DefaultPawnClass = Cast<UClass>(StaticLoadObject(UClass::StaticClass(), NULL, *PlayerPawnObject.ToSoftObjectPath().ToString(), NULL, LOAD_NoWarn));
	}

	Super::InitGame(MapName, Options, ErrorMessage);

	//  60 минут
	InactivePlayerStateLifeSpan = 60 * 60;


	auto InOpt = UGameplayStatics::ParseOption(Options, TEXT("Demorec"));
	if (InOpt.Len() > 0)
	{
		bRecordDemo = InOpt != TEXT("off") && InOpt != TEXT("false") && InOpt != TEXT("0") && InOpt != TEXT("no") && InOpt != GFalse.ToString() && InOpt != GNo.ToString();
		if (bRecordDemo)
		{
			DemoFilename = InOpt;
		}
	}

	PostInitGame(Options);

	static const FName AntiCheatFeatureName("AntiCheat");
	if (IModularFeatures::Get().IsModularFeatureAvailable(AntiCheatFeatureName))
	{
		AntiCheatEngine = &IModularFeatures::Get().GetModularFeature<UTAntiCheatModularFeature>(AntiCheatFeatureName);
	}
}

void AUTGameMode::InitGameState()
{
	Super::InitGameState();

	UTGameState = Cast<AUTGameState>(GameState);
	if (UTGameState != nullptr)
	{
		UTGameState->SetGoalScore(100);
		UTGameState->SetTimeLimit(Time_WaitingPlayers);
		UTGameState->bTeamGame = bTeamGame;
		UTGameState->bWeaponStay = bWeaponStayActive;
		UTGameState->bAllowTeamSwitches = false;		
	}
	else
	{
		UE_LOG(UT,Fatal, TEXT("UTGameState is NULL %s"), *GameStateClass->GetFullName());
	}

}

void AUTGameMode::PreInitializeComponents()
{
	Super::PreInitializeComponents();
	
	// Because of the behavior changes to PostBeginPlay() this really has to go here as PreInitializeCompoennts is sort of the UE4 PBP even
	// though PBP still exists.  It can't go in InitGame() or InitGameState() because team info needed for team locked GameObjectives are not
	// setup at that point.

	for (TActorIterator<AUTGameObjective> ObjIt(GetWorld()); ObjIt; ++ObjIt)
	{
		GameObjectiveInitialized(*ObjIt);
		ObjIt->InitializeObjective();
	}
}

void AUTGameMode::GameObjectiveInitialized(AUTGameObjective* Obj)
{
}

APlayerController* AUTGameMode::Login(UPlayer* NewPlayer, ENetRole InRemoteRole, const FString& Portal, const FString& Options, const FUniqueNetIdRepl& UniqueId, FString& ErrorMessage)
{
	APlayerController* Result = Super::Login(NewPlayer, InRemoteRole, Portal, Options, UniqueId, ErrorMessage);

	if (AntiCheatEngine)
	{
		AntiCheatEngine->OnPlayerLogin(Result, Options, UniqueId.GetUniqueNetId());
	}

	return Result;
}


UUTBotCharacter* AUTGameMode::ChooseRandomCharacter(uint8 TeamNum)
{
	UUTBotCharacter* ChosenCharacter = NULL;
	if (EligibleBots.Num() > 0)
	{
		int32 BestMatch = 0;
		for (int32 i = 0; i < EligibleBots.Num(); i++)
		{
			if (EligibleBots[i]->Skill >= GetMatchOptions().GetDifficulty())
			{
				BestMatch = i;
				break;
			}
		}
		int32 Index = FMath::Clamp(BestMatch + FMath::RandHelper(5) - 2, 0, EligibleBots.Num() - 1);
		ChosenCharacter = EligibleBots[Index];
		EligibleBots.RemoveAt(Index);
	}
	return ChosenCharacter;
}

AUTBot* AUTGameMode::AddBot(uint8 TeamNum)
{
	AUTBot* NewBot = GetWorld()->SpawnActor<AUTBot>(BotClass);
	if (NewBot != nullptr)
	{
		if (BotAssets.Num() == 0)
		{
			GetAllAssetData(UUTBotCharacter::StaticClass(), BotAssets);
		}
		if (EligibleBots.Num() == 0)
		{
			for (const FAssetData& Asset : BotAssets)
			{
				UUTBotCharacter* EligibleCharacter = Cast<UUTBotCharacter>(Asset.GetAsset());
				if (EligibleCharacter != nullptr)
				{
					EligibleBots.Add(EligibleCharacter);
				}
			}

			EligibleBots.Sort([](const UUTBotCharacter& A, const UUTBotCharacter& B) -> bool
			{
				return A.Skill < B.Skill;
			});
		}
		UUTBotCharacter* SelectedCharacter = NULL;
		int32 TotalStars = 0;

		if (SelectedCharacter == nullptr)
		{
			SelectedCharacter = ChooseRandomCharacter(TeamNum);
		}

		if (SelectedCharacter != nullptr)
		{
			NewBot->InitializeCharacter(SelectedCharacter);
			SetUniqueBotName(NewBot, SelectedCharacter);
		}
		NewBot->InitializeSkill(GetMatchOptions().GetDifficulty());
	
		NumBots++;

		ChangeTeam(NewBot, TeamNum);
		
		if (AUTPlayerState* UTPlayerState = Cast<AUTPlayerState>(NewBot->GetPlayerState<AUTPlayerState>()))
		{
			UTPlayerState->IsABot = true;
		}

	}
	return NewBot;
}

void AUTGameMode::SetUniqueBotName(AUTBot* B, const UUTBotCharacter* BotData)
{
	TArray<FString> PossibleNames(BotData->AltNames);
	PossibleNames.Add(BotData->GetName());

	for (int32 i = 1; true; i++)
	{
		for (const FString& TestName : PossibleNames)
		{
			FString FinalName = (i == 1) ? TestName : FString::Printf(TEXT("%s-%i"), *TestName, i);
			bool bTaken = false;
			for (FConstControllerIterator It = GetWorld()->GetControllerIterator(); It; ++It)
			{
				if (It->IsValid() && It->Get()->GetPlayerState<AUTPlayerState>() != nullptr && It->Get()->GetPlayerState<AUTPlayerState>()->GetPlayerName() == FinalName)
				{
					bTaken = true;
					break;
				}
			}
			if (!bTaken)
			{
				B->GetPlayerState<AUTPlayerState>()->SetPlayerName(FinalName);
				return;
			}
		}
	}
}

bool AUTGameMode::AllowRemovingBot(AUTBot* B)
{
	AUTPlayerState* PS = Cast<AUTPlayerState>(B->GetPlayerState<AUTPlayerState>());
	// flag carriers should stay in the game until they lose it
	if (PS != nullptr && PS->CarriedObject != nullptr)
	{
		return false;
	}
	else
	{
		// score leader should stay in the game unless it's the last bot
		if (NumBots > 1 && PS != nullptr)
		{
			bool bHighScore = true;
			for (APlayerState* OtherPS : GameState->PlayerArray)
			{
				if (OtherPS != PS && OtherPS->Score >= PS->Score)
				{
					bHighScore = false;
					break;
				}
			}
			if (bHighScore)
			{
				return false;
			}
		}

		// remove as soon as dead or out of combat
		// TODO: if this isn't getting them out fast enough we can restrict to only human players
		return B->GetPawn() == nullptr || B->GetEnemy() == nullptr || B->LostContact(5.0f);
	}
}

void AUTGameMode::CheckBotCount()
{
	if (NumPlayers + NumBots > GetMatchOptions().NumberOfBots)
	{
		for (FConstControllerIterator It = GetWorld()->GetControllerIterator(); It; ++It)
		{
			AUTBot* B = Cast<AUTBot>(It->Get());
			if (B != nullptr && AllowRemovingBot(B))
			{
				B->Destroy();
				break;
			}
		}
	}
	else while (NumPlayers + NumBots < GetMatchOptions().NumberOfBots)
	{
		AddBot();
	}
}


/**
 *	DefaultTimer is called once per second and is useful for consistent timed events that don't require to be 
 *  done every frame.
 **/
void AUTGameMode::DefaultTimer()
{
	//if (GetWorld()->IsPlayInEditor())
	//{
	//	// start match if necessary.
	//	if (GetMatchState() == MatchState::WaitingToStart)
	//	{
	//		StartMatch();
	//	}
	//	//return;
	//}

	//#if UE_SERVER && !WITH_EDITOR
	if (UTGameState && UTGameState->RemainingTime > 0)
	{
		UTGameState->RemainingTime--;

		UE_LOG(LogShooter, Warning, TEXT("[GameMode]:DefaultTimer >> RemainingTime: %d"), UTGameState->RemainingTime);

		if (GetMatchState() == MatchState::WaitingToStart)
		{
			if (FMath::IsNearlyEqual(float(UTGameState->RemainingTime), float(Time_WaitingStartMatch), 1.0f))
			{
				for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
				{
					if (auto TargetPC = Cast<AUTPlayerController>(*It))
					{
						TargetPC->ClientShowTextTimed(UTGameState->RemainingTime, ETimedTextMessage::WaitingStart);
					}
				}
			}
		}



		if (UTGameState->RemainingTime <= 0)
		{
			if (GetMatchState() == MatchState::WaitingPostMatch)
			{
				EndGame();
			}
			else if (GetMatchState() == MatchState::InProgress)
			{
				EndMatch();
			}
			else if (GetMatchState() == MatchState::WaitingToStart)
			{
				StartMatch();
			}
		}
		//=========================================================
	}

	CheckBotCount();
}


void AUTGameMode::RestartGame()
{
	for (FConstControllerIterator It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		AUTPlayerController* PlayerController = Cast<AUTPlayerController>(*It);
		if (PlayerController != nullptr)
		{
			PlayerController->TravelToLobbyRoom();
		}
	}

	Super::RestartGame();
}

bool AUTGameMode::IsEnemy(AController * First, AController* Second)
{
	return First && Second && !UTGameState->OnSameTeam(First, Second);
}

void AUTGameMode::Killed(AController* Killer, AController* KilledPlayer, APawn* KilledPawn, TSubclassOf<UDamageType> DamageType)
{
	if (GetMatchState() == MatchState::WaitingToStart)
	{
		return;
	}

	AUTPlayerState* KillerPlayerState = Killer ? Cast<AUTPlayerState>(Killer->GetPlayerState<AUTPlayerState>()) : NULL;
	AUTPlayerState* VictimPlayerState = KilledPlayer ? Cast<AUTPlayerState>(KilledPlayer->GetPlayerState<AUTPlayerState>()) : NULL;

	if (Killer && KilledPlayer && VictimPlayerState && KillerPlayerState)
	{
		KillerPlayerState->TrySendTauntMessage();

		for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
		{
			AUTPlayerController* PCPlayer = Cast<AUTPlayerController>(*It);
			if (PCPlayer)
			{
				PCPlayer->ClientAppendKillRow(KillerPlayerState, VictimPlayerState, VictimPlayerState->GetDamageDataLast().Weapon.Category, VictimPlayerState->GetDamageDataLast().Weapon.Model, (KilledPlayer->GetUniqueID() == PCPlayer->GetUniqueID()));
			}
		}
	}
	else if (KilledPlayer)
	{
		for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
		{
			AUTPlayerController* PCPlayer = Cast<AUTPlayerController>(*It);
			if (PCPlayer)
			{
				PCPlayer->ClientAppendKillRow(VictimPlayerState, VictimPlayerState);
			}
		}
	}

	if (KillerPlayerState && VictimPlayerState && KillerPlayerState != VictimPlayerState)
	{
		int32 GiveScore = KillScore;

		if (VictimPlayerState)
			GiveScore = FMath::CeilToInt(float(KillScore) * VictimPlayerState->GetCoefficient());

		const auto MyPawn = Cast<AUTCharacter>(KilledPawn);

		const auto AssistScore = FMath::CeilToInt(float(GiveScore) / 3.0f);
		const auto MyKillers = VictimPlayerState->GetDamageData();

		for (auto It : UTGameState->PlayerArray)
		{
			if (auto AssistState = Cast<AUTPlayerState>(It))
			{
				if (MyKillers.Contains(AssistState->GetUniqueID()))
				{
					if (MyPawn)
					{
						const auto DamageData = MyKillers.FindChecked(AssistState->GetUniqueID());
						const auto maxHealth = MyPawn->Armour.MaxAmount + MyPawn->Health.MaxAmount;
						const auto percent = DamageData.Damage / maxHealth;
						const auto ScaledScoreSource = float(GiveScore) * percent;
						const int32 ScaledScore = UTGameState->OnSameTeam(VictimPlayerState, AssistState) ? FMath::CeilToInt(float(ScaledScoreSource) * -1.0f) : FMath::CeilToInt(ScaledScoreSource);

						if (AssistState->GetUniqueID() == KillerPlayerState->GetUniqueID())
						{
							AssistState->ScoreKill(VictimPlayerState, ScaledScore);

							if (auto AssistPC = Cast<AUTPlayerController>(AssistState->GetOwner()))
								AssistPC->ClientShowActionMessage(EActionMessage::Kill, ScaledScore);
						}
						else
						{
							AssistState->ScorePoints(ScaledScore);

							if (auto AssistPC = Cast<AUTPlayerController>(AssistState->GetOwner()))
								AssistPC->ClientShowActionMessage(EActionMessage::Assist, ScaledScore);
						}
					}
					else
					{
						if (AssistState->GetUniqueID() == KillerPlayerState->GetUniqueID())
						{
							AssistState->ScoreKill(VictimPlayerState, GiveScore);

							if (auto AssistPC = Cast<AUTPlayerController>(AssistState->GetOwner()))
								AssistPC->ClientShowActionMessage(EActionMessage::Kill, GiveScore);
						}
						else
						{
							AssistState->ScorePoints(AssistScore);

							if (auto AssistPC = Cast<AUTPlayerController>(AssistState->GetOwner()))
								AssistPC->ClientShowActionMessage(EActionMessage::Assist, AssistScore);
						}
					}
				}
			}
		}

		switch (KillerPlayerState->GetKillsOneLife())
		{
			case 5: KillerPlayerState->AddAchievement(ItemAchievementTypeId::Medal_Match_SeriesKill_5); KillerPlayerState->ScorePoints(20); break;
			case 10: KillerPlayerState->AddAchievement(ItemAchievementTypeId::Medal_Match_SeriesKill_10); KillerPlayerState->ScorePoints(40); break;
			case 15: KillerPlayerState->AddAchievement(ItemAchievementTypeId::Medal_Match_SeriesKill_15); KillerPlayerState->ScorePoints(60); break;
			case 20: KillerPlayerState->AddAchievement(ItemAchievementTypeId::Medal_Match_SeriesKill_20); KillerPlayerState->ScorePoints(80); break;
			case 25: KillerPlayerState->AddAchievement(ItemAchievementTypeId::Medal_Match_SeriesKill_25); KillerPlayerState->ScorePoints(100); break;
			case 30: KillerPlayerState->AddAchievement(ItemAchievementTypeId::Medal_Match_SeriesKill_30); KillerPlayerState->ScorePoints(120); break;
			default:
				break;
		}
	}

	if (VictimPlayerState)
	{
		if (auto MyPC = Cast<AUTPlayerController>(KilledPlayer))
		{
			MyPC->ClientShowActionMessage(EActionMessage::Death, DeathScore);
		}

		VictimPlayerState->ScoreDeath(KillerPlayerState, DeathScore);
	}

	if (VictimPlayerState && KillerPlayerState)
	{
		if (!bFirstBloodOccurred)
		{
			KillerPlayerState->AddAchievement(ItemAchievementTypeId::Medal_Match_FirstBlood);
			KillerPlayerState->ScorePoints(100);
			bFirstBloodOccurred = true;
		}
	}



	//// Ignore all killing when entering overtime as we kill off players and don't want it affecting their score.
	//if ((GetMatchState() != MatchState::MatchEnteringOvertime) && (GetMatchState() != MatchState::WaitingPostMatch) && (GetMatchState() != MatchState::MapVoteHappening))
	//{
	//	AUTPlayerState* const KillerPlayerState = Killer ? Cast<AUTPlayerState>(Killer->GetPlayerState<AUTPlayerState>()) : NULL;
	//	AUTPlayerState* const KilledPlayerState = KilledPlayer ? Cast<AUTPlayerState>(KilledPlayer->GetPlayerState<AUTPlayerState>()) : NULL;

	//	//UE_LOG(UT, Log, TEXT("Player Killed: %s killed %s"), (KillerPlayerState != nullptr ? *KillerPlayerState->GetPlayerName() : TEXT("NULL")), (KilledPlayerState != nullptr ? *KilledPlayerState->GetPlayerName() : TEXT("NULL")));

	//	if (KilledPlayerState != nullptr)
	//	{
	//		bool const bEnemyKill = IsEnemy(Killer, KilledPlayer);
	//		KilledPlayerState->LastKillerPlayerState = KillerPlayerState;

	//		if (HasMatchStarted())
	//		{
	//			KilledPlayerState->IncrementDeaths(DamageType, KillerPlayerState);
	//			TSubclassOf<UUTDamageType> UTDamage(*DamageType);
	//			if (UTDamage && bEnemyKill)
	//			{
	//				UTDamage.GetDefaultObject()->ScoreKill(KillerPlayerState, KilledPlayerState, KilledPawn);

	//				if (EnemyKillsByDamageType.Contains(UTDamage))
	//				{
	//					EnemyKillsByDamageType[UTDamage] = EnemyKillsByDamageType[UTDamage] + 1;
	//				}
	//				else
	//				{
	//					EnemyKillsByDamageType.Add(UTDamage, 1);
	//				}
	//			}

	//			if (!bEnemyKill && (Killer != KilledPlayer) && (Killer != nullptr))
	//			{
	//				ScoreTeamKill(Killer, KilledPlayer, KilledPawn, DamageType);
	//			}
	//			else
	//			{
	//				ScoreKill(Killer, KilledPlayer, KilledPawn, DamageType);
	//			}
	//		}
	//		else if (KilledPlayerState)
	//		{
	//			KilledPlayerState->RespawnWaitTime = 2.f;
	//			KilledPlayerState->OnRespawnWaitReceived();
	//		}
	//		BroadcastDeathMessage(Killer, KilledPlayer, DamageType);

	//		if (bHasRespawnChoices)
	//		{
	//			KilledPlayerState->RespawnChoiceA = nullptr;
	//			KilledPlayerState->RespawnChoiceB = nullptr;
	//			KilledPlayerState->RespawnChoiceA = Cast<APlayerStart>(ChoosePlayerStart(KilledPlayer));
	//			KilledPlayerState->RespawnChoiceB = Cast<APlayerStart>(ChoosePlayerStart(KilledPlayer));
	//			KilledPlayerState->bChosePrimaryRespawnChoice = true;
	//		}
	//	}

	//	DiscardInventory(KilledPawn, Killer);
	//}

	DiscardInventory(KilledPawn, Killer);
	NotifyKilled(Killer, KilledPlayer, KilledPawn, DamageType);
}

void AUTGameMode::NotifyKilled(AController* Killer, AController* Killed, APawn* KilledPawn, TSubclassOf<UDamageType> DamageType)
{
	// update AI data
	if (Killer != nullptr && Killer != Killed)
	{
		AUTRecastNavMesh* NavData = GetUTNavData(GetWorld());
		if (NavData != nullptr)
		{
			{
				UUTPathNode* Node = NavData->FindNearestNode(KilledPawn->GetNavAgentLocation(), KilledPawn->GetSimpleCollisionCylinderExtent());
				if (Node != nullptr)
				{
					Node->NearbyDeaths++;
				}
			}
			if (Killer->GetPawn() != nullptr)
			{
				// it'd be better to get the node from which the shot was fired, but it's probably not worth it
				UUTPathNode* Node = NavData->FindNearestNode(Killer->GetPawn()->GetNavAgentLocation(), Killer->GetPawn()->GetSimpleCollisionCylinderExtent());
				if (Node != nullptr)
				{
					Node->NearbyKills++;
				}
			}
		}
	}

	for (FConstControllerIterator It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		if (It->IsValid())
		{
			AUTBot* B = Cast<AUTBot>(It->Get());
			if (B != nullptr && !B->IsPendingKillPending())
			{
				B->UTNotifyKilled(Killer, Killed, KilledPawn, DamageType.GetDefaultObject());
			}
		}
	}
}

void AUTGameMode::ScorePickup_Implementation(AUTPickup* Pickup, AUTPlayerState* PickedUpBy, AUTPlayerState* LastPickedUpBy)
{
}

void AUTGameMode::ScoreDamage_Implementation(int32 DamageAmount, AUTPlayerState* Victim, AUTPlayerState* Attacker)
{
	if (Victim && Attacker && UTGameState && !UTGameState->OnSameTeam(Victim, Attacker))
	{
		Attacker->IncrementDamageDone(DamageAmount);
	}
}

void AUTGameMode::ScoreTeamKill_Implementation(AController* Killer, AController* Other, APawn* KilledPawn, TSubclassOf<UDamageType> DamageType)
{
	AddKillEventToReplay(Killer, Other, DamageType);
}

void AUTGameMode::ScoreKill_Implementation(AController* Killer, AController* Other, APawn* KilledPawn, TSubclassOf<UDamageType> DamageType)
{
	AUTPlayerState* OtherPlayerState = Other ? Cast<AUTPlayerState>(Other->GetPlayerState<AUTPlayerState>()) : NULL;
	if( (Killer == Other) || (Killer == nullptr) )
	{
		// If it's a suicide, subtract a kill from the player...
		if (OtherPlayerState)
		{
			OtherPlayerState->AdjustScore(-1);
		}
	}
	else 
	{
		AUTPlayerState * KillerPlayerState = Cast<AUTPlayerState>(Killer->GetPlayerState<AUTPlayerState>());
		if ( KillerPlayerState != nullptr )
		{
			KillerPlayerState->AdjustScore(+1);
			KillerPlayerState->IncrementKills(DamageType, true, OtherPlayerState);
			CheckScore(KillerPlayerState);
		}

		if (!bFirstBloodOccurred)
		{
			BroadcastLocalized(this, UUTFirstBloodMessage::StaticClass(), 0, KillerPlayerState, NULL, NULL);
			bFirstBloodOccurred = true;
		}
	}

	AddKillEventToReplay(Killer, Other, DamageType);
	FindAndMarkHighScorer();
}

void AUTGameMode::AddKillEventToReplay(AController* Killer, AController* Other, TSubclassOf<UDamageType> DamageType)
{
	// Could be a suicide
	if (Killer == nullptr)
	{
		return;
	}

	// Shouldn't happen, but safety first
	if (Other == nullptr)
	{
		return;
	}
	
	UDemoNetDriver* DemoNetDriver = GetWorld()->DemoNetDriver;
	if (DemoNetDriver != nullptr && DemoNetDriver->ServerConnection == nullptr)
	{
		AUTPlayerState* KillerPlayerState = Cast<AUTPlayerState>(Killer->GetPlayerState<AUTPlayerState>());
		AUTPlayerState* OtherPlayerState = Cast<AUTPlayerState>(Other->GetPlayerState<AUTPlayerState>());

		FString KillerName = KillerPlayerState ? *KillerPlayerState->GetPlayerName() : TEXT("None");
		FString VictimName = OtherPlayerState ? *OtherPlayerState->GetPlayerName() : TEXT("None");

		KillerName.ReplaceInline(TEXT(" "), TEXT("%20"));
		VictimName.ReplaceInline(TEXT(" "), TEXT("%20"));

		FString DamageName = DamageType ? DamageType->GetName() : TEXT("Unknown");
		
		TArray<uint8> Data;
		FString KillInfo = FString::Printf(TEXT("%s %s %s"), *KillerName, *VictimName, *DamageName);

		FMemoryWriter MemoryWriter(Data);
		MemoryWriter.Serialize(TCHAR_TO_ANSI(*KillInfo), KillInfo.Len() + 1);

		FString MetaTag;
		if (KillerPlayerState != nullptr)
		{
			MetaTag = KillerPlayerState->StatsID;
			if (MetaTag.IsEmpty())
			{
				MetaTag = KillerPlayerState->GetPlayerName();
			}
		}
		DemoNetDriver->AddEvent(TEXT("Kills"), MetaTag, Data);
	}
}

void AUTGameMode::AddMultiKillEventToReplay(AController* Killer, int32 MultiKillLevel)
{
	UDemoNetDriver* DemoNetDriver = GetWorld()->DemoNetDriver;
	if (Killer && DemoNetDriver != nullptr && DemoNetDriver->ServerConnection == nullptr)
	{
		AUTPlayerState* KillerPlayerState = Cast<AUTPlayerState>(Killer->GetPlayerState<AUTPlayerState>());
		FString KillerName = KillerPlayerState ? *KillerPlayerState->GetPlayerName() : TEXT("None");
		KillerName.ReplaceInline(TEXT(" "), TEXT("%20"));

		TArray<uint8> Data;
		FString KillInfo = FString::Printf(TEXT("%s %d"), *KillerName, MultiKillLevel);

		FMemoryWriter MemoryWriter(Data);
		MemoryWriter.Serialize(TCHAR_TO_ANSI(*KillInfo), KillInfo.Len() + 1);

		FString MetaTag;
		if (KillerPlayerState != nullptr)
		{
			MetaTag = KillerPlayerState->StatsID;
			if (MetaTag.IsEmpty())
			{
				MetaTag = KillerPlayerState->GetPlayerName();
			}
		}
		DemoNetDriver->AddEvent(TEXT("MultiKills"), MetaTag, Data);
	}
}

void AUTGameMode::AddSpreeKillEventToReplay(AController* Killer, int32 SpreeLevel)
{
	UDemoNetDriver* DemoNetDriver = GetWorld()->DemoNetDriver;
	if (Killer && DemoNetDriver != nullptr && DemoNetDriver->ServerConnection == nullptr)
	{
		AUTPlayerState* KillerPlayerState = Cast<AUTPlayerState>(Killer->GetPlayerState<AUTPlayerState>());
		FString KillerName = KillerPlayerState ? *KillerPlayerState->GetPlayerName() : TEXT("None");
		KillerName.ReplaceInline(TEXT(" "), TEXT("%20"));

		TArray<uint8> Data;
		FString KillInfo = FString::Printf(TEXT("%s %d"), *KillerName, SpreeLevel);

		FMemoryWriter MemoryWriter(Data);
		MemoryWriter.Serialize(TCHAR_TO_ANSI(*KillInfo), KillInfo.Len() + 1);

		FString MetaTag;
		if (KillerPlayerState != nullptr)
		{
			MetaTag = KillerPlayerState->StatsID;
			if (MetaTag.IsEmpty())
			{
				MetaTag = KillerPlayerState->GetPlayerName();
			}
		}
		DemoNetDriver->AddEvent(TEXT("SpreeKills"), MetaTag, Data);
	}
}

bool AUTGameMode::OverridePickupQuery_Implementation(APawn* Other, TSubclassOf<AUTInventory> ItemClass, AActor* Pickup, bool& bAllowPickup)
{
	return false;// (BaseMutator != nullptr && BaseMutator->OverridePickupQuery(Other, ItemClass, Pickup, bAllowPickup));
}

void AUTGameMode::DiscardInventory(APawn* Other, AController* Killer)
{
	AUTCharacter* UTC = Cast<AUTCharacter>(Other);
	if (UTC != nullptr)
	{
		// toss weapon
		if (UTC->GetWeapon() != nullptr)
		{
			//if (UTC->GetWeapon()->ShouldDropOnDeath())
			//{
			//	AUTWeapon* OldWeapon = UTC->GetWeapon();
			//	UTC->TossInventory(UTC->GetWeapon());
			//	if (OldWeapon && !OldWeapon->IsPendingKillPending() && OldWeapon->bShouldAnnounceDrops && (OldWeapon->Ammo > 0) && (OldWeapon->PickupAnnouncementName != NAME_None) && bAllowPickupAnnouncements && IsMatchInProgress())
			//	{
			//		AUTPlayerState* UTPS = Cast<AUTPlayerState>(Other->GetPlayerState<AUTPlayerState>());
			//		if (UTPS)
			//		{
			//			UTPS->AnnounceStatus(OldWeapon->PickupAnnouncementName, 2);
			//		}
			//	}
			//}
			//else
			//{
			//	// drop default weapon instead @TODO FIXMESTEVE - this should go through default items array
			//	AUTWeapon* Enforcer = UTC->FindInventoryType<AUTWeapon>(AUTWeap_Enforcer::StaticClass());
			//	if (Enforcer && !Enforcer->IsPendingKillPending())
			//	{
			//		UTC->TossInventory(Enforcer);
			//	}
			//}
		}
		// toss all powerups
		//for (TInventoryIterator<> It(UTC); It; ++It)
		//{
		//	if (It->bAlwaysDropOnDeath)
		//	{
		//		UTC->TossInventory(*It, FVector(FMath::FRandRange(0.0f, 200.0f), FMath::FRandRange(-400.0f, 400.0f), FMath::FRandRange(0.0f, 200.0f)));
		//		if (It->bShouldAnnounceDrops && (It->PickupAnnouncementName != NAME_None) && bAllowPickupAnnouncements && IsMatchInProgress())
		//		{
		//			AUTPlayerState* UTPS = Cast<AUTPlayerState>(Other->GetPlayerState<AUTPlayerState>());
		//			AUTWeapon* Weap = Cast<AUTWeapon>(*It);
		//			if (UTPS && (!Weap || Weap->Ammo > 0))
		//			{
		//				UTPS->AnnounceStatus(It->PickupAnnouncementName, 2);
		//			}
		//		}
		//	}
		//}
		// delete the rest
		UTC->DiscardAllInventory();
	}
}

void AUTGameMode::FindAndMarkHighScorer()
{
	int32 BestScore = 0;
	for (int32 i = 0; i < UTGameState->PlayerArray.Num(); i++)
	{
		AUTPlayerState *PS = Cast<AUTPlayerState>(UTGameState->PlayerArray[i]);
		if (PS && !PS->bOnlySpectator)
		{
			if (BestScore == 0 || PS->Score > BestScore)
			{
				BestScore = PS->Score;
			}
		}
	}

	for (int32 i = 0; i < UTGameState->PlayerArray.Num(); i++)
	{
		AUTPlayerState* PS = Cast<AUTPlayerState>(UTGameState->PlayerArray[i]);
		AController* C = (PS != nullptr) ? Cast<AController>(PS->GetOwner()) : NULL;
		AUTCharacter *UTChar = (C != nullptr) ? Cast<AUTCharacter>(C->GetPawn()) : nullptr;
		if (UTChar != nullptr)
		{
			bool bOldHighScorer = UTChar->bHasHighScore;
			UTChar->bHasHighScore = (PS != nullptr && PS->Score == BestScore);
			if (bOldHighScorer != UTChar->bHasHighScore)
			{
				UTChar->HasHighScoreChanged();
			}
		}
	}
}

void AUTGameMode::StartMatch()
{
	if (HasMatchStarted())
	{
		// Already started
		return;
	}


	SetMatchState(MatchState::InProgress);
	
	// Any player that join pre-StartMatch is given a free pass to quit
	// Rejoining will not copy this flag to their new playerstate, so if they rejoin the server post-StartMatch and drop, they will get a quit notice
	for (int32 i = 0; i < InactivePlayerArray.Num(); i++)
	{
		AUTPlayerState* PS = Cast<AUTPlayerState>(InactivePlayerArray[i]);
		if (PS)
		{
			PS->bAllowedEarlyLeave = true;
		}
	}

	Super::StartMatch();
}

void AUTGameMode::RemoveAllPawns()
{
	// remove any warm up pawns
	for (FConstControllerIterator Iterator = GetWorld()->GetControllerIterator(); Iterator; ++Iterator)
	{
		// Detach all controllers from their pawns
		AController* Controller = Iterator->Get();
		if (Cast<AUTPlayerController>(Controller))
		{
			((AUTPlayerController *)Controller)->GetPlayerState<AUTPlayerState>()->bIsWarmingUp = false;
		}
		if (Controller->GetPawn() != nullptr)
		{
			Controller->UnPossess();
		}
	}

	TArray<APawn*> PawnsToDestroy;
	for (FConstPawnIterator It = GetWorld()->GetPawnIterator(); It; ++It)
	{
		if (It->Get() && !Cast<ASpectatorPawn>(It->Get()))
		{
			PawnsToDestroy.Add(It->Get());
		}
	}

	for (int32 i = 0; i<PawnsToDestroy.Num(); i++)
	{
		APawn* Pawn = PawnsToDestroy[i];
		if (Pawn != nullptr && !Pawn->IsPendingKill())
		{
			Pawn->Destroy();
		}
	}
}

void AUTGameMode::HandleMatchHasStarted()
{
	if (bRemovePawnsAtStart && (GetNetMode() != NM_Standalone) && !GetWorld()->IsPlayInEditor())
	{
		RemoveAllPawns();
	}

	// reset things, relevant for any kind of warmup mode and to make sure placed Actors like pickups are in their initial state no matter how much time has passed in pregame
	for (FActorIterator It(GetWorld()); It; ++It)
	{
		if (It->GetClass()->ImplementsInterface(UUTResetInterface::StaticClass()))
		{
			IUTResetInterface::Execute_Reset(*It);
		}
	}

	if (UTGameState != nullptr)
	{
		UTGameState->CompactSpectatingIDs();
	}

	UpdatePlayersPresence();

	Super::HandleMatchHasStarted();

	// notify players
	//for (FConstControllerIterator It = GetWorld()->GetControllerIterator(); It; ++It)
	//{
	//	AUTPlayerController* PC = Cast<AUTPlayerController>(*It);
	//	if (PC)
	//	{
	//		PC->ClientGameStarted();
	//	}
	//}

	if (UTIsHandlingReplays() && GetGameInstance() != nullptr)
	{
		GetGameInstance()->StartRecordingReplay(TEXT(""), GetWorld()->GetMapName());
	}

  UTGameState->SetTimeLimit(GetMatchOptions().RoundTimeInSeconds);
	bFirstBloodOccurred = false;
	AnnounceMatchStart();

	for (FConstControllerIterator It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		RestartPlayer(It->Get());
	}
}

void AUTGameMode::AnnounceMatchStart()
{
	BroadcastLocalized(this, UUTGameMessage::StaticClass(), 0, NULL, NULL, NULL);
}

void AUTGameMode::BeginGame()
{
	UE_LOG(UT,Log,TEXT("BEGIN GAME GameType: %s"), *GetNameSafe(this));

	for (FActorIterator It(GetWorld()); It; ++It)
	{
		AActor* TestActor = *It;
		if (TestActor && !TestActor->IsPendingKill() && TestActor->IsA<AUTPlayerState>())
		{
			Cast<AUTPlayerState>(TestActor)->StartTime = 0;
			Cast<AUTPlayerState>(TestActor)->bSentLogoutAnalytics = false;
		}
	}
	UTGameState->ElapsedTime = 0;

	//Let the game session override the StartMatch function, in case it wants to wait for arbitration
	if (GameSession->HandleStartMatchRequest())
	{
		return;
	}
	SetMatchState(MatchState::InProgress);
}

void AUTGameMode::EndMatch()
{
	UE_LOG(LogInit, Log, TEXT("AUTGameMode::EndMatch"));


	DetermineMatchWinner();

	FMatchEndResult matchResult;
	auto MyGameState = GetGameState<AUTGameState>();

	if (MyGameState)
	{
		if (NumPlayers)
		{
			MyGameState->SetRemainingTime(120.0f);
		}
		else
		{
			MyGameState->SetRemainingTime(10.0f);
		}

		matchResult.WinnerTeamId = (MyGameState->Teams.Num() && MyGameState->Teams.IsValidIndex(MatchWinner)) ? MyGameState->Teams[MatchWinner]->TeamId.ToString() : FString();
	}

	AUTPlayerState	*Best_Player = nullptr, *Best_Killer = nullptr, *Best_Shooter = nullptr, *Best_Hero = nullptr;
	auto KillsAvgLambda = [](AUTPlayerState* State) { return FMath::Max(.0f, float(State->Kills) / float(State->Deaths)); };
	auto BestRewardLambda = [](AUTPlayerState* State, ItemAchievementTypeId::Type Medal) {
		if (State)
		{
			State->AddAchievement(Medal);
			State->AddExtraScore(GET_PERCENT_OF(State->Score, 15.0f));
		}
	};

	TArray<APlayerState*> PlayerStates(GameState->PlayerArray);
	PlayerStates.Append(InactivePlayerArray);

	for (auto It : PlayerStates)
	{
		FMatchMember member;

		AUTPlayerState* PlayerState = Cast<AUTPlayerState>(It);
		if (PlayerState)
		{
			if (PlayerState->GetTeamNum() == MatchWinner) // Give 10 percents to winner team
			{
				PlayerState->AddExtraScore(GET_PERCENT_OF(PlayerState->Score, 10.0f));
			}

			if (Best_Player == nullptr || Best_Player->Score < PlayerState->Score)
			{
				Best_Player = PlayerState;
			}

			if (Best_Killer == nullptr || Best_Killer->Kills < PlayerState->Kills)
			{
				Best_Killer = PlayerState;
			}

			if (Best_Shooter == nullptr || (Best_Shooter->GetAccuracyAnyPawn() < PlayerState->GetAccuracyAnyPawn() && PlayerState->Kills > 2))
			{
				Best_Shooter = PlayerState;
			}

			if (Best_Hero == nullptr || (KillsAvgLambda(Best_Hero) < KillsAvgLambda(PlayerState) && PlayerState->Kills > 2))
			{
				Best_Hero = PlayerState;
			}

			/*if (auto PlayerPC = Cast<AUTPlayerController>(PlayerState->GetOwner()))
			{
				for (auto i : PlayerPC->GetInventory().Armour)
				{
					if (i && i->IsValidLowLevel())
					{
						PlayerState->AddExtraScore(GET_PERCENT_OF(PlayerState->Score, i->GetRewardBonus()));
					}
				}
			}*/
		}
	}

	BestRewardLambda(Best_Player, ItemAchievementTypeId::Medal_Match_Best_Player);
	BestRewardLambda(Best_Killer, ItemAchievementTypeId::Medal_Match_Best_Killer);
	BestRewardLambda(Best_Shooter, ItemAchievementTypeId::Medal_Match_Best_Shooter);
	BestRewardLambda(Best_Hero, ItemAchievementTypeId::Medal_Match_Best_Hero);

	for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
	{
		if (auto ctrl = Cast<AUTPlayerController>(*It))
		{
			ctrl->ClientShowMatchResults(MatchWinner);
		}
	}

	for (auto It : PlayerStates)
	{
		FMatchMember member;

		AUTPlayerState* PlayerState = Cast<AUTPlayerState>(It);
		if (PlayerState)
		{
			int32 TotalSeriesKill = 0;
			PlayerState->ApplyExtraScore();

			if (auto MyCtrl = Cast<AUTPlayerController>(PlayerState->GetOwner()))
			{
				// TODO: Нуждается в рефакторинге
				//if (MyCtrl->PresetData.Num() >= 4)
				//{
				//	for (SIZE_T i = 0; i < 5; ++i)
				//	{
				//		//for (auto w : MyCtrl->PresetData[i].Weapons)
				//		//{
				//		//	auto TargetWeapon = FWeaponHelper(CategoryTypeId::Weapon, w.Item);
				//		//	if (PlayerState->KillsData.Contains(TargetWeapon) && FWeaponHelper::KillsAchievementsList.Contains(TargetWeapon))
				//		//	{
				//		//		TArray<int32> Values;
				//		//		PlayerState->KillsData.MultiFind(TargetWeapon, Values);

				//		//		FMatchMemberAchievement KillsAchievement;
				//		//		KillsAchievement.AchievementTypeId = FWeaponHelper::KillsAchievementsList.FindChecked(TargetWeapon);
				//		//		KillsAchievement.Count = 0;

				//		//		for (auto v : Values)
				//		//		{
				//		//			KillsAchievement.Count += v;

				//		//			if (v >= 5) // First series kill medal
				//		//			{
				//		//				FMatchMemberAchievement SeriesKillsAchievement;
				//		//				SeriesKillsAchievement.AchievementTypeId = FWeaponHelper::SeriesKillAchievementsList.FindChecked(TargetWeapon);
				//		//				SeriesKillsAchievement.Count = v;
				//		//				member.Achievements.Add(SeriesKillsAchievement);

				//		//				++TotalSeriesKill;
				//		//			}
				//		//		}

				//		//		member.Achievements.Add(KillsAchievement);
				//		//	}
				//		//}

				//		//for (auto w : MyCtrl->PresetData[i].Grenades)
				//		//{
				//		//	auto TargetWeapon = FWeaponHelper(CategoryTypeId::Grenade, w);
				//		//	if (PlayerState->KillsData.Contains(TargetWeapon) && FWeaponHelper::KillsAchievementsList.Contains(TargetWeapon))
				//		//	{
				//		//		TArray<int32> Values;
				//		//		PlayerState->KillsData.MultiFind(TargetWeapon, Values);

				//		//		FMatchMemberAchievement KillsAchievement;
				//		//		KillsAchievement.AchievementTypeId = FWeaponHelper::KillsAchievementsList.FindChecked(TargetWeapon);
				//		//		KillsAchievement.Count = 0;

				//		//		for (auto v : Values)
				//		//		{
				//		//			KillsAchievement.Count += v;

				//		//			if (v >= 5) // First series kill medal
				//		//			{
				//		//				FMatchMemberAchievement SeriesKillsAchievement;
				//		//				SeriesKillsAchievement.AchievementTypeId = FWeaponHelper::SeriesKillAchievementsList.FindChecked(TargetWeapon);
				//		//				SeriesKillsAchievement.Count = v;
				//		//				member.Achievements.Add(SeriesKillsAchievement);

				//		//				++TotalSeriesKill;
				//		//			}
				//		//		}

				//		//		member.Achievements.Add(KillsAchievement);
				//		//	}
				//		//}
				//	}
				//}
			}

			{
				FMatchMemberAchievement Achievement;
				Achievement.AchievementTypeId = ItemAchievementTypeId::Kill_Total;
				Achievement.Count = PlayerState->Kills;
				member.Achievements.Add(Achievement);
			}

			{
				FMatchMemberAchievement Achievement;
				Achievement.AchievementTypeId = ItemAchievementTypeId::SeriesKill_Total;
				Achievement.Count = TotalSeriesKill;
				member.Achievements.Add(Achievement);
			}

			const auto money = PlayerState->Score > .0f ? FMath::FloorToInt(PlayerState->Score) : 0;
			PlayerState->GiveCash(EGameCurrency::Money, money);
			PlayerState->Experience = (money > 0) ? FMath::FloorToInt(PlayerState->Score / 2.5f) : 0;
			PlayerState->Reputation = FMath::FloorToInt(float(PlayerState->Experience) / 1.5f);

			AddExtraResult(PlayerState);

			member.MemberId = PlayerState->MemberId.ToString();
			member.Award.Experience = PlayerState->Experience + PlayerState->PremiumExperience;
			member.Award.Money = PlayerState->TakeCash(EGameCurrency::Money)->Amount;
			member.Award.Donate = PlayerState->TakeCash(EGameCurrency::Donate)->Amount;
			member.Award.Bitcoin = PlayerState->TakeCash(EGameCurrency::Coin)->Amount;

			member.Award.Reputation = PlayerState->Reputation + PlayerState->PremiumReputation;
			member.Award.FractionId = PlayerState->FractionId;

			// Statistics
			member.Statistic.Score = PlayerState->Score;
			member.Statistic.Service = PlayerState->UsingPremiumService;
			member.Statistic.Shots = PlayerState->ShootData.Num();
			member.Statistic.Hits = PlayerState->GetHitsByPawn(false);
			member.Statistic.Assists = 0; //TODO: Need add counters;
			member.Statistic.Kills = PlayerState->GetKills();
			member.Statistic.Deads = PlayerState->GetDeaths();

			member.Statistic.SmallSeriesKill = PlayerState->Achievements.FilterByPredicate([](const TEnumAsByte<ItemAchievementTypeId::Type>& Achiv) { return Achiv == ItemAchievementTypeId::Medal_Match_SeriesKill_5; }).Num();
			member.Statistic.ShortSeriesKill = PlayerState->Achievements.FilterByPredicate([](const TEnumAsByte<ItemAchievementTypeId::Type>& Achiv) { return Achiv == ItemAchievementTypeId::Medal_Match_SeriesKill_10; }).Num();
			member.Statistic.LargeSeriesKill = PlayerState->Achievements.FilterByPredicate([](const TEnumAsByte<ItemAchievementTypeId::Type>& Achiv) { return Achiv == ItemAchievementTypeId::Medal_Match_SeriesKill_15; }).Num();
			member.Statistic.EpicSeriesKill = PlayerState->Achievements.FilterByPredicate([](const TEnumAsByte<ItemAchievementTypeId::Type>& Achiv) { return Achiv == ItemAchievementTypeId::Medal_Match_SeriesKill_20; }).Num();

			for (auto Achiev: PlayerState->Achievements)
			{
				FMatchMemberAchievement Achievement;
				Achievement.AchievementTypeId = Achiev;
				Achievement.Count = 1;
				member.Achievements.Add(Achievement);
			}

			if (!PlayerState->bIsABot)
			{
				matchResult.Members.Add(member);
			}
		}
	}

	SendEndMatchRequest(matchResult);


	// Call DEFAULT AGameMode::EndMatch
	AGameMode::EndMatch();	
		
	FTimerHandle TempHandle;
	GetWorldTimerManager().SetTimer(TempHandle, this, &AUTGameMode::PlayEndOfMatchMessage, EndOfMatchMessageDelay * GetActorTimeDilation());

	for (FConstPawnIterator Iterator = GetWorld()->GetPawnIterator(); Iterator; ++Iterator )
	{
		// If a pawn is marked pending kill, *Iterator will be NULL
		APawn* Pawn = Iterator->Get();
		if (Pawn && !Cast<ASpectatorPawn>(Pawn))
		{
			Pawn->TurnOff();
		}
	}

	FTimerHandle _th;
	GetWorldTimerManager().SetTimer(_th, FTimerDelegate::CreateLambda([&]()
	{
		for (FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
		{
			if (Iterator && Iterator->IsValid())
			{
				Iterator->Get()->StartSpectatingOnly();

				auto TargetLoc = FVector::ZeroVector + FVector::UpVector * 5000.0f;
				Iterator->Get()->ServerSetSpectatorLocation(TargetLoc, (TargetLoc * -1.0f).Rotation());
			}
		}
	}), 2.0f, false);
}

void AUTGameMode::AddExtraResult(AUTPlayerState* PlayerState)
{
	UE_LOG(LogInit, Log, TEXT("%s [%s] UsingPremiumService %d"), *PlayerState->GetPlayerName(), *PlayerState->MemberId.ToString(), static_cast<int32>(PlayerState->UsingPremiumService));

	if (FFlagsHelper::HasAnyFlags(PlayerState->UsingPremiumService, UsingPremiumServiceFlag::PremiumAccount))
	{
		auto localCashMoney = PlayerState->TakeCash(EGameCurrency::Money);
		auto localCashDonate = PlayerState->TakeCash(EGameCurrency::Money);

		auto localPremiumCashMoney = PlayerState->TakePremiumCash(EGameCurrency::Money);
		auto localPremiumCashDonate = PlayerState->TakePremiumCash(EGameCurrency::Donate);

		localPremiumCashMoney->Amount += FMath::FloorToInt(float(localCashMoney->Amount) * .5f);
		localPremiumCashDonate->Amount += FMath::FloorToInt(float(localCashDonate->Amount) * .5f);

		PlayerState->PremiumExperience += GET_PERCENT_OF(PlayerState->Experience, 50);
		PlayerState->PremiumReputation += GET_PERCENT_OF(PlayerState->Reputation, 50);

		UE_LOG(LogInit, Log, TEXT("%s [%s] PremiumAccount [Money %d + %d] | [Experience %d + %d] | [Reputation %d + %d]"),
				   *PlayerState->GetPlayerName(),
				   *PlayerState->MemberId.ToString(),
					localCashMoney->Amount, localPremiumCashMoney->Amount,
					PlayerState->Experience, PlayerState->PremiumExperience,
					PlayerState->Reputation, PlayerState->PremiumReputation
			   )



;
	}

	if (FFlagsHelper::HasAnyFlags(PlayerState->UsingPremiumService, UsingPremiumServiceFlag::BoosterOfMoney))
	{
		auto localCashMoney = PlayerState->TakeCash(EGameCurrency::Money);
		auto localCashDonate = PlayerState->TakeCash(EGameCurrency::Money);

		auto localPremiumCashMoney = PlayerState->TakePremiumCash(EGameCurrency::Money);
		auto localPremiumCashDonate = PlayerState->TakePremiumCash(EGameCurrency::Donate);

		localPremiumCashMoney->Amount += FMath::FloorToInt(float(localCashMoney->Amount) * .2f);
		localPremiumCashDonate->Amount += FMath::FloorToInt(float(localCashDonate->Amount) * .2f);

		UE_LOG(LogInit, Log, TEXT("%s [%s] BoosterOfMoney %d + %d"), *PlayerState->GetPlayerName(), *PlayerState->MemberId.ToString(), localCashMoney->Amount, localPremiumCashMoney->Amount);
	}

	if (FFlagsHelper::HasAnyFlags(PlayerState->UsingPremiumService, UsingPremiumServiceFlag::BoosterOfExperience))
	{
		PlayerState->PremiumExperience += GET_PERCENT_OF(PlayerState->Experience, 20);
		UE_LOG(LogInit, Log, TEXT("%s [%s] BoosterOfExperience %d + %d"), *PlayerState->GetPlayerName(), *PlayerState->MemberId.ToString(), PlayerState->Experience, PlayerState->PremiumExperience);
	}

	if (FFlagsHelper::HasAnyFlags(PlayerState->UsingPremiumService, UsingPremiumServiceFlag::BoosterOfReputation))
	{
		PlayerState->PremiumReputation += GET_PERCENT_OF(PlayerState->Reputation, 20);
		UE_LOG(LogInit, Log, TEXT("%s [%s] BoosterOfReputation %d + %d"), *PlayerState->GetPlayerName(), *PlayerState->MemberId.ToString(), PlayerState->Reputation, PlayerState->PremiumReputation);
	}

	if (UTGameState && FFlagsHelper::HasAnyFlags(UTGameState->GetGameModeType().Value, EGameMode::LostDeadMatch.ToFlag() | EGameMode::TeamDeadMatch.ToFlag() | EGameMode::ResearchMatch.ToFlag()) && PlayerState->GetTeamNum() == MatchWinner)
	{
		PlayerState->GiveCash(EGameCurrency::Donate, 1);

		if (FFlagsHelper::HasAllFlags(PlayerState->UsingPremiumService, UsingPremiumServiceFlag::BoosterOfMoney | UsingPremiumServiceFlag::PremiumAccount))
		{
			PlayerState->GiveCash(EGameCurrency::Donate, 1);
		}
	}
}

bool AUTGameMode::AllowPausing(APlayerController* PC)
{
	return false;
}

void AUTGameMode::EndGame()
{
	UE_LOG(LogInit, Log, TEXT("AUTGameMode::EndMatch"));
#if UE_SERVER && !WITH_EDITOR
	for (FConstControllerIterator It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		ABasePlayerController* PlayerController = Cast<ABasePlayerController>(*It);
		if (PlayerController != nullptr)
		{
			PlayerController->TravelToLobbyRoom();
			//GameSession->KickPlayer(PlayerController, FText::FromString("EndGame();"));
		}
	}

	GetWorldTimerManager().ClearTimer(TimerHandle_DefaultTimer);
	GetWorldTimerManager().ClearAllTimersForObject(this);
	GetWorldTimerManager().SetTimer(ServerShutdownTimerHandle, ServerShutdownTimerDelegate, 5.0f, false);
#endif
}



void AUTGameMode::StopReplayRecording()
{
	if (UTIsHandlingReplays() && GetGameInstance() != nullptr)
	{
		GetGameInstance()->StopRecordingReplay();
	}
}

bool AUTGameMode::UTIsHandlingReplays()
{
	return false;

	// If we're running in PIE, don't record demos
	if (GetWorld() != nullptr && GetWorld()->IsPlayInEditor())
	{
		return false;
	}
	
#if !(UE_BUILD_SHIPPING)
	//Ignore bRecordReplays for non-shipping builds and always record replays on servers.
	return GetNetMode() == ENetMode::NM_DedicatedServer;
#endif

	return bRecordReplays && GetNetMode() == ENetMode::NM_DedicatedServer;
}



void AUTGameMode::SetEndGameFocus(AUTPlayerState* Winner)
{
	if (Winner == nullptr) return; // It's possible to call this with Winner == nullptr if timelimit is hit with noone on the server

	EndGameFocus = Cast<AController>(Winner->GetOwner())->GetPawn();
	if ( (EndGameFocus == nullptr) && (Cast<AController>(Winner->GetOwner()) != nullptr) )
	{
		// If the controller of the winner does not have a pawn, give him one.
		RestartPlayer(Cast<AController>(Winner->GetOwner()));
		EndGameFocus = Cast<AController>(Winner->GetOwner())->GetPawn();
	}

	if ( EndGameFocus != nullptr )
	{
		EndGameFocus->bAlwaysRelevant = true;
	}

	for( FConstControllerIterator Iterator = GetWorld()->GetControllerIterator(); Iterator; ++Iterator )
	{
		AController* Controller = Iterator->Get();
		Controller->GameHasEnded(EndGameFocus, (Controller->GetPlayerState<AUTPlayerState>() != nullptr) && (Controller->GetPlayerState<AUTPlayerState>() == Winner) );
	}
}


void AUTGameMode::PlayEndOfMatchMessage()
{
	if (!UTGameState || !UTGameState->WinnerPlayerState)
	{
		return;
	}
	for (FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
	{
		AUTPlayerController* PC = Cast<AUTPlayerController>(*Iterator);
		if (PC && (PC->GetPlayerState<AUTPlayerState>() != nullptr) && !PC->GetPlayerState<AUTPlayerState>()->bOnlySpectator)
		{
			PC->ClientReceiveLocalizedMessage(VictoryMessageClass, ((UTGameState->WinnerPlayerState == PC->GetPlayerState<AUTPlayerState>()) ? 1 : 0), UTGameState->WinnerPlayerState, PC->GetPlayerState<AUTPlayerState>(), NULL);
		}
	}
}

bool AUTGameMode::AllowSuicideBy(AUTPlayerController* PC)
{
	if (GetMatchState() != MatchState::InProgress)
	{
		return false;
	}
	if (GetWorld()->WorldType == EWorldType::PIE || GetNetMode() == NM_Standalone)
	{
		return true;
	}
	return (PC->GetPawn() != nullptr) && (GetWorld()->TimeSeconds - PC->GetPawn()->CreationTime > 10.0f);
}


bool AUTGameMode::ShouldSpawnAtStartSpot(AController* Player)
{
	if ( Player && Cast<APlayerStartPIE>(Player->StartSpot.Get()) )
	{
		return true;
	}

	return false;
}

/**
 *	We are going to duplicate GameMode's StartNewPlayer because we need to replicate the scoreboard class along with the hud class.  
 *  We are doing this here like this because we are trying to not change engine.  Ultimately the code to create the hud should be
 *  moved to it's own easy to override function instead of being hard-coded in StartNewPlayer
 **/
void AUTGameMode::HandleStartingNewPlayer_Implementation(APlayerController* NewPlayer)
{
	AUTPlayerController* UTNewPlayer = Cast<AUTPlayerController>(NewPlayer);
	if (UTNewPlayer != nullptr)
	{
		// tell client what hud class to use
		UTNewPlayer->HUDClass = HUDClass;
		if (Cast<UNetConnection>(UTNewPlayer->Player) == nullptr)
		{
			UTNewPlayer->OnRep_HUDClass();
		}

		// start match, or let player enter, immediately
		if (PlayerCanRestart(NewPlayer))
		{
			RestartPlayer(NewPlayer);
		}

		if (NewPlayer->GetPawn() != nullptr)
		{
			NewPlayer->ClientSetRotation(NewPlayer->GetPawn()->GetActorRotation());
		}
	}
	else
	{
		Super::HandleStartingNewPlayer(NewPlayer);
	}
}

bool AUTGameMode::ReadyToStartMatch_Implementation()
{
	return Super::ReadyToStartMatch_Implementation();

	//if (GetWorld()->IsPlayInEditor() || !bDelayedStart)
	//{
	//	// starting on first frame has side effects in PIE because of differences in ordering; components haven't been initialized/registered yet...
	//	if (GetWorld()->TimeSeconds == 0.0f)
	//	{
	//		GetWorldTimerManager().SetTimerForNextTick(this, &AUTGameMode::StartMatch);
	//		return false;
	//	}
	//	else
	//	{
	//		// PIE is always ready to start.
	//		return true;
	//	}
	//}

	//if (GetMatchState() == MatchState::WaitingToStart)
	//{
	//	StartPlayTime = (NumPlayers > 0) ? FMath::Min(StartPlayTime, GetWorld()->GetTimeSeconds()) : 10000000.f;
	//	float ElapsedWaitTime = FMath::Max(0.f, GetWorld()->GetTimeSeconds() - StartPlayTime);

	//	if (bRankedSession)
	//	{
	//		if (ExpectedPlayerCount != 0 && ExpectedPlayerCount == NumPlayers)
	//		{
	//			MaxReadyWaitTime = FMath::Min(10, MaxReadyWaitTime);
	//			//LockSession();
	//		}

	//		bool bMaxWaitComplete = (MaxReadyWaitTime > 0) && (GetNetMode() != NM_Standalone) && (ElapsedWaitTime > MaxReadyWaitTime);
	//		if (bMaxWaitComplete)
	//		{
	//			if (ExpectedPlayerCount == 0 || ExpectedPlayerCount == NumPlayers)
	//			{
	//				GetWorldTimerManager().ClearTimer(ServerRestartTimerHandle);

	//				//LockSession();

	//				return true;
	//			}
	//			else
	//			{
	//				UE_LOG(UT, Log, TEXT("Not enough players showed up, abandoning game"));

	//				// Not enough players showed up for the match, just send them back to the lobby
	//				GetWorldTimerManager().ClearTimer(ServerRestartTimerHandle);

	//				SendEveryoneBackToLobbyGameAbandoned();

	//				// TODO
	//				//SetMatchState(MatchState::MatchRankedAbandon);
	//				//AUTGameSessionRanked* RankedGameSession = Cast<AUTGameSessionRanked>(GameSession);
	//				//if (RankedGameSession)
	//				//{
	//				//	RankedGameSession->Restart();
	//				//}
	//			}
	//		}
	//	}
	//	else
	//	{
	//		//UTGameState->PlayersNeeded = (!bIsQuickMatch || GetWorld()->GetTimeSeconds() - StartPlayTime > MaxWaitForQuickMatch) ? FMath::Max(0, MinPlayersToStart - NumPlayers - NumBots) : FMath::Max(0, FMath::Min(GameSession->MaxPlayers, QuickPlayersToStart) - NumPlayers - NumBots);
	//		if (((GetNetMode() == NM_Standalone) || bDevServer || (UTGameState->PlayersNeeded == 0)) && (NumPlayers + NumSpectators > 0))
	//		{
	//			// Count how many ready players we have
	//			bool bCasterReady = false;
	//			int32 ReadyCount = 0;
	//			int32 AllCount = 0;
	//			int32 WarmupCount = 0;
	//			for (int32 i = 0; i < UTGameState->PlayerArray.Num(); i++)
	//			{
	//				AUTPlayerState* PS = Cast<AUTPlayerState>(UTGameState->PlayerArray[i]);
	//				if (PS != nullptr && !PS->bOnlySpectator)
	//				{
	//					if (PS->bReadyToPlay)
	//					{
	//						ReadyCount++;
	//						if (PS->bIsWarmingUp)
	//						{
	//							WarmupCount++;
	//						}
	//					}
	//					AllCount++;
	//				}
	//			}
	//		}
	//	}

	//	bool bUpdateWaitCountdown = (!bRequireReady && (MaxReadyWaitTime > 0) && UTGameState && (ElapsedWaitTime > 0));
	//	if (bRankedSession)
	//	{
	//		bUpdateWaitCountdown = true;
	//	}
	//}
	//return false;
}


/**	I needed to rework the ordering of SetMatchState until it can be corrected in the engine. **/
void AUTGameMode::SetMatchState(FName NewState)
{
	if (MatchState == NewState)
	{
		return;
	}

	MatchState = NewState;
	if (UTGameState)
	{
		UTGameState->SetMatchState(NewState);
	}

	CallMatchStateChangeNotify();
	K2_OnSetMatchState(NewState);

	if ((NewState == MatchState::WaitingPostMatch) && (UTGameState) && (UTGameState->InGameIntroHelper))
	{
		InGameIntroZoneTypes PlayType = UTGameState->InGameIntroHelper->GetIntroTypeToPlay(GetWorld());
		if (PlayType != InGameIntroZoneTypes::Invalid)
		{
			UTGameState->InGameIntroHelper->HandleEndMatchSummary(GetWorld(), PlayType);
		}
	}
}

void AUTGameMode::CallMatchStateChangeNotify()
{
	// Call change callbacks

	if (MatchState == MatchState::WaitingToStart)
	{
		HandleMatchIsWaitingToStart();
	}
	else if (MatchState == MatchState::InProgress)
	{
		HandleMatchHasStarted();
	}
	else if (MatchState == MatchState::WaitingPostMatch)
	{
		//FTimerHandle TempHandle;
		//GetWorldTimerManager().SetTimer(TempHandle, this, &AUTGameMode::HandleMatchHasEnded, 15.5f);
		//HandleMatchHasEnded();
	}
	else if (MatchState == MatchState::LeavingMap)
	{
		HandleLeavingMap();
	}
	else if (MatchState == MatchState::Aborted)
	{
		HandleMatchAborted();
	}
	else if (MatchState == MatchState::MatchEnteringOvertime)
	{
		HandleEnteringOvertime();
	}
	else if (MatchState == MatchState::MatchIsInOvertime)
	{
		HandleMatchInOvertime();
	}
}

void AUTGameMode::HandleMatchHasEnded()
{
	Super::HandleMatchHasEnded();

	// save AI data only after completed matches
	AUTRecastNavMesh* NavData = GetUTNavData(GetWorld());
	if (NavData != nullptr)
	{
		NavData->SaveMapLearningData();
	}

	for (FConstControllerIterator It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		AUTPlayerController* PlayerController = Cast<AUTPlayerController>(*It);
		if (PlayerController != nullptr)
		{
			PlayerController->TravelToLobbyRoom();
			//GameSession->KickPlayer(PlayerController, FText::FromString("EndGame();"));
		}
	}

	GetWorldTimerManager().ClearTimer(TimerHandle_DefaultTimer);
	//UGameplayStatics::OpenLevel(GetWorld(), TEXT("UTEntryScreen"), true, TEXT("?Game=Engine.GameMode"));

	FString TravelURL = FString::Printf(TEXT("/Engine/Maps/Entry?Game=Engine.GameMode"));
	GetWorld()->ServerTravel(TravelURL);
}


void AUTGameMode::HandleEnteringOvertime()
{
	SetMatchState(MatchState::MatchIsInOvertime);
}

void AUTGameMode::HandleMatchInOvertime()
{
	// Send the overtime message....
	BroadcastLocalized( this, UUTGameMessage::StaticClass(), 1, NULL, NULL, NULL);
}


AUTPlayerState* AUTGameMode::IsThereAWinner_Implementation(bool& bTied)
{
	AUTPlayerState* BestPlayer = NULL;
	float BestScore = 0.0;

	for (int32 PlayerIdx=0; PlayerIdx < UTGameState->PlayerArray.Num();PlayerIdx++)
	{
		if ((UTGameState->PlayerArray[PlayerIdx] != nullptr) && !UTGameState->PlayerArray[PlayerIdx]->bOnlySpectator)
		{
			if (BestPlayer == nullptr || UTGameState->PlayerArray[PlayerIdx]->Score > BestScore)
			{
				BestPlayer = Cast<AUTPlayerState>(UTGameState->PlayerArray[PlayerIdx]);
				BestScore = BestPlayer->Score;
				bTied = false;
			}
			else if (UTGameState->PlayerArray[PlayerIdx]->Score == BestScore)
			{
				bTied = true;
			}
		}
	}
	return BestPlayer;
}

bool AUTGameMode::CheckScore_Implementation(AUTPlayerState* Scorer)
{
	if (Scorer != nullptr)
	{
		if (Scorer->Score >= GetMatchOptions().GetScoreLimit())
		{
//			EndGame(Scorer, FName(TEXT("fraglimit")));
		}
	}
	return true;
}

void AUTGameMode::OverridePlayerState(APlayerController* PC, APlayerState* OldPlayerState)
{
	Super::OverridePlayerState(PC, OldPlayerState);
}


void AUTGameMode::PostLogin( APlayerController* NewPlayer )
{
	static const uint8 DEFAULT_TEAMID_ID_EDITOR = 0;
	auto TargetPC = Cast<AUTPlayerController>(NewPlayer);

	if (UTGameState)
	{
#if UE_SERVER && !WITH_EDITOR
		Super::PostLogin(NewPlayer);

		if (auto LocalPlayer = Cast<UNetConnection>(NewPlayer->Player))
		{
			const auto TargetMember = FindMatchMember(LocalPlayer->PlayerId->ToString());

			if (TargetMember && TargetPC)
			{
				if (auto TargetState = Cast<AUTPlayerState>(TargetPC->GetPlayerState<AUTPlayerState>()))
				{					
					TargetState->TeamId = TargetMember->TeamId;

					//=====================================================================
					if (GetNetMode() == NM_Standalone || GEngine->IsEditor())
					{
						ChangeTeam(TargetPC, DEFAULT_TEAMID_ID_EDITOR, false);
					}
					else
					{
						MovePlayerToTeam(TargetPC, TargetState, GetTeamIndexById(TargetState->TeamId));
						TargetState->HandleTeamChanged(TargetPC);
					}

					//=====================================================================
					UE_LOG(LogShooter, Log, TEXT("Member: %s connected with team: %s[%d]"), *TargetState->GetPlayerName(), *TargetMember->TeamId.ToString(), TargetState->GetTeamNum());
				}
			}
		}		

		if (NumPlayers >= GetMatchMebersNum() && GetMatchState() == MatchState::WaitingToStart && UTGameState->GetRemainingTime() > Time_WaitingStartMatch)
		{
			UTGameState->SetRemainingTime(Time_WaitingStartMatch);
		}
#else		
		Super::PostLogin(NewPlayer);
		UTGameState->SetRemainingTime(3.0f);
		
#endif

#if WITH_EDITOR
		ChangeTeam(NewPlayer, DeveloperTeamNum, false);
#endif

		if (TargetPC && GetMatchState() == MatchState::WaitingToStart)
		{
			TargetPC->ClientShowTextTimed(UTGameState->RemainingTime, (UTGameState->RemainingTime > Time_WaitingStartMatch) ? ETimedTextMessage::WaitingPlayers : ETimedTextMessage::WaitingStart);

			if (auto TeamStart = Cast<APlayerStart>(FindPlayerStart(NewPlayer)))
			{
				TargetPC->ClientSetLocation(TeamStart->GetActorLocation(), TeamStart->GetActorRotation());
			}
		}
	}



	if (GameSession != nullptr)
	{
		AUTGameSession* UTGameSession = Cast<AUTGameSession>(GameSession);
		if (UTGameSession != nullptr)
		{
			UTGameSession->UpdateGameState();
		}
	}

	
	CheckBotCount();
	
	// Check if a (re)joining player is the leader
	FindAndMarkHighScorer();
}


void AUTGameMode::Logout(AController* Exiting)
{
	// Lets Analytics know how long this player has been online....
	AUTPlayerState* PS = Cast<AUTPlayerState>(Exiting->GetPlayerState<AUTPlayerState>());

	if (AntiCheatEngine)
	{
		AntiCheatEngine->OnPlayerLogout(Cast<APlayerController>(Exiting));
	}

	if (Cast<AUTBot>(Exiting) != nullptr)
	{
		NumBots--;
	}

	// the demorec spectator doesn't count as a real player or spectator and we need to avoid the Super call that will incorrectly decrement the spectator count
	if (Cast<AUTDemoRecSpectator>(Exiting) == nullptr)
	{
		Super::Logout(Exiting);
	}

	if (GameSession != nullptr)
	{
		AUTGameSession* UTGameSession = Cast<AUTGameSession>(GameSession);
		if (UTGameSession != nullptr)
		{
			UTGameSession->UpdateGameState();
		}
	}

	UpdatePlayersPresence();


	if (NumPlayers == 0)
	{
		if (GetMatchState() == MatchState::InProgress)
		{
			EndMatch();
		}
		else if (GetMatchState() == MatchState::WaitingPostMatch)
		{
			EndGame();
//			EndGame(nullptr, FName(TEXT("LeavePlayers")));
		}
	}
}

bool AUTGameMode::ModifyDamage_Implementation(int32& Damage, FVector& Momentum, APawn* Injured, AController* InstigatedBy, const FHitResult& HitInfo, AActor* DamageCauser, TSubclassOf<UDamageType> DamageType)
{
	AUTCharacter* InjuredChar = Cast<AUTCharacter>(Injured);
	if (InjuredChar != nullptr && InjuredChar->bSpawnProtectionEligible && InstigatedBy != nullptr && InstigatedBy != Injured->Controller && GetWorld()->TimeSeconds - Injured->CreationTime < UTGameState->SpawnProtectionTime)
	{
		Damage = 0;
	}
	if (HasMatchStarted() && (!IsMatchInProgress()))
	{
		Damage = 0;
	}
	return true;
}

bool AUTGameMode::PreventDeath_Implementation(APawn* KilledPawn, AController* Killer, TSubclassOf<UDamageType> DamageType, const FHitResult& HitInfo)
{
	return false;// (BaseMutator != nullptr && BaseMutator->PreventDeath(KilledPawn, Killer, DamageType, HitInfo));
}

void AUTGameMode::SetWorldGravity(float NewGravity)
{
	AWorldSettings* Settings = GetWorld()->GetWorldSettings();
	Settings->bWorldGravitySet = true;
	Settings->WorldGravityZ = NewGravity;
}

bool AUTGameMode::ChangeTeam(AController* Player, uint8 NewTeam, bool bBroadcast)
{
	// By default, we don't do anything.
	return true;
}

TSubclassOf<AGameSession> AUTGameMode::GetGameSessionClass() const
{
	return AGameSession::StaticClass();
}

void AUTGameMode::ScoreObject_Implementation(AUTCarriedObject* GameObject, AUTCharacter* HolderPawn, AUTPlayerState* Holder, FName Reason)
{

}

void AUTGameMode::GetSeamlessTravelActorList(bool bToEntry, TArray<AActor*>& ActorList)
{
	Super::GetSeamlessTravelActorList(bToEntry, ActorList);
}


void AUTGameMode::ProcessServerTravel(const FString& URL, bool bAbsolute)
{
	if (GameSession != nullptr)
	{
		AUTGameSession* UTGameSession = Cast<AUTGameSession>(GameSession);
		if (UTGameSession != nullptr)
		{
			UTGameSession->UnRegisterServer(false);
		}
	}

	Super::ProcessServerTravel(URL, bAbsolute);
}

void AUTGameMode::BlueprintBroadcastLocalized( AActor* Sender, TSubclassOf<ULocalMessage> Message, int32 Switch, APlayerState* RelatedPlayerState_1, APlayerState* RelatedPlayerState_2, UObject* OptionalObject)
{
	BroadcastLocalized(Sender, Message, Switch, RelatedPlayerState_1, RelatedPlayerState_2, OptionalObject);
}

void AUTGameMode::BlueprintSendLocalized( AActor* Sender, AUTPlayerController* Receiver, TSubclassOf<ULocalMessage> Message, int32 Switch, APlayerState* RelatedPlayerState_1, APlayerState* RelatedPlayerState_2, UObject* OptionalObject)
{
	Receiver->ClientReceiveLocalizedMessage(Message, Switch, RelatedPlayerState_1, RelatedPlayerState_2, OptionalObject);
}

void AUTGameMode::BroadcastSpectator(AActor* Sender, TSubclassOf<ULocalMessage> Message, int32 Switch, APlayerState* RelatedPlayerState_1, APlayerState* RelatedPlayerState_2, UObject* OptionalObject)
{
	for (FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
	{
		APlayerController* PC = Iterator->Get();
		if (PC->GetPlayerState<AUTPlayerState>() != nullptr && PC->GetPlayerState<AUTPlayerState>()->bOnlySpectator)
		{
			PC->ClientReceiveLocalizedMessage(Message, Switch, RelatedPlayerState_1, RelatedPlayerState_2, OptionalObject);
		}
	}
}

void AUTGameMode::BroadcastSpectatorPickup(AUTPlayerState* PS, FName StatsName, UClass* PickupClass)
{
	if (PS != nullptr && PickupClass != nullptr && StatsName != NAME_None)
	{
		//0 will not show the pickup count numbers
		int32 Switch = 0;
		BroadcastSpectator(nullptr, UUTSpectatorPickupMessage::StaticClass(), Switch, PS, nullptr, PickupClass);
	}
}

void AUTGameMode::PrecacheAnnouncements(UUTAnnouncer* Announcer) const
{
	// slow but fairly reliable base implementation that looks up all local messages
	for (TObjectIterator<UClass> It; It; ++It)
	{
		if (It->IsChildOf(UUTLocalMessage::StaticClass()))
		{
			It->GetDefaultObject<UUTLocalMessage>()->PrecacheAnnouncements(Announcer);
		}
		if (It->IsChildOf(UUTDamageType::StaticClass()))
		{
			It->GetDefaultObject<UUTDamageType>()->PrecacheAnnouncements(Announcer);
		}
	}
}

void AUTGameMode::AssignDefaultSquadFor(AController* C)
{
	if (C != nullptr)
	{
		if (SquadType == nullptr)
		{
			UE_LOG(UT, Warning, TEXT("Game mode %s missing SquadType"), *GetName());
			SquadType = AUTSquadAI::StaticClass();
		}
		AUTPlayerState* PS = Cast<AUTPlayerState>(C->GetPlayerState<AUTPlayerState>());
		if (PS != nullptr && PS->Team != nullptr)
		{
			PS->Team->AssignDefaultSquadFor(C);
		}
		else
		{
			// default is to just spawn a squad for each individual
			AUTBot* B = Cast<AUTBot>(C);
			if (B != nullptr)
			{
				B->SetSquad(GetWorld()->SpawnActor<AUTSquadAI>(SquadType));
			}
		}
	}
}

void AUTGameMode::UpdatePlayersPresence()
{
	//bool bAllowJoin = (NumPlayers < GameSession->MaxPlayers);
	//
	//AUTGameSession* UTGameSession = Cast<AUTGameSession>(GameSession);
	//bool bNoJoinInProgress = UTGameSession ? UTGameSession->bNoJoinInProgress : false;
	//
	//bool bAllowInvites = !bPrivateMatch && (!bNoJoinInProgress || !UTGameState->HasMatchStarted());
	//bool bAllowJoinInProgress = !bNoJoinInProgress || !UTGameState->HasMatchStarted();
	//
	//UE_LOG(UT,Verbose,TEXT("UpdatePlayersPresence: AllowJoin: %i %i %i"), bAllowJoin, bAllowInvites, bAllowJoinInProgress);
	//
	//if (GameSession)
	//{
	//	const auto OnlineSub = IOnlineSubsystem::Get();
	//	if (OnlineSub)
	//	{
	//		const auto SessionInterface = OnlineSub->GetSessionInterface();
	//		if (SessionInterface.IsValid())
	//		{
	//			EOnlineSessionState::Type State = SessionInterface->GetSessionState(GameSessionName);
	//			if (State == EOnlineSessionState::Pending ||
	//				State == EOnlineSessionState::Starting ||
	//				State == EOnlineSessionState::InProgress)
	//			{
	//				GameSession->UpdateSessionJoinability(GameSessionName, true, bAllowInvites, true, false);
	//			}
	//		}
	//	}
	//}
	//
	//
	//UTGameState->bRestrictPartyJoin =  !bAllowJoinInProgress;
	//FString PresenceString = FText::Format(NSLOCTEXT("UTGameMode","PlayingPresenceFormatStr","Playing {0} on {1}"), DisplayName, FText::FromString(*GetWorld()->GetMapName())).ToString();
	//for( FConstControllerIterator Iterator = GetWorld()->GetControllerIterator(); Iterator; ++Iterator )
	//{
	//	AUTPlayerController* Controller = Cast<AUTPlayerController>(*Iterator);
	//	if (Controller)
	//	{
	//		Controller->ClientSetPresence(PresenceString, bAllowInvites, bAllowJoinInProgress, bAllowJoinInProgress, false);
	//	}
	//}
}


bool AUTGameMode::PlayerCanAltRestart_Implementation( APlayerController* Player )
{
	return PlayerCanRestart(Player);
}


void AUTGameMode::SetPlayerStateInactive(APlayerState* NewPlayerState)
{
	// bIsInactive needs to be set now as we are replicating right away
	NewPlayerState->bIsInactive = true;
}


bool AUTGameMode::CanBoost(AUTPlayerController* Who)
{
	if (Who && Who->GetPlayerState<AUTPlayerState>() && IsMatchInProgress() && (GetMatchState() != MatchState::MatchIntermission))
	{
		if (Who->GetPlayerState<AUTPlayerState>()->GetRemainingBoosts())
		{
			return true;
		}
	}

	return false;
}

bool AUTGameMode::TriggerBoost(AUTPlayerController* Who)
{
	return CanBoost(Who);
}

bool AUTGameMode::AttemptBoost(AUTPlayerController* Who)
{
	bool bCanBoost = CanBoost(Who);
	if (bCanBoost)
	{
		Who->GetPlayerState<AUTPlayerState>()->SetRemainingBoosts(Who->GetPlayerState<AUTPlayerState>()->GetRemainingBoosts() - 1);
	}
	return bCanBoost;
}

void AUTGameMode::SendComsMessage( AUTPlayerController* Sender, AUTPlayerState* Target, int32 Switch)
{
	AUTPlayerState* UTPlayerState = Cast<AUTPlayerState>(Sender->GetPlayerState<AUTPlayerState>());

	if (UTPlayerState != nullptr)
	{
		if (Target != nullptr)
		{
			// This is a targeting com message.  Send it only to the sender and the target
			AUTPlayerController* TargetPC = Cast<AUTPlayerController>(Target->GetOwner());
			if (TargetPC != nullptr) TargetPC->ClientReceiveLocalizedMessage(UTPlayerState->GetCharacterVoiceClass(), Switch, UTPlayerState, nullptr, UTPlayerState->LastKnownLocation);

			Sender->ClientReceiveLocalizedMessage(UTPlayerState->GetCharacterVoiceClass(), Switch, UTPlayerState, nullptr, UTPlayerState->LastKnownLocation);
		}
		else
		{
			for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
			{
				AUTPlayerController* UTPlayerController = Cast<AUTPlayerController>(It->Get());
				if (UTPlayerController != nullptr)
				{
					UTPlayerController->ClientReceiveLocalizedMessage(UTPlayerState->GetCharacterVoiceClass(), Switch, UTPlayerState, nullptr, UTPlayerState->LastKnownLocation);
				}
			}
		}
	}
}


int32 AUTGameMode::GetComSwitch(FName CommandTag, AActor* ContextActor, AUTPlayerController* InInstigator, UWorld* World)
{
	if (CommandTag == CommandTags::Yes)
	{
		return ACKNOWLEDGE_SWITCH_INDEX;
	}

	if (CommandTag == CommandTags::No)
	{
		return NEGATIVE_SWITCH_INDEX;
	}

	return INDEX_NONE;
}




bool AUTGameMode::ProcessConsoleExec(const TCHAR* Cmd, FOutputDevice& Ar, UObject* Executor)
{
	if (FParse::Command(&Cmd, TEXT("dumpsession")))
	{
		GLog->AddOutputDevice(&Ar);
		UE_SET_LOG_VERBOSITY(LogOnline, VeryVerbose);
		const IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
		if (OnlineSub)
		{
			const IOnlineSessionPtr SessionInt = OnlineSub->GetSessionInterface();
			if (SessionInt.IsValid())
			{
				SessionInt->DumpSessionState();
			}
		}
		UE_SET_LOG_VERBOSITY(LogOnline, Warning);
		GLog->RemoveOutputDevice(&Ar);
		return true;
	}
	else if (FParse::Command(&Cmd, TEXT("dumptime")))
	{
		Ar.Logf(TEXT("TimeSeconds: %f   RealTimeSeconds: %f    DeltaSeconds: %f"), GetWorld()->GetTimeSeconds(), GetWorld()->GetRealTimeSeconds(), GetWorld()->GetDeltaSeconds());
		return true;
	}
	return Super::ProcessConsoleExec(Cmd, Ar, Executor);

}



bool AUTGameMode::IsAllowJoinInProgress() const
{
	if (UTGameState)
	{
		return UTGameState->RemainingTime > FMath::CeilToInt(float(UTGameState->TimeLimit) / 2.0f);
	}
	
	return false;
}

void AUTGameMode::SendCheckAvalibleMember() const
{
#if UE_SERVER && !WITH_EDITOR
	if (GetMatchState() == MatchState::InProgress)
	{
		if (UTGameState && UTGameState->RemainingTime > 0 &&UTGameState->RemainingTime % 5 == 0)
		{
			if (const auto node = GetNodeComponent())
			{
				node->SendGetAvalibleMembersRequest(FMatchHeartBeatView(UTGameState->RemainingTime, IsAllowJoinInProgress()));
			}
		}
	}
#endif
}


AActor* AUTGameMode::FindPlayerStart_Implementation(AController* Player, const FString& IncomingName)
{
	AActor* const Best = Super::FindPlayerStart_Implementation(Player, IncomingName);
	if (Best)
	{
		LastStartSpot = Best;
	}

	return Best;
}

FString AUTGameMode::InitNewPlayer(APlayerController* NewPlayerController, const FUniqueNetIdRepl& UniqueId, const FString& Options, const FString& Portal)
{
	FString ErrorMessage = Super::InitNewPlayer(NewPlayerController, UniqueId, Options, Portal);
	return ErrorMessage;
}

AActor* AUTGameMode::ChoosePlayerStart_Implementation(AController* Player)
{
	AUTPlayerState* UTPS = Player != nullptr ? Cast<AUTPlayerState>(Player->GetPlayerState<AUTPlayerState>()) : NULL;
	AUTPlayerStart* BestStart = nullptr;
	TArray<AUTPlayerStart*> PlayerStarts;
	for (TActorIterator<AUTPlayerStart> It(GetWorld()); It; ++It)
	{
		PlayerStarts.Add(*It);
	}

	if (PlayerStarts.Num() == 0)
	{
		UE_LOG(UT, Error, TEXT("AUTGameMode::ChoosePlayerStart_Implementation -- PlayerStarts was empty"));
		return Super::ChoosePlayerStart_Implementation(Player);
	}

	float BestRate = .0f;
	for (auto ps : PlayerStarts)
	{
		if (AvoidPlayerStart(ps))
		{
			const float RatePS = RatePlayerStart(ps, Player);
			if (RatePS > BestRate)
			{
				BestRate = RatePS;
				BestStart = ps;
			}
		}
	}
	
	if (BestStart == nullptr)
	{
		UE_LOG(UT, Error, TEXT("AUTGameMode::ChoosePlayerStart_Implementation -- BestStart was nullptr"));
	}
	else
	{
		UE_LOG(UT, Error, TEXT("AUTGameMode::ChoosePlayerStart_Implementation -- BestStart %s [%s]"), *BestStart->GetFullName(), *BestStart->GetActorLocation().ToString());
	}
	return BestStart;// != nullptr) ? BestStart : Super::ChoosePlayerStart_Implementation(Player);
}

float AUTGameMode::RatePlayerStart(AUTPlayerStart* P, AController* Player)
{
	float Score = 29.0f + FMath::FRand();

	AActor* LastSpot = (Player != nullptr && Player->StartSpot.IsValid()) ? Player->StartSpot.Get() : NULL;
	AUTPlayerState *UTPS = Player ? Cast<AUTPlayerState>(Player->GetPlayerState<AUTPlayerState>()) : NULL;
	if (P == LastStartSpot || (LastSpot != nullptr && P == LastSpot))
	{
		// avoid re-using starts
		Score -= 15.0f;
	}
	FVector StartLoc = P->GetActorLocation() + AUTCharacter::StaticClass()->GetDefaultObject<AUTCharacter>()->BaseEyeHeight;

	if (Player != nullptr || Player->IsValidLowLevel())
	{
		for (FConstControllerIterator Iterator = GetWorld()->GetControllerIterator(); Iterator; ++Iterator)
		{
			AController* OtherController = Iterator->Get();
			ACharacter* OtherCharacter = Cast<ACharacter>(OtherController->GetPawn());

			if (OtherCharacter && OtherCharacter->GetPlayerState<AUTPlayerState>())
			{
				if (FMath::Abs(StartLoc.Z - OtherCharacter->GetActorLocation().Z) < P->GetCapsuleComponent()->GetScaledCapsuleHalfHeight() + OtherCharacter->GetCapsuleComponent()->GetScaledCapsuleHalfHeight()
					&& (StartLoc - OtherCharacter->GetActorLocation()).Size2D() < P->GetCapsuleComponent()->GetScaledCapsuleRadius() + OtherCharacter->GetCapsuleComponent()->GetScaledCapsuleRadius())
				{
					// overlapping - would telefrag
					return -10.f;
				}
				Score += AdjustNearbyPlayerStartScore(Player, OtherController, OtherCharacter, StartLoc, P);
			}
		}
	}

	if (UTPS && UTPS->SpawnAttempts >= FMath::RandRange(MinSpawnAttempts, MaxSpawnAttempts))
	{
		Score += (UTPS->SpawnAttempts > MaxSpawnAttempts) ? 50.0f : 15.0f;
	}

	return FMath::Max(Score, 0.2f);
}

float AUTGameMode::AdjustNearbyPlayerStartScore(const AController* Player, const AController* OtherController, const ACharacter* OtherCharacter, const FVector& StartLoc, const APlayerStart* P)
{
	float ScoreAdjust = 0.f;
	float NextDist = (OtherCharacter->GetActorLocation() - StartLoc).Size();
	bool bTwoPlayerGame = (NumPlayers + NumBots == 2);

	if (((NextDist < 8000.0f) || bTwoPlayerGame) && !UTGameState->OnSameTeam(Player, OtherController))
	{
		static FName NAME_RatePlayerStart = FName(TEXT("RatePlayerStart"));
		bool bIsLastKiller = (Cast<AUTPlayerState>(Player->GetPlayerState<AUTPlayerState>()) && OtherCharacter->GetPlayerState<AUTPlayerState>() == Cast<AUTPlayerState>(Player->GetPlayerState<AUTPlayerState>())->LastKillerPlayerState);
		if (!GetWorld()->LineTraceTestByChannel(StartLoc, OtherCharacter->GetActorLocation() + FVector(0.f, 0.f, OtherCharacter->GetCapsuleComponent()->GetScaledCapsuleHalfHeight()), ECC_Visibility, FCollisionQueryParams(NAME_RatePlayerStart, false)))
		{
			// Avoid the last person that killed me
			if (bIsLastKiller)
			{
				ScoreAdjust -= 7.f;
			}

			ScoreAdjust -= (5.f - 0.0003f * NextDist);
		}
		else if (NextDist < 4000.0f)
		{
			// Avoid the last person that killed me
			ScoreAdjust -= bIsLastKiller ? 5.f : 0.0005f * (5000.f - NextDist);

			if (!GetWorld()->LineTraceTestByChannel(StartLoc, OtherCharacter->GetActorLocation(), ECC_Visibility, FCollisionQueryParams(NAME_RatePlayerStart, false, this)))
			{
				ScoreAdjust -= 2.f;
			}
		}
	}
	return ScoreAdjust;
}

bool AUTGameMode::AvoidPlayerStart(AUTPlayerStart* InPlayerStart)
{
	if (UTGameState == nullptr || UTGameState->IsValidLowLevel() == false)
	{
		UE_LOG(UT, Error, TEXT("AUTGameMode::AvoidPlayerStart -- UTGameState was nullptr"));
		return false;
	}

	if (UTGameState->GetGameModeType() == EGameMode::None)
	{
		UE_LOG(UT, Error, TEXT("AUTGameMode::AvoidPlayerStart -- CurrentGameMode is None"));
		return false;
	}

	bool isSupported = (FFlagsHelper::HasAnyFlags(InPlayerStart->AllowGameModes, UTGameState->GetGameModeType().Value) || InPlayerStart->AllowGameModes == 0);
	UE_LOG(UT, Error, TEXT("AUTGameMode::AvoidPlayerStart -- CurrentGameMode is %d [%s] | SupportGameModes: %d [%d]"), UTGameState->GetGameModeType().Value, *UTGameState->GetGameModeType().ToString(), InPlayerStart->SupportGameModes, isSupported);
	
	return isSupported;
}

bool AUTGameMode::PlayerCanRestart_Implementation(APlayerController* Player)
{
	//=========================================================================
	if (Player == nullptr || Player->IsValidLowLevel() == false)
	{
		//UE_LOG(UT, Error, TEXT("AUTGameMode::PlayerCanRestart_Implementation -- Player was nullptr | match state: %s"), *GetMatchState().ToString());
		return false;
	}

	//=========================================================================
	if(Player->IsPendingKillPending())
	{
		if (Player->GetPlayerState<AUTPlayerState>())
		{
			//UE_LOG(UT, Error, TEXT("AUTGameMode::PlayerCanRestart_Implementation -- Player %s[%s] IsPendingKillPending| match state: %s"), *Player->GetFullName(), *Player->GetPlayerState<AUTPlayerState>()->GetPlayerName(), *GetMatchState().ToString());
		}
		else
		{
			//UE_LOG(UT, Error, TEXT("AUTGameMode::PlayerCanRestart_Implementation -- Player %s[null PlayerState] IsPendingKillPending| match state: %s"), *Player->GetFullName(), *GetMatchState().ToString());
		}
		return false;
	}

	//=========================================================================
	if (Player->GetPlayerState<AUTPlayerState>())
	{
		//UE_LOG(UT, Error, TEXT("AUTGameMode::PlayerCanRestart_Implementation -- Player %s[%s] IsMatchInProgress: %d| match state: %s"), *Player->GetFullName(), *Player->GetPlayerState<AUTPlayerState>()->GetPlayerName(), IsMatchInProgress(), *GetMatchState().ToString());
	}
	else
	{
		//UE_LOG(UT, Error, TEXT("AUTGameMode::PlayerCanRestart_Implementation -- UTPC was nullptr| match state: %s"), *Player->GetFullName(), IsMatchInProgress(), *GetMatchState().ToString());
	}

	//=========================================================================
	if (IsMatchInProgress() == false)
	{
		AUTPlayerController* UTPC = Cast<AUTPlayerController>(Player);
		if (!UTPC || (GetMatchState() != MatchState::WaitingToStart) || !UTPC->GetPlayerState<AUTPlayerState>() || !UTPC->GetPlayerState<AUTPlayerState>()->bIsWarmingUp)
		{
			return false;
		}
	}

	//=========================================================================
	// Ask the player controller if it's ready to restart as well
	const bool canRestartPlayer = Player->CanRestartPlayer();
	{
		if (Player->GetPlayerState<AUTPlayerState>())
		{
			//UE_LOG(UT, Error, TEXT("AUTGameMode::PlayerCanRestart_Implementation -- Player %s[%s] IsMatchInProgress: %d| match state: %s | canRestartPlayer: %d"), *Player->GetFullName(), *Player->GetPlayerState<AUTPlayerState>()->GetPlayerName(), IsMatchInProgress(), *GetMatchState().ToString(), canRestartPlayer);
		}
		else
		{
			//UE_LOG(UT, Error, TEXT("AUTGameMode::PlayerCanRestart_Implementation -- UTPC was nullptr| match state: %s | canRestartPlayer: %d"), *Player->GetFullName(), IsMatchInProgress(), *GetMatchState().ToString(), canRestartPlayer);
		}
	}
	return canRestartPlayer;
}

void AUTGameMode::RestartPlayer(AController* aPlayer)
{
	if (aPlayer == nullptr || aPlayer->IsValidLowLevel() == false)
	{
		return;
	}

	if (GetMatchState() == MatchState::WaitingPostMatch)
	{
		return;
	}

	if (auto bot = Cast<AUTBot>(aPlayer))
	{
		bot->LastRespawnTime = GetWorld()->TimeSeconds;
	}

	if (auto playerState = Cast<AUTPlayerState>(aPlayer->GetPlayerState<AUTPlayerState>()))
	{
		// clear multikill in progress
		playerState->LastKillTime = -100.f;
	}

	auto TeamStart = Cast<APlayerStart>(FindPlayerStart(aPlayer));

	if (TeamStart)
	{
		if (auto MyPlayerState = Cast<AUTPlayerState>(aPlayer->GetPlayerState<AUTPlayerState>()))
		{
			//UE_LOG(UT, Error, TEXT("AUTGameMode::RestartPlayer -- spawn point at %s [%s], player: %s | %s | attemp: %d of %d[%d]"), *TeamStart->GetFullName(), *TeamStart->GetActorLocation().ToString(), *aPlayer->GetFullName(), *MyPlayerState->GetPlayerName(), MyPlayerState->SpawnAttempts, MinSpawnAttempts, MaxSpawnAttempts);
			MyPlayerState->SpawnAttempts = 0;
		}
		Super::RestartPlayer(aPlayer);
	}
	else
	{
		if (auto MyPlayerState = Cast<AUTPlayerState>(aPlayer->GetPlayerState<AUTPlayerState>()))
		{
			++MyPlayerState->SpawnAttempts;

			FTimerHandle _thtmp;
			GetWorldTimerManager().SetTimer(_thtmp, FTimerDelegate::CreateUObject(this, &AUTGameMode::RestartPlayer, aPlayer), FMath::FRandRange(.8f, 2.0f), false);

			UE_LOG(UT, Error, TEXT("AUTGameMode::RestartPlayer -- Not found spawn point for, player: %s | %s | attemp: %d of %d[%d]"), *aPlayer->GetFullName(), *MyPlayerState->GetPlayerName(), MyPlayerState->SpawnAttempts, MinSpawnAttempts, MaxSpawnAttempts);
		}
		else
		{
			Super::RestartPlayer(aPlayer);
			UE_LOG(UT, Error, TEXT("AUTGameMode::RestartPlayer -- Not found spawn point for, player: %s | null player state"), *aPlayer->GetFullName());
		}
	}
}

bool AUTGameMode::FindInactivePlayer(APlayerController* PC)
{
	const bool Result = Super::FindInactivePlayer(PC);
	if (Result == false)
	{
		if (UTGameState && PC->GetPlayerState<AUTPlayerState>())
		{
			auto MyPlayerState = Cast<AUTPlayerState>(PC->GetPlayerState<AUTPlayerState>());
			if (MyPlayerState && MyPlayerState->MemberId.IsValid())
			{
				for (auto ps : UTGameState->PlayerArray)
				{
					if (auto OtherPlayerState = Cast<AUTPlayerState>(ps))
					{
						if (OtherPlayerState->MemberId.IsValid() && OtherPlayerState->MemberId == MyPlayerState->MemberId)
						{
							OtherPlayerState->CopyProperties(MyPlayerState);
							return true;
						}
					}
				}
			}
		}
	}

	return Result;
}