// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "OnlineGameMode.h"
#include "TAttributeProperty.h"

#include "UTAntiCheatModularFeature.h"
#include "UTGameMode.generated.h"
class UUTLocalPlayer;
class UUTBotCharacter;
class AUTBasePlayerController;
class AUTGameObjective;
class AUTGameState;

LOKAGAME_API DECLARE_LOG_CATEGORY_EXTERN(LogUTGame, Log, All);

class AUTPlayerController;

/** list of bots user asked to put into the game */
USTRUCT()
struct FSelectedBot
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	FStringAssetReference BotAsset;
	/** team to place them on - note that 255 is valid even for team games (placed on any team) */
	UPROPERTY()
	uint8 Team;

	FSelectedBot()
		: Team(255)
	{}
	FSelectedBot(const FStringAssetReference& InAssetPath, uint8 InTeam)
		: BotAsset(InAssetPath), Team(InTeam)
	{}
};

#if !UE_SERVER
class SUTHUDWindow;
#endif

UCLASS(Config = Game, Abstract)
class LOKAGAME_API AUTGameMode : public AOnlineGameMode
{
  GENERATED_BODY()


public:
  AUTGameMode();

	virtual bool MovePlayerToTeam(AController* Player, AUTPlayerState* PS, uint8 NewTeam) { return false; }

	UPROPERTY()
	FText DisplayName;

	/** Cached reference to our game state for quick access. */
	UPROPERTY()
	AUTGameState* UTGameState;		

	/** If true, allow announcements for pickup spawn. */
	UPROPERTY(EditDefaultsOnly, Category=Game)
		bool bAllowPickupAnnouncements;

	/** If true, allow armor pickup even if player can't use it. */
	UPROPERTY(EditDefaultsOnly, Category = Game)
		bool bAllowAllArmorPickups;


	/** Will be TRUE if this is a team game */
	UPROPERTY()
	uint32 bTeamGame:1;

	UPROPERTY(BlueprintReadWrite, Category = "Game")
	bool bFirstBloodOccurred;

	/** World time when match was first ready to start. */
	UPROPERTY()
	float StartPlayTime;

	/** # of seconds before the match begins */
	UPROPERTY()
	int32 CountDown;

	/** Holds the last place any player started from */
	UPROPERTY()
	class AActor* LastStartSpot;    // last place any player started from

	/** Timestamp of when this game ended */
	UPROPERTY()
	float EndTime;

	/** whether weapon stay is active */
	UPROPERTY(EditDefaultsOnly, Category = "Game")
	bool bWeaponStayActive;

	/** Which actor in the game should all other actors focus on after the game is over */
	UPROPERTY()
	class AActor* EndGameFocus;

	UPROPERTY(EditDefaultsOnly, Category = Game)
	TSubclassOf<class UUTLocalMessage> GameMessageClass;

	UPROPERTY(EditDefaultsOnly, Category = Game)
	TSubclassOf<class UUTLocalMessage> VictoryMessageClass;

	/** Last time asnyone sent a taunt voice message. */
	UPROPERTY()
	float LastGlobalTauntTime;


	/** Whether player is allowed to suicide. */
	virtual bool AllowSuicideBy(AUTPlayerController* PC);


	virtual void PostInitProperties()
	{
		Super::PostInitProperties();
		if (DisplayName.IsEmpty() || (GetClass() != AUTGameMode::StaticClass() && DisplayName.EqualTo(GetClass()->GetSuperClass()->GetDefaultObject<AUTGameMode>()->DisplayName)))
		{
			DisplayName = FText::FromName(GetClass()->GetFName());
		}
	}

	/** class used for AI bots */
	UPROPERTY(EditAnywhere, NoClear, BlueprintReadWrite, Category = Classes)
	TSubclassOf<class AUTBot> BotClass;

	/** cached list of UTBotCharacter assets from the asset registry, so we don't need to query the registry every time we add a bot */
	TArray<FAssetData> BotAssets;

	/** Sorted array of remaining eligible bot characters to select from. */
	UPROPERTY()
	TArray<UUTBotCharacter*> EligibleBots;

	/** type of SquadAI that contains game specific AI logic for this gametype */
	UPROPERTY(EditDefaultsOnly, Category = AI)
	TSubclassOf<class AUTSquadAI> SquadType;
	/** maximum number of players per squad (except human-led squad if human forces bots to follow) */
	UPROPERTY(EditDefaultsOnly, Category = AI)
	int32 MaxSquadSize;

	/** whether to record a demo (starts when the countdown starts) */
	UPROPERTY(GlobalConfig)
	bool bRecordDemo;

	/** filename for demos... should use one of the replacement strings or it'll overwrite every game */
	UPROPERTY(GlobalConfig)
	FString DemoFilename;

	UPROPERTY()
		float EndOfMatchMessageDelay;

	/** If true remove any pawns in the level when match starts, (normally true to clean up after warm up). 
	    Pawns will not be removed if in standalone or PIE. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Game")
		bool bRemovePawnsAtStart;

	/** assign squad to player - note that humans can have a squad for bots to follow their lead
	 * this method should always result in a valid squad being assigned
	 */
	virtual void AssignDefaultSquadFor(AController* C);


	virtual void InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage) override;
	UFUNCTION(BlueprintImplementableEvent)
	void PostInitGame(const FString& Options);
	/** add a mutator by string path name */
	virtual void InitGameState() override;
	virtual APlayerController* Login(class UPlayer* NewPlayer, ENetRole RemoteRole, const FString& Portal, const FString& Options, const FUniqueNetIdRepl& UniqueId, FString& ErrorMessage) override;
	virtual void RemoveAllPawns();
	virtual void RestartGame();
	virtual void BeginGame();
	virtual bool AllowPausing(APlayerController* PC) override;
	virtual bool AllowCheats(APlayerController* P) override;
	virtual bool IsEnemy(class AController* First, class AController* Second);
	virtual void Killed(class AController* Killer, class AController* KilledPlayer, class APawn* KilledPawn, TSubclassOf<UDamageType> DamageType);
	virtual void NotifyKilled(AController* Killer, AController* Killed, APawn* KilledPawn, TSubclassOf<UDamageType> DamageType);

	UFUNCTION(BlueprintNativeEvent, BlueprintAuthorityOnly)
	void ScorePickup(AUTPickup* Pickup, AUTPlayerState* PickedUpBy, AUTPlayerState* LastPickedUpBy);

	UFUNCTION(BlueprintNativeEvent, BlueprintAuthorityOnly)
		void ScoreDamage(int32 DamageAmount, AUTPlayerState* Victim, AUTPlayerState* Attacker);

	UFUNCTION(BlueprintNativeEvent, BlueprintAuthorityOnly)
	void ScoreKill(AController* Killer, AController* Other, APawn* KilledPawn, TSubclassOf<UDamageType> DamageType);

	TMap<TSubclassOf<UDamageType>, int32> EnemyKillsByDamageType;

	/** Score teammate killing another teammate. */
	UFUNCTION(BlueprintNativeEvent, BlueprintAuthorityOnly)
	void ScoreTeamKill(AController* Killer, AController* Other, APawn* KilledPawn, TSubclassOf<UDamageType> DamageType);

	UFUNCTION(BlueprintNativeEvent, BlueprintAuthorityOnly)
	void ScoreObject(AUTCarriedObject* GameObject, AUTCharacter* HolderPawn, AUTPlayerState* Holder, FName Reason);

	UFUNCTION(BlueprintNativeEvent, BlueprintAuthorityOnly)
	bool CheckScore(AUTPlayerState* Scorer);

	virtual void FindAndMarkHighScorer();
	virtual void SetEndGameFocus(AUTPlayerState* Winner);	

	UFUNCTION(BlueprintCallable, Category = UTGame)
	virtual void EndGame();

	void StartMatch() override;
	void EndMatch() override;
	virtual void PlayEndOfMatchMessage();

	UFUNCTION(BlueprintCallable, Category = UTGame)
	virtual void DiscardInventory(APawn* Other, AController* Killer = NULL);

	void OverridePlayerState(APlayerController* PC, APlayerState* OldPlayerState) override;
	void PostLogin( APlayerController* NewPlayer ) override;
	void Logout(AController* Exiting) override;
	void RestartPlayer(AController* aPlayer) override;
	bool PlayerCanRestart_Implementation(APlayerController* Player) override;


	FString InitNewPlayer(class APlayerController* NewPlayerController, const FUniqueNetIdRepl& UniqueId, const FString& Options, const FString& Portal = TEXT("")) override;

	virtual float OverrideRespawnTime(TSubclassOf<AUTInventory> InventoryType);

	/** Return true if playerstart P should be avoided for this game mode. */
	virtual bool AvoidPlayerStart(class AUTPlayerStart* InPlayerStart);

	virtual void HandleStartingNewPlayer_Implementation(APlayerController* NewPlayer) override;
	virtual bool ShouldSpawnAtStartSpot(AController* Player) override;
	virtual class AActor* FindPlayerStart_Implementation(AController* Player, const FString& IncomingName = TEXT("")) override;
	virtual AActor* ChoosePlayerStart_Implementation(AController* Player) override;
	virtual float RatePlayerStart(AUTPlayerStart* P, AController* Player);
	virtual float AdjustNearbyPlayerStartScore(const AController* Player, const AController* OtherController, const ACharacter* OtherCharacter, const FVector& StartLoc, const APlayerStart* P);

	virtual bool ReadyToStartMatch_Implementation() override;

	virtual void SetMatchState(FName NewState) override;
	virtual void CallMatchStateChangeNotify();


	void HandleMatchHasStarted() override;
	virtual void AnnounceMatchStart();
	virtual void HandleMatchHasEnded() override;
	virtual void HandleEnteringOvertime();
	virtual void HandleMatchInOvertime();

	virtual void StopReplayRecording();

	virtual void DefaultTimer() override;

	/**  Used to check when time has run out if there is a winner.  If there is a tie, return NULL to enter overtime. **/	
	UFUNCTION(BlueprintNativeEvent, BlueprintAuthorityOnly)
	AUTPlayerState* IsThereAWinner(bool& bTied);


	void AddKillEventToReplay(AController* Killer, AController* Other, TSubclassOf<UDamageType> DamageType);
	void AddMultiKillEventToReplay(AController* Killer, int32 MultiKillLevel);
	void AddSpreeKillEventToReplay(AController* Killer, int32 SpreeLevel);
	
	UPROPERTY(config)
	bool bRecordReplays;

	/** The standard IsHandlingReplays() codepath is not flexible enough for UT, this is the compromise */
	virtual bool UTIsHandlingReplays();

protected:

	/** Returns random bot character skill matched to current GameDifficulty. */
	virtual UUTBotCharacter* ChooseRandomCharacter(uint8 TeamNum);

	/** Adds a bot to the game */
	virtual class AUTBot* AddBot(uint8 TeamNum = 255);

	/** check for adding/removing bots to satisfy BotFillCount */
	virtual void CheckBotCount();
	/** returns whether we should allow removing the given bot to satisfy the desired player/bot count settings
	 * generally used to defer destruction of bots that currently are important to the current game state, like flag carriers
	 */
	virtual bool AllowRemovingBot(AUTBot* B);

	/** give bot a unique name based on the possible names in the given BotData */
	virtual void SetUniqueBotName(AUTBot* B, const class UUTBotCharacter* BotData);
public:

	/** NOTE: return value is a workaround for blueprint bugs involving ref parameters and is not used */
	UFUNCTION(BlueprintNativeEvent)
	bool ModifyDamage(UPARAM(ref) int32& Damage, UPARAM(ref) FVector& Momentum, APawn* Injured, AController* InstigatedBy, const FHitResult& HitInfo, AActor* DamageCauser, TSubclassOf<UDamageType> DamageType);

	/** return true to prevent the passed in pawn from dying (i.e. from Died()) */
	UFUNCTION(BlueprintNativeEvent)
	bool PreventDeath(APawn* KilledPawn, AController* Killer, TSubclassOf<UDamageType> DamageType, const FHitResult& HitInfo);

	/** changes world gravity to the specified value */
	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly, Category = World)
	void SetWorldGravity(float NewGravity);

	/** set or change a player's team
	 * NewTeam is a request, not a guarantee (game mode may force balanced teams, for example)
	 */
	UFUNCTION(BlueprintCallable, Category = TeamGame)
	virtual bool ChangeTeam(AController* Player, uint8 NewTeam = 255, bool bBroadcast = true);
	/** pick the best team to place this player to keep the teams as balanced as possible
	 * passed in team number is used as tiebreaker if the teams would be just as balanced either way
	 */

	virtual uint8 GetTeamIndexById(const FGuid& teamId)
	{
		return 255;
	}

	virtual TSubclassOf<class AGameSession> GetGameSessionClass() const;
	
	virtual void PreInitializeComponents() override;

	virtual void GameObjectiveInitialized(AUTGameObjective* Obj);

	virtual void GetSeamlessTravelActorList(bool bToEntry, TArray<AActor*>& ActorList) override;


	void ProcessServerTravel(const FString& URL, bool bAbsolute = false) override;

	UFUNCTION(BlueprintCallable, Category = Messaging)
	virtual void BlueprintBroadcastLocalized( AActor* Sender, TSubclassOf<ULocalMessage> Message, int32 Switch = 0, APlayerState* RelatedPlayerState_1 = NULL, APlayerState* RelatedPlayerState_2 = NULL, UObject* OptionalObject = NULL );

	UFUNCTION(BlueprintCallable, Category = Messaging)
	virtual void BlueprintSendLocalized( AActor* Sender, AUTPlayerController* Receiver, TSubclassOf<ULocalMessage> Message, int32 Switch = 0, APlayerState* RelatedPlayerState_1 = NULL, APlayerState* RelatedPlayerState_2 = NULL, UObject* OptionalObject = NULL );

	/**Broadcasts a localized message only to spectators*/
	UFUNCTION(BlueprintCallable, Category = Messaging)
	virtual void BroadcastSpectator(AActor* Sender, TSubclassOf<ULocalMessage> Message, int32 Switch, APlayerState* RelatedPlayerState_1, APlayerState* RelatedPlayerState_2, UObject* OptionalObject);

	/**Sends a pickup message to all spectators*/
	virtual void BroadcastSpectatorPickup(AUTPlayerState* PS, FName StatsName, UClass* PickupClass);

	/** called on the default object of the game class being played to precache announcer sounds
	 * needed because announcers are dynamically loaded for convenience of user announcer packs, so we need to load up the audio we think we'll use at game time
	 */
	virtual void PrecacheAnnouncements(class UUTAnnouncer* Announcer) const;

	/** OverridePickupQuery()
	* when pawn wants to pickup something, mutators are given a chance to modify it. If this function
	* returns true, bAllowPickup will determine if the object can be picked up.
	* Note that overriding bAllowPickup to false from this function without disabling the item in some way will have detrimental effects on bots,
	* since the pickup's AI interface will assume the default behavior and keep telling the bot to pick the item up.
	* @param Other the Pawn that wants the item
	* @param Pickup the Actor containing the item (this may be a UTPickup or it may be a UTDroppedPickup)
	* @param bAllowPickup (out) whether or not the Pickup actor should give its item to Other
	* @return whether or not to override the default behavior with the value of bAllowPickup
	*/
	UFUNCTION(BlueprintNativeEvent, BlueprintAuthorityOnly)
	bool OverridePickupQuery(APawn* Other, TSubclassOf<AUTInventory> ItemClass, AActor* Pickup, bool& bAllowPickup);

	virtual TSubclassOf<class AUTInventory> GetActivatedPowerupPlaceholderClass() { return nullptr; };

protected:

	UPROPERTY(transient)	FTimerHandle ServerShutdownTimerHandle;
	FTimerDelegate ServerShutdownTimerDelegate;

	UPROPERTY(transient) FTimerHandle ServerRestartTimerHandle;

	
	// When players leave/join or during the end of game state
	virtual void UpdatePlayersPresence();
	
	UPROPERTY()
	int32 ExpectedPlayerCount;

	private:
		// Never allow the engine to start replays, we'll do it manually
		bool IsHandlingReplays() override { return false; }

public:
	// Returns the # of players in this game.  By Default returns NumPlayers but can be overrride in children (like the HUBs)
	virtual int32 GetNumPlayers()
	{
		return NumPlayers;
	}

	// Returns the # of matches assoicated with this game type.  Typical returns 1 (this match) but HUBs will return all of their active matches
	virtual int32 GetNumMatches()
	{
		return 1;
	}

public:

#if WITH_EDITOR
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override
	{
		if (DefaultPawnClass != nullptr && PropertyChangedEvent.Property != nullptr && PropertyChangedEvent.Property->GetFName() == FName(TEXT("DefaultPawnClass")))
		{
			PlayerPawnObject = DefaultPawnClass;
		}
		Super::PostEditChangeProperty(PropertyChangedEvent);
	}
#endif


	//==========================================================================================================================================

	// note that this is not visible because in the editor we will still have the user set DefaultPawnClass, see PostEditChangeProperty()
	// in C++ constructors, you should set this as it takes priority in InitGame()
	UPROPERTY()
		TAssetSubclassOf<APawn> PlayerPawnObject;


public:

	/** Handle console exec commands */
	virtual bool ProcessConsoleExec(const TCHAR* Cmd, FOutputDevice& Ar, UObject* Executor);

	/** Build a JSON object that contains information about this game mode. */
	virtual bool IsAllowJoinInProgress() const override;
	virtual void SendCheckAvalibleMember() const override;

	// Called when the player attempts to restart using AltFire
	UFUNCTION(BlueprintNativeEvent, Category="Game")
	bool PlayerCanAltRestart( APlayerController* Player );


	/** Maximum time client can be ahead, without resetting. */
	UPROPERTY(GlobalConfig)
	float MaxTimeMargin;

	/** Maximum time client can be behind. */
	UPROPERTY(GlobalConfig)
	float MinTimeMargin;

	/** Accepted drift in clocks. */
	UPROPERTY(GlobalConfig)
	float TimeMarginSlack;

	/** Whether speedhack detection is enabled. */
	UPROPERTY(GlobalConfig)
	bool bSpeedHackDetection;

	virtual void NotifySpeedHack(ACharacter* Character);

	/** Overriden so we dont go into MatchState::LeavingMap state, which happens regardless if the travel fails
	* On failed map changes, the game will be stuck in a LeavingMap state
	*/
	virtual void StartToLeaveMap() {}

	/** Change playerstate properties as needed when becoming inactive. */
	virtual void SetPlayerStateInactive(APlayerState* NewPlayerState);

	/**Overridden to replicate Inactive Player States  */
	//virtual void AddInactivePlayer(APlayerState* PlayerState, APlayerController* PC) override;

private:
	UTAntiCheatModularFeature* AntiCheatEngine;

public:

	// Returns true if a player can activate a boost
	virtual bool CanBoost(AUTPlayerController* Who);

	/** PlayerController wants to trigger boost, return true if he can handle it. Mutator hooks as well. */
	virtual bool TriggerBoost(AUTPlayerController* Who);

	// Look to see if we can attempt a boost.  It will use up the charge if we can and return true, otherwise it returns false.
	virtual bool AttemptBoost(AUTPlayerController* Who);

	/**
	 *	Calculates the Com menu switch for a command command tag.
	 *
	 *  CommandTag - The tag we are looking for
	 *  ContextActor - The actor that is the current focus of the crosshair
	 *  Instigator - The Player Controller that owns the menu
	 **/
	virtual int32 GetComSwitch(FName CommandTag, AActor* ContextActor, AUTPlayerController* Instigator, UWorld* World);

	/**
	 *	Sends a voice communication message.  If Target is not null, then it will only be sent to the target and the sender, otherwise all of the players (or just
	 *  their teammates in a team game) will get the message.
	 * 
	 *  Sender is the controller sending this message.
	 *  Target is the player state of player who was the context target of this message.
	 *  Switch is the UTCharacterVoice switch to play
	 *  ContextObject is the object that 
	 **/
	virtual void SendComsMessage( AUTPlayerController* Sender, AUTPlayerState* Target, int32 Switch = 0);

	/** Holds the tutorial mask to set when this game completes.  */
	UPROPERTY(BlueprintReadOnly, Category=Onboarding)
	int32 TutorialMask;

	// ========================================[ LOKA

public:

	UPROPERTY(config)
	int32 KillScore;

	UPROPERTY(config)
	int32 DeathScore;

	int32 MatchWinner;

	UPROPERTY(config)
	int32 MinSpawnAttempts;

	UPROPERTY(config)
	int32 MaxSpawnAttempts;

	UPROPERTY(config)
	int32 Time_WaitingPlayers;

	UPROPERTY(config)
	int32 Time_WaitingStartMatch;

	virtual void DetermineMatchWinner() {}
	virtual void AddExtraResult(AUTPlayerState* PlayerState);

#if WITH_EDITORONLY_DATA
protected:

	UPROPERTY(config)
	int32 DeveloperTeamNum;

#endif

protected:

	// override this for search on active states, if client crashed player was kicked over 60 seconds
	virtual bool FindInactivePlayer(APlayerController* PC) override;
};

