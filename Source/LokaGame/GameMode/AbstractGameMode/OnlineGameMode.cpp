﻿#include "LokaGame.h"
#include "OnlineGameMode.h"
#include "OnlineGameState.h"
#include "NodeComponent.h"
#include "NodeComponent/MatchEndResult.h"
#include "CapturePoint.h"
#include "IdentityComponent.h"
#include "UTGameMode.h"
#include "GameSingleton.h"
#include "GamemodeOptions.h"
#include "NodeComponent/ConfirmMemberContainer.h"
#include "RequestManager/RequestManager.h"
#include "BasePlayerController.h"
#include "Messages/BaseLocalMessage.h"
#include "ShooterGame_CapturePoint.h"

AOnlineGameMode::AOnlineGameMode()
	: Super()
{
	GameStateClass = AOnlineGameState::StaticClass();
}


void AOnlineGameMode::EndMatch()
{
	Super::EndMatch();

	SendEndMatchRequest(FMatchEndResult());
}

void AOnlineGameMode::InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage)
{
	Super::InitGame(MapName, Options, ErrorMessage);

	//=============================================================
	UE_LOG(LogInit, Display, TEXT("==============="));
	UE_LOG(LogInit, Display, TEXT("  Init Game Option: %s | Map: %s"), *Options, *MapName);

	//=============================================================
}

void AOnlineGameMode::InitGameState()
{
	Super::InitGameState();

	if (!HasAnyFlags(RF_ClassDefaultObject | RF_ArchetypeObject))
	{
		//=============================================================
		if (auto gs = GetValidGameState<AGameStateBase>())
		{
			UE_LOG(LogInit, Display, TEXT("  [GameState] Name: %s | FullName: %s"), *gs->GetName(), *gs->GetFullName());
		}
		else
		{
			UE_LOG(LogInit, Fatal, TEXT("> AOnlineGameMode::PreInitializeComponents :: GameState was nullptr"));
		}

		//=============================================================
		if (auto gs = GetValidGameState<AOnlineGameState>())
		{
			UE_LOG(LogInit, Display, TEXT("  [OnlineGameState::Token]: %s"), *(gs->GetSessionToken().Get().ToString()));
		}
		else
		{
			UE_LOG(LogInit, Fatal, TEXT("> AOnlineGameMode::PreInitializeComponents :: OnlineGameState was nullptr"));
		}


		//=============================================================
		if (auto node = GetNodeComponent())
		{
			node->GetAvalibleMembersRequest->BindArray<TArray<FNodeSessionMember>>(200).AddUObject(this, &AOnlineGameMode::OnGetAvalibleMembers);

			if (WITH_EDITOR && this->IsA<AUTGameMode>())
			{
				auto FoundOptions = UGameSingleton::Get()->GetDataAssets<UGamemodeOptions>();

				for (auto MyOptions : FoundOptions)
				{
					if (MyOptions->TargetGameMode.Get() == GetClass())
					{
						// Подмена настроек
						node->OnSimulatedInformation(MyOptions->Options);
						break;
					}
				}				
			}
			else
			{
				node->BeginPoolingMatchInformation();
			}
		}
		else
		{
			UE_LOG(LogInit, Fatal, TEXT("> AOnlineGameMode::PreInitializeComponents :: NodeComponent was nullptr"));
		}
	}
}


void AOnlineGameMode::PreInitializeComponents()
{
	Super::PreInitializeComponents();

	//=============================================================
	if (!HasAnyFlags(RF_ClassDefaultObject | RF_ArchetypeObject))
	{
		if(auto w = GetValidWorld())
		{
			w->GetTimerManager().SetTimer(TimerHandle_DefaultTimer, this, &AOnlineGameMode::DefaultTimer, GetWorldSettings()->GetEffectiveTimeDilation() / GetWorldSettings()->DemoPlayTimeDilation, true);
		}
	}
}

void AOnlineGameMode::DefaultTimer()
{
	SendCheckAvalibleMember();
}

void AOnlineGameMode::SendEndMatchRequest(const FMatchEndResult& result)
{
	//=============================================================
	if (const auto node = GetNodeComponent())
	{
		node->SendCompleteMatchRequest(result);
	}
	else
	{
		UE_LOG(LogInit, Fatal, TEXT("> SendGetAvalibleMembersRequest :: NodeComponent was nullptr"));
	}
}

bool AOnlineGameMode::IsAllowJoinInProgress() const
{
	return true;
}

void AOnlineGameMode::SendCheckAvalibleMember() const
{
#if UE_SERVER && !WITH_EDITOR
	if (GetMatchState() == MatchState::InProgress)
	{
		if (const auto node = GetNodeComponent())
		{
			node->SendGetAvalibleMembersRequest(FMatchHeartBeatView(10000, IsAllowJoinInProgress()));
		}
		else
		{
			UE_LOG(LogInit, Error, TEXT("> SendCheckAvalibleMember :: NodeComponent was nullptr"));
		}
	}
#endif
}

void AOnlineGameMode::OnGetAvalibleMembers(const TArray<FNodeSessionMember>& members)
{

#if UE_SERVER && !WITH_EDITOR
	UE_LOG(LogInit, Error, TEXT("> OnGetAvalibleMembers :: members: %d"), members.Num());

	if (members.Num())
	{
		TArray<FConfirmMemberContainer> ConfirmedMembers;

		for (auto m : members)
		{
			FConfirmMemberContainer cm;

			cm.MemberId = m.MemberId;

			if (IsAllowJoinInProgress())
			{
				cm.Status = MemberJoinResponseStatus::Allow;
				UE_LOG(LogInit, Error, TEXT("> OnGetAvalibleMembers :: members %s - Allow"), *cm.MemberId.ToString());
				const_cast<TArray<FNodeSessionMember>&>(GetMatchMebers()).Add(m);
			}
			else
			{
				UE_LOG(LogInit, Error, TEXT("> OnGetAvalibleMembers :: members %s - DisAllow"), *cm.MemberId.ToString());
				cm.Status = MemberJoinResponseStatus::DisAllow;
			}

			ConfirmedMembers.Add(cm);
		}

		if (const auto node = GetNodeComponent())
		{
			node->SendConfirmJoinNewMembersRequest(ConfirmedMembers);
		}
		else
		{
			UE_LOG(LogInit, Error, TEXT("> OnGetAvalibleMembers :: NodeComponent was nullptr"));
		}
	}
#endif
}

void AOnlineGameMode::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	GetWorldTimerManager().ClearAllTimersForObject(this);
	GetWorldTimerManager().ClearAllTimersForObject(GetValidGameState());
	GetWorldTimerManager().ClearAllTimersForObject(GetNodeComponent());
	Super::EndPlay(EndPlayReason);
}

void AOnlineGameMode::StartPlay()
{
	Super::StartPlay();
}

void AOnlineGameMode::PreLogin(const FString& Options, const FString& Address, const FUniqueNetIdRepl& UniqueId, FString& ErrorMessage)
{
  UE_LOG(LogNet, Display, TEXT("> AOnlineGameMode::PreLogin | Address: %s | Id: %s | Options: %s"), *Address, *UniqueId->ToString(), *Options);

#if UE_SERVER && !WITH_EDITOR
	const auto TargetMember = FindMatchMember(UniqueId->ToString());

	if (TargetMember)
	{
		Super::PreLogin(Options, Address, UniqueId, ErrorMessage);
	}
	else
	{
		ErrorMessage = FString::Printf(TEXT("Member {%s} not found on this match!"), *UniqueId->ToString());
	}
#else
	Super::PreLogin(Options, Address, UniqueId, ErrorMessage);
#endif
}

void AOnlineGameMode::PostLogin(APlayerController* NewPlayer)
{
	auto LocalPlayer = Cast<UNetConnection>(NewPlayer->Player);
	if (LocalPlayer && NewPlayer->GetPlayerState<APlayerState>())
	{
		UE_LOG(LogGameMode, Warning, TEXT("AOnlineGameMode::PostLogin >> Controller: %s, Token: %s, PlayerState->UniqleId: %s"), *NewPlayer->GetName(), *LocalPlayer->PlayerId.ToString(), *NewPlayer->GetPlayerState<APlayerState>()->UniqueId.ToString());
	}

#if UE_SERVER && !WITH_EDITOR
	if (LocalPlayer)
	{
		const auto TargetMember = FindMatchMember(LocalPlayer->PlayerId->ToString());

		if (TargetMember)
		{
			NewPlayer->GetPlayerState<APlayerState>()->SetPlayerName(TargetMember->PlayerName);
			Super::PostLogin(NewPlayer);
		}		

		auto TargetState = NewPlayer->GetPlayerState<ABasePlayerState>();

		if (TargetMember && TargetState)
		{
			TargetState->SetIdentityToken(TargetMember->TokenId);
			TargetState->MemberId = TargetMember->MemberId;

			UE_LOG(LogGameMode, Warning, TEXT("AOnlineGameMode::PostLogin[%s][%s / %s] [TargetMember: %s]"),
				*TargetMember->PlayerName, *TargetMember->TokenId.ToString(), *TargetState->GetIdentityToken().ToString(),
				*LocalPlayer->PlayerId->ToString());

			TargetState->GetIdentityComponent()->SendRequestExist();
		}
	}
#else
	if (GetNetMode() != NM_DedicatedServer)
	{
		auto TargetState = NewPlayer->GetPlayerState<ABasePlayerState>();
		if (TargetState) TargetState->GetIdentityComponent()->SendRequestExist();
	}

	Super::PostLogin(NewPlayer);
#endif
}

void AOnlineGameMode::SendLeaveSessionRequest(const FGuid& member)
{
	if (const auto node = GetNodeComponent())
	{
		node->SendLeaveMemberRequest(member);
	}
	else
	{
		UE_LOG(LogInit, Error, TEXT("> OnGetAvalibleMembers :: SendLeaveSessionRequest | %s"), *member.ToString());
	}
}


void AOnlineGameMode::OnInitializeMatchInformation()
{
	if (this->IsA<AShooterGame_CapturePoint>() == false)
	{
		TArray<AActor*> Actors;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), ACapturePoint::StaticClass(), Actors);

		for (auto a : Actors)
		{
			if (a && a->IsValidLowLevel())
			{
				a->Destroy(true);
				if (a && a->IsPendingKillPending() == false)
				{
					a->SetLifeSpan(1.0f);
				}
			}
		}		
	}
}

void AOnlineGameMode::SendLocalizaedMessage(TSubclassOf<UBaseLocalMessage> InClass, ABasePlayerController* InTo, FBaseLocalMessageData& InMessage)
{
	if (InTo && InTo->IsValidLowLevel())
	{
		InTo->ReceiveLocalizaedMessage(InClass, InMessage);
	}
}

void AOnlineGameMode::BroadcastLocalizedMessage(TSubclassOf<UBaseLocalMessage> InClass, FBaseLocalMessageData& InMessage)
{
	for (FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
	{
		SendLocalizaedMessage(InClass, Cast<ABasePlayerController>(Iterator->Get()), InMessage);
	}
}

UNodeComponent* AOnlineGameMode::GetNodeComponent()
{
	if (auto gs = GetValidGameState())
	{
		return GetValidObject(gs->GetNodeComponent());
	}
	return nullptr;
}

UNodeComponent* AOnlineGameMode::GetNodeComponent() const
{
	if (auto gs = GetValidGameState())
	{
		return GetValidObject(gs->GetNodeComponent());
	}
	return nullptr;
}

const FSessionMatchOptions& AOnlineGameMode::GetMatchOptions() const
{
  return GetMatchInformation().Options;
}

const FNodeSessionMatch& AOnlineGameMode::GetMatchInformation() const
{
	return GetNodeComponent()->GetMatchInformation();
}

const TArray<FNodeSessionMember>& AOnlineGameMode::GetMatchMebers() const
{
	return GetNodeComponent()->GetMatchMebers();
}

const FNodeSessionMember* AOnlineGameMode::FindMatchMember(const FString& InMemberId) const
{
	FGuid TargetId = FLokaGuid::FromString(InMemberId);
	return GetMatchMebers().FindByPredicate([&](const FNodeSessionMember& A) {
		return (A.MemberId == TargetId);
	});
}

uint64 AOnlineGameMode::GetMatchMebersNum() const
{
	return GetMatchMebers().Num();
}
