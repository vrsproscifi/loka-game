// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#include "LokaGame.h"
#include "UTTeamGameMode.h"
#include "UTTeamInfo.h"
#include "UTPlayerStart.h"
#include "UTBotCharacter.h"
#include "MatchStateNames.h"
#include "UTGameState.h"
#include "UTPlayerState.h"
#include "UTPlayerController.h"
#include "Online.h"
#include "Shared/SessionMatchOptions.h"

UUTTeamInterface::UUTTeamInterface(const FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{
}

AUTTeamGameMode::AUTTeamGameMode()
: Super()
{
	TeamMomentumPct = 0.75f;
	WallRunMomentumPct = 0.5f;
	bTeamGame = true;
	bHasBroadcastDominating = false;
	bHighScorerPerTeamBasis = true;
}


void AUTTeamGameMode::Killed(AController* Killer, AController* KilledPlayer, APawn* KilledPawn, TSubclassOf<UDamageType> DamageType)
{
	Super::Killed(Killer, KilledPlayer, KilledPawn, DamageType);

	AUTPlayerState* VictimPlayerState = KilledPlayer ? Cast<AUTPlayerState>(KilledPlayer->GetPlayerState<AUTPlayerState>()) : NULL;

	if (UTGameState && VictimPlayerState)
	{
		UTGameState->GiveTeamScore(VictimPlayerState->GetTeamNum(), -1.0f);

		if (UTGameState->GetTeamScore(VictimPlayerState->GetTeamNum()) <= .0f && GetMatchState() == MatchState::InProgress)
		{
			EndMatch();
		}
	}
}

void AUTTeamGameMode::DetermineMatchWinner()
{
	MatchWinner = INDEX_NONE;

	if (UTGameState->GetNumTeams() == 2)
	{
		if (UTGameState->GetTeamScore(0) != UTGameState->GetTeamScore(1))
		{
			if (UTGameState->GetTeamScore(0) > UTGameState->GetTeamScore(1))
			{
				MatchWinner = 0;
			}
			else
			{
				MatchWinner = 1;
			}
		}
	}
}


uint8 AUTTeamGameMode::GetTeamIndexById(const FGuid& teamId)
{
	uint8 index = 0;
	for(const auto team : Teams)
	{
		if (team->TeamId == teamId)
		{
			return index;
		}
		index++;
	}
	return 255;
}


APlayerController* AUTTeamGameMode::Login(class UPlayer* NewPlayer, ENetRole InRemoteRole, const FString& Portal, const FString& Options, const FUniqueNetIdRepl& UniqueId, FString& ErrorMessage)
{
	return Super::Login(NewPlayer, InRemoteRole, Portal, Options, UniqueId, ErrorMessage);
}


bool AUTTeamGameMode::ShouldBalanceTeams(const bool bInitialTeam) const
{
	return bInitialTeam == false || HasMatchStarted();
}

bool AUTTeamGameMode::ChangeTeam(AController* Player, uint8 NewTeam, bool bBroadcast)
{
	//=====================================
	if (Player == nullptr)
	{
		return false;
	}

	//=====================================
	AUTPlayerState* PS = Cast<AUTPlayerState>(Player->GetPlayerState<AUTPlayerState>());
	if (PS == nullptr || PS->bOnlySpectator)
	{
		return false;
	}

	//=====================================
	if (PS->IsABot == false && PS->Team)
	{
		return false;
	}

	//=====================================

	bool bForceTeam = false;
	if (Teams.IsValidIndex(NewTeam))
	{
		// see if someone is willing to switch
		//for (FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
		//{
		//	AUTPlayerController* NextPlayer = Cast<AUTPlayerController>(*Iterator);
		//	AUTPlayerState* SwitchingPS = NextPlayer ? Cast<AUTPlayerState>(NextPlayer->GetPlayerState<AUTPlayerState>()) : NULL;
		//	if (SwitchingPS && SwitchingPS->Team == Teams[NewTeam] && Teams.IsValidIndex(1 - NewTeam))
		//	{
		//		UE_LOG(LogOnline, Warning, TEXT("ChangeTeam #2 %s moved to NewTeam %d"), *Player->GetPlayerState<AUTPlayerState>()->PlayerName, static_cast<int32>(NewTeam));

		//		// Found someone who wants to leave team, so just replace them
		//		MovePlayerToTeam(NextPlayer, SwitchingPS, 1 - NewTeam);
		//		SwitchingPS->HandleTeamChanged(NextPlayer);
		//		MovePlayerToTeam(Player, PS, NewTeam);
		//		return true;
		//	}
		//}

		if (ShouldBalanceTeams(PS->Team == nullptr))
		{
			UE_LOG(LogOnline, Warning, TEXT("ChangeTeam #3 %s ShouldBalanceTeams %d was true"), *Player->GetPlayerState<AUTPlayerState>()->GetPlayerName(), static_cast<int32>(NewTeam));

			for (int32 i = 0; i < Teams.Num(); i++)
			{
				const auto iHumans = Teams[i]->GetNumHumans();
				const auto nHumans = Teams[NewTeam]->GetNumHumans();

				// don't allow switching to a team with more players, or equal players if the player is on a team now
				if (i != NewTeam)
				{
					const auto v = PS->Team != nullptr && PS->Team->TeamIndex == i ? 1 : 0;
					if(iHumans - v < nHumans)
					{
						UE_LOG(LogOnline, Warning, TEXT("ChangeTeam #3.1 %s ShouldBalanceTeams %d was true"), *Player->GetPlayerState<AUTPlayerState>()->GetPlayerName(), static_cast<int32>(NewTeam));

						bForceTeam = true;
						break;
					}
				}
			}
		}
		else
		{
			UE_LOG(LogOnline, Warning, TEXT("ChangeTeam #3 %s ShouldBalanceTeams %d was false"), *Player->GetPlayerState<AUTPlayerState>()->GetPlayerName(), static_cast<int32>(NewTeam));
		}
	}
	else
	{
		bForceTeam = true;
		UE_LOG(LogOnline, Warning, TEXT("ChangeTeam #1 %s invalid NewTeam %d"), *Player->GetPlayerState<AUTPlayerState>()->GetPlayerName(), static_cast<int32>(NewTeam));
	}
			
	if (bForceTeam)
	{
		const auto oldNewTeam = static_cast<int32>(NewTeam);

		const auto realTeam = PS->Team == nullptr ? -1 : static_cast<int32>(PS->Team->TeamIndex);
		NewTeam = PickBalancedTeam(PS, NewTeam);
		UE_LOG(LogOnline, Warning, TEXT("ChangeTeam %s #4 from %d to %d [real new team %d]"), *Player->GetPlayerState<AUTPlayerState>()->GetPlayerName(), realTeam, oldNewTeam, static_cast<int32>(NewTeam));
	}
		
	if (MovePlayerToTeam(Player, PS, NewTeam))
	{
		UE_LOG(LogOnline, Warning, TEXT("ChangeTeam #5 %s MovePlayerToTeam %d was true"), *Player->GetPlayerState<AUTPlayerState>()->GetPlayerName(), static_cast<int32>(NewTeam));
		return true;
	}
	else
	{
		UE_LOG(LogOnline, Warning, TEXT("ChangeTeam #5 %s MovePlayerToTeam %d was false"), *Player->GetPlayerState<AUTPlayerState>()->GetPlayerName(), static_cast<int32>(NewTeam));
	}

	PS->ForceNetUpdate();
	return false;
}

bool AUTTeamGameMode::MovePlayerToTeam(AController* Player, AUTPlayerState* PS, uint8 NewTeam)
{
	if (Teams.IsValidIndex(NewTeam) && (PS->Team == nullptr || PS->Team->TeamIndex != NewTeam))
	{
		//Make sure we kill the player before they switch sides so the correct team loses the point
		AUTCharacter* UTC = Cast<AUTCharacter>(Player->GetPawn());
		if (UTC)
		{
			UTC->PlayerSuicide();
		}

		if (PS->Team)
		{
			PS->Team->RemoveFromTeam(Player);
		}

		Teams[NewTeam]->AddToTeam(Player);
		PS->ForceNetUpdate();
		return true;
	}
	return false;
}

uint8 AUTTeamGameMode::PickBalancedTeam(AUTPlayerState* PS, uint8 RequestedTeam)
{
	TArray< AUTTeamInfo*, TInlineAllocator<4> > BestTeams;
	int32 BestSize = -1;

	for (int32 i = 0; i < Teams.Num(); i++)
	{
		int32 TestSize = Teams[i]->GetSize();

		if (Teams[i] == PS->Team)
		{
			// player will be leaving this team so count its size as post-departure
			TestSize--;
		}

		if (BestTeams.Num() == 0 || TestSize < BestSize)
		{
			BestTeams.Empty();
			BestTeams.Add(Teams[i]);
			BestSize = TestSize;
		}
		else if (TestSize == BestSize)
		{
			BestTeams.Add(Teams[i]);
		}
	}


	// if in doubt choose team with bots on it as the bots will leave if necessary to balance
	{
		TArray< AUTTeamInfo*, TInlineAllocator<4> > TeamsWithBots;
		for (AUTTeamInfo* TestTeam : BestTeams)
		{
			bool bHasBots = false;
			TArray<AController*> Members = TestTeam->GetTeamMembers();
			for (AController* C : Members)
			{
				if (Cast<AUTBot>(C))
				{
					bHasBots = true;
					break;
				}
			}

			if (bHasBots)
			{
				TeamsWithBots.Add(TestTeam);
			}
		}

		if (TeamsWithBots.Num() > 0)
		{
			for (int32 i = 0; i < TeamsWithBots.Num(); i++)
			{
				if (TeamsWithBots[i]->TeamIndex == RequestedTeam)
				{
					return RequestedTeam;
				}
			}

			return TeamsWithBots[FMath::RandHelper(TeamsWithBots.Num())]->TeamIndex;
		}
	}


	for (int32 i = 0; i < BestTeams.Num(); i++)
	{
		if (BestTeams[i]->TeamIndex == RequestedTeam)
		{
			return RequestedTeam;
		}
	}
	
	if (BestTeams.Num())
	{
		return BestTeams[FMath::RandHelper(BestTeams.Num())]->TeamIndex;
	}
	return 0;
}


void AUTTeamGameMode::CheckBotCount()
{
	const auto options = GetMatchOptions();
	if (NumPlayers + NumBots > options.GetNumberOfBots())
	{
		TArray<AUTTeamInfo*> SortedTeams = UTGameState->Teams;
		SortedTeams.Sort([](AUTTeamInfo& A, AUTTeamInfo& B) { return A.GetSize() > B.GetSize(); });

		// try to remove bots from team with the most players
		for (AUTTeamInfo* Team : SortedTeams)
		{
			bool bFound = false;
			TArray<AController*> Members = Team->GetTeamMembers();
			for (AController* C : Members)
			{
				AUTBot* B = Cast<AUTBot>(C);
				if (B != nullptr)
				{
					if (AllowRemovingBot(B))
					{
						B->Destroy();
					}
					// note that we break from the loop on finding a bot even if we can't remove it yet, as it's still the best choice when it becomes available to remove (dies, etc)
					bFound = true;
					break;
				}
			}
			if (bFound)
			{
				break;
			}
		}
	}
	else while (NumPlayers + NumBots < options.GetNumberOfBots())
	{
		AddBot();
	}
}


UUTBotCharacter* AUTTeamGameMode::ChooseRandomCharacter(uint8 TeamNum)
{
	if (!Teams.IsValidIndex(TeamNum) || Teams.Num() < 2 || Teams[TeamNum] == nullptr || EligibleBots.Num() == 0)
	{
		return Super::ChooseRandomCharacter(TeamNum);
	}
	else
	{
		TArray<float> TeamAvgSkill;
		TeamAvgSkill.AddZeroed(Teams.Num());
		for (int32 i = 0; i < Teams.Num(); i++)
		{
			if (Teams[i] != nullptr)
			{
				int32 BotsOnTeam = 0;
				float Avg = 0.0f;
				for (AController* Member : Teams[i]->GetTeamMembers())
				{
					AUTBot* B = Cast<AUTBot>(Member);
					if (B != nullptr)
					{
						BotsOnTeam++;
						Avg += B->Skill;
					}
				}
				if (BotsOnTeam > 0)
				{
					Avg /= BotsOnTeam;
				}
			}
		}

		int32 ClosestTeam = INDEX_NONE;
		float ClosestSkillDiff = FLT_MAX;
		for (int32 i = 0; i < Teams.Num(); i++)
		{
			if (FMath::Abs<float>(TeamAvgSkill[i] - TeamAvgSkill[TeamNum]) < ClosestSkillDiff)
			{
				ClosestTeam = i;
				ClosestSkillDiff = TeamAvgSkill[i] - TeamAvgSkill[TeamNum];
			}
		}

		int32 BestMatch = 0;
		for (int32 i = 0; i < EligibleBots.Num(); i++)
		{
			if (EligibleBots[i]->Skill >= GetMatchOptions().GetDifficulty())
			{
				BestMatch = i;
				break;
			}
		}
		int32 Index = FMath::Clamp(BestMatch + FMath::RandHelper(5) - 2, 0, EligibleBots.Num() - 1);
		// shift to bot with different skill to balance this team's average against the other teams
		if (ClosestSkillDiff < 0.0f)
		{
			while (Index > 0 && EligibleBots[Index]->Skill > TeamAvgSkill[TeamNum])
			{
				Index--;
			}
		}
		else
		{
			while (Index < EligibleBots.Num() - 1 && EligibleBots[Index]->Skill < TeamAvgSkill[TeamNum])
			{
				Index++;
			}
		}
		UUTBotCharacter* ChosenCharacter = EligibleBots[Index];
		EligibleBots.RemoveAt(Index);
		return ChosenCharacter;
	}
}

void AUTTeamGameMode::DefaultTimer()
{
	Super::DefaultTimer();
}


bool AUTTeamGameMode::ModifyDamage_Implementation(int32& Damage, FVector& Momentum, APawn* Injured, AController* InstigatedBy, const FHitResult& HitInfo, AActor* DamageCauser, TSubclassOf<UDamageType> DamageType)
{
	if (InstigatedBy != nullptr && InstigatedBy != Injured->Controller && Cast<AUTGameState>(GameState)->OnSameTeam(Injured, InstigatedBy))
	{
		Damage *= TeamDamagePct;
		Momentum *= TeamMomentumPct;
		AUTCharacter* InjuredChar = Cast<AUTCharacter>(Injured);
		if (InjuredChar && InjuredChar->bApplyWallSlide)
		{
			Momentum *= WallRunMomentumPct;
		}
		AUTPlayerController* InstigatorPC = Cast<AUTPlayerController>(InstigatedBy);
		if (InstigatorPC && Cast<AUTPlayerState>(Injured->GetPlayerState<AUTPlayerState>()))
		{
			((AUTPlayerState *)(Injured->GetPlayerState<AUTPlayerState>()))->AnnounceSameTeam(InstigatorPC);
		}
	}
	Super::ModifyDamage_Implementation(Damage, Momentum, Injured, InstigatedBy, HitInfo, DamageCauser, DamageType);
	return true;
}

float AUTTeamGameMode::RatePlayerStart(AUTPlayerStart* P, AController* Player)
{
	float Result = Super::RatePlayerStart(P, Player);
	if (Player && Player->GetPlayerState<AUTPlayerState>())
	{
		if (auto MyPlayerState = Cast<AUTPlayerState>(Player->GetPlayerState<AUTPlayerState>()))
		{
			if (MyPlayerState->GetTeamNum() != P->GetTeamNum())
			{
				Result = -20.0f;
			}
			else if (P->IsOtherTeamCharacterOverlap())
			{
				Result -= 10.0f;
			}
		}
	}

	return Result;
}

bool AUTTeamGameMode::AvoidPlayerStart(AUTPlayerStart* InPlayerStart)
{
	return Super::AvoidPlayerStart(InPlayerStart);
}

bool AUTTeamGameMode::CanSpectate_Implementation(APlayerController* Viewer, APlayerState* ViewTarget)
{
	AUTPlayerController* PC = Cast<AUTPlayerController>(Viewer);
	return (Viewer->GetPlayerState<AUTPlayerState>()->bOnlySpectator || ViewTarget == nullptr || ViewTarget->bOnlySpectator || (PC != nullptr && PC->GetTeamNum() == 255) || GetWorld()->GetGameState<AUTGameState>()->OnSameTeam(Viewer, ViewTarget));
}

bool AUTTeamGameMode::CheckScore_Implementation(AUTPlayerState* Scorer)
{
	AUTTeamInfo* WinningTeam = nullptr;

	for (int32 i = 0; i < Teams.Num(); i++)
	{
		if (Teams[i]->Score >= GetMatchOptions().GetScoreLimit())
		{
			WinningTeam = Teams[i];
			break;
		}
	}

	if (WinningTeam != nullptr)
	{
		AUTPlayerState* BestPlayer = FindBestPlayerOnTeam(WinningTeam->GetTeamNum());
		if (BestPlayer == nullptr) BestPlayer = Scorer;
//		EndGame(BestPlayer, TEXT("fraglimit")); 
		return true;
	}
	else
	{
		return false;
	}
}

AUTPlayerState* AUTTeamGameMode::FindBestPlayerOnTeam(int32 TeamNumToTest)
{
	AUTPlayerState* Best = NULL;
	for (int32 i = 0; i < UTGameState->PlayerArray.Num(); i++)
	{
		AUTPlayerState* PS = Cast<AUTPlayerState>(UTGameState->PlayerArray[i]);
		if (PS != nullptr && PS->GetTeamNum() == TeamNumToTest && (Best == nullptr || Best->Score < PS->Score))
		{
			Best = PS;
		}
	}
	return Best;
}

void AUTTeamGameMode::BroadcastScoreUpdate(APlayerState* ScoringPlayer, AUTTeamInfo* ScoringTeam, int32 OldScore)
{
	// find best competing score - assume this is called after scores are updated.
	int32 BestScore = 0;
	for (int32 i = 0; i < Teams.Num(); i++)
	{
		if ((Teams[i] != ScoringTeam) && (Teams[i]->Score >= BestScore))
		{
			BestScore = Teams[i]->Score;
		}
	}
	//BroadcastLocalized(this, UUTCTFRewardMessage::StaticClass(), 3+ScoringTeam->TeamIndex, ScoringPlayer, NULL, ScoringTeam);
	//
	//if ((OldScore > BestScore) && (OldScore <= BestScore + 2) && (ScoringTeam->Score > BestScore + 2))
	//{
	//	BroadcastLocalized(this, UUTCTFGameMessage::StaticClass(), 8, ScoringPlayer, NULL, ScoringTeam);
	//}
	//else
	//{
	//	bHasBroadcastDominating = false; // since other team scored, need new reminder if mercy rule might be hit again
	//	BroadcastLocalized(this, UUTCTFGameMessage::StaticClass(), 2, ScoringPlayer, NULL, ScoringTeam);
	//}
}

void AUTTeamGameMode::PlayEndOfMatchMessage()
{
	if (UTGameState && UTGameState->WinningTeam)
	{
		for (FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
		{
			APlayerController* Controller = Iterator->Get();
			if (Controller && Controller->IsA(AUTPlayerController::StaticClass()))
			{
				AUTPlayerController* PC = Cast<AUTPlayerController>(Controller);
				if (PC && Cast<AUTPlayerState>(PC->GetPlayerState<AUTPlayerState>()))
				{
					PC->ClientReceiveLocalizedMessage(VictoryMessageClass, ((UTGameState->WinningTeam == Cast<AUTPlayerState>(PC->GetPlayerState<AUTPlayerState>())->Team) ? 1 : 0), UTGameState->WinnerPlayerState, PC->GetPlayerState<AUTPlayerState>(), UTGameState->WinningTeam);
				}
			}
		}
	}
}

void AUTTeamGameMode::FindAndMarkHighScorer()
{
	// Some game modes like Duel may not want every team to have a high scorer
	if (!bHighScorerPerTeamBasis)
	{
		Super::FindAndMarkHighScorer();
		return;
	}

	for (int32 i = 0; i < Teams.Num(); i++)
	{
		int32 BestScore = 0;

		for (int32 PlayerIdx = 0; PlayerIdx < Teams[i]->GetTeamMembers().Num(); PlayerIdx++)
		{
			if (Teams[i]->GetTeamMembers()[PlayerIdx] != nullptr)
			{
				AUTPlayerState *PS = Cast<AUTPlayerState>(Teams[i]->GetTeamMembers()[PlayerIdx]->GetPlayerState<AUTPlayerState>());
				if (PS != nullptr)
				{
					if (BestScore == 0 || PS->Score > BestScore)
					{
						BestScore = PS->Score;
					}
				}
			}
		}

		for (int32 PlayerIdx = 0; PlayerIdx < Teams[i]->GetTeamMembers().Num(); PlayerIdx++)
		{
			if (Teams[i]->GetTeamMembers()[PlayerIdx] != nullptr)
			{
				AUTPlayerState *PS = Teams[i]->GetTeamMembers()[PlayerIdx]->GetPlayerState<AUTPlayerState>();
				if (PS != nullptr)
				{
					AUTCharacter *UTChar = Cast<AUTCharacter>(Teams[i]->GetTeamMembers()[PlayerIdx]->GetPawn());
					if (UTChar)
					{
						bool bOldHighScorer = UTChar->bHasHighScore;
						UTChar->bHasHighScore = (BestScore == PS->Score);
						if (bOldHighScorer != UTChar->bHasHighScore)
						{
							UTChar->HasHighScoreChanged();
						}
					}
				}
			}
		}
	}
}

void AUTTeamGameMode::SendComsMessage( AUTPlayerController* Sender, AUTPlayerState* Target, int32 Switch)
{
	AUTPlayerState* UTPlayerState = Cast<AUTPlayerState>(Sender->GetPlayerState<AUTPlayerState>());
	if (UTPlayerState)
	{
		if (Target != nullptr)
		{
			AUTPlayerController* UTPlayerController = Cast<AUTPlayerController>(Target->GetOwner());
			if (UTPlayerController)
			{
				UTPlayerController->ClientReceiveLocalizedMessage(UTPlayerState->GetCharacterVoiceClass(), Switch, UTPlayerState, nullptr, UTPlayerState->LastKnownLocation);
			}
			Sender->ClientReceiveLocalizedMessage(UTPlayerState->GetCharacterVoiceClass(), Switch, UTPlayerState, nullptr, UTPlayerState->LastKnownLocation);
		}
		else
		{
			for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
			{
				AUTPlayerController* UTPlayerController = Cast<AUTPlayerController>(It->Get());
				if ( UTPlayerController != nullptr && UTPlayerController->GetTeamNum() == Sender->GetTeamNum())
				{
					UTPlayerController->ClientReceiveLocalizedMessage(UTPlayerState->GetCharacterVoiceClass(), Switch, UTPlayerState, nullptr, UTPlayerState->LastKnownLocation);
				}
			}
		}
	}
}

