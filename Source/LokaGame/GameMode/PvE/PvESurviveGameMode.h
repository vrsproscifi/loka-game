#pragma once

#include "UTTeamGameMode.h"

#include "PvESurviveGameState.h"
#include "PvESurviveGameModeSettings.h"
#include "PvESurviveGameMode.generated.h"	


class AUTPlayerState;

UCLASS()
class APvESurviveGameMode : public AUTTeamGameMode
{
	GENERATED_BODY()

	const int32 TeamIndexOfPlayer = 0;
	const int32 TeamIndexOfEnemy = 1;

	UPROPERTY(VisibleInstanceOnly)
	bool MatchIsStarted;

	APvESurviveGameMode();

	//-------------------------------------------------------------
	//	The delay between the waves in seconds

	UPROPERTY(EditAnywhere, Category = "PvE")
		TArray<FPvESurviveGameModeSettings> SettingsDifficultyLevel;

	UPROPERTY(VisibleInstanceOnly)
		int32 CurrentDifficultyLevel;

	UPROPERTY(VisibleInstanceOnly)
		FPvESurviveGameModeSettings CurrentDifficultySettings;

	UPROPERTY(VisibleInstanceOnly)
		FPvESurviveWaveSettings CurrentWaveSettings;

	virtual void StartMatch() override;
	virtual void EndMatch() override;
	virtual void DefaultTimer() override;
	virtual void StartPlay() override;
	virtual void RestartPlayer(AController* aPlayer) override;
	virtual void InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage) override;
	virtual void InitGameState() override;
	virtual void CheckBotCount() override;
	virtual void Killed(AController* Killer, AController* KilledPlayer, APawn* KilledPawn, TSubclassOf<UDamageType> DamageType) override;
	virtual void PostLogin(APlayerController* NewPlayer) override;
	virtual void DetermineMatchWinner() override;

	/** Initialize for new round. */
	virtual void InitRound();
	virtual void InitRound(const FPvESurviveWaveSettings& wave);
	virtual AUTBot* AddBot(uint8 TeamNum) override;

	/** Initialize a player for the new round. */
	virtual void InitPlayerForRound(AUTPlayerState* PS);
	

};