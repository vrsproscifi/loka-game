#pragma once


#include "UTGameState.h"
#include "PvESurviveGameState.generated.h"	

UCLASS()
class APvESurviveGameState : public AUTGameState
{
	GENERATED_BODY()
		
	APvESurviveGameState();

public:
	UPROPERTY(Replicated)
		uint8 CurrentRound;

	UPROPERTY(Replicated)
		uint8 NumberOfRounds;

};