#include "LokaGame.h"
#include "PvESurviveGameMode.h"
#include "UTPlayerController.h"
#include "UTTeamInfo.h"
#include "UTBot.h"



APvESurviveGameMode::APvESurviveGameMode()
	: CurrentDifficultyLevel(0)
{
	GameStateClass = APvESurviveGameState::StaticClass();

}
//void APvESurviveGameMode::InitTeam()
//{
//	//NumTeams = 2;
//	////=============================================================================
//	//if (TeamClass == nullptr)
//	//{
//	//	TeamClass = AUTTeamInfo::StaticClass();
//	//}
//
//	//	 TODO
//	////=============================================================================
//	//FNodeSessionMatch* matchRef = serverController ? serverController->CheckAvailableMatch.CurrentSessionMatch : nullptr;
//	//UE_LOG(UT, Log, TEXT("InitGame[IsEditor: %d] [matchRef: %d]"), GEngine->IsEditor(), matchRef != nullptr);
//	//
//	////=============================================================================
//	//for (uint8 i = 0; i < NumTeams; i++)
//	//{
//	//	AUTTeamInfo* NewTeam = GetWorld()->SpawnActor<AUTTeamInfo>(TeamClass);
//	//	{
//	//		NewTeam->TeamId = FGuid::NewGuid();
//	//		NewTeam->TeamIndex = i;
//	//	}
//	//	if (TeamColors.IsValidIndex(i))
//	//	{
//	//		NewTeam->TeamColor = TeamColors[i];
//	//	}
//	//
//	//	if (TeamNames.IsValidIndex(i))
//	//	{
//	//		NewTeam->TeamName = TeamNames[i];
//	//	}
//	//
//	//	Teams.Add(NewTeam);
//	//	checkSlow(Teams[i] == NewTeam);
//	//}
//	//
//	//if (matchRef)
//	//{
//	//	UE_LOG(UT, Log, TEXT("InitGame: [Members: %d] / [Teams: %d]"), matchRef->Members.Num(), matchRef->TeamList.Num());
//	//	if (matchRef->TeamList.Num() == 1)
//	//	{
//	//		FGuid::Parse(matchRef->TeamList[TeamIndexOfPlayer], Teams[TeamIndexOfPlayer]->TeamId);
//	//	}
//	//
//	//}
//}

void APvESurviveGameMode::DefaultTimer()
{
	Super::DefaultTimer();
}

void APvESurviveGameMode::CheckBotCount()
{
	Super::CheckBotCount();
}

void APvESurviveGameMode::InitGameState()
{
	AUTGameMode::InitGameState();

	if (GetWorld()->IsPreviewWorld())
	{
		return;
	}

	//==============================================================
	APvESurviveGameState* const MyGameState = Cast<APvESurviveGameState>(GameState);
	if (MyGameState)
	{
		//-------------------------------------------------------------------------------------
		MyGameState->Teams = Teams;
		MyGameState->SetTeamScore(TeamIndexOfPlayer, CurrentDifficultySettings.NumberOfPlayerHealth);

		//-------------------------------------------------------------------------------------
		InitRound();

		//-------------------------------------------------------------------------------------
		MyGameState->NumberOfRounds = CurrentDifficultySettings.GetWaveCount();
	}
}

void APvESurviveGameMode::InitRound()
{
	APvESurviveGameState* const MyGameState = Cast<APvESurviveGameState>(GameState);
	if (MyGameState)
	{
		MyGameState->GiveTeamScore(TeamIndexOfPlayer, CurrentDifficultySettings.NumberOfHealthToRegenerate);
		InitRound(CurrentDifficultySettings.GetWave(MyGameState->CurrentRound));
		MyGameState->CurrentRound++;
	}
	else
	{
		UE_LOG(LogInit, Error, TEXT("APvESurviveGameMode::InitRound - PvESurviveGameState was nullptr [%s]"), *GameState->GetName());
	}
}

AUTBot* APvESurviveGameMode::AddBot(uint8 TeamNum)
{
	return Super::AddBot(TeamIndexOfEnemy);
}

void APvESurviveGameMode::Killed(AController* Killer, AController* KilledPlayer, APawn* KilledPawn, TSubclassOf<UDamageType> DamageType)
{
	if (GetMatchState() == MatchState::InProgress)
	{
		Super::Killed(Killer, KilledPlayer, KilledPawn, DamageType);
		if (KilledPlayer->IsA<AUTBot>())
		{
			if (auto MyPlayerState = KilledPlayer->GetPlayerState<AUTPlayerState>())
			{
				MyPlayerState->RespawnTime = CurrentWaveSettings.GetIntevalOfBotSpawns();
			}
		}
	}
}


void APvESurviveGameMode::InitRound(const FPvESurviveWaveSettings& wave)
{
	APvESurviveGameState* const MyGameState = CastChecked<APvESurviveGameState>(GameState);

	//-------------------------------------------------------------------------------------
	for (FConstControllerIterator It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		AUTBot* B = Cast<AUTBot>(It->Get());

		//	Destroy enemys after end wave
		if (B != nullptr)
		{
			B->Destroy();
		}
		else
		{
			//	Show notity: waiting next wave
			if (auto P = Cast<AUTPlayerController>(It->Get()))
			{
				P->ClientShowTextTimed(CurrentDifficultySettings.GetDelayBetweenWaves(), ETimedTextMessage::WaitingStart);
			}
		}
	}

	//-------------------------------------------------------------------------------------
	//	We update the number of lives for the current wave of bots
	MyGameState->SetTeamScore(TeamIndexOfEnemy, wave.NumberOfBotsPerWave);

	//-------------------------------------------------------------------------------------
	if (GetMatchState() == MatchState::InProgress || GetMatchState() == MatchState::WaitingPostMatch)
	{
		//	Install the game in WaitingToStart
		SetMatchState(MatchState::WaitingToStart);

		//	Making the delay between the waves
		MyGameState->SetRemainingTime(CurrentDifficultySettings.GetDelayBetweenWaves());
	}

	////
	//BotFillCount = wave.GetNumberOfBotsToSpawn() + 1;
  //
	////	We set the duration of time limit on the wave
	//TimeLimit = wave.WaveTimeInMinuts * 60;

	//	Update Inteval Of Player Spawns
	MinRespawnDelay = wave.GetIntevalOfPlayersSpawns();

	//	Update wave cache
	CurrentWaveSettings = wave;

	//UE_LOG(LogInit, Log, TEXT("InitRound %d / %d | %s"), MyGameState->CurrentRound, MyGameState->NumberOfRounds, *wave.ToString());
	//UE_LOG(LogInit, Log, TEXT("+ BotFillCount: %d | NumberOfBotsPerWave %d | TimeLimit: %d | MinRespawnDelay: %d"), BotFillCount, wave.NumberOfBotsPerWave, TimeLimit, MinRespawnDelay);

}

void APvESurviveGameMode::StartMatch()
{
	MatchIsStarted = true;
	Super::StartMatch();
}


void APvESurviveGameMode::EndMatch()
{
	APvESurviveGameState* const MyGameState = Cast<APvESurviveGameState>(GameState);
	if (GetMatchState() == MatchState::InProgress && MatchIsStarted)
	{
  		if (MyGameState)
		{
			MatchIsStarted = false;
			UE_LOG(LogInit, Log, TEXT("APvESurviveGameMode::EndMatch %d of %d"), static_cast<int32>(MyGameState->CurrentRound), static_cast<int32>(MyGameState->NumberOfRounds));

			if (MyGameState->CurrentRound >= MyGameState->NumberOfRounds)
			{
				Super::EndMatch();
			}
			else if (MyGameState->GetTeamScore(TeamIndexOfPlayer) <= 0 || MyGameState->GetTeamScore(TeamIndexOfEnemy) > 0)
			{
				Super::EndMatch();
			}
			else if(HasMatchStarted() || IsMatchInProgress())
			{
				InitRound();
			}
		}
		else
		{
			UE_LOG(LogInit, Error, TEXT("APvESurviveGameMode::EndMatch - PvESurviveGameState was nullptr [%s]"), *GameState->GetName());
			Super::EndMatch();
		}
	}
}

void APvESurviveGameMode::StartPlay()
{
	Super::StartPlay();
}

void APvESurviveGameMode::RestartPlayer(AController* aPlayer)
{
	//if (aPlayer && aPlayer->IsValidLowLevel())
	//{
	//
	//}
	Super::RestartPlayer(aPlayer);
}

void APvESurviveGameMode::InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage)
{
	Super::InitGame(MapName, Options, ErrorMessage);

	CurrentDifficultyLevel = UGameplayStatics::GetIntOption(Options, TEXT("AbstractMatchFlag"), 0);
	if (SettingsDifficultyLevel.Num() == 0)
	{
		if (GetWorld()->IsPlayInEditor())
		{
			UE_LOG(LogInit, Error, TEXT("APvESurviveGameMode::InitGame - SettingsDifficultyLevel was empty"));
		}
		else
		{
			UE_LOG(LogInit, Fatal, TEXT("APvESurviveGameMode::InitGame - SettingsDifficultyLevel was empty"));
		}
	}
	else
	{
		if (SettingsDifficultyLevel.IsValidIndex(CurrentDifficultyLevel))
		{
			CurrentDifficultySettings = SettingsDifficultyLevel[CurrentDifficultyLevel];
		}
		else
		{
			if (GetWorld()->IsPlayInEditor())
			{
				UE_LOG(LogInit, Error, TEXT("APvESurviveGameMode::InitGame - %d / %d SettingsDifficultyLevel not found"), CurrentDifficultyLevel, SettingsDifficultyLevel.Num());
			}
			else
			{
				UE_LOG(LogInit, Fatal, TEXT("APvESurviveGameMode::InitGame - %d / %d SettingsDifficultyLevel not found"), CurrentDifficultyLevel, SettingsDifficultyLevel.Num());
			}
		}
	}

	//UE_LOG(LogInit, Log, TEXT("APvESurviveGameMode::InitGameState - CurrentDifficultyLevel[%d] | Waves[%d | %d loop: %d] | Health: %d  | GameDifficulty: %f"), CurrentDifficultyLevel, CurrentDifficultySettings.Wave.Num(), CurrentDifficultySettings.NumberOfWaves, CurrentDifficultySettings.UseLoopWaves, CurrentDifficultySettings.NumberOfPlayerHealth, GameDifficulty);
	//{
	//	int32 index = 0;
	//	const int32 countOfWaves = CurrentDifficultySettings.Wave.Num();
	//	for (const auto wave : CurrentDifficultySettings.Wave)
	//	{
	//		UE_LOG(LogInit, Log, TEXT("Wave %d / %d | %s"), index, countOfWaves, *wave.ToString())
	//		index++;
	//	}
	//}
}


void APvESurviveGameMode::InitPlayerForRound(AUTPlayerState * PS)
{
}

void APvESurviveGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);

	//ChangeTeam(NewPlayer, 0);
}

void APvESurviveGameMode::DetermineMatchWinner()
{
	const auto MyGameState = CastChecked<AUTGameState>(GameState);
	if (MyGameState->GetTeamScore(TeamIndexOfPlayer) > 0 && MyGameState->GetTeamScore(TeamIndexOfEnemy) <= 0)
	{
		MatchWinner = TeamIndexOfPlayer;
	}
	else
	{
		MatchWinner = TeamIndexOfEnemy;
	}
}