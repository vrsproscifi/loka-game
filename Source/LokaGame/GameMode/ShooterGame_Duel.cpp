﻿// VRSPRO

#include "LokaGame.h"
#include "UTPlayerState.h"
#include "UTGameState.h"
#include "UTPlayerController.h"
#include "ShooterGame_Duel.h"

#include "ShooterGameInstance.h"
#include "UTTeamInfo.h"
#include "Pickup/UTPickup_Weapon.h"
#include "Shared/SessionMatchOptions.h"
#include "NodeComponent/NodeSessionMatch.h"
#include "GameSingleton.h"
#include "Weapon/ItemWeaponEntity.h"
#include "InventoryHacks.h"
#include "Character/CharacterBaseEntity.h"

AShooterGame_Duel::AShooterGame_Duel() 
  : Super()
{
	bDelayedStart = true;
//	BotFillCount = 0;
	//bAllowBots = false;
	//bNeedsBotCreation = false;
	
	
	Bet.Currency = EGameCurrency::Money;
	Bet.Amount = 0;
}

void AShooterGame_Duel::InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage)
{
	Super::InitGame(MapName, Options, ErrorMessage);
}

void AShooterGame_Duel::OnInitializeMatchInformation()
{
	Super::OnInitializeMatchInformation();

	Bet = GetMatchInformation().Options.Bet;
	auto WeapType = GetMatchInformation().Options.GetWeaponType();

	TArray<UItemWeaponEntity*> FoundListWeapon = UGameSingleton::Get()->FindList<UItemWeaponEntity>(ECategoryTypeId::Weapon).FilterByPredicate([&, w = WeapType](const UItemWeaponEntity* InSource) { return FFlagsHelper::HasAnyFlags(w.ToFlag(), FFlagsHelper::ToFlag<int32>(InSource->GetWeaponType().GetValue())) && InSource->IsValidItem(); });
	TArray<UCharacterBaseEntity*> FoundListCharacter = UGameSingleton::Get()->FindList<UCharacterBaseEntity>(ECategoryTypeId::Character).FilterByPredicate([](const UCharacterBaseEntity* InSource) { return InSource->IsValidItem(); });

	FRandomStream RandomStream(FDateTime::UtcNow().GetMillisecond());
	
	auto FoundCharacter = FoundListCharacter[RandomStream.RandRange(0, FoundListCharacter.Num() - 1)];
	auto FoundWeapon = FoundListWeapon[RandomStream.RandRange(0, FoundListWeapon.Num() - 1)];

	Profile.CharacterModelId = FoundCharacter->GetModelId();
	Profile.PrimaryWeaponModelId = FoundWeapon->GetModelId();
	Profile.PrimaryWeaponSkin = RandomStream.RandRange(ItemMaterialModelId::Default, ItemMaterialModelId::End - 1);

	UE_LOG(LogGameMode, Warning, TEXT("AShooterGame_Duel::OnInitializeMatchInformation >> Character: [%d]%s, Weapon: [%d]%s, Bet: %s"), FoundCharacter->GetModelId(), *FoundCharacter->GetName(), FoundWeapon->GetModelId(), *FoundWeapon->GetName(), *Bet.ToString());
}

void AShooterGame_Duel::DefaultTimer()
{
	AUTGameMode::DefaultTimer();

	if (GetMatchState() == MatchState::InProgress)
	{
		if (UTGameState)
		{
			for (auto &p : UTGameState->PlayerArray)
			{
				if (auto t = Cast<AUTPlayerState>(p))
				{
					// Make team if null
					if (t->Team == nullptr)
					{
						if (auto NewTeam = GetWorld()->SpawnActor<AUTTeamInfo>(AUTTeamInfo::StaticClass()))
						{
							NewTeam->TeamId = t->TeamId.IsValid() ? t->TeamId : FGuid::NewGuid();
							NewTeam->AddToTeam(Cast<AController>(t->GetOwner()));
							const auto TeamIndex = UTGameState->Teams.Add(NewTeam);
							NewTeam->TeamIndex = TeamIndex;

							UE_LOG(LogGameMode, Warning, TEXT("AShooterGame_Duel::DefaultTimer >> Added new team %d/%d"), TeamIndex, UTGameState->Teams.Num());
						}
					}
				}
			}
		}
	}
}

void AShooterGame_Duel::InitGameState()
{
	Super::InitGameState();
}

void AShooterGame_Duel::StartPlay()
{
	Super::StartPlay();

	// Find and destroy weapon pickups
	for (TActorIterator<AUTPickup_Weapon> It(GetWorld()); It; ++It)
	{
		It->Destroy(true);
	}
}

void AShooterGame_Duel::Killed(AController* Killer, AController* KilledPlayer, APawn* KilledPawn, TSubclassOf<UDamageType> DamageType)
{
	Super::Killed(Killer, KilledPlayer, KilledPawn, DamageType);

	AUTPlayerState* VictimPlayerState = KilledPlayer ? Cast<AUTPlayerState>(KilledPlayer->GetPlayerState<AUTPlayerState>()) : NULL;

	if (UTGameState && VictimPlayerState && VictimPlayerState->GetDeaths() >= GetMatchOptions().GetNumberOfLives())
	{
		EndMatch();
	}
}

void AShooterGame_Duel::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);

	auto EnteringPlayerState = Cast<AUTPlayerState>(NewPlayer->GetPlayerState<AUTPlayerState>());
	if (EnteringPlayerState)
	{
		EnteringPlayerState->bIsAllowUserInventory = false;
		EnteringPlayerState->InitializeProfiles(TArray<FProfileHack>({ Profile }));
	}	

	if (UTGameState && EnteringPlayerState)
	{
		// Find player
		SIZE_T _count = 0;
		bool localIsFound = false, localIsLast = false;
		for (auto TargetPlayer : ExitingPlayerState)
		{
			if (EnteringPlayerState->MemberId == TargetPlayer->MemberId)
			{
				ExitingPlayerState.RemoveAt(_count);
				if (ExitingPlayerState.Num() == false)
				{
					localIsLast = true;
				}

				localIsFound = true;
			}

			++_count;
		}

		if (localIsFound)
		{
			UE_LOG(LogGameMode, Warning, TEXT("AShooterGame_Duel::Logout >> EnteringPlayer %s"), *EnteringPlayerState->GetPlayerName());

			if (localIsLast)
			{
				UTGameState->SetRemainingTime(ExitingRemainingTime);

				for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
				{
					AUTPlayerController* PlayerController = Cast<AUTPlayerController>(*It);
					if (PlayerController != nullptr)
					{
						PlayerController->ClientShowTextTimed(0, ETimedTextMessage::WaitingEnemy);
					}
				}
			}
			else
			{
				for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
				{
					AUTPlayerController* PlayerController = Cast<AUTPlayerController>(*It);
					if (PlayerController != nullptr)
					{
						PlayerController->ClientShowTextTimed(UTGameState->GetRemainingTime(), ETimedTextMessage::WaitingEnemy);
					}
				}
			}
		}
	}
}

void AShooterGame_Duel::Logout(AController * Exiting)
{
	ExitingPlayerState.AddUnique(Exiting->GetPlayerState<AUTPlayerState>());
	auto localExitingPlayerState = ExitingPlayerState.Last();

	if (UTGameState && UTGameState->GetRemainingTime() > 10 && GetMatchState() == MatchState::InProgress && localExitingPlayerState && localExitingPlayerState->bIsABot == false && localExitingPlayerState->IsOwnedByReplayController() == false)
	{
		if (ExitingPlayerState.Num() == 1)
		{
			ExitingRemainingTime = UTGameState->GetRemainingTime();
			UTGameState->SetRemainingTime(120);
		}

		UE_LOG(LogGameMode, Warning, TEXT("AShooterGame_Duel::Logout >> ExitingPlayer %s"), *localExitingPlayerState->GetPlayerName());

		for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
		{
			AUTPlayerController* PlayerController = Cast<AUTPlayerController>(*It);
			if (PlayerController != nullptr)
			{
				PlayerController->ClientShowTextTimed(120, ETimedTextMessage::WaitingEnemy);
			}
		}
	}

	Super::Logout(Exiting);
}

void AShooterGame_Duel::DetermineMatchWinner()
{
	MatchWinner = INDEX_NONE;

	if (ExitingPlayerState.Num() == 1 && UTGameState)
	{
		AUTPlayerState* LivePlayerState = nullptr;

		for (int32 i = 0; i < UTGameState->PlayerArray.Num(); i++)
		{
			if (auto ShooterState = Cast<AUTPlayerState>(UTGameState->PlayerArray[i]))
			{
				if (ShooterState->MemberId != ExitingPlayerState.Last()->MemberId)
				{
					LivePlayerState = ShooterState;
					break;
				}
			}
		}

		if (LivePlayerState)
		{
			if (LivePlayerState->GetKills() > ExitingPlayerState.Last()->GetKills())
			{
				WinnerPlayerState = LivePlayerState;
				WinnerPlayerState->AddExtraScore(GET_PERCENT_OF(WinnerPlayerState->GetScore(), 20.0f));
				MatchWinner = WinnerPlayerState->GetTeamNum();
			}
		}
	}
	else if(UTGameState && ExitingPlayerState.Num() == 0)
	{
		AUTPlayerState* FirstPlayerState = nullptr;
		AUTPlayerState* SecondPlayerState = nullptr;

		for (int32 i = 0; i < UTGameState->PlayerArray.Num(); i++)
		{
			if (auto ShooterState = Cast<AUTPlayerState>(UTGameState->PlayerArray[i]))
			{
				if (!FirstPlayerState)
				{
					FirstPlayerState = ShooterState;
				}
				else
				{
					SecondPlayerState = ShooterState;
				}
			}
		}

		if (FirstPlayerState && SecondPlayerState)
		{
			if (FirstPlayerState->GetKills() > SecondPlayerState->GetKills())
			{
				WinnerPlayerState = FirstPlayerState;
				WinnerPlayerState->AddExtraScore(GET_PERCENT_OF(WinnerPlayerState->GetScore(), 20.0f));
				MatchWinner = WinnerPlayerState->GetTeamNum();
			}
			else if (FirstPlayerState->GetKills() == SecondPlayerState->GetKills())
			{
				WinnerPlayerState = (FirstPlayerState->GetScore() > SecondPlayerState->GetScore()) ? FirstPlayerState : SecondPlayerState;
				WinnerPlayerState->AddExtraScore(GET_PERCENT_OF(WinnerPlayerState->GetScore(), 20.0f));
				MatchWinner = WinnerPlayerState->GetTeamNum();
			}
			else
			{
				WinnerPlayerState = SecondPlayerState;
				WinnerPlayerState->AddExtraScore(GET_PERCENT_OF(WinnerPlayerState->GetScore(), 20.0f));
				MatchWinner = WinnerPlayerState->GetTeamNum();
			}
		}
	}
}

void AShooterGame_Duel::AddExtraResult(AUTPlayerState* PlayerState)
{
	Super::AddExtraResult(PlayerState);
	if (MatchWinner != INDEX_NONE)
	{
		if (PlayerState == WinnerPlayerState)
		{
			PlayerState->GiveCash(Bet.Currency, Bet.Amount);
		}
		else
		{
			PlayerState->GiveCash(Bet.Currency, -Bet.Amount);
		}
	}
}