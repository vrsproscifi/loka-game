#pragma once

#include "OnlineGameMode.h"
#include "ConstructionGameMode.generated.h"

class UBaseLocalMessage;

UCLASS(Config=Game)
class AConstructionGameMode : public AOnlineGameMode
{
GENERATED_BODY()

	AConstructionGameMode();
	virtual void DefaultTimer() override;
	virtual void OnInitializeMatchInformation() override;
	virtual void StartPlay() override;
	virtual void PostLogin(APlayerController* NewPlayer) override;
	virtual void Logout(AController* Exiting) override;
	virtual AActor* ChoosePlayerStart_Implementation(AController* Player) override;

	virtual void EndMatch() override;

protected:

	UPROPERTY(Config)
	int32 TimeBeforeEndMatch;

	UPROPERTY()
	int32 CounterBeforeEndMatch;

	UPROPERTY()
	int32 TimeCounter;

	UPROPERTY()
	TMap<APlayerState*, uint8> KickCounter;

	UPROPERTY(EditDefaultsOnly, Category = Messages)
	TSubclassOf<UBaseLocalMessage> NotSquadMessage;

	UPROPERTY(EditDefaultsOnly, Category = Messages)
	TSubclassOf<UBaseLocalMessage> ServerShutdownIn;
};