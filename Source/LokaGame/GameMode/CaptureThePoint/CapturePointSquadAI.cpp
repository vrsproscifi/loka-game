// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#include "LokaGame.h"
#include "CapturePointSquadAI.h"
#include "CapturePoint.h"
#include "UTDefensePoint.h"
#include "UTBot.h"
#include "Weapon/ShooterWeapon.h"

ACapturePointSquadAI::ACapturePointSquadAI(const FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{
	Points.Reserve(3);
}

void ACapturePointSquadAI::Initialize(AUTTeamInfo* InTeam, FName InOrders)
{
	Super::Initialize(InTeam, InOrders);

	for (TActorIterator<ACapturePoint> It(GetWorld()); It; ++It)
	{
		Points.AddUnique(*It);
	}

	//if(InTeam && InTeam->)
	if (Orders == NAME_Attack)
	{
		SetObjective(FindNearCapturePoint());
	}
	else if (Orders == NAME_Defend)
	{
		SetObjective(FindNearCapturePoint(false));
	}

	LastPointChanged = .0f;
}

void ACapturePointSquadAI::SetObjective(AActor* InObjective)
{
	if (InObjective != Objective)
	{
		CapRoutes.Empty();
	}
	
	GamePointObjective = Cast<ACapturePoint>(InObjective);
	Super::SetObjective(InObjective);
}

bool ACapturePointSquadAI::MustKeepEnemy(APawn* TheEnemy)
{
	if (AUTCharacter* MyCharacter = Cast<AUTCharacter>(TheEnemy))
	{
		return (GamePointObjective && GamePointObjective->GetTeamNum() == GetTeamNum() && IsActorNearPoint(MyCharacter, GamePointObjective));
	}

	return false;
}

bool ACapturePointSquadAI::ShouldUseTranslocator(AUTBot* B)
{
	return false;
}

bool ACapturePointSquadAI::IsNearEnemyBase(const FVector& TestLoc)
{
	// return true if TestLoc near 30 meeters
	return (GamePointObjective && GamePointObjective->GetTeamNum() != GetTeamNum() && FVector::Dist(TestLoc, GamePointObjective->GetActorLocation()) <= 30.0f * 100.0f);
}

float ACapturePointSquadAI::ModifyEnemyRating(float CurrentRating, const FBotEnemyInfo& EnemyInfo, AUTBot* B)
{
	if ( EnemyInfo.GetUTChar() != nullptr && EnemyInfo.GetUTChar()->GetCarriedObject() != nullptr && EnemyInfo.GetUTChar()->GetCarriedObject()->GetTeamNum() == GetTeamNum() &&
		 B->CanAttack(EnemyInfo.GetPawn(), EnemyInfo.LastKnownLoc, false) )
	{
		if ( (B->GetPawn()->GetActorLocation() - EnemyInfo.LastKnownLoc).Size() < 3500.0f || (B->GetUTChar() != nullptr && B->GetUTChar()->GetWeapon() != nullptr && B->GetUTChar()->GetWeapon()->IsSniping()) ||
			IsNearEnemyBase(EnemyInfo.LastKnownLoc) )
		{
			return CurrentRating + 6.0f;
		}
		else
		{
			return CurrentRating + 1.5f;
		}
	}
	else
	{
		return CurrentRating;
	}
}

void ACapturePointSquadAI::DrawDebugSquadRoute(AUTBot* B) const
{
	if (B->GetUTChar() != nullptr && B->GetUTChar()->GetCarriedObject() != nullptr)
	{
		if (CapRoutes.IsValidIndex(B->UsingSquadRouteIndex))
		{
			DrawDebugRoute(GetWorld(), B->GetPawn(), CapRoutes[B->UsingSquadRouteIndex].RouteCache);
		}
	}
	else
	{
		Super::DrawDebugSquadRoute(B);
	}
}

bool ACapturePointSquadAI::TryPathTowardObjective(AUTBot* B, AActor* Goal, bool bAllowDetours, const FString& SuccessGoalString)
{
	// maintain a separate list of alternate routes for capturing the taken enemy flag (i.e. from enemy base to home base)
	if (Goal == GamePointObjective && FollowAlternateRoute(B, Goal, CapRoutes, bAllowDetours, SuccessGoalString))
	{
		return true;
	}
	else
	{
		bool bResult = Super::TryPathTowardObjective(B, Goal, bAllowDetours, SuccessGoalString);
		if (bResult && GamePointObjective != nullptr && (Goal == GamePointObjective || Goal == GamePointObjective->GetCarriedObject()) && B->GetRouteDist() < 2500 && B->LineOfSightTo(Goal))
		{
			B->SendVoiceMessage(StatusMessage::IGotFlag);
		}
		return bResult;
	}
}

void ACapturePointSquadAI::GetPossibleEnemyGoals(AUTBot* B, const FBotEnemyInfo* EnemyInfo, TArray<FPredictedGoal>& Goals)
{
	if (GamePointObjective != nullptr && GamePointObjective->GetCarriedObject() != nullptr && GamePointObjective->GetCarriedObject()->HoldingPawn == EnemyInfo->GetPawn())
	{
		// enemy flag carrier
		if (GamePointObjective != nullptr && GamePointObjective->GetCarriedObjectState() == CarriedObjectState::Home)
		{
			// enemy flag is home so clearly he's going there
			Goals.Add(FPredictedGoal(GamePointObjective->GetActorLocation() + FVector(0.0f, 0.0f, 45.0f), true));
		}
		else
		{
			if (GamePointObjective != nullptr)
			{
				// still might camp on the stand
				Goals.Add(FPredictedGoal(GamePointObjective->GetActorLocation(), false));
				// predict a hiding spot
				FHideLocEval NodeEval(FMath::FRand() < (0.07f * B->Skill + 0.5f * B->Personality.MapAwareness), FSphere(GamePointObjective->GetActorLocation(), (GamePointObjective->GetActorLocation() - GamePointObjective->GetActorLocation()).Size()));
				float Weight = 0.0f;
				TArray<FRouteCacheItem> EnemyHidingRoute;
				if (NavData->FindBestPath(EnemyInfo->GetPawn(), EnemyInfo->GetPawn()->GetNavAgentPropertiesRef(), B, NodeEval, EnemyInfo->LastKnownLoc, Weight, false, EnemyHidingRoute))
				{
					Goals.Add(FPredictedGoal(EnemyHidingRoute.Last().GetLocation(NULL), false));
				}
			}
			Super::GetPossibleEnemyGoals(B, EnemyInfo, Goals);
		}
	}
	else
	{
		// possibly headed to friendly flag
		if (GamePointObjective != nullptr && GamePointObjective->GetCarriedObject() != nullptr)
		{
			if (GamePointObjective->GetCarriedObjectState() == CarriedObjectState::Home)
			{
				Goals.Add(FPredictedGoal(GamePointObjective->GetActorLocation(), true));
			}
			else if (GamePointObjective->GetCarriedObject()->HoldingPawn != nullptr)
			{
				const FBotEnemyInfo* FCInfo = B->GetEnemyInfo(GamePointObjective->GetCarriedObject()->HoldingPawn, true);
				if (FCInfo != nullptr)
				{
					Goals.Add(FPredictedGoal(FCInfo->LastKnownLoc, false));
				}
			}
			else
			{
				// TODO: model of where flag might be, search around for it
				Goals.Add(FPredictedGoal(GamePointObjective->GetCarriedObject()->GetActorLocation(), true));
			}
		}
		// possibly headed to my team's flag carrier
		if (GamePointObjective != nullptr && GamePointObjective->GetCarriedObject() != nullptr && GamePointObjective->GetCarriedObjectState() != CarriedObjectState::Home)
		{
			if (GamePointObjective->GetCarriedObject()->HoldingPawn != nullptr)
			{
				Goals.Add(FPredictedGoal(GamePointObjective->GetCarriedObject()->HoldingPawn->GetNavAgentLocation(), true));
			}
			else
			{
				Goals.Add(FPredictedGoal(GamePointObjective->GetCarriedObject()->GetActorLocation(), true));
			}
		}

		Super::GetPossibleEnemyGoals(B, EnemyInfo, Goals);
	}
}

bool ACapturePointSquadAI::CheckSquadObjectives(AUTBot* B)
{
	MaybeChangeCapturePoint();

	FName CurrentOrders = GamePointObjective ? ((GamePointObjective->GetTeamNum() == B->GetTeamNum() && GamePointObjective->IsHold() == false) ? NAME_Defend : NAME_Attack) : GetCurrentOrders(B);
	
	if (GamePointObjective == nullptr || GamePointObjective->IsHold())
	{
		if (CurrentOrders == NAME_Attack)
		{
			SetObjective(FindNearCapturePoint(true, B));
		}
		else if (CurrentOrders == NAME_Defend)
		{
			SetObjective(FindNearCapturePoint(false, B));
		}
	}

	if (CurrentOrders == NAME_Defend)
	{
		SetDefensePointFor(B);
	}
	else
	{
		B->SetDefensePoint(nullptr);
	}

	// TODO: will need to redirect to vehicle for VCTF
	if (B->GetUTChar() != nullptr && B->GetUTChar()->GetCarriedObject() != nullptr)
	{
		return false;// SetFlagCarrierAction(B);
	}
	else if (CurrentOrders == NAME_Defend)
	{
		if (B->NeedsWeapon() && (GameObjective == nullptr || (B->GetPawn()->GetActorLocation() - GameObjective->GetActorLocation()).Size() > 3000.0f) && B->FindInventoryGoal(0.0f))
		{
			B->GoalString = FString::Printf(TEXT("Get inventory %s"), *GetNameSafe(B->RouteCache.Last().Actor.Get()));
			B->SetMoveTarget(B->RouteCache[0]);
			B->StartWaitForMove();
			return true;
		}
		else if (B->GetEnemy() != nullptr)
		{
			if (!B->LostContact(3.0f) || MustKeepEnemy(B->GetEnemy()))
			{
				B->GoalString = "Fight attacker";
				return false;
			}
			else if (CheckSuperPickups(B, 5000))
			{
				return true;
			}
			else if (B->GetDefensePoint() != nullptr)
			{
				return B->TryPathToward(B->GetDefensePoint(), true, false, "Go to defense point");
			}
			else
			{
				B->GoalString = "Fight attacker";
				return false;
			}
		}
		else if (Super::CheckSquadObjectives(B))
		{
			return true;
		}
		else if (B->FindInventoryGoal(0.0003f))
		{
			B->GoalString = FString::Printf(TEXT("Get inventory %s"), *GetNameSafe(B->RouteCache.Last().Actor.Get()));
			B->SetMoveTarget(B->RouteCache[0]);
			B->StartWaitForMove();
			return true;
		}
		else if (B->GetDefensePoint() != nullptr)
		{
			return B->TryPathToward(B->GetDefensePoint(), true, false, "Go to defense point");
		}
		else if (Objective != nullptr)
		{
			return B->TryPathToward(Objective, true, true, "Defend objective");
		}
		else
		{
			return false;
		}
	}
	else if (CurrentOrders == NAME_Attack)
	{
		if (B->NeedsWeapon() && B->FindInventoryGoal(0.0f))
		{
			B->GoalString = FString::Printf(TEXT("Get inventory %s"), *GetNameSafe(B->RouteCache.Last().Actor.Get()));
			B->SetMoveTarget(B->RouteCache[0]);
			B->StartWaitForMove();
			return true;
		}
		else if (GamePointObjective != nullptr)
		{
			if (GamePointObjective->GetTeamNum() == GetTeamNum() && GamePointObjective->GetCapturingTeam() != NONE_TEAM_ID && GamePointObjective->IsCapturing())
			{
				if (B->GetEnemy() != nullptr && B->LineOfSightTo(GameObjective))
				{
					return false;
				}
				else
				{
					return B->TryPathToward(GameObjective, true, true, "Find friendly flag carrier");
				}
			}
			else
			{
				if (GamePointObjective->IsInAnyTeamWithout(GetTeamNum()))
				{
					auto FindEnemies = B->GetEnemiesNear(GamePointObjective->GetActorLocation(), 50.0f * 100.0f, true);
					if (FindEnemies.Num() > 1)
					{
						B->SetEnemy(FindEnemies[FMath::RandRange(0, FindEnemies.Num() - 1)]);
						return false;
					}
					else
					{
						return B->TryPathToward(GameObjective, true, true, "Go to point and attack");
					}
				}
				else
				{
					return B->TryPathToward(GameObjective, false, true, "Go to point");
				}
			}
		}
		// prioritize grabbing some powerups just after respawning, less likely to do so after engaging enemy base
		// bias towards powerups more if less aggressive personality
		// skip if flag is out (prioritize getting hands on enemy flag ASAP)
		// note that even if this check never fires, standard pathing detours will still result in bots picking up powerups near their chosen assault path
		else if ( (GamePointObjective == nullptr) &&
					((B->GetEnemy() == nullptr && B->Personality.Aggressiveness <= 0.0f) || GetWorld()->TimeSeconds - B->LastRespawnTime < 10.0f * (1.0f - B->Personality.Aggressiveness)) &&
					CheckSuperPickups(B, 5000) )
		{
			return true;
		}
		else
		{
			return TryPathTowardObjective(B, Objective, true, "Attack objective");
		}
	}
	else if (Cast<APlayerController>(Leader) != nullptr && Leader->GetPawn() != nullptr)
	{
		if (B->NeedsWeapon() && B->FindInventoryGoal(0.0f))
		{
			B->GoalString = FString::Printf(TEXT("Get inventory %s"), *GetNameSafe(B->RouteCache.Last().Actor.Get()));
			B->SetMoveTarget(B->RouteCache[0]);
			B->StartWaitForMove();
			return true;
		}
		else if (B->GetEnemy() != nullptr && !B->LostContact(3.0f))
		{
			// fight!
			return false;
		}
		else
		{
			return B->TryPathToward(Leader->GetPawn(), true, true, "Find leader");
		}
	}
	else
	{
		// TODO: midfield - engage enemies, acquire powerups, attack when stacked
		return Super::CheckSquadObjectives(B);
	}
}

void ACapturePointSquadAI::NotifyObjectiveEvent(AActor* InObjective, AController* InstigatedBy, FName EventName)
{
	AUTGameObjective* InGameObjective = Cast<AUTGameObjective>(InObjective);
	if (InstigatedBy != nullptr && InGameObjective != nullptr && InGameObjective->GetCarriedObject() != nullptr && InGameObjective->GetCarriedObject()->Holder == InstigatedBy->PlayerState && Members.Contains(InstigatedBy))
	{
		// re-enable alternate paths for flag carrier so it can consider them for planning its escape
		AUTBot* B = Cast<AUTBot>(InstigatedBy);
		if (B != nullptr)
		{
			B->bDisableSquadRoutes = false;
			B->SquadRouteGoal.Clear();
			B->UsingSquadRouteIndex = INDEX_NONE;
		}
		SetLeader(InstigatedBy);
	}
	for (AController* C : Members)
	{
		AUTBot* B = Cast<AUTBot>(C);
		if (B != nullptr)
		{
			if (B->GetUTChar() != nullptr && B->GetUTChar()->GetCarriedObject() != nullptr)
			{
				// retask flag carrier immediately
				B->WhatToDoNext();
			}
			else if (B->GetMoveTarget().Actor != nullptr && (B->GetMoveTarget().Actor == InGameObjective || B->GetMoveTarget().Actor == InGameObjective->GetCarriedObject()))
			{
				SetRetaskTimer(B);
			}
		}
	}

	Super::NotifyObjectiveEvent(InObjective, InstigatedBy, EventName);
}

bool ACapturePointSquadAI::HasHighPriorityObjective(AUTBot* B)
{
	// if our flag is out and enemy's is safe, everyone needs to try to rectify that in some way or another
	if (GamePointObjective != nullptr && GamePointObjective != nullptr && GamePointObjective->GetCarriedObjectState() != CarriedObjectState::Home && GamePointObjective->GetCarriedObjectState() == CarriedObjectState::Home)
	{
		return true;
	}
	else
	{
		// otherwise only high priority if the flag this squad cares about is out
		return (GameObjective != nullptr && GameObjective->GetCarriedObjectState() != CarriedObjectState::Home && GameObjective->GetCarriedObjectHolder() != B->GetPlayerState<APlayerState>());
	}
}

ACapturePoint* ACapturePointSquadAI::FindNearCapturePoint(const bool IsEnemy, AUTBot* B) const
{
	ACapturePoint* ReturnPoint = nullptr;
	float LastDist = 1000.0f * 100.0f;
	for (auto point : Points)
	{
		if (IsEnemy ? (point->GetTeamNum() != GetTeamNum()) : (point->GetTeamNum() == GetTeamNum()))
		{
			const float Dist = FVector::Dist(point->GetActorLocation(), (B != nullptr && B->GetPawn()) ? B->GetPawn()->GetActorLocation() : GetActorLocation());
			if (Dist < LastDist)
			{
				LastDist = Dist;
				ReturnPoint = point;
			}
		}
	}

	return ReturnPoint;
}

bool ACapturePointSquadAI::IsActorNearPoint(AActor* InActor, ACapturePoint* InPoint) const
{
	return (InActor && InPoint && FVector::Dist(InActor->GetActorLocation(), InPoint->GetActorLocation()) <= 30.0f * 100.0f);
}

void ACapturePointSquadAI::MaybeChangeCapturePoint()
{
	if (GetWorld() && GetWorld()->GetTimeSeconds() > LastPointChanged)
	{
		LastPointChanged = GetWorld()->GetTimeSeconds() + FMath::FRandRange(10.0f, 40.0f);
		SetObjective(Points[FMath::RandRange(0, Points.Num() - 1)]);
	}	
}