// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#pragma once
#include "UTGameMode.h"
#include "ShooterGame_FreeForAll.generated.h"

UCLASS()
class AShooterGame_FreeForAll : public AUTGameMode
{
  GENERATED_BODY()

    //==================================================
public:
  AShooterGame_FreeForAll();
public:

	virtual void Killed(AController* Killer, AController* KilledPlayer, APawn* KilledPawn, TSubclassOf<UDamageType> DamageType) override;
	virtual bool IsAllowJoinInProgress() const override { return false; }
	virtual void RestartPlayer(class AController* NewPlayer) override;

protected:

	/** best player */
	UPROPERTY(transient)
	class AUTPlayerState* WinnerPlayerState;

	virtual void DefaultTimer() override;

	virtual void InitGameState() override;
	virtual void OnInitializeMatchInformation() override;

	/** check who won */
	virtual void DetermineMatchWinner() override;

	/** check if PlayerState is a winner */
	virtual bool IsWinner(class AUTPlayerState* PlayerState) const;
};
