﻿#include "LokaGame.h"
#include "ShooterGame_BuildingAttack.h"

#include "BuildSystem/UsableBuildPlacedComponent.h"

#include "UTPlayerState.h"
#include "UTGameState.h"
#include "UTPlayerController.h"
#include "GameModeTypeId.h"



#include "UTTeamInfo.h"

AShooterGame_BuildingAttack::AShooterGame_BuildingAttack()
	: Super()
{
	bTeamGame = true;
	bDelayedStart = true;
}

void AShooterGame_BuildingAttack::StartPlay()
{
	//======================================================================
	//	Нужно использовать AGameMode::StartPlay, что бы вручную вызвать SendStartMatchRequest после того как будет получен список построек.
	AGameMode::StartPlay();

	//buildingController.GetListOfBuildings.OnSuccess.AddUObject(this, &AShooterGame_BuildingAttack::OnBuildingsList);
	//buildingController.GetListOfBuildings.OnFailed.AddLambda([&, this] {
	//	if (buildingController.GetListOfBuildings.Attemp++ < 5)
	//	{
	//		buildingController.GetListOfBuildings.Request();
	//	}
	//	else
	//	{
	//		UE_LOG(UT, Error, TEXT("Failed GetListOfBuildings"));
	//	}
	//});
	//
	////======================================================================
	//ContextValidator = IOperationContainer::ContextValidatorValue;
	//IOperationContainer::Context = this;
	//IOperationContainer::AllowHttpRequest = true;
	//IOperationContainer::ContextValidator = &ContextValidator;
	//
	//buildingController.GetListOfBuildings.Request();
}

/*
	Выдаем игроку то что он заработал за бой, сколько смог разграбить каждый игрок - столько и получает.
	ћастер сервер умеет закидывать отр¤ды в аттаку. ¬ конце бо¤ мы отправл¤ем два запроса - стандартное окончание бо¤,
	второй - информаци¤ о состо¤нии построек: ид, здоровье и сколько было украдено ресурсов.
	—колько было украдено, а не осталось! Ёто нужно что бы правильно вычесть награбленное, т.к:
	1. Добыча ресурсов продолжаетс¤
	2. Игрок может в это врем¤ задонатить
	3. Ќападающий грабит не весь склад игрока, а только какую то часть

*/
void AShooterGame_BuildingAttack::EndMatch()
{
	TeamAttackWinningScore = 0;
	//FMatchBuildingUpdateContainer DamagedBuildings;
	//
	//for (auto building : DamagedBuildingsList)
	//{
	//	FMatchBuildingView buildingState;
	//	buildingState.ItemId = building.Key.ToString();
	//	buildingState.Health = IDestroyableActorInterface::Execute_GetCurrentHealth(building.Value);
	//	buildingState.Storage = 0;
	//
	//	if (auto StorageBuilding = Cast<AUsableBuildPlacedComponent>(building.Value))
	//	{
	//		if (StorageBuilding->PlacedBuildingData.Contains(TEXT("Storage")))
	//		{
	//			auto Value = FMath::CeilToInt(float(StorageBuilding->PlacedBuildingData.FindChecked(TEXT("Storage"))) * .3f);
	//			TeamAttackWinningScore += Value;
	//			buildingState.Storage = Value;
	//		}			
	//	}
	//
	//	DamagedBuildings.Items.Add(buildingState);
	//}

#if UE_SERVER && !WITH_EDITOR
	//serverController->OnUpdateBuilding.Request(DamagedBuildings);
#endif

	Super::EndMatch();
}

void AShooterGame_BuildingAttack::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	//IOperationContainer::AllowHttpRequest = false;

	Super::EndPlay(EndPlayReason);
}

void AShooterGame_BuildingAttack::InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage)
{
	Super::InitGame(MapName, Options, ErrorMessage);

	bDeveloperIsEnemy = (UGameplayStatics::GetIntOption(Options, TEXT("DeveloperIsEnemy"), 0) >= 1);
}

void AShooterGame_BuildingAttack::InitGameState()
{
	Super::InitGameState();
}

void AShooterGame_BuildingAttack::DefaultTimer()
{
	Super::DefaultTimer();

	if (GetMatchState() == MatchState::InProgress && GeneralTargetBuilding && IDestroyableActorInterface::Execute_IsDying(GeneralTargetBuilding))
	{
		EndMatch();
	}
}

void AShooterGame_BuildingAttack::OnBuildingsList(const TArray<FWorldBuildingItemView>& InList)
{
	//if (auto GameInst = Cast<UShooterGameInstance>(GetGameInstance()))
	//{
	//	UE_LOG(LogShooter, Log, TEXT("Pre Begin spawn buildings >> ItemsNum:%d"), InList.Num());

	//	TArray<ABuildPlacedComponent*> ArrayPlacedBuildings;
	//	TArray<AActor*> FindPlacedBuildings;
	//	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ABuildPlacedComponent::StaticClass(), FindPlacedBuildings);

	//	for (auto PlacedBuilding : FindPlacedBuildings)
	//	{
	//		auto PrePlacedBuilding = Cast<ABuildPlacedComponent>(PlacedBuilding);
	//		if (PrePlacedBuilding && PrePlacedBuilding->IsPrePlaced())
	//		{
	//			ArrayPlacedBuildings.Add(PrePlacedBuilding);
	//		}
	//	}

	//	FActorSpawnParameters ActorSpawnParameters;
	//	ActorSpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	//	ActorSpawnParameters.Owner = UTGameState;
	//	ActorSpawnParameters.Instigator = nullptr;

	//	auto InstRepository = &GameInst->EntityRepository->InstanceRepository;

	//	// Spawn
	//	for (auto Item : InList)
	//	{
	//		UE_LOG(LogShooter, Log, TEXT("Begin spawn building >> ItemId:%s"), *Item.ItemId);
	//		if (InstRepository->Build[Item.ModelId] && InstRepository->Build[Item.ModelId]->ComponentTemplate)
	//		{
	//			ABuildPlacedComponent* SpawnedActor = nullptr;
	//			for (auto preplaced : ArrayPlacedBuildings)
	//			{
	//				if (preplaced && preplaced->GetPrePlacedInstanceId() == Item.ModelId)
	//				{
	//					SpawnedActor = preplaced;
	//					break;
	//				}
	//			}

	//			if (SpawnedActor == nullptr)
	//			{
	//				SpawnedActor = GetWorld()->SpawnActor<ABuildPlacedComponent>(InstRepository->Build[Item.ModelId]->ComponentTemplate, FTransform(Item.Rotation, Item.Position), ActorSpawnParameters);
	//			}

	//			if (SpawnedActor)
	//			{
	//				FGuid::Parse(Item.ItemId, SpawnedActor->GUID);
	//				SpawnedActor->InitializeInstance(InstRepository->Build[Item.ModelId]);
	//				SpawnedActor->OnComponentPlaced(true);
	//				SpawnedActor->Health.X = FMath::Min<float>(Item.Health, SpawnedActor->Health.Y);

	//				int32 BuildingType = SpawnedActor->Instance->GetBuildingType();
	//				if (FFlagsHelper::HasAnyFlags<int32>(BuildingType, FFlagsHelper::ToFlag<int32>(EBuildingType::WarehouseMoney) | FFlagsHelper::ToFlag<int32>(EBuildingType::MinerMoney)))
	//				{
	//					SpawnedActor->PlacedBuildingData.FindOrAdd(TEXT("Storage")) = Item.Storage;
	//					SpawnedActor->ReInitializeBuildingData(false);

	//					StorageComponents.Add(SpawnedActor->GUID, SpawnedActor);

	//					UE_LOG(LogShooter, Log, TEXT("Process of spawn building >> ItemId:%s, this is storage component."), *Item.ItemId);
	//				}

	//				BuildingsList.Add(SpawnedActor->GUID, SpawnedActor);

	//				SpawnedActor->OnComponentTakeDamage.AddDynamic(this, &AShooterGame_BuildingAttack::OnComponentTakeDamage);
	//				SpawnedActor->OnComponentDeath.AddDynamic(this, &AShooterGame_BuildingAttack::OnComponentDeath);

	//				if (SpawnedActor->GetPrePlacedInstanceId() == ItemBuildModelId::LobbyMainBuilding)
	//				{
	//					GeneralTargetBuilding = SpawnedActor;
	//					UE_LOG(LogShooter, Log, TEXT("Process of spawn building >> ItemId:%s, this is general component."), *Item.ItemId);
	//				}
	//			}
	//		}
	//	}

	//	// Links
	//	for (auto Item : InList)
	//	{
	//		FGuid TargetGuid, TargetGuidParent;
	//		FGuid::Parse(Item.ItemId, TargetGuid);
	//		if (BuildingsList.Contains(TargetGuid) == false) continue;
	//		auto Building = BuildingsList.FindChecked(TargetGuid);

	//		for (auto Parents : Item.Sockets)
	//		{
	//			FGuid::Parse(Parents.TargetId, TargetGuidParent);
	//			if (BuildingsList.Contains(TargetGuidParent) == false) continue;
	//			auto BuildingParent = BuildingsList.FindChecked(TargetGuidParent);

	//			BuildingParent->ChildBuildComponents.Add(Building);
	//			Building->ParentBuildComponents.Add(BuildingParent);
	//		}
	//	}

	//	//======================================================================
	//	//	ќтправл¤ем уведомление о том что сервер готов принимать игроков.
	//	SendStartMatchRequest();
	//}
	//else
	//{
	//	UE_LOG(UT, Error, TEXT("Failed Cast GameInstance [%s] to UShooterGameInstance"), *GetGameInstance()->GetFullName());
	//}
}

void AShooterGame_BuildingAttack::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);

#if WITH_EDITOR
	if (bDeveloperIsEnemy == false)
	{
		OwnerState = NewPlayer ? NewPlayer->GetPlayerState<AUTPlayerState>() : nullptr;
		OwnerTeam = OwnerState ? OwnerState->Team : nullptr;
	}
	else
	{
		auto TargetPS = NewPlayer ? NewPlayer->GetPlayerState<AUTPlayerState>() : nullptr;
		for (auto team : UTGameState->Teams)
		{
			if (team && team != TargetPS->Team)
			{
				OwnerTeam = team;
				break;
			}
		}
	}
#endif

#if UE_SERVER && !WITH_EDITOR
	for (auto team : UTGameState->Teams)
	{
		if (team)
		{
			UE_LOG(LogShooter, Log, TEXT("Find owner team >> OwnerId:%s, TeamId:%s, TeamIndex:%d"), *OwnerId.ToString(), *team->TeamId.ToString(), team->GetTeamNum());
		}

		if (team && team->TeamId == OwnerId)
		{
			OwnerTeam = team;
			UE_LOG(LogShooter, Log, TEXT("Try set owner team from id %s"), *OwnerId.ToString());

			for (auto member : team->GetTeamMembers())
			{
				if (member && member->GetPlayerState())
				{
					if (auto TargetPS = Cast<AUTPlayerState>(member->GetPlayerState()))
					{
						if (TargetPS->GetPlayerName() == OwnerName)
						{
							OwnerState = TargetPS;							
							UE_LOG(LogShooter, Log, TEXT("Try set owner from player %s"), *TargetPS->GetPlayerName());
							break;
						}
					}
				}
			}
		}
	}

	if (NumPlayers >= 1 && GetMatchState() == MatchState::WaitingToStart && UTGameState->GetRemainingTime() > Time_WaitingStartMatch)
	{
		UTGameState->SetRemainingTime(Time_WaitingStartMatch);
	}
#endif

	SwitchTeamsIfNeed();
}

void AShooterGame_BuildingAttack::HandleMatchHasStarted()
{
	Super::HandleMatchHasStarted();
}

void AShooterGame_BuildingAttack::DetermineMatchWinner()
{
	if (GeneralTargetBuilding && IDestroyableActorInterface::Execute_IsDying(GeneralTargetBuilding) == false)
	{
		if (OwnerTeam)
		{
			MatchWinner = OwnerTeam->GetTeamNum();
		}
	}
	else
	{
		for (auto team : UTGameState->Teams)
		{
			if (team && OwnerTeam && team != OwnerTeam)
			{
				MatchWinner = team->GetTeamNum();
				break;
			}
		}
	}
}

void AShooterGame_BuildingAttack::AddExtraResult(AUTPlayerState* PlayerState)
{
	Super::AddExtraResult(PlayerState);

	if (UTGameState && PlayerState && PlayerState->Team && PlayerState->Team != OwnerTeam)
	{
		auto GiveMoney = FMath::CeilToInt(float(TeamAttackWinningScore) / float(PlayerState->Team->GetTeamMembers().Num()));
		PlayerState->GiveCash(EGameCurrency::Money, GiveMoney);
	}
}

void AShooterGame_BuildingAttack::OnComponentTakeDamage(ABuildPlacedComponent* InComponent)
{
	if (InComponent)
	{
		auto& Component = DamagedBuildingsList.FindOrAdd(InComponent->GUID);
		Component = InComponent;
	}
}

void AShooterGame_BuildingAttack::OnComponentDeath(ABuildPlacedComponent* InComponent)
{
	if (InComponent)
	{
		auto& Component = DamagedBuildingsList.FindOrAdd(InComponent->GUID);
		Component = InComponent;

		StorageComponents.Remove(InComponent->GUID);

		if (UTGameState)
		{
			UTGameState->GiveTeamScore(ATTACK_ON_PLAYER_TEAM, Component->RewardTotalScoreTeam);

			for (auto &i : Component->DamageByPlayers)
			{
				if (i.Value > .0f)
				{
					int32 ScaledScore = FMath::CeilToInt(Component->RewardTotalScorePlayer * (i.Value / Component->Health.Y));

					auto TargetPS = Cast<AUTPlayerState>(i.Key);
					auto TargetPC = Cast<AUTPlayerController>(i.Key->GetOwner());

					if (TargetPC && TargetPS)
					{
						if (UTGameState->OnSameTeam(TargetPS, InComponent))
						{
							ScaledScore = -ScaledScore;
						}

						TargetPS->ScorePoints(ScaledScore);
						TargetPC->ClientShowActionMessage(EActionMessage::BuildingKill, ScaledScore);
					}
				}
			}
		}
	}
}

void AShooterGame_BuildingAttack::SwitchTeamsIfNeed()
{
	if (UTGameState->Teams.Num() > 1)
	{
		if (OwnerTeam == UTGameState->Teams[0])
		{
			UTGameState->Teams[0]->TeamIndex = DEFEND_ON_PLAYER_TEAM;
			UTGameState->Teams[1]->TeamIndex = ATTACK_ON_PLAYER_TEAM;
		}
		else
		{
			UTGameState->Teams[1]->TeamIndex = DEFEND_ON_PLAYER_TEAM;
			UTGameState->Teams[0]->TeamIndex = ATTACK_ON_PLAYER_TEAM;
		}	

		if (OwnerState || OwnerTeam)
		{
			for (auto Building : BuildingsList)
			{
				if (Building.Value && Building.Value->IsValidLowLevel())
				{
					AActor* OwnerActor = (OwnerState != nullptr) ? Cast<AActor>(OwnerState) : Cast<AActor>(OwnerTeam);
					Building.Value->SetOwner(OwnerActor);
				}
			}
		}
	}
}