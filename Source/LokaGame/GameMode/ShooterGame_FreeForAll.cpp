// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "LokaGame.h"
#include "ShooterGame_FreeForAll.h"
#include "UTPlayerState.h"
#include "UTGameState.h"
#include "UTPlayerController.h"
#include "GameModeTypeId.h"

#include "UTTeamInfo.h"
#include "Shared/SessionMatchOptions.h"

AShooterGame_FreeForAll::AShooterGame_FreeForAll()
	: Super()
	, WinnerPlayerState(nullptr)
{
	bDelayedStart = true;
}

void AShooterGame_FreeForAll::InitGameState()
{
	Super::InitGameState();
}

void AShooterGame_FreeForAll::OnInitializeMatchInformation()
{
	Super::OnInitializeMatchInformation();
}

void AShooterGame_FreeForAll::DefaultTimer()
{
	Super::DefaultTimer();

	UE_LOG(UT, Error, TEXT("  AShooterGame_FreeForAll::DefaultTimer: %s"), *GetMatchState().ToString());
	if (GetMatchState() == MatchState::InProgress)
	{
		if (UTGameState)
		{
			//----------------------------------------------
			SIZE_T alivePlayers = 0;
			SIZE_T realPlayers = 0;
			for (auto &p : UTGameState->PlayerArray)
			{
				if (auto t = Cast<AUTPlayerState>(p))
				{
					if (t->bIsABot == false)
					{
						realPlayers++;
					}

					if (t->GetDeaths() < GetMatchOptions().GetNumberOfLives())
					{
						if (t->bIsDemoRecording == false && t->IsQuitter() == false)
						{
							t->Score += 10.0f;
							alivePlayers++;
						}
					}	

					// Make team if null
					if (t->Team == nullptr)
					{
						if (auto NewTeam = GetWorld()->SpawnActor<AUTTeamInfo>(AUTTeamInfo::StaticClass()))
						{
							NewTeam->TeamId = t->TeamId.IsValid() ? t->TeamId : FGuid::NewGuid();
							NewTeam->AddToTeam(Cast<AController>(t->GetOwner()));
							const auto TeamIndex = UTGameState->Teams.Add(NewTeam);
							NewTeam->TeamIndex = TeamIndex;
						}
					}
				}
			}

			UE_LOG(UT, Error, TEXT("  AShooterGame_FreeForAll::DefaultTimer: %s | NumPlayers: %d | NumBots: %d | realPlayers: %d | alivePlayers: %d"), *GetMatchState().ToString(), NumPlayers, NumBots, realPlayers, alivePlayers);
#if WITH_EDITOR
			if (alivePlayers <= 1)
#else
			if ((realPlayers < 1 && NumPlayers == 1) || (realPlayers <= 1 && NumPlayers > 1) || alivePlayers <= 1)
#endif
			{
				EndMatch();
			}
			else
			{
				if ((UTGameState->GetRemainingTime() < 120 || alivePlayers <= 5) && UTGameState->GetGameModeType() == EGameMode::LostDeadMatch)
				{
					// Show every 15 seconds
					if (UTGameState->GetRemainingTime() % 15 == 0)
					{
						for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
						{
							if (auto PlayerPC = Cast<AUTPlayerController>(*It))
							{
								PlayerPC->MakeLastAliveShow();
							}
						}
					}
				}
			}
		}	
		else
		{
			UE_LOG(UT, Error, TEXT("  AShooterGame_FreeForAll::Killed | UTGameState was nullptr"));
		}
	}
}

void AShooterGame_FreeForAll::Killed(AController* Killer, AController* KilledPlayer, APawn* KilledPawn, TSubclassOf<UDamageType> DamageType)
{
	Super::Killed(Killer, KilledPlayer, KilledPawn, DamageType);
	
	if (UTGameState)
	{
		{
			const AUTPlayerState* const VictimPlayerState = KilledPlayer ? (KilledPlayer->GetPlayerState<AUTPlayerState>()) : NULL;
			if (VictimPlayerState && VictimPlayerState->GetDeaths() >= GetMatchOptions().GetNumberOfLives())
			{
				AUTPlayerController* VictimPlayerController = Cast<AUTPlayerController>(KilledPlayer);
				if (VictimPlayerController)
				{
					VictimPlayerController->StartSpectatingOnly();
				}
			}
		}

	}
	else
	{
		UE_LOG(UT, Error, TEXT("  AShooterGame_FreeForAll::Killed | UTGameState was nullptr"));
	}
}

void AShooterGame_FreeForAll::DetermineMatchWinner()
{
	float BestScore = .0f;
	AUTPlayerState* BestPlayerState = nullptr;

	WinnerPlayerState = nullptr;
	for (int32 i = 0; i < UTGameState->PlayerArray.Num(); i++)
	{
		if (auto ShooterState = Cast<AUTPlayerState>(UTGameState->PlayerArray[i]))
		{
			if (ShooterState->IsQuitter() == false && ShooterState->GetDeaths() < GetMatchOptions().GetNumberOfLives())
			{
				const float PlayerScore = ShooterState->GetScore();
				if (BestScore < PlayerScore)
				{
					BestScore = PlayerScore;
					BestPlayerState = ShooterState;
				}
			}
		}
	}

	if (BestPlayerState && UTGameState)
	{
		WinnerPlayerState = BestPlayerState;
		BestPlayerState->AddExtraScore(GET_PERCENT_OF(BestPlayerState->GetScore(), 20.0f));
		MatchWinner = BestPlayerState->GetTeamNum();
	}
}

bool AShooterGame_FreeForAll::IsWinner(AUTPlayerState* PlayerState) const
{
	return PlayerState && PlayerState->IsQuitter() == false && PlayerState == WinnerPlayerState;
}

void AShooterGame_FreeForAll::RestartPlayer(class AController* NewPlayer)
{
	if (auto PlayerPC = Cast<AUTPlayerController>(NewPlayer))
	{
		Super::RestartPlayer(NewPlayer);
	}
	else
	{
		AUTGameState* const MyGameState = Cast<AUTGameState>(GameState);
		if (auto OtherState = NewPlayer->GetPlayerState<AUTPlayerState>())
		{
			if (MyGameState && OtherState->GetDeaths() < GetMatchOptions().GetNumberOfLives())
			{
				Super::RestartPlayer(NewPlayer);
			}
		}
	}
}