// VRSPRO

#pragma once
#include "TypeCost.h"
#include "GameMode/ShooterGame_FreeForAll.h"
#include "InventoryHacks.h"
#include "ShooterGame_Duel.generated.h"


class AUTPlayerState;

UCLASS()
class LOKAGAME_API AShooterGame_Duel : public AShooterGame_FreeForAll
{
	GENERATED_BODY()
public:
	
	AShooterGame_Duel();

	virtual void Killed(AController* Killer, AController* KilledPlayer, APawn* KilledPawn, TSubclassOf<UDamageType> DamageType) override;
	virtual void InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage) override;
	virtual void OnInitializeMatchInformation() override;

	UPROPERTY()
	FTypeCost Bet;

protected:

	virtual void DefaultTimer() override;
	virtual void InitGameState() override;	
	virtual void Logout(AController * Exiting) override;
	virtual void PostLogin(APlayerController* NewPlayer) override;
	virtual void DetermineMatchWinner() override;
	virtual void AddExtraResult(AUTPlayerState* PlayerState);
	virtual void StartPlay() override;

	UPROPERTY()
	TArray<AUTPlayerState*> ExitingPlayerState;

	UPROPERTY()
	int32 ExitingRemainingTime;

	UPROPERTY()
	FProfileHack Profile;
};
