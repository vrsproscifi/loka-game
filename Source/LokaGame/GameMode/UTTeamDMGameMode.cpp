// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#include "LokaGame.h"
#include "UTTeamGameMode.h"
#include "UTTeamDMGameMode.h"
#include "StatNames.h"

#include "UTGameState.h"
#include "UTTeamInfo.h"
#include "UTPlayerState.h"

AUTTeamDMGameMode::AUTTeamDMGameMode()
	: Super()
{
	bScoreSuicides = true;
	bScoreTeamKills = true;
	DisplayName = NSLOCTEXT("UTGameMode", "TDM", "Team Deathmatch");
}

void AUTTeamDMGameMode::ScoreTeamKill_Implementation(AController* Killer, AController* Other, APawn* KilledPawn, TSubclassOf<UDamageType> DamageType)
{
	AUTPlayerState* KillerState = (Killer != nullptr) ?Killer->GetPlayerState<AUTPlayerState>() : NULL;
	AUTPlayerState* VictimState = (Other != nullptr) ? Other->GetPlayerState<AUTPlayerState>() : NULL;
	if (VictimState && VictimState->Team && KillerState && KillerState->Team)
	{
		if (bScoreTeamKills)
		{
			int32 ScoreChange = -1;
			KillerState->AdjustScore(ScoreChange); // @TODO FIXMESTEVE track team kills
			KillerState->Team->Score += ScoreChange;
			KillerState->Team->ForceNetUpdate();
		}
	}

	AddKillEventToReplay(Killer, Other, DamageType);
}

void AUTTeamDMGameMode::ScoreKill_Implementation(AController* Killer, AController* Other, APawn* KilledPawn, TSubclassOf<UDamageType> DamageType)
{
	AUTPlayerState* KillerState = (Killer != nullptr) ? Killer->GetPlayerState<AUTPlayerState>() : NULL;
	AUTPlayerState* VictimState = (Other != nullptr) ? Other->GetPlayerState<AUTPlayerState>() : NULL;
	if (VictimState != nullptr && VictimState->Team != nullptr)
	{
		int32 ScoreChange = 0;
		if (Killer == nullptr || Killer == Other)
		{
			if (bScoreSuicides)
			{
				ScoreChange = -1;
			}
		}
		else if (Cast<AUTGameState>(GameState)->OnSameTeam(Killer, Other))
		{
			if (bScoreTeamKills)
			{
				ScoreChange = -1;
			}
		}
		else
		{
			ScoreChange = 1;
		}

		if (KillerState != nullptr && KillerState->Team != nullptr)
		{
			KillerState->Team->Score += ScoreChange;
			KillerState->Team->ForceNetUpdate();
			if (!bHasBroadcastDominating)
			{
				int32 BestScore = 0;
				for (int32 i = 0; i < Teams.Num(); i++)
				{
					if ((Teams[i] != KillerState->Team) && (Teams[i]->Score >= BestScore))
					{
						BestScore = Teams[i]->Score;
					}
				}
				if (KillerState->Team->Score >= BestScore + 20)
				{
					bHasBroadcastDominating = true;
					//BroadcastLocalized(this, UUTCTFGameMessage::StaticClass(), 10, KillerState, NULL, KillerState->Team);
				}
			}
		}
		else
		{
			VictimState->Team->Score += ScoreChange;
			VictimState->Team->ForceNetUpdate();
		}
	}

	Super::ScoreKill_Implementation(Killer, Other, KilledPawn, DamageType);
}

AUTPlayerState* AUTTeamDMGameMode::IsThereAWinner_Implementation(bool& bTied)
{
	AUTTeamInfo* BestTeam = NULL;
	bTied = false;
	for (int32 i = 0; i < UTGameState->Teams.Num(); i++)
	{
		if (UTGameState->Teams[i] != nullptr)
		{
			if (BestTeam == nullptr || UTGameState->Teams[i]->Score > BestTeam->Score)
			{
				BestTeam = UTGameState->Teams[i];
				bTied = false;
			}
			else if (UTGameState->Teams[i]->Score == BestTeam->Score)
			{
				bTied = true;
			}
		}
	}

	AUTPlayerState* BestPlayer = NULL;
	if (!bTied)
	{
		float BestScore = 0.0;

		for (int32 PlayerIdx = 0; PlayerIdx < UTGameState->PlayerArray.Num();PlayerIdx++)
		{
			AUTPlayerState* PS = Cast<AUTPlayerState>(UTGameState->PlayerArray[PlayerIdx]);
			if (PS != nullptr && PS->Team == BestTeam)
			{
				if (BestPlayer == nullptr || PS->Score > BestScore)
				{
					BestPlayer = PS;
					BestScore = BestPlayer->Score;
				}
			}
		}
	}
	return BestPlayer;
}