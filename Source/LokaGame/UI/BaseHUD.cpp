// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "BaseHUD.h"
#include "Interfaces/IconsDrawInterface.h"
#include "GameVersion.h"
#include "BasePlayerState.h"
#include "UTGameUserSettings.h"

DECLARE_CYCLE_STAT(TEXT("BaseHUD:DrawHUD Time"), STAT_BaseHUD_DrawHUD, STATGROUP_UI);
DECLARE_CYCLE_STAT(TEXT("BaseHUD:DrawWorldIcons Time"), STAT_BaseHUD_DrawWorldIcons, STATGROUP_UI);

ABaseHUD::ABaseHUD()
	: Super()
{
#if !UE_SERVER // Don't use on server, give crash
	static ConstructorHelpers::FObjectFinder<UFont> TFont(TEXT("Font'/Game/1LOKAgame/UserInterface/Fonts/Bender.Bender'"));
	BaseFonts.Tiny = TFont.Object;

	static ConstructorHelpers::FObjectFinder<UFont> SFont(TEXT("Font'/Game/1LOKAgame/UserInterface/Fonts/Bender.Bender'"));
	BaseFonts.Small = SFont.Object;

	static ConstructorHelpers::FObjectFinder<UFont> MFont(TEXT("Font'/Game/1LOKAgame/UserInterface/Fonts/Days.Days'"));
	BaseFonts.Medium = MFont.Object;

	static ConstructorHelpers::FObjectFinder<UFont> BFont(TEXT("Font'/Game/1LOKAgame/UserInterface/Fonts/Days.Days'"));
	BaseFonts.Big = BFont.Object;

	static ConstructorHelpers::FObjectFinder<UFont> LFont(TEXT("Font'/Game/1LOKAgame/UserInterface/Fonts/AeromaticsNC.AeromaticsNC'"));
	BaseFonts.Large = LFont.Object;

	static ConstructorHelpers::FObjectFinder<UFont> NormFont(TEXT("Font'/Game/1LOKAgame/UserInterface/Fonts/Days.Days'"));
	BaseFonts.Normal = NormFont.Object;

	static ConstructorHelpers::FObjectFinder<UFont> NumFont(TEXT("Font'/Game/1LOKAgame/UserInterface/Fonts/Research_Remix.Research_Remix'"));
	BaseFonts.Number = NumFont.Object;

	static ConstructorHelpers::FObjectFinder<UFont> AFont(TEXT("Font'/Game/1LOKAgame/UserInterface/Fonts/FontAwesome.FontAwesome'"));
	BaseFonts.Awesome = AFont.Object;
#endif
}

void ABaseHUD::DrawHUD()
{
	SCOPE_CYCLE_COUNTER(STAT_BaseHUD_DrawHUD);

	if (!IsPendingKillPending() && !IsPendingKill())
	{
		Super::DrawHUD();

		if (bShowHUD)
		{
			DrawWorldIcons();
			DrawGameVersion();
			DrawDebugInfo();
		}
	}
}

bool ABaseHUD::CorrectProject(const FVector &Location, FVector2D &OutScreen)
{
	if (Canvas && Canvas->SceneView)
	{
		FPlane ScreenPosition3D = Canvas->SceneView->Project(Location);

		ScreenPosition3D.X = (Canvas->ClipX / 2.f) + (ScreenPosition3D.X*(Canvas->ClipX / 2.f));
		ScreenPosition3D.Y *= -1.f * GProjectionSignY;
		ScreenPosition3D.Y = (Canvas->ClipY / 2.f) + (ScreenPosition3D.Y*(Canvas->ClipY / 2.f));

		OutScreen.X = ScreenPosition3D.X;
		OutScreen.Y = ScreenPosition3D.Y;

		return ScreenPosition3D.W > .0f;
	}

	return false;
}

void ABaseHUD::DrawWorldIcons()
{
	SCOPE_CYCLE_COUNTER(STAT_BaseHUD_DrawWorldIcons);

	const float ScaleUI = Canvas->ClipY / 1080.0f;
	FVector2D Pos;

	TArray<AActor*> Actors;
	UGameplayStatics::GetAllActorsWithInterface(this, UIconsDrawInterface::StaticClass(), Actors);
	for (auto TargetActor : Actors)
	{
		if (auto CastedActor = Cast<IIconsDrawInterface>(TargetActor))
		{
			if (CorrectProject(CastedActor->GetWorldIconLocation(), Pos) || CastedActor->IsValidAltWorldIconCheck(this))
			{
				IIconsDrawInterface::Execute_DrawWorldIcon(TargetActor, this, Canvas, Pos, ScaleUI);
			}
		}
	}
}

void ABaseHUD::DrawGameVersion()
{
	FVector2D TextSize;
	Canvas->TextSize(BaseFonts.Number, GAME_VERSION_STRING, TextSize.X, TextSize.Y);

	FCanvasTextItem TextItem(FVector2D(Canvas->ClipX - TextSize.X - 5.0f, Canvas->ClipY - TextSize.Y - 2.0f), FText::FromString(GAME_VERSION_STRING), BaseFonts.Number, FColor::White.WithAlpha(64));
	Canvas->DrawItem(TextItem);
}

void ABaseHUD::DrawDebugInfo()
{
	UUTGameUserSettings* MyGameSettings = Cast<UUTGameUserSettings>(UGameUserSettings::GetGameUserSettings());
	ABasePlayerState* MyPlayerState = PlayerOwner ? Cast<ABasePlayerState>(PlayerOwner->PlayerState) : nullptr;
	if (MyPlayerState && MyGameSettings)
	{
		if (MyGameSettings->IsShowPing())
		{
			const int32 PingValue = FMath::FloorToInt(MyPlayerState->ExactPing);
			FCanvasTextItem TextPing(FVector2D(20.0f, (Canvas->SizeY / 3) - 20), FText::Format(FText::FromString("Ping: {0} ms"), FText::AsNumber(PingValue)), BaseFonts.Tiny, (PingValue >= 300 ? FColor::Red : (PingValue >= 150) ? FColor::Yellow : FColor::Green));
			TextPing.bCentreX = false;
			TextPing.bCentreY = false;
			TextPing.bOutlined = true;
			TextPing.OutlineColor = FColor::Black;
			Canvas->DrawItem(TextPing);
		}

		if (MyGameSettings->IsShowFPS())
		{
			const int32 FPSValue = FMath::FloorToInt(1.0f / FSlateApplication::Get().GetAverageDeltaTime());
			FCanvasTextItem TextFPS(FVector2D(20.0f, Canvas->SizeY / 3), FText::Format(FText::FromString("FPS: {0}"), FText::AsNumber(FPSValue)), BaseFonts.Tiny, (FPSValue >= 30 ? FColor::Green : (FPSValue >= 15) ? FColor::Yellow : FColor::Red));
			TextFPS.bCentreX = false;
			TextFPS.bCentreY = false;
			TextFPS.bOutlined = true;
			TextFPS.OutlineColor = FColor::Black;
			Canvas->DrawItem(TextFPS);
		}
	}
}
