// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "ItemTransportEntity.h"


UItemTransportEntity::UItemTransportEntity()
	: Super()
{
	EntityDescription.CategoryType = ECategoryTypeId::Transport;

	IsAllowToBuy = true;
}

TSubclassOf<ATransportBase> UItemTransportEntity::GetTransportTemplate() const
{
	return TransportTemplate;
}
