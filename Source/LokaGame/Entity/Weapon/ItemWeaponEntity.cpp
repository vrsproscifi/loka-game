// VRSPRO

#include "LokaGame.h"
#include "ItemWeaponEntity.h"
#include "Ammo/ItemAmmoEntity.h"
#include "Module/ItemModuleEntity.h"
#include "StoreController/Models/ItemBaseContainer.h"

#include "Engine/ActorChannel.h"
#include "PhysicsEngine/BodySetup.h"
#include "PlayerInventoryItemWeapon.h"

// Sets default values
UItemWeaponEntity::UItemWeaponEntity()
{
	InventoryClass = UPlayerInventoryItemWeapon::StaticClass();

	EntityDescription.CategoryType = ECategoryTypeId::Weapon;
	EntityDescription.SlotType = ItemSlotTypeId::PrimaryWeapon;

	static ConstructorHelpers::FClassFinder<UCameraShake> FireCameraShakeDefaultClass(TEXT("/Game/1LOKAgame/Blueprints/Weapons/Default_FireCameraShake"));
	FireCameraShakeDefault = FireCameraShakeDefaultClass.Class;

	WeaponConfiguration.Name_InstantMuzzle = TEXT("InstMuzzle");
	WeaponConfiguration.Name_ProjMuzzle = TEXT("ProjMuzzle");
}

void UItemWeaponEntity::PostInitProperties()
{
	Super::PostInitProperties();

	if (!HasAnyFlags(RF_ClassDefaultObject | RF_ArchetypeObject))
	{
		for (SIZE_T i = 0; i < EAmmoSlot::End; i++)
		{
			if (AmmoInstance[i]->IsValidLowLevel())
			{
				Ammo[i] = NewObject<UItemAmmoEntity>(this, AmmoInstance[i]);
			}
		}
	}	

	if (!WeaponConfiguration.FireCameraShake)
	{
		WeaponConfiguration.FireCameraShake = FireCameraShakeDefault;
	}
}

bool UItemWeaponEntity::IsValidItem() const
{
	const bool IsValidType = EWeaponType(WeaponType).IsValid() && EntityDescription.CategoryType == ECategoryTypeId::Weapon;
	const bool IsValidMesh = SkeletalMesh != nullptr;

	return IsValidType && IsValidMesh;
}

TArray<FDisplayParam> UItemWeaponEntity::GetDisplayParams() const
{
	TArray<FDisplayParam> ReturnValue;

	ReturnValue.Add(FDisplayParam(NSLOCTEXT("UItemWeaponEntity", "WeaponEntity.FireRate", "Rate of fire"), WeaponProperty.FireRate, EValueDisplayAs::FireRate));
	ReturnValue.Add(FDisplayParam(NSLOCTEXT("UItemWeaponEntity", "WeaponEntity.ReloadDuration", "Reload duration"), WeaponProperty.ReloadDuration, EValueDisplayAs::Seconds));
	ReturnValue.Add(FDisplayParam(NSLOCTEXT("UItemWeaponEntity", "WeaponEntity.Damage", "Damage"), WeaponProperty.Damage));
	ReturnValue.Add(FDisplayParam(NSLOCTEXT("UItemWeaponEntity", "WeaponEntity.Spread", "Spread"), WeaponProperty.Spread, EValueDisplayAs::Degres));
	ReturnValue.Add(FDisplayParam(NSLOCTEXT("UItemWeaponEntity", "WeaponEntity.Feedback", "Feedback"), WeaponProperty.Feedback, EValueDisplayAs::Degres));
	ReturnValue.Add(FDisplayParam(NSLOCTEXT("UItemWeaponEntity", "WeaponEntity.Mass", "Mass"), WeaponProperty.Mass, EValueDisplayAs::Kilograms));

	return ReturnValue;
}

UItemAmmoEntity* UItemWeaponEntity::GetPrimaryAmmo() const
{
	checkf(Ammo[EAmmoSlot::Primary], TEXT("Primary ammo slot was nullptr, weapon: %s"), *GetDescription().Name.ToString());
	return Ammo[EAmmoSlot::Primary];
}

UItemAmmoEntity* UItemWeaponEntity::GetSecondaryAmmo() const
{
	ensureAlwaysMsgf(Ammo[EAmmoSlot::Secondary], TEXT("Primary ammo slot was nullptr, weapon: %s"), *GetDescription().Name.ToString());
	return Ammo[EAmmoSlot::Secondary];
}
