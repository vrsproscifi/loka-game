// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "ItemBuildEntity.h"
#include "StoreController/Models/ItemBaseContainer.h"
#include "BuildSystem/BuildPlacedComponent.h"


UItemBuildEntity::UItemBuildEntity()
	: Super()
{
	EntityDescription.CategoryType = ECategoryTypeId::Building;

	BuildingType = FFlagsHelper::ToFlag<int32>(EBuildingType::None);
	ReplaceMaterials.Empty();
	IsAllowToBuy = true;
}

void UItemBuildEntity::Initialize(const FItemBaseContainer& initializer)
{
	Super::Initialize(initializer);
}
