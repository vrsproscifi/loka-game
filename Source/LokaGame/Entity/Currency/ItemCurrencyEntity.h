// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Entity/Item/ItemBaseEntity.h"
#include "ItemCurrencyEntity.generated.h"

/**
 * 
 */
UCLASS()
class LOKAGAME_API UItemCurrencyEntity : public UItemBaseEntity
{
	GENERATED_BODY()
	
public:
	
	UItemCurrencyEntity();

	virtual void Initialize(const struct FItemBaseContainer& initializer) override;
	
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Currency)
	FORCEINLINE FTypeCost GetCurrency() const { return Currency; }

protected:

	UPROPERTY()
	FTypeCost Currency;
};
