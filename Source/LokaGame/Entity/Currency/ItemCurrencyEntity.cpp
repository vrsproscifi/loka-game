// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "StoreController/Models/ItemBaseContainer.h"
#include "ItemCurrencyEntity.h"
#include "TypeCash.h"


UItemCurrencyEntity::UItemCurrencyEntity()
	: Super()
{
	EntityDescription.CategoryType = ECategoryTypeId::Currency;
}

void UItemCurrencyEntity::Initialize(const struct FItemBaseContainer& initializer)
{
	Super::Initialize(initializer);

	if (initializer.Performance > 0)
	{
		Currency = FTypeCost(initializer.Performance, EGameCurrency::Donate);
	}
	else if (initializer.Storage > 0)
	{
		Currency = FTypeCost(initializer.Storage, EGameCurrency::Money);
	}

	UE_LOG(LogEntityItem, Log, TEXT("[Currency] Initialize entity item, Currency:%s"), *Currency.ToString());
}
