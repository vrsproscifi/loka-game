#pragma once

#include "ItemGrenadeModelId.generated.h"

UENUM()
namespace ItemGrenadeModelId
{
	enum Type
	{
		Explosive,
		Precision,

		// Need add to db
		IRB_Grenade_G67,
		End UMETA(Hidden)
	};
}
