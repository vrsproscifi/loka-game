#pragma once

#include "TypeDescription.h"
#include "AbstractItemEntity.generated.h"

UCLASS(Blueprintable, Abstract, hidecategories=Object, MinimalAPI)
class UAbstractItemEntity
	: public UObject
{
	GENERATED_BODY()
public:
	UAbstractItemEntity();
	void Initialize(const struct FItemBaseContainer& initializer);

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Description)
	bool IsAllowToDisplay;

	//==================================================================
	//						[ Description ]
protected:
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Description)
	FTypeDescription EntityDescription;

public:
	template<typename TInstanceModelId>
	const TInstanceModelId GetModelIdAs() const
	{
		return static_cast<TInstanceModelId>(EntityDescription.GetModelId<TInstanceModelId>());
	}

	const FTypeDescription&  GetDescription()
	{
		return EntityDescription;
	}

	const FTypeDescription&  GetDescription() const
	{
		return EntityDescription;
	}


	//==================================================================
	//						[ Image ]
protected:
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Description)
		FSlateBrush EntityImage;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Description)
		FSlateBrush EntityIcon;

public:
	virtual const FSlateBrush* GetImage() const
	{
		return &EntityImage;
	}

	virtual const FSlateBrush* GetIcon() const
	{
		return &EntityIcon;
	}
};
