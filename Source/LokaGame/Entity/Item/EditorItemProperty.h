#pragma once
#include "Entity/Item/CategoryTypeId.h"
#include "Entity/Fraction/FractionTypeId.h"
#include "Entity/Types/TypeCost.h"
#include "EditorItemProperty.generated.h"


USTRUCT()
struct FEditorItemProperty
{
	GENERATED_USTRUCT_BODY()

	//==========================
	//			[ Property ]

	UPROPERTY()	uint32 ModelId;
	UPROPERTY()	TEnumAsByte<CategoryTypeId::Type> CategoryTypeId;
	UPROPERTY()	int32 DefaultAmount;
	UPROPERTY()	int64 Performance;
	UPROPERTY()	int64 Storage;

	UPROPERTY()	FractionTypeId FractionId;
	UPROPERTY()	uint8 Level;
	UPROPERTY()	FTypeCost Cost;
	UPROPERTY()	FTypeCost SubscribeCost;

	
};