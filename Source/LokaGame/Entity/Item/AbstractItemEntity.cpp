// VRSPRO

#include "LokaGame.h"
#include "AbstractItemEntity.h"
#include "StoreController/Models/ItemBaseContainer.h"

UAbstractItemEntity::UAbstractItemEntity() 
	: Super()
	, IsAllowToDisplay(true)
{

}

void UAbstractItemEntity::Initialize(const FItemBaseContainer& initializer)
{
	EntityDescription.ModelId = initializer.ModelId;
}