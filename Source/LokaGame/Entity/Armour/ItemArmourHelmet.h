// VRSPRO

#pragma once
#include "ItemArmourEntity.h"
#include "ItemArmourHelmet.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class LOKAGAME_API UItemArmourHelmet
	: public UItemArmourEntity
{
	GENERATED_BODY()

public:
	UItemArmourHelmet();
};
