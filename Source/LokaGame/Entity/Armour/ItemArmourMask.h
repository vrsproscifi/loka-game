// VRSPRO

#pragma once
#include "ItemArmourEntity.h"
#include "ItemArmourMask.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class LOKAGAME_API UItemArmourMask
	: public UItemArmourEntity
{
	GENERATED_BODY()

public:
	UItemArmourMask();
};
