#pragma once

#include "FractionTypeId.generated.h"

UENUM()
enum class FractionTypeId : uint8
{
	/// <summary>
	/// Keepers of artificial intelligence Alysium
	/// Uses electronic advanced technologies
	/// </summary>
	Keepers,

	/// <summary>
	/// Allies keepers of artificial intelligence Elysium
	/// Uses advanced classical weapons
	/// </summary>
	Keepers_Friend,

	//======================
	/// <summary>
	/// Radicals against the artificial intelligence
	/// Uses classical weapons
	/// </summary>
	RIFT,

	/// <summary>
	/// Allies radicals against the artificial intelligence
	/// Uses advanced electronic/classical weapons
	/// </summary>
	RIFT_Friend,

	//======================
	End UMETA(Hidden)
};