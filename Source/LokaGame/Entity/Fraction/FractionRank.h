// VRSPRO

#pragma once
#include "FractionRank.generated.h"


USTRUCT(Blueprintable)
struct FFractionRank
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly)		FText Name;
	UPROPERTY(VisibleInstanceOnly)	int32 Reputation;
};
