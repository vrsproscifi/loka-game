// VRSPRO

#pragma once

#include "Entity/Module/ItemModuleEntity.h"
#include "ItemModuleTrunkEntity.generated.h"

/**
 * 
 */
UCLASS()
class LOKAGAME_API UItemModuleTrunkEntity : public UItemModuleEntity
{
	GENERATED_BODY()
	
public:

	UItemModuleTrunkEntity();
	
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	FORCEINLINE float GetFlameIntensity() const { return FlameIntensity; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	FORCEINLINE float GetSoundIntensity() const { return SoundIntensity; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	FORCEINLINE FName GetMuzzleSocketInstant() const { return Name_InstantMuzzle; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	FORCEINLINE FName GetMuzzleSocketProj() const { return Name_ProjMuzzle; }

protected:

	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	float FlameIntensity;

	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	float SoundIntensity;

	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	FName Name_InstantMuzzle;

	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	FName Name_ProjMuzzle;
};
