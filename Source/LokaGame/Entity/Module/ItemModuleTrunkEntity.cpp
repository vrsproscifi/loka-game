// VRSPRO

#include "LokaGame.h"
#include "ItemModuleTrunkEntity.h"


UItemModuleTrunkEntity::UItemModuleTrunkEntity()
	: Super()
	, FlameIntensity(0.0f)
	, SoundIntensity(0.0f)
{
	ModuleType = EItemModuleType::TrunkSimple;

	Name_InstantMuzzle = TEXT("InstMuzzle");
	Name_ProjMuzzle = TEXT("ProjMuzzle");
}

