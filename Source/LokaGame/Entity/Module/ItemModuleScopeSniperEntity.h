// VRSPRO

#pragma once

#include "Entity/Module/ItemModuleEntity.h"
#include "ItemModuleScopeSniperEntity.generated.h"

/**
 * 
 */
UCLASS()
class LOKAGAME_API UItemModuleScopeSniperEntity 
	: public UItemModuleEntity
{
	GENERATED_BODY()	
	
public:

	UItemModuleScopeSniperEntity();

	FORCEINLINE const FSlateBrush* GetScope() const { return &Scope; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	FORCEINLINE FSlateColor GetBackground() const { return Background; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	FORCEINLINE float GetScopeDistance() const { return Distance; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	FORCEINLINE float GetScopeFOV(const float SourceFOV) const { return SourceFOV / (Distance / 50.0f); }

	virtual bool IsSupportedForNetworking() const override
	{
		return true;
	}

protected:

	UPROPERTY(EditDefaultsOnly, Category = Gameplay, Replicated)
	FSlateBrush Scope;

	UPROPERTY(EditDefaultsOnly, Category = Gameplay, Replicated)
	FSlateColor Background;

	UPROPERTY(EditDefaultsOnly, Category = Gameplay, Replicated)
	float Distance;
	
};
