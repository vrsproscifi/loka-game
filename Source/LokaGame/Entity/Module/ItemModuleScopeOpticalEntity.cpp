// VRSPRO

#include "LokaGame.h"
#include "ItemModuleScopeOpticalEntity.h"


UItemModuleScopeOpticalEntity::UItemModuleScopeOpticalEntity()
	: Super()
	, ReplaceToMaterial(nullptr)
	, Distance(100.0f)
{
	ModuleType = EItemModuleType::ScopeOptical;
	bIsUseRenderTarget = false;
}

void UItemModuleScopeOpticalEntity::PostInitProperties()
{
	Super::PostInitProperties();

	if (!RenderTextureScope)
	{
		RenderTextureScope = NewObject<UTextureRenderTarget2D>(this);
		RenderTextureScope->InitAutoFormat(512.f, 512.f);
		RenderTextureScope->CompressionSettings = TextureCompressionSettings::TC_EditorIcon;
		RenderTextureScope->LODGroup = TextureGroup::TEXTUREGROUP_UI;
		RenderTextureScope->OverrideFormat = EPixelFormat::PF_FloatRGB;
	}
}