#pragma once
#include "GameCurrency.generated.h"

UENUM()
namespace EGameCurrency
{
	enum Type
	{
		Money,
		Donate,
		Coin
	};
}