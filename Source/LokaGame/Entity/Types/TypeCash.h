#pragma once
#include "GameCurrency.h"
#include "SResourceTextBoxValue.h"
#include "TypeCash.generated.h"


USTRUCT(Blueprintable)
struct FTypeCash
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()		int64 Amount;
	UPROPERTY()		TEnumAsByte<EGameCurrency::Type> Currency;

	FTypeCash()
		: Amount(0)
		, Currency(0)
	{
		
	}

	FTypeCash(const EGameCurrency::Type& InCurrency, const int64& InAmount)
		: Amount(InAmount)
		, Currency(InCurrency)
	{

	}

	FResourceTextBoxValue AsResource();
	FResourceTextBoxValue AsResource() const;
};