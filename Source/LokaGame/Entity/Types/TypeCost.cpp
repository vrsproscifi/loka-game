#include "LokaGame.h"
#include "TypeCost.h"
#include "TypeCash.h"
#include "RichTextHelpers.h"
#include "SResourceTextBoxType.h"

FString FTypeCost::ToString() const
{
	return FString::Printf(TEXT("FTypeCost(Currency = %d, Amount = %d)"), static_cast<int32>(Currency), Amount);
}

FText FTypeCost::ToText() const
{
	static const FText donate = FText::FromString(TEXT("Energo-crystal"));
	static const FText money = FText::FromString(TEXT("Crypto-dollars"));
	return FText::Format(FText::FromString(TEXT("{0} {1}")), FText::AsNumber(Amount), Currency == EGameCurrency::Donate ? donate : money);
}

FText FTypeCost::ToResourse() const
{
	return FRichHelpers::FResourceHelper().SetType(static_cast<EResourceTextBoxType::Type>(Currency.GetValue())).SetValue(Amount).ToText();
}

EResourceTextBoxType::Type FTypeCost::AsResourceTextBoxType() const
{
	if(Currency == EGameCurrency::Donate)
	{
		return EResourceTextBoxType::Donate;
	}
	return EResourceTextBoxType::Money;
}

bool FTypeCost::IsEnough(const FTypeCash* InOther) const
{
	return InOther->Amount >= Amount;
}

bool FTypeCost::IsEnough(const TArray<FTypeCash>& InOther) const
{
	auto cash = InOther.FindByPredicate([c = Currency](const FTypeCash& t) {
		return t.Currency == c;
	});

	if(cash == nullptr) return false;
	return cash->Amount >= Amount;
}

FResourceTextBoxValue FTypeCost::GetCostAsResource()
{
	return FResourceTextBoxValue(static_cast<EResourceTextBoxType::Type>(Currency.GetValue()), Amount);
}

FResourceTextBoxValue FTypeCost::GetCostAsResource() const
{
	return FResourceTextBoxValue(static_cast<EResourceTextBoxType::Type>(Currency.GetValue()), Amount);
}
