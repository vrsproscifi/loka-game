#pragma once

#include "ItemAmmoModelId.generated.h"

UENUM()
namespace ItemAmmoModelId
{
	enum Type
	{
		None,
		End UMETA(Hidden)
	};
}
