// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Interfaces/UsableActorInterface.h"
#include "TransportBase.generated.h"


class UPlayerInventoryItem;
class ABaseCharacter;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnUserEntyExit, ABaseCharacter*, User);

#define COLLISION_TRANSPORT ECC_GameTraceChannel12

UCLASS()
class LOKAGAME_API ATransportBase 
	: public APawn
	, public IUsableActorInterface
{
GENERATED_BODY()

public:
	ATransportBase();

	// IUsableActorInterface Begin
	virtual void OnFocusStart_Implementation(AActor* InInstigator) override;
	virtual void OnFocusLost_Implementation(AActor* InInstigator) override;
	virtual void OnInteractStart_Implementation(AActor* InInstigator) override;
	virtual void OnInteractLost_Implementation(AActor* InInstigator) override;
	// IUsableActorInterface End

	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual void MoveForward(float InValue) {}
	virtual void MoveRight(float InValue) {}
	virtual void MoveUp(float InValue) {}
	virtual void TurnAtRate(float InValue);
	virtual void LookUpAtRate(float InValue);

	virtual void OnUseAnyAction();

	UFUNCTION(BlueprintCallable, Category = Instance)
	virtual void InitializeInstance(UPlayerInventoryItem* InInstance);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Instance)
	UPlayerInventoryItem* GetInstance() const { return Instance; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	virtual int32 GetSupportedSeats() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	virtual ABaseCharacter* GetDriver() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	virtual ABaseCharacter* GetPassenger(const int32 SeatId = 0) const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Gameplay)
	virtual int32 GetSeatIdFor(ABaseCharacter* InUser) const;

	// * return true if Authority & valid checks or if not Authority return false & call server side
	UFUNCTION(BlueprintCallable, Category = Gameplay)
	virtual bool DriverEntry(ABaseCharacter* InUser);

	// * return true if Authority & valid checks or if not Authority return false & call server side
	UFUNCTION(BlueprintCallable, Category = Gameplay)
	virtual bool DriverExit(ABaseCharacter* InUser);

	// * return true if Authority & valid checks or if not Authority return false & call server side
	UFUNCTION(BlueprintCallable, Category = Gameplay)
	virtual bool PassengerEntry(ABaseCharacter* InUser, const int32 SeatId = 0);

	// * return true if Authority & valid checks or if not Authority return false & call server side
	UFUNCTION(BlueprintCallable, Category = Gameplay)
	virtual bool PassengerExit(ABaseCharacter* InUser);

	UFUNCTION(Reliable, Server, WithValidation, BlueprintCallable, Category = Gameplay)
	void ServerPassengerEntry(ABaseCharacter* InUser, const int32 SeatId = 0);

	UFUNCTION(Reliable, Server, WithValidation, BlueprintCallable, Category = Gameplay)
	void ServerPassengerExit(ABaseCharacter* InUser);

	UPROPERTY(BlueprintAssignable, Category = Gameplay)
	FOnUserEntyExit OnUserEntry;

	UPROPERTY(BlueprintAssignable, Category = Gameplay)
	FOnUserEntyExit OnUserExit;

protected:

	UFUNCTION()	virtual void OnRep_Instance();
	UFUNCTION()	virtual void OnRep_Seats();

	UPROPERTY()
	TArray<ABaseCharacter*> LastSeats;

	UPROPERTY(BlueprintReadWrite, ReplicatedUsing = OnRep_Seats, Category = Gameplay)
	TArray<ABaseCharacter*> Seats;
	
	UPROPERTY(BlueprintReadWrite, ReplicatedUsing = OnRep_Instance, Category = Gameplay)
	UPlayerInventoryItem* Instance;
};
