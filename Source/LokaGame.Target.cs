// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;
using System.IO;

public class LokaGameTarget : TargetRules
{
    public LokaGameTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
        bUsesSteam = true;
		
        bUsesCEF3 = false;

        // Turn on shipping checks and logging
        bUseLoggingInShipping = true;
        bUseChecksInShipping = true;

        ExtraModuleNames.Add("LokaGame");
        ExtraModuleNames.Add("LokaGameLoading");

        LinkType = TargetLinkType.Monolithic;
    }	
	bool IsLicenseeBuild()
    {
        return !Directory.Exists("Runtime/NotForLicensees");
    }

    //public override List<UnrealTargetPlatform> GUBP_GetPlatforms_MonolithicOnly(UnrealTargetPlatform HostPlatform)
    //{
    //    if (HostPlatform == UnrealTargetPlatform.Mac)
    //    {
    //        return new List<UnrealTargetPlatform> { UnrealTargetPlatform.Mac };
    //    }
    //    return new List<UnrealTargetPlatform> { UnrealTargetPlatform.Win32, UnrealTargetPlatform.Win64, UnrealTargetPlatform.Linux };
    //}
    //
    //public override List<UnrealTargetConfiguration> GUBP_GetConfigs_MonolithicOnly(UnrealTargetPlatform HostPlatform, UnrealTargetPlatform Platform)
    //{
    //    // ORDER HERE MATTERS, THE FIRST ENTRY IS PUT IN Manifest_NonUFSFiles.txt AND THE FOLLOWING ARE PUT IN Manifest_DebugFiles.txt
    //    return new List<UnrealTargetConfiguration> { UnrealTargetConfiguration.Shipping, UnrealTargetConfiguration.Test };
    //}
}
