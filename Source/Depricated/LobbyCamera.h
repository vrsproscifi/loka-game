// VRSPRO

#pragma once

#include "GameFramework/Actor.h"
#include "LobbyCamera.generated.h"

UENUM()
namespace ELobbyCameraMode
{
	enum Type
	{
		Character,
		Weapon,
		End UMETA(Hidden)
	};
}

UCLASS()
class LOKAGAME_API ALobbyCamera : public AActor
{
	GENERATED_BODY()
	
public:	

	ALobbyCamera(const class FObjectInitializer& ObjectInitializer);

	virtual void BeginPlay() override;
	virtual void Tick( float DeltaSeconds ) override;

	void AddScroolDistance(const float);
	void SetScroolDistance(const float);
	float GetScroolDistance() const;

	void AddRotation(const FRotator);
	void SetRotation(const FRotator);
	FRotator GetRotation() const;

	void SetIsAutoRotation(const bool);
	bool IsAutoRotation() const;

	void SetIsEnabledDOF(const bool);
	bool IsEnabledDOF() const;

	void SetMode(const ELobbyCameraMode::Type);
	ELobbyCameraMode::Type GetMode() const;

	FVector GetCameraLocation() const;

	USpringArmComponent* GetSpringComponent() const { return TPCameraSpring; }
	UCameraComponent* GetCameraComponent() const { return TPCamera; }

	void BeginActivateWM(const FVector);
	void BeginDeactivateWM(const FVector);

protected:

	UPROPERTY(VisibleDefaultsOnly, Category = Camera)
	class USpringArmComponent *TPCameraSpring;

	UPROPERTY(VisibleDefaultsOnly, Category = Camera)
	class UCameraComponent *TPCamera;

	UPROPERTY(EditDefaultsOnly, Category = Camera)
	FVector2D SpringDistanceLimits[ELobbyCameraMode::End];
	
	UPROPERTY(EditDefaultsOnly, Category = Camera)
	float SmoothScroolSpeed;

	float TargetScroolDistance;
	float OldScroolDistance[ELobbyCameraMode::End];

	UPROPERTY(EditDefaultsOnly, Category = Camera)
	bool bEnableAutoRotate;

	UPROPERTY(EditDefaultsOnly, Category = Camera)
	bool bRotationDirection;

	UPROPERTY(EditDefaultsOnly, Category = Camera)
	float RotationIterator;

	UPROPERTY(EditDefaultsOnly, Category = Camera)
	float RotationSmoothSpeed;

	UPROPERTY(EditDefaultsOnly, Category = Camera)
	float BlurRadius[ELobbyCameraMode::End];

	// Used on lerp from minimal to maximal distance
	UPROPERTY(EditDefaultsOnly, Category = Camera, meta = (UIMin=0, UIMax=1))
	float StartDistance;

	FRotator TargetRotation;

	ELobbyCameraMode::Type CurrentMode;

	bool bEnableDOF;

	FVector TargetPosition, SourcePosition;
	double RotationLastTime;
};
