#include "LokaGame.h"
#include "Reqest.Interface.h"
#include "Service.Interface.h"

//FOnRequestErrorHandler IReqestInterface::Delegate_OnRequestError;
//#define SERVICE_HOST "http://vrspro.no-ip.org"
//#define SERVICE_HOST "http://78.46.46.148:4444"
#define SERVICE_HOST "http://localhost:4444"



IReqestInterface::IReqestInterface(const IServiceInterface * Service)
	: ONLINE_SERVICE(Service)
	, IIdentityToken(Service->getTokenPtr())
	, SERVICE_CONTROLLER("NULL_CONTROLLER")
{

}

IReqestInterface::IReqestInterface()
{

}

IReqestInterface::~IReqestInterface()
{

}



bool IReqestInterface::PostRequest(const FString &Url, const FString &Data, const FHttpRequestCompleteDelegate &Delegate)
{
	return PostRequest(Url, Data, getToken(), Delegate);
}

bool IReqestInterface::PostRequest(const FString & Url, const FString & Data, const FString & Token, const FHttpRequestCompleteDelegate & Delegate) const
{
//
//	TSharedRef<class IHttpRequest> HttpRequest = FHttpModule::Get().CreateRequest();
//
//	HttpRequest->SetContentAsString(Data.IsEmpty() ? "1" : Data);
//	HttpRequest->OnProcessRequestComplete() = Delegate;
//	HttpRequest->SetHeader(TEXT("Content-Type"), TEXT("application/json"));
//
//	if (Token.IsEmpty() == false)
//	{
//		auto TokenBase = FBase64::Encode(Token);
//#if UE_SERVER == 0
//		HttpRequest->SetHeader(TEXT("Authorization"), FString::Printf(TEXT("client %s"), *TokenBase));
//#else
//		HttpRequest->SetHeader(TEXT("Authorization"), FString::Printf(TEXT("dedicated %s"), *TokenBase));
//#endif
//
//	}
//
//	HttpRequest->SetVerb(TEXT("POST"));
//	auto FullURL = FString::Printf(TEXT("%s/%s/%s"), TEXT(SERVICE_HOST), *SERVICE_CONTROLLER, *Url);
//	auto FullURLPtr = TCHAR_TO_ANSI(*FullURL);
//	
//	HttpRequest->SetURL(FullURL);
//
//	return HttpRequest->ProcessRequest();
	return false;
}

bool IReqestInterface::isValidHttpResponse(FHttpResponsePtr HttpResponse) const
{
	FHttpResponseCode ResponseCode;

	if (HttpResponse.IsValid() == false)
	{
		ResponseCode.Error = FString("HttpResponse was invalid");
	}
	else if (EHttpResponseCodes::IsOk(HttpResponse->GetResponseCode()))
	{
		return true;
	}
	else
	{
		ResponseCode.Error = FString("Unknown");
	}

	if (HttpResponse.IsValid())
	{
		ResponseCode.Code = HttpResponse->GetResponseCode();
		ResponseCode.Url = HttpResponse->GetURL();
		ResponseCode.Data = HttpResponse->GetContentAsString();

		if (RequestErrorHandlers.Contains(ResponseCode.Code))
		{
			RequestErrorHandlers[ResponseCode.Code].ExecuteIfBound(ResponseCode.Data);
		}
	}
	else ResponseCode.Data = FString("Http Response is null");

	Delegate_OnRequestError.ExecuteIfBound(ResponseCode);
	return false;
}