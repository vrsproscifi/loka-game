#pragma once

#ifndef H_REQUEST_INTERFACE
#define H_REQUEST_INTERFACE


#include "Reqest.Header.h"
#include "Identity.Token.h"
#include "http.h"
#include "Runtime/JsonUtilities/Public/JsonObjectConverter.h"

class IReqestInterface : public IIdentityToken
{	
protected:
	FString SERVICE_CONTROLLER;
	const class IServiceInterface* ONLINE_SERVICE;

public:
	IReqestInterface();
	IReqestInterface(const IServiceInterface * Service);
	virtual ~IReqestInterface() = 0;
	
	bool PostRequest(const FString &Url, const FString &Data, const FHttpRequestCompleteDelegate &Delegate);
	bool PostRequest(const FString &Url, const FString &Data, const FString &Token, const FHttpRequestCompleteDelegate &Delegate) const;

	FOnRequestErrorHandler Delegate_OnRequestError;
protected:
	DECLARE_DELEGATE_OneParam(FOnRequestErrorDelegate, const FString&);

	TMap<int32, FOnRequestErrorDelegate> RequestErrorHandlers;
	void OnRequestErrorHandler(const int &ResponseCode, const FString &ResponceStr)
	{
		if (RequestErrorHandlers.Contains(ResponseCode))
		{
			RequestErrorHandlers[ResponseCode].ExecuteIfBound(ResponceStr);
		}
	}


	virtual bool isValidHttpResponse(FHttpResponsePtr HttpResponse) const;
	
	template<typename OutStructType>
	bool JsonObjectStringToUStruct(const FHttpResponsePtr &HttpResponse, OutStructType *Data)
	{
		FHttpResponseCode ResponseCode;

		bool isValidJSon = false;
		auto ResponceStr = HttpResponse->GetContentAsString().TrimQuotes().ReplaceEscapedCharWithChar();
		if (ResponceStr.IsEmpty())
		{
			isValidJSon = false;
			ResponseCode.Error = "Json String Data was empty";
		}
		else
		{
			isValidJSon = FJsonObjectConverter::JsonObjectStringToUStruct(ResponceStr, Data, 0, 0);

			if (isValidJSon == false)
			{
				ResponseCode.Error = "Bad Json Client Data";
			}
		}

		if (isValidJSon == false)
		{
			ResponseCode.Data = ResponceStr;
			Delegate_OnRequestError.ExecuteIfBound(ResponseCode);
		}
		return isValidJSon;
	}


	template<typename OutStructType>
	bool JsonArrayStringToUStruct(const FHttpResponsePtr &HttpResponse, TArray<OutStructType>* Data)
	{
		FHttpResponseCode ResponseCode;

		bool isValidJSon = false;
		auto ResponceStr = HttpResponse->GetContentAsString().TrimQuotes().ReplaceEscapedCharWithChar();
		if (ResponceStr.IsEmpty())
		{
			isValidJSon = false;
			ResponseCode.Error = "Json String Data was empty";
		}
		else
		{
			isValidJSon = FJsonObjectConverter::JsonArrayStringToUStruct(ResponceStr, Data, 0, 0);

			if (isValidJSon == false)
			{
				ResponseCode.Error = "Bad Json Client Data";
			}
		}

		if (isValidJSon == false)
		{
			ResponseCode.Data = ResponceStr;
			Delegate_OnRequestError.ExecuteIfBound(ResponseCode);
		}

		return isValidJSon;
	}
	
	template<typename ReturnType>
	bool JsonArrayStringToSerializable(const FHttpResponsePtr &HttpResponse, TArray<ReturnType>* To)
	{
		auto ResponceStr = HttpResponse->GetContentAsString().TrimQuotes().ReplaceEscapedCharWithChar();
		if (ResponceStr.IsEmpty())
		{
			return false;
		}

		const TSharedRef< TJsonReader<> >& Reader = TJsonReaderFactory<>::Create(ResponceStr);
		TArray<TSharedPtr<FJsonValue> > JsonArray;
		if (FJsonSerializer::Deserialize(Reader, JsonArray))
		{
			if (JsonArray.Num() > 0)
			{
				for (SSIZE_T i = 0; i < JsonArray.Num(); ++i)
				{
					FUserWhisper tmp;
					if (!tmp.FromJson(JsonArray[i]->AsObject()))
					{
						return false;
					}

					To->Add(tmp);
				}

				return true;
			}

			return false;
		}

		return false;
	}
};
#endif
