#pragma once

#ifndef H_Corporation_INTERFACE
#define H_Corporation_INTERFACE

#include "Reqest.Interface.h"
#include "CorporationDetails.h"
#include "CorporationPreview.h"
#include "CorporationCreate.h"
#include "SessionIdentityInfo.h"

#define DEFAULT_CORPORATION_INDEX 0
#define DEFAULT_CORPORATION_PAGE 0

class CCorporationSerivce : public IReqestInterface
{


	const FString ms_QueryReqestInfo;
	const FString ms_QueryReqestJoin;
	const FString ms_QueryReqestEdit;
	const FString ms_QueryReqestEditMemberRole;
	const FString ms_QueryReqestKickMember;

	FHttpRequestCompleteDelegate m_OnCorporationInfoCompleteReqest;
	FHttpRequestCompleteDelegate m_OnCorporationEditCompleteReqest;
public:
	CCorporationSerivce(IServiceInterface* Service);
	~CCorporationSerivce();

public:
	bool RequestSetMemberRole(const FCorporationEditMemberRole &Request);
	bool RequestKickMemeber(const FRequestOneParam<int32> &Request);
	
	bool ReqestCorporationInfo(const uint32& index = DEFAULT_CORPORATION_INDEX);
	void OnCorporationInfoReqestComplete(FHttpRequestPtr HttpRequest, FHttpResponsePtr HttpResponse, bool bSucceeded);
	FOnCorporationMemberView Delegate_OnCorporationViewOfMember;
	FOnCorporationGuestView  Delegate_OnCorporationViewOfGuest;


	bool RequestJoin(const FRequestOneParam<uint32>& index = DEFAULT_CORPORATION_INDEX);
	bool RequestEdit(const FCorporationEdit& Data);
	void OnCorporationEditCompleteReqest(FHttpRequestPtr HttpRequest, FHttpResponsePtr HttpResponse, bool bSucceeded);
	FOnCorporationAcceptEdit Delegate_OnCorporationAcceptEdit;

	DEFINE_ONLINE_SERVICE_ARRAY(CorporationSearch, "Search", FRequestOneParam<FString>, FCorporationPreviewResponse);
	DEFINE_ONLINE_SERVICE_ARRAY(CorporationList, "RequestList", FRequestOneParam<int>, FCorporationPreviewResponse);

	DEFINE_ONLINE_SERVICE_JSON(ToDonate, "RequestToDonate", FCorporationDonate, FRequestOneParam<bool>);
	DEFINE_ONLINE_SERVICE_OBJECT(GetChatMessages, "GetChatMessage", FRequestOneParam<bool>, FChatRows);
	DEFINE_ONLINE_SERVICE_OBJECT(AddChatMessage, "AddChatMessage", FRequestOneParam<FString>, FChatRows);

	enum EHttpResponseError
	{
		NotFoundCorporation = 600,
		CorporationWasPrivate,
		YouAreInCorporation,
		YouAreNotInCorporation,
		PlayerAreInCorporation,
		PlayerAreNotInCorporation,
		NeedMoneyForCreate,
		CorporationNameWasExist,
		CorporationNameWasInvalid,
		CorporationAbbrWasExist,
		CorporationAbbrWasInvalid,
		CorporationPropertyWasChanged,
		CorporationMemberWasChanged,
		CorporationMemberWasKicked,
		NotAllowedPermission,
		PermissionArgumemtOutOfRange,
		CorporationDonateSuccess,
		CorporationDonateFail,
	};

	DEFINE_SERVICE_ERROR_HANDLER(NotFoundCorporation);
	DEFINE_SERVICE_ERROR_HANDLER(CorporationWasPrivate);
	DEFINE_SERVICE_ERROR_HANDLER(YouAreInCorporation);
	DEFINE_SERVICE_ERROR_HANDLER(YouAreNotInCorporation);
	DEFINE_SERVICE_ERROR_HANDLER(PlayerAreInCorporation);
	DEFINE_SERVICE_ERROR_HANDLER(PlayerAreNotInCorporation);
	DEFINE_SERVICE_ERROR_HANDLER_OneParam(NeedMoneyForCreate, FRequestOneParam<int32>);
	DEFINE_SERVICE_ERROR_HANDLER(CorporationNameWasExist);
	DEFINE_SERVICE_ERROR_HANDLER(CorporationNameWasInvalid);
	DEFINE_SERVICE_ERROR_HANDLER(CorporationAbbrWasExist);
	DEFINE_SERVICE_ERROR_HANDLER(CorporationAbbrWasInvalid);
	DEFINE_SERVICE_ERROR_HANDLER(CorporationPropertyWasChanged);
	DEFINE_SERVICE_ERROR_HANDLER_OneParam(CorporationMemberWasChanged, FCorporationEditMemberRole); //FCorporationEditMemberRole
	DEFINE_SERVICE_ERROR_HANDLER_OneParam(CorporationMemberWasKicked, FRequestOneParam<int32>); //PlayerId
	DEFINE_SERVICE_ERROR_HANDLER_OneParam(NotAllowedPermission, FRequestOneParam<int32>); //MemberAccess
	DEFINE_SERVICE_ERROR_HANDLER(PermissionArgumemtOutOfRange);
	DEFINE_SERVICE_ERROR_HANDLER(CorporationDonateSuccess);
	DEFINE_SERVICE_ERROR_HANDLER(CorporationDonateFail);
};

#endif