#pragma once
#include "CorporationAccess.generated.h"


UENUM(Blueprintable)
enum class ECorporationAccess : uint8
{
	Public,
	Private
};