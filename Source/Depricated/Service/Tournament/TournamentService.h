#pragma once

#ifndef H_Tournament_INTERFACE
#define H_Tournament_INTERFACE

#include "Reqest.Interface.h"
#include "Tournament/TournamentDetails.h"
#include "Tournament/TournamentPreview.h"
#include "Tournament/TournamentMember.h"
#include "Tournament/TournamentSession.h"

class CTournamentSerivce : public IReqestInterface
{
public:
	CTournamentSerivce(IServiceInterface* Service);
	~CTournamentSerivce();

	//=================================================
	//	Tournament

	DEFINE_ONLINE_SERVICE_ARRAY(TournamentList, "RequestList", FRequestOneParam<int32>, FTournamentPreview)

	DEFINE_ONLINE_SERVICE_OBJECT(TournamentDetails, "RequestInformation", FRequestOneParam<int32>, FTournamentDetails)

	DEFINE_ONLINE_SERVICE_OBJECT(MatchMaking, "RequestMatchMaking", FRequestOneParam<int32>, FTournamentDetails)


	//=================================================
	//	Members

	DEFINE_ONLINE_SERVICE_OBJECT(CorporationMember, "RequestCorporationMember", FRequestOneParam<FString>, FTournamentMembersSelector)

	DEFINE_ONLINE_SERVICE_OBJECT(ConfirmTeam, "ConfirmTeam", FConfirmTournamentMemeber, FTournamentDetails)

	//=================================================
	//	Session

	DEFINE_ONLINE_SERVICE_OBJECT(TournamentSession, "RequestSessionInformation", FRequestOneParam<int32>, FTournamentSessionClient)
	DEFINE_ONLINE_SERVICE_OBJECT(Winner, "RequestWinner", FRequestOneParam<int32>, FTournamentDetails)

	enum EHttpResponseError
	{
		WaitingForGame = 600,
		NoFreeTounaments,
		YouAreInTournament,
		YouAreNotInCorporation,
		YourCorporationIsLarge,
		YourCorporationIsSmall,
		YourCorporationIsMember,
		YourRequestIsAccepted,
		YourRequestIsSent,
		PlayerAreNotInCorporation,
		PlayerAreInTournament,
		NeedMoneyForJoin,
		TournamentWasComplete,
		YouMovedToNextRound,
		NeedDonateForJoin,
		NeedLeagueMoneyForContinue,
		NeedLeagueDonateForContinue
	};

	DEFINE_SERVICE_ERROR_HANDLER(WaitingForGame);
	DEFINE_SERVICE_ERROR_HANDLER(NoFreeTounaments);
	DEFINE_SERVICE_ERROR_HANDLER(YouAreInTournament);
	DEFINE_SERVICE_ERROR_HANDLER(YouAreNotInCorporation);
	DEFINE_SERVICE_ERROR_HANDLER_OneParam(YourCorporationIsLarge, FRequestOneParam<int32>);
	DEFINE_SERVICE_ERROR_HANDLER_OneParam(YourCorporationIsSmall, FRequestOneParam<int32>);
	DEFINE_SERVICE_ERROR_HANDLER(YourCorporationIsMember);
	DEFINE_SERVICE_ERROR_HANDLER(YourRequestIsAccepted);
	DEFINE_SERVICE_ERROR_HANDLER(YourRequestIsSent);
	DEFINE_SERVICE_ERROR_HANDLER(PlayerAreNotInCorporation);
	DEFINE_SERVICE_ERROR_HANDLER(PlayerAreInTournament);
	DEFINE_SERVICE_ERROR_HANDLER_OneParam(NeedMoneyForJoin, FRequestOneParam<int32>);
	DEFINE_SERVICE_ERROR_HANDLER_OneParam(NeedDonateForJoin, FRequestOneParam<int32>);
	DEFINE_SERVICE_ERROR_HANDLER(TournamentWasComplete);
	DEFINE_SERVICE_ERROR_HANDLER(YouMovedToNextRound);
	DEFINE_SERVICE_ERROR_HANDLER_OneParam(NeedLeagueMoneyForContinue, FRequestOneParam<int32>);
	DEFINE_SERVICE_ERROR_HANDLER_OneParam(NeedLeagueDonateForContinue, FRequestOneParam<int32>);
};

#endif