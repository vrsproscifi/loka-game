#include "LokaGame.h"
#include "TournamentService.h"

#define __CLASS__ CTournamentSerivce

CTournamentSerivce::CTournamentSerivce(IServiceInterface * Service)
	: IReqestInterface(Service)
	//-------------------------------------------
{
	SERVICE_CONTROLLER = "api/Tournament";
	//=================================================
	//	Tournament
	DEFINE_ONLINE_SERVICE_DELEGATE(TournamentList)
	DEFINE_ONLINE_SERVICE_DELEGATE(TournamentDetails)
	DEFINE_ONLINE_SERVICE_DELEGATE(MatchMaking)

	//=================================================
	//	Members
	DEFINE_ONLINE_SERVICE_DELEGATE(CorporationMember)
	DEFINE_ONLINE_SERVICE_DELEGATE(ConfirmTeam)


	//=================================================
	//	Session
	DEFINE_ONLINE_SERVICE_DELEGATE(TournamentSession)
	DEFINE_ONLINE_SERVICE_DELEGATE(Winner)


		//
	DEFINE_SERVICE_ERROR_DELEGATE(WaitingForGame);
	DEFINE_SERVICE_ERROR_DELEGATE(NoFreeTounaments);
	DEFINE_SERVICE_ERROR_DELEGATE(YouAreInTournament);
	DEFINE_SERVICE_ERROR_DELEGATE(YouAreNotInCorporation);
	DEFINE_SERVICE_ERROR_DELEGATE(YourCorporationIsLarge);
	DEFINE_SERVICE_ERROR_DELEGATE(YourCorporationIsSmall);
	DEFINE_SERVICE_ERROR_DELEGATE(YourCorporationIsMember);
	DEFINE_SERVICE_ERROR_DELEGATE(YourRequestIsAccepted);
	DEFINE_SERVICE_ERROR_DELEGATE(YourRequestIsSent);
	DEFINE_SERVICE_ERROR_DELEGATE(PlayerAreNotInCorporation);
	DEFINE_SERVICE_ERROR_DELEGATE(PlayerAreInTournament);
	DEFINE_SERVICE_ERROR_DELEGATE(NeedMoneyForJoin);
	DEFINE_SERVICE_ERROR_DELEGATE(NeedDonateForJoin);
	DEFINE_SERVICE_ERROR_DELEGATE(TournamentWasComplete);
	DEFINE_SERVICE_ERROR_DELEGATE(YouMovedToNextRound);
	DEFINE_SERVICE_ERROR_DELEGATE(NeedLeagueMoneyForContinue);
	DEFINE_SERVICE_ERROR_DELEGATE(NeedLeagueDonateForContinue);
}

CTournamentSerivce::~CTournamentSerivce()
{

}
