
#pragma once
#include "Corporation/CorporationMemberAccess.h"
#include "TournamentMember.generated.h"

struct FConfirmTournamentMemeber : public FOnlineJsonSerializable
{
	FConfirmTournamentMemeber(const TArray<FString>& Array) : Members(Array) {}

	TArray<FString> Members;

	BEGIN_ONLINE_JSON_SERIALIZER
		ONLINE_JSON_SERIALIZE_ARRAY("Members", Members);
	END_ONLINE_JSON_SERIALIZER
};

USTRUCT(BlueprintType, Blueprintable)
struct FTournamentCorporationMemeber
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadOnly)
		int32 PlayerId;
	
	UPROPERTY(BlueprintReadOnly)
		float KillsAvg;

	UPROPERTY(BlueprintReadOnly)
		FString Name;

	UPROPERTY(BlueprintReadOnly)
		ECorporationMemberAccess Access;

	UPROPERTY(BlueprintReadOnly)
		int32 LastActivity;
};

USTRUCT(Blueprintable)
struct FTournamentMembersSelector
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadOnly)
		int32 TargetBalanceValue;

	UPROPERTY(BlueprintReadOnly)
		int32 TargetBalanceType;

	UPROPERTY(BlueprintReadOnly)
		int32 OneMemberCost;

	UPROPERTY(BlueprintReadOnly)
		TArray<FTournamentCorporationMemeber> LeagueMembers;

	UPROPERTY(BlueprintReadOnly)
		TArray<FTournamentCorporationMemeber> TeamMembers;
};