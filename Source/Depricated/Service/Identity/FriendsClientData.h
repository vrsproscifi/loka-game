
#pragma once
#include "Reqest.Header.h"
#include "FriendsClientData.generated.h"

USTRUCT(BlueprintType, Blueprintable)
struct FAfterMatchStatus
{
	GENERATED_USTRUCT_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 Money;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 Exp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 Status;
};

UENUM(BlueprintType, Blueprintable)
enum class EOnlineStatus : uint8
{
	Offline,
	Online,
	InRoom,
	End
};

USTRUCT()
struct FTopPlayer
{
	GENERATED_USTRUCT_BODY()

		UPROPERTY()
		FString Name;

	UPROPERTY()
		int32 Score;
};



DECLARE_DELEGATE_OneParam(FOnOnlineStatusCheckComplete, EOnlineStatus);

DECLARE_DELEGATE_OneParam(FOnReqestClientMatchStatus, const FAfterMatchStatus&);


struct FUserFriendAction : public FOnlineJsonSerializable
{
	FUserFriendAction()
		: Index(0)
		, Action(0)
		, Desc()
	{}

	FUserFriendAction(const int32& _index, const int32& _action) 
		: Index(_index)
		, Action(_action)
		, Desc()
	{}

	int32 Index;
	int32 Action;
	FString Desc;

	BEGIN_ONLINE_JSON_SERIALIZER
		ONLINE_JSON_SERIALIZE("Index", Index);
		ONLINE_JSON_SERIALIZE("Action", Action);
		ONLINE_JSON_SERIALIZE("Desc", Desc);
	END_ONLINE_JSON_SERIALIZER
};

struct FUserWhisper : public FOnlineJsonSerializable
{
	FString Tag;
	FString Sender;
	FString Message;
	int32 Time;

	BEGIN_ONLINE_JSON_SERIALIZER
		ONLINE_JSON_SERIALIZE("Tag", Tag);
		ONLINE_JSON_SERIALIZE("Sender", Sender);
		ONLINE_JSON_SERIALIZE("Message", Message);
		ONLINE_JSON_SERIALIZE("Time", Time);
	END_ONLINE_JSON_SERIALIZER
};