#pragma once
#include "LeagueAccess.h"

struct FLeaguePreview final : FOnlineJsonSerializable
{

	FJsonGuid Index;

	int32 Members;

	int32 Rating;
	
	int32 Position;
	
	FString Name;
	
	FString Abbr;
	
	int32 JoinPrice;
	
	ELeagueAccess Access;

	BEGIN_ONLINE_JSON_SERIALIZER
		ONLINE_JSON_SERIALIZE("Index", Index);

	END_ONLINE_JSON_SERIALIZER
};
