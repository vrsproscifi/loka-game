// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Types/TypeCash.h"
#include "PlayerController/BasePlayerController.h"
#include "RichTextHelpers.h"
#include "SquadController/Models/SquadInviteListContainer.h"
#include "LobbyController/Models/LobbyMatchResultPreview.h"
#include "BuildPlayerController.generated.h"


class SBuildSystem_ToolBar;
class SBuildSystem_Inventory;
class SVerticalMenuBuilder;
class SSettingsManager;
class SWindowBuyBuilding;
class ABuilderCharacter;
class ABuildPlacedComponent;

UCLASS(Config=Game)
class LOKAGAME_API ABuildPlayerController : public ABasePlayerController
{
	GENERATED_BODY()
	
public:
	
	ABuildPlayerController(const FObjectInitializer& ObjectInitializer);

	virtual void SetPawn(APawn* InPawn) override;
	virtual void Destroyed() override;

	virtual void SetupInputComponent() override;

	void ToggleHUDVisibility();

	template<bool Value>
	void OnBuildAction_Helper() { OnBuildAction(Value); }
	void OnBuildAction(const bool);

	template<bool Value>
	void OnRotateAction_Helper() { OnRotateAction(Value); }
	void OnRotateAction(const bool);

	template<bool Value>
	void OnSelectionActionList_Helper() { OnSelectionActionList(Value); }
	void OnSelectionActionList(const bool);

	template<int32 Value>
	void OnSelectionAction_Helper() { OnSelectionAction(Value); }
	void OnSelectionAction(const int32);

	template<int32 Value>
	void OnModeAction_Helper() { OnModeAction(Value); }
	void OnModeAction(const int32);

	void OnToggleInventory();
	void OnToggleMode();
	void OnToggleInGameMenu();

	TSharedPtr<SBuildSystem_ToolBar> Slate_ToolBar;
	TSharedPtr<SBuildSystem_Inventory> Slate_Inventory;
	TSharedPtr<SVerticalMenuBuilder> Slate_EscapeMenu;
	TSharedPtr<SSettingsManager> Slate_SettingsManager;
	TSharedPtr<SWindowBuyBuilding> Slate_WindowBuyBuilding;

	UFUNCTION(Client, Reliable)
	void ClientPlaceObjectOnNotFoundInstance();

	UFUNCTION(Client, Reliable)
	void ClientPlaceObjectOnNotEnouthItem();

	UFUNCTION(Client, Reliable)
	void ClientPlaceObjectOnFailed();

	UFUNCTION(Client, Reliable)
	void ClientRemoveObjectOnFailed();

	UFUNCTION(Client, Reliable)
	void ClientOnStartTutorialOnFailed();

	UFUNCTION(Client, Reliable)
	void ClientOnStartTutorialOnNotFoundInstance();

	UFUNCTION(Client, Reliable)
	void ClientOnStartTutorialOnNotFoundSteps();

	UFUNCTION(Client, Reliable)
	void ClientRespondSqaudInvite(const FString& InPlayerName);

	UFUNCTION(Client, Reliable)
	void ClientRespondSqaudInviteNotFound();

	UFUNCTION(Client, Reliable)
	void ClientRespondSqauInviteAreRecived(const FString& InPlayerName);

	UFUNCTION(Client, Reliable)
	void ClientRespondSenderInSquad(const FString& InPlayerName);

	UFUNCTION(Client, Reliable)
	void ClientSquadDuelInvitesResponse(const FSquadInviteListContainer& InInvites);

	UFUNCTION(Client, Reliable)
	void ClientRespondDuelInviteNoMoney(const FString& InPlayerName);

	UFUNCTION(Client, Reliable)
	void ClientRespondDuelSenderNoMoney(const FString& InPlayerName);

	UFUNCTION(Client, Reliable)
	void ClientRespondDuelInvite(const FString& InPlayerName);

	UFUNCTION(Client, Reliable)
	void ClientMatchResults(const TArray<FLobbyMatchResultPreview>& InResults);

	UFUNCTION(Client, Reliable)
	void ClientToggleBuyWaiting(const int32 InModelId, const int32 InCount, const bool InToggle = true);

	UFUNCTION(Client, Reliable)
	void ClientShowBuyRequest(const int32 InModelId, const int32 InCount, const FString& InFromName);

protected:

	bool TryCreateSettingsManager();
	bool TryCreateMenu();

	UPROPERTY()
	ABuilderCharacter* MyCharacterBuilder;

	UFUNCTION(Client, Reliable)
	void OnBuildItemSelected(const int32 InSlotNumTarget, UItemBuildEntity* InItem);

	UFUNCTION(Client, Reliable)
	void OnBuildItemBuyRequest(UItemBuildEntity* InItem);

	UFUNCTION()
	void OnRep_SavedSlots();

	UPROPERTY(Config, ReplicatedUsing=OnRep_SavedSlots)
	TArray<int32> SavedSlots;

	UFUNCTION()
	void OnBuildingSelected(ABuildPlacedComponent* InComponent);

	UFUNCTION()
	void OnBuildingPlaced(ABuildPlacedComponent* InComponent);

	UFUNCTION()
	void OnBuildingPreRemove(ABuildPlacedComponent* InComponent);

	UFUNCTION(Reliable, Server, WithValidation)
	void ServerRequestChangeSlot(const int32 InSlot, const int32 InModelId);

	UFUNCTION(Reliable, Server, WithValidation)
	void ServerRequestBuyBuilding(const int32 InModelId, const int32 InCount = 1);

	UFUNCTION(Reliable, Server, WithValidation)
	void ServerDuelAnswer(const FString& InInviteId, const bool InAnswer);

	UFUNCTION(BlueprintCallable, Reliable, Server, WithValidation)
	void ServerRequestExitMe();

	FTypeCash GetCurrentOwnerCash() const;

	UPROPERTY()
	bool IsInitAfterTimer;

	UPROPERTY()
	bool IsMatchMakingPrepareBeginNotified;
};
