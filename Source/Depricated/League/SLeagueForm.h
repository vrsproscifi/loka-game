// VRSPRO

#pragma once

#include "Widgets/SCompoundWidget.h"
#include "Slate/Styles/LeagueFormWidgetStyle.h"

#include "Slate/Components/IOnlineSlateError.h"

#include "LeagueController/Operation/Create.h"

struct FPlayerEntityInfo;

DECLARE_DELEGATE_OneParam(FOnMakeLeague, const Operation::CreateLeague::Request&)
DECLARE_DELEGATE_OneParam(FOnClickedLeague, const int32)

class LOKAGAME_API SLeagueForm : public SCompoundWidget, public IOnlineSlateError
{
public:
	SLATE_BEGIN_ARGS(SLeagueForm)
		: _Style(&FLokaSlateStyle::Get().GetWidgetStyle<FLeagueFormStyle>("SLeagueFormStyle"))
	{}
	SLATE_EVENT(FOnMakeLeague, OnMakeLeague)
	SLATE_EVENT(FOnClickedLeague, OnClickedLeague)
	SLATE_EVENT(FOnTextChanged, OnSearch)
	SLATE_STYLE_ARGUMENT(FLeagueFormStyle, Style)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	void AddItem(const struct FCorporationPreviewResponse &Details);
	void ClearItems();

protected:

	// IOnlineSlateError Interface Begin
	virtual void OnErrorReportingCode(const int32 &ErrorCode) override;
	virtual bool IsExistCode(const int32 &ErrorCode) const override;

	// IOnlineSlateError Interface End


	TSharedPtr<class SScrollBox> ListContainer;
	TArray<TSharedPtr<class SItemListRow>> ListWidgets;

	TSharedPtr<class SCheckBox> CheckBox_Open;
	TSharedPtr<class SCheckBox> CheckBox_Close;

	TSharedPtr<class SEditableTextBox> Text_Name;
	TSharedPtr<class SEditableTextBox> Text_Abbr;

	const FLeagueFormStyle *Style;

	void onCheckBoxLeagueType(ECheckBoxState State, const bool IsOpen);

	bool IsLeagueTypeOpen;

	FOnMakeLeague OnMakeLeague;
	FOnClickedLeague OnClickedLeague;

	FReply onClickedMakeLeague();
	FReply onClickedItem(const int32 Index);

	TSharedPtr<class FRegexPattern> Regex_Abbr;
};
