#pragma once
#include "SocialGroupId.generated.h"

UENUM()
namespace SocialGroupId
{
	enum Type
	{
		None,
		Vkontakte,
		FaceBook,
		Twitter,
		Steam,
		GooglePlus,

		End
	};
}
