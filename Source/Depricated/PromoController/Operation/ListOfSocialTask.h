#pragma once

#include "ServiceOperation.h"
#include "../Models/SocialTaskWrapper.h"
#include "PoolingOperation.h"

namespace Operation
{
	namespace ListOfSocialTask
	{
		class Operation final 
			: public COperation<void, ObjectSerializer::JsonObject, TArray<FSocialTaskWrapper>, ObjectSerializer::JsonArrayObject>
			, public FPoolingOperation
		{
		public:
			Operation() 
				: Super(FServerHost::MasterClient, "Promo/ListOfSocialTask", FVerb::Get) 
				, FPoolingOperation(30.0f, 2.5f)
			{
				TimerDelegate.BindRaw(this, &Operation::Request);
			}

		};
	}
}
