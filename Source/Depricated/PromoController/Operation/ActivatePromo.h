#pragma once

#include "ServiceHandler.h"
#include "ServiceOperation.h"
#include "../Models/ActivatePromo.h"
#include "RequestOneParam.h"


namespace Operation
{
	namespace ActivatePromo
	{
		class Operation final 
			: public COperation<FRequestOneParam<FString>, ObjectSerializer::NativeObject, FActivatePromoResponse, ObjectSerializer::JsonObject>
		{
		public:			
			IResponseHandler<void, OperationResponseType::None> OnCodeNotFound;
			IResponseHandler<void, OperationResponseType::None> OnCodeActivated;


			Operation() 
				: Super(FServerHost::MasterClient, "Promo/ActivatePromo")
				, OnCodeNotFound(400, this)
				, OnCodeActivated(409, this)
			{

			}

		};
	}
}
