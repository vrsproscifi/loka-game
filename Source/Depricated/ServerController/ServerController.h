#pragma once

#include "Operation/HeartBeat.h"

#include "Operation/AbortMatch.h"
#include "Operation/EndMatch.h"
#include "Operation/StartMatch.h"
#include "Operation/LeaveSession.h"

#include "Operation/ConfirmMemberJoin.h"

#include "Operation/OnUpdateBuilding.h"
#include "Operation/GetListOfBuildings.h"
#include "Operation/GetBuildingServerOwnerId.h"

class ServerController
{
public:
	Operation::NodeHeartBeat::Operation				NodeHeartBeat;
	Operation::AbortMatch::Operation				AbortMatch;
	Operation::EndMatch::Operation					EndMatch;
	Operation::StartMatch::Operation				StartMatch;
	
	
	Operation::LeaveSession::Operation				LeaveSession;

	Operation::ConfirmMemberJoin::Operation			ConfirmMemberJoin;

	Operation::OnUpdateBuilding::Operation				OnUpdateBuilding;
	Operation::GetServerListOfBuildings::Operation		GetListOfBuildings;
	Operation::GetBuildingServerOwnerId::Operation		GetBuildingServerOwnerId;

	
};