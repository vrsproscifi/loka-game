#pragma once
#include "ServiceHandler.h"
#include "ServiceOperation.h"
#include "RequestOneParam.h"
#include "../Models/MatchBuildingInformation.h"

namespace Operation
{
	namespace GetServerListOfBuildings
	{
		class Operation final
			: public COperation<void, ObjectSerializer::NativeObject, FMatchBuildingInformation, ObjectSerializer::JsonObject>
		{
		public:
			uint64 Attemp;
			Operation()
				: Super(FServerHost::MasterServer, "Node/GetListOfBuildings", FVerb::Get)
				, Attemp(1)
			{

			}
		};
	}
}