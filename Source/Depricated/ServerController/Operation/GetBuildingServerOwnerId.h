#pragma once
#include "ServiceOperation.h"
#include "RequestOneParam.h"


namespace Operation
{
	namespace GetBuildingServerOwnerId
	{
		class Operation final 
			: public COperation<void, ObjectSerializer::NativeObject, FRequestOneParam<FString>, ObjectSerializer::NativeObject>
		{
		public:
			Operation() 
				: Super(FServerHost::MasterServer, "Node/GetBuildingServerOwnerId", FVerb::Get)
			{
			}
		};
	}
}
