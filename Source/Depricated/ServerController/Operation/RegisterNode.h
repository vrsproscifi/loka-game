#pragma once
#include "ServiceOperation.h"
#include "PoolingOperation.h"


namespace Operation
{
	namespace RegisterNode
	{
		struct Request final : FOnlineJsonSerializable
		{
			virtual ~Request() { }

			FString NodeId;
			FString ClusterId;
			FString Host;
			int32 Port;

			BEGIN_ONLINE_JSON_SERIALIZER
				ONLINE_JSON_SERIALIZE("NodeId", NodeId);
				ONLINE_JSON_SERIALIZE("ClusterId", ClusterId);
				ONLINE_JSON_SERIALIZE("Host", Host);
				ONLINE_JSON_SERIALIZE("Port", Port);
			END_ONLINE_JSON_SERIALIZER
		};

		class Operation final 
			: public COperation<Request, ObjectSerializer::NativeObject, void, ObjectSerializer::NativeObject>
			, public FPoolingOperation
		{
		public:
			static const uint32 MaxAttemps = 5;
			uint32 CurrentAttemps;
			bool bIsRegistered;

			Operation() 
				: Super(FServerHost::ClusterServer, "Cluster/NodeRegisterInstance")
				, FPoolingOperation(2.5f, 0.0f)
				, CurrentAttemps(1)
				, bIsRegistered(false)
			{
			}
		};
	}
}