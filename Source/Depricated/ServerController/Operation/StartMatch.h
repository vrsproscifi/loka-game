#pragma once
#include "ServiceHandler.h"
#include "ServiceOperation.h"
#include "RequestOneParam.h"

namespace Operation
{
	namespace StartMatch
	{
		class Operation final
			: public COperation<FRequestOneParam<FString>, ObjectSerializer::NativeObject, void, ObjectSerializer::NativeObject>
		{
		public:
			Operation()
				: Super(FServerHost::MasterServer, "Node/StartMatch")
			{

			}
		};
	}
}