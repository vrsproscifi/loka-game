#pragma once
#include "ServiceHandler.h"
#include "ServiceOperation.h"
#include "Models/ConfirmMemberContainer.h"

namespace Operation
{
	namespace ConfirmMemberJoin
	{
		class Operation final
			: public COperation<FConfirmMemberRequest, ObjectSerializer::JsonObject, void, ObjectSerializer::NativeObject>
		{
		public:
			Operation()
				: Super(FServerHost::MasterServer, "Node/ConfirmMemberJoin")
			{

			}
		};
	}
}