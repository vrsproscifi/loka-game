// VRSPRO

#pragma once


#include "Components/SAnimatedBackground.h"

/**
 * 
 */
class LOKAGAME_API SScrollableContainer : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SScrollableContainer)
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_ARGUMENT(const TArray<TSharedRef<SWidget>>*, WidgetsSource)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);
	
	FORCEINLINE int32 GetActiveIndex() const { return ActiveIndex; }
	bool SetActiveIndex(const int32&);

protected:

	bool IsNextAsCurrent;
	int32 ActiveIndex;

	TSharedPtr<class SAnimatedBackground> Widget_Current;
	TSharedPtr<class SAnimatedBackground> Widget_Next;

	const TArray<TSharedRef<SWidget>>* Widget_Array;
};
