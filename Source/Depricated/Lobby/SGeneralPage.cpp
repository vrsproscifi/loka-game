// VRSPRO

#include "LokaGame.h"
#include "SGeneralPage.h"
#include "SlateOptMacros.h"

#include "Layout/SScaleBox.h"
#include "SWindowBackground.h"
#include "IdentityController/Models/PlayerEntityInfo.h"
#include "PromoController/Models/SocialTaskWrapper.h"

#include "SFriends_Commands.h"

#include "SMessageBox.h"
#include "SLokaToolTip.h"
#include "SWindowVotingForChange.h"
#include "SWindowVotingForProposals.h"
#include "SWindowDetailsForProposals.h"

#include "MarketingController/MarketingController.h"
#include "Marketing/Improvements/ImprovementProgressState.h"

class SGeneralPage_TopRow : public SMultiColumnTableRow<TSharedPtr<FPlayerRatingTopContainer>>
{
	typedef SMultiColumnTableRow<TSharedPtr<FPlayerRatingTopContainer>> SSuper;

public:
	SLATE_BEGIN_ARGS(SGeneralPage_TopRow)
		: _Style(&FLokaSlateStyle::Get().GetWidgetStyle<FGeneralPageStyle>("SGeneralPageStyle"))
	{}
	SLATE_STYLE_ARGUMENT(FGeneralPageStyle, Style)
	SLATE_END_ARGS()

	BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
	void Construct(const FArguments& InArgs, const TSharedRef<STableViewBase>& OwnerTable, TSharedPtr<FPlayerRatingTopContainer> Item)
	{
		Style = InArgs._Style;
		Instance = Item;

		SSuper::Construct(SSuper::FArguments().Style(&Style->TableRow).Padding(FMargin(0, 2)), OwnerTable);
	}
	END_SLATE_FUNCTION_BUILD_OPTIMIZATION

	virtual TSharedRef<SWidget> GenerateWidgetForColumn(const FName& InColumnName) override
	{
		if (InColumnName == TEXT("Place"))
		{
			return SNew(STextBlock).TextStyle(/*Instance->IsLocal ? &Style->YourRowText : */&Style->OtherRowText).Text_Lambda([&]() { return FText::AsNumber(IndexInList + 1); }).Margin(FMargin(5, 0, 0, 0));
		}
		else if (InColumnName == TEXT("Name"))
		{
			return SNew(SBox).HAlign(HAlign_Left)[SNew(SHyperlink).UnderlineStyle(&Style->Hyperlink).TextStyle(Instance->IsOnline ? &Style->OtherRowTextOnline : &Style->OtherRowText).Text(FText::FromString(Instance->Name)).OnNavigate_Lambda([&]() {
				SFriendsCommands::GetAlt().SetCurrentTargetId(Instance->Id);
				FMenuBuilder MenuBuilder(true, SFriendsCommands::Get().GetCommandList());
				{
					MenuBuilder.BeginSection("FriendMenu.Header", FText::FromString(Instance->Name));
					{
						MenuBuilder.AddMenuEntry(SFriendsCommands::Get().GetCommand(SFriendsCommands::DuelInvite));
						MenuBuilder.AddMenuEntry(SFriendsCommands::Get().GetCommand(SFriendsCommands::SquadInvite));
						MenuBuilder.AddMenuEntry(SFriendsCommands::Get().GetCommand(SFriendsCommands::Attack));
					}
					MenuBuilder.EndSection();

					MenuBuilder.BeginSection("FriendMenu.Middle");
					{
						MenuBuilder.AddMenuEntry(SFriendsCommands::Get().GetCommand(SFriendsCommands::AddFriend));
					}
					MenuBuilder.EndSection();
				}

				FLokaSlateStyle::ReGenerateContextMenuStyle();
				MenuBuilder.SetStyle(&FLokaSlateStyle::Get(), "Menu");

				FSlateApplication::Get().PushMenu(AsShared(), FWidgetPath(), MenuBuilder.MakeWidget(), FSlateApplication::Get().GetCursorPos(), FPopupTransitionEffect(FPopupTransitionEffect::ContextMenu));
			})];
		}
		else if (InColumnName == TEXT("Score"))
		{
			return SNew(STextBlock).TextStyle(/*Instance->IsLocal ? &Style->YourRowText : */&Style->OtherRowText).Text(FText::AsNumber(Instance->Score));
		}
		else
		{
			return SNew(STextBlock).TextStyle(&Style->OtherRowText).Text(FText::Format(FText::FromString("Unsupported column with name: {0}"), FText::FromName(InColumnName)));
		}
	}

protected:

	const FGeneralPageStyle* Style;

	TSharedPtr<FPlayerRatingTopContainer> Instance;
};


BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SGeneralPage::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	
	OnClickedSuggestion = InArgs._OnClickedSuggestion;
	OnCreateProposal = InArgs._OnCreateProposal;

	TSharedPtr<SScrollBar> ScrollBar = SNew(SScrollBar).Style(&FLokaSlateStyle::Get().GetWidgetStyle<FScrollBarStyle>("Default_ScrollBar")).Thickness(FVector2D(4, 4));
	TSharedPtr<SScrollBar> ScrollBar_InDev = SNew(SScrollBar).Style(&FLokaSlateStyle::Get().GetWidgetStyle<FScrollBarStyle>("Default_ScrollBar")).Thickness(FVector2D(4, 4));
	TSharedPtr<SScrollBar> ScrollBar_Ideas = SNew(SScrollBar).Style(&FLokaSlateStyle::Get().GetWidgetStyle<FScrollBarStyle>("Default_ScrollBar")).Thickness(FVector2D(4, 4));
	SAssignNew(Widget_VotingForChanges, SWindowVotingForChange).OnVoteCreate(InArgs._OnVoteChange);
	SAssignNew(Widget_VotingForProposals, SWindowVotingForProposals).OnVoted(InArgs._OnVoteProposal)
	.OnCreate_Lambda([&]() {
		FImprovementView Temp;
		Temp.Title = FString( FPlayerEntityInfo::Instance.Login + "New Improvement");
		TSharedRef<FImprovementView> Item = MakeShareable(new FImprovementView(Temp));
		OpenDetailsForProposals(Item, false);
	});

	SAssignNew(Widget_DetailsForProposals, SWindowDetailsForProposals)
	.OnCreated_Lambda([&](FImprovementViewPtr Item) {
		FImprovementCreateView Temp;
		Temp.Title = Item->Title;
		Temp.Message = Item->Message;
		OnCreateProposal.ExecuteIfBound(Temp);
		SMessageBox::Get()->ToggleWidget(false);
	});

	ChildSlot
	[
		SNew(SOverlay)
		+ SOverlay::Slot().HAlign(HAlign_Left)
		[
			SNew(SBox).WidthOverride(400)
			[
				SAssignNew(SideContainer_Left, SVerticalBox)
				+ SVerticalBox::Slot().AutoHeight()
				[
					SNew(SWindowBackground)
					.IsExpandable(true)
					.HeaderPadding(FMargin(0, 6))
					.IsEnabledHeaderBackground(false)
					.Header(NSLOCTEXT("SGeneralPage", "SGeneralPage.YourStatistics.Title", "Your Statistics"))
					.Padding(FMargin(10, 6))
					.IsEnabledBlur(true)
					[
						SNew(SVerticalBox)
						+ SVerticalBox::Slot().AutoHeight().Padding(FMargin(80, 2))
						[
							SNew(SHorizontalBox)
							+ SHorizontalBox::Slot()
							[
								SNew(STextBlock).Text(NSLOCTEXT("SGeneralPage", "SGeneralPage.YourStatistics.YourPlace", "Your Place")).TextStyle(&Style->YourPlaceText)
							]
							+ SHorizontalBox::Slot().AutoWidth()
							[
								SNew(STextBlock).TextStyle(&Style->YourPlaceText)
								.Text_Lambda([&]() {
								return FText::AsNumber(FPlayerEntityInfo::Instance.Raiting.Position + 1);
								})
							]
						]
						+ SVerticalBox::Slot().AutoHeight().Padding(FMargin(80, 2))
						[
							SNew(SHorizontalBox)
							+ SHorizontalBox::Slot()
							[
								SNew(STextBlock).Text(NSLOCTEXT("SGeneralPage", "SGeneralPage.YourStatistics.Playing", "Playing hours")).TextStyle(&Style->YourPlaceText)
							]
							+ SHorizontalBox::Slot().AutoWidth()
							[
								SNew(STextBlock).Text(FText::FromString("--/60")).TextStyle(&Style->YourPlaceText)
								.Text_Lambda([&]() {
								auto FNFO = FNumberFormattingOptions::DefaultNoGrouping();
								FNFO.SetMinimumIntegralDigits(1);
								FNFO.SetMaximumFractionalDigits(1);

								return FText::Format(FText::FromString("{0}/60"), FText::AsNumber(FTimespan::FromSeconds(FPlayerEntityInfo::Instance.Raiting.ConductedTime).GetTotalHours(), &FNFO));
								})
							]
						]
						+ SVerticalBox::Slot().AutoHeight().Padding(FMargin(80, 2))
						[
							SNew(SHorizontalBox)
							+ SHorizontalBox::Slot()
							[
								SNew(STextBlock).Text(NSLOCTEXT("SGeneralPage", "SGeneralPage.YourStatistics.Score", "Score")).TextStyle(&Style->YourPlaceText)
							]
							+ SHorizontalBox::Slot().AutoWidth()
							[
								SNew(STextBlock).TextStyle(&Style->YourPlaceText)
								.Text_Lambda([&]() {
								return FText::AsNumber(FPlayerEntityInfo::Instance.Raiting.Experience);
								})
							]
						]
					]
				]
				+ SVerticalBox::Slot()
				[
					SNew(SWindowBackground)
					.HeaderPadding(FMargin(0, 6))
					.IsEnabledHeaderBackground(false)
					.Header(NSLOCTEXT("SGeneralPage", "SGeneralPage.TopPlayers.Title", "Top Players"))
					.Padding(FMargin(10, 6))
					.IsEnabledBlur(true)
					[
						SNew(SVerticalBox)
						+ SVerticalBox::Slot().Padding(FMargin(2, 10))
						[
							SNew(SOverlay)
							+ SOverlay::Slot()
							[
								SAssignNew(Widget_TopTable, SListView<TSharedPtr<FPlayerRatingTopContainer>>)
								.ListItemsSource(&TopTablePlayers)
								.SelectionMode(ESelectionMode::None)
								.ExternalScrollbar(ScrollBar)
								.HeaderRow(SNew(SHeaderRow).Style(&Style->HeaderRow)
									+ SHeaderRow::Column("Place").FillWidth(.2f).HeaderContentPadding(FMargin(0, 4))[SNew(STextBlock).Text(NSLOCTEXT("SGeneralPage", "SGeneralPage.TopPlayers.Place", "#")).TextStyle(&Style->TableRowText).Margin(FMargin(5, 0, 0, 0))]
									+ SHeaderRow::Column("Name").FillWidth(1.f).HeaderContentPadding(FMargin(0, 4))[SNew(STextBlock).Text(NSLOCTEXT("SGeneralPage", "SGeneralPage.TopPlayers.Name", "Name")).TextStyle(&Style->TableRowText)]
									+ SHeaderRow::Column("Score").FillWidth(.6f).HeaderContentPadding(FMargin(0, 4))[SNew(STextBlock).Text(NSLOCTEXT("SGeneralPage", "SGeneralPage.TopPlayers.Score", "Score")).TextStyle(&Style->TableRowText)]
								)
								.OnGenerateRow_Lambda([&](TSharedPtr<FPlayerRatingTopContainer> Item, const TSharedRef<STableViewBase>& OwnerTable) {
									return SNew(SGeneralPage_TopRow, OwnerTable, Item);
								})
							]
							+ SOverlay::Slot().HAlign(HAlign_Right).Padding(FMargin(0, 40, 0, 0))
							[
								ScrollBar.ToSharedRef()
							]
						]
					]
				]
			]
		]
		+ SOverlay::Slot().HAlign(HAlign_Right).VAlign(VAlign_Top)
		[
			SNew(SBox).WidthOverride(400)
			[
				SAssignNew(SideContainer_Right, SVerticalBox)				
				//+ SVerticalBox::Slot().AutoHeight()
				//[
				//	SNew(SWindowBackground)
				//	.IsEnabledHeaderBackground(false)
				//	.HeaderPadding(FMargin(0, 6))
				//	.Header(NSLOCTEXT("SGeneralPage", "SGeneralPage.PromoCode.Title", "Promo Code"))
				//	.Padding(FMargin(10, 6))
				//	[
				//		SNew(SVerticalBox)
				//		+ SVerticalBox::Slot().AutoHeight().Padding(FMargin(40, 2))
				//		[
				//			SAssignNew(Input_ReedemCode, SEditableTextBox).Style(&FLokaSlateStyle::Get().GetWidgetStyle<FEditableTextBoxStyle>("Default_EditableTextBox"))
				//		]
				//		+ SVerticalBox::Slot().AutoHeight().HAlign(HAlign_Center).Padding(FMargin(10, 4))
				//		[
				//			SNew(SButton)
				//			.HAlign(HAlign_Center)
				//			.VAlign(VAlign_Center)
				//			.TextStyle(&Style->YourPlaceText)
				//			.ContentPadding(FMargin(8, 2))
				//			.Text(NSLOCTEXT("SGeneralPage", "SGeneralPage.PromoCode.REDEEM", "REDEEM CODE"))
				//			.ButtonStyle(&Style->Button)
				//			.OnClicked_Lambda([&]() {
				//				OnReedemCode.ExecuteIfBound(Input_ReedemCode->GetText().ToString()); 
				//				return FReply::Handled(); 
				//			})
				//		]
				//	]
				//]
				//+ SVerticalBox::Slot().AutoHeight()
				//[
				//	SNew(SBox).MaxDesiredHeight(360)
				//	[
				//		SNew(SWindowBackground)
				//		.IsExpandable(true)
				//		.IsEnabledHeaderBackground(false)
				//		.HeaderPadding(FMargin(0, 6))
				//		.Header(NSLOCTEXT("SGeneralPage", "SGeneralPage.Changes.Title", "Changes"))
				//		.Padding(FMargin(10, 6))
				//		.IsEnabledBlur(true)
				//		[
				//			SNew(SVerticalBox)
				//			+ SVerticalBox::Slot().AutoHeight().Padding(FMargin(2, 6))
				//			[
				//				SNew(STextBlock)
				//				.Text(NSLOCTEXT("SGeneralPage", "SGeneralPage.Changes.List", "The list below, reflects the current state of readiness of selected changes in the past of voting (completed is marked)."))
				//				.TextStyle(&Style->YourPlaceText)
				//				.AutoWrapText(true)
				//				.Visibility_Lambda([&]() { return (ItemsInDev.Num() > 0) ? EVisibility::Visible : EVisibility::Hidden; })
				//			]
				//			+ SVerticalBox::Slot().HAlign(HAlign_Fill).Padding(FMargin(2, 6))
				//			[
				//				SNew(SOverlay)
				//				+ SOverlay::Slot()
				//				[
				//					SAssignNew(Widget_ListInDev, SListView<FImprovementViewPtr>)
				//					.SelectionMode(ESelectionMode::None)
				//					.ListItemsSource(&ItemsInDev)
				//					.ExternalScrollbar(ScrollBar_InDev)
				//					.OnGenerateRow_Lambda([&, VotingStyle = &FLokaSlateStyle::Get().GetWidgetStyle<FVotingStyle>("SVotingStyle")](FImprovementViewPtr Item, const TSharedRef<STableViewBase>& OwnerTable) {
				//						return SNew(STableRow<FImprovementViewPtr>, OwnerTable)
				//						.Style(&VotingStyle->Changes.Row)
				//						.ToolTip(SNew(SLokaToolTip).Text(FText::FromString(Item->Message)))
				//						.Padding(FMargin(4, VotingStyle->Changes.RowHeightPadding))
				//						[
				//							SNew(SHorizontalBox)
				//							+ SHorizontalBox::Slot().AutoWidth()
				//							[
				//								SNew(SCheckBox)
				//								.Style(&FLokaSlateStyle::Get().GetWidgetStyle<FCheckBoxStyle>("Default_CheckBox"))
				//								.IsChecked_Lambda([&, i = Item]() { return (i->ProgressState == EImprovementProgressState::Release) ? ECheckBoxState::Checked : ECheckBoxState::Unchecked; })
				//							]
				//							+ SHorizontalBox::Slot()
				//							[
				//								SNew(STextBlock)
				//								.Text(FText::FromString(Item->Title))
				//								.TextStyle(&VotingStyle->Changes.VoteTitle)
				//							]
				//						];
				//					})	
				//				]
				//				+ SOverlay::Slot().HAlign(HAlign_Right)
				//				[
				//					ScrollBar_InDev.ToSharedRef()
				//				]
				//			]
				//			+ SVerticalBox::Slot().AutoHeight().Padding(FMargin(2, 6))
				//			[
				//				SNew(STextBlock).Text(NSLOCTEXT("SGeneralPage", "SGeneralPage.Changes.Desc", "Influence the development of the game, you can choose what should be in the next update.")).TextStyle(&Style->YourPlaceText).AutoWrapText(true)
				//			]
				//			+ SVerticalBox::Slot().AutoHeight().HAlign(HAlign_Fill).Padding(FMargin(2, 6))
				//			[
				//				SNew(SButton)
				//				.HAlign(HAlign_Center)
				//				.VAlign(VAlign_Center)
				//				.TextStyle(&Style->YourPlaceText)
				//				.ContentPadding(FMargin(8, 2))
				//				.Text(NSLOCTEXT("SGeneralPage", "SGeneralPage.Changes.Show", "Vote for the next changes"))
				//				.ButtonStyle(&Style->Button)
				//				.OnClicked_Lambda([&]() {
				//					OpenVotingForChanges();
				//					return FReply::Handled();
				//				})
				//			]						
				//		]
				//	]
				//]
				//+ SVerticalBox::Slot().AutoHeight()
				//[
				//	SNew(SBox).MaxDesiredHeight(360)
				//	[
				//		SNew(SWindowBackground)
				//		.IsExpandable(true)
				//		.IsEnabledHeaderBackground(false)
				//		.HeaderPadding(FMargin(0, 6))
				//		.Header(NSLOCTEXT("SGeneralPage", "SGeneralPage.TopProposals.Title", "Top ideas"))
				//		.Padding(FMargin(10, 6))
				//		.IsEnabledBlur(true)
				//		[
				//			SNew(SVerticalBox)
				//			+ SVerticalBox::Slot().AutoHeight().Padding(FMargin(2, 6))
				//			[
				//				SNew(STextBlock)
				//				.Text(NSLOCTEXT("SGeneralPage", "SGeneralPage.TopProposals.List", "Below, you can see part of the best ideas from all players for the future vote, the list is sorted by rating."))
				//				.TextStyle(&Style->YourPlaceText)
				//				.AutoWrapText(true)
				//				.Visibility_Lambda([&]() { return (ItemsTopProposals.Num() > 0) ? EVisibility::Visible : EVisibility::Hidden; })
				//			]						
				//			+ SVerticalBox::Slot().HAlign(HAlign_Fill).Padding(FMargin(2, 6))
				//			[
				//				SNew(SOverlay)
				//				+ SOverlay::Slot()
				//				[
				//					SAssignNew(Widget_ListTopProposals, SListView<FImprovementViewPtr>)
				//					.SelectionMode(ESelectionMode::None)
				//					.ListItemsSource(&ItemsTopProposals)
				//					.ExternalScrollbar(ScrollBar_Ideas)
				//					.OnGenerateRow_Lambda([&, VotingStyle = &FLokaSlateStyle::Get().GetWidgetStyle<FVotingStyle>("SVotingStyle")](FImprovementViewPtr Item, const TSharedRef<STableViewBase>& OwnerTable) {
				//						return SNew(STableRow<FImprovementViewPtr>, OwnerTable)
				//						.Style(&VotingStyle->Changes.Row)
				//						.ToolTip(SNew(SLokaToolTip).Text(FText::FromString(Item->Message)))
				//						.Padding(FMargin(4, VotingStyle->Changes.RowHeightPadding))
				//						[
				//							SNew(STextBlock)
				//							.Text(FText::FromString(Item->Message.Left(30) + "..."))
				//							.TextStyle(&VotingStyle->Changes.VoteTitle)
				//						];
				//					})
				//				]
				//				+ SOverlay::Slot().HAlign(HAlign_Right)
				//				[
				//					ScrollBar_Ideas.ToSharedRef()
				//				]
				//			]
				//			+ SVerticalBox::Slot().AutoHeight().Padding(FMargin(2, 6))
				//			[
				//				SNew(STextBlock).Text(NSLOCTEXT("SGeneralPage", "SGeneralPage.TopProposals.Desc", "Rate the offers of other players, or leave your own. The best ideas will be included in the list of future updates.")).TextStyle(&Style->YourPlaceText).AutoWrapText(true)
				//			]
				//			+ SVerticalBox::Slot().AutoHeight().HAlign(HAlign_Fill).Padding(FMargin(2, 6))
				//			[
				//				SNew(SButton)
				//				.HAlign(HAlign_Center)
				//				.VAlign(VAlign_Center)
				//				.TextStyle(&Style->YourPlaceText)
				//				.ContentPadding(FMargin(8, 2))
				//				.Text(NSLOCTEXT("SGeneralPage", "SGeneralPage.Proposal.Show", "Vote for the ideas"))
				//				.ButtonStyle(&Style->Button)
				//				.OnClicked_Lambda([&]() {
				//					OpenVotingForProposals();
				//					return FReply::Handled();
				//				})
				//			]
				//			+ SVerticalBox::Slot().AutoHeight().HAlign(HAlign_Fill).Padding(FMargin(2, 6))
				//			[
				//				SNew(SButton)
				//				.HAlign(HAlign_Center)
				//				.VAlign(VAlign_Center)
				//				.TextStyle(&Style->YourPlaceText)
				//				.ContentPadding(FMargin(8, 2))
				//				.Text(NSLOCTEXT("SWindowVotingForProposals", "SWindowVotingForProposals.New", "Leave your idea"))
				//				.ButtonStyle(&Style->Button)
				//				.OnClicked_Lambda([&]() {
				//					FImprovementView Temp;
				//					Temp.Title = FString( FPlayerEntityInfo::Instance.Login + "New Improvement");
				//					TSharedRef<FImprovementView> Item = MakeShareable(new FImprovementView(Temp));
				//					OpenDetailsForProposals(Item, false);
				//					return FReply::Handled();
				//				})
				//				.IsEnabled_Lambda([&]() { return (FDateTime::UtcNow().ToUnixTimestamp() - FPlayerEntityInfo::Instance.DateOfNextAvailableProposal >= 0); })
				//				.ToolTip(SNew(SLokaToolTip)
				//							.Text_Lambda([&]() 
				//				{
				//					return FText::Format
				//					(
				//						NSLOCTEXT("SWindowVotingForProposals", "SWindowVotingForProposals.AvailableIn", "Available in {0} minuts"),
				//						FText::AsNumber(FTimespan::FromSeconds(FPlayerEntityInfo::Instance.DateOfNextAvailableProposal - FDateTime::UtcNow().ToUnixTimestamp()).GetMinutes())); 
				//					})
				//					.IsEnabled_Lambda([&]() 
				//					{
				//						return (FPlayerEntityInfo::Instance.DateOfNextAvailableProposal - FDateTime::UtcNow().ToUnixTimestamp() >= 0); 
				//					}))
				//			]
				//		]
				//	]
				//]
				//+ SVerticalBox::Slot().AutoHeight().Padding(FMargin(4, 10))
				//[
				//	SNew(STextBlock).Text_Lambda([&]() {
				//		return FText::Format(NSLOCTEXT("Notify", "Notify.AlphaVersionWarning", "You are currently playing on the open alpha version of the game. Thank you for your attention and support! Servers can be wiped and you progress lost, before the tournament. The next update maybe: {0}."), FText::AsDate(FDateTime::UtcNow() + FTimespan(1, 0, 0, 0, 0), EDateTimeStyle::Long));
				//	}).TextStyle(&Style->YourPlaceText).AutoWrapText(true)
				//]
			]
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SGeneralPage::OpenVotingForChanges()
{
	SMessageBox::Get()->SetHeaderText(NSLOCTEXT("SGeneralPage", "SGeneralPage.Voting.Changes", "Voting For Changes"));
	SMessageBox::Get()->SetContent(Widget_VotingForChanges.ToSharedRef());
	SMessageBox::Get()->SetAllowedSize(FBox2D(FVector2D(520, 120), FVector2D(520, 920)));
	SMessageBox::Get()->SetButtonsText();
	SMessageBox::Get()->SetEnableClose(true);
	SMessageBox::Get()->ToggleWidget(true);
}

void SGeneralPage::OpenVotingForProposals()
{
	SMessageBox::Get()->SetHeaderText(NSLOCTEXT("SGeneralPage", "SGeneralPage.Voting.Proposals", "Voting For Ideas"));
	SMessageBox::Get()->SetContent(Widget_VotingForProposals.ToSharedRef());
	SMessageBox::Get()->SetAllowedSize(FBox2D(FVector2D(520, 120), FVector2D(520, 920)));
	SMessageBox::Get()->SetButtonsText();
	SMessageBox::Get()->SetEnableClose(true);
	SMessageBox::Get()->ToggleWidget(true);
}

void SGeneralPage::OpenDetailsForProposals(TSharedRef<FImprovementView> InData, const bool IsReadOnly)
{
	Widget_DetailsForProposals->SetSource(InData, IsReadOnly);

	SMessageBox::Get()->SetHeaderText(NSLOCTEXT("SGeneralPage", "SGeneralPage.Voting.ProposalDetails", "Idea Details"));
	SMessageBox::Get()->SetContent(Widget_DetailsForProposals.ToSharedRef());
	SMessageBox::Get()->SetAllowedSize(FBox2D(FVector2D(480, 320), FVector2D(480, 920)));
	SMessageBox::Get()->SetButtonsText();
	SMessageBox::Get()->SetEnableClose(true);
	SMessageBox::Get()->ToggleWidget(true);
}

void SGeneralPage::OnFillRaiting(const FPlayerRatingTopView& view)
{
	TopTablePlayers.Empty();

	for (auto &a : view.PlayerRating)
	{
		TopTablePlayers.Add(MakeShareable(new FPlayerRatingTopContainer(a)));
	}

	Widget_TopTable->RequestListRefresh();
}

void SGeneralPage::OnFillIssues(const FImprovementViewModel& InData)
{
	//ItemsInDev.Empty();
	//ItemsTopProposals.Empty();

	//auto ListInDev = InData.ImprovementViews.FilterByPredicate([](const FImprovementView& Source) { return (Source.ProgressState == EImprovementProgressState::Development || Source.ProgressState == EImprovementProgressState::Release); });
	//ListInDev.Sort([](const FImprovementView& A, const FImprovementView& B) { return B.CreatedDate > A.CreatedDate; });
	//for (auto i : ListInDev)
	//{
	//	ItemsInDev.Add(MakeShareable(new FImprovementView(i)));
	//}
	//
	//auto ListTopProposals = InData.SentenceViews;
	//ListTopProposals.Sort([](const FImprovementView& A, const FImprovementView& B) { return B.Count < A.Count; });
	//for (auto i : ListTopProposals)
	//{
	//	ItemsTopProposals.Add(MakeShareable(new FImprovementView(i)));
	//}

	//Widget_ListInDev->RequestListRefresh();
	//Widget_ListTopProposals->RequestListRefresh();

	//Widget_VotingForChanges->OnFillChangeIssues(InData.ImprovementViews, InData.DateOfEndVotes);
	//Widget_VotingForProposals->OnFillProposalsIssues(InData.SentenceViews);
}

void SGeneralPage::SetBuildTime_MasterServer(const int32& InBuildTime)
{
	BuildTime_MasterServer = InBuildTime;
}

void SGeneralPage::AddSocialTask(const FSocialTaskWrapper& Task)
{
	//SocialTaskContainer->AddSlot().AutoWidth().Padding(FMargin(4, 2))
	//	[
	//		SNew(SButton)
	//		.ButtonStyle(&Style->Button)
	//		.HAlign(HAlign_Center)
	//		.VAlign(VAlign_Center)
	//		.ContentPadding(FMargin(8, 2))
	//		.TextStyle(&Style->YourPlaceText)
	//		.Text(FText::FromString(GetEnumValueToString<SocialGroupId::Type>("SocialGroupId", static_cast<SocialGroupId::Type>(Task.SocialGroupId))))
	//		.OnClicked_Lambda([&, t = Task]() {
	//			OnClickedSuggestion.ExecuteIfBound(FRequestOneParam<FString>(t.TaskId));
	//			return FReply::Handled();
	//		})
	//	];
}

void SGeneralPage::ClearCodeInput()
{
	Input_ReedemCode->SetText(FText::GetEmpty());
}

void SGeneralPage::SetSpecificWidget(TSharedRef<SWidget> Widget, const bool IsFriends)
{
	if (IsFriends)
	{
		SideContainer_Left->AddSlot().FillHeight(.6f).Padding(FMargin(4, 10))
			[
				Widget
			];
	}
	else
	{
		SideContainer_Right->AddSlot().FillHeight(1.0f).Padding(FMargin(4, 10))
			[
				Widget
			];
	}
}