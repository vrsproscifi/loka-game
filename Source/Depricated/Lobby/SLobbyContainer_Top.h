// VRSPRO

#pragma once


#include "SLobbyContainer_PVE.h"

#include "SquadController/Models/SqaudInformation.h"

namespace EResourceTextBoxType { enum Type; };
namespace EGameModeTypeId { enum Type; };

namespace EESearchSessionState
{
	enum Type
	{
		None = 0,
		Search = 1,
		Waiting = 2,
		Complete = 4
	};
}

typedef EESearchSessionState::Type ESearchSessionState;

struct FSearchSessionParams;
DECLARE_DELEGATE_OneParam(FOnStartSearchGame, const FSearchSessionParams&);
DECLARE_DELEGATE_OneParam(FOnClcikedBooster, const EResourceTextBoxType::Type);

struct FPlayerEntityInfo;
struct FInstanceRepository;

class LOKAGAME_API SLobbyContainer_Top : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SLobbyContainer_Top)
		: _Style(&FLokaSlateStyle::Get().GetWidgetStyle<FLobbyContainerStyle>("SLobbyContainerStyle"))
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_STYLE_ARGUMENT(FLobbyContainerStyle, Style)
	SLATE_EVENT(FOnClickedOutside, OnSearchSessionCancel)
	SLATE_EVENT(FOnStartSearchGame, OnSearchSessionBegin)
	SLATE_EVENT(FOnClcikedBooster, OnClickedBooster)
	SLATE_EVENT(FOnClickedOutside, OnClickedBuildMode)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);
	void SetRepository(const FInstanceRepository* InRepository);

	void OnSquadInformationUpdate(const FSqaudInformation& information);
	void OnFillRaiting(const FPlayerRatingTopView&);

	ESearchSessionState GetSearchSessionStatus() const
	{
		return SearchSessionStatus;
	}

protected:

	const FLobbyContainerStyle* Style;
	const FInstanceRepository* Repository;

	FText GetPlayerName() const;
	int32 GetPlayerLevel() const;
	int32 GetPlayerMoney() const;
	int32 GetPlayerDonate() const;

	TOptional<float> GetPlayerProgress_Exp() const;
	FText GetPlayerProgressText_Exp() const;

	FText GetFractionRankName() const;
	FText GetFractionName() const;
	TOptional<float> GetFractionProgress_Exp() const;
	FText GetFractionProgressText_Exp() const;
	const FSlateBrush* GetFractionIcon() const;

	// Into Fight
	void OnClickedIntoFight(ECheckBoxState, const bool = false, const int32 = 0);
	FText GetIntoFightButtonText() const;

	FText GetLevelText() const;
	FText GetTimeText() const;

public:
	bool GetIsEnableModeChange() const;

protected:
	TSharedRef<SWidget> BuildBigModeButton(const FSlateBrush*, const EGameModeTypeId::Type&, const bool = true);
	TSharedPtr<class SAnimatedBackground> Widget_BigModeAnimated[2];
	TSharedPtr<SLobbyContainer_PVE> Widget_PveWindow;

public:
	void OnSearchSessionStatusRecived();
	void OnSearchSessionStatusFailed();

protected:
	ESearchSessionState SearchSessionStatus;
	FOnClickedOutside OnSearchSessionCancel;
	FOnStartSearchGame OnSearchSessionBegin;
	FOnClcikedBooster OnClickedBooster;

	int64 SearchTime;
	EGameModeTypeId::Type CurrentGameModeTypeId;
	bool IsSquadMemeber;
	bool IsSquadMemeberReady;

	int64 GetBoosterTime(const EResourceTextBoxType::Type) const;

	int32 SelectedGameModesFlags;
};
