// VRSPRO

#include "LokaGame.h"
#include "SLobbyContainer.h"
#include "SlateOptMacros.h"

#include "SScrollableContainer.h"
#include "SLobbyAchievements.h"
#include "Components/SLokaToolTip.h"
#include "Components/SAnimatedBackground.h"

#include "Notify/SNotifyList.h"
#include "Notify/SLobbyChat.h"
#include "Notify/SMessageBox.h"
#include "Friends/SFriendsForm.h"
#include "Settings/SSettingsManager.h"

#include "Analytics/AnalyticsMainUI.h"

#include "GameVersion.h"

// Need standart constructor for build
SLobbyContainer::SLobbyContainer()
	: Widget_NotifyList(StaticCastSharedRef<SNotifyList>(SNullWidget::NullWidget))
	, Widget_LobbyChat(StaticCastSharedRef<SLobbyChat>(SNullWidget::NullWidget))
	, Widget_FriendsForm(StaticCastSharedRef<SFriendsForm>(SNullWidget::NullWidget))
	, Widget_SettingsManager(StaticCastSharedRef<SSettingsManager>(SNullWidget::NullWidget))
{}

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SLobbyContainer::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	
	OnQuit = InArgs._OnQuit;
	OnClickedBuildMode = InArgs._OnClickedBuildMode;

	Widget_NotifyList = StaticCastSharedRef<SNotifyList>(InArgs._RefNotifyList);
	Widget_LobbyChat = StaticCastSharedRef<SLobbyChat>(InArgs._RefLobbyChat);
	Widget_FriendsForm = StaticCastSharedRef<SFriendsForm>(InArgs._RefFriendsForm);
	Widget_SettingsManager = StaticCastSharedRef<SSettingsManager>(InArgs._RefSettingsManager);

	MAKE_UTF8_SYMBOL(sChat, 0xf0e6);
	MAKE_UTF8_SYMBOL(sNotify, 0xf0f3);
	MAKE_UTF8_SYMBOL(sFriends, 0xf0c0);
	MAKE_UTF8_SYMBOL(sQuit, 0xf011);
	MAKE_UTF8_SYMBOL(sSettring, 0xf1de);

	ChildSlot
	[
		SNew(SOverlay)
		+ SOverlay::Slot().Padding(FMargin(0, 80, 0, 80)) // Scrollably widgets
		[
			SAssignNew(Widget_FxCenter, SAnimatedBackground)
			.ShowAnimation(EAnimBackAnimation::ZoomIn)
			.HideAnimation(EAnimBackAnimation::ZoomIn)
			.InitAsHide(true)
			[
				SAssignNew(ScrollablyContainer, SScrollableContainer)
				.WidgetsSource(&ScrollablyWidgets)
			]
		]
		+ SOverlay::Slot().VAlign(VAlign_Bottom).HAlign(HAlign_Fill).Padding(FMargin(0, Style->TopBottomOffset)) // Interface
		[
			SAssignNew(Widget_FxBottom, SAnimatedBackground)
			.ShowAnimation(EAnimBackAnimation::DownFade)
			.HideAnimation(EAnimBackAnimation::DownFade)
			.InitAsHide(true)
			[
				SNew(SBorder)
				.Padding(0)
				.BorderImage(&Style->BottomBackground)
				[
					SNew(SBorder)
					.BorderImage(&Style->BottomBorder)
					[
						SNew(SBox).HeightOverride(64).VAlign(VAlign_Center)
						[
							SNew(SOverlay)
							+ SOverlay::Slot().HAlign(HAlign_Center)
							[
								SAssignNew(ButtonsContainer, SHorizontalBox)
							]
							+ SOverlay::Slot().HAlign(HAlign_Left)
							[
								SNew(SHorizontalBox)
								+ SHorizontalBox::Slot().AutoWidth().HAlign(HAlign_Center).VAlign(VAlign_Center)
								[
									SAssignNew(Button_FriendsForm, SCheckBox).Type(ESlateCheckBoxType::ToggleButton)
									.Style(&Style->Buttons).ToolTip(SNew(SLokaToolTip).Text(NSLOCTEXT("SLobbyContainer", "Lobby.Contacts", "Contacts")))
									.OnCheckStateChanged_Lambda([&](ECheckBoxState InState) {
										if (InState == ECheckBoxState::Checked)
										{
											FLokaAnalytics::Get()->MakeActionAndStart(this, FAnalyticAction(EAnalyticCategory::Main, ELobbyMainAnalytic::Contacts));
										}
										else
										{
											StopAnalyticAction(ELobbyMainAnalytic::Contacts);
										}
									})
									[
										SNew(STextBlock).Text(FText::FromString(sFriends))
										.TextStyle(&Style->ButtonsIcons)
										.Justification(ETextJustify::Center)
										.Margin(FMargin(10, 6))
									]
								]
								+ SHorizontalBox::Slot().AutoWidth().HAlign(HAlign_Center).VAlign(VAlign_Center)
								[
									SAssignNew(Button_LobbyChat, SCheckBox).Type(ESlateCheckBoxType::ToggleButton)
									.Style(&Style->Buttons).ToolTip(SNew(SLokaToolTip).Text(NSLOCTEXT("SLobbyContainer", "Lobby.Chat", "Chat")))
									.OnCheckStateChanged_Lambda([&](ECheckBoxState InState) {
										if (InState == ECheckBoxState::Checked)
										{
											FLokaAnalytics::Get()->MakeActionAndStart(this, FAnalyticAction(EAnalyticCategory::Main, ELobbyMainAnalytic::Chat));
										}
										else
										{
											StopAnalyticAction(ELobbyMainAnalytic::Chat);
										}
									})
									[
										SNew(STextBlock).Text(FText::FromString(sChat))
										.TextStyle(&Style->ButtonsIcons)
										.Justification(ETextJustify::Center)
										.Margin(FMargin(10, 6))
									]
								]
							]
							+ SOverlay::Slot().HAlign(HAlign_Right)
							[
								SNew(SHorizontalBox)
								+ SHorizontalBox::Slot().AutoWidth().HAlign(HAlign_Center).VAlign(VAlign_Center)
								[
									SAssignNew(Button_NotifyList, SCheckBox).Type(ESlateCheckBoxType::ToggleButton)
									.Style(&Style->Buttons).ToolTip(SNew(SLokaToolTip).Text(NSLOCTEXT("SLobbyContainer", "Lobby.Notify", "Notifications")))
									.OnCheckStateChanged_Lambda([&, t = Widget_NotifyList](ECheckBoxState InState) {
										t->ToggleMode(InState == ECheckBoxState::Unchecked);

										if (InState == ECheckBoxState::Checked)
										{
											FLokaAnalytics::Get()->MakeActionAndStart(this, FAnalyticAction(EAnalyticCategory::Main, ELobbyMainAnalytic::Notifications));
										}
										else
										{
											StopAnalyticAction(ELobbyMainAnalytic::Notifications);
										}
									})
									[
										SNew(STextBlock).Text(FText::FromString(sNotify))
										.TextStyle(&Style->ButtonsIcons)
										.Justification(ETextJustify::Center)
										.Margin(FMargin(10, 6))
									]
								]
								+ SHorizontalBox::Slot().AutoWidth().HAlign(HAlign_Center).VAlign(VAlign_Center)
								[
									SAssignNew(Button_SettingsManager, SCheckBox).Type(ESlateCheckBoxType::ToggleButton)
									.Style(&Style->Buttons).ToolTip(SNew(SLokaToolTip).Text(NSLOCTEXT("SLobbyContainer", "Lobby.Settings", "Settings")))
									.OnCheckStateChanged_Lambda([&](ECheckBoxState InState) {
										if (InState == ECheckBoxState::Checked)
										{
											FLokaAnalytics::Get()->MakeActionAndStart(this, FAnalyticAction(EAnalyticCategory::Main, ELobbyMainAnalytic::Settings));
										}
										else
										{
											StopAnalyticAction(ELobbyMainAnalytic::Settings);
										}
									})
									[
										SNew(STextBlock).Text(FText::FromString(sSettring))
										.TextStyle(&Style->ButtonsIcons)
										.Justification(ETextJustify::Center)
										.Margin(FMargin(10, 6))
									]
								]
								+ SHorizontalBox::Slot().AutoWidth().HAlign(HAlign_Center).VAlign(VAlign_Center)
								[
									SAssignNew(CheckBox_Quit, SCheckBox).Type(ESlateCheckBoxType::ToggleButton)
									.Style(&Style->Buttons).ToolTip(SNew(SLokaToolTip).Text(NSLOCTEXT("SLobbyContainer", "Lobby.Exit", "Exit")))
									.OnCheckStateChanged_Lambda([&](ECheckBoxState State)
									{
										SMessageBox::Get()->SetHeaderText(NSLOCTEXT("SLobbyContainer", "Lobby.Exit", "Exit"));
										SMessageBox::Get()->SetContent(NSLOCTEXT("SLobbyContainer", "Lobby.Exit.Content", "You seriously want to exit game?"));
										SMessageBox::Get()->SetButtonText(EMessageBoxButton::Left, NSLOCTEXT("SLobbyContainer", "Lobby.Exit.Yes", "Yes"));
										SMessageBox::Get()->SetButtonText(EMessageBoxButton::Middle, FText::GetEmpty());
										SMessageBox::Get()->SetButtonText(EMessageBoxButton::Right, NSLOCTEXT("SLobbyContainer", "Lobby.Exit.No", "No"));
										SMessageBox::Get()->SetEnableClose(false);
										SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](const EMessageBoxButton& Button)
										{
											if (Button == EMessageBoxButton::Left)
											{
												OnQuit.ExecuteIfBound();
											}
											else
											{
												CheckBox_Quit->SetIsChecked(ECheckBoxState::Unchecked);
											}

											SMessageBox::Get()->ToggleWidget(false);
										});

										SMessageBox::Get()->ToggleWidget(true);
									})
									[
										SNew(STextBlock).Text(FText::FromString(sQuit))
										.TextStyle(&Style->ButtonsIcons)
										.Justification(ETextJustify::Center)
										.Margin(FMargin(10, 6))
									]
								]
							]
						]
					]
				]
			]			
		]
		+ SOverlay::Slot().VAlign(VAlign_Fill).HAlign(HAlign_Fill).Padding(0) // Interface
		[
			SAssignNew(Widget_FxTop, SAnimatedBackground)
			.ShowAnimation(EAnimBackAnimation::UpFade)
			.HideAnimation(EAnimBackAnimation::UpFade)
			.InitAsHide(true)
			[
				SAssignNew(Widget_ContainerTop, SLobbyContainer_Top)
				.OnSearchSessionBegin(InArgs._OnSearchSessionBegin)
				.OnSearchSessionCancel(InArgs._OnSearchSessionCancel)
				.OnClickedBooster(InArgs._OnClickedBooster)
				.OnClickedBuildMode(OnClickedBuildMode)
			]
		]
		+ SOverlay::Slot() // Overlay widgets (friends, chat, etc.)
		[
			SNew(SOverlay)
			+ SOverlay::Slot()
			[
				Widget_FriendsForm
			]
			+ SOverlay::Slot()
			[
				Widget_LobbyChat
			]
			+ SOverlay::Slot().HAlign(HAlign_Right).VAlign(VAlign_Bottom).Padding(FMargin(40, 100))
			[
				SNew(SBox).WidthOverride(420).HeightOverride(560)
				[
					Widget_NotifyList
				]
			]
			+ SOverlay::Slot().HAlign(HAlign_Right).Padding(FMargin(20, 100))
			[
				Widget_SettingsManager
			]
		]
		+ SOverlay::Slot().HAlign(HAlign_Right).VAlign(VAlign_Bottom)
		[
			SNew(STextBlock).Text(FText::FromString(GAME_VERSION_STRING))
		]
	];

	Widget_LobbyChat->SetVisibility(TAttribute<EVisibility>(this, &SLobbyContainer::GetVisibilityByCheck_LobbyChat));
	Widget_FriendsForm->SetVisibility(TAttribute<EVisibility>(this, &SLobbyContainer::GetVisibilityByCheck_FriendsForm));
	Widget_SettingsManager->SetVisibility(TAttribute<EVisibility>(this, &SLobbyContainer::GetVisibilityByCheck_SettingsManager)); 

	Widget_SettingsManager->OnClickedX.BindLambda([&]() { 
		Button_SettingsManager->SetIsChecked(ECheckBoxState::Unchecked); 
		StopAnalyticAction(ELobbyMainAnalytic::Settings);
	});
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SLobbyContainer::AddScrollablyWidget(const ELobbyActionButton Index, TSharedRef<SWidget> Widget, const FText& Name)
{
	ButtonsWidgets.Add
		(
			SNew(SCheckBox)
			.Type(ESlateCheckBoxType::ToggleButton)
			.OnCheckStateChanged(this, &SLobbyContainer::OnSetScrollablyWidget, ButtonsWidgets.Num())
			.IsChecked(this, &SLobbyContainer::IsCheckedWidget, ButtonsWidgets.Num())
			.Style(&Style->Buttons)
			[
				SNew(SBorder)
				.BorderImage(new FSlateNoResource())
				.Padding(0)
				.HAlign(HAlign_Center)
				.VAlign(VAlign_Center)
				[
					SNew(STextBlock)
					.TextStyle(&Style->ButtonsText)
					.Text(Name)
					.Justification(ETextJustify::Center)
					.Margin(FMargin(10, 6))
				]
			]
		);


	ScrollablyWidgetsMap.Add(Index, ScrollablyWidgets.Add(Widget));
	ButtonsContainer->AddSlot().AutoWidth()
		[
			ButtonsWidgets.Last().ToSharedRef()
		];

	if (ButtonsWidgets.Num() == 1)
	{
		const auto ActiveIndex = ScrollablyContainer->GetActiveIndex();
		ScrollablyContainer->SetActiveIndex(ActiveIndex);
		
		FLokaAnalytics::Get()->MakeActionAndStart(this, FAnalyticAction(EAnalyticCategory::Main, ActiveIndex));
	}
}

bool SLobbyContainer::SetScrollablyWidget(const ELobbyActionButton Index, TSharedRef<SWidget> Widget)
{
	if (const int32 *Founded = ScrollablyWidgetsMap.Find(Index))
	{
		ScrollablyWidgets[*Founded] = Widget;
		const auto OldActiveIndex = ScrollablyContainer->GetActiveIndex();
		if (ScrollablyContainer->SetActiveIndex(ScrollablyContainer->GetActiveIndex()))
		{
			FSlateApplication::Get().PlaySound(Style->SoundScollSlide);
			OnSwitchScrollablyWidget.Broadcast(Index, *ScrollablyWidgetsMap.FindKey(OldActiveIndex));
			return true;
		}
	}

	return false;
}

void SLobbyContainer::SetActiveScrollablyWidget(const ELobbyActionButton Index)
{
	if (ScrollablyWidgetsMap.Contains(Index))
	{
		const auto OldActiveIndex = ScrollablyContainer->GetActiveIndex();
		const auto NewActiveIndex = ScrollablyWidgetsMap.FindChecked(Index);
		if (ScrollablyContainer->SetActiveIndex(NewActiveIndex))
		{
			StopAnalyticAction(OldActiveIndex);
			FLokaAnalytics::Get()->MakeActionAndStart(this, FAnalyticAction(EAnalyticCategory::Main, NewActiveIndex));

			OnSwitchScrollablyWidget.Broadcast(Index, *ScrollablyWidgetsMap.FindKey(OldActiveIndex));
			FSlateApplication::Get().PlaySound(Style->SoundScollSlide);
		}
	}
}

void SLobbyContainer::ToggleWidget(const bool Toggle)
{
	Widget_FxTop->ToggleWidget(Toggle);
	Widget_FxBottom->ToggleWidget(Toggle);
	Widget_FxCenter->ToggleWidget(Toggle);
}

void SLobbyContainer::OnSetScrollablyWidget(ECheckBoxState State, const int32 Index) 
{
	const auto OldActiveIndex = ScrollablyContainer->GetActiveIndex();
	if (ScrollablyContainer->SetActiveIndex(Index))
	{
		StopAnalyticAction(OldActiveIndex);
		FLokaAnalytics::Get()->MakeActionAndStart(this, FAnalyticAction(EAnalyticCategory::Main, Index));

		Button_SettingsManager->SetIsChecked(ECheckBoxState::Unchecked);
		StopAnalyticAction(ELobbyMainAnalytic::Settings);

		OnSwitchScrollablyWidget.Broadcast(*ScrollablyWidgetsMap.FindKey(Index), *ScrollablyWidgetsMap.FindKey(OldActiveIndex));
		FSlateApplication::Get().PlaySound(Style->SoundScollSlide);
	}
}

ECheckBoxState SLobbyContainer::IsCheckedWidget(const int32 Index) const
{
	return (Index == ScrollablyContainer->GetActiveIndex()) ? ECheckBoxState::Checked : ECheckBoxState::Unchecked;
}

EVisibility SLobbyContainer::GetVisibilityByCheck_LobbyChat() const
{
	return Button_LobbyChat->IsChecked() ? EVisibility::SelfHitTestInvisible : EVisibility::Hidden;
}

EVisibility SLobbyContainer::GetVisibilityByCheck_FriendsForm() const
{
	return Button_FriendsForm->IsChecked() ? EVisibility::SelfHitTestInvisible : EVisibility::Hidden;
}

EVisibility SLobbyContainer::GetVisibilityByCheck_SettingsManager() const
{
	return Button_SettingsManager->IsChecked() ? EVisibility::SelfHitTestInvisible : EVisibility::Hidden;
}

void SLobbyContainer::OnSquadInformationUpdate(const FSqaudInformation& information)
{
	if (Widget_ContainerTop.IsValid())
	{
		Widget_ContainerTop->OnSquadInformationUpdate(information);
	}
}