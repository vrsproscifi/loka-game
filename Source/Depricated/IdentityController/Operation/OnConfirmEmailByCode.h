#pragma once
#include "ServiceHandler.h"
#include "ServiceOperation.h"
#include "RequestOneParam.h"

namespace Operation
{
	namespace OnConfirmEmailByCode
	{
		class Operation final :// public COperation<FRequestOneParam<FString>, ObjectSerializer::NativeObject, FRequestOneParam<bool>, ObjectSerializer::NativeObject>
		{
		public:
			IResponseHandler<void, OperationResponseType::None> OnBadEmailAddresFormat;


			Operation() 
				: Super(FServerHost::MasterClient, "Identity/OnConfirmEmailByCode")
				, OnBadEmailAddresFormat(400, this)

			{
				
			}

		};
	}
}