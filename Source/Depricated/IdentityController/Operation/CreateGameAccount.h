#pragma once
#include "ServiceOperation.h"
#include "RequestOneParam.h"

namespace Operation
{
	namespace CreateGameAccount
	{	
		class Operation final :// public COperation<FRequestOneParam<FString>, ObjectSerializer::NativeObject, FRequestOneParam<bool>, ObjectSerializer::NativeObject>
		{
		public:
			IResponseHandler<void, OperationResponseType::None> OnBadNameRequest;

			Operation() 
				: Super(FServerHost::MasterClient, "Identity/CreateGameAccount")
				, OnBadNameRequest(400, this)
			{

			}

		};

		static FText DialogTitle()
		{
			return NSLOCTEXT("ALobbyGameMode", "AuthAction.AuthRenameTitle", "Your name?");
		}

		static FText EnterName()
		{
			return NSLOCTEXT("ALobbyGameMode", "AuthAction.AuthRename", "Enter your name for game");
		}

		static FText SmallName()
		{
			return NSLOCTEXT("ALobbyGameMode", "AuthAction.AuthRename", "Enter your name for game");
		}

		static FText LargeName()
		{
			return NSLOCTEXT("ALobbyGameMode", "AuthAction.AuthRename", "Enter your name for game");
		}

		static FText InvalidName()
		{
			return NSLOCTEXT("ALobbyGameMode", "AuthAction.AuthRename", "Enter your name for game");
		}

		static FText ExistName()
		{
			return NSLOCTEXT("ALobbyGameMode", "AuthAction.AuthRenameNameToo", "Entetered name is already taken. Enter other name for game");
		}

	}
}