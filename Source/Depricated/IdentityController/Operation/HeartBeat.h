#pragma once
#include "ServiceHandler.h"
#include "ServiceOperation.h"
#include "RequestOneParam.h"


namespace Operation
{
	enum class EHeartBeatStatus
	{
		Offline,
		Online,
		InGame,
		End
	};

	namespace HeartBeat
	{
		class Operation final :// public COperation<void, ObjectSerializer::NativeObject, FRequestOneParam<int32>, ObjectSerializer::NativeObject>
		{
		public:
			FTimerHandle TimerHandle;
			FTimerDelegate TimerDelegate;

			void TimerStart(FTimerManager& timerManager)
			{
				timerManager.SetTimer(TimerHandle, TimerDelegate, 2.5f, true);
			}

			void TimerStop(FTimerManager& timerManager)
			{
				timerManager.ClearTimer(TimerHandle);
			}

			Operation() 
				: Super(FServerHost::MasterClient, "Identity/OnHeartBeat")
			{
				TimerDelegate.BindRaw(this, &Operation::Request);
			}
		};
	}
}