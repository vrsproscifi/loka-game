// VRSPRO

#include "LokaGame.h"
#include "SCharacterManager_Lock.h"
#include "SlateOptMacros.h"

#include "Character/CharacterBaseEntity.h"

#include "Components/SLokaToolTip.h"
#include "Components/SCharacteristicParameter.h"

#define LOCTEXT_NAMESPACE "SCharacterManager"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SCharacterManager_Lock::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;
	Character = InArgs._Character;

	ChildSlot
	[
		SNew(SBorder)
		.BorderImage(&Style->List.Background)
		.Padding(0)
		[
			SNew(SBorder)
			.BorderImage(&Style->List.Border)
			.Padding(2)
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot().AutoHeight()
				[
					SNew(STextBlock)
					.TextStyle(&Style->Lock.HeadText)
					.Justification(ETextJustify::Center)
					.Text(this, &SCharacterManager_Lock::GetCharacterName)
				]
				+ SVerticalBox::Slot().FillHeight(1).VAlign(VAlign_Center).HAlign(HAlign_Center)
				[
					SNew(SVerticalBox)
					+ SVerticalBox::Slot().AutoHeight().HAlign(HAlign_Fill)
					[
						SNew(SResourceTextBox)
						.Type(EResourceTextBoxType::Level)
						.Value(this, &SCharacterManager_Lock::GetCharacterLevel)
					]
					+ SVerticalBox::Slot()
					[
						SNew(SSpacer).Size(FVector2D(0, 8))
					]
					+ SVerticalBox::Slot().AutoHeight().HAlign(HAlign_Fill)
					[
						SNew(SResourceTextBox)
						.Type(this, &SCharacterManager_Lock::GetCharacterCostType)
						.Value(this, &SCharacterManager_Lock::GetCharacterCostValue)
					]
				]
				+ SVerticalBox::Slot().AutoHeight().Padding(FMargin(10, 8))
				[
					SNew(STextBlock)
					.TextStyle(&Style->Lock.DescText)
					.Justification(ETextJustify::Center)
					.Text(this, &SCharacterManager_Lock::GetCharacterDesc)
					.AutoWrapText(true)
				]
				+ SVerticalBox::Slot().AutoHeight().Padding(FMargin(10, 8))
				[
					SNew(SHorizontalBox)
					+ SHorizontalBox::Slot().Padding(FMargin(10))
					[
						SNew(SCharacteristicParameter)
						.Icon(&Style->Asset.IconHealth)
						.TextStyleName(&Style->Asset.ParameterName)
						.TextStyleValue(&Style->Asset.ParameterValue)
						.ToolTip(SNew(SLokaToolTip).Text(LOCTEXT("Health", "Health")))
						.Value_Lambda([&]() {
							return (Character.Get() != nullptr) ? Character.Get()->Property.Health.MaxAmount : 0;
						})
					]
					+ SHorizontalBox::Slot().Padding(FMargin(10))
					[
						SNew(SCharacteristicParameter)
						.Icon(&Style->Asset.IconArmour)
						.TextStyleName(&Style->Asset.ParameterName)
						.TextStyleValue(&Style->Asset.ParameterValue)
						.ToolTip(SNew(SLokaToolTip).Text(LOCTEXT("Armour", "Armour")))
						.Value_Lambda([&]() {
							return (Character.Get() != nullptr) ? Character.Get()->Property.Armour.MaxAmount : 0;
						})
					]
					+ SHorizontalBox::Slot().Padding(FMargin(10))
					[
						SNew(SCharacteristicParameter)
						.Icon(&Style->Asset.IconSpeed)
						.TextStyleName(&Style->Asset.ParameterName)
						.TextStyleValue(&Style->Asset.ParameterValue)
						.ToolTip(SNew(SLokaToolTip).Text(LOCTEXT("Speed", "Speed")))
						.Value_Lambda([&]() {
							return (Character.Get() != nullptr) ? Character.Get()->Property.Speed.MaxAmount : 0;
						})
					]
				]
				+ SVerticalBox::Slot().AutoHeight()
				[
					SNew(SButton)
					.ButtonStyle(&Style->Lock.Button)
					.TextStyle(&Style->Lock.ButtonText)
					.HAlign(HAlign_Center)
					.VAlign(VAlign_Center)
					.OnClicked(InArgs._OnClicked)
					.ContentPadding(FMargin(6))
					.Text(NSLOCTEXT("SCharacterManager", "Lock.Unlock", "UNLOCK"))
				]
			]
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

FText SCharacterManager_Lock::GetCharacterName() const
{
	if (Character.Get())
	{
		return Character.Get()->GetDescription().Name;
	}

	return FText::GetEmpty();
}

FText SCharacterManager_Lock::GetCharacterDesc() const
{
	if (Character.Get())
	{
		return Character.Get()->GetDescription().Description;
	}

	return FText::GetEmpty();
}

int32 SCharacterManager_Lock::GetCharacterLevel() const
{
	//TODO: SeNTIke
	//if (Character.Get())
	//{
	//	return Character.Get()->GetLevel();
	//}

	return 0;
}

EResourceTextBoxType::Type SCharacterManager_Lock::GetCharacterCostType() const
{
	//TODO: SeNTIke
	//if (Character.Get() && Character.Get()->GetCost().IsDonate)
	//{
	//	return EResourceTextBoxType::Donate;
	//}

	return EResourceTextBoxType::Money;
}

int32 SCharacterManager_Lock::GetCharacterCostValue() const
{
	//TODO: SeNTIke
	//if (Character.Get())
	//{
	//	return Character.Get()->GetCost().Amount;
	//}

	return 0;
}

#undef LOCTEXT_NAMESPACE