// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Styles/CharacterManagerWidgetStyle.h"

class UItemAmmoEntity;

class LOKAGAME_API SCharacterManager_Ammo : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SCharacterManager_Ammo)
		: _Style(&FLokaSlateStyle::Get().GetWidgetStyle<FCharacterManagerStyle>("SCharacterManagerStyle"))
	{}
		SLATE_STYLE_ARGUMENT(FCharacterManagerStyle, Style)
		SLATE_ATTRIBUTE(int32, Limit)
		SLATE_EVENT(FOnInt32ValueChanged, OnValueChanged)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	void SetInstance(UItemAmmoEntity*);
	UItemAmmoEntity* GetInstance() const;
	void SetDelegate(FOnInt32ValueChanged);
	void SetAmount(const int32&);

protected:

	const FCharacterManagerStyle* Style;

	TAttribute<int32> Limit;
	UItemAmmoEntity* Instance;
	FOnInt32ValueChanged OnValueChanged;

	TSharedPtr<SImage> Widget_Image;
	TSharedPtr<STextBlock> Widget_TextBlock;
	TSharedPtr<SHorizontalBox> Widget_HorizontalBox;
	TSharedPtr<SSpinBox<int32>> Widget_SpinBox;

	TOptional<int32> GetSpinBoxLimits(const bool) const;

	void OnAmountChanged(int32);
};
