// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Styles/CharacterManagerWidgetStyle.h"
#include "Item/CategoryTypeId.h"
/**
 * 
 */

class UItemBaseEntity;

DECLARE_DELEGATE_OneParam(FOnItemAction, const UItemBaseEntity*);

class LOKAGAME_API SCharacterManager_Item : public SButton
{
public:

	enum EItemState
	{
		Inventory,
		Dragged,
		InSlot,
		End
	};

	SLATE_BEGIN_ARGS(SCharacterManager_Item)
		: _Style(&FLokaSlateStyle::Get().GetWidgetStyle<FCharacterManagerStyle>("SCharacterManagerStyle"))
	{
		_Visibility = EVisibility::Visible;
	}
	SLATE_STYLE_ARGUMENT(FCharacterManagerStyle, Style)
	SLATE_ARGUMENT(TSharedPtr<SCharacterManager_Item>, Source)
	SLATE_ARGUMENT(EItemState, State)
	SLATE_EVENT(FOnItemAction, OnClickedModify)
	SLATE_EVENT(FOnItemAction, OnClickedSell)
	SLATE_EVENT(FOnItemAction, OnDoubleClicked)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs, UItemBaseEntity* InInstance);

	FORCEINLINE void SetImage(const FSlateBrush* _Image)
	{
		Image->SetImage(_Image);
	}

	FORCEINLINE void SetName(const FText& _Text)
	{
		TextName->SetText(_Text);
	}

	FORCEINLINE void SetLevel(const int32& _Level)
	{
		Level = _Level;
	}
	FORCEINLINE FText GetInventoryLevelText() const { return FText::AsNumber(Level + 1); }
	FORCEINLINE uint8 GetInventoryLevel() const { return Level; }
	uint8 GetInventorySlot() const;

	FORCEINLINE UItemBaseEntity* GetInstance() const { return Instance; }

	template<typename UItemTypeEntity>
	FORCEINLINE UItemTypeEntity* GetInstanceAs() const { return Cast<UItemTypeEntity>(Instance); }

protected:

	UItemBaseEntity* Instance = nullptr;

	TSharedPtr< FUICommandList > UICommandList;

	uint8 Level;
	EItemState State;

	FOnItemAction OnClickedModify;
	FOnItemAction OnClickedSell;
	FOnItemAction OnDoubleClicked;

	const FCharacterManagerStyle *Style;

	const FSlateBrush* ImageBrush;

	TSharedPtr<SImage> Image;
	TSharedPtr<SButton> Button;
	TSharedPtr<STextBlock> TextName, TextLevel;
	TSharedPtr<SBorder> BackgroundName, BackgroundLevel;
	
	FReply OnClickedButton();
	FReply OpenContextMenu();

	virtual FReply OnMouseButtonDown(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override;
	virtual FReply OnMouseButtonDoubleClick(const FGeometry& InMyGeometry, const FPointerEvent& InMouseEvent) override;
	virtual FReply OnDragDetected(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override;
	virtual void OnDragEnter(const FGeometry& MyGeometry, const FDragDropEvent& DragDropEvent) override;
	virtual void OnDragLeave(const FDragDropEvent& DragDropEvent) override;
	virtual FReply OnDrop(const FGeometry& MyGeometry, const FDragDropEvent& DragDropEvent) override;	

	bool CanItemModify() const;
	bool CanItemSell() const;

	void ItemModify();
	void ItemSell();
};