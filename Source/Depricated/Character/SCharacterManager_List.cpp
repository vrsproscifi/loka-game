// VRSPRO

#include "LokaGame.h"
#include "SCharacterManager_List.h"
#include "SlateOptMacros.h"
#include <Runtime/Slate/Public/SlateMaterialBrush.h>
#include <Weapon/ItemWeaponEntity.h>
#include <Armour/ItemArmourEntity.h>
#include <Grenade/ItemGrenadeEntity.h>
#include <Service/ItemServiceEntity.h>


BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SCharacterManager_List::Construct(const FArguments& InArgs)
{
	Style = InArgs._Style;

	OnItemUpgrade = InArgs._OnItemUpgrade;
	OnItemModify = InArgs._OnItemModify;
	OnItemSell = InArgs._OnItemSell;
	OnItemInstall = InArgs._OnItemInstall;
		
	for (SIZE_T i = 0; i <= 10; ++i)
		Sort_LevelArray.Add(MakeShareable(new int32(i)));

	Sort_CategoryArray.Add(MakeShareable(new int32(ECategoryTypeId::End)));
	Sort_CategoryArray.Add(MakeShareable(new int32(ECategoryTypeId::Weapon)));
	Sort_CategoryArray.Add(MakeShareable(new int32(ECategoryTypeId::Armour)));
	Sort_CategoryArray.Add(MakeShareable(new int32(ECategoryTypeId::Service)));

	SAssignNew(Sort_Level, SListView<TSharedPtr<int32>>)
		.SelectionMode(ESelectionMode::Single)
		.ListItemsSource(&Sort_LevelArray)
		.OnSelectionChanged(this, &SCharacterManager_List::OnStartSort_Level)
		.OnGenerateRow_Lambda([s = Style](TSharedPtr<int32> item, const TSharedRef<STableViewBase>& OwnerTable) 
		{ 
			return 
			SNew(STableRow<TSharedPtr<int32>>, OwnerTable)
			.Style(&s->List.SortRow)
			[
				SNew(STextBlock)
				.TextStyle(&s->List.SortText)
				.Text((*item == 0) ? FText::FromString("All") : FText::AsNumber(*item))
			]; 
	});	

	SAssignNew(Sort_Category, SListView<TSharedPtr<int32>>)
		.SelectionMode(ESelectionMode::Single)
		.ListItemsSource(&Sort_CategoryArray)
		.OnSelectionChanged(this, &SCharacterManager_List::OnStartSort_Category)
		.OnGenerateRow_Lambda([s = Style](TSharedPtr<int32> item, const TSharedRef<STableViewBase>& OwnerTable) 
		{ 
			return
			SNew(STableRow<TSharedPtr<int32>>, OwnerTable)
			.Style(&s->List.SortRow)
			[
				SNew(STextBlock)
				.TextStyle(&s->List.SortText)
				.Text((*item == ECategoryTypeId::End) ? FText::FromString("All") : FCategoryTypeId::GetText(static_cast<ECategoryTypeId>(*item)))
			];
	});	

	TSharedPtr<SVerticalBox> VerticalBox_Frac;

	ChildSlot
	[
		SNew(SOverlay)
		+ SOverlay::Slot()
		[
			SNew(SBorder)
			.BorderImage(&Style->List.Background)
			.Padding(0)
			[
				SNew(SBorder)
				.BorderImage(&Style->List.Border)
				.Padding(6)
				[
					SNew(SOverlay)
					+ SOverlay::Slot()
					[
						SNew(SVerticalBox)
						+ SVerticalBox::Slot()
						.AutoHeight()
						[
							SNew(SHorizontalBox)
							+ SHorizontalBox::Slot()
							[
								SAssignNew(SortButton_Category, SComboButton)
								.HasDownArrow(false)
								.Method(EPopupMethod::UseCurrentWindow)
								.ComboButtonStyle(&Style->List.SortButton)
								.ButtonContent()
								[
									SNew(STextBlock)
									.TextStyle(&Style->List.SortText)
									.Text(this, &SCharacterManager_List::GetTextSort_Category)
								]
								.MenuContent()
								[
									Sort_Category.ToSharedRef()
								]
							]
							+ SHorizontalBox::Slot()
							[
								SAssignNew(SortButton_Level, SComboButton)
								.HasDownArrow(false)
								.Method(EPopupMethod::UseCurrentWindow)
								.ComboButtonStyle(&Style->List.SortButton)
								.ButtonContent()
								[
									SNew(STextBlock)
									.TextStyle(&Style->List.SortText)
									.Text(this, &SCharacterManager_List::GetTextSort_Level)
								]
								.MenuContent()								
								[
									Sort_Level.ToSharedRef()
								]
							]
						]
						+ SVerticalBox::Slot()
						.FillHeight(1)
						[
							SNew(SScrollBox)
							.ScrollBarStyle(&Style->List.ScrollBar)
							.Style(&Style->List.ScrollBox)
							+ SScrollBox::Slot()
							.HAlign(HAlign_Fill)
							.VAlign(VAlign_Fill)
							[
								SAssignNew(ItemsContainer, SWrapBox)
								.InnerSlotPadding(FVector2D(1.0f, 1.0f))
								.UseAllottedWidth(true)
								.PreferredWidth(1.0f)
							]
						]
					]
					+ SOverlay::Slot()
					.HAlign(HAlign_Left)
					.VAlign(VAlign_Top)
					[
						SNew(SBox)
						.WidthOverride(64)
						.HeightOverride(320)
						.RenderTransform(FVector2D(-70, 40))
						[
							SAssignNew(VerticalBox_Frac, SVerticalBox)
						]
					]
				]
			]
		]
		+ SOverlay::Slot().HAlign(HAlign_Center).VAlign(VAlign_Top)
		[
			SNew(STextBlock)
			.RenderTransformPivot(FVector2D(.5f, 1.f))
			.RenderTransform(FSlateRenderTransform(FVector2D(0, -60)))
			.TextStyle(&Style->Headers)
			.Text(NSLOCTEXT("SCharacterManager", "Headers.Inventory", "Inventory"))
		]
	];

	//for (int32 i = 0; i < 5; ++i)
	//{
	//	VerticalBox_Frac->AddSlot()
	//	[
	//		SNew(SCheckBox)
	//		.Type(ESlateCheckBoxType::ToggleButton)
	//		.Style(&Style->Assets.Button)			
	//		.OnCheckStateChanged(this, &SCharacterManager_List::FracSort, i)
	//		.IsChecked(this, &SCharacterManager_List::GetIsSelectedFracSort, i)
	//		[
	//			SNew(SBorder)
	//			.BorderImage(new FSlateColorBrush(FColor::Transparent))
	//			.Visibility(EVisibility::HitTestInvisible)
	//			.HAlign(HAlign_Center)
	//			.VAlign(VAlign_Center)
	//			[
	//				SNew(STextBlock)
	//				.Text((i == 0) ? FText::FromString("All") : FText::AsNumber(i))
	//				.TextStyle(&Style->Assets.Number)
	//				.Justification(ETextJustify::Center)
	//			]
	//		]
	//	];
	//}

	Sort_Level->SetItemSelection(Sort_LevelArray[0], true);
	Sort_Category->SetItemSelection(Sort_CategoryArray[0], true);
	SelectedFracSort = 0;
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SCharacterManager_List::FracSort(ECheckBoxState, const int32 Sort)
{
	SelectedFracSort = Sort;
}

ECheckBoxState SCharacterManager_List::GetIsSelectedFracSort(const int32 i) const
{
	return SelectedFracSort == i ? ECheckBoxState::Checked : ECheckBoxState::Unchecked;
}

void SCharacterManager_List::OnStartSort_Level(TSharedPtr<int32> Item, ESelectInfo::Type Info)
{
	SortButton_Level->SetIsOpen(false);

	if (Sort_Level->GetSelectedItems().Num() && Sort_Category->GetSelectedItems().Num())
	{
		StartSort(*Sort_Category->GetSelectedItems()[0], *Sort_Level->GetSelectedItems()[0]);
	}	
}

void SCharacterManager_List::OnStartSort_Category(TSharedPtr<int32> Item, ESelectInfo::Type Info)
{
	SortButton_Category->SetIsOpen(false);

	if (Sort_Level->GetSelectedItems().Num() && Sort_Category->GetSelectedItems().Num())
	{
		StartSort(*Sort_Category->GetSelectedItems()[0], *Sort_Level->GetSelectedItems()[0]);
	}
}

FText SCharacterManager_List::GetTextSort_Level() const 
{ 
	auto Items = Sort_Level->GetSelectedItems();
	if (Items.Num())
	{
		return (*Items[0] == 0) ? NSLOCTEXT("All", "All.All", "All") : FText::AsNumber(*Items[0]);
	}

	return NSLOCTEXT("All", "All.All", "All");
}

FText SCharacterManager_List::GetTextSort_Category() const 
{ 
	auto Items = Sort_Category->GetSelectedItems();
	if (Items.Num())
	{
		return (*Items[0] == ECategoryTypeId::End) ? NSLOCTEXT("All", "All.All", "All") : FCategoryTypeId::GetText(static_cast<ECategoryTypeId>(*Items[0]));
	}

	return NSLOCTEXT("All", "All.All", "All");
}

void SCharacterManager_List::StartSort(const int32& Category, const int32& Level)
{
	for (auto i : ItemWidgets)
	{
		i.Value->SetVisibility(EVisibility::Visible);

		//TODO:SeNTike
		//if ((i.Key->GetDisplayLevel() != Level && Level != 0) || (i.Key->GetDescription().CategoryType != Category && Category != ECategoryTypeId::End))
		//{
		//	i.Value->SetVisibility(EVisibility::Collapsed);
		//}
	}
}

void SCharacterManager_List::FillList(UItemBaseEntity* entity)
{
	auto itemWidget = SNew(SCharacterManager_Item, entity)
		.OnClickedModify(OnItemModify)
		.OnClickedSell(OnItemSell)
		.OnDoubleClicked(OnItemInstall);

	if (ItemWidgets.Add(entity, itemWidget).IsValid())
	{
		ItemsContainer->AddSlot()[itemWidget];
	}
}

void SCharacterManager_List::FillList(const FInstanceRepository& entityList)
{
	//TODO:SeNTike
	//for (auto item : entityList.Weapon)
	//{
	//	if(item && item->IsLocked() == false)
	//		FillList(item);
	//}
	//
	//for (auto item : entityList.Armour)
	//{
	//	if (item && item->IsLocked() == false)
	//		FillList(item);
	//}
	//
	//for (auto item : entityList.Grenade)
	//{
	//	if (item && item->IsLocked() == false)
	//		FillList(item);
	//}
	//
	//for (auto item : entityList.Service)
	//{
	//	if (item && item->IsLocked() && item->GetCount() > 0)
	//		FillList(item);
	//}
}

bool SCharacterManager_List::RemoveItem(UItemBaseEntity* entity)
{
	if (ItemWidgets.Contains(entity))
	{
		ItemsContainer->RemoveSlot(ItemWidgets[entity].ToSharedRef());
		ItemWidgets.Remove(entity);
		return true;
	}

	return false;
}

void SCharacterManager_List::ClearList()
{
	ItemsContainer->ClearChildren();
	ItemWidgets.Empty();
}