// VRSPRO

#pragma once

#include "SCharacterManager_Sets.h"
#include "SCharacterManager_List.h"
#include "SCharacterManager_Modify.h"
#include "Components/SSliderSpin.h"
#include "StoreController/Operation/UnlockItem.h"

namespace CharacterModelId{
	enum Type;
}

class UCharacterBaseEntity;
class UPlayerCharacterInstance;


DECLARE_DELEGATE_OneParam(FOnSwitchCharacter, const UCharacterBaseEntity*);
DECLARE_DELEGATE_OneParam(FOnCharacterSelected, const CharacterModelId::Type&);


class LOKAGAME_API SCharacterManager : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SCharacterManager)
		: _Style(&FLokaSlateStyle::Get().GetWidgetStyle<FCharacterManagerStyle>("SCharacterManagerStyle"))
		, _OtherManager(SNullWidget::NullWidget)
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_EVENT(FOnSwitchCharacter, OnSwitchCharacter)
	SLATE_EVENT(FOnSwitchCharacter, OnConfirmCharacterSelect)
	SLATE_EVENT(FOnClickedOutside, OnCancelCharacterSelect)
	SLATE_EVENT(FOnChangeSet, OnChangeAsset)
	SLATE_EVENT(FOnChangeSetName, OnChangeSetName)
	SLATE_EVENT(FOnItemAction, OnItemModify)
	SLATE_EVENT(FOnItemAction, OnItemSell)
	SLATE_EVENT(FOnItemActionSetSlot, OnItemEquip)
	SLATE_EVENT(FOnItemActionSetSlot, OnItemUnEquip)
	SLATE_EVENT(FOnAmmoAmountChange, OnAmmoAmountChange)
	SLATE_EVENT(Operation::FOnUnlockItem, OnUnlockCharacter)
	SLATE_EVENT(FOnClicked, OnClickedBack)
	SLATE_EVENT(FOnItemAction, OnItemInstall)
	SLATE_STYLE_ARGUMENT(FCharacterManagerStyle, Style)
	SLATE_ARGUMENT(TSharedRef<SWidget>, OtherManager)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	void SetPlayerControllerRef(class ALobbyPlayerController *);

	// * true - modify mode, false - normal mode
	void SwitchMode(const bool /*Mode*/);

	bool AddCharacter(const UCharacterBaseEntity*);
	bool ReplaceCharacter(const UCharacterBaseEntity* InFrom, const UCharacterBaseEntity* InTo);
	bool SetCharacter(const UCharacterBaseEntity*);
	bool SetCharacterByModel(const CharacterModelId::Type);

	FORCEINLINE TSharedRef<SCharacterManager_List> GetInventoryManager() const { return InventoryManager.ToSharedRef(); }
	FORCEINLINE TSharedRef<SCharacterManager_Sets> GetSetManager() const { return SetManager.ToSharedRef(); }
	FORCEINLINE TSharedRef<SCharacterManager_Modify> GetModifyManager() const { return ModifyManager.ToSharedRef(); }
	Operation::FOnUnlockItem OnUnlockCharacter;

	TAttribute<bool> IsEnableInventory;

protected:	

	const FCharacterManagerStyle* Style;

	FOnChangeSet OnChangeAsset;
	FOnSwitchCharacter OnConfirmCharacterSelect;
	FOnClickedOutside OnCancelCharacterSelect;
	FOnSwitchCharacter OnSwitchCharacter;

	class ALobbyPlayerController *ControllerRef;

	TSharedPtr<class SCharacterManager_Lock> LockWindow;
	TSharedPtr<SCharacterManager_List> InventoryManager;
	TSharedPtr<SCharacterManager_Sets> SetManager;
	TSharedPtr<SCharacterManager_Modify> ModifyManager;

	TSharedPtr<class SAnimatedBackground> AnimatedBackgrounds[7];

	TSharedPtr<class SSliderSpin> CharactersSlider;

	void OnChangeSetEvent(const EPlayerPreSetId::Type& setId);

	void OnCharacterList(const int32&);
	FReply OnClickedSelectCharacter() const;
	TMap<int32, const UCharacterBaseEntity*> CharacterList;
	const UCharacterBaseEntity* CurrentCharacter;
};
