// VRSPRO

#include "LokaGame.h"
#include "SCharacterManager_Item.h"
#include "SlateOptMacros.h"

#include "Layout/SScaleBox.h"
#include "Components/STextBlockButton.h"

#include "LokaDragDrop.h"
#include "WidgetLayoutLibrary.h"

#include "Item/ItemBaseEntity.h"

class SInventoryItemCommands : public TCommands<SInventoryItemCommands>
{
public:

	SInventoryItemCommands() : TCommands<SInventoryItemCommands>
		(
			"SInventoryItemCommands", // Context name for fast lookup
			NSLOCTEXT("SInventoryItemCommands", "SInventoryItemCommands", "SInventoryItemCommands"), // Localized context name for displaying
			NAME_None, FCoreStyle::Get().GetStyleSetName()
		)
	{}

	TSharedPtr<FUICommandInfo> ItemModify;
	TSharedPtr<FUICommandInfo> ItemSell;

	virtual void RegisterCommands() override
	{
		FUICommandInfo::MakeCommandInfo(
			this->AsShared(),
			ItemModify,
			TEXT("ItemModify"),
			NSLOCTEXT("SCharacterManager", "Item.Modify", "Modify"),
			NSLOCTEXT("SCharacterManager", "Item.Modify.Desc", "Modify this item to install or uninstall some module (sniper scope, collimator scope, laser point, etc.)."),
			FSlateIcon(),
			EUserInterfaceActionType::Button,
			FInputChord()
		);

		FUICommandInfo::MakeCommandInfo(
			this->AsShared(),
			ItemSell,
			TEXT("ItemSell"),
			NSLOCTEXT("SCharacterManager", "Item.Sell", "Sell"),
			NSLOCTEXT("SCharacterManager", "Item.Sell.Desc", "Sell this item to revert some cost of buy."),
			FSlateIcon(),
			EUserInterfaceActionType::Button,
			FInputChord()
		);
	}
};


BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SCharacterManager_Item::Construct(const FArguments& InArgs, UItemBaseEntity* InInstance)
{
	Instance = InInstance;

	Style = InArgs._Style;
	State = InArgs._State;

	OnClickedModify = InArgs._OnClickedModify;
	OnClickedSell = InArgs._OnClickedSell;
	OnDoubleClicked = InArgs._OnDoubleClicked;

	if (InArgs._Source.IsValid())
	{
		auto Source = InArgs._Source.ToSharedRef();
		Instance = Source->Instance;

		OnClickedModify = Source->OnClickedModify;
		OnClickedSell = Source->OnClickedSell;
	}

	ImageBrush = Instance->GetImage();
	FText Name = Instance->GetDescription().Name;
	//TODO: SeNTike
//	Level = Instance->GetLevel();

	MAKE_UTF8_SYMBOL(sUpgrade, 0xf013)

	SButton::Construct(
		SButton::FArguments()
		.OnClicked(this, &SCharacterManager_Item::OnClickedButton)
		.ButtonStyle(&Style->Item.Button)
		.ContentPadding(FMargin(0))
		[
			SNew(SBox)
			.HeightOverride(128)
			.WidthOverride(256)
			[
				SNew(SOverlay)
				+ SOverlay::Slot()
				[
					SNew(SScaleBox)
					.VAlign(VAlign_Center)
					.HAlign(HAlign_Center)
					.Stretch(EStretch::ScaleToFit)
					[
						SAssignNew(Image, SImage)
						.Image(ImageBrush)
					]
				]
				+ SOverlay::Slot()
				.HAlign(HAlign_Left)
				.VAlign(VAlign_Top)
				.Padding(0)
				[
					SAssignNew(BackgroundName, SBorder)
					.BorderImage(&Style->Item.TextBackground)
					.Padding(FMargin(8, 2))
					[
						SAssignNew(TextName, STextBlock)
						.Text(Name)
						.TextStyle(&Style->Item.Name)
					]
				]
				+ SOverlay::Slot()
				.HAlign(HAlign_Right)
				.VAlign(VAlign_Bottom)
				.Padding(0)
				[
					SAssignNew(BackgroundLevel, SBorder)
					.BorderImage(&Style->Item.TextBackground)					
					.Padding(FMargin(8, 2))
					[
						SAssignNew(TextLevel, STextBlock)
						.TextStyle(&Style->Item.Level)
						.Text(this, &SCharacterManager_Item::GetInventoryLevelText)
					]
				]
				+ SOverlay::Slot()
				.HAlign(HAlign_Left)
				.VAlign(VAlign_Bottom)
				.Padding(0)
				[
					SNew(STextBlockButton)
					.Visibility_Lambda([&]() { return CanItemModify() ? EVisibility::Visible : EVisibility::Hidden; })
					.Style(&Style->Item.Upgrade)
					.Text(FText::FromString(sUpgrade))
					.OnClicked(this, &SCharacterManager_Item::OpenContextMenu)
				]
			]
		]
	);

	if (State == EItemState::InSlot)
	{
		SButton::SetBorderImage(new FSlateColorBrush(FColor::Transparent));
		//BackgroundName->SetBorderBackgroundColor(FColor::Transparent);
		//BackgroundLevel->SetBorderBackgroundColor(FColor::Transparent);
	}

	if (State != EItemState::Dragged)
	{
		//SInventoryItemCommands::Register();

		//UICommandList = MakeShareable(new FUICommandList());

		//UICommandList->MapAction(SInventoryItemCommands::Get().ItemModify,
		//	FExecuteAction::CreateSP(this, &SCharacterManager_Item::ItemModify),
		//	FCanExecuteAction::CreateSP(this, &SCharacterManager_Item::CanItemModify));

		//UICommandList->MapAction(SInventoryItemCommands::Get().ItemSell,
		//	FExecuteAction::CreateSP(this, &SCharacterManager_Item::ItemSell),
		//	FCanExecuteAction::CreateSP(this, &SCharacterManager_Item::CanItemSell));
	}
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

FReply SCharacterManager_Item::OnMouseButtonDown(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent)
{
	if (MouseEvent.GetEffectingButton() == EKeys::LeftMouseButton)
	{
		return FReply::Handled().DetectDrag(AsShared(), EKeys::LeftMouseButton);
	}
//	else if (MouseEvent.GetEffectingButton() == EKeys::RightMouseButton)
//	{
//		FMenuBuilder MenuBuilder(true, UICommandList);
//		{
//			MenuBuilder.BeginSection("SCharacterManager.ItemMenu", NSLOCTEXT("SCharacterManager", "Item.Menu", "Select action"));
//			{
//				MenuBuilder.AddMenuEntry(SInventoryItemCommands::Get().ItemModify);
//				MenuBuilder.AddMenuEntry(SInventoryItemCommands::Get().ItemSell);
//			}
//			MenuBuilder.EndSection();
//		}
//
////		MenuBuilder.SetStyle(&FLokaSlateStyle::Get(), "Menu");
//
//		FWidgetPath WidgetPath = MouseEvent.GetEventPath() != nullptr ? *MouseEvent.GetEventPath() : FWidgetPath();
//		FSlateApplication::Get().PushMenu(AsShared(), WidgetPath, MenuBuilder.MakeWidget(), MouseEvent.GetScreenSpacePosition(), FPopupTransitionEffect(FPopupTransitionEffect::ContextMenu), true, FVector2D::ZeroVector, EPopupMethod::UseCurrentWindow);
//
//		return OpenContextMenu();
//	}
	else
	{
		return FReply::Unhandled();
	}
}

FReply SCharacterManager_Item::OnMouseButtonDoubleClick(const FGeometry& InMyGeometry, const FPointerEvent& InMouseEvent)
{
	if (InMouseEvent.GetEffectingButton() == EKeys::LeftMouseButton)
	{
		OnDoubleClicked.ExecuteIfBound(Instance);
		return FReply::Handled();
	}

	return SButton::OnMouseButtonDoubleClick(InMyGeometry, InMouseEvent);
}

FReply SCharacterManager_Item::OpenContextMenu()
{
	//FMenuBuilder MenuBuilder(true, UICommandList);
	//{
	//	MenuBuilder.BeginSection("SCharacterManager.ItemMenu", NSLOCTEXT("SCharacterManager", "Item.Menu", "Select action"));
	//	{
	//		MenuBuilder.AddMenuEntry(SInventoryItemCommands::Get().ItemModify);
	//		MenuBuilder.AddMenuEntry(SInventoryItemCommands::Get().ItemSell);
	//	}
	//	MenuBuilder.EndSection();
	//}

	////MenuBuilder.SetStyle(&FLokaSlateStyle::Get(), "Menu");
	//
	//FSlateApplication::Get().PushMenu(AsShared(), FWidgetPath(), MenuBuilder.MakeWidget(), FSlateApplication::Get().GetCursorPos(), FPopupTransitionEffect(FPopupTransitionEffect::ContextMenu), true, FVector2D::ZeroVector, EPopupMethod::UseCurrentWindow);

	ItemModify();
	return FReply::Handled();
}

FReply SCharacterManager_Item::OnClickedButton()
{
	return FReply::Handled().DetectDrag(AsShared(), EKeys::LeftMouseButton);
}

FReply SCharacterManager_Item::OnDragDetected(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent)
{
	if (MouseEvent.IsMouseButtonDown(EKeys::LeftMouseButton))
	{
		TSharedRef<FLokaDragDropOp> Operation = MakeShareable(new FLokaDragDropOp(AsShared(), AsShared()));

		Operation->SetDecoratorVisibility(true);
		Operation->Construct();
		return FReply::Handled().BeginDragDrop(Operation);
	}
	else
	{
		return FReply::Unhandled();
	}
}

void SCharacterManager_Item::OnDragEnter(const FGeometry& MyGeometry, const FDragDropEvent& DragDropEvent)
{
	SCompoundWidget::OnDragEnter(MyGeometry, DragDropEvent);
}

void SCharacterManager_Item::OnDragLeave(const FDragDropEvent& DragDropEvent)
{
	SCompoundWidget::OnDragLeave(DragDropEvent);
	Release();
}

FReply SCharacterManager_Item::OnDrop(const FGeometry& MyGeometry, const FDragDropEvent& DragDropEvent)
{
	return FReply::Unhandled();
}

bool SCharacterManager_Item::CanItemModify() const
{
	if (Instance && Instance->GetDescription().CategoryType == ECategoryTypeId::Weapon)
	{
		return true;
	}

	return false;
}

bool SCharacterManager_Item::CanItemSell() const
{
	return true;
}

void SCharacterManager_Item::ItemModify()
{
	OnClickedModify.ExecuteIfBound(Instance);
}

void SCharacterManager_Item::ItemSell()
{
	OnClickedSell.ExecuteIfBound(Instance);
}

uint8 SCharacterManager_Item::GetInventorySlot() const
{
	if (Instance)
	{
		return Instance->GetDescription().SlotType;
	}

	return EItemSlotTypeId::End;
}