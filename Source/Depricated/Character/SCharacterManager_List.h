// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Styles/CharacterManagerWidgetStyle.h"
#include "SCharacterManager_Item.h"

struct FInstanceRepository;
class UItemWeaponEntity;
class UItemArmourEntity;
class UItemBaseEntity;

class LOKAGAME_API SCharacterManager_List : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SCharacterManager_List)
		: _Style(&FLokaSlateStyle::Get().GetWidgetStyle<FCharacterManagerStyle>("SCharacterManagerStyle"))
	{
		_Visibility = EVisibility::SelfHitTestInvisible;
	}
	SLATE_EVENT(FOnItemAction, OnItemUpgrade)
	SLATE_EVENT(FOnItemAction, OnItemModify)
	SLATE_EVENT(FOnItemAction, OnItemSell)
	SLATE_EVENT(FOnItemAction, OnItemInstall)
	SLATE_STYLE_ARGUMENT(FCharacterManagerStyle, Style)
	SLATE_END_ARGS()

	/** Constructs this widget with InArgs */
	void Construct(const FArguments& InArgs);

	void FillList(const FInstanceRepository &entityList);
	void FillList(UItemBaseEntity* entity);
	bool RemoveItem(UItemBaseEntity* entity);
	void ClearList();

	void StartSort(const int32& Category, const int32& Level);

protected:

	void FracSort(ECheckBoxState, const int32);
	ECheckBoxState GetIsSelectedFracSort(const int32) const;
	int32 SelectedFracSort;

	FOnItemAction OnItemUpgrade;
	FOnItemAction OnItemModify;
	FOnItemAction OnItemSell;
	FOnItemAction OnItemInstall;

	const FCharacterManagerStyle *Style;

	TSharedPtr<SWrapBox> ItemsContainer;
	TSharedPtr<class SCharacterManager_Item> ItemSelected;

	TSharedPtr<SComboButton> SortButton_Level;
	TArray<TSharedPtr<int32>> Sort_LevelArray;
	TSharedPtr<SListView<TSharedPtr<int32>>> Sort_Level;
	void OnStartSort_Level(TSharedPtr<int32>, ESelectInfo::Type);
	FText GetTextSort_Level() const;
	
	TSharedPtr<SComboButton> SortButton_Category;
	TArray<TSharedPtr<int32>> Sort_CategoryArray;
	TSharedPtr<SListView<TSharedPtr<int32>>> Sort_Category;
	void OnStartSort_Category(TSharedPtr<int32>, ESelectInfo::Type);
	FText GetTextSort_Category() const;

	//TArray<TSharedPtr<class SCharacterManager_Item>> ItemWidgets;
	TMap<UItemBaseEntity*, TSharedPtr<class SCharacterManager_Item>> ItemWidgets;
};
