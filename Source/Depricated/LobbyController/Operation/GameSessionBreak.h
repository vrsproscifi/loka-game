#pragma once
#include "ServiceHandler.h"
#include "ServiceOperation.h"

namespace Operation
{
	namespace GameSessionBreak
	{
		class Operation final 
			:// public COperation<void, ObjectSerializer::JsonObject, void, ObjectSerializer::JsonObject>
		{
		public:			
			Operation() 
				: Super(FServerHost::MasterClient, "Session/GameSessionBreak", FVerb::Get)
			{
			}

		};
	}
}
