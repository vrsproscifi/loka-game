#pragma once
#include "ServiceOperation.h"

namespace Operation
{
	namespace SearchSessionCancel
	{
		class Operation final :// public COperation<void, ObjectSerializer::JsonObject, void, ObjectSerializer::NativeObject>
		{
		public:
			Operation() : Super(FServerHost::MasterClient, "Session/SearchCancel") { }

		};
	}
}
