// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "GameMode_Build.h"
#include "GameState_Build.h"

#include "BuildSystem/BuildPlacedComponent.h"
#include "Build/ItemBuildEntity.h"

#include "EntityRepository.h"
#include "ShooterGameInstance.h"
#include "BuildPlayerController.h"

#include "SMessageBox.h"
#include "RichTextHelpers.h"
#include "TutorialBase.h"
#include "Build/SBuildSystem_ToolBar.h"

#define CALL_CTR_FNC(Fnc, ...) \
for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; ++It)\
{\
	if (auto TargetCtrl = Cast<ABuildPlayerController>(*It))\
	{\
		TargetCtrl->##Fnc(__VA_ARGS__);\
	}\
}

AGameMode_Build::AGameMode_Build(const class FObjectInitializer& PCIP)
	: Super(PCIP)
{
	GameStateClass = AGameState_Build::StaticClass();

	if (!HasAnyFlags(RF_ClassDefaultObject | RF_ArchetypeObject))
	{
		buildingController.GetListOfBuildings.OnSuccess.AddUObject(this, &AGameMode_Build::OnBuildingsList);
		buildingController.GetListOfBuildings.OnFailed.AddLambda([&, this] {
			if (buildingController.GetListOfBuildings.Attemp++ < 5)
			{
				buildingController.GetListOfBuildings.Request(OwnerTokenId);
			}
			else
			{
				UE_LOG(UT, Error, TEXT("Failed GetListOfBuildings"));
			}
		});

		buildingController.PlaceObject.OnSuccess.AddUObject(this, &AGameMode_Build::OnBuildPlaced);
		
		buildingController.PlaceObject.OnNotFoundInstance.Handler.AddLambda([&]() {
			CALL_CTR_FNC(ClientPlaceObjectOnNotFoundInstance);
		});

		buildingController.PlaceObject.OnNotEnouthItem.Handler.AddLambda([&]() {
			CALL_CTR_FNC(ClientPlaceObjectOnNotEnouthItem);
		});

		buildingController.PlaceObject.OnFailed.AddLambda([&]() {
			CALL_CTR_FNC(ClientPlaceObjectOnFailed);
		});

		buildingController.RemoveObject.OnSuccess.AddUObject(this, &AGameMode_Build::OnBuildRemoved);
		buildingController.RemoveObject.OnFailed.AddLambda([&]() {
			CALL_CTR_FNC(ClientRemoveObjectOnFailed);
		});

		buildingController.OnRepairObject.OnSuccess.AddUObject(this, &AGameMode_Build::OnBuildingsRepairList);
		identityController.RequestClientData.OnSuccess.AddUObject(this, &AGameMode_Build::OnRequestClientData);
		identityController.CheckAccountStatus.OnSuccess.AddUObject(this, &AGameMode_Build::OnCheckAccountStatusResponse);
		identityController.RequestLootData.OnSuccess.AddUObject(this, &AGameMode_Build::OnLootDataList);
		identityController.OnStartTutorial.OnSuccess.AddUObject(this, &AGameMode_Build::OnStartTutorial);
		identityController.OnStartTutorial.OnStepInProgress.Handler.AddUObject(this, &AGameMode_Build::OnStartTutorial);

		identityController.OnStartTutorial.OnFailed.AddLambda([&]() {
			CALL_CTR_FNC(ClientOnStartTutorialOnFailed);
		});

		identityController.OnStartTutorial.OnNotFoundInstance.Handler.AddLambda([&]() {
			CALL_CTR_FNC(ClientOnStartTutorialOnNotFoundInstance);
		});

		identityController.OnStartTutorial.OnNotFoundSteps.Handler.AddLambda([&]() {
			CALL_CTR_FNC(ClientOnStartTutorialOnNotFoundSteps);
		});

		storeController.UnlockItem.OnSuccess.AddUObject(this, &AGameMode_Build::OnBuildPurchased);
		storeController.UnlockItem.OnPaymentRequired.Handler.AddUObject(this, &AGameMode_Build::OnBuildPurchaseNoMoney);
#if WITH_EDITOR
		if (auto GameInst = Cast<UShooterGameInstance>(GetGameInstance()))
		{
			storeController.ListOfInstance.OnSuccess.AddUObject(GameInst->EntityRepository, &UEntityRepository::Initialize);
			storeController.ListOfInstance.OnSuccess.AddUObject(GameInst, &UShooterGameInstance::InitTutorialsData);
			storeController.ListOfInstance.OnSuccess.AddLambda([&] (const FListOfInstance&){
				identityController.RequestLootData.Request(OwnerTokenId);
			});
		}
#endif
	}

	NonePlayersTime = 0;
}

void AGameMode_Build::OnRequestClientData(const FPlayerEntityInfo& response) const
{
	//FPlayerEntityInfo::Instance = response;
	identityController.RequestLootData.Request(OwnerTokenId);
}



void AGameMode_Build::InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage)
{
	Super::InitGame(MapName, Options, ErrorMessage);

#if UE_SERVER
	if (serverController == nullptr)
	{
		UE_LOG(LogInit, Fatal, TEXT("[AGameMode_Build::InitGame] | serverController was nullptr"));
	}

	if (serverController->CheckAvailableMatch.CurrentSessionMatch == nullptr)
	{
		UE_LOG(LogInit, Fatal, TEXT("[AGameMode_Build::InitGame] | CurrentSessionMatch was nullptr"));
	}

	OwnerTokenId = serverController->CheckAvailableMatch.CurrentSessionMatch->UniversalId;
	if(OwnerTokenId.IsEmpty())
	{
		UE_LOG(LogInit, Fatal, TEXT("[AGameMode_Build::InitGame] | OwnerTokenId was nullptr"));
	}

	UE_LOG(LogInit, Warning, TEXT("AGameMode_Build::InitGame | OwnerTokenId: %s"), *OwnerTokenId);
#endif

}

void AGameMode_Build::InitGameState()
{
	Super::InitGameState();

	BuildGameState = Cast<AGameState_Build>(GameState);
}

void AGameMode_Build::SendStartMatchRequest()
{
#if UE_SERVER && !WITH_EDITOR
	serverController->GetBuildingServerOwnerId.OnSuccess.AddLambda([&](const FRequestOneParam<FString>& InOwnerId) {
		OwnerId = InOwnerId;
		Super::SendStartMatchRequest();
	});

	serverController->GetBuildingServerOwnerId.Request();
#endif
}

void AGameMode_Build::PostLogin(APlayerController* NewPlayer)
{
	auto TargetPC = Cast<ABuildPlayerController>(NewPlayer);

	if (BuildGameState && TargetPC)
	{
#if UE_SERVER && !WITH_EDITOR
		if (auto LocalPlayer = Cast<UNetConnection>(NewPlayer->Player))
		{
			const auto TargetMember = serverController->CheckAvailableMatch.CurrentSessionMatch->Members.FindByPredicate([&](const FNodeSessionMember& A) {
				return (A.PlayerId == LocalPlayer->PlayerId->ToString());
			});

			if (TargetMember && TargetPC->PlayerState)
			{
				TargetPC->PlayerState->PlayerName = TargetMember->PlayerName;
				const bool IsOwner = OwnerId == TargetMember->PlayerId;
				if (IsOwner)
				{
					BuildGameState->OwnerState = TargetPC->PlayerState;
				}

				UE_LOG(LogGameMode, Warning, TEXT("AGameMode_Build::PostLogin >> PlayerId: %s, PlayerName: %s, IsOwner: %s"), *TargetMember->PlayerId, *TargetMember->PlayerName, IsOwner ? TEXT("True") : TEXT("False"));
				Super::PostLogin(NewPlayer);
			}
		}
#else
		if (TargetPC && TargetPC->PlayerState)
		{
			BuildGameState->OwnerState = TargetPC->PlayerState;
		}

		Super::PostLogin(NewPlayer);
#endif
	}
}

void AGameMode_Build::Logout(AController* Exiting)
{
	Super::Logout(Exiting);
	ValidatePlayerLogoutTimeout();
}

void AGameMode_Build::RequestPlayerExiting(APlayerController* InRequestFrom)
{
	if (BuildGameState && InRequestFrom && InRequestFrom->PlayerState)
	{
		const auto IsOwner = (BuildGameState->OwnerState == InRequestFrom->PlayerState);
		const auto gm = FGameModeTypeId::GetString(EGameModeTypeId::Lobby);
		const auto mp = FGameMapTypeId::GetString(EGameMapTypeId::Lobby);

		UE_LOG(LogGameMode, Warning, TEXT("AGameMode_Build::RequestPlayerExiting >> PlayerName: %s, IsOwner: %s"), *InRequestFrom->PlayerState->PlayerName, IsOwner ? TEXT("True") : TEXT("False"));

		if (IsOwner)
		{
			EndMatch();

			for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
			{
				(*It)->ClientTravel(*FString::Printf(TEXT("%s?game=%s"), *mp, *gm), TRAVEL_Absolute);
			}			
		}
		else
		{
			InRequestFrom->ClientTravel(*FString::Printf(TEXT("%s?game=%s"), *mp, *gm), TRAVEL_Absolute);
		}
	}
}

void AGameMode_Build::ValidatePlayerLogoutTimeout()
{
	if (GetNetMode() == NM_DedicatedServer)
	{
		if (BuildGameState && BuildGameState->IsValidLowLevel())
		{
			if (BuildGameState->PlayerArray.Num())
			{
				GetWorld()->GetTimerManager().ClearTimer(PlayerLogoutTimeoutHandler);
			}
			else if (PlayerLogoutTimeoutHandler.IsValid() == false)
			{
				GetWorld()->GetTimerManager().SetTimer(PlayerLogoutTimeoutHandler, this, &AGameMode_Build::OnPlayerLogoutTimeout, 120.0f, true, 20.0f);
			}
		}
	}
}

void AGameMode_Build::OnPlayerLogoutTimeout()
{

	if (BuildGameState && BuildGameState->IsValidLowLevel())
	{
		UE_LOG(LogInit, Warning, TEXT("AGameMode_Build::OnPlayerLogoutTimeout | PlayerArray: %d"), BuildGameState->PlayerArray.Num());
		if (BuildGameState->PlayerArray.Num() == 0)
		{
			EndMatch();
		}
		else
		{
			GetWorld()->GetTimerManager().ClearTimer(PlayerLogoutTimeoutHandler);
		}
	}
	else
	{
		EndMatch();
	}
}

void AGameMode_Build::EndMatch()
{
	Super::EndMatch();

	GetWorld()->GetTimerManager().SetTimer(ShutDownTimerHandler, this, &AGameMode_Build::OnServerShutDown, 5.0f, false, 5.0f);
}

void AGameMode_Build::OnServerShutDown() const
{
	UE_LOG(LogInit, Warning, TEXT("AGameMode_Build::OnServerShutDown"));
	FGenericPlatformMisc::RequestExit(false);
}


void AGameMode_Build::DefaultTimer()
{
	Super::DefaultTimer();
	ValidatePlayerLogoutTimeout();
}

void AGameMode_Build::StartPlay()
{
	Super::StartPlay();


	BindSquadHandlers();

	ContextValidator = IOperationContainer::ContextValidatorValue;
	IOperationContainer::Context = this;
	IOperationContainer::AllowHttpRequest = true;
	IOperationContainer::ContextValidator = &ContextValidator;
	
	buildingController.OnRepairObject.OverrideGlobalToken = OwnerTokenId;
	buildingController.OnHarvest.OverrideGlobalToken = OwnerTokenId;
	buildingController.RemoveObject.OverrideGlobalToken = OwnerTokenId;
	buildingController.PlaceObject.OverrideGlobalToken = OwnerTokenId;

	identityController.CheckAccountStatus.OverrideGlobalToken = OwnerTokenId;
	identityController.CheckAccountStatus.TimerStart(GetWorldTimerManager());
#if WITH_EDITOR
	storeController.ListOfInstance.Request(OwnerTokenId);
#else
	identityController.RequestClientData.Request(OwnerTokenId);
#endif
}

void AGameMode_Build::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	lobbyController.GetMatchResults.TimerStop(GetWorldTimerManager());
	identityController.CheckAccountStatus.TimerStop(GetWorldTimerManager());
	squadController.GetSquadInvites.TimerStop(GetWorldTimerManager());
	//GetWorldTimerManager().ClearTimer(lobbyController.SearchStatus.TimerHandle);

	IOperationContainer::AllowHttpRequest = false;
}

void AGameMode_Build::HandleMatchIsWaitingToStart()
{
	Super::HandleMatchIsWaitingToStart();
}

void AGameMode_Build::HandleMatchHasStarted()
{
	Super::HandleMatchHasStarted();
}

void AGameMode_Build::HandleMatchHasEnded()
{
	Super::HandleMatchHasEnded();
}

AActor* AGameMode_Build::ChoosePlayerStart_Implementation(AController* Player)
{
	TArray<AActor*> Actors;
	UGameplayStatics::GetAllActorsWithTag(GetWorld(), TEXT("BuildingStart"), Actors);
	if (Actors.Num())
	{
		return (Actors.Num() >= 2) ? Actors[FMath::RandRange(0, Actors.Num() - 1)] : Actors[0];
	}

	return Super::ChoosePlayerStart_Implementation(Player);
}

void AGameMode_Build::RequestBuyBuildItem(APlayerController* InRequestFrom, UItemBuildEntity* InItem, const int32& InCount)
{
	if (InItem && BuildGameState && InRequestFrom)
	{
		const auto IsOwner = (InRequestFrom->PlayerState == BuildGameState->OwnerState);
		UE_LOG(LogGameMode, Warning, TEXT("AGameMode_Build::RequestBuyBuildItem >> PlayerName: %s, IsOwner: %s"), *InRequestFrom->PlayerState->PlayerName, IsOwner ? TEXT("True") : TEXT("False"));

		if (IsOwner || InCount == 0)
		{
			CALL_CTR_FNC(ClientToggleBuyWaiting, 0, 0, false);
		}

		//TODO: SeNTIke
		//if (IsOwner && InCount >= 1)
		//{
		//	storeController.UnlockItem.Request(FUnlockItemRequestContainer(InItem->GetModelId(), CategoryTypeId::Building, InCount), OwnerTokenId);			
		//}
		//else if(BuildGameState->OwnerState && InRequestFrom->PlayerState && InCount >= 1)
		//{
		//	// TODO: �������� ������������ ������� � ��������
		//	auto OwnerCtrl = Cast<ABuildPlayerController>(BuildGameState->OwnerState->GetOwner());
		//	auto RequestCtrl = Cast<ABuildPlayerController>(InRequestFrom);
		//	if (OwnerCtrl && RequestCtrl)
		//	{
		//		RequestCtrl->ClientToggleBuyWaiting(InItem->GetModelId(), InCount, true);
		//		OwnerCtrl->ClientShowBuyRequest(InItem->GetModelId(), InCount, InRequestFrom->PlayerState->PlayerName);
		//	}			
		//}
	}
}

void AGameMode_Build::OnBuildPurchased(const FUnlockItemResponseContainer& InData)
{
	//TODO: SeNTIke
	//if (BuildGameState && BuildGameState->OwnerInventory.IsValidIndex(InData.ModelId))
	//{
	//	BuildGameState->OwnerInventory[InData.ModelId]->SetCount(InData.TotalAmount);
	//}
}

void AGameMode_Build::OnLootDataList(const FLobbyClientLootData& InList)
{
	squadController.RespondDuelInvite.OverrideGlobalToken = OwnerTokenId;
	squadController.GetSquadInvites.OverrideGlobalToken = OwnerTokenId;
	lobbyController.GetMatchResults.OverrideGlobalToken = OwnerTokenId;
	//lobbyController.SearchStatus.OverrideGlobalToken    = OwnerTokenId;

	squadController.GetSquadInvites.TimerStart(GetWorldTimerManager());
	lobbyController.GetMatchResults.TimerStart(GetWorldTimerManager());
	//GetWorldTimerManager().SetTimer(lobbyController.SearchStatus.TimerHandle, lobbyController.SearchStatus.TimerDelegate, 4.0f, true, 4.0f);

	//===========================================================================
	if (BuildGameState)
	{		
		if (auto GameInst = Cast<UShooterGameInstance>(GetGameInstance()))
		{
			auto InstRepository = &GameInst->EntityRepository->InstanceRepository;
			for (auto item : InstRepository->Build) // Duplicate objects for replication
			{
				if (BuildGameState->OwnerInventory.IsValidIndex(item.Key))
				{
					BuildGameState->OwnerInventory[item.Key] = DuplicateObject<UItemBuildEntity>(item.Value, BuildGameState, *FString::Printf(TEXT("OwnerInventory_%d"), item.Key));
				}
				else
				{
					for (int32 i = BuildGameState->OwnerInventory.Num(); i <= item.Key; ++i)
					{
						BuildGameState->OwnerInventory.AddZeroed();
					}

					BuildGameState->OwnerInventory[item.Key] = DuplicateObject<UItemBuildEntity>(item.Value, BuildGameState, *FString::Printf(TEXT("OwnerInventory_%d"), item.Key));
				}
			}

			//TODO: SeNTIke
			//for (auto item : InList.BuildingList)
			//{
			//	if (BuildGameState->OwnerInventory.IsValidIndex(item.ModelId) && BuildGameState->OwnerInventory[item.ModelId])
			//	{
			//		BuildGameState->OwnerInventory[item.ModelId]->SetCount(item.Count);
			//		BuildGameState->OwnerInventory[item.ModelId]->SetEntityId(item.EntityId);
			//	}
			//}
		}

		BuildGameState->OnRep_OwnerInventory();
	}

	buildingController.GetListOfBuildings.Request(OwnerTokenId);	
}

void AGameMode_Build::RequestStartTutorial() const
{
#if !UE_SERVER
	if (GetNetMode() != NM_DedicatedServer)
	{
		bool isFound = false;
		const auto TutorialId = 6;

		//for (auto tut : FPlayerEntityInfo::Instance.Tutorials)
		//{
		//	if (tut.TutorialId == TutorialId && tut.TutorialComplete == false)
		//	{
		//		identityController.OnStartTutorial.Request(TutorialId);
		//		isFound = true;
		//		break;
		//	}
		//}

		if (isFound == false)
		{
			identityController.OnStartTutorial.Request(TutorialId);
		}
	}
#endif
}

void AGameMode_Build::OnBuildingsList(const TArray<FWorldBuildingItemView>& InList)
{
	//if (BuildGameState)
	//{
	//	TArray<ABuildPlacedComponent*> ArrayPlacedBuildings;
	//	TArray<AActor*> FindPlacedBuildings;
	//	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ABuildPlacedComponent::StaticClass(), FindPlacedBuildings);

	//	for (auto PlacedBuilding : FindPlacedBuildings)
	//	{
	//		auto PrePlacedBuilding = Cast<ABuildPlacedComponent>(PlacedBuilding);
	//		if (PrePlacedBuilding && PrePlacedBuilding->IsPrePlaced())
	//		{
	//			ArrayPlacedBuildings.Add(PrePlacedBuilding);
	//		}
	//	}

	//	FActorSpawnParameters ActorSpawnParameters;
	//	ActorSpawnParameters.Owner = BuildGameState;
	//	ActorSpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	//	// Spawn
	//	for (auto Item : InList)
	//	{			
	//		if (BuildGameState->OwnerInventory.IsValidIndex(Item.ModelId) && BuildGameState->OwnerInventory[Item.ModelId] && BuildGameState->OwnerInventory[Item.ModelId]->ComponentTemplate)
	//		{
	//			ABuildPlacedComponent* SpawnedActor = nullptr;
	//			for (auto preplaced : ArrayPlacedBuildings)
	//			{
	//				if (preplaced && preplaced->GetPrePlacedInstanceId() == Item.ModelId)
	//				{
	//					SpawnedActor = preplaced;
	//					break;
	//				}
	//			}

	//			if (SpawnedActor == nullptr)
	//			{
	//				SpawnedActor = GetWorld()->SpawnActor<ABuildPlacedComponent>(BuildGameState->OwnerInventory[Item.ModelId]->ComponentTemplate, FTransform(Item.Rotation, Item.Position), ActorSpawnParameters);
	//			}

	//			if (SpawnedActor)
	//			{
	//				FGuid::Parse(Item.ItemId, SpawnedActor->GUID);
	//				SpawnedActor->InitializeInstance(BuildGameState->OwnerInventory[Item.ModelId]);
	//				SpawnedActor->OnComponentPlaced();
	//				SpawnedActor->Health.X = FMath::Min<float>(Item.Health, SpawnedActor->Health.Y);
	//				SpawnedActor->OnRep_Health();

	//				int32 BuildingType = SpawnedActor->Instance->GetBuildingType();
	//				if (FFlagsHelper::HasAnyFlags<int32>(BuildingType, FFlagsHelper::ToFlag<int32>(EBuildingType::WarehouseMoney) | FFlagsHelper::ToFlag<int32>(EBuildingType::MinerMoney)))
	//				{
	//					SpawnedActor->PlacedBuildingData.FindOrAdd(TEXT("Storage")) = Item.Storage;
	//					SpawnedActor->ReInitializeBuildingData(false);

	//					StorageComponents.Add(SpawnedActor->GUID, SpawnedActor);
	//				}

	//				BuildingsList.Add(SpawnedActor->GUID, SpawnedActor);

	//				if (SpawnedActor->Health.X < SpawnedActor->Health.Y)
	//				{
	//					DamagedBuildingsList.Add(SpawnedActor->GUID, SpawnedActor);
	//				}
	//			}
	//		}
	//	}

	//	// Links
	//	for (auto Item : InList)
	//	{
	//		FGuid TargetGuid, TargetGuidParent;
	//		FGuid::Parse(Item.ItemId, TargetGuid);
	//		if (BuildingsList.Contains(TargetGuid) == false) continue;
	//		auto Building = BuildingsList.FindChecked(TargetGuid);

	//		for (auto Parents : Item.Sockets)
	//		{
	//			FGuid::Parse(Parents.TargetId, TargetGuidParent);
	//			if (BuildingsList.Contains(TargetGuidParent) == false) continue;
	//			auto BuildingParent = BuildingsList.FindChecked(TargetGuidParent);

	//			BuildingParent->ChildBuildComponents.Add(Building);
	//			Building->ParentBuildComponents.Add(BuildingParent);
	//		}
	//	}
		/* TODO: ����� ��������� ����������.
		if (DamagedBuildingsList.Num())
		{
			FTimerHandle _th;
			GetWorldTimerManager().SetTimer(_th, FTimerDelegate::CreateLambda([&]() {
				FWorldBuildingRepairContainer Items;
				int32 _cost = 0;
				for (auto Item : DamagedBuildingsList)
				{
					if (Item.Value && Item.Value->IsValidLowLevel() && Item.Value->Instance)
					{
						FWorldBuildingRepairView RepairView;
						RepairView.ItemId = Item.Value->GUID.ToString();
						RepairView.Health = Item.Value->Health.Y;
						Items.Items.Add(RepairView);

						_cost += FMath::CeilToInt(float(Item.Value->Instance->GetCost().Amount) / 2);
					}
				}

#if !UE_SERVER
				SMessageBox::AddQueueMessage(TEXT("DamagedBuildingsList"), FOnClickedOutside::CreateLambda([&, _c_cost = _cost, _Items = Items]() {
					SMessageBox::Get()->SetHeaderText(NSLOCTEXT("Notify", "Notify.DamagedBuildingsList.Title", "Repair Buildings"));
					SMessageBox::Get()->SetContent(FText::Format(NSLOCTEXT("Notify", "Notify.DamagedBuildingsList.Desc", "We found on your base damaged buildings, repairing all?\r\nRepairing cost: {0}"), FRichTextHelpers::GetResourse(TEXT("money"), _c_cost)), ETextJustify::Center);
					SMessageBox::Get()->SetButtonsText(FButtonNameLocalization::Yes, FButtonNameLocalization::No);
					SMessageBox::Get()->OnMessageBoxButton.AddLambda([&, _Items2 = _Items](EMessageBoxButton InButton) {
						if (InButton == EMessageBoxButton::Left)
						{
							buildingController.OnRepairObject.Request(_Items2, OwnerTokenId);
						}
						SMessageBox::Get()->ToggleWidget(false);
					});
					SMessageBox::Get()->SetEnableClose(true);
					SMessageBox::Get()->ToggleWidget(true);
					SMessageBox::Get()->OnMessageBoxHidden.AddLambda([&]() { SMessageBox::RemoveQueueMessage("DamagedBuildingsList"); });
				}));
#endif

			}), 2.5f, false);
		}
		
	}*/
}

void AGameMode_Build::OnBuildingsRepairList(const TArray<FWorldBuildingRepairView>& InList)
{
	for (auto ans : InList)
	{
		FGuid guid;
		FGuid::Parse(ans.ItemId, guid);
		if (BuildingsList.Contains(guid))
		{
			BuildingsList.FindChecked(guid)->Health.X = ans.Health;
			BuildingsList.FindChecked(guid)->OnRep_Health();
		}
	}
}

void AGameMode_Build::OnBuildPlaced(const FWorldBuildingItemView& InAnswer)
{
	/*TArray<AActor*> Actors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ABuildPlacedComponent::StaticClass(), Actors);

	for (auto actor : Actors)
	{
		if (auto PlacedBuilding = Cast<ABuildPlacedComponent>(actor))
		{
			FGuid OldGuid;
			FGuid::Parse(InAnswer.ItemId, OldGuid);
			if (PlacedBuilding->GUID == OldGuid && PlacedBuilding->Instance)
			{
				FGuid::Parse(InAnswer.NewItemId, PlacedBuilding->GUID);
				int32 BuildingType = PlacedBuilding->Instance->GetBuildingType();
				if (FFlagsHelper::HasAnyFlags<int32>(BuildingType, FFlagsHelper::ToFlag<int32>(EBuildingType::WarehouseMoney) | FFlagsHelper::ToFlag<int32>(EBuildingType::MinerMoney)))
				{
					PlacedBuilding->PlacedBuildingData.FindOrAdd(TEXT("Storage")) = InAnswer.Storage;
					PlacedBuilding->ReInitializeBuildingData(false);

					StorageComponents.Add(PlacedBuilding->GUID, PlacedBuilding);
				}

				BuildingsList.Add(PlacedBuilding->GUID, PlacedBuilding);
				break;
			}
		}
	}*/
}

void AGameMode_Build::OnBuildRemoved(const FWorldBuildingItemView& InAnswer)
{

}

void AGameMode_Build::OnCheckAccountStatusResponse(const FAccountStatusContainer& response)
{
	auto MyCtrl = Cast<ABuildPlayerController>(GetWorld()->GetFirstPlayerController());
	if (MyCtrl)
	{
		for (auto sItem : response.Storage)
		{
			FGuid guid;
			FGuid::Parse(sItem.ItemId, guid);
			if (StorageComponents.Contains(guid))
			{
				auto TargetBuilding = StorageComponents.FindChecked(guid);

				if (TargetBuilding->PlacedBuildingData.Contains(TEXT("Storage")))
				{
					TargetBuilding->PlacedBuildingData.FindChecked(TEXT("Storage")) = sItem.Storage;
					const int32 index = TargetBuilding->PlacedBuildingData_Names.Find(TEXT("Storage"));
					if (TargetBuilding->PlacedBuildingData_Data.IsValidIndex(index))
						TargetBuilding->PlacedBuildingData_Data[index] = sItem.Storage;
				}
			}
		}

		if (BuildGameState)
		{
			BuildGameState->Cash = response.Cash;
		}
#if !UE_SERVER
		//FPlayerEntityInfo::Instance.TutorialId = response.TutorialId;
		//FPlayerEntityInfo::Instance.Tutorials = response.Tutorials;		
#endif
	}	
}

void AGameMode_Build::OnBuildPurchaseNoMoney()
{
	//SMessageBox::Get()->SetHeaderText(NSLOCTEXT("All", "All.NoMoney.Title", "No Money"));
	//SMessageBox::Get()->SetContent(NSLOCTEXT("All", "All.NoMoney.Desc2", "Not enough money for this action.\nMaybe go into battle?"));
	//SMessageBox::Get()->SetButtonsText(FButtonNameLocalization::Yes, FButtonNameLocalization::No);
	//SMessageBox::Get()->OnMessageBoxButton.AddLambda([&](const EMessageBoxButton& InButton) { 
	//	if (InButton == EMessageBoxButton::Left)
	//	{
	//		UGameplayStatics::OpenLevel(GetWorld(), TEXT("LobbyRoom"), true, TEXT("?game=LobbyGame?IntoBattle=1"));
	//	}

	//	SMessageBox::Get()->ToggleWidget(false); 
	//});
	//SMessageBox::Get()->SetEnableClose(false);
	//SMessageBox::Get()->ToggleWidget(true);	
}

void AGameMode_Build::OnStartTutorial(const FStartTutorialView& InData)
{	
	auto GameInst = Cast<UShooterGameInstance>(GetGameInstance());
	if (InData.TutorialComplete == false && InData.TutorialId != INDEX_NONE && GameInst && GameInst->CurrentTutorialId != InData.TutorialId)
	{
		GameInst->CurrentTutorialId = InData.TutorialId;
		if (GameInst->GetTutorials().Contains(GameInst->CurrentTutorialId))
		{
			GameInst->GetTutorials().FindChecked(GameInst->CurrentTutorialId)->StartTutorial(InData.StepId);
		}
	}
}

ABuildPlayerController* AGameMode_Build::GetSessionOwner() const
{
	if (BuildGameState && BuildGameState->OwnerState)
	{
		return Cast<ABuildPlayerController>(BuildGameState->OwnerState->GetOwner());
	}

	return nullptr;
}