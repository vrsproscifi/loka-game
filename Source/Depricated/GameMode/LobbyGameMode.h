#pragma once

//#include "Service.Header.h"
//
//#include "Reqest.Header.h"

#include <InstanceRepository.h>
#include <StoreController/StoreController.h>
#include <IdentityController/IdentityController.h>
#include <LeagueController/LeagueController.h>
#include <LobbyController/LobbyController.h>
#include <HangarController/HangarController.h>
#include <FriendController/FriendController.h>
#include <ChatController/ChatController.h>
#include <PromoController/PromoController.h>
#include <SquadController/SquadController.h>
#include <MarketingController/MarketingController.h>
#include <CompanyController/CompanyController.h>
#include "BuildingController/BuildingController.h"

#include "OnlineSubsystem.h"
#include "OnlineIdentityInterface.h"

#include "RichTextHelpers.h"
#include "Marketing/Lobby/LobbyActionView.h"

#include "GameFramework/GameMode.h"
#include "LobbyGameMode.generated.h"

namespace ELeaguePlayerRole
{
	enum Type;
};

namespace ELobbyChatChannel
{
	enum Type;
};

namespace EFreindAction
{
	enum Type;
};

namespace MessageBoxButton
{
	enum Type;
}

typedef MessageBoxButton::Type EMessageBoxButton;

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



class ALobbyPlayerController;
class UItemBaseEntity;
//

namespace EJoinGameState
{
	enum Type
	{
		None,
		Search,
		Continue
	};
}

UCLASS()
class ALobbyGameMode : public AGameMode
{
	friend class SFriendsCommands;

	GENERATED_BODY()

	UPROPERTY(Transient) uint64 ContextValidator;
	UPROPERTY(Transient, VisibleInstanceOnly) bool bUseSteamAuthentication;
	UPROPERTY(Transient, VisibleInstanceOnly) TMap<FString, FLobbyActionContainer> LobbyActionStatisticInProcess;
	UPROPERTY(Transient, VisibleInstanceOnly) TArray<FLobbyActionView> LobbyActionStatistic;
	UPROPERTY(Transient, VisibleInstanceOnly) FLobbyActionView CurrentLobbyAction;

	void OnUpdateSquadMembers(const FSqaudInformation& squad);
	TArray<FString> GetSquadMemberNames() const;
	TArray<FQueueMemberContainer> SquadMembers;


	FCriticalSection RequestArraysLock;

	friend class ALobbyPlayerController;

	IOnlineSubsystem* GetSteamSubsystem()
	{
		return IOnlineSubsystem::Get(FName(*FString("Steam")));
	}

	void StopHttpQueries();
	//==============================================================================================================================
	//												[ ServiceController ]
	IdentityController	identityController;
	StoreController		storeController;
	LeagueController	leagueController;
	LobbyController		lobbyController;
	HangarController	hangarController;
	FriendController	friendController;
	ChatController		chatController;
	PromoController		promoController;
	SquadController		squadController;
	MarketingController marketingController;
	CompanyController	companyController;
	BuildingController	buildingController;

	void OnAttackToResponse() const;
	void OnAttack_AttackOnYourself() const;
	void OnAttack_PlayerNotFound() const;
	void OnAttack_PlayerUnderDefence() const;
	void OnAttack_ServerError() const;

	

	void OnQueryAchievementsComplete(const FUniqueNetId& playerId, const bool isSu);
	void OnStartTutorial(const FStartTutorialView& InData);

	bool IsAuthenticationComplete;
	void OnSteamPaymentRequired();
	void OnSteamAuthenticationComplete(const Operation::Authentication::Response& response);
	void OnAuthenticationComplete(const Operation::Authentication::Response& response) const;
	void OnRequestClientData(const FPlayerEntityInfo& response);
	void OnRequestClientDataComplete();
	void OnRequestClientDataAttemp();

	void OnRequestRequestLootData(const FLobbyClientLootData& response);
	void OnRequestRequestPreSetData(const TArray<FClientPreSetData>& response);
	void OnUnlockItem(const FUnlockItemResponseContainer& response);
	void OnUnlockProfile(const FUnlockProfileResponse& response);
	void OnSwitchFractionResponse(const FRequestOneParam<int32>& fractionId);
	void OnSelectFractionResponse(const FFractionReputation& fraction);

	void OnCheckExistAccount(const FRequestOneParam<bool>& response);
	void OnCreateGameAccount(const FRequestOneParam<bool>& response) const;
	void OnCreateGameAccountBadResponse() const;

	void AddLobbyStatisticProxy();
	void OnSuccessAddLobbyStatistic(const FRequestOneParam<FString>& requestId);
	
	UPROPERTY(Transient) FTimerHandle AddLobbyStatisticTimerHandle;
	FTimerDelegate AddLobbyStatisticTimerDelegate;

	void OnGetMatchResults(const TArray<FLobbyMatchResultPreview>& results);

	void OnCheckAccountStatusResponse(const FAccountStatusContainer& response);
	void OnCheckServerStatusResponse(const FRequestOneParam<int32>& response);

	void OnCheckServerStatusNotResponsed();
	void OnCheckServerStatusNotWorking();

	void OnChangeAccountEmailSuccess(const FRequestOneParam<FString>& response = FString(""));
	void OnBadEmailAddresFormat() const;

	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//																		[ Promo ]
	void BindPromoHandlers();
	void OnActivatePromoResponse(const FActivatePromoResponse& response);
	void OnActivatePromoCodeNotFound();
	void OnActivatePromoCodeActivated();


	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//																		[ Social ]
	void BindSocialTaskHandlers();
	void OnTakeSocialTaskResponse(const FRequestOneParam<FString>& response);
	void OnTakeSocialTaskNotFound();
	void OnTakeSocialTaskActivated();
	void OnStatusOfSocialTaskResponse(const FSocialTaskStatusResponse& response);
	void OnListOfSocialTaskResponse(const TArray<FSocialTaskWrapper>& tasks);

	void OnRequestImprovementsListResponse(const FImprovementViewModel& response);
	void OnVoteImprovementResponse(const FImprovementVoteCreate& response);

	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//																		[ SQUAD ]
	void BindSquadHandlers();

	void OnGetSquadDuelInvitesResponse(const FSquadInviteListContainer& invites);
	void OnGetSquadInvitesResponse(const FSquadInviteListContainer& invites);
	void OnRemoveMemberFromSquad(const FRequestOneParam<FString>& playerName);

	void OnLeaveMemberFromSquad();

	//==============================================================
	//						SendSquadInvite 
	void OnSendSquadInviteResponse(const FRequestOneParam<FString>& playerName);
	void OnSendDuelInviteResponse(const FSendSquadDuelInviteResponse& playerName);
	void OnSendDuelInviteProxy(const FRequestOneParam<FString>& playerName);

	void OnSendSquadInviteYourself();
	void OnSendDuelInviteYourself();
	void OnSendSquadInviteAlreadySent(const FRequestOneParam<FString>& playerName);
	void OnSendSquadInvitePlayerNotFound();
	void OnSendSquadInvitePlayerInSquad(const FRequestOneParam<FString>& playerName);
	void OnSendDuelInvitePlayerNotEnouthMoney(const FRequestOneParam<FString>& playerName);
	void OnSendSquadInvitePlayerNotOnline(const FRequestOneParam<FString>& playerName);
	void OnSendSquadInviteYouAreNotLeader();
	void OnSendSquadInviteInviteTimeoutSent();

	//==============================================================
	//						RemoveFromSquad

	void OnRemoveFromSquadPlayerNotFound();
	void OnRemoveFromSquadPlayerNotSquad();
	void OnRemoveFromSquadYouAreNotLeader();
	  
	//==============================================================
	//						RespondSqaudInvite
	void OnRespondSqaudInviteResponse(const FRequestOneParam<FString>& playerName);
	void OnRespondSqaudInviteInviteNotFound();
	void OnRespondSqaudInviteInviteAreRecived(const FRequestOneParam<FString>& playerName);
	void OnRespondDuelInviteNotEnouthMoney(const FRequestOneParam<FString>& playerName);
	void OnRespondSqaudInviteSenderInSquad(const FRequestOneParam<FString>& playerName);
	void OnRespondDuelInviteSenderNotEnouthMoney(const FRequestOneParam<FString>& playerName);
	void OnRespondDuelInviteResponse(const FRequestOneParam<FString>& playerName);
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	void OnPreSetItemEquip(const FPreSetEquipResponse& response);

	void OnBuildingsList(const TArray<FWorldBuildingItemView>& InList);

protected:
	void UpdateFractionProgress(const FFractionReputation& fraction);

	void OnProxyBuildingMode();
	void OnProxySearchSessionBegin(const FSearchSessionParams& params);
	void OnProxySearchSessionCancel() const;
	void OnSessionMatchFounded(const FSearchSessionStatus& response);
	void OnSessionNotAuthorized();
	mutable bool IsAuthRequestHandled;

	mutable EJoinGameState::Type JoinGameState;
	bool IsMatchMakingPrepareBeginNotified;
	bool IsMatchMakingPrepareCompleteNotified;

	//==================================================
	UPROPERTY(Transient)
	FTimerHandle HandleProcessTravelToGame;
	FTimerDelegate DelegateProcessTravelToGame;
	FSearchSessionStatus SearchSessionStatus;
	void OnProcessTravelToGame();
	//==================================================
	void TravelToGame(const FSearchSessionStatus& response);
	void ShowTravelMessage(const FSearchSessionStatus& response) const;
	void OnSuccessEmailConfirmStatus();
	
	FORCEINLINE ALobbyPlayerController* LobbyPlayerController() const;

	FOnLoginCompleteDelegate OnSteamLoginCompleteDelegate;
	void OnSteamLoginComplete(int32 ctr, bool isOk, const FUniqueNetId& id, const FString& token);
	void OnSteamLoginAttemp();
	int AttempCount = 0;

	UPROPERTY(Transient)
	FTimerHandle SteamLoginAttempTimerHandle;

	FTimerDelegate SteamLoginAttempTimerDelegate;

	void OnStoreInitialized(const FListOfInstance& data);
	//=====================================================================================================
	//										[ CLIENT BEGIN ]
	//=====================================================================================================
	//virtual void StartToLeaveMap();



	bool bIsJoined = false;
	//========================================================================================================
	//												[ Chat Service ]


	//=====================================================================================================
	//										[ CLIENT END ]
	//=====================================================================================================
//#else
//	void StartPlayClientService() { }
//	void InitializeClientService() { }
//#endif

public:
	UPROPERTY(Blueprintable, BlueprintType, BlueprintReadWrite, EditAnywhere, Category = "Loka Game | Characters")
		class UEntityRepository* EntityRepository;

	FInstanceRepository* InstanceRepository;


public:
	ALobbyGameMode(const class FObjectInitializer& PCIP);
	void InitializeSlateWidgets();
	void OnSwitchedLobbySlide(const ELobbyActionButton& Index, const ELobbyActionButton& OldIndex);
	void ShowEnterEmailBox();
	void ShowEnterEmailConfirmCodeBox(const FRequestOneParam<FString>& response);

public:
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Lobby Characters

	UPROPERTY(Transient)
	FVector LobbyPlayerCharacterPosition;
	
	UPROPERTY(Transient)
	FRotator LobbyPlayerCharacterRotation;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Player Character Index
public:
	void onMessageSend(const FString&, const ELobbyChatChannel::Type, const FName&);

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Base Events
	virtual void StartPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Characters")
	UAnimBlueprintGeneratedClass*			CharacterAnimation;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
public:

	UFUNCTION()
	void onClickMessageBoxButton(uint8 button);

	void OnClickCreateGameAccount(const EMessageBoxButton& button);

	//====================================================================================================================================================================================================================================================================================================
	//====================================================================================================================================================================================================================================================================================================
	//====================================================================================================================================================================================================================================================================================================
	//																			Slate UI Pointers
	TSharedPtr<class SLobbyContainer> Slate_LobbyContainer;
	
	TSharedPtr<class SInputWindow> Slate_MessageBoxInput;
	TSharedPtr<class SWindowDuelRules> Slate_MessageBoxDuel;

	TSharedPtr<class SAuthForm> Slate_AuthForm;
	TSharedPtr<class SGeneralPage> Slate_GeneralPage;
	
	TSharedPtr<class SUTMenuBase> Slate_SettingsMenu;

	TSharedPtr<class SSettingsManager> Slate_SettingsManager;
	TSharedPtr<class SFractionManager> Slate_FractionManager;
	TSharedPtr<class SFractionSelector> Slate_FractionSelector;
	TSharedPtr<class SLobbyAchievements> Slate_LobbyAchievements;
	
	TSharedPtr<class STournamentForm> Slate_TournamentForm;
	TSharedPtr<class STournamentTeam> Slate_TournamentTeam;
	TSharedPtr<class STournamentTree> Slate_TournamentTree;

	TSharedPtr<class SNotifyList> Slate_NotifyList;
	TSharedPtr<class SLobbyChat> Slate_LobbyChat;
	TSharedPtr<class SFriendsForm> Slate_FriendsForm;

	TSharedPtr<class SLeagueForm> Slate_LeagueForm;
	TSharedPtr<class SLeagueFormInfo> Slate_LeagueFormInfo;

	TSharedPtr<class SCharacterManager> Slate_CharacterManager;	


	//====================================================================================================================================================================================================================================================================================================
	//																			Slate UI Helper Events

	TOptional<int32> GetPlayerMoney() const
	{
		return FPlayerEntityInfo::Instance.Cash.Money;
	}

	TOptional<int32> GetPlayerDonate() const
	{
		return FPlayerEntityInfo::Instance.Cash.Donate;
	}




	UPROPERTY(EditDefaultsOnly, Category = LokaGamePlay)
	TSubclassOf<class ALobbyPlayerCharacter> DefaultPawnCustom;

public:

	FRichTextHelpers::FTextHelper TextHelper_NotifyValue;

	virtual void InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage) override;

protected:

	FTimerHandle timer_CheckEntityRepository;
	void InternalBeginPlay();

	bool bIsImprovementsVoteNotified;
	bool bIntoBattle;

	void OpenBuildingMode();
};