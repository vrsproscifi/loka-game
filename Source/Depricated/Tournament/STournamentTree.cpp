// VRSPRO

#include "LokaGame.h"
#include "STournamentTree.h"
#include "SlateOptMacros.h"

#include "STreePin.h"
#include "SResourceTextBox.h"

#include "TournamentPreview.h"

class LOKAGAME_API STournamentTreeItem : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(STournamentTreeItem)
	{}
	SLATE_EVENT(FOnClickedTeam, OnClickedTeam)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs)
	{
		OnClickedTeam = InArgs._OnClickedTeam;

		ChildSlot
		[
			SAssignNew(Border, SBorder)
			.Padding(0)
			.BorderImage(new FSlateColorBrush(FLinearColor::Black))
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot().AutoHeight()
				[
					SAssignNew(Button[0], SButton)
					.OnClicked(this, &STournamentTreeItem::OnClickedTeamLocal, false)
					.VAlign(EVerticalAlignment::VAlign_Center)
					[
						SAssignNew(TextBlock[0], STextBlock).Text(FText::FromString("Team_1"))
					]					
				]
				+ SVerticalBox::Slot().AutoHeight()
				[
					SNew(SSeparator).Orientation(EOrientation::Orient_Horizontal).Thickness(2.0f)
				]
				+ SVerticalBox::Slot().AutoHeight()
				[
					SAssignNew(Button[1], SButton)
					.OnClicked(this, &STournamentTreeItem::OnClickedTeamLocal, true)
					.VAlign(EVerticalAlignment::VAlign_Center)
					[
						SAssignNew(TextBlock[1], STextBlock).Text(FText::FromString("Team_2"))
					]
				]
			]
		];
	}

	void SetTeamPair(const FTournamentMatchDetails &PairTeam)
	{
		Team = PairTeam;
		TextBlock[0]->SetText(FText::FromString(Team.First.Name));
		TextBlock[1]->SetText(FText::FromString(Team.Second.Name));
	}

	FTournamentMatchDetails Team;

	void SetOtherItem(TSharedPtr<STournamentTreeItem> Other)
	{
		OtherItem = Other;
	}

	TSharedPtr<STournamentTreeItem> GetOtherItem() const
	{
		return OtherItem;
	}

protected:

	FReply OnClickedTeamLocal(const bool IsSecond)
	{
		OnClickedTeam.ExecuteIfBound(Team.First.Uid);
		return FReply::Handled();
	}

	FOnClickedTeam OnClickedTeam;

	TSharedPtr<SBorder> Border;
	TSharedPtr<SButton> Button[2];
	TSharedPtr<STextBlock> TextBlock[2];

	TSharedPtr<STournamentTreeItem> OtherItem;
};

class LOKAGAME_API STournamentTreeStep : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(STournamentTreeStep)
		: _Count(1)
	{}
	SLATE_ARGUMENT(int32, Count)
	SLATE_EVENT(FOnClickedTeam, OnClickedTeam)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs)
	{
		CountPairs = InArgs._Count;

		ChildSlot
		[
			SAssignNew(Background, SBorder)
			.Padding(0)
			.BorderImage(new FSlateColorBrush(FColor::FromHex("DCDCDCAA")))
			[
				SAssignNew(VerticalBox, SVerticalBox)
			]
		];

		for (int i = 0; i < CountPairs; ++i)
		{
			TreeItems.Add(SNew(STournamentTreeItem).OnClickedTeam(InArgs._OnClickedTeam));

			VerticalBox->AddSlot().FillHeight(1);
			VerticalBox->AddSlot().Padding(FMargin(120, 10)).AutoHeight()
			[
				TreeItems.Last().ToSharedRef()
			];
			VerticalBox->AddSlot().FillHeight(1);
		}
	}

	void SetIsActive(const bool IsActive)
	{
		Background->SetBorderImage(IsActive ? new FSlateColorBrush(FColor::FromHex("FFAF00AA")) : new FSlateColorBrush(FColor::FromHex("DCDCDCAA")));
	}

	TSharedPtr<STournamentTreeItem> GetItem(const int32 &Index)
	{
		if (TreeItems.IsValidIndex(Index))
		{
			return TreeItems[Index];
		}

		return nullptr;
	}

	int32 GetNumItems() const
	{
		return CountPairs;
	}

protected:
	TSharedPtr<SBorder> Background;
	TArray<TSharedPtr<STournamentTreeItem>> TreeItems;
	TSharedPtr<SVerticalBox> VerticalBox;

	int32 CountPairs;
};

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void STournamentTree::Construct(const FArguments& InArgs)
{
	OnClickedJoin = InArgs._OnClickedJoin;

	Steps.Add(SNew(STournamentTreeStep).Count(4).OnClickedTeam(InArgs._OnClickedTeam));
	Steps.Add(SNew(STournamentTreeStep).Count(2).OnClickedTeam(InArgs._OnClickedTeam));
	Steps.Add(SNew(STournamentTreeStep).Count(1).OnClickedTeam(InArgs._OnClickedTeam));

	ChildSlot
	[
		SNew(SOverlay)
		+ SOverlay::Slot()
		.HAlign(EHorizontalAlignment::HAlign_Fill)
		.VAlign(EVerticalAlignment::VAlign_Fill)
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot()
			[
				Steps[0].ToSharedRef()
			]
			+ SHorizontalBox::Slot()
			[
				Steps[1].ToSharedRef()
			]
			+ SHorizontalBox::Slot()
			[
				Steps[2].ToSharedRef()
			]
		]
		+ SOverlay::Slot()
		.HAlign(EHorizontalAlignment::HAlign_Right)
		.VAlign(EVerticalAlignment::VAlign_Top)
		[
			SNew(SBox)
			.MinDesiredWidth(FOptionalSize(480))
			[
				SNew(SBorder)
				.Padding(10)
				.BorderImage(new FSlateColorBrush(FColor::Black.WithAlpha(100)))
				[
					SNew(SVerticalBox)
					+ SVerticalBox::Slot().Padding(FMargin(10, 2)).AutoHeight()
					[
						SNew(SHorizontalBox)
						+ SHorizontalBox::Slot()
						[
							SNew(STextBlock).Text(FText::FromString("Current step:"))
						]
						+ SHorizontalBox::Slot()
						[
							SNew(STextBlock).Text(FText::FromString("1/3"))
						]
					]
					+ SVerticalBox::Slot().Padding(FMargin(10, 2)).AutoHeight()
					[
						SNew(SHorizontalBox)
						+ SHorizontalBox::Slot()
						[
							SNew(STextBlock).Text(FText::FromString("Reward:"))
						]
						+ SHorizontalBox::Slot()
						[
							SNew(SResourceTextBox)
							.Type(EResourceTextBoxType::Donate)
							.Value(600)
						]
					]
					+ SVerticalBox::Slot().Padding(FMargin(10, 2)).AutoHeight()
					[
						SNew(SHorizontalBox)
						+ SHorizontalBox::Slot()
						[
							SNew(STextBlock).Text(FText::FromString("End in:"))
						]
						+ SHorizontalBox::Slot()
						[
							SNew(SResourceTextBox)
							.Type(EResourceTextBoxType::Time)
							.Value(3600)
						]
					]
					+ SVerticalBox::Slot().Padding(FMargin(10, 2)).AutoHeight()
					[
						SNew(SButton).Text(FText::FromString("Join"))
						.OnClicked(OnClickedJoin)
					]
				]
			]
		]
	];	

	SetActiveStep(0);

	Steps[0]->GetItem(0)->SetOtherItem(Steps[1]->GetItem(0));
	Steps[0]->GetItem(1)->SetOtherItem(Steps[1]->GetItem(0));
	Steps[0]->GetItem(2)->SetOtherItem(Steps[1]->GetItem(1));
	Steps[0]->GetItem(3)->SetOtherItem(Steps[1]->GetItem(1));

	Steps[1]->GetItem(0)->SetOtherItem(Steps[2]->GetItem(0));
	Steps[1]->GetItem(1)->SetOtherItem(Steps[2]->GetItem(0));

	Items.Add(Steps[0]->GetItem(0));
	Items.Add(Steps[0]->GetItem(1));
	Items.Add(Steps[0]->GetItem(2));
	Items.Add(Steps[0]->GetItem(3));

	Items.Add(Steps[1]->GetItem(0));
	Items.Add(Steps[1]->GetItem(1));
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void STournamentTree::SetActiveStep(const int32 &Index)
{
	for (auto step : Steps)
	{
		step->SetIsActive(false);
	}

	if (Steps.IsValidIndex(Index))
	{
		Steps[Index]->SetIsActive(true);
	}
}

void STournamentTree::SetTeamPair(const int32 &Index, const int32& PairIndex, const FTournamentMatchDetails &PairTeam)
{
	if (Steps.IsValidIndex(Index))
	{
		if (Steps[Index]->GetItem(PairIndex).IsValid())
		{
			Steps[Index]->GetItem(PairIndex)->SetTeamPair(PairTeam);
		}
	}
}

int32 STournamentTree::OnPaint(const FPaintArgs& Args, const FGeometry& AllottedGeometry, const FSlateRect& MyClippingRect, FSlateWindowElementList& OutDrawElements, int32 LayerId, const FWidgetStyle& InWidgetStyle, bool bParentEnabled) const
{
	for (auto item : Items)
	{
		if (item.IsValid() && item->GetOtherItem().IsValid())
		{
			auto target = item->GetOtherItem();

			FVector2D StartGeometryPosition;
			FVector2D StartGeometrySize;
		
			GetChildPositionAndSize(AllottedGeometry, item.ToSharedRef(), StartGeometryPosition, StartGeometrySize);
		
			StartGeometryPosition += StartGeometrySize;
			StartGeometryPosition.Y = StartGeometryPosition.Y - (StartGeometrySize.Y / 2);

			FVector2D EndGeometryPosition;
			FVector2D EndGeometrySize;
		
			GetChildPositionAndSize(AllottedGeometry, target.ToSharedRef(), EndGeometryPosition, EndGeometrySize);
		
			EndGeometryPosition.Y = EndGeometryPosition.Y + (EndGeometrySize.Y / 2);

			FVector2D StartGeometryDir = StartGeometryPosition;
			FVector2D EndGeometryDir = EndGeometryPosition;

			FSlateDrawElement::MakeDrawSpaceSpline(
				OutDrawElements,
				LayerId + 10,
				StartGeometryPosition, StartGeometryDir,
				EndGeometryPosition, EndGeometryDir,
				MyClippingRect,
				4.0f,
				ESlateDrawEffect::None,
				FColor::Black
			);
		}
	}

	return SCompoundWidget::OnPaint(Args, AllottedGeometry, MyClippingRect, OutDrawElements, LayerId, InWidgetStyle, bParentEnabled);
}

void STournamentTree::GetChildPositionAndSize(const FGeometry& MyGeometry, TSharedRef<SWidget> WidgetToFind, FVector2D &OutPosition, FVector2D &OutSize) const
{
	auto StarterGeometry = STournamentTree::FindChildGeometry(MyGeometry, WidgetToFind);

	OutPosition = StarterGeometry.GetAccumulatedLayoutTransform().GetTranslation();
	OutSize = StarterGeometry.GetLocalSize() * StarterGeometry.GetAccumulatedLayoutTransform().GetScale();
}