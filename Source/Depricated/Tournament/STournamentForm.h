// VRSPRO

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "Slate/Styles/TournamentFormWidgetStyle.h"

DECLARE_DELEGATE_OneParam(FOnClickedTournament, const FString&);

struct FPlayerEntityInfo;

class LOKAGAME_API STournamentForm : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(STournamentForm)
		: _Style(&FLokaSlateStyle::Get().GetWidgetStyle<FTournamentFormStyle>("STournamentFormStyle"))
	{}
	SLATE_STYLE_ARGUMENT(FTournamentFormStyle, Style)
	SLATE_EVENT(FOnClickedTournament, OnClickedTournament)
	SLATE_END_ARGS()

	/** Constructs this widget with InArgs */
	void Construct(const FArguments& InArgs);

	void Append(const struct FTournamentPreview &Info);
	void Clear();

protected:

	TSharedPtr<class SScrollBox> ListCintainer;

	const FTournamentFormStyle *Style;

	FReply onClicked(const FString Uid);

	FOnClickedTournament OnClickedTournament;
};
