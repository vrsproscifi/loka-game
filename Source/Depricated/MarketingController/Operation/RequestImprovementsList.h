#pragma once
#include "ServiceOperation.h"
#include "Marketing/Improvements/ImprovementView.h"
#include "PoolingOperation.h"

namespace Operation
{
	namespace RequestImprovementsList
	{
		class Operation final 
			: public COperation<void, ObjectSerializer::JsonObject, FImprovementViewModel, ObjectSerializer::JsonObject>
			, public FPoolingOperation

		{
		public:			
			Operation() 
				: Super(FServerHost::MasterClient, "Marketing/RequestImprovementsList", FVerb::Get)
				, FPoolingOperation(15.0f, 2.5f)
			{
				TimerDelegate.BindRaw(this, &Operation::Request);
			}

		};
	}
}
