// VRSPRO

#include "LokaGame.h"
#include "SFractionManager.h"
#include "SlateOptMacros.h"

#include "Layout/SScaleBox.h"

// Components
#include "SFractionManager_Column.h"
#include "SFractionManager_Botton.h"

#include "SWindowBackground.h"

// ...
#include "Fraction/FractionEntity.h"
#include <Character/CharacterBaseEntity.h>


BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SFractionManager::Construct(const FArguments& InArgs)
{
	Style					= InArgs._Style;
	OnClickedItem			= InArgs._OnClickedItem;
	OnFractionSelect		= InArgs._OnFractionSelect;
	CurrentFraction			= 0;
	CurrentCharacter		= 0;

	SAssignNew(Sort_Characters, SListView<UCharacterBaseEntity*>)
		.SelectionMode(ESelectionMode::Single)
		.ListItemsSource(&List_Characters)
		.OnSelectionChanged(this, &SFractionManager::OnSetCurrentCharacter)
		.OnGenerateRow_Lambda([s = Style](UCharacterBaseEntity* Item, const TSharedRef<STableViewBase>& OwnerTable)
	{
		return
			SNew(STableRow<UItemBaseEntity*>, OwnerTable)
			[
				SNew(STextBlock).Text(Item->GetDescription().Name)
			];
	});

	ChildSlot
	[
		SNew(SWindowBackground)
		.IsEnabledBlur(true)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot().AutoHeight()
			[
				SNew(SBox).MinDesiredHeight(30)
				[
					SNew(SOverlay)
					+ SOverlay::Slot().HAlign(HAlign_Fill).VAlign(VAlign_Fill).Padding(FMargin(680, 0))
					[				
						SAssignNew(ButtonsContainer, SHorizontalBox)
					]
					+ SOverlay::Slot().HAlign(HAlign_Right).VAlign(VAlign_Fill)
					[
						SNew(SComboButton)
						.ComboButtonStyle(&Style->Container.CharSelector)
						.ButtonContent()
						[
							SNew(STextBlock).Text_Lambda([&]() { return List_Characters[CurrentCharacter]->GetDescription().Name; })
						]
						.MenuContent()
						[
							Sort_Characters.ToSharedRef()
						]
					]
				]
			]
			+ SVerticalBox::Slot().FillHeight(1.0f).HAlign(HAlign_Center)
			[
				SNew(SBox).WidthOverride(480*4)
				[
					SAssignNew(Switcher, SWidgetSwitcher).WidgetIndex(this, &SFractionManager::GetActiveFractionIndex)
				]
			]
		]
	];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SFractionManager::OnScrolledFraction(float Offset, const int32 Target)
{	
	//if (Offset < Fractions[Target].CachedScrollOffset)
	//{	
	//	Fractions[Target].CachedScrollIndex = FMath::Clamp(Fractions[Target].CachedScrollIndex - 1, 0, Fractions[Target].Instance->RankList.Num() - 4);		
	//	Fractions[Target].ScrollBox->SetScrollOffset(480 * Fractions[Target].CachedScrollIndex);
	//}
	//else if (Offset > Fractions[Target].CachedScrollOffset)
	//{
	//	Fractions[Target].CachedScrollIndex = FMath::Clamp(Fractions[Target].CachedScrollIndex + 1, 0, Fractions[Target].Instance->RankList.Num() - 4);
	//	Fractions[Target].ScrollBox->SetScrollOffset(480 * Fractions[Target].CachedScrollIndex);
	//}

	//Fractions[Target].CachedScrollOffset = Offset;

	GEngine->AddOnScreenDebugMessage(2, 1.5f, FColor::White, *FString::Printf(TEXT("OnScrolledFraction >> offset: %.2f, target: %02d, scrollIndex: %d"), Offset, Target, Fractions[Target].CachedScrollIndex));
}

int32 SFractionManager::GetActiveFractionIndex() const
{
	return CurrentFraction;
}

ECheckBoxState SFractionManager::IsActiveFractionIndex(const int32 i) const
{
	return (CurrentFraction == i) ? ECheckBoxState::Checked : ECheckBoxState::Unchecked;
}

void SFractionManager::OnActiveFractionIndex(ECheckBoxState, const int32 i)
{
	CurrentFraction = i;
}

void SFractionManager::OnSetCurrentCharacter(UCharacterBaseEntity* Character, ESelectInfo::Type Info)
{
	CurrentCharacter = List_Characters.Find(Character);
}

//===================================================================================================================================================================================================================] Public methods

void SFractionManager::AddFraction(const UFractionEntity* Fraction)
{
	if (!Fraction) return;
	
	FractionContainer Container;

	Container.Instance = Fraction;

	auto CurrentIndex = Fractions.Num();

	SAssignNew(Container.ScrollBox, SScrollBox)
		.AllowOverscroll(EAllowOverscroll::No)
		.ConsumeMouseWheel(EConsumeMouseWheel::Always)
		.Orientation(EOrientation::Orient_Horizontal)
		.OnUserScrolled(this, &SFractionManager::OnScrolledFraction, CurrentIndex)
		.ScrollBarStyle(&Style->Container.ScrollBar);

	Switcher->AddSlot()[Container.ScrollBox.ToSharedRef()];

	ButtonsContainer->AddSlot().HAlign(HAlign_Fill).VAlign(VAlign_Fill).FillWidth(1)
		[
			SAssignNew(Container.ToggleButton, SFractionManager_Botton)
			.IsChecked(this, &SFractionManager::IsActiveFractionIndex, CurrentIndex)
			.OnCheckStateChanged(this, &SFractionManager::OnActiveFractionIndex, CurrentIndex)
			.Image(Fraction->GetIcon())
			.Name(Fraction->GetDescription().Name)
			.OnClickedJoin_Lambda([&, f = Fraction]() { OnFractionSelect.ExecuteIfBound(f->GetModelIdAs<int32>()); return FReply::Handled(); })
		];

	SIZE_T _count = 1;
	for (auto rank : Fraction->GetRankList())
	{
		auto Column = SNew(SFractionManager_Column, Container.Instance).OnClickedItem(OnClickedItem).TargetLevel(_count);

		Container.ScrollBox->AddSlot()
			[
				SNew(SBox).WidthOverride(633).Padding(FMargin(4, 0))
				[
					Column
				]
			];

		Column->FillList(Fraction->GetItemsByLevel(Container.Columns.Num(), true));

		Container.Columns.Add(Column);
		++_count;
	}

	Fractions.Add(Container);
}

bool SFractionManager::RemoveFraction(const UFractionEntity* Fraction)
{
	return false;
}

bool SFractionManager::SetActiveFraction(const UFractionEntity* Fraction)
{
	SIZE_T _count = 0;
	for (auto &i : Fractions)
	{
		if (i.Instance == Fraction)
		{
			OnActiveFractionIndex(ECheckBoxState::Checked, _count);
			return true;
		}
		++_count;
	}

	return false;
}

void SFractionManager::AddCharacter(UCharacterBaseEntity* Character)
{
	List_Characters.Add(Character);
	Sort_Characters->RequestListRefresh();
}