#pragma once
#include "ImprovementView.generated.h"

enum class EImprovementProgressState : uint8;
enum class EImprovementCategoryId : uint8;


USTRUCT()
struct FImprovementView
{
	GENERATED_USTRUCT_BODY()
	
	UPROPERTY(VisibleInstanceOnly) FString ImprovementId;
	UPROPERTY(VisibleInstanceOnly) EImprovementProgressState ProgressState;
	UPROPERTY(VisibleInstanceOnly) EImprovementCategoryId CategoryId;
	UPROPERTY(VisibleInstanceOnly) FString Title;
	UPROPERTY(VisibleInstanceOnly) FString Message;
	UPROPERTY(VisibleInstanceOnly) int64 CreatedDate;
	UPROPERTY(VisibleInstanceOnly) int64 Count;
	UPROPERTY(VisibleInstanceOnly) bool IsVoted;
};

USTRUCT()
struct FImprovementViewModel
{
	GENERATED_USTRUCT_BODY()

		UPROPERTY(VisibleInstanceOnly) int64 DateOfEndVotes;
		UPROPERTY(VisibleInstanceOnly) TArray<FImprovementView> ImprovementViews;
		UPROPERTY(VisibleInstanceOnly) TArray<FImprovementView> SentenceViews;
};