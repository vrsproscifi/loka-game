#pragma once
#include "ImprovementCreateView.generated.h"

enum class EImprovementCategoryId : uint8;


USTRUCT()
struct FImprovementCreateView
{
	GENERATED_USTRUCT_BODY()
	
	UPROPERTY(VisibleInstanceOnly) EImprovementCategoryId CategoryId;
	UPROPERTY(VisibleInstanceOnly) FString Title;
	UPROPERTY(VisibleInstanceOnly) FString Message;
};
