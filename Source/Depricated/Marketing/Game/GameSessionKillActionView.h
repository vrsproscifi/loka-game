#pragma once
#include "GameSessionKillActionView.generated.h"

USTRUCT()
struct FGameDamageStatistic
{
	GENERATED_USTRUCT_BODY()

		UPROPERTY(VisibleInstanceOnly) int64 Kills;
	UPROPERTY(VisibleInstanceOnly) int64 Deads;
	UPROPERTY(VisibleInstanceOnly) int64 Assists;
};