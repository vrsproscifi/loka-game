#pragma once

#include "Company/ScenarioCompanyEntity.h"
#include "ScenarioMissionRepository.generated.h"

UCLASS(BlueprintType, Blueprintable)
class UScenarioMissionRepository : public UDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(EditDefaultsOnly)			FScenarioCompanyEntity Companies[EScenarioCompanyId::End];
	void OnInitialize(const TArray<FScenarioMissionView>& missions)
	{
		for(const auto mp : missions)
		{
			//==============================================================
			if(mp.CompanyId < 0 || mp.CompanyId >= EScenarioCompanyId::End)
			{
				continue;
			}

			//==============================================================
			auto &company = Companies[mp.CompanyId];
			auto &mission = company.Missions[mp.AbstractMissionId];

			//==============================================================
			mission.Company = &company;
			mission.Property = mp;

			//==============================================================
			for(const auto mt : mp.TaskViews)
			{
				auto &task = mission.Tasks[mt.AbstractTaskId];
				task.Mission = &mission;
				task.Company = &company;
				task.Property = mt;
			}
		}
	}
};