#pragma once

#include "PlayerMissionProgress.generated.h"

USTRUCT()
struct FPlayerMissionProgress
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Transient)		int32 Current;
	UPROPERTY(Transient)		int32 Last;
};