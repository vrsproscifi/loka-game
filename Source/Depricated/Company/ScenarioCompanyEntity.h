#pragma once

#include "ScenarioCompanyId.h"
#include "ScenarioMissionEntity.h"
#include "ScenarioCompanyEntity.generated.h"

USTRUCT(BlueprintType)
struct FScenarioCompanyEntity
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly)		FString Name;
	UPROPERTY(EditDefaultsOnly)		FString Description;
	UPROPERTY(EditDefaultsOnly)		FSlateBrush Image;
	UPROPERTY(Transient)			TEnumAsByte<EScenarioCompanyId::Type> Id;
	UPROPERTY(EditDefaultsOnly)		TArray<FScenarioMissionEntity> Missions;
};