// VRSPRO

#include "LokaGame.h"
#include "BasePlayerCharacter.h"
#include <Armour/ItemArmourHelmet.h>
#include <Weapon/ItemWeaponEntity.h>
#include "Module/ItemModuleEntity.h"
#include "EntityRepository.h"
#include "ShooterGameInstance.h"

// Sets default values
ABasePlayerCharacter::ABasePlayerCharacter()
	: EntityInstance(nullptr)
	, HelmetEntity(nullptr)
	, MaskEntity(nullptr)
	, GlovesEntity(nullptr)
	, BackpackEntity(nullptr)
	, BodyEntity(nullptr)
	, PantsEntity(nullptr)
	, BootsEntity(nullptr)
	, PrimaryWeaponEntity(nullptr)
	, SecondaryWeaponEntity(nullptr)
{

	GetMesh()->SetRelativeLocation(FVector(0.0f, 0.0f, -90.0f));
	GetMesh()->SetRelativeRotation(FRotator(0.0f, -90.0f, 0.0f));
	PrimaryActorTick.bCanEverTick = true;


	//-----------------------------------------------------------------------------------------
	//	Head
	{
		HelmetEntity = ArmourSlotMap.Add(ItemSlotTypeId::Helmet, CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("ArmourSlot_Helmet")));
		MaskEntity = ArmourSlotMap.Add(ItemSlotTypeId::Mask, CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("ArmourSlot_Mask")));
	}
	
	//-----------------------------------------------------------------------------------------
	//	Body
	{
		GlovesEntity = ArmourSlotMap.Add(ItemSlotTypeId::Gloves, CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("ArmourSlot_Gloves")));
		BackpackEntity = ArmourSlotMap.Add(ItemSlotTypeId::Backpack, CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("ArmourSlot_Backpack")));
		BodyEntity = ArmourSlotMap.Add(ItemSlotTypeId::Body, CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("ArmourSlot_Body")));
	}
	
	//-----------------------------------------------------------------------------------------
	//	Foot
	{
		PantsEntity = ArmourSlotMap.Add(ItemSlotTypeId::Pants, CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("ArmourSlot_Pants")));
		BootsEntity = ArmourSlotMap.Add(ItemSlotTypeId::Boots, CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("ArmourSlot_Boots")));
	}

	//-----------------------------------------------------------------------------------------
	//	Attach Armour Slots
	for (auto& slot : ArmourSlotMap)
	{
		slot.Value->SetupAttachment(GetMesh());

		slot.Value->SetCollisionObjectType(ECC_Pawn);
		slot.Value->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
		slot.Value->SetCollisionResponseToChannel(ECC_Visibility, ECR_Block);
		slot.Value->SetMasterPoseComponent(GetMesh());
	}

	//-----------------------------------------------------------------------------------------
	//	Weapon
	{
		PrimaryWeaponEntity = WeaponSlotMap.Add(ItemSlotTypeId::PrimaryWeapon, CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponSlot_PrimaryWeapon")));
		SecondaryWeaponEntity = WeaponSlotMap.Add(ItemSlotTypeId::SecondaryWeapon, CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponSlot_SecondaryWeapon")));
	}

	for (int32 i = 0; i < 10; ++i)
	{
		InstalledModulesMesh.Add(CreateDefaultSubobject<USkeletalMeshComponent>(*FString::Printf(TEXT("Module_%d"), i)));
		InstalledModulesMesh.Last()->SetupAttachment(RootComponent);
		InstalledModulesMesh.Last()->bReceivesDecals = false;
		InstalledModulesMesh.Last()->bComponentUseFixedSkelBounds = true;
		InstalledModulesMesh.Last()->SetVisibility(false, true);
	}
}

void ABasePlayerCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	FAttachmentTransformRules Rules(EAttachmentRule::SnapToTarget, false);
	//-----------------------------------------------------------------------------------------
	//	Attach Armour Slots  RightHandSocket - 1, LeftHandSocket - 2
	for (auto &slot : WeaponSlotMap)
	{
		slot.Value->SetCollisionObjectType(ECC_Pawn);
		slot.Value->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
		slot.Value->SetCollisionResponseToChannel(ECC_Visibility, ECR_Block);

		if (slot.Key == ItemSlotTypeId::PrimaryWeapon)
		{	
			slot.Value->AttachToComponent(GetMesh(), Rules, TEXT("RightHandSocket"));
		}
		else if (slot.Key == ItemSlotTypeId::SecondaryWeapon)
		{
			slot.Value->AttachToComponent(GetMesh(), Rules, TEXT("LeftHandSocket"));
		}
	}
}

void ABasePlayerCharacter::SetArmourEntity(const UItemArmourEntity* armourEntity)
{
	if (armourEntity)
	{
		SetArmourEntitySlot(armourEntity, armourEntity->GetDescription().SlotType);
	}
}

void ABasePlayerCharacter::SetArmourEntitySlot(const UItemArmourEntity* armourEntity, const ItemSlotTypeId::Type& slot)
{
	if (armourEntity && ArmourSlotMap.Contains(slot))
	{
		auto armourSlotInstance = ArmourSlotMap.FindChecked(slot);
		//armourSlotInstance->SetArmourInstance(armourEntity);
		armourSlotInstance->SetSkeletalMesh(armourEntity->GetSkeletalMesh());
		armourSlotInstance->SetVisibility(true);
		armourSlotInstance->SetMaterial(0, armourEntity->GetCurrentMaterial());
		OnArmourInstalled.Broadcast(armourEntity);
	}
}

void ABasePlayerCharacter::UnSetArmourEntity(const UItemArmourEntity * armourEntity)
{
	if (armourEntity && ArmourSlotMap.Contains(armourEntity->GetDescription().SlotType))
	{
		auto armourSlotInstance = ArmourSlotMap.FindChecked(armourEntity->GetDescription().SlotType);
		armourSlotInstance->SetSkeletalMesh(nullptr);
		OnArmourUnInstalled.Broadcast(armourEntity);
	}
}

void ABasePlayerCharacter::SetWeaponEntity(const UItemWeaponEntity* WeaponEntity)
{
	if (WeaponEntity)
	{
		SetWeaponEntitySlot(WeaponEntity, WeaponEntity->GetDescription().SlotType);
	}
}

void ABasePlayerCharacter::SetWeaponEntitySlot(const UItemWeaponEntity* WeaponEntity, const ItemSlotTypeId::Type& Slot)
{
	if (WeaponEntity && WeaponSlotMap.Contains(Slot))
	{
		FString SlotString = (Slot == ItemSlotTypeId::PrimaryWeapon) ? "PrimaryWeapon" : "SecondaryWeapon";

		auto SlotInstance = WeaponSlotMap.FindChecked(Slot);
		SlotInstance->SetSkeletalMesh(WeaponEntity->GetSkeletalMesh());
		SlotInstance->SetAnimInstanceClass(WeaponEntity->GetAnimClass());
		SlotInstance->SetVisibility(true);
		SlotInstance->SetMaterial(0, WeaponEntity->GetCurrentMaterial());

		for (auto &s : InstalledModulesMeshMap)
		{
			if (s.Key.ToString().Find(SlotString) != INDEX_NONE)
			{
				s.Value->SetVisibility(false, true);
				InstalledModulesMeshMap.Remove(s.Key);
			}
		}

		for (auto s : WeaponEntity->SupportedSockets)
		{
			for (auto &c : InstalledModulesMesh)
			{
				if (!c->IsVisible())
				{
					InstalledModulesMeshMap.Add(*FString(SlotString + s.ToString()), c);
					c->SetSkeletalMesh(nullptr);
					c->AttachToComponent(SlotInstance, FAttachmentTransformRules::SnapToTargetIncludingScale, s);
					c->bComponentUseFixedSkelBounds = true;
					c->SetVisibility(true, true);
					break;
				}
			}
		}
		
		auto GameInst = Cast<UShooterGameInstance>(GetWorld()->GetGameInstance());
		if (GameInst)
		{
			auto Repository = GameInst->EntityRepository->InstanceRepository;

			if (WeaponEntity->DefaultModules.Num())
			{
				TArray<uint8> NoInstall;
				for (auto i : WeaponEntity->DefaultModules)
				{
					if (i >= 0 && i < ItemModuleModelId::End && Repository.Module[i])
					{
						for (auto &m : WeaponEntity->InstalledModules)
						{
							InstallModule(m, Slot);

							if (m->TargetSocket == Repository.Module[i]->TargetSocket)
							{
								NoInstall.Add(i);
							}
						}
					}
				}

				for (auto i : WeaponEntity->DefaultModules)
				{
					if (NoInstall.Contains(i) == false)
					{
						if (i >= 0 && i < ItemModuleModelId::End && Repository.Module[i])
						{
							InstallModule(Repository.Module[i], Slot);
						}
					}
				}
			}
			else
			{
				for (auto i : WeaponEntity->SupportedModules)
				{
					if (i >= 0 && i < ItemModuleModelId::End && Repository.Module[i])
					{
						for (auto &m : WeaponEntity->InstalledModules)
						{
							InstallModule(m, Slot);
						}
					}
				}
			}
		}

		OnWeaponInstalled.Broadcast(WeaponEntity);
	}
}

void ABasePlayerCharacter::UnsetWeaponEntity(const UItemWeaponEntity* WeaponEntity)
{
	if (WeaponEntity && WeaponSlotMap.Contains(WeaponEntity->GetDescription().SlotType))
	{
		auto SlotInstance = WeaponSlotMap.FindChecked(WeaponEntity->GetDescription().SlotType);
		//SlotInstance->SetVisibility(false);
		SlotInstance->SetSkeletalMesh(nullptr);

		for (auto &m : WeaponEntity->InstalledModules)
		{
			UninstallModule(m, WeaponEntity->GetDescription().SlotType);
		}

		OnWeaponUnInstalled.Broadcast(WeaponEntity);
	}
}

void ABasePlayerCharacter::ApplyEntity(const UCharacterBaseEntity* entity)
{
}

void ABasePlayerCharacter::InitializeEntity(const UCharacterBaseEntity* entity)
{
	for (auto &s : ArmourSlotMap)
	{
		if (s.Value && s.Value->IsValidLowLevel())
		{
			s.Value->SetVisibility(false, true);
		}
	}

	for (auto &s : WeaponSlotMap)
	{
		if (s.Value && s.Value->IsValidLowLevel())
		{
			s.Value->SetVisibility(false, true);
		}
	}

	if (VisualActor)
	{
		VisualActor->Destroy();
		VisualActor = nullptr;
	}	

	checkf(entity, ANSI_TO_TCHAR("entity was nullptr"));
	EntityInstance = entity;
	GetMesh()->SetSkeletalMesh(entity->GetSkeletalMesh());
	GetMesh()->SetAnimInstanceClass(entity->GetAnimClass());

	//TODO:SeNTike
	//for (auto &s : ArmourSlotMap)
	//{
	//	if (s.Value && s.Value->IsValidLowLevel())
	//	{
	//		s.Value->SetVisibility(!EntityInstance->IsLocked(), true);
	//	}
	//}
	//
	//for (auto &s : WeaponSlotMap)
	//{
	//	if (s.Value && s.Value->IsValidLowLevel())
	//	{
	//		s.Value->SetVisibility(!EntityInstance->IsLocked(), true);
	//	}
	//}	

	FActorSpawnParameters Params;
	Params.Instigator = this;
	Params.Owner = this;

	VisualActor = GetWorld()->SpawnActor<AActor>(EntityInstance->VisualActor, GetTransform(), Params);

	//FTimerHandle _th;
	//GetWorldTimerManager().SetTimer(_th, FTimerDelegate::CreateLambda([&]() {
		// Try set Z correctly
		FVector StartTrace, EndTrace;
		StartTrace = GetActorLocation();
		EndTrace = StartTrace + FVector(0, 0, -1000);
		FHitResult HitResult;
		FCollisionQueryParams FindZParams(TEXT("FindZParams"), false, this);
		FCollisionResponseParams FindZResponseParams = FCollisionResponseParams::DefaultResponseParam;
		FindZResponseParams.CollisionResponse.SetAllChannels(ECR_Ignore);
		FindZResponseParams.CollisionResponse.SetResponse(ECC_WorldStatic, ECR_Block);

		if (GetWorld()->LineTraceSingleByChannel(HitResult, StartTrace, EndTrace, ECC_WorldStatic, FindZParams, FindZResponseParams))
		{
			FVector BoundsCenter, BoundsExtent;
			GetMesh()->UpdateBounds();
			BoundsCenter = GetMesh()->Bounds.GetBox().GetCenter();
			BoundsExtent = GetMesh()->Bounds.GetBox().GetExtent();

			SetActorLocation(HitResult.Location + FVector(0, 0, BoundsExtent.Z));

#if (ENABLE_DRAW_DEBUG && WITH_EDITOR)
			::DrawDebugBox(GetWorld(), BoundsCenter, BoundsExtent, FColor::Orange, false, 10.0f);

			::DrawDebugLine(GetWorld(), StartTrace, HitResult.Location, FColor::Green, false, 10.0f);
			::DrawDebugPoint(GetWorld(), HitResult.Location, 10, FColor::Red, false, 10.0f);
#endif
		}
	//}), FMath::FRandRange(0.2f, 1.1f), false);
}

void ABasePlayerCharacter::RestoreEnity()
{
}

void ABasePlayerCharacter::SwapEnity(const UCharacterBaseEntity* entity)
{
}

// Called when the game starts or when spawned
void ABasePlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABasePlayerCharacter::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

// Called to bind functionality to input
void ABasePlayerCharacter::SetupPlayerInputComponent(class UInputComponent* InInputComponent)
{
	Super::SetupPlayerInputComponent(InInputComponent);

}

void ABasePlayerCharacter::InstallModule(UItemModuleEntity* InModule, const ItemSlotTypeId::Type& Slot)
{
	FName SlotString = *FString((Slot == ItemSlotTypeId::PrimaryWeapon ? "PrimaryWeapon" : "SecondaryWeapon") + InModule->TargetSocket.ToString());

	if (InstalledModulesMeshMap.Contains(SlotString))
	{
		InstalledModulesMeshMap[SlotString]->SetSkeletalMesh(InModule->GetSkeletalMesh());
	}
}

void ABasePlayerCharacter::UninstallModule(UItemModuleEntity* InModule, const ItemSlotTypeId::Type& Slot)
{
	FName SlotString = *FString((Slot == ItemSlotTypeId::PrimaryWeapon ? "PrimaryWeapon" : "SecondaryWeapon") + InModule->TargetSocket.ToString());

	if (InstalledModulesMeshMap.Contains(SlotString))
	{
		InstalledModulesMeshMap[SlotString]->SetSkeletalMesh(nullptr);
	}
}