// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaGame.h"
#include "BuilderCharacter.h"
#include "Interfaces/UsableActorInterface.h"
#include "BuildHelperComponent.h"

// Sets default values
ABuilderCharacter::ABuilderCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	bReplicates = true;
	bIsAllowAnyBuildMode = true;
}

// Called when the game starts or when spawned
void ABuilderCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (Role == ROLE_Authority)
	{
		FActorSpawnParameters SpawnParameters;
		SpawnParameters.Instigator = this;
		SpawnParameters.Owner = this;
		SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		BuildHelperComponent = GetWorld()->SpawnActor<ABuildHelperComponent>(BuildHelperComponentClass, SpawnParameters);	
	}
}

// Called every frame
void ABuilderCharacter::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
	auto tmpUsable = GetUsableObject();
	auto tmpInterface = Cast<IUsableActorInterface>(tmpUsable);

	if (UsableTargetObject != tmpInterface)
	{
		if (UsableTargetObject)
		{
			IUsableActorInterface::Execute_OnFocusLost(UsableTargetObject.GetObject(), this);
		}

		UsableTargetObject.SetObject(tmpUsable);
		UsableTargetObject.SetInterface(tmpInterface);

		if (UsableTargetObject)
		{			
			IUsableActorInterface::Execute_OnFocusStart(UsableTargetObject.GetObject(), this);
		}
	}
}

// Called to bind functionality to input
void ABuilderCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &ABuilderCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ABuilderCharacter::MoveRight);
	PlayerInputComponent->BindAxis("MoveUp", this, &ABuilderCharacter::MoveUp);
	PlayerInputComponent->BindAxis("Turn", this, &ABuilderCharacter::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ABuilderCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &ABuilderCharacter::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ABuilderCharacter::LookUpAtRate);

	PlayerInputComponent->BindAction("UseAny", IE_Pressed, this, &ABuilderCharacter::OnUseAny_Helper<true>);
	PlayerInputComponent->BindAction("UseAny", IE_Released, this, &ABuilderCharacter::OnUseAny_Helper<false>);
}

bool ABuilderCharacter::OnUseAny_Validate(const bool IsPressed) { return true; }
void ABuilderCharacter::OnUseAny_Implementation(const bool IsPressed)
{
	if (UsableTargetObject)
	{
		if (IsPressed)
		{
			IUsableActorInterface::Execute_OnInteractStart(UsableTargetObject.GetObject(), this);
		}
		else
		{
			IUsableActorInterface::Execute_OnInteractLost(UsableTargetObject.GetObject(), this);
		}
	}
}

void ABuilderCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// find out which way is forward
		const FRotator Rotation = GetControlRotation();
		FRotator YawRotation = FRotator(0, Rotation.Yaw, 0);

		AddMovementInput(FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X), Value);
	}
}

void ABuilderCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// find out which way is right
		const FRotator Rotation = GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		AddMovementInput(FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y), Value);
	}
}

void ABuilderCharacter::MoveUp(float Value)
{
	if (Value != 0.0f)
	{
		AddMovementInput(FVector(0.f, 0.f, 1.f), Value);
	}
}

void ABuilderCharacter::TurnAtRate(float Val)
{
	AddControllerYawInput(Val * 45.0f * GetWorld()->GetDeltaSeconds());
}

void ABuilderCharacter::LookUpAtRate(float Val)
{
	AddControllerPitchInput(Val * 45.0f * GetWorld()->GetDeltaSeconds());
}

void ABuilderCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ABuilderCharacter, BuildHelperComponent);
}

AActor* ABuilderCharacter::GetUsableObject()
{
	AActor* Answer = nullptr;
	APlayerController* PlayerController = Cast<APlayerController>(GetController());	

	if (PlayerController && GetWorld())
	{
		FVector TraceStart = FVector::ZeroVector;
		FVector TraceEnd = FVector::ZeroVector;
		FRotator CamRot;
		PlayerController->GetPlayerViewPoint(TraceStart, CamRot);
		TraceEnd = TraceStart + CamRot.Vector() * 100.0f * 3.0f;

		FHitResult HitResult;
		FCollisionQueryParams QueryFindUsableObject(TEXT("FindUsableObject"), false, this);
		QueryFindUsableObject.AddIgnoredActor(BuildHelperComponent);

		FCollisionResponseParams ResponseFindUsableObject = FCollisionResponseParams::DefaultResponseParam;
		ResponseFindUsableObject.CollisionResponse.SetAllChannels(ECR_Block);
		ResponseFindUsableObject.CollisionResponse.SetResponse(COLLISION_USABLE, ECR_Block);

		if (GetWorld()->LineTraceSingleByChannel(HitResult, TraceStart, TraceEnd, COLLISION_USABLE, QueryFindUsableObject, ResponseFindUsableObject))
		{
			Answer = HitResult.GetActor();
		}
	}

	return Answer;
}

void ABuilderCharacter::SetAllowAnyBuildMode(const bool InAllow)
{
	bIsAllowAnyBuildMode = InAllow;

	if (bIsAllowAnyBuildMode == false && BuildHelperComponent)
	{
		BuildHelperComponent->SetCurrentMode(EBuildHelperMode::None);
	}
}